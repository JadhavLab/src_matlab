function h = find_blobs(x,periodic,threshold)
%FIND_BLOBS Detect blobs in an N-dimensional gridded intensity map.
%
%   H = FIND_BLOBS(X,PERIODIC,THRESHOLD) finds contiguous "blobs" around
%   local maxima in the gridded data X using a watershed algorithm. 
%
%   X must be an N-dimensional array whose elements are all finite. Values in X
%   are assumed to reside on a uniformly-spaced grid. 
%
%   PERIODIC must be a logical row vector whose length equals the number of
%   dimensions of X. If PERIODIC(d) is true, then X is interpreted as being
%   periodic along its dth dimension. (For example, if X is a 2-dimensional
%   array, and PERIODIC = [true true], then X is a torus.)
%
%   THRESH is a two-element vector of threshold values of the same numeric class
%   as X. A blob is defined as a collection of contiguous voxels, of which at
%   least one voxel has value greater than or equal to TRESH(1) and all included
%   voxels have values greater than or equal to THRESH(2). A pair of voxels are
%   "contiguous" if they share a common edge (more than just a corner vertex).
%
%   The output H is an array of the same size as X, containing non-negative
%   integer values. Voxels in X which do not belong to a blob are indicated by
%   zero. Voxels which belong to a blob are indicated by an integer value which
%   enumerates blob identity. The order of enumeration is arbitrary.
%   
%
%Written by smk, 2008 November 8.
%

if ~isreal(thresh) || ~isvector(thresh) || (numel(thresh) ~= 2) || ...
    ~all(isfinite(thresh))
  error('THRESH must be a two-element real vector with finite values');
end
if (thresh(2) >= thresh(1))
  error('THRESH(2) must be smaller than THRESH(1)');
end

if isempty(x) || ~isnumeric(x) || ~isreal(x) || ~all(isfinite(x(:)))
  error(['X must be a non-empty real numeric array of finite values']);
end
% Get size of X
sz = size(x);

if ~islogical(periodic) || ~isvector(periodic) || ...
    ~isequal(size(periodic),size(sz))
  error(['PERIODIC must be a logical row vector of size [1, ndims(X)] ' ...
      '(remember that MATLAB gives column vectors a dimensionality of 2)']);
end

% Find non-singleton dimensions of X
non_singleton_dim = find(sz > 1);

% Number of voxels and subscripts
num_voxels = prod(sz);
[voxel_subs{1:n}] = ind2sub(sz,(1:num_voxels)');

% For each voxel, get the linear indices of its neighbors; each voxel faces 2*n
% neighbors (except for those along non-periodic boundaries
neighbors = cell([num_voxels, 1]);
for i = 1:numel(sz)
  if periodic(i)
    neighbors
    % enforce wrap-around at edges
    tempsubs{i} = voxsubs{i} - 1;
    tempsubs{i}(tempsubs{i} == 0) = sz(i);
    neighbors(:,2*i-1) = sub2ind(sz,tempsubs{:});
    tempsubs{i} = voxsubs{i} + 1;
    tempsubs{i}(tempsubs{i} == sz(i)+1) = 1;
    neighbors(:,2*i) = sub2ind(sz,tempsubs{:});
  else
    % edge voxels are assigned themselves as their own neighbors; these are
    % dummy entries which will be effectively ignored in the flood-fill
    % algorithm
    tempsubs{i} = voxsubs{i} - 1*(voxsubs{i} > 1);
    neighbors(:,2*i-1) = sub2ind(sz,tempsubs{:});
    tempsubs{i} = voxsubs{i} + 1*(voxsubs{i} < sz(i));
    neighbors(:,2*i) = sub2ind(sz,tempsubs{:});
  end
end

% initialize output
h = zeros(sz);
% find max and min values in D
peak = max(d(:));
base = min(d(:));
% identify voxels that exceed thresh(1); we will use these 
% as starting seeds for the flood-fill algorithm
seeds = find(d(:) > thresh(1));
% running count of hot spots
count = 0;
while ~isempty(seeds)
  % initialize queue with the seed voxel that has the 
  % smallest subscript along the 1st dimension
  seedsubs{1} = ind2sub(sz,seeds);
  [junk, idx] = min(seedsubs{1});
  queue = seeds(idx);
  count = count+1;
  while ~isempty(queue)
    % mark all queued voxels as belonging to the current hotspot
    h(queue) = count;
    % replace queue with new members, which are neighbors of previously queued
    % voxels
    queue = neighbors(queue,:); 
    queue = unique(queue(:));
    % remove those voxels which are already marked
    queue(h(queue) > 0) = []; 
    % remove those voxels which do not exceed threshold
    queue(d(queue) <= thresh(2)) = [];
    % trim the list of seeds
    seeds(h(seeds) > 0) = [];
  end
end

