% STATISTICS TOOL BOX (STAT)--(A JUTAN.,uwo  '98)
% DOCUMENTATION 
%
%   COXY         : Cov or Correlation of x,y 
%   CHITEST	: Chi  lack of fit test for univatiate Time series
%   CHIOVRAL	: Chi overall lack of fit test for XferFcn+Noise Time series
%   JNTCREG      : Joint Confidence Region eg
%     JOINTCR
%     MODELCR1
%     SUMSSCR
%  CURVEFITEG1	 :Least Squares Example FOR VER 5
%    MODELCURVE  : sub model for this eg
%  	REGDATA  : Regression data used with CURVEFIT IN OPTIMIZATION TOOLBOX
%   LEASRSM      : Least Squares for Response Surfaces
%      MODELRSM  : Sub Modelrsm forms RSM polynomials
%	  RSMX   : Forms X matrix for RSM models (main cross quad effects)
%      SYMUNPCK  : Unpacks Matrices for leasrsm
%      SYMPCK    : Reverse of Symunpck
%   MRESP	 :Multivariable Response Regression and Eigenvalues
%      mncn,auto : mean and auto centering of a data matrix (from pls)
%   NLIN	 : NonLinear Param estimation using Stats Toolbox
%     NLPARCI    : Modified nlparci from stats toolbox
%    NLINEG1     : Example
%   ODEFIT5	 : Fits ODE equations
%      MODODE5
%      XPRIME
%   OPTRSM	 : Maximise RSM models
%      MAXRSM1   : Sub model for OPTRSM
%   PLOTRSM      : Plot Surface and Contours for RSM models
%   RATCOF       : Rational Approx to any data set (as opposed to Poly approx)
%     RATERP     : Calcs output y from coeff produced in ratcof
%   YRSM	 : Calculate Y for RSM model single point
%   TSIM	 : Time series Generation
%   IDF		 : Time series Identification Plots
% WELCHT         : Test of two means with unequal variance
