% THICKLINES: Set global graphics defaults for thick lines, for Word and 
%             PowerPoint.
%

set(0,'DefaultLineLineWidth',1.5);       % Line plot width
set(0,'DefaultAxesLineWidth',1.5);       % Axis line width
