/** Encode values **/

#define    NONDISPLAYABLE_ENCODES    // Don't display encode values listed here
#define    NOCODE                                   0  
#define    SPIKE1                                         1  
#define    SPIKE2                                         2  
#define    DISPLAYABLE_ENCODES        // Display encode values listed here
#define    REWARD                                         3  
#define    BAR_UP                                         4  
#define    BAR_LEFT                                         5  
#define    BAR_RIGHT                                     6  
#define    BAR_DOWN                                         7  
#define    FIXATION_OCCURS                             8  
#define    START_INTER_TRIAL                             9  
#define    END_INTER_TRIAL                             10 
#define    START_WAIT_FIXATION                         11 
#define    END_WAIT_FIXATION                             12 
#define    START_WAIT_LEVER                             13 
#define    END_WAIT_LEVER                                 14 
#define    START_PRE_TRIAL                             15 
#define    END_PRE_TRIAL                                 16 
#define    START_POST_TRIAL                             17 
#define    END_POST_TRIAL                                 18 
#define    START_PAUSE                                     19 
#define    END_PAUSE                                     20 
#define    START_RANDOM_PAUSE                         21 
#define    END_RANDOM_PAUSE                             22 
#define    TURN_TEST0_ON                                23 
#define    TURN_TEST0_OFF                                24 
#define    TURN_TEST1_ON                                 25 
#define    TURN_TEST1_OFF                                 26 
#define    TURN_TEST2_ON                                 27 
#define    TURN_TEST2_OFF                                 28 
#define    TURN_TEST3_ON                                 29 
#define    TURN_TEST3_OFF                                 30 
#define    TURN_TEST4_ON                                 31 
#define    TURN_TEST4_OFF                                 32 
#define    TURN_TEST5_ON                                 33 
#define    TURN_TEST5_OFF                                 34 
#define    TURN_FIXSPOT_ON                             35 
#define    TURN_FIXSPOT_OFF                             36 
#define    START_FIXSPOT_DIM                             37 
#define    START_UP_LEVER                                 38 
#define    END_UP_LEVER                                 39 
#define    START_LEFT_LEVER                             40 
#define    END_LEFT_LEVER                                 41 
#define    START_RIGHT_LEVER                             42 
#define    END_RIGHT_LEVER                             43 
#define    START_EYE1                                     44 
#define    END_EYE1                                         45 
#define    START_EYE2                                    46 
#define    END_EYE2                                        47 
#define    TURN_TEST6_ON                                48 
#define    TURN_TEST6_OFF                                49 
#define    TURN_TEST7_ON                                50 
#define    TURN_TEST7_OFF                                51 
#define    TURN_TEST8_ON                                52 
#define    TURN_TEST8_OFF                                53 
#define    TURN_TEST9_ON                                54 
#define    TURN_TEST9_OFF                                55 
#define    START_SCROLL_BITMAP_TEST0                 56 
#define    END_SCROLL_BITMAP_TEST0                    57 
#define    START_SCROLL_BITMAP_TEST1                 58 
#define    END_SCROLL_BITMAP_TEST1                     59 
#define    START_SCROLL_BITMAP_TEST2                 60 
#define    END_SCROLL_BITMAP_TEST2                     61 
#define    START_SCROLL_BITMAP_TEST3                 62 
#define    END_SCROLL_BITMAP_TEST3                     63 
#define    START_SCROLL_BITMAP_TEST4                 64 
#define    END_SCROLL_BITMAP_TEST4                     65 
#define    START_SCROLL_BITMAP_TEST5                 66 
#define    END_SCROLL_BITMAP_TEST5                     67 
#define    START_SCROLL_BITMAP_TEST6                 68 
#define    END_SCROLL_BITMAP_TEST6                     69 
#define    START_SCROLL_BITMAP_TEST7                 70 
#define    END_SCROLL_BITMAP_TEST7                     71 
#define    START_SCROLL_BITMAP_TEST8                 72 
#define    END_SCROLL_BITMAP_TEST8                     73 
#define    START_SCROLL_BITMAP_TEST9                 74 
#define    END_SCROLL_BITMAP_TEST9                     75 
#define    START_SCROLL_SCREEN_TEST0                 76 
#define    END_SCROLL_SCREEN_TEST0                     77 
#define    START_SCROLL_SCREEN_TEST1                 78 
#define    END_SCROLL_SCREEN_TEST1                     79 
#define    START_SCROLL_SCREEN_TEST2                 80 
#define    END_SCROLL_SCREEN_TEST2                     81 
#define    START_SCROLL_SCREEN_TEST3                 82 
#define    END_SCROLL_SCREEN_TEST3                     83 
#define    START_SCROLL_SCREEN_TEST4                 84 
#define    END_SCROLL_SCREEN_TEST4                     85 
#define    START_SCROLL_SCREEN_TEST5                 86 
#define    END_SCROLL_SCREEN_TEST5                     87 
#define    START_SCROLL_SCREEN_TEST6                 88 
#define    END_SCROLL_SCREEN_TEST6                     89 
#define    START_SCROLL_SCREEN_TEST7                 90 
#define    END_SCROLL_SCREEN_TEST7                     91 
#define    START_SCROLL_SCREEN_TEST8                 92 
#define    END_SCROLL_SCREEN_TEST8                     93 
#define    START_SCROLL_SCREEN_TEST9                 94 
#define    END_SCROLL_SCREEN_TEST9                     95 
#define    REWARD_GIVEN                                 96 
#define    START_EXTRA_LEVER                             97 
#define    END_EXTRA_LEVER                             98 
#define    BAR_EXTRA                                     99
#define    START_EYE_DATA                                100
#define    END_EYE_DATA                                101
#define    NONDISPLAYABLE_ENCODES    // Don't display encode values listed here
#define    SPIKE3                                        102
#define    SPIKE4                                        103
#define    SPIKE5                                        104
#define    SPIKE6                                        105
#define    SPIKE7                                        106
#define    SPIKE8                                        107
#define    SPIKE9                                        108
#define    SPIKE10                                        109
#define    SPIKE11                                        110
#define    SPIKE12                                        111
#define    SPIKE13                                        112
#define    SPIKE14                                        113
#define    SPIKE15                                        114
#define    SPIKE16                                        115
#define    TOUCHED_SCREEN                                116

