function mm = minmax(x)
% minmax - return minimum and maximum 
%	 MINMAX(X) = [MIN(X); MAX(X)]

mm = [min(x); max(x)];