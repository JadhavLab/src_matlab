%sj_findoutbound_alldays_Ada('F:\Ripple_Dist\Justin\',[1],[1:8],'DM4',1)
%sj_findinbound_alldays_Ada('F:\Ripple_Dist\Justin\',[1],[1:8],'DM4',1)
%js_findoverallperformance_singleDay('F:\Ripple_Dist\Justin\',[1],[1:8],'DM4',1);
%linpos{1}

%linpos
%linpos{1}{2}
%linpos{1}{2}.trajwells
load('DM4pos01.mat')
load('DM4linpos01.mat')
cntsess=8;
type=[]; rewd=[]; for i=1:8, [type{i},rewd{i}] = sj_trackperformance([1 i],[], linpos, pos, [2 1 3]); end
rewd
rewd{1}
ntrials=[]; rew_col=[]; for i=1:8; rew_col=[rew_col;rewd{i}]; ntrials(i)=length(rew_col); end
%ntrials_axis = cumsum(ntrials);
ntrials_axis = ntrials;
71+24;
[pc, lt] = getestprobcorrect(rew_col, 0.5, 0);

outbound_curve_witherr = pc(2:end,:);
    outbound_curve = pc(2:end,1);
    outbound_lowerr = pc(2:end,2);
    outbound_uperr = pc(2:end,3);
    outbound_learningtrial = lt;
    
    figure; hold on; clr='b'; clr2=clr;
    %% Get time (day) axis
    t = 1:size(pc,1)-1;
    %%% Plot data %%
    plot(t', pc(2:end,1),clr, 'Linewidth', 2);
    taxis = t';
    data=[pc(2:end,2),pc(2:end,3)];
    jbfill(taxis',pc(2:end,3)',pc(2:end,2)',clr,clr,0.2,0.2);
    plot(t', pc(2:end,2),[clr2,'--'], 'Linewidth', 2);
    plot(t', pc(2:end,3),[clr2,'--'], 'Linewidth', 2);
    
    %xax = [0, ntraj_axis_sess];
    xax = [0, ntrials_axis];
    for i=1:cntsess
        %line([ntrajs_perday(i) ntrajs_perday(i)], [0 1],'g--');
        plot((ntrials_axis(i))*ones(size([0:0.05:1.05])) , [0:0.05:1.05], 'k--');
        currxax = xax(i):xax(i+1);
        % Get triangle on top of shading area
        midpt = floor(length(currxax)/2);
        len1 = midpt;
        spacing1  = (1.05-1)./(len1-1);
        yup1 = 1:spacing1:1.05; % Corrsponds to currax(1):midpt
        len2 = length(currxax) - len1; 
        spacing2 = (1-1.05)./(len2-1);
        yup2 = 1.05:spacing2:1; % Corrsponds to currax(midpt+1):end
        yup = [yup1,yup2];
        
        if mod(i,2)==1 % odd days
            jbfill(currxax,yup, 0*ones(size(currxax)),'k','k',0.2,0.2);
        else % even days
            jbfill(currxax,yup, 0*ones(size(currxax)),'k','k',0.2,0.2);
        end
    end
    
    % Trials in a session
    %shifts = [0, ntrajs_persess(1:end-1)];
    %cumshifts = cumsum(shifts);
    %ntraj_axis_sess = ntrajs_persess+cumshifts;    
    %learning_sess = min(find(ntraj_axis_sess>lt));
    
      %% Backgnd Prob
    %line([1 t(end)], [background_prob  background_prob ], 'k');
    plot(t, 0.5*ones(size(t)),'k', 'Linewidth', 2);
    
    %% Title
    title([prefix ' - All Trials'],'FontSize',titlefont,'Fontweight','normal');
    
    %% Axes Names
    xlabel('Trial Number','FontSize',xfont,'Fontweight','normal')
    ylabel('Probability of a Correct Response','FontSize',yfont,'Fontweight','normal')
    
    