

% From Gideons tagCA2CellsForShantanu
% Instead of changing "area", can add a tag field called CA2 tag, eg. sj_addFStag6.m
% For now, just change it for Ndl Tet 14


    animstrs={'Pkr','Rtl','Ndl','Brg'};
    ca2cells=[];
    peranimdata={};
    %basedir='/opt/data15/gideon/';
    basedir = '/data25/sjadhav/HPExpt/Rtl_direct/';
    daydir = basedir;
    animdirect = basedir;
    fileprefix = 'Rtl';
    
    
    for anim=2
        animstr=animstrs{anim};
        
        
        %daydir = [basedir animstr 'Data/'];
        
        load([basedir '/' animstr 'cellinfo.mat'])
        animprefix=lower(animstr);
        
      
        toplot=0;
        allTPinterval=[];
        allTPampratio=[];
        allinds=[];
        firstday=1;
       
        for day1=firstday:length(cellinfo)
            if ~isempty(cellinfo{day1})
                if day1<10
                    daystr=['0' num2str(day1)];
                else
                    daystr=num2str(day1);
                end
                
                for tet1=1:21
                    if tet1<10
                        tetstr=['0' num2str(tet1)];
                    else
                        tetstr=num2str(tet1);
                    end
                    
                
                    for epoch1=1:length(cellinfo{day1})
                        if tet1<=length(cellinfo{day1}{epoch1})&~isempty(cellinfo{day1}{epoch1}{tet1})
                            
                            for cell1=1:length(cellinfo{day1}{epoch1}{tet1})
                                if ~isempty(cellinfo{day1}{epoch1}{tet1}{cell1})
                                  
                                    day1, epoch1,tet1,cell1
                                    
                                    if isfield(cellinfo{day1}{epoch1}{tet1}{cell1},'area')
                                        if anim==2&tet1==14&strcmp(cellinfo{day1}{epoch1}{tet1}{cell1}.area,'CA1')
                                            ca2cells=[ca2cells ;anim day1 tet1 cell1];
                                            cellinfo{day1}{epoch1}{tet1}{cell1}.area='CA2';
                                        end
                                    end
                                    
                                    
                                end
                            end
                        end
                    end
                    
                end
            end
        end
        
        ca2c=unique(ca2cells,'rows');
        ca2c
        disp('watch the list to make sure it makes sense before saving');
        keyboard
        fname = [animdirect, fileprefix,'cellinfo']
        save([animdirect, fileprefix,'cellinfo'], 'cellinfo');
        %save([basedir animstr '/' animstr 'cellinfo.mat'],'cellinfo')
        
   
    end


