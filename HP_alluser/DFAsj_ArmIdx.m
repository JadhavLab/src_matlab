
function out = DFAsj_ArmIdx(index, excludetimes, linfields, varargin)

% Calc ArmIdx (similar to Overlap/PathEq) for trajectories from SINGLE CELL

% Dont output linear traj and maps - too big

% Takes in index for pairs of cells and calculates overlap
% No excludetimes in here - linfields has no time information



%function overlap = calcoverlap(trajdata1,trajdata2, varargin)
% overlap = calcoverlap(trajdata1,trajdata2, varargin)
% Trajdata had all trajectories unlike calcoverlap2 which expects only one trajectory as input
%
% trajdata1{traj}
% Compute overlap for all traj in trajdata
% If more than half bins are exclude, overlap will not be .computed
%
% options
%   Normalize, 0 or 1, default 0
%       if 0 calculates overlap, if 1 calculates normalized overlap
%   thresh, minimim peak to compute overlap
%   MinBins, value between 0 and 1, default 0.5
%       proportion of bins that must be defined to calculate overlap,
%       otherwise overlap = NaN

normalize = 0;
thresh = 3;
minbins = 0.5;
trajmapping = [1 1 2 2]; % Traj 1 = left in and out/ traj 2 = right in and out

if ~isempty(excludetimes)
    excludetimes = [];
end

for option = 1:2:length(varargin)-1
    if isstr(varargin{option})
        switch(varargin{option})
            case 'normalize'
                normalize = varargin{option+1};
            case 'thresh'
                thresh = varargin{option+1};
            case 'minbins'
                minbins = varargin{option+1};
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end
    end
end


try
    trajdata = linfields{index(1)}{index(2)}{index(3)}{index(4)}; %lf1 = all traj
catch
    index; keyboard;
end

% In case you want to use   a peak condition: at least 1 trajectory above thresh=3Hz
peaks = [];
for traj = 1:length(trajdata)
    if length(trajdata)>=traj
        if ~isempty(trajdata{traj})
            peaks = [peaks (max(trajdata{traj}(:,5)))];
            
        end
    end
end
[peak, trajpeak] = max(peaks);

ArmIdx = NaN;
if (peak >= thresh) % Compute only is peak exceeds thresh = 3 Hz
    
    % SKIP CONDITIONS - Much simpler than overlap or patheq
    
    bothtrajdata = {}; bothtrajdata = cell(2,1);
    for traj = 1:4
        
        ArmIdx = nan;
        
        if length(trajdata)==4
            if ~isempty(trajdata{traj})
                if isempty(bothtrajdata{trajmapping(traj)})
                    bothtrajdata{trajmapping(traj)} = trajdata{traj}(:,5);
                else
                    %bothtrajdata{trajmapping(traj)} = bothtrajdata{trajmapping(traj)} + trajdata{traj}(:,5);
                    bothtrajdata{trajmapping(traj)} = nansum([bothtrajdata{trajmapping(traj)}, trajdata{traj}(:,5)],2);
                end
                
                % One way: get area under curve for all probabilities>Choice Point
                % OR Get the one with higher mean probability - entire trajectory.
                area1 = nansum(bothtrajdata{1}); % Traj1 is Left arm, our view
                area2 = nansum(bothtrajdata{2}); % Traj2 is Right arm, our view
                ArmIdx = (area1-area2)./(area1+area2);
                
            else
                %index, keyboard;
                ArmIdx = nan;
            end
        end
    end 
end

% if isnan(ArmIdx),
%     index, keyboard;
% end

out.index = index;
out.ArmIdx = ArmIdx;
out.peak = peak;
out.trajdata = trajdata;

