% CA1 Dirn Idx like PFC Dirn Idx

clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells

% Parameters
norm_overlap = 0;  % For place-field overlap
thresh_peakrate = 3; % For place-field overlap

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';


% PFC all
% ------------------------------
val=0;savefile = [savedir 'HP_PFC_DirnIdx_noFS_X6']; area = 'PFC'; clr = 'c';
%val=1;savefile = [savedir 'HP_PFC_DirnIdx_X6']; area = 'PFC'; clr = 'c';

savefig1=0;

% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    %switch val
    %   case 1
    animals = {'HPa','HPb','HPc','Ndl','Rtl','Brg'}; %{'HPa','HPb','HPc','Ndl'};
    %end
    
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    %dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    %runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    runepochfilter = 'isequal($type, ''run'') && ~isequal($environment, ''lin'')';
    
    % %Cell filter
    % %-----------
    
    switch val
        case 0
            cellfilter = '( strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($FStag, ''n'') )';    
        case 1
            cellfilter = '( strcmp($area, ''PFC'') && ($numspikes > 100))';    
    end
    
    % Iterator
    % --------
    iterator = 'singlecellanal';  %
    
    % Filter creation
    % ----------------
    patheqf = createfilter('animal',animals,'epochs',runepochfilter,'cells',...
        cellfilter,'iterator', iterator);
    
    
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    
    patheqf = setfilterfunction(patheqf, 'DFAsj_DirnIdx', {'linfields'},...
        'normalize',norm_overlap,'thresh',thresh_peakrate,'minbins',0.5);
    
    
    % Run analysis
    % ------------
    patheqf = runfilter(patheqf);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile,'-v7.3');
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------


% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
switch val
    case 0
        gatherdatafile = [savedir 'HP_PFC_DirnIdx_gather_noFS_X6']; area = 'PFC';
    case 1
        gatherdatafile = [savedir 'HP_PFC_DirnIdx_gather_X6']; area = 'PFC';
end


if gatherdata
    
    % Parameters if any
    % -----------------
    
    % -------------------------------------------------------------
    
    cnt=0;
    allanimindex=[]; anim_index=[];
    allpatheq1=[]; all_lf=[];
    
    for an = 1:length(patheqf)
        for i=1:length(patheqf(an).output{1})
            cnt=cnt+1;
            anim_index{an}(cnt,:) = patheqf(an).output{1}(i).index;
            % Only indexes
            animindex=[an patheqf(an).output{1}(i).index]; % Put animal index in front
            allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet1 Cell1  Index
            
            all_Dirnidx(cnt) =  patheqf(an).output{1}(i).DirnIdx;
            all_lf{cnt} = patheqf(an).output{1}(i).trajdata;
            %alltrajdata1{cnt} = patheqf(an).output{1}(i).trajdata1;
            %             alltrajdata1{an}{i} = xrun(an).output{1}(i).trajdata1;
            %             alltrajdata2{an}{i} = xrun(an).output{1}(i).trajdata2;
            %             allmapdata1{an}{i} = xrun(an).output{1}(i).mapdata1;
            %             allmapdata2{an}{i} = xrun(an).output{1}(i).mapdata2;
        end
        
    end
    
    
    
    % CONSOLIDATE ACROSS EPOCHS FOR SAME CELL PAIRS
    % ----------------------------------------------------------
    patheq_output = struct; patheq_output_idx=[];
    dummyindex=allanimindex;  % all anim-day-epoch-tet1-cell1 indices
    cntcells=0;
    
    for i=1:size(allanimindex)
        animdaytetcell=allanimindex(i,[1 2 4 5]);
        ind=[];
        while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
            dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one if it exists
        end
        
        % Gather everything for the current cell across epochs
        allDirnidx_epcomb = []; all_lf_epcomb=[];  ep=0;      
        for r=ind
            ep=ep+1;
            allDirnidx_epcomb = [allDirnidx_epcomb; all_Dirnidx(r)];
            all_lf_epcomb{ep} = all_lf{r};
        end
    
        if ~isempty(allDirnidx_epcomb)
            cntcells = cntcells + 1;
            
            % Save both in struct, and variable format
            all_DI_PFCidxs(cntcells,:) = animdaytetcell;
            all_DI_PFCDirnIdx(cntcells) = nanmean(allDirnidx_epcomb);
            
            all_DI_PFC(cntcells).index = animdaytetcell;
            all_DI_PFC(cntcells).DirnIdx_eps = allDirnidx_epcomb;
            all_DI_PFC(cntcells).lf = all_lf_epcomb;
            all_DI_PFC(cntcells).DirnIdx = nanmean(allDirnidx_epcomb);
    
        end
    end
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data







% figdir = '/data25/sjadhav/HPExpt/Figures/';
% set(0,'defaultaxesfontsize',20);
% tfont = 20; xfont = 20; yfont = 20;
% 
% % Plotting for indiv cells
% % --------------------------
% if figopt1
%     keyboard;
% end
% 
% 
% % ------------------
% % Population Figures
% % ------------------
% 
% forppr = 0;
% % If yes, everything set to redimscreen_figforppr1
% % If not, everything set to redimscreen_figforppt1
% 
% figdir = '/data25/sjadhav/HPExpt/Figures/Jan2014/';
% summdir = figdir;
% set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
% 
% if forppr==1
%     set(0,'defaultaxesfontsize',16);
%     tfont = 18; xfont = 16; yfont = 16;
% else
%     set(0,'defaultaxesfontsize',40);
%     tfont = 40; xfont = 40; yfont = 40;
% end
% 
% 
% % Plotting Distribution
% % ---------------------
% 
% 
% figure; hold on;
% hist(allpatheq);
% 
% 
% 
% 
