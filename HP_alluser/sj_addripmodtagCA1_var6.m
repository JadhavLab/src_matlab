
function sj_addripmodtagCA1_var6(animno,animdirect,fileprefix)

% WRONG!!
% DOES NOT WORK LIKE THIS! HAS TO BE IN THE SAME FILE AS PFC, OR ELSE WILL OVERWRITE EVERYTHING
% Write a separate one for CA1, similar to PFC
% INSTEAD, USE THE NEW FUNC sj_addripmodtag_PFCandCA1_var6

% Add the var2 variable as ripplemodtag2: RE_INITALIZE ONLY var, or else this will erase everything previous
% ONLY Runs, and only PFC implemented right now. Comment Sleeps out for now

% sj_addripmodtagCA1_var6(1,'/data25/sjadhav/HPExpt/HPa_direct/','HPa');
% sj_addripmodtagCA1_var6(2,'/data25/sjadhav/HPExpt/HPb_direct/','HPb');
% sj_addripmodtagCA1_var6(3,'/data25/sjadhav/HPExpt/HPc_direct/','HPc');
% sj_addripmodtagCA1_var6(4,'/data25/sjadhav/HPExpt/Ndl_direct/','Ndl');
% sj_addripmodtagCA1_var6(5,'/data25/sjadhav/HPExpt/Rtl_direct/','Rtl');
% sj_addripmodtagCA1_var6(6,'/data25/sjadhav/HPExpt/Brg_direct/','Brg');

% OR
%sj_addripmodtagCA1_var6(1,'/opt15/data15/gideon/HPa_direct/','HPa');

% Add tag to identify ripple modulated and non-modulated cells. Whether a cell is modulated or not is obtained from *ripplemod_gather files.
% For run epochs, currently, the significance is calculated by combining across run epochs in a day
% For sleep epochs, pre-sleep is kept separate from post-sleep epochs.

% Update - I need to propogate the sleepripmodtag to other epochs as well for theta cov analysis.
% So make separate fields "postsleepripmodtag", "presleepripmodtag" to transfer to all epochs.
% Also do the same for "runripmodtag". Might be useful later.
% Note: Only doing this for PFC cells right now. "ripmodtag" for CA1 not that useful currently.

% Note that you are not using the "ripplemod" structure files, which contains the ripple-aligned responses for each epoch separately,
% but you are not using statistics from that file.

% Start with a clean slate. Set ripplemodtag2 for all to Undefined = "u"
load([animdirect, fileprefix,'cellinfo']);
o = cellfetch(cellinfo,'numspikes');
targetcells = o.index;
for i = 1:size(targetcells,1)
    % ripmodtag will mark signficance for current epoch
    cellinfo{targetcells(i,1)}{targetcells(i,2)}{targetcells(i,3)}{targetcells(i,4)}.ripmodtag3 = 'u';
    % The following tags will be propagated across epochs - Doing this only for PFC cells?
    cellinfo{targetcells(i,1)}{targetcells(i,2)}{targetcells(i,3)}{targetcells(i,4)}.runripmodtag3 = 'u';
    cellinfo{targetcells(i,1)}{targetcells(i,2)}{targetcells(i,3)}{targetcells(i,4)}.postsleepripmodtag2 = 'u';
    cellinfo{targetcells(i,1)}{targetcells(i,2)}{targetcells(i,3)}{targetcells(i,4)}.presleepripmodtag2 = 'u';
    cellinfo{targetcells(i,1)}{targetcells(i,2)}{targetcells(i,3)}{targetcells(i,4)}.ripmodtype3 = 'u';
    cellinfo{targetcells(i,1)}{targetcells(i,2)}{targetcells(i,3)}{targetcells(i,4)}.runripmodtype3 = 'u';
    
end


procdatadir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
%procdatadir = '/opt/data15/gideon/HP_ProcessedData/';
%procdatadir = 'mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';

%1) Do CA1 next

%1a) RUN DATA FOR CA1
load([procdatadir, 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6']);
animidxs = find(allripplemod_idx(:,1)==animno);
daytetcell_list = allripplemod_idx(animidxs,[2 3 4]);

% Instead of looping through daytetcell_list, loop through cellinfo and find matches
%Nadal:
mindays = 1; maxdays = length(cellinfo);
if animno==4,
    mindays = 8; maxdays = 17;
end

for d=mindays:maxdays
    
    if animno<4 % Shantanu's animals
        if d==1,
            runepochs = [4 6]; % only Wtracks for now.
            allepochs = [1:7]; % To propagate runripmodtag
        else
            runepochs = [2 4];
            allepochs = [1:5]; % To propagate runripmodtag
        end
    else
        d % day starts from 7 for Ndl, so get from task
        taskfile = sprintf('%s/%stask%02d.mat', animdirect, fileprefix, d);
        load(taskfile);
        allepochs = 1:length(task{d});
        tmpepochs = length(task{d});
        runepochs = [];
        for tmpep = 1:tmpepochs
            if strcmp(task{d}{tmpep}.type,'run') % If a run epoch
                runepochs = [runepochs; tmpep];
            end
        end
    end
    
    
    for e=1:length(runepochs)
        ep = runepochs(e);
        for tet=1:length(cellinfo{d}{ep})
            if ~isempty(cellinfo{d}{ep}{tet}) % if cells for the tet
                for c=1:length(cellinfo{d}{ep}{tet})
                    
                    currdaytetcell=[d tet c];
                    match = rowfind(currdaytetcell, daytetcell_list);
                    
                    % There should be only 1 match. If no match, its undefined for that cell.
                    if match~=0
                        getdataidx = animidxs(match);
                        
                        if allripplemod(getdataidx).rasterShufP2 < 0.05
                            if strcmp(allripplemod(getdataidx).type,'exc')
                                cellinfo{d}{ep}{tet}{c}.ripmodtag3='y';
                                cellinfo{d}{ep}{tet}{c}.ripmodtype3='exc';
                                
                                for tmpe=allepochs
                                    tmpe;
                                    cellinfo{d}{tmpe}{tet}{c}.runripmodtag3='y';
                                    cellinfo{d}{tmpe}{tet}{c}.runripmodtype3='exc';
                                    
                                end
                            else
                                cellinfo{d}{ep}{tet}{c}.ripmodtag3='y';
                                cellinfo{d}{ep}{tet}{c}.ripmodtype3='inh';
                                
                                for tmpe=allepochs
                                    tmpe;
                                    cellinfo{d}{tmpe}{tet}{c}.runripmodtag3='y';
                                    cellinfo{d}{tmpe}{tet}{c}.runripmodtype3='inh';
                                    
                                end
                                
                            end
                            
                        else
                            cellinfo{d}{ep}{tet}{c}.ripmodtag3='n';
                            for tmpe=allepochs
                                tmpe;
                                cellinfo{d}{tmpe}{tet}{c}.runripmodtag3='n';
                            end
                            
                        end
                    end
                    
                end % end c
            end % end c
        end % end tet
    end % end ep
end %end d









i=1;
% Save updated cellinfo file
save([animdirect, fileprefix,'cellinfo'], 'cellinfo');



