

% Limit only to output of W-track in pre-sleep glm. So will end up doing only Wtrack for all

% USe output anf presleep glm and run glm.
% Use run rip glm to predict presleep rip resp; and vice versa
% Use run theta glm to predict presleep rip resp.
% Will need index matching

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
val = 1;
switch val
    case 1
        savefile1 = [savedir 'HP_ripmod_glmfit_presleep_gather6allripmod_X8_Wonly'];
        savefile2 = [savedir 'HP_ripmod_glmfit_theta_gather6allripmod_X8']; area = 'PFCripmod'; clr = 'k'; % PFC
    case 2
        savefile1 = [savedir 'HP_ripmod_glmfit_presleep_gather6ripexc_X8_Wonly'];
        savefile2 = [savedir 'HP_ripmod_glmfit_theta_gather6ripexc_X8']; area = 'PFCexc'; clr = 'r'; % PFCexc
    case 3
        savefile1 = [savedir 'HP_ripmod_glmfit_presleep_gather6ripinh_X8_Wonly'];
        savefile2 = [savedir 'HP_ripmod_glmfit_theta_gather6ripinh_X8']; area = 'PFCinh'; clr = 'b'; % PFCinh
    case 4
        savefile1 = [savedir 'HP_ripmod_glmfit_presleep_gather6unripmod_X8_Wonly'];
        savefile2 = [savedir 'HP_ripmod_glmfit_theta_gather6unripmod_X8']; area = 'PFCunmod'; clr = 'b'; % PFCinh
end

load(savefile1);
load(savefile2);

% All PFC cells that go into glm
nPFCrun_all = length(XYinds); %102
nPFC_presl_all = length(XYinds_presl); %93

% PFC cells for which glm converged
nPFCrun = length(allErrReal); %77. Main Fig^E is with these cells
nPFC_presl = length(allErrReal_presl); %73


% GLM prediction
% --------------
allErrReal_runTOpresl=[];
allErrShuf_runTOpresl=[];
allPs_runTOpresl=[];
allErrReal_thetaTOpresl=[];
allErrShuf_thetaTOpresl=[];
allPs_thetaTOpresl=[];

savenPFCells_runTOpresl=[];
savenCA1cells_runTOpresl=[];
savenRips_runTOpresl=[];

counter_runTOpresl=0;
numTrain=1000;


% GO over all the run cells and find matches in presleep with length of predictors as well
% These run cells already have a glmfit which has converged, but stats and indices havent been saved,
% so redo and check whether run glm converges
for ii=1:nPFCrun_all
    curr_PFCrun = uniqueIndices(ii,:);
    ind1 = find(ismember(uniqueIndices_presl,curr_PFCrun,'rows'));
    
    if ~isempty(ind1) % PFC match exists
        curr_XYinds = XYinds{ii};
        curr_XYinds_presl = XYinds_presl{ind1};
        % get data
        currXY=XYmats2{ii};
        currX=currXY(:,1:end-1);
        currY=currXY(:,end);
        
        currXYTheta=XYmats2Theta{ii};
        currXTheta=currXYTheta(:,1:end-1);
        currYTheta=currXYTheta(:,end);
        
        currXY_presl=XYmats2_presl{ind1};
        currX_presl=currXY_presl(:,1:end-1);
        currY_presl=currXY_presl(:,end);
        
        % Only proceed if no. of CA1 predictors are the same in run and pre-sleep
        % if size(curr_XYinds,1)==size(curr_XYinds_presl,1)
        % relax this condition? as long as pre-sleep has equal or greater number of CA1 cells
        % indexes have to match for glmval. Or else, you have to find subset of indices that match, and only keep those
        if size(curr_XYinds,1)<=size(curr_XYinds_presl,1) %have to index match to pick correct Xindices in presl
            
            if size(curr_XYinds,1)<size(curr_XYinds_presl,1) % only update if sizes dont match
                
                % find PFC cell in index list and skip it. Should be last one
                indPFC = find(ismember(curr_XYinds,curr_PFCrun,'rows'));
                curr_XYinds_loop = curr_XYinds; curr_XYinds_loop(indPFC,:)=[];
                
                %for jj = 1:size(curr_XYinds,1)-1  % skip that last PFC cell
                keepidx=[];
                for jj = 1:size(curr_XYinds_loop,1)  % skip that PFC cell
                    currCA1idx = curr_XYinds_loop(jj,:);
                    ind_match = find(ismember(curr_XYinds_presl,currCA1idx,'rows'));
                    if~isempty(ind_match)
                        keepidx=[keepidx;ind_match];
                    end
                end
                % update currX_presl
                try
                    currX_presl = currX_presl(:,keepidx);
                catch
                    keyboard;
                end
            end
            
            % Only proceed if sizes match now. They will not match if there is a run CA1 cell that is not present in sleep
            if size(currX,2)==size(currX_presl,2)
                % Check if glm converges for rip run. Only proceed if it does.
                lastwarn('');
                [btrall, ~, statsall] = glmfit(currX,currY,'poisson');
                
                if isempty(lastwarn)
                    counter_runTOpresl = counter_runTOpresl + 1;
                    disp(['ii=',num2str(ii), '; ind1=',num2str(ind1) '; counter_runTOpresl=',num2str(counter_runTOpresl)]);
                    
                    nPFCcells=size(currY,2); savenPFCells_runTOpresl(ii) = nPFCcells;
                    nCA1cells=size(currX,2); savenCA1cells_runTOpresl(ii) = nCA1cells;
                    savenRips_runTOpresl(ii) = size(currX,1);
                    
                    % a) predicting PFC presl ripple firing from training on run ripples firing
                    %[btrall, ~, statsall] = glmfit(currX,currY,'poisson');
                    try
                        yfit_runTOpresl = glmval(btrall, currX_presl,'log');
                    catch
                        keyboard;
                    end
                    errReal_runTOpresl=nanmean(abs(yfit_runTOpresl-currY_presl));
                    allErrShufTmp=[];
                    for pp=1:numTrain
                        errShuf=nanmean(abs(yfit_runTOpresl-currY_presl(randperm(length(currY_presl)))));
                        allErrShufTmp=[allErrShufTmp errShuf];
                    end
                    currP=nanmean(errReal_runTOpresl>allErrShufTmp);
                    allPs_runTOpresl=[allPs_runTOpresl currP];
                    allErrReal_runTOpresl=[allErrReal_runTOpresl errReal_runTOpresl];
                    allErrShuf_runTOpresl=[allErrShuf_runTOpresl nanmean(allErrShufTmp)];
                    
                    % b) predicting PFC presl ripple firing from training on theta
                    [btrallT, ~, statsallT] = glmfit(currXTheta,currYTheta,'poisson');
                    yfit_thetaTOpresl = glmval(btrallT, currX_presl,'log');
                    errReal_thetaTOpresl=nanmean(abs(yfit_thetaTOpresl-currY_presl));
                    allErrShufTmp=[];
                    for pp=1:numTrain
                        errShuf=nanmean(abs(yfit_thetaTOpresl-currY_presl(randperm(length(currY_presl)))));
                        allErrShufTmp=[allErrShufTmp errShuf];
                    end
                    currP=nanmean(errReal_thetaTOpresl>allErrShufTmp);
                    allPs_thetaTOpresl=[allPs_thetaTOpresl currP];
                    allErrReal_thetaTOpresl=[allErrReal_thetaTOpresl errReal_thetaTOpresl];
                    allErrShuf_thetaTOpresl=[allErrShuf_thetaTOpresl nanmean(allErrShufTmp)];
                    
                    
                    % Do this in a separate loop over pre-sleep cells
                    % ----------------------------------------------
                    %             % predicting PFC run ripple firing from training on presl ripples firing
                    %             [btrall_presl, ~, statsall_presl] = glmfit(currX_presl,currY_presl,'poisson');
                    %             yfit_preslTOrun = glmval(btrall, currX,'log');
                    %             errReal_preslTOrun=nanmean(abs(yfit_preslTOrun-currY));
                    %             allErrShufTmp=[];
                    %             for pp=1:numTrain
                    %                 errShuf=nanmean(abs(yfit_preslTOrun-currY(randperm(length(currY)))));
                    %                 allErrShufTmp=[allErrShufTmp errShuf];
                    %             end
                    %             currP=nanmean(errReal_preslTOrun>allErrShufTmp);
                    %             allPs_preslTOrun=[allPs_preslTOrun currP];
                    %             allErrReal_preslTOrun=[allErrReal_preslTOrun errReal_preslTOrun];
                    %             allErrShuf_preslTOrun=[allErrShuf_preslTOrun nanmean(allErrShufTmp)];
                    
                end % lastwarn
            end %compare size of currrX in presl and run - inside loop
        end %indices compare presl and run
        
    end % if ind1 exists
end


counter_preslTOrun=0;
allErrReal_preslTOrun=[];
allErrShuf_preslTOrun=[];
allPs_preslTOrun=[];
allErrReal_preslTOrunTheta=[];
allErrShuf_preslTOrunTheta=[];
allPs_preslTOrunTheta=[];
savenPFCells_preslTOrun=[];
savenCA1cells_preslTOrun=[];
savenRips_preslTOrun=[];


% Re-do for predicting run rip from presl and run theta from presl
% ----------------------------------------------------------------
for ii=1:nPFC_presl_all
    curr_PFCpresl = uniqueIndices_presl(ii,:);
    ind1 = find(ismember(uniqueIndices,curr_PFCpresl,'rows'));
    
    if ~isempty(ind1) % PFC match exists
        curr_XYinds_presl = XYinds_presl{ii};
        curr_XYinds = XYinds{ind1};
        
        % get data
        currXY_presl=XYmats2_presl{ii};
        currX_presl=currXY_presl(:,1:end-1);
        currY_presl=currXY_presl(:,end);
        
        currXY=XYmats2{ind1};
        currX=currXY(:,1:end-1);
        currY=currXY(:,end);
        
        currXYTheta=XYmats2Theta{ind1};
        currXTheta=currXYTheta(:,1:end-1);
        currYTheta=currXYTheta(:,end);        
        
        % Only proceed if no. of CA1 predictors are the same in run and pre-sleep
        % if size(curr_XYinds,1)==size(curr_XYinds_presl,1)
        % relax this condition? as long as run has equal or greater number of CA1 cells
        % indexes have to match for glmval. Or else, you have to find subset of indices that match, and only keep those
        if size(curr_XYinds_presl,1)<=size(curr_XYinds,1) %have to index match to pick correct Xindices in presl
            
            if size(curr_XYinds_presl,1)<size(curr_XYinds,1) % only update if sizes dont match
                
                % find PFC cell in index list and skip it. Should be last one
                indPFC = find(ismember(curr_XYinds_presl,curr_PFCpresl,'rows'));
                curr_XYinds_presl_loop = curr_XYinds_presl; curr_XYinds_presl_loop(indPFC,:)=[];
                indPFC = find(ismember(curr_XYinds,curr_PFCpresl,'rows'));
                curr_XYinds_loop = curr_XYinds; curr_XYinds_loop(indPFC,:)=[];
                
                %for jj = 1:size(curr_XYinds_presl,1)-1  % skip that last PFC cell
                keepidx=[];
                for jj = 1:size(curr_XYinds_presl_loop,1)  % skip that PFC cell
                    currCA1idx = curr_XYinds_presl_loop(jj,:);
                    ind_match = find(ismember(curr_XYinds_loop,currCA1idx,'rows'));
                    if~isempty(ind_match)
                        keepidx=[keepidx;ind_match];
                    end
                end
                % update currX_presl
                try
                    currX = currX(:,keepidx);
                    currXTheta = currXTheta(:,keepidx);
                catch
                    keyboard;
                end
            end
            
            % Only proceed if sizes match now. They will not match if there is a run CA1 cell that is not present in sleep
            if size(currX,2)==size(currX_presl,2)
  
            % Check if glm converges for presl run. Only proceed if it does.
            lastwarn('');
            [btrall_presl, ~, statsall_presl] = glmfit(currX_presl,currY_presl,'poisson');
            
            if isempty(lastwarn)
                counter_preslTOrun = counter_preslTOrun + 1;
                disp(['ii=',num2str(ii), '; ind1=',num2str(ind1) '; counter_preslTOrun=',num2str(counter_preslTOrun)]);
                
                nPFCcells=size(currY,2); savenPFCells_preslTOrun(ii) = nPFCcells;
                nCA1cells=size(currX,2); savenCA1cells_preslTOrun(ii) = nCA1cells;
                savenRips_preslTOrun(ii) = size(currX,1);
                
                % a) predicting PFC run ripple firing from training on presl ripples firing
                yfit_preslTOrun = glmval(btrall_presl, currX,'log');
                errReal_preslTOrun=nanmean(abs(yfit_preslTOrun-currY));
                allErrShufTmp=[];
                for pp=1:numTrain
                    errShuf=nanmean(abs(yfit_preslTOrun-currY(randperm(length(currY)))));
                    allErrShufTmp=[allErrShufTmp errShuf];
                end
                currP=nanmean(errReal_preslTOrun>allErrShufTmp);
                allPs_preslTOrun=[allPs_preslTOrun currP];
                allErrReal_preslTOrun=[allErrReal_preslTOrun errReal_preslTOrun];
                allErrShuf_preslTOrun=[allErrShuf_preslTOrun nanmean(allErrShufTmp)];
                
                % b) predicting PFC Theta ripple firing from training on presl ripples firing
                yfit_preslTOrunTheta = glmval(btrall_presl, currXTheta,'log');
                errReal_preslTOrunTheta=nanmean(abs(yfit_preslTOrunTheta-currYTheta));
                allErrShufTmp=[];
                for pp=1:numTrain
                    errShuf=nanmean(abs(yfit_preslTOrunTheta-currYTheta(randperm(length(currYTheta)))));
                    allErrShufTmp=[allErrShufTmp errShuf];
                end
                currP=nanmean(errReal_preslTOrunTheta>allErrShufTmp);
                allPs_preslTOrunTheta=[allPs_preslTOrun currP];
                allErrReal_preslTOrunTheta=[allErrReal_preslTOrunTheta errReal_preslTOrunTheta];
                allErrShuf_preslTOrunTheta=[allErrShuf_preslTOrunTheta nanmean(allErrShufTmp)];
                
            end % lastwarn
                        end %compare size of currrX in presl and run - inside loop

        end %indices compare presl and run
        
    end % if ind1 exists
end



%keyboard;


%%
% ------------
% PLOTTING, ETC
% ------------
figdir = '/data25/sjadhav/HPExpt/Figures/PreSleep/PreSleepGLM/Wtronly/'; 
forppr=0; set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
if forppr==1
    set(0,'defaultaxesfontsize',20);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end

fractionSigPredictableK=mean(allPsK<0.05);
fractionSigPredictableTheta=mean(allPs_theta<0.05);
fractionSigPredictableK_presl=mean(allPsK_presl<0.05);
%fractionSigPredictableTheta=mean(allPs_theta<0.05)

% These alternatives yield almost identical result:
% 1.
%errDecrease=(1-allErrReal./allErrShuf);
%errDecrease_theta=(1-allErrReal_theta./allErrShuf_theta);
% 2.

% For RUN file, limit data to W-track only. pre-sleep is already W-tr only
% ------------------------------------------------------------------------

startidxs = uniqueIndices(find(converged1),:); % Get 77 out of 102 PFC cells for which runrip glm converged
Wtr_an = find(startidxs(:,1)<=3); % 38 of these are from W-track
allErrShuf = allErrShuf(Wtr_an); allErrShuf_theta = allErrShuf_theta(Wtr_an);
allErrReal = allErrReal(Wtr_an); allErrReal_theta = allErrReal_theta(Wtr_an);


errDecrease=(allErrShuf./allErrReal-1);
errDecrease_theta=(allErrShuf_theta./allErrReal_theta-1);
errDecrease_presl=(allErrShuf_presl./allErrReal_presl-1);
errDecrease_runTOpresl=(allErrShuf_runTOpresl./allErrReal_runTOpresl-1);
errDecrease_thetaTOpresl=(allErrShuf_thetaTOpresl./allErrReal_thetaTOpresl-1);
errDecrease_preslTOrun=(allErrShuf_preslTOrun./allErrReal_preslTOrun-1);
errDecrease_preslTOrunTheta=(allErrShuf_preslTOrunTheta./allErrReal_preslTOrunTheta-1);

%errDecrease_theta=(allErrShuf_theta./allErrReal_theta-1);


% SKIP SIG RATE BY CELLS
% ---------------------
% % sig rate SWRs
% sigratebycells_presl=[];
%
% sigrate1to5=mean(allPsK_presl(allnumcells<=5)<0.05); numsig1to5=sum(allPsK_presl(allnumcells<=5)<0.05);numcells1to5=length(allPsK_presl(allnumcells<=5)<0.05);
% sigrate6to10=mean(allPsK_presl(allnumcells>5&allnumcells<=10)<0.05);numsig6to10=sum(allPsK_presl(allnumcells>5&allnumcells<=10)<0.05);numcells6to10=length(allPsK_presl(allnumcells>5&allnumcells<=10)<0.05);
% sigrate11to30=mean(allPsK_presl(allnumcells>10&allnumcells<=30)<0.05);numsig11to30=sum(allPsK_presl(allnumcells>10&allnumcells<=30)<0.05);numcells11to30=length(allPsK_presl(allnumcells>10&allnumcells<=30)<0.05);
%
% sigratebycells_presl=[sigrate1to5 sigrate6to10 sigrate11to30];
% numsigbycells_presl = [numsig1to5 numsig6to10 numsig11to30];
% numcellsbycells_presl = [numcells1to5 numcells6to10 numcells11to30]
% Ncells_presl = sum(numcellsbycells_presl) % should be same as counter
%
% % % sig rate theta
% % sigratebycellstheta=[];
% %
% % sigrate1to5T=mean(allPs_theta(allnumcells<=5)<0.05); numsig1to5T=sum(allPs_theta(allnumcells<=5)<0.05);numcells1to5T=length(allPs_theta(allnumcells<=5)<0.05);
% % sigrate6to10T=mean(allPs_theta(allnumcells>5&allnumcells<=10)<0.05);numsig6to10T=sum(allPs_theta(allnumcells>5&allnumcells<=10)<0.05);numcells6to10T=length(allPs_theta(allnumcells>5&allnumcells<=10)<0.05);
% % sigrate11to30T=mean(allPs_theta(allnumcells>10&allnumcells<=30)<0.05);numsig11to30T=sum(allPs_theta(allnumcells>10&allnumcells<=30)<0.05);numcells11to30T=length(allPs_theta(allnumcells>10&allnumcells<=30)<0.05);
% %
% % sigratebycellstheta=[sigrate1to5T sigrate6to10T sigrate11to30T];
%
%
% % both
% %figure;bar([sigratebycells;sigratebycellstheta]')
% figure;bar([sigratebycells_presl]')
% text(0.7,1.1,[num2str(numsig1to5) '/' num2str(numcells1to5)])
% text(1.7,1.1,[num2str(numsig6to10) '/' num2str(numcells6to10)])
% text(2.7,1.1,[num2str(numsig11to30) '/' num2str(numcells11to30)])
%
% % text(1.1,1.1,[num2str(numsig1to5T) '/' num2str(numcells1to5T)])
% % text(2.1,1.1,[num2str(numsig6to10T) '/' num2str(numcells6to10T)])
% % text(3.1,1.1,[num2str(numsig11to30T) '/' num2str(numcells11to30T)])
% ylim([0 1.2])
% set(gca,'XTickLabel',{'1-5','6-10','11-30'})

%--------- error rate
% % err rate SWRs
% errratebycells_presl=[];
%
% errrate1to5=mean(errDecrease_presl(allnumcells<=5)); errnumcells1to5=sum(allnumcells<=5);
% errrate6to10=mean(errDecrease_presl(allnumcells>5&allnumcells<=10));errnumcells6to10=sum(allnumcells>5&allnumcells<=10);
% errrate11to30=mean(errDecrease_presl(allnumcells>10&allnumcells<=30));errnumcells11to30=sum(allnumcells>10&allnumcells<=30);
% errratebycells_presl=[errrate1to5 errrate6to10 errrate11to30];
%
% % SEM:
% errrate1to5SEM=std(errDecrease_presl(allnumcells<=5))/sqrt(sum(allnumcells<=5));
% errrate6to10SEM=std(errDecrease_presl(allnumcells>5&allnumcells<=10))/sqrt(sum(allnumcells>5&allnumcells<=10));
% errrate11to30SEM=std(errDecrease_presl(allnumcells>10&allnumcells<=30))/sqrt(sum(allnumcells>10&allnumcells<=30));
% errratebycellsSEM_presl=[errrate1to5SEM errrate6to10SEM errrate11to30SEM];
%
%
% % % err rate theta
% % errratebycellstheta=[];
% %
% % errrate1to5=mean(errDecrease_theta(allnumcells<=5));
% % errrate6to10=mean(errDecrease_theta(allnumcells>5&allnumcells<=10));
% % errrate11to30=mean(errDecrease_theta(allnumcells>10&allnumcells<=30));
% % errratebycellstheta=[errrate1to5 errrate6to10 errrate11to30];
% %
% % % SEM:
% % errrate1to5SEM=std(errDecrease_theta(allnumcells<=5))/sqrt(sum(allnumcells<=5));
% % errrate6to10SEM=std(errDecrease_theta(allnumcells>5&allnumcells<=10))/sqrt(sum(allnumcells>5&allnumcells<=10));
% % errrate11to30SEM=std(errDecrease_theta(allnumcells>10&allnumcells<=30))/sqrt(sum(allnumcells>10&allnumcells<=30));
% % errratebycellsthetaSEM=[errrate1to5SEM errrate6to10SEM errrate11to30SEM];
%
% % both
% figure;
% bar(1:2:6,errratebycells_presl,0.25,'linewidth',2);
% hold on;
% errorbar(1:2:6,errratebycells_presl,errratebycellsSEM_presl,'.k','linewidth',2)
% %bar(1.6:2:6,errratebycellstheta,0.25,'y','linewidth',2);
% %errorbar(1.6:2:6,errratebycellstheta,errratebycellsthetaSEM,'.k','linewidth',2)
%
%
% text(0.7,.085,[num2str(errnumcells1to5)])
% text(1.7,.085,[num2str(errnumcells6to10)])
% text(2.7,.085,[num2str(errnumcells11to30)])
%
% % text(1.1,.085,[num2str(errnumcells1to5)])
% % text(2.1,.085,[num2str(errnumcells6to10)])
% % text(3.1,.085,[num2str(errnumcells11to30)])
% ylim([0 0.09])
% set(gca,'XTickLabel',{'1-5','6-10','11-30'})

% Plot directly comparison of Error rates, without division between categories of cells 1to5, 6to10, etc
% ----------------------------------------------------------------------------------------------------------

errrate_rip = [nanmean(errDecrease) nanmean(errDecrease_runTOpresl) nanmean(errDecrease_preslTOrun) nanmean(errDecrease_presl)]
errrate_ripSEM = [nansem(errDecrease) nansem(errDecrease_runTOpresl) nansem(errDecrease_preslTOrun) nansem(errDecrease_presl)]

errrate_theta = [nanmean(errDecrease_theta) nanmean(errDecrease_thetaTOpresl) nanmean(errDecrease_preslTOrunTheta)]
errrate_thetaSEM = [nansem(errDecrease_theta) nansem(errDecrease_thetaTOpresl) nansem(errDecrease_preslTOrunTheta)]

switch val
    case 1
        area = 'PFCripmod-Wonly'
    case 2
        area = 'PFCexc-Wonly'
    case 3
        area = 'PFCinh-Wonly'    
    case 4
        area = 'PFCunmod-Wonly'
end
        
        
savefileN = [savefile1 'Wgetgather']
save(savefileN);


figure; hold on;
bar([1:2:8],errrate_rip*100,0.5,'g','linewidth',2);
errorbar([1:2:8],errrate_rip*100,errrate_ripSEM*100,'.k','linewidth',2)
bar([12:2:16],errrate_theta*100,0.5,'y','linewidth',2);
errorbar([12:2:16],errrate_theta*100,errrate_thetaSEM*100,'.k','linewidth',2)
%bar(1.6:2:6,errratebycellstheta,0.25,'y','linewidth',2);
%errorbar(1.6:2:6,errratebycellstheta,errratebycellsthetaSEM,'.k','linewidth',2)
title(['Run vs Pre-sleep cross-prediction: ', area],'FontSize',24,'FontWeight','normal');
ylabel('% improvement over shuffled','FontSize',24,'FontWeight','normal');
xlim([0 18]); ylim([-1 4])
rotateXLabels( gca(), 45 );
text(0.8,3.2,num2str(length(errDecrease)),'FontSize',24)
text(2.8,3.2,num2str(length(errDecrease_runTOpresl)),'FontSize',24)
text(4.8,3.2,num2str(length(errDecrease_preslTOrun)),'FontSize',24)
text(6.8,3.2,num2str(length(errDecrease_presl)),'FontSize',24)


figfile = [figdir,'Run_vs_PreSleep_GLMcross_',area]
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
end



% Statistics
% -----------
% rip prediction
alldata = [errDecrease errDecrease_runTOpresl errDecrease_preslTOrun errDecrease_presl];
numcellgroup=[1*(ones(size(errDecrease))) 2*(ones(size(errDecrease_runTOpresl))) 3*(ones(size(errDecrease_preslTOrun))) 4*(ones(size(errDecrease_presl)))];
p=anovan(alldata,{numcellgroup})
p_rip1 = ranksum(errDecrease,errDecrease_runTOpresl) %2.8e-7
p_rip2 = ranksum(errDecrease,errDecrease_preslTOrun) %5.1e-5
p_rip3 = ranksum(errDecrease,errDecrease_presl) %3.8e-4
p_rip4 = ranksum(errDecrease_preslTOrun,errDecrease_presl)% 0.039
p_rip5 = ranksum(errDecrease_runTOpresl,errDecrease_presl)% 0.0038

% theta prediction
alldata = [errDecrease_theta errDecrease_thetaTOpresl errDecrease_preslTOrunTheta];
numcellgroup=[1*(ones(size(errDecrease_theta))) 2*(ones(size(errDecrease_thetaTOpresl))) 3*(ones(size(errDecrease_preslTOrunTheta)))];
p=anovan(alldata,{numcellgroup})
[p_theta1] = ranksum(errDecrease_theta, errDecrease_thetaTOpresl)
[p_theta2] = ranksum(errDecrease_theta, errDecrease_preslTOrunTheta)

[savefile1,'_compare']
save([savefile1,'_compare'])
keyboard;




% -------------------------------------------------------------------------------
% %% Statistics between groups
% %savedir = '/opt/data15/gideon/HP_ProcessedData/';
% savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
%
% gatherdatafileAllripmod = [savedir 'HP_ripmod_glmfit_theta_gather6allripmod_X8'];
% gatherdatafileExc = [savedir 'HP_ripmod_glmfit_theta_gather6ripexc_X8'];
% gatherdatafileInh = [savedir 'HP_ripmod_glmfit_theta_gather6ripinh_X8'];
% gatherdatafileUnmod = [savedir 'HP_ripmod_glmfit_theta_gather6unripmod_X8'];
%
%
% load(gatherdatafileAllripmod);
% errDecreaseAllripmod=(allErrShuf./allErrReal-1);
% errDecrease_thetaAllripmod=(allErrShuf_theta./allErrReal_theta-1);
% numcellgroupAllripmod=zeros(1,length(allnumcells));
% numcellgroupAllripmod(allnumcells<=5)=1; Ncells_mod1to5 = length(find(allnumcells<=5));
% numcellgroupAllripmod(allnumcells>5&allnumcells<=10)=2; Ncells_mod6to10 = length(find(allnumcells>5&allnumcells<=10));
% numcellgroupAllripmod(allnumcells>10&allnumcells<=30)=3; Ncells_mod11to30 = length(find(allnumcells>10&allnumcells<=30));
%
%
%
% load(gatherdatafileExc);
% errDecreaseExc=(allErrShuf./allErrReal-1);
% errDecrease_thetaExc=(allErrShuf_theta./allErrReal_theta-1);
% numcellgroupExc=zeros(1,length(allnumcells));
% numcellgroupExc(allnumcells<=5)=1; Ncells_exc1to5 = length(find(allnumcells<=5));
% numcellgroupExc(allnumcells>5&allnumcells<=10)=2; Ncells_exc6to10 = length(find(allnumcells>5&allnumcells<=10));
% numcellgroupExc(allnumcells>10&allnumcells<=30)=3; Ncells_exc11to30 = length(find(allnumcells>10&allnumcells<=30));
%
% load(gatherdatafileInh);
% errDecreaseInh=(allErrShuf./allErrReal-1);
% errDecrease_thetaInh=(allErrShuf_theta./allErrReal_theta-1);
% numcellgroupInh=zeros(1,length(allnumcells));
% numcellgroupInh(allnumcells<=5)=1; Ncells_inh1to5 = length(find(allnumcells<=5));
% numcellgroupInh(allnumcells>5&allnumcells<=10)=2; Ncells_inh6to10 = length(find(allnumcells>5&allnumcells<=10));
% numcellgroupInh(allnumcells>10&allnumcells<=30)=3; Ncells_inh11to30 = length(find(allnumcells>10&allnumcells<=30));
%
% load(gatherdatafileUnmod);
% errDecreaseUnmod=(allErrShuf./allErrReal-1);
% errDecrease_thetaUnmod=(allErrShuf_theta./allErrReal_theta-1);
% numcellgroupUnmod=zeros(1,length(allnumcells));
% numcellgroupUnmod(allnumcells<=5)=1; Ncells_unmod1to5 = length(find(allnumcells<=5));
% numcellgroupUnmod(allnumcells>5&allnumcells<=10)=2; Ncells_unmod6to10 = length(find(allnumcells>5&allnumcells<=10));
% numcellgroupUnmod(allnumcells>10&allnumcells<=30)=3; Ncells_unmod11to30 = length(find(allnumcells>10&allnumcells<=30));
%
% %% testing effect of cell number
% alldata=[errDecreaseAllripmod errDecreaseUnmod];
% %modunmod=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecreaseUnmod))];
% numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
% p=anovan(alldata,{numcellgroup})
% %% testing effect of cell number on theta-trained data
% alldata=[errDecrease_thetaAllripmod errDecrease_thetaUnmod];
% numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
% p=anovan(alldata,{numcellgroup})
% %% comparing SWR-modulated and SWR-unmodulated
% alldata=[errDecreaseAllripmod errDecreaseUnmod];
% modunmod=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecreaseUnmod))];
% numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
% p=anovan(alldata,{modunmod,numcellgroup})
%
% %% comparing SWR-exc and SWR-inh
% alldata=[errDecreaseExc errDecreaseInh];
% excinh=[ones(1,length(errDecreaseExc)) 2*ones(1,length(errDecreaseInh))];
% numcellgroup=[numcellgroupExc numcellgroupInh ];
% p=anovan(alldata,{excinh,numcellgroup})
%
% %% comparing theta trained/SWR trained for swr-modulated cells
% alldata=[errDecreaseAllripmod errDecrease_thetaAllripmod errDecreaseUnmod errDecrease_thetaUnmod];
% modunmod=[ones(1,length(errDecreaseAllripmod)+length(errDecrease_thetaAllripmod)) 2*ones(1,length(errDecreaseUnmod)+length(errDecrease_thetaUnmod))];
% riptheta=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecrease_thetaAllripmod)) ones(1,length(errDecreaseUnmod)) 2*ones(1,length(errDecrease_thetaUnmod)) ];
% numcellgroup=[numcellgroupAllripmod numcellgroupAllripmod numcellgroupUnmod numcellgroupUnmod];
% p=anovan(alldata,{modunmod,riptheta,numcellgroup})
%
% %% comparing theta trained/SWR trained for swr-modulated cells, with mod/unmod distinction
% alldata=[errDecreaseAllripmod errDecrease_thetaAllripmod errDecreaseUnmod errDecrease_thetaUnmod];
% modunmod=[ones(1,length(errDecreaseAllripmod)+length(errDecrease_thetaAllripmod)) 2*ones(1,length(errDecreaseUnmod)+length(errDecrease_thetaUnmod))];
% riptheta=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecrease_thetaAllripmod)) ones(1,length(errDecreaseUnmod)) 2*ones(1,length(errDecrease_thetaUnmod)) ];
% numcellgroup=[numcellgroupAllripmod numcellgroupAllripmod numcellgroupUnmod numcellgroupUnmod];
% p=anovan(alldata,{modunmod,riptheta,numcellgroup})
%
% %% comparing theta trained/SWR trained for swr-excited and swr-inhibited cells
% alldata=[errDecreaseExc errDecrease_thetaExc errDecreaseInh errDecrease_thetaInh];
% excinh=[ones(1,length(errDecreaseExc)+length(errDecrease_thetaExc)) 2*ones(1,length(errDecreaseInh)+length(errDecrease_thetaInh))];
% riptheta=[ones(1,length(errDecreaseExc)) 2*ones(1,length(errDecrease_thetaExc)) ones(1,length(errDecreaseInh)) 2*ones(1,length(errDecrease_thetaInh)) ];
% numcellgroup=[numcellgroupExc numcellgroupExc numcellgroupInh numcellgroupInh];
% p=anovan(alldata,{excinh,riptheta,numcellgroup})
%
% [p tbl stats] = anovan(alldata,{excinh,riptheta,numcellgroup},'model','interaction','varnames',{'excinh','riptheta','numcellgroup'})
% results = multcompare(stats,'Dimension',[1])
%
% %%
% alldata=[errDecreaseExc errDecrease_thetaExc errDecreaseInh errDecrease_thetaInh errDecreaseUnmod errDecrease_thetaUnmod];
% excinhunmod=[ones(1,length(errDecreaseExc)+length(errDecrease_thetaExc)) 2*ones(1,length(errDecreaseInh)+length(errDecrease_thetaInh)) 3*ones(1,length(errDecreaseUnmod)+length(errDecrease_thetaUnmod))];
% riptheta=[ones(1,length(errDecreaseExc)) 2*ones(1,length(errDecrease_thetaExc)) ones(1,length(errDecreaseInh)) 2*ones(1,length(errDecrease_thetaInh)) ones(1,length(errDecreaseUnmod)) 2*ones(1,length(errDecrease_thetaUnmod))];
% numcellgroup=[numcellgroupExc numcellgroupExc numcellgroupInh numcellgroupInh numcellgroupUnmod numcellgroupUnmod];
% p=anovan(alldata,{excinhunmod,riptheta,numcellgroup})
% [p tbl stats] = anovan(alldata,{excinhunmod,riptheta,numcellgroup},'model','interaction','varnames',{'excinhunmod','riptheta','numcellgroup'})
% results = multcompare(stats,'Dimension',[1])
%
%
%