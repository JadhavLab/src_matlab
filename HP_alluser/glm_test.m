
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
%val=0; gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6allripmod_0'];
val=1; gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6allripmod'];

load(gatherdatafile);

%Shantanu - how many animals have how many cells
whichan=[];
for i=1:size(XYinds,2)
    whichan(i) = XYinds{i}(1);
end
[length(find(whichan==1)) length(find(whichan==2)) length(find(whichan==3)) length(find(whichan==4)) length(find(whichan==5))]

allErrRealThetaTheta2=[];
allErrShufThetaTheta2=[];
allPsThetaTheta=[];
allErrReal_theta=[];
allErrShuf_theta=[];
allErrRealThetaTheta=[];
allErrShufThetaTheta=[];
allErrReal=[];
allErrShuf=[];
allPs_theta=[];
allPs=[];
allPsK=[];
nsig=[];
fracsig=[];
nsigT=[];
fracsigT=[];
corrPlast=[];
counter=0;
allRealPlast=[];
allShufPlast=[];
figdir = '/data15/gideon/Figs/';
plotGraphs=1;
converged1=[];
converged1theta=[];
allnumrips=[];
allnumcells=[];

savenPFCcells = [];
savenCA1cells = [];
savenRips = [];

for ii=1:size(XYmats2,2)
    ii
    currXY=XYmats2{ii};
    currX=currXY(:,1:end-1);
    currY=currXY(:,end);
    
    currXYTheta=XYmats2Theta{ii};
    currXTheta=currXYTheta(:,1:end-1);
    currYTheta=currXYTheta(:,end);
    
    nPFCcells=size(currY,2); savenPFCcells(ii) = nPFCcells;
    nCA1cells=size(currX,2); savenCA1cells(ii) = nCA1cells;
    savenRips(ii) = size(currX,1);
    
% %             if plotGraphs
% %                 [rr pp]=corrcoef([currX currY]);
% %                 regionsLabels=[repmat('CA1',nCA1cells,1);repmat('PFC',nPFCcells,1)];
% %                 % plotting only PFC-CA1 edges (removing CA1-CA1 and PFC-PFC)
% %                 for i=1:length(rr),for j=1:length(rr), if i<=nCA1cells&j<=nCA1cells,rr(i,j)=NaN;end,if i>nCA1cells&j>nCA1cells,rr(i,j)=NaN;end;end;end
% %                 plotNiceCorrGraph(rr,pp,regionsLabels)
% %                 animStr=num2str(allglmidxsrip2(counter,1));
% %     
% %                 dayStr=num2str(allglmidxsrip2(counter,2));
% %                 epochStr=num2str(allglmidxsrip2(counter,3));
% %                 title(['Rip- corrs, anim=' animStr ' day=' dayStr ' ep=' epochStr])
% %                % saveas(gcf,[figdir 'graph' animStr dayStr epochStr 'B'],'jpg')
% %                 keyboard
% %                 close all
% %             end
    
%             lastwarn('');
%     
%             [btrall, ~, statsall] = glmfit(currX,currY,'poisson');
%             if isempty(lastwarn)
%                 converged1=[converged1 1];
%             else
%                 converged1=[converged1 0];
%     
%             end
%             % continue only if converged
%             if isempty(lastwarn)
%                 currsig = find(statsall.p(2:end) < 0.05);
%                 nsig = [nsig length(currsig)];
%                 fracsig = [fracsig length(currsig)/nCA1cells];
%     
%     
%                 % predicting PFC ripple firing from training on ripples
%     
%                 numRips=size(currX,1);
%                 allnumrips=[allnumrips numRips];
%                 numCells=size(currX,2);
%                 allnumcells=[allnumcells numCells];
%     
%                 allErrReal1=[];
%                 allErrShuf1=[];
%                 numTrain=1000;
%     
%                 for iii=1:numTrain
%                     ripidxs=randperm(numRips);
%                     dataPercentForTrain=0.9;
%                     Ntrain=ripidxs(1:round(numRips*dataPercentForTrain));
%                     Ntest=ripidxs(round(numRips*dataPercentForTrain)+1:numRips);
%                     lastwarn('');
%                     [btr, ~, statstr] = glmfit(currX(Ntrain,:),currY(Ntrain),'poisson');
%                     % continue only if converged
%                     if isempty(lastwarn)
%                         yfit = glmval(btr, currX(Ntest,:),'log',statstr,0.95);
%                         Ntestshufd=Ntest(randperm(length(Ntest)));
%                         errReal=nanmean(abs(yfit-currY(Ntest)));
%                         errShuf=nanmean(abs(yfit-currY(Ntestshufd)));
%     
%                         allErrReal1=[allErrReal1 errReal];
%                         allErrShuf1=[allErrShuf1 errShuf];
%                     end
%                 end
%     
%                 [r1,kp1]=ttest2(allErrReal1,allErrShuf1,0.05,'left');
%     
%                 allErrReal=[allErrReal nanmean(allErrReal1)];
%                 allErrShuf=[allErrShuf nanmean(allErrShuf1)];
%     
%                 allPsK=[allPsK kp1];
%     
%                 [btrallT, ~, statsallT] = glmfit(currXTheta,currYTheta,'poisson');
%                 currsigT = find(statsallT.p(2:end) < 0.05);
%                 nsigT = [nsigT length(currsigT)];
%                 fracsigT = [fracsigT length(currsigT)/nCA1cells];
%     
%                 % predicting PFC ripple firing from training on theta
%                 yfit_theta = glmval(btrallT, currX,'log');
%                 errReal_theta=nanmean(abs(yfit_theta-currY));
%                 allErrShufTmp=[];
%                 for pp=1:numTrain
%                     errShuf=nanmean(abs(yfit_theta-currY(randperm(length(currY)))));
%                     allErrShufTmp=[allErrShufTmp errShuf];
%                 end
%                 currP=nanmean(errReal_theta>allErrShufTmp);
%                 allPs_theta=[allPs_theta currP];
%                 allErrReal_theta=[allErrReal_theta errReal_theta];
%                 allErrShuf_theta=[allErrShuf_theta nanmean(allErrShufTmp)];
%                 counter=counter+1;
%            end
end

if val==0
    savenCA1cells_0=savenCA1cells;
    savenPFCcells_0=savenPFCcells;
    savenRips_0 = savenRips;
    save_converged1_0 = converged1;
    save('glm_0','savenRips_0','savenCA1cells_0','savenPFCcells_0','save_converged1_0');
else
    savenCA1cells_1=savenCA1cells;
    savenPFCcells_1=savenPFCcells;
    savenRips_1 = savenRips;
    save_converged1_1 = converged1;
    save('glm_1','savenRips_1','savenCA1cells_1','savenPFCcells_1','save_converged1_1');
end


counter
keyboard;





