% ver6: note:
% 1. has all animals
% 2. removed requirement for nrip>1 because it's too harsh

% Ver 4: Sync with everyone

% Ver2, Dec 2013 - Implement Min. NSpike condition for PFC cells. See Line 47 and Line 240

% Ripple modulation of cells, especilally PFC cells. Time filter version of sj_HPexpt_ripalign_singlecell_getrip4.
% Will call DFAsj_getripalign.m
% Also see DFSsj_plotthetamod.m and DFSsj_HPexpt_xcorrmeasures2. Will gather data like these

clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells
%savedir = '/opt/data15/gideon/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
figdir =  '/data25/sjadhav/HPExpt/Figures/Ripple/'; saveg1=1;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
forppr=1;
if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',40);
    tfont = 40;
    xfont = 40;
    yfont = 40;
end

[y, m, d] = datevec(date);
val=6;savefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_X6_forripLFP']; area = 'PFC'; clr = 'b';
%val=7;savefile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_X6_forripLFP']; area = 'CA1'; clr = 'r';

savefig1=0;

% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


%If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    %animals = {'HPa','HPb','HPc','Nadal','Rosenthal','Borg'}
    %animals = {'Rtl'}
    animals = {'HPa','HPb','HPc','Ndl','Rtl'}
    
    %Filter creation
    %-----------------------------------------------------
    
    runepochfilter = 'isequal($type, ''run'')';
    
    % Cell filter
    % -----------
    switch val
        
        case 6
            cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; % PFC cells with spiking criterion
            %cellfilter = 'strcmp($area, ''PFC'')'; % This includes all, including silent cells
        case 7
            cellfilter = '(strcmp($area, ''CA1'')|| strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7) ';
    end
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    eegfilter = {'sj_geteegtet_first', 'ripple', 'tetfilter',riptetfilter}; % get first riptet, usualyy 1
    
    % Iterator
    % --------
    iterator = 'singlecelleeganal';
    
    % Filter creation
    % ----------------
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter,'eegtetrodes', eegfilter, 'iterator', iterator);
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    
    switch val
        
        % NOTE CHANGE HERE TO MINRIP
        case 6
            modf = setfilterfunction(modf,'DFAsj_getripalignspiking5_forRipLFP',{'spikes','ripples', 'tetinfo', 'pos','ripple'},'dospeed',1,'lowsp_thrs',4,'minrip',1); % Default stdev is 3
        case 7
            modf = setfilterfunction(modf,'DFAsj_getripalignspiking5_forRipLFP',{'spikes','ripples', 'tetinfo', 'pos','ripple'},'dospeed',1,'lowsp_thrs',4,'minrip',1); % Default stdev is 3
            
            
    end
    
    % Run analysis
    % ------------
    modf = runfilter(modf);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile, '-v7.3');
    end
    
else
    
    %x=1
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------



% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
[y, m, d] = datevec(date);
symmetricwindow=0;
switch val
    
    case 6
        if symmetricwindow
            gatherdatafile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6symmetricwindow'];
        else
            gatherdatafile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6_forripLFP'];
            
        end
        
    case 7
        if symmetricwindow
            
            gatherdatafile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6symmetricwindow'];
        else
            gatherdatafile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6_forripLFP'];
            
        end
        
end




if gatherdata
    
    % Parameters if any
    % -----------------
    
    % -------------------------------------------------------------
    
    cnt=0; % Count how many cells will be kept based on nspikes in output: >0
    allanimindex=[]; alldataraster=[]; alldatahist = []; all_Nspk=[]; allNrip=[];
    allripenv=[]; allripamp=[];allriptime=[];
    
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
            if (modf(an).output{1}(i).Nspikes > 0)
                cnt=cnt+1;
                anim_index{an}(cnt,:) = modf(an).output{1}(i).index;
                % Only indexes
                animindex=[an modf(an).output{1}(i).index]; % Put animal index in front
                allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
                
                % Rip
                all_Nspk(cnt) = modf(an).output{1}(i).Nspikes;
                allripenv{cnt} = modf(an).output{1}(i).ripenv;
                allripamp{cnt} = modf(an).output{1}(i).ripamp;
                allriptime{cnt} = modf(an).output{1}(i).rip_starttime;
                allNrip(cnt) = modf(an).output{1}(i).Nrip;
                
                %end
                if cnt==1
                    pret =  modf(an).output{1}(i).pret;
                    postt = modf(an).output{1}(i).postt;
                    preind =  modf(an).output{1}(i).preind;
                    postind = modf(an).output{1}(i).postind;
                end
            end
        end
        
    end
    
    % Consolidate single cells across epochs. Multiple methods: see also DFSsj_getcellinfo and DFSsj_xcorrmeasures2
    % ----------------------------------------------------------------------------
    
    allripplemodlfp = struct;
    
    
    % ---------
    dummyindex=allanimindex;  % all anim-day-epoch-tet-cell indices
    cntcells=0;
    for i=1:length(allripenv)
        animdaytetcell=allanimindex(i,[1 2 4 5]);
        ind=[];
        while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
            dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one
        end
        
        % Gather everything for the current cell across epochs
        currNspk=0; currNrip=0;
        currripenv=[]; currripamp=[];currriptime=[];
        
        for r=ind
            currNspk = currNspk + all_Nspk(r);
            currNrip = currNrip + allNrip(r);
            currripenv = [currripenv; allripenv{r}];
            currripamp = [currripamp; allripamp{r}];
            currriptime = [currriptime; allriptime{r}];
        end
        
        % Condition for Nspk. Version 1 had a min of 50 for entire window. Increase it to 100,
        % and can also add a condition for spikes in (resp+bck) window. Need to try a few values
        if (currNspk >= 50)
            %if ((currNspk_resp+currNspk_bck) >= 40)
            %if (currNspk >= 100) && ((currNspk_resp+currNspk_bck) >= 40)
            cntcells = cntcells + 1;
            % For Ndl-GIdeon;s animal, shift days
            %             if animdaytetcell(1)==4
            %                 animdaytetcell(2)=animdaytetcell(2)-7; % Day starts from no. 8
            %             end
            allripplemodlfp_idx(cntcells,:)=animdaytetcell;
            allripplemodlfp(cntcells).index=animdaytetcell;
            allripplemodlfp(cntcells).ripenv=currripenv;
            allripplemodlfp(cntcells).ripamp=currripamp;
            allripplemodlfp(cntcells).riptime=currriptime;
            allripplemodlfp(cntcells).Nspk=currNspk;
        end
    end
    
    % THE WINDOWS FOR RESPONSE AND BACK ARE SET IN
    % DFAsj_getripalignspiking5 AND ARE 0:200 AND -500:-100 RESPECTIVELY
    
    
    figopt1=0; saveg1=0;
    popln_ripenv = []; popln_ripenvnorm = [];
    
    Fs=1.5; % kHz    preind=450=300ms/ postind = 450=300ms
    preteeg = (preind/Fs);
    postteeg = (postind/Fs);
    binsize=1/Fs;
    taxiseeg = -preteeg:binsize:postteeg;
    
    
    % Calculations for ripenv
    % -----------------------------------------------------------
    for i=1:cntcells
        
        curridx = allripplemodlfp(i).index;
        curr_ripenv = allripplemodlfp(i).ripenv;
        currmax = nanmax(curr_ripenv,[],2);
        currmax = repmat(currmax,1,size(curr_ripenv,2));
        norm_ripenv = curr_ripenv./currmax;
        
        % Save normalized ripenv
        allripplemodlfp(i).ripenvnorm = norm_ripenv;
        
        % Get means of raw and normalized
        allripplemodlfp(i).ripenvmean = nanmean(allripplemodlfp(i).ripenv);
        allripplemodlfp(i).ripenverr = nansem(allripplemodlfp(i).ripenv);
        
        allripplemodlfp(i).ripenvnormmean = nanmean(allripplemodlfp(i).ripenvnorm);
        allripplemodlfp(i).ripenvnormerr = nansem(allripplemodlfp(i).ripenvnorm);
        
        % Save for population plot
        popln_ripenv = [popln_ripenv; curr_ripenv];
        popln_ripenvnorm = [popln_ripenvnorm; norm_ripenv];
        
        % ------------------------------
        % Plotting for individual cells
        % ------------------------------
        
        if (figopt1)
            
            anim = curridx(1); day = curridx(2); tet = curridx(3); cell = curridx(4);

            figure; hold on;
            subplot(2,1,1); hold on;
            plot(taxiseeg,allripplemodlfp(i).ripenvmean,'k-','Linewidth',2);
            plot(taxiseeg,allripplemodlfp(i).ripenvmean-allripplemodlfp(i).ripenverr,'k--');
            plot(taxiseeg,allripplemodlfp(i).ripenvmean+allripplemodlfp(i).ripenverr,'k--');
            
            title(sprintf('%d Day %d Tet %d Cell %d', anim, day, tet, cell),'FontSize',tfont,'Fontweight','normal');
            xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
            ylabel('SWR env','FontSize',yfont,'Fontweight','normal');
            
            
            subplot(2,1,2); hold on;
            plot(taxiseeg,allripplemodlfp(i).ripenvnormmean,'k-','Linewidth',2);
            plot(taxiseeg,allripplemodlfp(i).ripenvnormmean-allripplemodlfp(i).ripenvnormerr,'k--');
            plot(taxiseeg,allripplemodlfp(i).ripenvnormmean+allripplemodlfp(i).ripenvnormerr,'k--');
            xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
            ylabel('SWR Norm env','FontSize',yfont,'Fontweight','normal');
            
            keyboard;
        end
        
    end % for cnt cells
    
    
    % Save
    % -----
    if savegatherdata == 1
        disp('Saving gatherdatafile');
        save(gatherdatafile,'-v7.3');
        %return;
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data


% ------------------
% Population Figures
% ------------------

popmean = nanmean(popln_ripenv);
poperr = nansem(popln_ripenv);
popnormmean = nanmean(popln_ripenvnorm);
popnormerr = nansem(popln_ripenvnorm);

figure; hold on;
subplot(2,1,1); hold on;
plot(taxiseeg,popmean,'k-','Linewidth',2);
plot(taxiseeg,popmean - poperr,'k--');
plot(taxiseeg,popmean + poperr,'k--');
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Popln SWR env','FontSize',yfont,'Fontweight','normal');

xlim([-200 300])
line([0 0], [min(popmean)-1 max(popmean)+1])


subplot(2,1,2); hold on;
plot(taxiseeg,popnormmean ,'k-','Linewidth',2);
plot(taxiseeg,popnormmean - popnormerr,'k--');
plot(taxiseeg,popnormmean + popnormerr,'k--');
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Popln SWR Norm env','FontSize',yfont,'Fontweight','normal');

xlim([-200 300])
ylim([0.2 0.55])
line([0 0], [0.2 0.55])



figfile = [figdir,area,'_RipEnv_Popln']
keyboard;

if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end
















