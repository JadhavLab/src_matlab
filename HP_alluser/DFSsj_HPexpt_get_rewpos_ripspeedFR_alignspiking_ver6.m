
% Add high seed firing rate calculations in function, and compare FR modlns
% similar to rialign spiking. Use reward times extracted from trajinfo

clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells
%savedir = '/opt/data15/gideon/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';

highspeedthrs=10; % cm/sec for high speeed threshold
binsize = 0.2; % secs; bins for computing speed and firing rates

[y, m, d] = datevec(date);
%val=1;savefile = [savedir 'HP_rewposmod_ripspeedFR_PFCripmod_X6']; area = 'PFC'; clr = 'b';
%val=2;savefile = [savedir 'HP_rewposmod_ripspeedFR_PFCripEXC_X6']; area = 'PFC'; clr = 'b';
%val=3;savefile = [savedir 'HP_rewposmod_ripspeedFR_PFCripINH_X6']; area = 'PFC'; clr = 'b';
val=4;savefile = [savedir 'HP_rewposmod_ripspeedFR_PFCripUnmod_X6']; area = 'PFC'; clr = 'b';

%val=5;savefile = [savedir 'HP_rewposmod_ripspeedFR_CA1_X6']; area = 'PFC'; clr = 'b';
savefig1=0;

% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days

%If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Nadal','Rosenthal','Brg'};
    %Filter creation
    %-----------------------------------------------------
    
    %runepochfilter = 'isequal($type, ''run'')';
    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    
    % Cell filter
    % -----------
    switch val
%         case {5}
%             cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && strcmp($FStag, ''n'') && strcmp($ripmodtag3, ''y'') )';
         case {2}
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc'') && strcmp($FStag, ''n''))';
        case {3}
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh'') && strcmp($FStag, ''n''))';
        case {1}
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n''))';
        case {4}
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''n'')  && strcmp($FStag, ''n'') )';
    end
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % For high speed. Use absvel instead of linearvel
    timefilter_place_new = { {'DFTFsj_getvelpos', '(($absvel >= 0))'},...
        {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',3} };
    
    
    % Iterator
    % --------
    iterator = 'singlecellanal';
    
    % Filter creation
    % ----------------
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter, 'excludetime', timefilter_place_new, 'iterator', iterator);

%     modg = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
%         cellfilter,  'excludetime', timefilter_place_new, 'iterator', iterator);
    
    
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    
    switch val
        
        % NOTE CHANGE HERE TO MINRIP=1
        case {1,2,3,4,5}
            modf = setfilterfunction(modf,'DFAsj_get_rewpos_ripspeedFR_alignspiking',{'spikes', 'pos', 'linpos', 'trajinfo', 'cellinfo','ripplemod'},'thrsdist',10,'thrstime',1, 'highspeedthrs', highspeedthrs,'binsize',0.2);
            
%         case {1,2,3,4,5}
%             modf = setfilterfunction(modf,'DFAsj_get_rewpos_alignspiking',{'spikes', 'pos', 'linpos', 'trajinfo'},'thrsdist',10);
             
    end
    
    % Run analysis
    % ------------
    modf = runfilter(modf);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile, '-v7.3');
    end
    
else
    
    %x=1
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------





% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 0; savegatherdata = 1;
[y, m, d] = datevec(date);
symmetricwindow=0;
switch val
    case 1
        gatherdatafile = [savedir 'HP_rewposmod_ripspeedFR_PFCripmod_X6_gather']
    case 2
        gatherdatafile = [savedir 'HP_rewposmod_ripspeedFR_PFCripEXC_X6_gather']
    case 3
        gatherdatafile = [savedir 'HP_rewposmod_ripspeedFR_PFCripINH_X6_gather']
    case 4
        gatherdatafile = [savedir 'HP_rewposmod_ripspeedFR_PFCripUnmod_X6_gather']
end




if gatherdata
    
    % Parameters if any
    % -----------------
    
    % -------------------------------------------------------------
    
    cnt=0; % Count how many cells will be kept based on nspikes in output: >0
    allanimindex=[];
    
    % Rew
    alldataraster=[]; alldatahist = []; all_Nspk=[];
    alldataraster_rew=[]; alldatahist_rew = []; all_Nspk_rew=[];
    alldataraster_norew=[]; alldatahist_norew = []; all_Nspk_norew=[];
    
    % high speed and pre-rip fr
    allrs=[];allps=[];allinds=[];
    allpreripFR=[]; allhighspeedFR=[]; allFRmodln=[];
    allripFR=[]; allcellFR=[]; allcellinfoFR=[];
    allpreripFR2=[];allFRmodln2=[];
    allfits=[];
    allspeedspikingstruct={};
    
    % Rew Modlns
    allFRmodln_rew=[]; allFRmodln_norew=[]; allFRmodln_rewnorew=[];
    allFRmodln_rewhighspeed=[]; allFRmodln_rewprerip=[]; allFRmodln_rewrip=[];
    
    % Rew FR
    allrewFR=[]; allrewbckFR=[]; allnorewFR=[]; allnorewbckFR=[];
    
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
            if ~isempty(modf(an).output{1}(i).allrewtime) % && ~isnan(modf(an).output{1}(i).FRmodln_rew) && ~isnan(modf(an).output{1}(i).FRmodln_norew)
                %if (modf(an).output{1}(i).Nspikes > 0)
                cnt=cnt+1;
                anim_index{an}(cnt,:) = modf(an).output{1}(i).index;
                % Only indexes
                animindex=[an modf(an).output{1}(i).index]; % Put animal index in front
                allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
                
                % Data
                %alldataraster{cnt} = modf(an).output{1}(i).rip_spks_cell; % Only get raster and histogram response
                %alldatahist{cnt} = modf(an).output{1}(i).rip_spkshist_cell;
                %all_Nspk(cnt) = modf(an).output{1}(i).Nspikes;
                
                alldataraster_rew{cnt} = modf(an).output{1}(i).allrew_spks;
                alldatahist_rew{cnt} = modf(an).output{1}(i).allrew_spkshist;
                all_Nspk_rew(cnt) = modf(an).output{1}(i).Nspikes_rew;
                alldatatrialResps_rew{cnt} = modf(an).output{1}(i).trialResps_rew;  % trialResps: Summed Nspks/trial in respective window
                alldatatrialResps_bck_rew{cnt} = modf(an).output{1}(i).trialResps_bck_rew;
                
                alldataraster_norew{cnt} = modf(an).output{1}(i).allnorew_spks;
                alldatahist_norew{cnt} = modf(an).output{1}(i).allnorew_spkshist;
                all_Nspk_norew(cnt) = modf(an).output{1}(i).Nspikes_norew;
                alldatatrialResps_norew{cnt} = modf(an).output{1}(i).trialResps_norew;
                alldatatrialResps_bck_norew{cnt} = modf(an).output{1}(i).trialResps_bck_norew;
                
                % Nspikes summed across trials in response and bckgnd window
                %all_Nspk_resp(cnt) = sum(modf(an).output{1}(i).trialResps);
                %all_Nspk_bck(cnt) = sum(modf(an).output{1}(i).trialResps_bck);
                % Properties
                allcellfr(cnt) = modf(an).output{1}(i).cellfr;
                
                % FR modlns for reward
                % --------------------
                allFRmodln_rew(cnt) = modf(an).output{1}(i).FRmodln_rew;
                allFRmodln_norew(cnt) = modf(an).output{1}(i).FRmodln_norew;
                allFRmodln_rewnorew(cnt) = modf(an).output{1}(i).FRmodln_rewnorew;
                allFRmodln_rewhighspeed(cnt) = modf(an).output{1}(i).FRmodln_rewhighspeed;
                allFRmodln_rewprerip(cnt) = modf(an).output{1}(i).FRmodln_rewprerip;
                allFRmodln_rewrip(cnt) = modf(an).output{1}(i).FRmodln_rewrip;
                
                % Firing rates
                % ------------
                allrewFR(cnt) = modf(an).output{1}(i).rewFR;
                try
                    allrewbckFR(cnt) = modf(an).output{1}(i).rewbckFR;
                catch
                    keyboard;
                end
                allnorewFR(cnt) = modf(an).output{1}(i).norewFR;
                allnorewbckFR(cnt) = modf(an).output{1}(i).norewbckFR;
                
                % Highspeed and prerip FR
                % ----------------------
                allpreripFR = [allpreripFR modf(an).output{1}(i).preripFR];
                allhighspeedFR = [allhighspeedFR modf(an).output{1}(i).highspeedFR];
                allFRmodln = [allFRmodln modf(an).output{1}(i).FRmodln];
                allpreripFR2 = [allpreripFR2 modf(an).output{1}(i).preripFR2];
                allFRmodln2 = [allFRmodln2 modf(an).output{1}(i).FRmodln2];
                
                allripFR = [allripFR modf(an).output{1}(i).ripFR];
                allcellFR = [allripFR modf(an).output{1}(i).cellFR];
                allcellinfoFR = [allripFR modf(an).output{1}(i).cellinfoFR];
                
                allrs = [allrs modf(an).output{1}(i).FR_speed_corr_r];
                allps = [allps modf(an).output{1}(i).FR_speed_corr_p];
                
                prevsize=length(allspeedspikingstruct);
                allspeedspikingstruct(prevsize+1).ind=allanimindex;
                allspeedspikingstruct(prevsize+1).speeds=modf(an).output{1}(i).speeds;
                allspeedspikingstruct(prevsize+1).spiking=modf(an).output{1}(i).spiking;
                
                %end
                if cnt==1
                    pret =  modf(an).output{1}(i).pret;
                    postt = modf(an).output{1}(i).postt;
                    binsize = modf(an).output{1}(i).binsize;
                    rwin = modf(an).output{1}(i).rwin;
                    bckwin = modf(an).output{1}(i).bckwin;
                    %bins_resp  = modf(an).output{1}(i).bins_resp;
                    %bins_bck = modf(an).output{1}(i).bins_bck;
                    %timeaxis = modf(an).output{1}(i).timeaxis;
                end
            end
        end
        
    end
    
    % Consolidate single cells across epochs. Multiple methods: see also DFSsj_getcellinfo and DFSsj_xcorrmeasures2
    % ----------------------------------------------------------------------------
    
    allripplemod = struct;
    allrewmod = struct;
    
    % ---------
    dummyindex=allanimindex;  % all anim-day-epoch-tet-cell indices
    cntcells=0;
    for i=1:length(alldatahist_rew)
        animdaytetcell=allanimindex(i,[1 2 4 5]);
        ind=[];
        while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
            dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one
        end
        
        % Gather everything for the current cell across epochs
        currhist_rew=[]; currraster_rew=[]; currNspk_rew=0;  %currNspk_resp=0; currNspk_bck=0;
        currhist_norew=[]; currraster_norew=[]; currNspk_norew=0;
        currtrialResps_rew=[]; currtrialResps_bck_rew=[];
        currtrialResps_norew=[]; currtrialResps_bck_norew=[];
        
        % Modlns
        currFRmodln_rew=[]; currFRmodln_norew=[]; currFRmodln_rewnorew=[];
        currFRmodln_rewhighspeed=[]; currFRmodln_rewprerip=[]; currFRmodln_rewrip=[];
        currFRmodln=[];
        
        % FRs
        % Properties
        curr_cellfr=[]; curr_cellFR=[]; curr_cellinfoFR=[];
        currrewFR=[]; currrewbckFR=[]; currnorewFR=[]; currnorewbckFR=[];
        currpreripFR=[]; currhighspeedFR=[]; currripFR=[];
        
        for r=ind
            currNspk_rew = currNspk_rew + all_Nspk_rew(r);
            currNspk_norew = currNspk_norew + all_Nspk_norew(r);
            %currNspk_resp = currNspk_resp + all_Nspk_resp(r);
            %currNspk_bck = currNspk_bck + all_Nspk_bck(r);
            
            currhist_rew = [currhist_rew; alldatahist_rew{r}];
            currraster_rew = [currraster_rew, alldataraster_rew{r}];
            
            currhist_norew = [currhist_norew; alldatahist_norew{r}];
            currraster_norew = [currraster_norew, alldataraster_norew{r}];
            
            %try
                currtrialResps_rew = [currtrialResps_rew, alldatatrialResps_rew{r}];
            %catch
            %    keyboard;
            %  end
            currtrialResps_bck_rew = [currtrialResps_bck_rew, alldatatrialResps_bck_rew{r}];
            
            currtrialResps_norew = [currtrialResps_norew, alldatatrialResps_norew{r}];
            currtrialResps_bck_norew = [currtrialResps_bck_norew, alldatatrialResps_bck_rew{r}];
            
            
            % Modlns
            currFRmodln_rew = [currFRmodln_rew; allFRmodln_rew(r)];
            currFRmodln_norew = [currFRmodln_norew; allFRmodln_norew(r)];
            currFRmodln_rewnorew = [currFRmodln_rewnorew; allFRmodln_rewnorew(r)];
            currFRmodln_rewhighspeed = [currFRmodln_rewhighspeed; allFRmodln_rewhighspeed(r)];
            currFRmodln_rewprerip = [currFRmodln_rewprerip; allFRmodln_rewprerip(r)];
            currFRmodln_rewrip = [currFRmodln_rewrip; allFRmodln_rewrip(r)];
            currFRmodln = [currFRmodln; allFRmodln(r)]; % highspeed vs prerip
            
            % FRs
            %cell
            curr_cellfr = [curr_cellfr; allcellfr(r)];
            curr_cellFR = [curr_cellFR; allcellFR(r)];
            curr_cellinfoFR = [curr_cellinfoFR; allcellinfoFR(r)];
            %rew
            currrewFR = [currrewFR; allrewFR(r)];
            currrewbckFR = [currrewbckFR; allrewbckFR(r)];
            currnorewFR = [currnorewFR; allnorewFR(r)];
            currnorewbckFR = [currnorewbckFR; allnorewbckFR(r)];
            % rip-speed
            currripFR = [currripFR; allripFR(r)];
            currhighspeedFR = [currhighspeedFR; allhighspeedFR(r)];
            currpreripFR = [currpreripFR; allpreripFR(r)];
            
        end
 
        
        if (currNspk_rew >= 10)
            %if ((currNspk_resp+currNspk_bck) >= 40)
            %if (currNspk >= 100) && ((currNspk_resp+currNspk_bck) >= 40)
            cntcells = cntcells + 1;
            % For Ndl-GIdeon;s animal, shift days
            %             if animdaytetcell(1)==4
            %                 animdaytetcell(2)=animdaytetcell(2)-7; % Day starts from no. 8
            %             end
            allrewmod_idx(cntcells,:)=animdaytetcell;
            allrewmod(cntcells).index=animdaytetcell;
            
            allrewmod(cntcells).hist_rew=currhist_rew*(1000/binsize); % Convert to firing rate in Hz
            allrewmod(cntcells).raster_rew=currraster_rew;
            allrewmod(cntcells).Nspk_rew=currNspk_rew;
            
            allrewmod(cntcells).hist_norew=currhist_norew*(1000/binsize); % Convert to firing rate in Hz
            allrewmod(cntcells).raster_norew=currraster_norew;
            allrewmod(cntcells).Nspk_norew=currNspk_norew;
            
            % Trial Resps
            allrewmod(cntcells).trialResps_rew = currtrialResps_rew';
            allrewmod(cntcells).trialResps_bck_rew = currtrialResps_bck_rew';
            allrewmod(cntcells).trialResps_norew = currtrialResps_norew';
            allrewmod(cntcells).trialResps_bck_norew = currtrialResps_bck_norew';
            
            % Combine rew and norew
            allrewmod(cntcells).Nspk = allrewmod(cntcells).Nspk_rew + allrewmod(cntcells).Nspk_norew;
            allrewmod(cntcells).hist = [allrewmod(cntcells).hist_rew; allrewmod(cntcells).hist_norew];
            allrewmod(cntcells).raster = [ allrewmod(cntcells).raster_rew, allrewmod(cntcells).raster_norew];
            allrewmod(cntcells).trialResps = [allrewmod(cntcells).trialResps_rew; allrewmod(cntcells).trialResps_norew];
            allrewmod(cntcells).trialResps_bck = [allrewmod(cntcells).trialResps_bck_rew; allrewmod(cntcells).trialResps_bck_norew];
            % Can use the above to get combined rew_norew FR, and also modln
            
            % Modlns
            allrewmod(cntcells).FRmodln_rew = nanmean(currFRmodln_rew);
            allrewmod(cntcells).FRmodln_norew = nanmean(currFRmodln_norew);
            allrewmod(cntcells).FRmodln_rewnorew = nanmean(currFRmodln_rewnorew);
            allrewmod(cntcells).FRmodln_rewhighspeed = nanmean(currFRmodln_rewhighspeed);
            allrewmod(cntcells).FRmodln_rewprerip = nanmean(currFRmodln_rewprerip);
            allrewmod(cntcells).FRmodln_rewrip = nanmean(currFRmodln_rewrip);
            allrewmod(cntcells).FRmodln = nanmean(currFRmodln);
            
            % Firing rates
            % Properties
            allrewmod(cntcells).cellfr = nanmean(curr_cellfr);
            allrewmod(cntcells).cellFR = nanmean(curr_cellFR);
            allrewmod(cntcells).cellinfoFR = nanmean(curr_cellinfoFR);
            % rew
            allrewmod(cntcells).rewFR = nanmean(currrewFR);
            allrewmod(cntcells).rewbckFR = nanmean(currrewbckFR);
            allrewmod(cntcells).norewFR = nanmean(currnorewFR);
            allrewmod(cntcells).norewbckFR = nanmean(currnorewbckFR);
            % speed-rip
            allrewmod(cntcells).ripFR = nanmean(currripFR);
            allrewmod(cntcells).preripFR = nanmean(currpreripFR);
            allrewmod(cntcells).highspeedFR = nanmean(currhighspeedFR);
        end
    end
    
    % Save in non-structure format for ease of loading
    cntcells
    save_FRmodln_rew = [];
    save_FRmodln_norew = [];
    save_FRmodln_rewnorew = [];
    save_FRmodln_rewhighspeed = [];
    save_FRmodln_rewprerip = [];
    save_FRmodln_rewrip = [];
    
    save_cellFR = [];
    save_rewFR = [];
    save_rewbckFR = [];
    save_norewFR = [];
    save_norewbckFR = [];
    save_ripFR = [];
    save_preripFR = [];
    save_highspeedFR = [];

    
    for c=1:cntcells;
        save_FRmodln_rew = [save_FRmodln_rew allrewmod(c).FRmodln_rew];
        save_FRmodln_norew = [save_FRmodln_norew allrewmod(c).FRmodln_norew];
        save_FRmodln_rewnorew = [save_FRmodln_rewnorew allrewmod(c).FRmodln_rewnorew];
        save_FRmodln_rewhighspeed = [save_FRmodln_rewhighspeed allrewmod(c).FRmodln_rewhighspeed];
        save_FRmodln_rewprerip = [save_FRmodln_rewprerip allrewmod(c).FRmodln_rewprerip];
        save_FRmodln_rewrip = [save_FRmodln_rewrip allrewmod(c).FRmodln_rewrip];
        
        save_cellFR = [save_cellFR allrewmod(c).cellFR];
        save_rewFR = [save_rewFR allrewmod(c).rewFR];
        save_rewbckFR = [save_rewbckFR allrewmod(c).rewbckFR];
        save_norewFR = [save_norewFR allrewmod(c).norewFR];
        save_norewbckFR = [save_norewbckFR allrewmod(c).norewbckFR];
        save_ripFR = [save_ripFR allrewmod(c).ripFR];
        save_preripFR = [save_preripFR allrewmod(c).preripFR];
        save_highspeedFR = [save_highspeedFR allrewmod(c).highspeedFR];
        
    end
    
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile, '-v7.3');
        return;
    end
    
else % gatherdata=0
    
    %load(gatherdatafile);
    %end % end gather data
    % NOT GATHERING - LOAD THE SEPARATE FILES AND COMPARE
    
    set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
    
    %figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/SpeedMod/';
    figdir = '/data25/sjadhav/HPExpt/Figures/Reward/RewardSumm/';
    
    savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';


    %a) inh  n=50
    load([savedir 'HP_rewposmod_ripspeedFR_PFCripINH_X6_gather'],'save_FRmodln_rew','save_FRmodln_rewhighspeed',...
        'save_FRmodln_rewnorew','save_rewFR','save_cellFR');
    FRmodln_rew_Inh = save_FRmodln_rew;
    FRmodln_rewhighspeed_Inh = save_FRmodln_rewhighspeed;
    FRmodln_rewnorew_Inh = save_FRmodln_rewnorew;
    rewFR_Inh = save_rewFR;
    cellFR_Inh = save_cellFR;
 
     %b) exc  n=57
    load([savedir 'HP_rewposmod_ripspeedFR_PFCripEXC_X6_gather'],'save_FRmodln_rew','save_FRmodln_rewhighspeed',...
        'save_FRmodln_rewnorew','save_rewFR','save_cellFR');
    FRmodln_rew_Exc = save_FRmodln_rew;
    FRmodln_rewhighspeed_Exc = save_FRmodln_rewhighspeed;
    FRmodln_rewnorew_Exc = save_FRmodln_rewnorew;
    rewFR_Exc = save_rewFR;
    cellFR_Exc = save_cellFR;
    
    [p_rew,h_rew] = ranksum(FRmodln_rew_Inh, FRmodln_rew_Exc)  % p = 0.0075
    [p_rewhs,h_rewhs] = ranksum(FRmodln_rewhighspeed_Inh, FRmodln_rewhighspeed_Exc) % p=0.006
    
%     %c) Unmod  n=194
    load([savedir 'HP_rewposmod_ripspeedFR_PFCripUnmod_X6_gather'],'save_FRmodln_rew','save_FRmodln_rewhighspeed',...
        'save_FRmodln_rewnorew','save_rewFR','save_cellFR');
    FRmodln_rew_Unmod = save_FRmodln_rew;
    FRmodln_rewhighspeed_Unmod = save_FRmodln_rewhighspeed;
    FRmodln_rewnorew_Unmod = save_FRmodln_rewnorew;
    rewFR_Unmod = save_rewFR;
    cellFR_Unmod = save_cellFR;
    
    [p_rew2,h_rew2] = ranksum(FRmodln_rew_Inh, FRmodln_rew_Unmod)  % 0.046
    [p_rewhs2,h_rewhs2] = ranksum(FRmodln_rewhighspeed_Inh, FRmodln_rewhighspeed_Unmod) %0.0016
    
    [p_rew3,h_rew3] = ranksum(FRmodln_rew_Exc, FRmodln_rew_Unmod) %0.43
    [p_rewhs3,h_rewhs3] = ranksum(FRmodln_rewhighspeed_Exc, FRmodln_rewhighspeed_Unmod) %0.9
    
    
    % Figure 1
    % --------
    figure; hold on;
    %subplot(2,1,1)
    hold on;
    bar(1,mean(FRmodln_rew_Exc),'facecolor','r');
    bar(2,mean(FRmodln_rew_Inh),'facecolor','b');
    bar(3,nanmean(FRmodln_rew_Unmod),'facecolor','c');
    errorbar2(2,mean( FRmodln_rew_Inh),sem( FRmodln_rew_Inh),0.3,'b');
    errorbar2(1,mean(FRmodln_rew_Exc),sem(FRmodln_rew_Exc),0.3,'r');
    errorbar2(3,nanmean(FRmodln_rew_Unmod),nansem(FRmodln_rew_Unmod),0.3,'c');
    
    ylabel('Post vs Pre Reward Modln')
    set(gca,'XTick',[]);
    title(['Reward Modln Index'])
    ylim([-0.15 0.22]);
    
    figfile = [figdir,'Reward_Modulation']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    % Figure 2
    % --------
    figure; hold on;
    %subplot(2,1,1)
    hold on;
    bar(2,mean(FRmodln_rewhighspeed_Inh),'facecolor','b');
    bar(1,mean(FRmodln_rewhighspeed_Exc),'facecolor','r');
    bar(3,nanmean(FRmodln_rewhighspeed_Unmod),'facecolor','c');
    errorbar2(2,mean(FRmodln_rewhighspeed_Inh),sem(FRmodln_rewhighspeed_Inh),0.3,'b');
    errorbar2(1,mean(FRmodln_rewhighspeed_Exc),sem(FRmodln_rewhighspeed_Exc),0.3,'r');
    errorbar2(3,nanmean(FRmodln_rewhighspeed_Unmod),nansem(FRmodln_rewhighspeed_Unmod),0.3,'c');
    
    ylabel('Reward vs HighSpeed FRbias')
    set(gca,'XTick',[]);
    title(['Reward-HighSpd Index'])
    ylim([-0.22 0.15]);
    
    figfile = [figdir,'Reward_vs_HighSpeed_FRbias']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    % Figure 3
    % --------
    figure; hold on;
    %subplot(2,1,1)
    hold on;
    bar(2,mean(FRmodln_rewnorew_Inh),'facecolor','b');
    bar(1,mean(FRmodln_rewnorew_Exc),'facecolor','r');
    bar(3,nanmean(FRmodln_rewnorew_Unmod),'facecolor','c');
    errorbar2(2,mean(FRmodln_rewnorew_Inh),sem(FRmodln_rewnorew_Inh),0.3,'b');
    errorbar2(1,mean(FRmodln_rewnorew_Exc),sem(FRmodln_rewnorew_Exc),0.3,'r');
    errorbar2(3,nanmean(FRmodln_rewnorew_Unmod),nansem(FRmodln_rewnorew_Unmod),0.3,'c');
    
    ylabel('Reward vs NoRew FRbias')
    set(gca,'XTick',[]);
    title(['Reward-NoRew Index'])
    
    figfile = [figdir,'Reward_vs_NoRew_FRbias']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    
    keyboard;
    
end