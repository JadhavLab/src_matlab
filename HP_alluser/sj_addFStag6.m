
function sj_addFStag6(animno,animdirect,fileprefix)
% Adds a FStag based on output of DFSsj_getcellinfo2. For PFC cells, the
% thresh firing rate turns out to be 15Hz, and for CA1, its the usual of 7Hz
% No need to do this for CA1, but doing for completemess sake

%sj_addFStag6(1,'/data25/sjadhav/HPExpt/HPa_direct/','HPa');
%sj_addFStag6(2,'/data25/sjadhav/HPExpt/HPb_direct/','HPb');
%sj_addFStag6(3,'/data25/sjadhav/HPExpt/HPc_direct/','HPc');
%sj_addFStag6(4,'/data25/sjadhav/HPExpt/Ndl_direct/','Ndl');
%sj_addFStag6(5,'/data25/sjadhav/HPExpt/Rtl_direct/','Rtl');
%sj_addFStag6(6,'/data25/sjadhav/HPExpt/Brg_direct/','Brg');

%sj_addFStag6(1,'/mnt/data25new/sjadhav/HPExpt/HPa_direct/','HPa')
%sj_addFStag6(2,'/mnt/data25new/sjadhav/HPExpt/HPb_direct/','HPb')
%sj_addFStag6(3,'/mnt/data25new/sjadhav/HPExpt/HPc_direct/','HPc')
%sj_addFStag6(4,'/mnt/data25new/sjadhav/HPExpt/Ndl_direct/','Ndl')

% ---------------------------------------------------

% Start with a clean slate. Set FStag for all to Undefined = "u".
% This one is a little different, since you want to set the tag in all the epochs. Ideally, shouldn't 
% loop across epochs, but easier to keep the structure as-is. 


load([animdirect, fileprefix,'cellinfo']);
o = cellfetch(cellinfo,'numspikes');
targetcells = o.index;
for i = 1:size(targetcells,1)
    cellinfo{targetcells(i,1)}{targetcells(i,2)}{targetcells(i,3)}{targetcells(i,4)}.FStag = 'u';
end

procdatadir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
%procdatadir = 'mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';

%1) Do PFC cells first

%1a) RUN DATA FOR PFC
load([procdatadir, 'HP_cellinfo_PFC_gather_4-22-2015']);
animidxs = find(allcellidx(:,1)==animno);
daytetcell_list = allcellidx(animidxs,[2 3 4]);

% Instead of looping through daytetcell_list, loop through cellinfo and find matches

mindays = 1; maxdays = length(cellinfo);
if animno==4, 
    mindays = 8; maxdays = 17;
end

for d=mindays:maxdays
    
    if animno<4 % If Wtrack
        if d==1,
            runepochs = [4 6]; % only Wtracks for now.
            allepochs = [1:7]; % To propagate FStag across epochs
        else
            runepochs = [2 4];
            allepochs = [1:5]; % To propagate FStag across epochs
        end
    else
        d % day starts from 8 for anim4
        taskfile = sprintf('%s/%stask%02d.mat', animdirect, fileprefix, d);
        load(taskfile);
        allepochs = 1:length(task{d});
        tmpepochs = length(task{d}); 
        runepochs = [];
        for tmpep = 1:tmpepochs
            if strcmp(task{d}{tmpep}.type,'run') % If a run epoch
                runepochs = [runepochs; tmpep];
            end
        end
    end
        
    
    for e=1:length(runepochs)
        ep = runepochs(e);
        for tet=1:length(cellinfo{d}{ep})
            if ~isempty(cellinfo{d}{ep}{tet}) % if cells for the tet
                for c=1:length(cellinfo{d}{ep}{tet})
                    
                    currdaytetcell=[d tet c];
                    match = rowfind(currdaytetcell, daytetcell_list);
                    
                    % There should be only 1 match. If no match, its undefined for that cell.
                    if match~=0                       
                        getdataidx = animidxs(match);
   
                        if strcmp(celloutput(getdataidx).FStag,'y') 
                            %currdaytetcell, keyboard;
                            disp('FS Neuron');
                            [d ep tet c],
                            cellinfo{d}{ep}{tet}{c}.FStag='y';
                            for tmpe=allepochs
                                tmpe;
                                [d tmpe tet c];
                                cellinfo{d}{tmpe}{tet}{c}.FStag='y';
                            end
                            
                        else
                           cellinfo{d}{ep}{tet}{c}.FStag='n';
                            for tmpe=allepochs
                                tmpe;
                                [d tmpe tet c];
                                cellinfo{d}{tmpe}{tet}{c}.FStag='n';
                            end
                            
                        end
                    end
                    
                end % end c
            end % end c
        end % end tet
    end % end ep
end %end d



          



% %2) Do CA1 and iCA1 cells next
% 
% %1a) RUN DATA FOR Hipp
load([procdatadir, 'HP_cellinfo_CA1_gather_4-22-2015']);
animidxs = find(allcellidx(:,1)==animno);
daytetcell_list = allcellidx(animidxs,[2 3 4]);

% Instead of looping through daytetcell_list, loop through cellinfo and find matches

mindays = 1; maxdays = length(cellinfo);
if animno==4, %Ndl
    mindays = 8; maxdays = 17;
end

for d=mindays:maxdays
    
    if animno<4 % If Wtrack
        if d==1,
            runepochs = [4 6]; % only Wtracks for now.
            allepochs = [1:7]; % To propagate runripmodtag
        else
            runepochs = [2 4];
            allepochs = [1:5]; % To propagate runripmodtag
        end
    else
        d % day starts from 8
        taskfile = sprintf('%s/%stask%02d.mat', animdirect, fileprefix, d);
        load(taskfile);
        allepochs = 1:length(task{d});
        tmpepochs = length(task{d}); 
        runepochs = [];
        for tmpep = 1:tmpepochs
            if strcmp(task{d}{tmpep}.type,'run') % If a run epoch
                runepochs = [runepochs; tmpep];
            end
        end
    end
    
    for e=1:length(runepochs)
        ep = runepochs(e);
        for tet=1:length(cellinfo{d}{ep})
            if ~isempty(cellinfo{d}{ep}{tet}) % if cells for the tet
                for c=1:length(cellinfo{d}{ep}{tet})
                    
                    currdaytetcell=[d tet c];
                    match = rowfind(currdaytetcell, daytetcell_list);
                    
                    % There should be only 1 match. If no match, its undefined for that cell.
                    if match~=0
                        getdataidx = animidxs(match);
                        if strcmp(celloutput(getdataidx).FStag,'y') 
                            cellinfo{d}{ep}{tet}{c}.FStag='y';
                             disp('CA1 FS Neuron');
                            [d ep tet c],
                            for tmpe=allepochs
                                tmpe;
                                [d tmpe tet c];
                                cellinfo{d}{tmpe}{tet}{c}.FStag='y';
                            end
                            
                        else
                           cellinfo{d}{ep}{tet}{c}.FStag='n';
                            for tmpe=allepochs
                                tmpe;
                                [d tmpe tet c];
                                cellinfo{d}{tmpe}{tet}{c}.FStag='n';
                            end 
                        end
                    end
                    
                end % end c
            end % end c
        end % end tet
    end % end ep
end %end d




i=1;
% Save updated cellinfo file
save([animdirect, fileprefix,'cellinfo'], 'cellinfo');



