function out = DFAsj_getripalignspiking5_forRipLFP(sind,tind, excludetimes, spikes, ripples, tetinfo, pos, ripple, varargin)
% out = DFAsj_getripalignspiking(spike_index, eegindex, excludeperiods, spikes, ripples, tetinfo, options)

% Called from DFSsj_getripalignspiking
% Use tetinfo and tetfilter passed in, or redefine here to get riptets
% Then use ripples to getriptimes. Use inter-ripple-interval of 1 sec, and use a low-speed criterion.
% Then align spikes to ripples


tetfilter = '';
excludetimes = [];
maxcell = 0;
minstd = 3;
lowsp_thrs = 5; %cm/sec
highsp_thrs = lowsp_thrs;
dospeed = 0;
minrip=1;

% EEG
% No. of indices for swr envelope:
%500ms before and 500ms after, Fs=1500Hz,
% so 750 before and 750 after = 500ms
% 0r 450 before and 450 after is 300ms
preind=450; postind=450;

% Spikes
pret=550; postt=550;

for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'minstd'
            minstd = varargin{option+1};
        case 'minrip'
            minrip = varargin{option+1};
        case 'maxcell'
            maxcell = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        case 'lowsp_thrs'
            lowsp_thrs = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

day = tind(1);
epoch = tind(2);

% Get riptimes
% -------------
if isempty(tetfilter)
    riptimes = sj_getripples_tetinfo(tind, ripples, tetinfo, 'tetfilter', '(isequal($descrip, ''riptet''))','minstd',minstd,'minrip',minrip);
else
    riptimes = sj_getripples_tetinfo(tind, ripples, tetinfo, 'tetfilter', tetfilter, 'minstd', minstd);
end
% Can opt to have a cellcountthresh for each event as in getpopulationevents2 or  sj_HPexpt_ripalign_singlecell_getrip4
% Not using as of now

% Get triggers as rip starttimes separated by at least 1 sec
% ----------------------------------------------------------
rip_starttime = 1000*riptimes(:,1);  % in ms

% Find ripples separated by atleast a second
% --------------------------------------------
iri = diff(rip_starttime);
keepidx = [1;find(iri>=1000)+1];
rip_starttime = rip_starttime(keepidx);

% Implement speed criterion - Skip for now, or keep. Try both
% ----------------------------------------
if dospeed
    absvel = abs(pos{day}{epoch}.data(:,5)); % Can also use field 9
    postime = pos{day}{epoch}.data(:,1); % in secs
    pidx = lookup(rip_starttime,postime*1000);
    speed_atrip = absvel(pidx);
    lowsp_idx = find(speed_atrip <= lowsp_thrs);
    highsp_idx = find(speed_atrip > highsp_thrs);
    
    rip_starttime = rip_starttime(lowsp_idx);
end


rip_starttime = rip_starttime./1000; % Convert to secs for eeg

try
    teeg = geteegtimes(ripple{tind(1)}{tind(2)}{tind(3)});
catch
    keyboard;
end
    ripenv_all = ripple{tind(1)}{tind(2)}{tind(3)}.data(:,3);
ripamp_all = ripple{tind(1)}{tind(2)}{tind(3)}.data(:,1);


% Get the spike times
% --------------------
if ~isempty(spikes{sind(1)}{sind(2)}{sind(3)}{sind(4)}.data)
    spikeu = spikes{sind(1)}{sind(2)}{sind(3)}{sind(4)}.data(:,1)*1000;  % in ms
else
    spikeu = [];
end


% Get SWR LFP aligned to ripple
% ------------------------------
cntrip=0; ripenv=[]; ripamp=[]; nspk=0;
for i=2:length(rip_starttime)-1  % Skip first and last
    i;
    % Align to ripples
    % ------------------------------------
    cntrip=cntrip+1;
    currrip = rip_starttime(i);
    
    currtind = lookup( currrip, teeg);
    ripenv(cntrip,:) = ripenv_all(currtind-preind:currtind+preind);
    ripamp(cntrip,:) = ripamp_all(currtind-preind:currtind+preind);
    
    % Get spike count for comparing with original. this is in ms now
    currrip_ms = currrip*1000;
    currspks =  spikeu(find( (spikeu>=(currrip_ms-pret)) & (spikeu<=(currrip_ms+postt)) ));
    nspk = nspk + length(currspks);
    
end


rip_starttime = rip_starttime*1000; % conver to ms for passing to output


% Output
% ------
out.index = sind;
out.tindex = tind;
out.rip_starttime = rip_starttime; % in ms
out.Nrip = cntrip;
out.ripenv = ripenv;
out.ripamp = ripamp;
out.Nspikes = nspk; % Nspks in the entire -pret:postt window across all the events for real ripple events

out.preind = preind;
out.postind = postind;
out.pret = pret;
out.postt = postt;





