% % ------------
% % PLOTTING, ETC
% % PFC Properties - from Demetris' code: DFS_DRsj_spatialvsripcorr.m
% % ------------

% Update everything for FS removed condition

makeCA1neuInds=0;
makePFCneuInds=0;
gatherGlobalTraj = 0; % From global traj, get PFC ones for Exc, Inh, and Neu - just like gatherPE above
% Also get allRunTraj - separated by arm, but in and out combined
gatherPE=0; % organize the PE values into something managebale for plotting

plotPE_armidx=0; % plot PE vs armidx
plotDirnIdx=1; % Plot Dirn Idx spread in poln - can also compare to Arm Idx and PE
%plotPE_replayidx=0; %plot PE vs replayidx
%plotSWRcorr=1; % Plot swrcorr vs spatialcorr

figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/';
savefig1=0;
% % ----FIG PROP --------------
forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
if forppr==1
    set(0,'defaultaxesfontsize',20);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end
% % ---------------------

if makeCA1neuInds % Get CA1 Inds - All Neurons
    load ('/data25/sjadhav/HPExpt/HP_ProcessedData/HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6.mat','allripplemod');
    
    CA1inds = []; CA1indsFS = [];
    for i=1:length(allripplemod)
        if strcmp(allripplemod(i).FStag,'n')
            CA1inds = [CA1inds; allripplemod(i).index];
        else
            CA1indsFS = [CA1indsFS; allripplemod(i).index];
        end
    end
    
    % Originally, we had 585 CA!. With FS removed, have to update this. 
    % We now have        583 CA1, and 2 FS which were still in ripplemod (out of 23): Total 583+23; 23/606=3.8% FS
    
    savefile =  '/data25/sjadhav/HPExpt/HP_ProcessedData/CA1Inds_noFS_X6.mat'
    save(savefile,'CA1inds','CA1indsFS');
end % end makePFCneuInds


if makePFCneuInds
    load ('/data25/sjadhav/HPExpt/HP_ProcessedData/HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6.mat','allripplemod');
    
    % The orig PFCindsExc, PFCindsInh. 
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/swrmodinds_Jan27th.mat'; % PFC Exc-Inh Indices
    PFCindsExc_orig = PFCindsExc; PFCindsInh_orig = PFCindsInh; % Keep for comparison
    
    PFCindsNeu = []; PFCindsExc_Redo = []; PFCindsInh_Redo = []; PFCindsFS=[];
    for i=1:length(allripplemod)
        
        if strcmp(allripplemod(i).FStag,'y')
            PFCindsFS = [PFCindsFS; allripplemod(i).index];
        end
        
        if allripplemod(i).rasterShufP2>=0.05 && strcmp(allripplemod(i).FStag,'n')   % Get non-modulated indices, and not FS
            PFCindsNeu = [PFCindsNeu; allripplemod(i).index];
        else %Re-check Exc-Inh indices
            if strcmp(allripplemod(i).type, 'exc')  && strcmp(allripplemod(i).FStag,'n')
                PFCindsExc_Redo = [PFCindsExc_Redo; allripplemod(i).index];
            elseif strcmp(allripplemod(i).type, 'inh')  && strcmp(allripplemod(i).FStag,'n')
                PFCindsInh_Redo = [PFCindsInh_Redo; allripplemod(i).index];
            end
        end
    end
    
    % Originally, we had 60 Exc, 55 Inh, and 209 Neu. With FS removed, have to update this. 
    % We now have        57 Exc, 52 Inh, and 203 Neu. (12 FS cells removed):312 RS and 12 FS, 12/324 = 3.7% FS; 
    % Mod = 109/312 = 34.9% Exc = 57/312 = 18.3%, Inh = 52/312 = 1.7%
  
    PFCindsExc = PFCindsExc_Redo; % Overwrite the original PFCindsExc and Inh
    PFCindsInh = PFCindsInh_Redo;
    
    savefile =  '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_noFS_X6.mat'
    save(savefile,'PFCindsExc','PFCindsInh','PFCindsNeu','PFCindsExc_Redo','PFCindsInh_Redo','PFCindsFS','PFCindsExc_orig','PFCindsInh_orig');
end % end makePFCneuInds



if gatherGlobalTraj
    %fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/GlobalPEtraj_X6.mat';
    %load(fname,'allGlobalPEtraj','allGlobalIdxs', 'allGlobalPE');
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/GlobalPEtraj_Runtraj_noFS_X6.mat';
    load(fname,'allGlobalPEtraj','allGlobalIdxs', 'allGlobalPE','allRunTraj');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_noFS_X6.mat'; % PFC Exc-Inh-Neu Indices
    
    % a) First just get allPFC ones, instead of separating in Exc, Inh, Neu
    % This will get values for PFC, skipping all CA1 ones
    % You can separate while comparing with replay response directly
    PFCindsAll = [PFCindsExc;PFCindsInh;PFCindsNeu]; % All PFC mod
    PFCGlobalPEtraj=[]; PFCGlobalIdxs=[]; PFCGlobalPE=[]; pfcmatch=[];
    PFCRunTraj=[];
    cnt=0;
    for i = 1:length(PFCindsAll)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsAll(i,:), 'rows'));
        
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCGlobalIdxs = [PFCGlobalIdxs; PFCindsAll(i,:)];
            PFCGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % Also Separate Exc, Inh, Neu, just in case
    % b) PFCexc
    PFCexcGlobalPEtraj=[]; PFCexcGlobalIdxs=[]; PFCexcGlobalPE=[]; pfcmatch=[];
    cnt=0;
    for i = 1:length(PFCindsExc)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsExc(i,:), 'rows'));
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCexcGlobalIdxs = [PFCexcGlobalIdxs; PFCindsExc(i,:)];
            PFCexcGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCexcGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCexcRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % c) PFCinh
    PFCinhGlobalPEtraj=[]; PFCinhGlobalIdxs=[]; PFCinhGlobalPE=[]; pfcmatch=[];
    cnt=0;
    for i = 1:length(PFCindsInh)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsInh(i,:), 'rows'));
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCinhGlobalIdxs = [PFCinhGlobalIdxs; PFCindsInh(i,:)];
            PFCinhGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCinhGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCinhRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % d) PFCneu
    PFCneuGlobalPEtraj=[]; PFCneuGlobalIdxs=[]; PFCneuGlobalPE=[]; pfcmatch=[];
    cnt=0;
    for i = 1:length(PFCindsNeu)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsNeu(i,:), 'rows'));
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCneuGlobalIdxs = [PFCneuGlobalIdxs; PFCindsNeu(i,:)];
            PFCneuGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCneuGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCneuRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % Have 310 of 312 total cells, 57/57 exc, 52/52 inh, 201/203 Neu
    
    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_GlobalPEtraj_Runtraj_noFS_X6.mat'
    save(savefile,'PFCGlobalIdxs','PFCGlobalPEtraj','PFCGlobalPE','PFCexcGlobalIdxs','PFCexcGlobalPEtraj','PFCexcGlobalPE', ...
        'PFCinhGlobalIdxs','PFCinhGlobalPEtraj','PFCinhGlobalPE','PFCneuGlobalIdxs','PFCneuGlobalPEtraj','PFCneuGlobalPE', ...
        'PFCRunTraj','PFCexcRunTraj','PFCinhRunTraj','PFCneuRunTraj');
    
end % end gatherGlobalTraj




if gatherPE
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/DR_Proc_SJ_noFS_X6.mat';
    load(fname,'PEcoefallv2INDS','PEcoefallv2','sparsityALLdata');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_noFS_X6.mat'; % PFC Exc-Inh-Neu Indices
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/CA1Inds_noFS_X6.mat'; % CA1 Indices: all
    
    pfcEXCPE=[]; pfcINHPE=[]; pfcNEUPE=[]; skippedPE=[]; usePECval = 3; %3 is norm PE2, 11 is norm PE1
    pfcEXCPEinds=[]; pfcINHPEinds=[]; pfcNEUPEinds=[];
    % Get all the PE values for Exc and Inh PFC
    for i = 1:length(PFCindsExc);
        try%skip cells without PEC
            %              mean(pfcINHPE)   pfcexcmatch = find(ismember(PEcoefall(:,1:4), PFCindsExc(i,:), 'rows'));
            pfcexcmatch = find(ismember(PEcoefallv2INDS(:,1:4), PFCindsExc(i,:), 'rows'));
            %                 pfcEXCPE(i,:) = PEcoefallv2{pfcexcmatch,3}';
            pfcEXCPE(i,:) = mean([PEcoefallv2{pfcexcmatch,usePECval}']); % mean across the two PE values
            pfcEXCPEinds(i,:) = PEcoefallv2{pfcexcmatch,1};
            %             if PEcoefallv2{pfcexcmatch,1}(1,[1:4]) ~= sparsityALLdata{pfcexcmatch,1};
            %                 keyboard %pause if the sparsity data and the PE data don't have matching indices
            %             end
            %             pfcEXCSpar(i,:) = [sparsityALLdata{pfcexcmatch,2} nanmean(pfcEXCPE(i,[1:2]))];
        catch
            %keyboard;
            skippedPE = [skippedPE; PFCindsExc(i,:)];
        end
    end
    
    for i = 1:length(PFCindsInh);
        try %skip cells without PEC
            %                 pfcinhmatch = find(ismember(PEcoefall(:,1:4), PFCindsInh(i,:), 'rows'));
            %                 pfcINHPE(i) = PEcoefall(pfcinhmatch,5);
            pfcinhmatch = find(ismember(PEcoefallv2INDS(:,1:4), PFCindsInh(i,:), 'rows'));
            %                 pfcINHPE(i,:) = PEcoefallv2{pfcinhmatch,3}';
            pfcINHPE(i,:) = mean([PEcoefallv2{pfcinhmatch,usePECval}']);
            pfcINHPEinds(i,:) = PEcoefallv2{pfcinhmatch,1};
            %             if PEcoefallv2{pfcinhmatch,1}(1,[1:4]) ~= sparsityALLdata{pfcinhmatch,1};
            %                 keyboard %pause if the sparsity data and the PE data don't have matching indices
            %             end
            %             pfcINHSpar(i,:) = [sparsityALLdata{pfcinhmatch,2} nanmean(pfcINHPE(i,[1:2]))];
        catch
            %keyboard;
            skippedPE = [skippedPE; PFCindsInh(i,:)];
        end
    end
    
    for i = 1:length(PFCindsNeu);
        try %skip cells without PEC
            %                 pfcinhmatch = find(ismember(PEcoefall(:,1:4), PFCindsInh(i,:), 'rows'));
            %                 pfcINHPE(i) = PEcoefall(pfcinhmatch,5);
            pfcneumatch = find(ismember(PEcoefallv2INDS(:,1:4), PFCindsNeu(i,:), 'rows'));
            %                 pfcINHPE(i,:) = PEcoefallv2{pfcinhmatch,3}';
            pfcNEUPE(i,:) = mean([PEcoefallv2{pfcneumatch,usePECval}']);
            pfcNEUPEinds(i,:) = PEcoefallv2{pfcneumatch,1};
            %             if PEcoefallv2{pfcinhmatch,1}(1,[1:4]) ~= sparsityALLdata{pfcinhmatch,1};
            %                 keyboard %pause if the sparsity data and the PE data don't have matching indices
            %             end
            %             pfcINHSpar(i,:) = [sparsityALLdata{pfcinhmatch,2} nanmean(pfcINHPE(i,[1:2]))];
        catch
            %keyboard;
            skippedPE = [skippedPE; PFCindsNeu(i,:)];
        end
    end
    
    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_PE_noFS_X6.mat'
    save(savefile,'pfcEXCPE','pfcINHPE','pfcNEUPE','pfcEXCPEinds','pfcINHPEinds','pfcNEUPEinds');
    
    % Get CA1 PE as well
    CA1PE=[]; skippedCA1PE=[]; CA1PEinds=[]; 
    % Get all the PE values for Exc and Inh PFC
    for i = 1:length(CA1inds);
        try%skip cells without PEC
            %              mean(pfcINHPE)   pfcexcmatch = find(ismember(PEcoefall(:,1:4), PFCindsExc(i,:), 'rows'));
            match = find(ismember(PEcoefallv2INDS(:,1:4), CA1inds(i,:), 'rows'));
            %                 pfcEXCPE(i,:) = PEcoefallv2{pfcexcmatch,3}';
            CA1PE(i,:) = mean([PEcoefallv2{match,usePECval}']); % mean across the two PE values
            CA1PEinds(i,:) = PEcoefallv2{match,1};
            %             if PEcoefallv2{pfcexcmatch,1}(1,[1:4]) ~= sparsityALLdata{pfcexcmatch,1};
            %                 keyboard %pause if the sparsity data and the PE data don't have matching indices
            %             end
            %             pfcEXCSpar(i,:) = [sparsityALLdata{pfcexcmatch,2} nanmean(pfcEXCPE(i,[1:2]))];
        catch
            %keyboard;
            skippedCA1PE = [skippedCA1PE; CA1inds(i,:)];
        end
    end
    
    
    figure; hold on;
    %bar([mean(inhsignegrippairs) mean(inhnonsigrippairs) mean(inhsigposrippairs) mean(excsignegrippairs) mean(excnonsigrippairs) mean(excsigposrippairs)]); hold on;
    bar(1, nanmean(pfcEXCPE),'r');  
    bar(2, nanmean(pfcINHPE),'b');  
    bar(3, nanmean(pfcNEUPE),'g');  
    bar(4, nanmean(CA1PE),'k');  
    legend('PFCexc','PFCinh','PFCneu','CA1'); 
    errorbar2(1, nanmean(pfcEXCPE), nanstderr(pfcEXCPE), 0.3, 'r');
    errorbar2(2, nanmean(pfcINHPE), nanstderr(pfcINHPE), 0.3, 'b');
    errorbar2(3, nanmean(pfcNEUPE), nanstderr(pfcNEUPE), 0.3, 'g');
    errorbar2(4, nanmean(CA1PE), nanstderr(CA1PE), 0.3, 'k');

    %bar([mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)]); hold on;
    %errorbar2([1 2 3], [mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)],  [stderr(pfcEXCPE) stderr(pfcINHPE) stderr(pfcNEUPE)] , 0.3, 'k')
    ylim([0 1]);
    title('Path Equivalence for PFC and CA1 neurons ','FontSize',24,'FontWeight','normal');
    ylabel('Path Equivalence','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3 4]); set(gca,'XTickLabel',{'Exc','Inh','Neu','CA1'});
    
     
    figfile = [figdir,'PathEquivalence_PFCandCA1']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    [pKW table statsKW] = kruskalwallis([pfcEXCPE' pfcINHPE' pfcNEUPE' CA1PE'], [ones(1, length(pfcEXCPE)) ones(1,length(pfcINHPE)).*2 ...
        ones(1,length(pfcNEUPE)).*3 ones(1,length(CA1PE)).*4]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    [pKW table statsKW] = kruskalwallis([pfcEXCPE' pfcINHPE' pfcNEUPE'], [ones(1, length(pfcEXCPE)) ones(1,length(pfcINHPE)).*2 ...
        ones(1,length(pfcNEUPE)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    
    figure; hold on;
    bar(1, nanmean(pfcEXCPE),'r');  
    bar(2, nanmean(pfcINHPE),'b');  
    bar(3, nanmean(pfcNEUPE),'g'); 
    legend('PFCexc','PFCinh','PFCneu'); 
    errorbar2(1, nanmean(pfcEXCPE), nanstderr(pfcEXCPE), 0.3, 'r');
    errorbar2(2, nanmean(pfcINHPE), nanstderr(pfcINHPE), 0.3, 'b');
    errorbar2(3, nanmean(pfcNEUPE), nanstderr(pfcNEUPE), 0.3, 'g');
    
    %bar([mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)]); hold on;
    %errorbar2([1 2 3], [mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)],  [stderr(pfcEXCPE) stderr(pfcINHPE) stderr(pfcNEUPE)] , 0.3, 'k')
    ylim([0 1]);
    title('Path Equivalence for PFC neurons ','FontSize',24,'FontWeight','normal');
    ylabel('Path Equivalence','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3]); set(gca,'XTickLabel',{'Exc','Inh','Neu'});
    
     
    figfile = [figdir,'PathEquivalence_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    [pKW table statsKW] = kruskalwallis([pfcEXCPE' pfcINHPE' pfcNEUPE'], [ones(1, length(pfcEXCPE)) ones(1,length(pfcINHPE)).*2 ...
        ones(1,length(pfcNEUPE)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    [pKW table statsKW] = kruskalwallis([pfcEXCPE' pfcINHPE'], [ones(1, length(pfcEXCPE)) ones(1,length(pfcINHPE)).*2]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    
    
    %reshape ripmodpfc - Both epochs in there. so need avg
    %pfcEXCPE = reshape(pfcEXCPE(:,[1:2]), 2*length(pfcEXCPE(:,1)),1);
    %pfcINHPE = reshape(pfcINHPE(:,[1:2]),2*length(pfcINHPE(:,1)),1);
    
end





% Arm Index - and comparison with PE
% ----------------------------------
if plotPE_armidx
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_PFC_ArmIdx_gather_noFS_X6.mat';
    load(fname,'all_AI_PFCidxs','all_AI_PFCArmIdx');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_noFS_X6.mat'; % PFC Exc-Inh-Neu Indices
    PFCmod = [PFCindsExc;PFCindsInh]; % All PFC mod
    
    % Plot just ArmIdx
    excind = []; inhind = []; neuind = [];
    for i=1:size(all_AI_PFCidxs,1)
        excmatch = find(ismember(all_AI_PFCidxs(i,:), PFCindsExc, 'rows'));
        if ~isempty(excmatch)
            excind = [excind; i];
        else
            inhmatch = find(ismember(all_AI_PFCidxs(i,:), PFCindsInh, 'rows'));
            if ~isempty(inhmatch)
                inhind = [inhind; i];
            else
                neumatch = find(ismember(all_AI_PFCidxs(i,:), PFCindsNeu, 'rows'));
                if ~isempty(neumatch)
                    neuind = [neuind; i];
                end
            end
        end
    end
    
    exc_armidx = all_AI_PFCArmIdx(excind);
    inh_armidx = all_AI_PFCArmIdx(inhind);
    neu_armidx = all_AI_PFCArmIdx(neuind);
    
    figure; hold on;
    bar(1, nanmean(abs(exc_armidx)),'r');  
    bar(2, nanmean(abs(inh_armidx)),'b');  
    bar(3, nanmean(abs(neu_armidx)),'k'); 
    legend('PFCexc','PFCinh','PFCneu'); 
    errorbar2(1, nanmean(abs(exc_armidx)), nanstderr(abs(exc_armidx)), 0.3, 'r');
    errorbar2(2, nanmean(abs(inh_armidx)), nanstderr(abs(inh_armidx)), 0.3, 'b');
    errorbar2(3, nanmean(abs(neu_armidx)), nanstderr(abs(neu_armidx)), 0.3, 'k');
    
    %ylim([-1 1]);
    title('Arm Idx for PFC neurons','FontSize',24,'FontWeight','normal');
    ylabel('Arm Idx','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3]); set(gca,'XTickLabel',{'Exc','Inh','Neu'});
    
    figfile = [figdir,'Abs-ArmIdx_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    [pKW table statsKW] = kruskalwallis([abs(exc_armidx) abs(inh_armidx) abs(neu_armidx)], [ones(1, length(exc_armidx)) ones(1,length(inh_armidx)).*2 ones(1,length(neu_armidx)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range
    c
    title(sprintf('Arm Idx for PFC neurons; pKW(%0.3f)', pKW));
 
    
    % Plot PE vs ArmIdx
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_PE_noFS_X6.mat'
    excmatch=[]; inhmatch=[]; neumatch=[];
    excAI=[]; inhAI = []; neuAI=[]; excPE=[]; inhPE=[]; neuPE=[];
    
    for i=1:size(all_AI_PFCidxs,1)
        excmatch = find(ismember(pfcEXCPEinds, all_AI_PFCidxs(i,:), 'rows'));
        if ~isempty(excmatch)
            excAI = [excAI; all_AI_PFCArmIdx(i)];
            excPE = [excPE; pfcEXCPE(excmatch)];
        else
            inhmatch = find(ismember( pfcINHPEinds, all_AI_PFCidxs(i,:), 'rows'));
            if ~isempty(inhmatch)
                inhAI = [inhAI; all_AI_PFCArmIdx(i)];
                inhPE = [inhPE; pfcINHPE(inhmatch)];
            else
                neumatch = find(ismember(pfcNEUPEinds, all_AI_PFCidxs(i,:), 'rows'));
                if ~isempty(neumatch)
                    neuAI = [neuAI; all_AI_PFCArmIdx(i)];
                    neuPE = [neuPE; pfcNEUPE(neumatch)];
                end
            end
        end
    end
    
    rem = find(isnan(excPE) | isnan(excAI)); excAI(rem)=[]; excPE(rem)=[];
    rem = find(isnan(inhPE) | isnan(inhAI)); inhAI(rem)=[]; inhPE(rem)=[];
    rem = find(isnan(neuPE) | isnan(neuAI)); neuAI(rem)=[]; neuPE(rem)=[];
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(excPE,excAI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(excPE));
        randPE = excPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,excAI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inhPE,inhAI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inhPE));
        randPE = inhPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,inhAI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neuPE,neuAI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neuPE));
        randPE = neuPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,neuAI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    
    figure; hold on;
    subplot(2,2,1);
    scatter(excPE, excAI, 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(excAI, [ones(size(excPE)) excPE]);
    plot(min(excPE):.01:max(excPE), b(1)+b(2)*[min(excPE):.01:max(excPE)],'r')
    axis([0 1 -1 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inhPE, inhAI, 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(inhAI, [ones(size(inhPE)) inhPE]);
    plot(min(inhPE):.01:max(inhPE), b(1)+b(2)*[min(inhPE):.01:max(inhPE)],'b')
    axis([0 1 -1 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neuPE, neuAI, 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(neuAI, [ones(size(neuPE)) neuPE]);
    plot(min(neuPE):.01:max(neuPE), b(1)+b(2)*[min(neuPE):.01:max(neuPE)],'k')
    axis([0 1 -1 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
    
    figfile = [figdir,'ArmIdx_vs_PE_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    % Plot PE vs ABS ArmIdx, which makes more sense
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(excPE,abs(excAI)); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(excPE));
        randPE = excPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,abs(excAI));
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inhPE,abs(inhAI)); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inhPE));
        randPE = inhPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,abs(inhAI));
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neuPE,abs(neuAI)); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neuPE));
        randPE = neuPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,abs(neuAI));
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    
    figure; hold on;
    subplot(2,2,1);
    scatter(excPE, abs(excAI), 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(abs(excAI), [ones(size(excPE)) excPE]);
    plot(min(excPE):.01:max(excPE), b(1)+b(2)*[min(excPE):.01:max(excPE)],'r')
    axis([0 1 0 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inhPE, abs(inhAI), 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(abs(inhAI), [ones(size(inhPE)) inhPE]);
    plot(min(inhPE):.01:max(inhPE), b(1)+b(2)*[min(inhPE):.01:max(inhPE)],'b')
    axis([0 1 0 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neuPE, abs(neuAI), 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(abs(neuAI), [ones(size(neuPE)) neuPE]);
    plot(min(neuPE):.01:max(neuPE), b(1)+b(2)*[min(neuPE):.01:max(neuPE)],'k')
    axis([0 1 0 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
    
    figfile = [figdir,'Abs-ArmIdx_vs_PE_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    
end % end plotPE_armidx









if plotDirnIdx
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_PFC_DirnIdx_gather_noFS_X6.mat';
    load(fname,'all_DI_PFCidxs','all_DI_PFCDirnIdx');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_noFS_X6.mat'; % PFC Exc-Inh-Neu Indices
    PFCmod = [PFCindsExc;PFCindsInh]; % All PFC mod
    
    % Plot just DirnIdx
    excind = []; inhind = []; neuind = [];
    for i=1:size(all_DI_PFCidxs,1)
        excmatch = find(ismember(all_DI_PFCidxs(i,:), PFCindsExc, 'rows'));
        if ~isempty(excmatch)
            excind = [excind; i];
        else
            inhmatch = find(ismember(all_DI_PFCidxs(i,:), PFCindsInh, 'rows'));
            if ~isempty(inhmatch)
                inhind = [inhind; i];
            else
                neumatch = find(ismember(all_DI_PFCidxs(i,:), PFCindsNeu, 'rows'));
                if ~isempty(neumatch)
                    neuind = [neuind; i];
                end
            end
        end
    end
    
    % USE ABSOLUTE VALUES
    exc_Dirnidx = abs(all_DI_PFCDirnIdx(excind));
    inh_Dirnidx = abs(all_DI_PFCDirnIdx(inhind));
    neu_Dirnidx = abs(all_DI_PFCDirnIdx(neuind));
    
    figure; hold on;
    bar(1, nanmean(exc_Dirnidx),'r');  
    bar(2, nanmean(inh_Dirnidx),'b');  
    bar(3, nanmean(neu_Dirnidx),'k');  
    legend('PFCexc','PFCinh','PFCneu'); 
    errorbar2(1, nanmean(exc_Dirnidx), nanstderr(exc_Dirnidx), 0.3, 'r');
    errorbar2(2, nanmean(inh_Dirnidx), nanstderr(inh_Dirnidx), 0.3, 'b');
    errorbar2(3, nanmean(neu_Dirnidx), nanstderr(neu_Dirnidx), 0.3, 'k');
    %ylim([-1 1]);
    title('Dirn Idx for PFC neurons ','FontSize',24,'FontWeight','normal');
    ylabel('Abs (Dirn Idx)','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3]); set(gca,'XTickLabel',{'Exc','Inh','Neu'});
    
    figfile = [figdir,'Abs-DirnIdx_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    [pKW table statsKW] = kruskalwallis([exc_Dirnidx inh_Dirnidx neu_Dirnidx], [ones(1, length(exc_Dirnidx)) ones(1,length(inh_Dirnidx)).*2 ones(1,length(neu_Dirnidx)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range
    c
    
    title(sprintf('Dirn Idx for PFC neurons; pKW(%0.3f)', pKW));

    
       
    % Plot PE vs DirnIdx
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_PE_noFS_X6.mat'
    excmatch=[]; inhmatch=[]; neumatch=[];
    excDI=[]; inhDI = []; neuDI=[]; excPE=[]; inhPE=[]; neuPE=[];
    
    for i=1:size(all_DI_PFCidxs,1)
        excmatch = find(ismember(pfcEXCPEinds, all_DI_PFCidxs(i,:), 'rows'));
        if ~isempty(excmatch)
            excDI = [excDI; all_DI_PFCDirnIdx(i)];
            excPE = [excPE; pfcEXCPE(excmatch)];
        else
            inhmatch = find(ismember( pfcINHPEinds, all_DI_PFCidxs(i,:), 'rows'));
            if ~isempty(inhmatch)
                inhDI = [inhDI; all_DI_PFCDirnIdx(i)];
                inhPE = [inhPE; pfcINHPE(inhmatch)];
            else
                neumatch = find(ismember(pfcNEUPEinds, all_DI_PFCidxs(i,:), 'rows'));
                if ~isempty(neumatch)
                    neuDI = [neuDI; all_DI_PFCDirnIdx(i)];
                    neuPE = [neuPE; pfcNEUPE(neumatch)];
                end
            end
        end
    end
    
    excDI = abs(excDI); inhDI = abs(inhDI); neuDI = abs(neuDI);
    
    rem = find(isnan(excPE) | isnan(excDI)); excDI(rem)=[]; excPE(rem)=[];
    rem = find(isnan(inhPE) | isnan(inhDI)); inhDI(rem)=[]; inhPE(rem)=[];
    rem = find(isnan(neuPE) | isnan(neuDI)); neuDI(rem)=[]; neuPE(rem)=[];
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(excPE,excDI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(excPE));
        randPE = excPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,excDI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inhPE,inhDI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inhPE));
        randPE = inhPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,inhDI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neuPE,neuDI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neuPE));
        randPE = neuPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,neuDI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    
    figure; hold on;
    subplot(2,2,1);
    scatter(excPE, excDI, 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(excDI, [ones(size(excPE)) excPE]);
    plot(min(excPE):.01:max(excPE), b(1)+b(2)*[min(excPE):.01:max(excPE)],'r')
    axis([0 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inhPE, inhDI, 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(inhDI, [ones(size(inhPE)) inhPE]);
    plot(min(inhPE):.01:max(inhPE), b(1)+b(2)*[min(inhPE):.01:max(inhPE)],'b')
    axis([0 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neuPE, neuDI, 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(neuDI, [ones(size(neuPE)) neuPE]);
    plot(min(neuPE):.01:max(neuPE), b(1)+b(2)*[min(neuPE):.01:max(neuPE)],'k')
    axis([0 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
    
    figfile = [figdir,'Abs-DirnIdx_vs_PE_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    % Plot Dirn Idx vs Arm Idx
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_PFC_ArmIdx_gather_noFS_X6.mat';
    load(fname,'all_AI_PFCidxs','all_AI_PFCArmIdx');
    
    % NOTE: AI and DI idxs are the same! 
    exc_armidx = abs(all_AI_PFCArmIdx(excind));
    inh_armidx = abs(all_AI_PFCArmIdx(inhind));
    neu_armidx = abs(all_AI_PFCArmIdx(neuind));
    
    rem = find(isnan(exc_armidx) | isnan(exc_Dirnidx)); exc_armidx(rem)=[]; exc_Dirnidx(rem)=[];
    rem = find(isnan(inh_armidx) | isnan(inh_Dirnidx)); inh_armidx(rem)=[]; inh_Dirnidx(rem)=[];
    rem = find(isnan(neu_armidx) | isnan(neu_Dirnidx)); neu_armidx(rem)=[]; neu_Dirnidx(rem)=[];
   
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(exc_armidx,exc_Dirnidx); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(exc_armidx));
        rand_armidx = exc_armidx(rorder);
        [rtmp, ptmp] = corrcoef(rand_armidx,exc_Dirnidx);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inh_armidx,inh_Dirnidx); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inh_armidx));
        rand_armidx = inh_armidx(rorder);
        [rtmp, ptmp] = corrcoef(rand_armidx,inh_Dirnidx);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neu_armidx,neu_Dirnidx); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neu_armidx));
        rand_armidx = neu_armidx(rorder);
        [rtmp, ptmp] = corrcoef(rand_armidx,neu_Dirnidx);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    exc_armidx = exc_armidx';
    inh_armidx = inh_armidx';
    neu_armidx = neu_armidx';
    exc_Dirnidx = exc_Dirnidx';
    inh_Dirnidx = inh_Dirnidx';
    neu_Dirnidx = neu_Dirnidx';
    
    figure; hold on;
    subplot(2,2,1);
    scatter(exc_armidx, exc_Dirnidx, 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(exc_Dirnidx, [ones(size(exc_armidx)) exc_armidx]);
    plot(min(exc_armidx):.01:max(exc_armidx), b(1)+b(2)*[min(exc_armidx):.01:max(exc_armidx)],'r')
    axis([-0.05 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('ArmIdx'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inh_armidx, inh_Dirnidx, 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(inh_Dirnidx, [ones(size(inh_armidx)) inh_armidx]);
    plot(min(inh_armidx):.01:max(inh_armidx), b(1)+b(2)*[min(inh_armidx):.01:max(inh_armidx)],'b')
    axis([-0.05 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('ArmIdx'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neu_armidx, neu_Dirnidx, 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(neu_Dirnidx, [ones(size(neu_armidx)) neu_armidx]);
    plot(min(neu_armidx):.01:max(neu_armidx), b(1)+b(2)*[min(neu_armidx):.01:max(neu_armidx)],'k')
    axis([-0.05 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('ArmIdx'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
    figfile = [figdir,'Abs-DirnIdx_vs_Abs-ArmIdx_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
end % end plotPE_Dirnidx


