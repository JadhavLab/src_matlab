function out = DFAsj_HPexpt_getripprop(index, excludetimes, prefix, ripples, tetinfo, pos, linpos, varargin)

% Derived from DFAsj_getrippos 

% Remove references to intersections 2 and 3 

% out = DFAsj_getripalignspiking(spike_index, excludeperiods, spikes, ripples, tetinfo, options)

% Called from DFSsj_getripalignspiking
% Use tetinfo and tetfilter passed in, or redefine here to get riptets
% Then use ripples to getriptimes. Use inter-ripple-interval of 1 sec, and use a low-speed criterion.
% Then align spikes to ripples


tetfilter = '';
excludetimes = [];
maxcell = 0;
minstd = 3;
lowsp_thrs = 5; %cm/sec
highsp_thrs = lowsp_thrs;
dospeed = 0;
minrip=1;


% Fixed and variable parameters from DFAsj_getbehstats
Fs=29.97; %video sampling rate
tsamp=1/Fs; % in sec
lockout=0.25; % in sec
dur = 20*60; % Standard epoch duration in secs
% Use Calebs speed filter if necessary
defaultfilter = 'velocitydayprocess_filter.mat';
eval(['load ', defaultfilter]);
L = length(velocityfilter.kernel);
fix=0;
clr = {'b','r','g','c','m','y','k','r'};
wellclr = {'r','m','g'};

% Variable
thrsvel=5;  %<5cm per sec is still on track (3/2 for sleep session in sj_stimresp2_withvel.m)
pastsecs = 1; % For looking at history of Beh, eg. velocity: used in stimresp2
thrsdistwell = 15; %in cm, for defining well boundary
thrsdistint = 15; %in cm, for defining intersection boundary

% ----------------------------------------------------


% For ripple trigger: Dont need
% ------------------
% binsize = 10; % ms
% pret=550; postt=550; %% Times to plot
% push = 500; % either bwin(2) or postt-trim=500. For jittered trigger in background window
% trim = 50;
% cellcountthresh = 3;  % Can be used to parse ripples
% smwin=10; %Smoothing Window - along y-axis for matrix. Carry over from ...getrip4
% 
% rwin = [0 200];
% bwin = [-500 -100];
% push = 500; % either bwin(2) or postt-trim=500. If doing random events. See ... getrip4
% 
% 
for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'minstd'
            minstd = varargin{option+1};
        case 'minrip'
            minrip = varargin{option+1};
        case 'maxcell'
            maxcell = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        case 'lowsp_thrs'
            lowsp_thrs = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

% Current day and epoch
% ---------------------
day = index(1);
epoch = index(2);



% Get Position and Velocity: Considering smoothened speed
% ----------------------------
currtime = pos{day}{epoch}.data(:,1);
totaltime = length(currtime)/Fs; % in sec. This will be slightly inaccurate-limited by Fs. ~100ms
if size(pos{day}{epoch}.data,2)>5 % already smoothed position and filtered velocity
    currpos = pos{day}{epoch}.data(:,6:7);
    currvel = pos{day}{epoch}.data(:,9);
    smooth_currvel=currvel;
else
    currpos = pos{day}{epoch}.data(:,2:3);
    currvel = pos{day}{epoch}.data(:,5);
    % Look at instantaneous smoothed velocity - Smooth velocity
    smooth_currvel = [filtfilt(velocityfilter.kernel, 1, currvel)];
    %smooth_currvel = smoothvect(currvel,filt); 2nd option for convolution
end


% Get Well and Intersection positions
wellpos = linpos{day}{epoch}.wellSegmentInfo.wellCoord;
try
    intpos(1,:) = linpos{day}{epoch}.segmentInfo.segmentCoords(1,3:4);
    %intpos(2,:) = linpos{day}{epoch}.segmentInfo.segmentCoords(2,3:4);
    %intpos(3,:) = linpos{day}{epoch}.segmentInfo.segmentCoords(4,3:4);
catch
    keyboard;
end
      
% Vel-Moving vs Still
% --------------------
% Divide all velocity into moving and still
moving_vel =  smooth_currvel(find( smooth_currvel>highsp_thrs));
still_vel =  smooth_currvel(find( smooth_currvel<=lowsp_thrs));
time_moving = length(moving_vel)/Fs;
time_still = length(still_vel)/Fs;



% Get riptimes
% -------------
if isempty(tetfilter)
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', '(isequal($descrip, ''riptet''))','minstd',minstd,'minrip',minrip);
else
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', tetfilter, 'minstd', minstd);
end
% Can opt to have a cellcountthresh for each event as in getpopulationevents2 or  sj_HPexpt_ripalign_singlecell_getrip4
% Not using as of now

% Get triggers as rip starttimes separated by at least 1 sec
% ----------------------------------------------------------
rip_starttime = 1000*riptimes(:,1);  % in ms
rip_endtime = 1000*riptimes(:,2); 

% Find ripples separated by atleast a second
% --------------------------------------------
iri = diff(rip_starttime);
keepidx = [1;find(iri>=1000)+1];
rip_starttime = rip_starttime(keepidx);
rip_endtime = rip_endtime(keepidx);


% Implement speed criterion: Use same as DFAsj_getripalignspiking
% --------------------------------------------------------------
%if dospeed
absvel = abs(pos{day}{epoch}.data(:,5)); % Can also use field 9
postime = pos{day}{epoch}.data(:,1); % in secs % Same as current time
pidx = lookup(rip_starttime,postime*1000);
speed_atrip = absvel(pidx);

% Posn at ripple
pos_atrip  = pos{day}{epoch}.data(pidx,2:3);


% Bound by speed - Keep only these ripples
lowsp_idx = find(speed_atrip <= lowsp_thrs);
highsp_idx = find(speed_atrip > highsp_thrs);

rip_starttime = rip_starttime(lowsp_idx);
rip_endtime = rip_endtime(lowsp_idx);
pos_atrip = pos_atrip(lowsp_idx,:); 
% %Alternatively, find new pidx for the new subset of rip_starttime
% pidx = lookup(rip_starttime,postime*1000);
% pos_atrip  = pos{day}{epoch}.data(pidx,2:3);

Nrip = length(rip_starttime);
riprate = Nrip./totaltime;

%end

rip_duration = rip_endtime-rip_starttime; % in ms


% -------------------------------
% For rip position: don't need in this code
% -----------------------------
% SWRs close to well location: use from stim_behstats
% --------------------------------
for i=1:3
    currwellpos = wellpos(i,:);
    nripwell(i) = length(find(dist(pos_atrip,repmat(currwellpos,length(pos_atrip),1)) <= thrsdistwell));    
    pripwell{i} = pos_atrip(find(dist(pos_atrip,repmat(currwellpos,length(pos_atrip),1)) <= thrsdistwell),:); % actual position at wells
    
    % Ctr Intersection
    if i==1
        currintpos = intpos(i,:);
        nripint(i) = length(find(dist(pos_atrip,repmat(currintpos,length(pos_atrip),1)) <= thrsdistint));
        pripint{i} = pos_atrip(find(dist(pos_atrip,repmat(currintpos,length(pos_atrip),1)) <= thrsdistint),:); % actual position at intersection
        
    end
    
end

% Get fraction of ripples at wells/ ctr well, and ctr intersection
% ----------------------------------------------------------------
Nripwell = sum(nripwell); Fracripwell = Nripwell./Nrip;
NripCtrwell = nripwell(1); FracripCtrwell = NripCtrwell./Nrip;
NripSidewell = nripwell(2)+nripwell(3); FracripSidewell = NripSidewell./Nrip;

NripCtrint = nripint(1); FracripCtrint = NripCtrint./Nrip;


% Figure to test out SWR location
figopt=0;
if figopt==1;
    figure; hold on;
    %redimscreen_2x2subplots
    % Plot rippos spreads
    plot(currpos(:,1),currpos(:,2),'color',[0.9 0.9 0.9])
    plot(pos_atrip(:,1),pos_atrip(:,2),'.')
    % plot Well and Intersection positions, and circles around it
    for i=1:3
        plot(wellpos(i,1),wellpos(i,2),[wellclr{i} 's'],'MarkerSize',12,'MarkerFaceColor',[wellclr{i}]);   
        plot(pripwell{i}(:,1),pripwell{i}(:,2),[wellclr{i} '.']);
        if i==1
            plot(intpos(i,1),intpos(i,2),[wellclr{i} 's'],'MarkerSize',12,'MarkerFaceColor',[wellclr{i}]);
            plot(pripint{i}(:,1),pripint{i}(:,2),[wellclr{i} '.']);
        end
        
        %plot(pstimarm{i}(:,1),pstimarm{i}(:,2),[wellclr{i} '.']);
    end
    
    %plot(wellpos(:,1),wellpos(:,2),'rs','MarkerSize',12,'MarkerFaceColor','r');
    %plot(intpos(:,1),intpos(:,2),'cs','MarkerSize',12,'MarkerFaceColor','c');
    
    % Plot circle
    for i=1:3
        N=256;
        circle(wellpos(i,:),thrsdistwell,N,'r-');
        if i==1
            circle(intpos(i,:),thrsdistint,N,'c-');
        end
    end
    %axis([-5 150 -5 150]);
    xlabel('X-position (mm)');
    ylabel('Y-position (mm)');
    title(['Posn at Rip: Anim' prefix ' Day' num2str(day) ' Ep' num2str(epoch) '; Nrip: ' num2str(length(pos_atrip)) '; Rate: ' num2str(roundn(riprate)) ' Hz; Fracripwell: ' num2str(roundn(Fracripwell))],'FontSize',20,'Fontweight','normal');
    
    % Plot stimulation statistics
    figure; hold on;
    %subplot(2,length(allepochs),length(allepochs)+ep); hold on;
    %one = [nripwell(d,ep,1), nriparm(d,ep,1), nripint(d,ep,1)];
    %two = [nripwell(d,ep,2), nriparm(d,ep,2), nripint(d,ep,2) ];
    %thr = [nripwell(d,ep,3), nriparm(d,ep,3), nripint(d,ep,3)];
    %Y = [two; one; thr];
    one = [nripwell(1)/Nrip, nripint(1)/Nrip];
    two = [nripwell(2)/Nrip, 0];
    thr = [nripwell(3)/Nrip, 0];
    Y = [two; one; thr];
    bar(Y,'group');
    title(['Frac. of rip']);
    %if ep==1,
        legend('Well','Intersection','Location','NorthEastOutside');
    %end
    xlabel(['Section']);
    set(gca,'xtick',[1 2 3],'xticklabel',{'2';'1';'3'});
end




% From DFAsj_behstats
% ------------------
% For all 3 wells - get no. of time posn is within thrsdist to well and convert to time
for i=1:3
    currwellpos = wellpos(i,:);
    nposwell(i) = length(find(dist(currpos,repmat(currwellpos,length(currpos),1)) <= thrsdistwell));
    timewell(i) = nposwell(i)*tsamp;
end

% Sum time across all 3 wells
totaltimewells = sum(timewell); 
% Now, you also need number of trials. Use yout behavior plotting function for this
[outbound_logic] = sj_day_findoutbound (linpos,day,epoch);
[inbound_logic] = sj_day_findinbound (linpos,day,epoch);

Ntrials = length(outbound_logic) + length(inbound_logic);
Ntrials_out = length(outbound_logic);
Ntrials_in = length(inbound_logic);
avgwelltime = totaltimewells/Ntrials;
avgwelltime1 = timewell(1)/Ntrials_in; %Inbounds end at vtr well
avgwelltime2 = timewell(2)/(0.5*Ntrials_out); %Outbounds end at side wells
avgwelltime3 = timewell(3)/(0.5*Ntrials_out);






% Output
% ------
out.index = index;
out.riptime = rip_starttime;
out.pos_atrip = pos_atrip;
out.Nrip = Nrip;
out.riprate = riprate;
out.rip_duration = rip_duration;


out.wellpos = wellpos;
out.intpos = intpos;
out.Nripwell = Nripwell;
out.NripCtrwell = NripCtrwell;
out.NripSidewell = NripSidewell;
out.NripCtrint = NripCtrint;


out.Fracripwell = Fracripwell;
out.FracripCtrwell = FracripCtrwell;
out.FracripSidewell = FracripSidewell;
out.FracripCtrint = FracripCtrint;

out.pripwell = pripwell;
out.pripint = pripint;

out.thrsdistwell = thrsdistwell;
out.thrsdistint = thrsdistint;


% Output From DFAsj_behstats.m
% Vel and pos in entire epoch
out.vel = smooth_currvel;
out.pos = currpos;
% Well and intersection positions for current epoch - if you need to calculate pos_stim
out.wellpos = wellpos;
out.intpos = intpos;
out.time_moving = time_moving; % in sec
out.time_still = time_still; % in sec
out.vel_moving = moving_vel;
out.vel_still = still_vel;
%Time at reward wells
out.totaltimewells = totaltimewells;
out.nposwell = nposwell;
out.timewell = timewell;
out.Ntrials = Ntrials;
out.avgwelltime = avgwelltime;
out.avgwelltime1 = avgwelltime1;
out.avgwelltime2 = avgwelltime2;
out.avgwelltime3 = avgwelltime3;









% 
% % Output
% % ------
% out.index = sind;
% 
% 
% 
% 
% out.rip_spkshist_cell = rip_spkshist_cell;
% out.rip_spks_cell = rip_spks_cell;
% out.Nspikes = nspk; % Nspks in the entire -pret:postt window across all the events for real ripple events
% out.rdm_spkshist_cell = rdm_spkshist_cell;
% out.rdm_spks_cell = rdm_spks_cell;
% % Nspikes in respective window - summed response
% out.trialResps = trialResps;
% out.trialResps_rdm = trialResps_rdm;
% out.trialResps_bck = trialResps_bck;
% 
% % Nspikes summed across trials in response and bckgnd window
% out.Nspikes_resp = sum(trialResps);
% out.Nspikes_bck = sum(trialResps_bck);
% 
% out.pret = pret;
% out.postt = postt;
% out.binsize = binsize;
% out.rwin = rwin;
% out.bckwin = bwin;
% out.bins_resp  = bins_resp;
% out.bins_bck = bins_bck;
% out.timeaxis = timeaxis;
% 
% % Propoerties
% out.cellfr = cellfr;
% out.Nrip = cntrip;




