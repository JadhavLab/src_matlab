
% Add using a subset of neurons with defined spatial selectivvity to address reviewer comments
% ------------% ------------% ------------% ------------% ------------% ------------% ------------

% From PFcprop_corrs: only do the pairwaire correlations

% % ------------
% % PLOTTING, ETC
% % PFC Properties - from Demetris' code: DFS_DRsj_spatialvsripcorr.m
% % ------------


plotSWRcorr=1; % Plot swrcorr vs spatialcorr
savefig1=0;

%figdir = '/data25/sjadhav/HPExpt/Figures/2015_ReplayandPFCprop/SpatialCorrAndProperties';
figdir = '/data25/sjadhav/HPExpt/Figures/SpatialCorr/Subsample/';
% % ----FIG PROP --------------
forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1
figdir = '/data25/sjadhav/HPExpt/Figures/SpatialCorr/Subsample/'; summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
if forppr==1
    set(0,'defaultaxesfontsize',20);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end
% % ---------------------

val=0; area='PFC_SWRmod_noFS'; % matpairs original with FS removed, from DFS_DRsj_spatialvsripcorr_FSemoved.m
%val=6; area='Wtr_noiCA1';

%val=1; area='PFC_SWRmod'; % matpairs original, from DFS_DRsj_spatialvsripcorr.m
%val=2; area = 'PFC_SWRmod_nospeed'; % matpairs_nospeed_X6, from DFS_DRsj_spatialvsripcorr_PlotFields.m
%val=3; area = 'PFC_onlytheta'; % matpairs_PFConlytheta_X6, from DFS_DRsj_spatialvsripcorr_PFCcategories.m
%val=4; area = 'PFC_SWRandTheta'; % matpairs_PFC_SWRandTheta_X6, from DFS_DRsj_spatialvsripcorr_PFCcategories.m
%val=5; area = 'PFC_onlySWR';  %matpairs_PFC_onlySWR_X6, from DFS_DRsj_spatialvsripcorr_PFCcategories.m


if plotSWRcorr
    
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh Indices
    
    switch val
        case 0
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_noFS_X8.mat'; % Has SWRcorr and SpatCorr
        case 1
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs.mat'; % Has SWRcorr and SpatCorr
        case 2
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_nospeed_X6.mat'; % Has SWRcorr and SpatCorr: Spat Corr with no speed criterion
        case 3
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_PFConlytheta_X6.mat';
        case 4
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_PFC_SWRandTheta_X6.mat';
        case 5
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_PFConlySWR_X6.mat';
        case 6
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_Wtr_noiCA1_X8.mat';
    end
    
    % [An day CA1tet CA1cell PFCtet PFCcell SWRcorrPval SWRcorrRval SpatCorrRval]
    
    % 1.) Entire population: find sig SWR correlated pairs, and then pos or neg correlated
    % ------------------------------------------------------------------------------------
    
    allsigrippairs = matpairs(matpairs(:,7)<0.05,9);
    allnonsigrippairs = matpairs(matpairs(:,7)>0.05,9);
    mean(abs(allsigrippairs)), length(allsigrippairs)
    mean(abs(allnonsigrippairs)), length(allnonsigrippairs)
    
    possigrippairs = matpairs(matpairs(:,7)<0.05 & matpairs(:,8) > 0,9);
    negsigrippairs = matpairs(matpairs(:,7)<0.05 & matpairs(:,8) < 0,9);
    nonsigrippairs = matpairs(matpairs(:,7)>0.05, 9);
    
    % Get sig SWR pair idxs
    sigrippairs_idx = find(matpairs(:,7)<0.05);
    
    % 2.) All Exc vs Inh vs Non-Modulated Neurons
    % -------------------------------------------
    % Get appropriate indices in matpairs. Can loop and use ismember, or just use intersect
    
    matpairs_PFC = matpairs(:,[1 2 5 6]);
    % [~,Exc_ind,ib_exc] = intersect(matpairs_PFC,PFCindsExc,'rows');
    % [~,Inh_ind,ib_inh] = intersect(matpairs_PFC,PFCindsInh,'rows');
    % Mod_ind = [Exc_ind;Inh_ind];
    % Neu_ind=[1:length(matpairs_PFC)];
    % Neu_ind(Mod_ind)=[];
    
    cntexccells=0; cntexcpairs=0; Excind_inpairs=[];
    for i=1:size(PFCindsExc,1)
        ind=[]; curridx=[];
        cntexccells = cntexccells+1;
        curridx = PFCindsExc(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntexcpairs = cntexcpairs+length(ind);
        Excind_inpairs=[Excind_inpairs;ind'];
    end
    cntinhcells=0; cntinhpairs=0; Inhind_inpairs=[];
    for i=1:size(PFCindsInh,1)
        ind=[]; curridx=[];
        cntinhcells = cntinhcells+1;
        curridx = PFCindsInh(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntinhpairs = cntinhpairs+length(ind);
        Inhind_inpairs=[Inhind_inpairs;ind'];
    end
    
    Modind_inpairs = [Excind_inpairs;Inhind_inpairs];
    Neuind_inpairs = [1:length(matpairs_PFC)];
    Neuind_inpairs(Modind_inpairs) = [];
    
    excrippairs = matpairs(Excind_inpairs,9);
    inhrippairs = matpairs(Inhind_inpairs,9);
    neurippairs = matpairs(Neuind_inpairs, 9);
    
    
    %     %3.) Excited vs Inh Significant SWR corr neurons
    matpairs_Exc = matpairs(Excind_inpairs,:);
    matpairs_Inh = matpairs(Inhind_inpairs,:);
    matpairs_Neu = matpairs(Neuind_inpairs,:);
    
    excsigrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05,9);
    inhsigrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 ,9);
    neusigrippairs = matpairs_Neu(matpairs_Neu(:,7)<0.05, 9);
    
    
    %     %4.) Excited vs Inh Significant +ve or -ve SWR corr neurons
    %
    excsigposrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05 & matpairs_Exc(:,8) > 0,9);
    excsignegrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05 & matpairs_Exc(:,8) < 0,9);
    excnonsigrippairs = matpairs_Exc(matpairs_Exc(:,7)>0.05,9);
    inhsigposrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 & matpairs_Inh(:,8) > 0,9);
    inhsignegrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 & matpairs_Inh(:,8) < 0,9);
    inhnonsigrippairs = matpairs_Inh(matpairs_Inh(:,7)>0.05,9);
    %Neusigrippairs = matpairs_Neu(matpairs_Neu(:,7)<0.05, 9);
    
    % Get indices of sig SWR pairs for exc and inh separately
    excsigrippairs_idx = find(matpairs_Exc(:,7)<0.05);
    inhsigrippairs_idx = find(matpairs_Inh(:,7)<0.05);
    
    
end
    
    
    %*) Scatter: Exc vs Inh
    figure; hold on;
    subplot(1,2,1)
    scatter(matpairs_Exc(:,8), matpairs_Exc(:,9),'.k'); hold on;
    scatter(matpairs_Exc(excsigrippairs_idx,8), matpairs_Exc(excsigrippairs_idx,9),'.r'); hold on;
    
    [b,bint,r,rint,stats] = regress(matpairs_Exc(:,9), [ones(size(matpairs_Exc(:,8))) matpairs_Exc(:,8)]);
    plot(min(matpairs_Exc(:,8)):.1:max(matpairs_Exc(:,8)), b(1)+b(2)*[min(matpairs_Exc(:,8)):.1:max(matpairs_Exc(:,8))],'r')
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Exc-PFC: r^2(%0.3f) p(%0.3f)', stats(1), stats(3))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    subplot(1,2,2)
    scatter(matpairs_Inh(:,8), matpairs_Inh(:,9),'.k'); hold on;
    scatter(matpairs_Inh(inhsigrippairs_idx,8), matpairs_Inh(inhsigrippairs_idx,9),'.b'); hold on;
    
    [b,bint,r,rint,stats] = regress(matpairs_Inh(:,9), [ones(size(matpairs_Inh(:,8))) matpairs_Inh(:,8)]);
    plot(min(matpairs_Inh(:,8)):.1:max(matpairs_Inh(:,8)), b(1)+b(2)*[min(matpairs_Inh(:,8)):.1:max(matpairs_Inh(:,8))])
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Inh-PFC: r^2(%0.3f) p(%0.3f)', stats(1), stats(3))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    figfile = [figdir,'SpatialvsSWRcorr_ExcInhScatter_color_X8',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-dpdf', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    





[rexc, pexc] = corrcoef(matpairs_Exc(:,8), matpairs_Exc(:,9))
[rinh, pinh] = corrcoef(matpairs_Inh(:,8), matpairs_Inh(:,9))

keyboard;


% How many neurons participated in the pairs:
allsigpair_PFC = matpairs(sigrippairs_idx,[1,5,6]);
PFC_cells = unique(allsigpair_PFC,'rows');
nPFC = length(PFC_cells)
allsigpair_CA1 = matpairs(sigrippairs_idx,[1,3,4]);
CA1_cells = unique(allsigpair_CA1,'rows');
nCA1 = length(CA1_cells)


excsigpair_PFC = matpairs_Exc(excsigrippairs_idx,[1,5,6]);
PFC_cellsexc = unique(excsigpair_PFC,'rows');
nPFCexc = length(PFC_cellsexc)
excsigpair_CA1 = matpairs_Exc(excsigrippairs_idx,[1,3,4]);
CA1_cellsexc = unique(excsigpair_CA1,'rows');
nCA1exc = length(CA1_cellsexc)

inhsigpair_PFC = matpairs_Inh(inhsigrippairs_idx,[1,5,6]);
PFC_cellsinh = unique(inhsigpair_PFC,'rows');
nPFCinh = length(PFC_cellsinh)
inhsigpair_CA1 = matpairs_Inh(inhsigrippairs_idx,[1,3,4]);
CA1_cellsinh = unique(inhsigpair_CA1,'rows');
nCA1inh = length(CA1_cellsinh)


% Shuffle test between excited and inhibited

xexc = matpairs_Exc(:,8); yexc =  matpairs_Exc(:,9);
xinh = matpairs_Inh(:,8); yinh =  matpairs_Inh(:,9);

% First, try a shuffle for excited and inhibited separately to assess their individual significance

% % Shuffling Excited
% % ----------------
disp('Exc')
allmodln = xexc; allripmodln = yexc;
[bexc,bintexc,rexc,rintexc,statsexc] = regress(allripmodln, [ones(size(allmodln)) allmodln]);

for n=1:1000
    rorder = randperm(length(allmodln));
    randmodln = allmodln(rorder);
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, allripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(allripmodln, [ones(size(randmodln)) randmodln]);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end

% Significance from shuffle
rsquare_exc = statsexc(1)
rsq_shuf99_exc = prctile(rsquare_shuffle,99),
%figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
[r2,p2] = corrcoef(allmodln,allripmodln);
r_exc = r2(1,2),
r_shuf99_exc = prctile(r_shuffle,99),
pshuf_r_exc = length(find(r2(1,2)<r_shuffle))/n



% % Shuffling Inhibted
% % ------------------
disp('Inh')
allmodln = xinh; allripmodln = yinh;
[binh,bintinh,rinh,rintinh,statsinh] = regress(allripmodln, [ones(size(allmodln)) allmodln]);

for n=1:1000
    rorder = randperm(length(allmodln));
    randmodln = allmodln(rorder);
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, allripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(allripmodln, [ones(size(randmodln)) randmodln]);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end

% Significance from shuffle
rsquare_inh = statsinh(1)
rsq_shuf99_inh = prctile(rsquare_shuffle,99),
%figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
[r2,p2] = corrcoef(allmodln,allripmodln);
r_inh = r2(1,2),
r_shuf99_inh = prctile(r_shuffle,99),
pshuf_r_inh = length(find(r2(1,2)<r_shuffle))/n





% Mix excited and inhibited, shuffle 1000 times, and draw samples equal to length(inh) for each shuffle
% Compare r_inh with r_shuf from this distribution/ or rsquare instead
% -----------------------------------------------------------------------------------------------------
x=[xexc;xinh];
y=[yexc;yinh];
ninh = length(yinh); nexc = length(yexc);

disp('Mix ExcInh: Whether Inhibited is more correlated than mixed population')
allmodln = x; allripmodln = y;
[b00,bint00,r00,rint00,stats00] = regress(allripmodln, [ones(size(allmodln)) allmodln]);
rsquare00 = stats00(1)

for n=1:1000
    rorder = randperm(nexc+ninh); % Randomly pick ninh points out of nexc+ninh distribution
    randmodln = allmodln(rorder(1:ninh)); randripmodln = allripmodln(rorder(1:ninh));
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, randripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(randripmodln, [ones(size(randmodln)) randmodln]);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end


rsquare_inh, prc99_rsqshuf = prctile(rsquare_shuffle,99), prc95_rsqshuf = prctile(rsquare_shuffle,95),
r_inh, prc99_rshuf = prctile(r_shuffle,99),  prc95_rshuf = prctile(r_shuffle,95),

pinh_r_shuf = length(find(r_inh<=r_shuffle))./n   % p=0.004-0.007      % p = 0.04 / for X7: p=0.016/ for X8: 0.007
pinh_rsq_shuf = length(find(rsquare_inh<=rsquare_shuffle))./n   %p=0.004-0.007



disp('Mix ExcInh: Whether Excited is less correlated than mixed population')

rshuffle=[]; rsquare_shuffle=[];
for n=1:1000
    rorder = randperm(nexc+ninh); % Randomly pick ninh points out of nexc+ninh distribution
    randmodln = allmodln(rorder(1:nexc)); randripmodln = allripmodln(rorder(1:nexc));
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, randripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(randripmodln, [ones(size(randmodln)) randmodln]);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end


rsquare_exc, prc99_rsqshuf = prctile(rsquare_shuffle,99), prc95_rsqshuf = prctile(rsquare_shuffle,95),
r_exc, prc99_rshuf = prctile(r_shuffle,99), prc95_rshuf = prctile(r_shuffle,95),

pexc_r_shuf = length(find(r_exc>=r_shuffle))./n    % p=0.01        p = 0.09 / 0.065 for X7/ for X8: 0.01
pexc_rsq_shuf = length(find(rsquare_exc>=rsquare_shuffle))./n  % p=0.01














% ------------------------------------------------------------------
% --------------------
% COmpare to spatial
% --------------------
matpairs_PFCExc = matpairs_Exc(:,[1 2 5 6]);
matpairs_PFCInh = matpairs_Inh(:,[1 2 5 6]);
load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_SpatialSparsityInds_noFS_X8.mat';

cntexccells_spat=0; cntexcpairs_spat=0; Excind_inpairs_spat=[];
ripcorrExc=[]; spatcorrExc=[]; spatExc=[];
for i=1:size(pfcEXCinds_sort,1)
    currspatcov = pfcEXCSpar_sort(i);
    if~isnan(currspatcov)
        ind=[]; curridx=[];
        cntexccells_spat = cntexccells_spat+1;
        curridx = pfcEXCinds_sort(i,:);
        ind=find(ismember(matpairs_PFCExc,curridx,'rows'))';
        ripcorrExc = [ripcorrExc; matpairs_Exc(ind,8)];
        spatcorrExc = [spatcorrExc; matpairs_Exc(ind,9)];
        currspatcov_vec = repmat(currspatcov,length(ind),1);
        spatExc = [spatExc; currspatcov_vec];
        cntexcpairs_spat = cntexcpairs_spat+length(ind);
        Excind_inpairs_spat=[Excind_inpairs_spat;ind'];
    end
end

cntinhcells_spat=0; cntinhpairs_spat=0; Inhind_inpairs_spat=[];
ripcorrInh=[]; spatcorrInh=[]; spatInh=[];
for i=1:size(pfcINHinds_sort,1)
    currspatcov = pfcINHSpar_sort(i);
    if~isnan(currspatcov)
        ind=[]; curridx=[];
        cntinhcells_spat = cntinhcells_spat+1;
        curridx = pfcINHinds_sort(i,:);
        ind=find(ismember(matpairs_PFCInh,curridx,'rows'))';
        ripcorrInh = [ripcorrInh; matpairs_Inh(ind,8)];
        spatcorrInh = [spatcorrInh; matpairs_Inh(ind,9)];
        currspatcov_vec = repmat(currspatcov,length(ind),1);
        spatInh = [spatInh; currspatcov_vec];
        cntinhpairs_spat = cntinhpairs_spat+length(ind);
        Inhind_inpairs_spat=[Inhind_inpairs_spat;ind'];
    end
end

[prctile(spatInh,25), median(spatInh),  prctile(spatInh,75)]
[prctile(spatExc,25), median(spatExc), prctile(spatExc,75)]

% subsamlpe based on spatial selectivity in Inh
useinh = find( spatInh >=prctile(spatInh,25) & spatInh <=prctile(spatInh,75)); %184/320
useexc = find( spatExc >=prctile(spatInh,25) & spatExc <=prctile(spatInh,75)); %203/320

% THESE ARE THE NEW VALUES
xexc =  ripcorrExc(useexc); yexc =  spatcorrExc(useexc);
xinh =  ripcorrInh(useinh); yinh =  spatcorrInh(useinh);

% DO original correlations and new correlations, and compare. Also do old and new plots

[rexc, pexc] = corrcoef(ripcorrExc, spatcorrExc)  %0.19 // 0.0002
[rinh, pinh] = corrcoef(ripcorrInh, spatcorrInh) % 0.36, 0.0000

[rexcsub, pexcsub] = corrcoef(xexc, yexc) %0.33 / p=0
[rinhsub, pinhsub] = corrcoef(xinh, yinh) % 0.39 or 0.4/ p =0

% Figure Original
% -----------------
figure; hold on;
subplot(1,2,1)
scatter(ripcorrExc, spatcorrExc,'.k'); hold on;
scatter(matpairs_Exc(excsigrippairs_idx,8), matpairs_Exc(excsigrippairs_idx,9),'.r'); hold on;

[b,bint,r,rint,stats] = regress(spatcorrExc, [ones(size(ripcorrExc)) ripcorrExc]);
plot(min(ripcorrExc):.1:max(ripcorrExc), b(1)+b(2)*[min(ripcorrExc):.1:max(ripcorrExc)],'r')
xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Exc-PFC: r(%0.2f) p(%0.3f)', rexc(2), stats(3))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])

subplot(1,2,2)
scatter(ripcorrInh, spatcorrInh,'.k'); hold on;
scatter(matpairs_Inh(inhsigrippairs_idx,8), matpairs_Inh(inhsigrippairs_idx,9),'.b'); hold on;

[b,bint,r,rint,stats] = regress(spatcorrInh, [ones(size(ripcorrInh)) ripcorrInh]);
plot(min(ripcorrInh):.1:max(ripcorrInh), b(1)+b(2)*[min(ripcorrInh):.1:max(ripcorrInh)])
xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Inh-PFC: r(%0.2f) p(%0.3f)', rinh(2), stats(3))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])

%figfile = [figdir,'SpatialvsSWRcorr_ExcInhScatter_color_X8',area]
%if savefig1==1,
%    print('-depsc2', figfile); print('-dpdf', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%end

% Figure Subsample
% -----------------
figure; hold on;
subplot(1,2,1)
scatter(xexc, yexc,'.k'); hold on;
%scatter(matpairs_Exc(excsigrippairs_idx,8), matpairs_Exc(excsigrippairs_idx,9),'.r'); hold on;

[b,bint,r,rint,stats] = regress(yexc, [ones(size(xexc)) xexc]);
plot(min(xexc):.1:max(xexc), b(1)+b(2)*[min(xexc):.1:max(xexc)],'r')
xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Exc-PFC: r(%0.2f) p(%0.3f)', rexcsub(2), stats(3))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])

subplot(1,2,2)
scatter(xinh, yinh,'.k'); hold on;
%scatter(matpairs_Inh(inhsigrippairs_idx,8), matpairs_Inh(inhsigrippairs_idx,9),'.b'); hold on;

[b,bint,r,rint,stats] = regress(yinh, [ones(size(xinh)) xinh]);
plot(min(xinh):.1:max(xinh), b(1)+b(2)*[min(xinh):.1:max(xinh)])
xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Inh-PFC: r^2(%0.2f) p(%0.3f)', rinhsub(2), stats(3))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])

figfile = [figdir,'SpatialvsSWRcorr_ExcInhScatter_color_X8_Subsample',area]
if savefig1==1,
    print('-depsc2', figfile); print('-dpdf', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
end


keyboard;



% Mix excited and inhibited, shuffle 1000 times, and draw samples equal to length(inh) for each shuffle
% Compare r_inh with r_shuf from this distribution/ or rsquare instead
% -----------------------------------------------------------------------------------------------------
x=[xexc;xinh];
y=[yexc;yinh];
ninh = length(yinh); nexc = length(yexc);

disp('Mix ExcInh: Whether Inhibited is more correlated than mixed population')
allmodln = x; allripmodln = y;
[b00,bint00,r00,rint00,stats00] = regress(allripmodln, [ones(size(allmodln)) allmodln]);
rsquare00 = stats00(1)

for n=1:1000
    rorder = randperm(nexc+ninh); % Randomly pick ninh points out of nexc+ninh distribution
    randmodln = allmodln(rorder(1:ninh)); randripmodln = allripmodln(rorder(1:ninh));
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, randripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(randripmodln, [ones(size(randmodln)) randmodln]);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end


rsquare_inh, prc99_rsqshuf = prctile(rsquare_shuffle,99), prc95_rsqshuf = prctile(rsquare_shuffle,95),
r_inh, prc99_rshuf = prctile(r_shuffle,99),  prc95_rshuf = prctile(r_shuffle,95),

pinh_r_shuf = length(find(r_inh<=r_shuffle))./n    % p = 0.182
pinh_rsq_shuf = length(find(rsquare_inh<=rsquare_shuffle))./n  % p=0.182



disp('Mix ExcInh: Whether Excited is less correlated than mixed population')

rshuffle=[]; rsquare_shuffle=[];
for n=1:1000
    rorder = randperm(nexc+ninh); % Randomly pick ninh points out of nexc+ninh distribution
    randmodln = allmodln(rorder(1:nexc)); randripmodln = allripmodln(rorder(1:nexc));
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, randripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(randripmodln, [ones(size(randmodln)) randmodln]);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end


rsquare_exc, prc99_rsqshuf = prctile(rsquare_shuffle,99), prc95_rsqshuf = prctile(rsquare_shuffle,95),
r_exc, prc99_rshuf = prctile(r_shuffle,99), prc95_rshuf = prctile(r_shuffle,95),

pexc_r_shuf = length(find(r_exc>=r_shuffle))./n    % p = 0.25
pexc_rsq_shuf = length(find(rsquare_exc>=rsquare_shuffle))./n  % p=0.25





