% % ------------
% % PLOTTING, ETC
% % PFC Properties - from Demetris' code: DFS_DRsj_spatialvsripcorr.m
% % ------------

makeCA1neuInds=0;
makePFCneuInds=0;
gatherGlobalTraj = 0; % From global traj, get PFC ones for Exc, Inh, and Neu - just like gatherPE above
% Also get allRunTraj - separated by arm, but in and out combined
gatherPE=0; % organize the PE values into something managebale for plotting

plotPE_armidx=0; % plot PE vs armidx
plotDirnIdx=0; % Plot Dirn Idx spread in poln - can also compare to Arm Idx and PE
%plotPE_replayidx=0; %plot PE vs replayidx
plotSWRcorr=1; % Plot swrcorr vs spatialcorr

figdir = '/data25/sjadhav/HPExpt/ProcessedData/SpatialCorrAndProperties';
% % ----FIG PROP --------------
forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1
figdir = '/data25/sjadhav/HPExpt/ProcessedData/SpatialCorrAndProperties'; summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
if forppr==1
    set(0,'defaultaxesfontsize',20);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end
% % ---------------------

if makeCA1neuInds % Get CA1 Inds - All Neurons
    load ('/data25/sjadhav/HPExpt/HP_ProcessedData/HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6.mat','allripplemod');
    
    CA1inds = [];
    for i=1:length(allripplemod)
        CA1inds = [CA1inds; allripplemod(i).index];
    end
    savefile =  '/data25/sjadhav/HPExpt/HP_ProcessedData/CA1Inds_X6.mat';
    save(savefile,'CA1inds');
end % end makePFCneuInds


if makePFCneuInds
    load ('/data25/sjadhav/HPExpt/HP_ProcessedData/HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6.mat','allripplemod');
    % Get PFCindsExc, PFCindsInh
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/swrmodinds_Jan27th.mat'; % PFC Exc-Inh Indices
    
    PFCindsNeu = []; PFCindsExc_Redo = []; PFCindsInh_Redo = [];
    for i=1:length(allripplemod)
        if allripplemod(i).rasterShufP2>=0.05   % Get non-modulated indices
            PFCindsNeu = [PFCindsNeu; allripplemod(i).index];
        else %Re-check Exc-Inh indices
            if strcmp(allripplemod(i).type, 'exc')
                PFCindsExc_Redo = [PFCindsExc_Redo; allripplemod(i).index];
            else
                PFCindsInh_Redo = [PFCindsInh_Redo; allripplemod(i).index];
            end
        end
    end
    
    savefile =  '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat';
    save(savefile,'PFCindsExc','PFCindsInh','PFCindsNeu','PFCindsExc_Redo','PFCindsInh_Redo');
end % end makePFCneuInds



if gatherGlobalTraj
    %fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/GlobalPEtraj_X6.mat';
    %load(fname,'allGlobalPEtraj','allGlobalIdxs', 'allGlobalPE');
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/GlobalPEtraj_Runtraj_X6.mat';
    load(fname,'allGlobalPEtraj','allGlobalIdxs', 'allGlobalPE','allRunTraj');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh-Neu Indices
    
    % a) First just get allPFC ones, instead of separating in Exc, Inh, Neu
    % This will get values for PFC, skipping all CA1 ones
    % You can separate while comparing with replay response directly
    PFCindsAll = [PFCindsExc;PFCindsInh;PFCindsNeu]; % All PFC mod
    PFCGlobalPEtraj=[]; PFCGlobalIdxs=[]; PFCGlobalPE=[]; pfcmatch=[];
    PFCRunTraj=[];
    cnt=0;
    for i = 1:length(PFCindsAll)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsAll(i,:), 'rows'));
        
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCGlobalIdxs = [PFCGlobalIdxs; PFCindsAll(i,:)];
            PFCGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % Also Separate Exc, Inh, Neu, just in case
    % b) PFCexc
    PFCexcGlobalPEtraj=[]; PFCexcGlobalIdxs=[]; PFCexcGlobalPE=[]; pfcmatch=[];
    cnt=0;
    for i = 1:length(PFCindsExc)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsExc(i,:), 'rows'));
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCexcGlobalIdxs = [PFCexcGlobalIdxs; PFCindsExc(i,:)];
            PFCexcGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCexcGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCexcRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % c) PFCinh
    PFCinhGlobalPEtraj=[]; PFCinhGlobalIdxs=[]; PFCinhGlobalPE=[]; pfcmatch=[];
    cnt=0;
    for i = 1:length(PFCindsInh)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsInh(i,:), 'rows'));
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCinhGlobalIdxs = [PFCinhGlobalIdxs; PFCindsInh(i,:)];
            PFCinhGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCinhGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCinhRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % d) PFCneu
    PFCneuGlobalPEtraj=[]; PFCneuGlobalIdxs=[]; PFCneuGlobalPE=[]; pfcmatch=[];
    cnt=0;
    for i = 1:length(PFCindsNeu)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsNeu(i,:), 'rows'));
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCneuGlobalIdxs = [PFCneuGlobalIdxs; PFCindsNeu(i,:)];
            PFCneuGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCneuGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCneuRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_GlobalPEtraj_Runtraj_X6.mat'
    save(savefile,'PFCGlobalIdxs','PFCGlobalPEtraj','PFCGlobalPE','PFCexcGlobalIdxs','PFCexcGlobalPEtraj','PFCexcGlobalPE', ...
        'PFCinhGlobalIdxs','PFCinhGlobalPEtraj','PFCinhGlobalPE','PFCneuGlobalIdxs','PFCneuGlobalPEtraj','PFCneuGlobalPE', ...
        'PFCRunTraj','PFCexcRunTraj','PFCinhRunTraj','PFCneuRunTraj');
    
end % end gatherGlobalTraj




if gatherPE
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/DR_Proc_X6.mat';
    load(fname,'PEcoefallv2INDS','PEcoefallv2','sparsityALLdata');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh-Neu Indices
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/CA1Inds_X6.mat'; % CA1 Indices: all
    
    pfcEXCPE=[]; pfcINHPE=[]; pfcNEUPE=[]; skippedPE=[]; usePECval = 3; %3 is norm PE2, 11 is norm PE1
    pfcEXCPEinds=[]; pfcINHPEinds=[]; pfcNEUPEinds=[];
    % Get all the PE values for Exc and Inh PFC
    for i = 1:length(PFCindsExc);
        try%skip cells without PEC
            %              mean(pfcINHPE)   pfcexcmatch = find(ismember(PEcoefall(:,1:4), PFCindsExc(i,:), 'rows'));
            pfcexcmatch = find(ismember(PEcoefallv2INDS(:,1:4), PFCindsExc(i,:), 'rows'));
            %                 pfcEXCPE(i,:) = PEcoefallv2{pfcexcmatch,3}';
            pfcEXCPE(i,:) = mean([PEcoefallv2{pfcexcmatch,usePECval}']); % mean across the two PE values
            pfcEXCPEinds(i,:) = PEcoefallv2{pfcexcmatch,1};
            %             if PEcoefallv2{pfcexcmatch,1}(1,[1:4]) ~= sparsityALLdata{pfcexcmatch,1};
            %                 keyboard %pause if the sparsity data and the PE data don't have matching indices
            %             end
            %             pfcEXCSpar(i,:) = [sparsityALLdata{pfcexcmatch,2} nanmean(pfcEXCPE(i,[1:2]))];
        catch
            %keyboard;
            skippedPE = [skippedPE; PFCindsExc(i,:)];
        end
    end
    
    for i = 1:length(PFCindsInh);
        try %skip cells without PEC
            %                 pfcinhmatch = find(ismember(PEcoefall(:,1:4), PFCindsInh(i,:), 'rows'));
            %                 pfcINHPE(i) = PEcoefall(pfcinhmatch,5);
            pfcinhmatch = find(ismember(PEcoefallv2INDS(:,1:4), PFCindsInh(i,:), 'rows'));
            %                 pfcINHPE(i,:) = PEcoefallv2{pfcinhmatch,3}';
            pfcINHPE(i,:) = mean([PEcoefallv2{pfcinhmatch,usePECval}']);
            pfcINHPEinds(i,:) = PEcoefallv2{pfcinhmatch,1};
            %             if PEcoefallv2{pfcinhmatch,1}(1,[1:4]) ~= sparsityALLdata{pfcinhmatch,1};
            %                 keyboard %pause if the sparsity data and the PE data don't have matching indices
            %             end
            %             pfcINHSpar(i,:) = [sparsityALLdata{pfcinhmatch,2} nanmean(pfcINHPE(i,[1:2]))];
        catch
            %keyboard;
            skippedPE = [skippedPE; PFCindsInh(i,:)];
        end
    end
    
    for i = 1:length(PFCindsNeu);
        try %skip cells without PEC
            %                 pfcinhmatch = find(ismember(PEcoefall(:,1:4), PFCindsInh(i,:), 'rows'));
            %                 pfcINHPE(i) = PEcoefall(pfcinhmatch,5);
            pfcneumatch = find(ismember(PEcoefallv2INDS(:,1:4), PFCindsNeu(i,:), 'rows'));
            %                 pfcINHPE(i,:) = PEcoefallv2{pfcinhmatch,3}';
            pfcNEUPE(i,:) = mean([PEcoefallv2{pfcneumatch,usePECval}']);
            pfcNEUPEinds(i,:) = PEcoefallv2{pfcneumatch,1};
            %             if PEcoefallv2{pfcinhmatch,1}(1,[1:4]) ~= sparsityALLdata{pfcinhmatch,1};
            %                 keyboard %pause if the sparsity data and the PE data don't have matching indices
            %             end
            %             pfcINHSpar(i,:) = [sparsityALLdata{pfcinhmatch,2} nanmean(pfcINHPE(i,[1:2]))];
        catch
            %keyboard;
            skippedPE = [skippedPE; PFCindsNeu(i,:)];
        end
    end
    
    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_PE_X6.mat';
    save(savefile,'pfcEXCPE','pfcINHPE','pfcNEUPE','pfcEXCPEinds','pfcINHPEinds','pfcNEUPEinds');
    
    % Get CA1 PE as well
    CA1PE=[]; skippedCA1PE=[]; CA1PEinds=[]; 
    % Get all the PE values for Exc and Inh PFC
    for i = 1:length(CA1inds);
        try%skip cells without PEC
            %              mean(pfcINHPE)   pfcexcmatch = find(ismember(PEcoefall(:,1:4), PFCindsExc(i,:), 'rows'));
            match = find(ismember(PEcoefallv2INDS(:,1:4), CA1inds(i,:), 'rows'));
            %                 pfcEXCPE(i,:) = PEcoefallv2{pfcexcmatch,3}';
            CA1PE(i,:) = mean([PEcoefallv2{match,usePECval}']); % mean across the two PE values
            CA1PEinds(i,:) = PEcoefallv2{match,1};
            %             if PEcoefallv2{pfcexcmatch,1}(1,[1:4]) ~= sparsityALLdata{pfcexcmatch,1};
            %                 keyboard %pause if the sparsity data and the PE data don't have matching indices
            %             end
            %             pfcEXCSpar(i,:) = [sparsityALLdata{pfcexcmatch,2} nanmean(pfcEXCPE(i,[1:2]))];
        catch
            %keyboard;
            skippedCA1PE = [skippedCA1PE; CA1inds(i,:)];
        end
    end
    
    
    figure; hold on;
    plot(1, nanmean(pfcEXCPE),'ro');  errorbar2(1, nanmean(pfcEXCPE), nanstderr(pfcEXCPE), 0.3, 'r');
    plot(2, nanmean(pfcINHPE),'bo');  errorbar2(2, nanmean(pfcINHPE), nanstderr(pfcINHPE), 0.3, 'b');
    plot(3, nanmean(pfcNEUPE),'go');  errorbar2(3, nanmean(pfcNEUPE), nanstderr(pfcNEUPE), 0.3, 'g');
    plot(4, nanmean(CA1PE),'ko');  errorbar2(4, nanmean(CA1PE), nanstderr(CA1PE), 0.3, 'k');

    %bar([mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)]); hold on;
    %errorbar2([1 2 3], [mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)],  [stderr(pfcEXCPE) stderr(pfcINHPE) stderr(pfcNEUPE)] , 0.3, 'k')
    ylim([0 1]);
    title('PE for PFC and CA1 neurons ','FontSize',24,'FontWeight','normal');
    ylabel('Path Equivalence','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3 4]); set(gca,'XTickLabel',{'Exc','Inh','Neu','CA1'});
    
    [pKW table statsKW] = kruskalwallis([pfcEXCPE' pfcINHPE' pfcNEUPE' CA1PE'], [ones(1, length(pfcEXCPE)) ones(1,length(pfcINHPE)).*2 ...
        ones(1,length(pfcNEUPE)).*3 ones(1,length(CA1PE)).*4]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    [pKW table statsKW] = kruskalwallis([pfcEXCPE' pfcINHPE' pfcNEUPE'], [ones(1, length(pfcEXCPE)) ones(1,length(pfcINHPE)).*2 ...
        ones(1,length(pfcNEUPE)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    
    %reshape ripmodpfc - Both epochs in there. so need avg
    %pfcEXCPE = reshape(pfcEXCPE(:,[1:2]), 2*length(pfcEXCPE(:,1)),1);
    %pfcINHPE = reshape(pfcINHPE(:,[1:2]),2*length(pfcINHPE(:,1)),1);
    
end




if plotDirnIdx
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_PFC_DirnIdx_gather_X6.mat';
    load(fname,'all_DI_PFCidxs','all_DI_PFCDirnIdx');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh-Neu Indices
    PFCmod = [PFCindsExc;PFCindsInh]; % All PFC mod
    
    % Plot just DirnIdx
    excind = []; inhind = []; neuind = [];
    for i=1:size(all_DI_PFCidxs,1)
        excmatch = find(ismember(all_DI_PFCidxs(i,:), PFCindsExc, 'rows'));
        if ~isempty(excmatch)
            excind = [excind; i];
        else
            inhmatch = find(ismember(all_DI_PFCidxs(i,:), PFCindsInh, 'rows'));
            if ~isempty(inhmatch)
                inhind = [inhind; i];
            else
                neumatch = find(ismember(all_DI_PFCidxs(i,:), PFCindsNeu, 'rows'));
                if ~isempty(neumatch)
                    neuind = [neuind; i];
                end
            end
        end
    end
    
    exc_Dirnidx = all_DI_PFCDirnIdx(excind);
    inh_Dirnidx = all_DI_PFCDirnIdx(inhind);
    neu_Dirnidx = all_DI_PFCDirnIdx(neuind);
    
    figure; hold on;
    plot(1, nanmean(exc_Dirnidx),'ro');  errorbar2(1, nanmean(exc_Dirnidx), nanstderr(exc_Dirnidx), 0.3, 'r');
    plot(2, nanmean(inh_Dirnidx),'bo');  errorbar2(2, nanmean(inh_Dirnidx), nanstderr(inh_Dirnidx), 0.3, 'b');
    plot(3, nanmean(neu_Dirnidx),'ko');  errorbar2(3, nanmean(neu_Dirnidx), nanstderr(neu_Dirnidx), 0.3, 'k');
    ylim([-1 1]);
    title('Dirn Idx for PFC neurons ','FontSize',24,'FontWeight','normal');
    ylabel('Dirn Idx','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3]); set(gca,'XTickLabel',{'Exc','Inh','Neu'});
    
    %[pKW table statsKW] = kruskalwallis([exc_Dirnidx inh_Dirnidx neu_Dirnidx], [ones(1, length(exc_Dirnidx)) ones(1,length(inh_Dirnidx)).*2 ones(1,length(neu_Dirnidx)).*3]);
    %        [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range
    
    
            
    % Plot PE vs DirnIdx
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_PE_X6.mat'
    excmatch=[]; inhmatch=[]; neumatch=[];
    excDI=[]; inhDI = []; neuDI=[]; excPE=[]; inhPE=[]; neuPE=[];
    
    for i=1:size(all_DI_PFCidxs,1)
        excmatch = find(ismember(pfcEXCPEinds, all_DI_PFCidxs(i,:), 'rows'));
        if ~isempty(excmatch)
            excDI = [excDI; all_DI_PFCDirnIdx(i)];
            excPE = [excPE; pfcEXCPE(excmatch)];
        else
            inhmatch = find(ismember( pfcINHPEinds, all_DI_PFCidxs(i,:), 'rows'));
            if ~isempty(inhmatch)
                inhDI = [inhDI; all_DI_PFCDirnIdx(i)];
                inhPE = [inhPE; pfcINHPE(inhmatch)];
            else
                neumatch = find(ismember(pfcNEUPEinds, all_DI_PFCidxs(i,:), 'rows'));
                if ~isempty(neumatch)
                    neuDI = [neuDI; all_DI_PFCDirnIdx(i)];
                    neuPE = [neuPE; pfcNEUPE(neumatch)];
                end
            end
        end
    end
    
    rem = find(isnan(excPE) | isnan(excDI)); excDI(rem)=[]; excPE(rem)=[];
    rem = find(isnan(inhPE) | isnan(inhDI)); inhDI(rem)=[]; inhPE(rem)=[];
    rem = find(isnan(neuPE) | isnan(neuDI)); neuDI(rem)=[]; neuPE(rem)=[];
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(excPE,excDI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(excPE));
        randPE = excPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,excDI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inhPE,inhDI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inhPE));
        randPE = inhPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,inhDI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neuPE,neuDI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neuPE));
        randPE = neuPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,neuDI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    
    figure; hold on;
    subplot(2,2,1);
    scatter(excPE, excDI, 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(excDI, [ones(size(excPE)) excPE]);
    plot(min(excPE):.01:max(excPE), b(1)+b(2)*[min(excPE):.01:max(excPE)],'r')
    axis([0 1 -1 1]);
    ylabel('DirnIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inhPE, inhDI, 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(inhDI, [ones(size(inhPE)) inhPE]);
    plot(min(inhPE):.01:max(inhPE), b(1)+b(2)*[min(inhPE):.01:max(inhPE)],'b')
    axis([0 1 -1 1]);
    ylabel('DirnIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neuPE, neuDI, 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(neuDI, [ones(size(neuPE)) neuPE]);
    plot(min(neuPE):.01:max(neuPE), b(1)+b(2)*[min(neuPE):.01:max(neuPE)],'k')
    axis([0 1 -1 1]);
    ylabel('DirnIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
    
    % Plot Dirn Idx vs Arm Idx
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_PFC_ArmIdx_gather_X6.mat';
    load(fname,'all_AI_PFCidxs','all_AI_PFCArmIdx');
    
    % NOTE: AI and DI idxs are the same! 
    exc_armidx = all_AI_PFCArmIdx(excind);
    inh_armidx = all_AI_PFCArmIdx(inhind);
    neu_armidx = all_AI_PFCArmIdx(neuind);
    
    rem = find(isnan(exc_armidx) | isnan(exc_Dirnidx)); exc_armidx(rem)=[]; exc_Dirnidx(rem)=[];
    rem = find(isnan(inh_armidx) | isnan(inh_Dirnidx)); inh_armidx(rem)=[]; inh_Dirnidx(rem)=[];
    rem = find(isnan(neu_armidx) | isnan(neu_Dirnidx)); neu_armidx(rem)=[]; neu_Dirnidx(rem)=[];
   
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(exc_armidx,exc_Dirnidx); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(exc_armidx));
        rand_armidx = exc_armidx(rorder);
        [rtmp, ptmp] = corrcoef(rand_armidx,exc_Dirnidx);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inh_armidx,inh_Dirnidx); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inh_armidx));
        rand_armidx = inh_armidx(rorder);
        [rtmp, ptmp] = corrcoef(rand_armidx,inh_Dirnidx);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neu_armidx,neu_Dirnidx); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neu_armidx));
        rand_armidx = neu_armidx(rorder);
        [rtmp, ptmp] = corrcoef(rand_armidx,neu_Dirnidx);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    exc_armidx = exc_armidx';
    inh_armidx = inh_armidx';
    neu_armidx = neu_armidx';
    exc_Dirnidx = exc_Dirnidx';
    inh_Dirnidx = inh_Dirnidx';
    neu_Dirnidx = neu_Dirnidx';
    
    figure; hold on;
    subplot(2,2,1);
    scatter(exc_armidx, exc_Dirnidx, 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(exc_Dirnidx, [ones(size(exc_armidx)) exc_armidx]);
    plot(min(exc_armidx):.01:max(exc_armidx), b(1)+b(2)*[min(exc_armidx):.01:max(exc_armidx)],'r')
    axis([-1 1 -1 1]);
    ylabel('DirnIdx'); xlabel('ArmIdx'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inh_armidx, inh_Dirnidx, 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(inh_Dirnidx, [ones(size(inh_armidx)) inh_armidx]);
    plot(min(inh_armidx):.01:max(inh_armidx), b(1)+b(2)*[min(inh_armidx):.01:max(inh_armidx)],'b')
    axis([-1 1 -1 1]);
    ylabel('DirnIdx'); xlabel('ArmIdx'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neu_armidx, neu_Dirnidx, 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(neu_Dirnidx, [ones(size(neu_armidx)) neu_armidx]);
    plot(min(neu_armidx):.01:max(neu_armidx), b(1)+b(2)*[min(neu_armidx):.01:max(neu_armidx)],'k')
    axis([-1 1 -1 1]);
    ylabel('DirnIdx'); xlabel('ArmIdx'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
    
    
end % end plotPE_Dirnidx




% Arm Index - and comparison with PE
% ----------------------------------
if plotPE_armidx
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_PFC_ArmIdx_gather_X6.mat';
    load(fname,'all_AI_PFCidxs','all_AI_PFCArmIdx');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh-Neu Indices
    PFCmod = [PFCindsExc;PFCindsInh]; % All PFC mod
    
    % Plot just ArmIdx
    excind = []; inhind = []; neuind = [];
    for i=1:size(all_AI_PFCidxs,1)
        excmatch = find(ismember(all_AI_PFCidxs(i,:), PFCindsExc, 'rows'));
        if ~isempty(excmatch)
            excind = [excind; i];
        else
            inhmatch = find(ismember(all_AI_PFCidxs(i,:), PFCindsInh, 'rows'));
            if ~isempty(inhmatch)
                inhind = [inhind; i];
            else
                neumatch = find(ismember(all_AI_PFCidxs(i,:), PFCindsNeu, 'rows'));
                if ~isempty(neumatch)
                    neuind = [neuind; i];
                end
            end
        end
    end
    
    exc_armidx = all_AI_PFCArmIdx(excind);
    inh_armidx = all_AI_PFCArmIdx(inhind);
    neu_armidx = all_AI_PFCArmIdx(neuind);
    
    figure; hold on;
    plot(1, nanmean(exc_armidx),'ro');  errorbar2(1, nanmean(exc_armidx), nanstderr(exc_armidx), 0.3, 'r');
    plot(2, nanmean(inh_armidx),'bo');  errorbar2(2, nanmean(inh_armidx), nanstderr(inh_armidx), 0.3, 'b');
    plot(3, nanmean(neu_armidx),'ko');  errorbar2(3, nanmean(neu_armidx), nanstderr(neu_armidx), 0.3, 'k');
    ylim([-1 1]);
    title('Arm Idx for PFC neurons ','FontSize',24,'FontWeight','normal');
    ylabel('Arm Idx','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3]); set(gca,'XTickLabel',{'Exc','Inh','Neu'});
    
    %[pKW table statsKW] = kruskalwallis([exc_armidx inh_armidx neu_armidx], [ones(1, length(exc_armidx)) ones(1,length(inh_armidx)).*2 ones(1,length(neu_armidx)).*3]);
    %        [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range
    
    % Plot PE vs ArmIdx
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_PE_X6.mat'
    excmatch=[]; inhmatch=[]; neumatch=[];
    excAI=[]; inhAI = []; neuAI=[]; excPE=[]; inhPE=[]; neuPE=[];
    
    for i=1:size(all_AI_PFCidxs,1)
        excmatch = find(ismember(pfcEXCPEinds, all_AI_PFCidxs(i,:), 'rows'));
        if ~isempty(excmatch)
            excAI = [excAI; all_AI_PFCArmIdx(i)];
            excPE = [excPE; pfcEXCPE(excmatch)];
        else
            inhmatch = find(ismember( pfcINHPEinds, all_AI_PFCidxs(i,:), 'rows'));
            if ~isempty(inhmatch)
                inhAI = [inhAI; all_AI_PFCArmIdx(i)];
                inhPE = [inhPE; pfcINHPE(inhmatch)];
            else
                neumatch = find(ismember(pfcNEUPEinds, all_AI_PFCidxs(i,:), 'rows'));
                if ~isempty(neumatch)
                    neuAI = [neuAI; all_AI_PFCArmIdx(i)];
                    neuPE = [neuPE; pfcNEUPE(neumatch)];
                end
            end
        end
    end
    
    rem = find(isnan(excPE) | isnan(excAI)); excAI(rem)=[]; excPE(rem)=[];
    rem = find(isnan(inhPE) | isnan(inhAI)); inhAI(rem)=[]; inhPE(rem)=[];
    rem = find(isnan(neuPE) | isnan(neuAI)); neuAI(rem)=[]; neuPE(rem)=[];
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(excPE,excAI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(excPE));
        randPE = excPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,excAI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inhPE,inhAI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inhPE));
        randPE = inhPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,inhAI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neuPE,neuAI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neuPE));
        randPE = neuPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,neuAI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    
    figure; hold on;
    subplot(2,2,1);
    scatter(excPE, excAI, 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(excAI, [ones(size(excPE)) excPE]);
    plot(min(excPE):.01:max(excPE), b(1)+b(2)*[min(excPE):.01:max(excPE)],'r')
    axis([0 1 -1 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inhPE, inhAI, 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(inhAI, [ones(size(inhPE)) inhPE]);
    plot(min(inhPE):.01:max(inhPE), b(1)+b(2)*[min(inhPE):.01:max(inhPE)],'b')
    axis([0 1 -1 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neuPE, neuAI, 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(neuAI, [ones(size(neuPE)) neuPE]);
    plot(min(neuPE):.01:max(neuPE), b(1)+b(2)*[min(neuPE):.01:max(neuPE)],'k')
    axis([0 1 -1 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
end % end plotPE_armidx











if plotSWRcorr
    
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh Indices
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs.mat'; % Has SWRcorr and SpatCorr
    %load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_nospeed_X6.mat'; % Has SWRcorr and SpatCorr: Spat Corr with no speed criterion
    % [An day CA1tet CA1cell PFCtet PFCcell SWRcorrPval SWRcorrRval SpatCorrRval]
    
    % 1.) Entire population: find sig SWR correlated pairs, and then pos or neg correlated
    % ------------------------------------------------------------------------------------
    possigrippairs = matpairs(matpairs(:,7)<0.05 & matpairs(:,8) > 0,9);
    negsigrippairs = matpairs(matpairs(:,7)<0.05 & matpairs(:,8) < 0,9);
    nonsigrippairs = matpairs(matpairs(:,7)>0.05, 9);
    figure; hold on;
    bar([mean(negsigrippairs) mean(nonsigrippairs) mean(possigrippairs)]); hold on;
    legend
    errorbar2([1 2 3], [mean(negsigrippairs) mean(nonsigrippairs) mean(possigrippairs)],  [stderr(negsigrippairs) stderr(nonsigrippairs) stderr(possigrippairs)] , 0.3, 'k')
    title('Spatial Corrln for SWR-correlated pair categories','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2 3]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    %stats
    [pKW table statsKW] = kruskalwallis([negsigrippairs' nonsigrippairs' possigrippairs'], [ones(1, length(negsigrippairs)) ones(1,length(nonsigrippairs)).*2 ones(1,length(possigrippairs)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.01); % change the alpha values to determine significance range.
    c
    
    % 2.) All Exc vs Inh vs Non-Modulated Neurons
    % -------------------------------------------
    % Get appropriate indices in matpairs. Can loop and use ismember, or just use intersect
    
    matpairs_PFC = matpairs(:,[1 2 5 6]);
    % [~,Exc_ind,ib_exc] = intersect(matpairs_PFC,PFCindsExc,'rows');
    % [~,Inh_ind,ib_inh] = intersect(matpairs_PFC,PFCindsInh,'rows');
    % Mod_ind = [Exc_ind;Inh_ind];
    % Neu_ind=[1:length(matpairs_PFC)];
    % Neu_ind(Mod_ind)=[];
    
    cntexccells=0; cntexcpairs=0; Excind_inpairs=[];
    for i=1:size(PFCindsExc,1)
        ind=[]; curridx=[];
        cntexccells = cntexccells+1;
        curridx = PFCindsExc(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntexcpairs = cntexcpairs+length(ind);
        Excind_inpairs=[Excind_inpairs;ind'];
    end
    cntinhcells=0; cntinhpairs=0; Inhind_inpairs=[];
    for i=1:size(PFCindsInh,1)
        ind=[]; curridx=[];
        cntinhcells = cntinhcells+1;
        curridx = PFCindsInh(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntinhpairs = cntinhpairs+length(ind);
        Inhind_inpairs=[Inhind_inpairs;ind'];
    end
    
    Modind_inpairs = [Excind_inpairs;Inhind_inpairs];
    Neuind_inpairs = [1:length(matpairs_PFC)];
    Neuind_inpairs(Modind_inpairs) = [];
    
    excrippairs = matpairs(Excind_inpairs,9);
    inhrippairs = matpairs(Inhind_inpairs,9);
    neurippairs = matpairs(Neuind_inpairs, 9);
    
    figure; hold on;
    bar([mean(inhrippairs) mean(excrippairs) mean(neurippairs)]); hold on;
    legend
    errorbar2([1 2 3], [mean(inhrippairs) mean(excrippairs) mean(neurippairs)],  [stderr(inhrippairs) stderr(excrippairs) stderr(neurippairs)] , 0.3, 'k')
    title('Spatial Corrln for Exc vs Inh AllSWRCorr pairs','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2 3]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    %stats
    [pKW table statsKW] = kruskalwallis([inhrippairs' excrippairs' neurippairs'], [ones(1, length(inhrippairs)) ones(1,length(excrippairs)).*2 ones(1,length(neurippairs)).*3]);
    pKW
    %[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.01); % change the alpha values to determine significance range.
    %c
    
    %3.) Excited vs Inh Significant SWR corr neurons
    matpairs_Exc = matpairs(Excind_inpairs,:);
    matpairs_Inh = matpairs(Inhind_inpairs,:);
    matpairs_Neu = matpairs(Neuind_inpairs,:);
    
    excsigrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05,9);
    inhsigrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 ,9);
    neusigrippairs = matpairs_Neu(matpairs_Neu(:,7)<0.05, 9);
    
    figure; hold on;
    bar([mean(inhsigrippairs) mean(excsigrippairs)  mean(neusigrippairs)]); hold on;
    legend
    errorbar2([1 2 3], [mean(inhsigrippairs) mean(excsigrippairs) mean(neusigrippairs)],  [stderr(inhsigrippairs) stderr(excsigrippairs) stderr(neusigrippairs)] , 0.3, 'k')
    title('Spatial Corrln for Inh vs Exc Sig SWRCorr pairs','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    %stats
    [pKW table statsKW] = kruskalwallis([inhsigrippairs' excsigrippairs' neusigrippairs'], [ones(1, length(inhsigrippairs)) ones(1,length(excsigrippairs)).*2 ones(1,length(neusigrippairs)).*3]);
    pKW
    
    
    %4.) Excited vs Inh Significant +ve or -ve SWR corr neurons
    
    excsigposrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05 & matpairs_Exc(:,8) > 0,9);
    excsignegrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05 & matpairs_Exc(:,8) < 0,9);
    excnonsigrippairs = matpairs_Exc(matpairs_Exc(:,7)>0.05,9);
    inhsigposrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 & matpairs_Inh(:,8) > 0,9);
    inhsignegrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 & matpairs_Inh(:,8) < 0,9);
    inhnonsigrippairs = matpairs_Inh(matpairs_Inh(:,7)>0.05,9);
    %Neusigrippairs = matpairs_Neu(matpairs_Neu(:,7)<0.05, 9);
    
    %POS
    figure; hold on;
    bar([mean(inhsigposrippairs) mean(excsigposrippairs)]); hold on;
    legend
    errorbar2([1 2], [mean(inhsigposrippairs) mean(excsigposrippairs)],  [stderr(inhsigposrippairs) stderr(excsigposrippairs)] , 0.3, 'k')
    title('Spatial Corrln for Inh vs Exc Sig POS SWRCorr pairs','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    %stats
    [pKW table statsKW] = kruskalwallis([inhsigposrippairs' excsigposrippairs'], [ones(1, length(inhsigposrippairs)) ones(1,length(excsigposrippairs)).*3]);
    pKW
    
    %NEG
    figure; hold on;
    bar([mean(inhsignegrippairs) mean(excsignegrippairs)]); hold on;
    legend
    errorbar2([1 2], [mean(inhsignegrippairs) mean(excsignegrippairs)],  [stderr(inhsignegrippairs) stderr(excsignegrippairs)] , 0.3, 'k')
    title('Spatial Corrln for Inh vs Exc Sig NEG SWRCorr pairs','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    %stats
    [pKW table statsKW] = kruskalwallis([inhsignegrippairs' excsignegrippairs'], [ones(1, length(inhsignegrippairs)) ones(1,length(excsignegrippairs)).*3]);
    pKW
    
    
    %POS and NEG
    figure; hold on;
    bar([mean(inhsignegrippairs) mean(inhnonsigrippairs) mean(inhsigposrippairs) mean(excsignegrippairs) mean(excnonsigrippairs) mean(excsigposrippairs)]); hold on;
    legend
    errorbar2([1 2 3  4 5 6], [mean(inhsignegrippairs)  mean(inhnonsigrippairs) mean(inhsigposrippairs) mean(excsignegrippairs) mean(excnonsigrippairs) mean(excsigposrippairs)],...
        [stderr(inhsignegrippairs)  stderr(inhnonsigrippairs) stderr(inhsigposrippairs) stderr(excsignegrippairs) stderr(excnonsigrippairs) stderr(excsigposrippairs)] , 0.3, 'k')
    title('Spatial Corrln Separated by Pairs with Inh vs Exc PFC neurons','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[2 5]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    %stats
    [pKW table statsKW] = kruskalwallis([inhsignegrippairs' inhnonsigrippairs' inhsigposrippairs' excsignegrippairs' excnonsigrippairs' excsigposrippairs'], [ones(1, length(inhsignegrippairs)) ones(1, length(inhnonsigrippairs))*2 ones(1, length(inhsigposrippairs)).*3 ...
        ones(1,length(excsignegrippairs)).*4 ones(1,length(excnonsigrippairs)).*5 ones(1,length(excsigposrippairs)).*6]);
    pKW
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.01); % change the alpha values to determine significance range.
    c
    
    
    figure; hold on;
    subplot(1,2,1)
    scatter(matpairs_Exc(:,8), matpairs_Exc(:,9),'.k'); hold on;
    [b,bint,r,rint,stats] = regress(matpairs_Exc(:,9), [ones(size(matpairs_Exc(:,8))) matpairs_Exc(:,8)]);
    plot(min(matpairs_Exc(:,8)):.1:max(matpairs_Exc(:,8)), b(1)+b(2)*[min(matpairs_Exc(:,8)):.1:max(matpairs_Exc(:,8))])
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Exc-PFC: r^2(%0.3f) p(%0.3f)', stats(1), stats(4))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    subplot(1,2,2)
    scatter(matpairs_Inh(:,8), matpairs_Inh(:,9),'.k'); hold on;
    [b,bint,r,rint,stats] = regress(matpairs_Inh(:,9), [ones(size(matpairs_Inh(:,8))) matpairs_Inh(:,8)]);
    plot(min(matpairs_Inh(:,8)):.1:max(matpairs_Inh(:,8)), b(1)+b(2)*[min(matpairs_Inh(:,8)):.1:max(matpairs_Inh(:,8))])
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Inh-PFC: r^2(%0.3f) p(%0.3f)', stats(1), stats(4))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    
end
