
% Version 2 - USing filter framework ripplemod 

% USe the saved data files - HP_thetamod and HP_ripplemod to plot
% correlations between theta modulation and ripple modulation'

clear;

savefig1=0;
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
%savedir = '/opt/data15/gideon/HP_ProcessedData/';

%PFC or CA1 - specify area above
area = 'PFC'; thetafile = [savedir 'HP_thetamod_',area,'_alldata_Nspk50_gather_X6']; 
%area = 'CA1'; thetafile = [savedir 'HP_thetamod_',area,'_alldata_Nspk50_gather_X8']; % New CA1 file 


%thetafile = [savedir 'HP_thetamod_',area,'_alldata_Nspk50_gather_3-6-2014']; 
state = ''; %state = 'sleep'; %or state = '';
%ripplefile = [savedir 'HP_ripplemod',state,'_',area,'_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014']; % ripple mod in awake or sleep
ripplefile = [savedir 'HP_ripplemod',state,'_',area,'_alldata_std3_speed4_ntet2_Nspk50_gather_X6'];

load(ripplefile, 'allripplemod','allripplemod_idx'); % load allripplemod and allripplemod_idx. 
load(thetafile,'allthetamod','allthetamod_idx'); 

if strcmp(state,'sleep'),
    statename = 'Sleep';
else
    statename = 'Run';
end

% Match idxs as in xcorrmesaures2

cntcells=0; cnt_mismatch=0;

% for i=1:length(allripplemod)
%     
%     rippleidx = allripplemod_idx(i,:);
%     match = rowfind(rippleidx, allthetamod_idx);
    
for i=1:length(allthetamod)
    i;
    curridx = allthetamod_idx(i,:);
    
    %% Temp - till Gideon's animal Nadal is fixed in DFSsj_plotthetamod
%     if curridx(1)==4
%         curridx(2)=curridx(2)-7; %Adjust day
%     end
%     
    match = rowfind(curridx, allripplemod_idx);
    
    if match~=0,    
        cntcells = cntcells+1;
        allmod(cntcells).idx = curridx;
        % Theta
        allmod(cntcells).sph = allthetamod(i).sph;
        allmod(cntcells).Nspk = allthetamod(i).Nspk;
        allmod(cntcells).kappa = allthetamod(i).kappa;
        allmod(cntcells).modln = allthetamod(i).modln;
        allmod(cntcells).meanphase = allthetamod(i).meanphase;
        allmod(cntcells).prayl = allthetamod(i).prayl;
        allmod(cntcells).thetahistnorm = allthetamod(i).thetahistnorm;
        % Ripple
%        allmod(cntcells).sig_shuf = allripplemod(match).sig_shuf; % Use this to determine significance
        %allmod(cntcells).pshuf = allripplemod(match).pshuf;
        %allmod(cntcells).D = allripplemod(match).D; % Distance metric
        allmod(cntcells).ripmodln_peak = allripplemod(match).modln_peak; % % peak change above baseline
        allmod(cntcells).ripmodln = allripplemod(match).modln; % Mean change over baseline
     %   allmod(cntcells).ripmodln_shuf = allripplemod(match).modln_shuf; % %value of significance
        allmod(cntcells).sig_ttest = allripplemod(match).sig_ttest;
         
        % New
        %allmod(cntcells).modln_raw = allripplemod(match).modln_raw;
        allmod(cntcells).ripmodln_div = allripplemod(match).modln_div; % % peak change above baseline
   %     allmod(cntcells).pvar = allripplemod(match).rasterShufP;
    %    allmod(cntcells).mvar = allripplemod(match).varRespAmp;
        allmod(cntcells).mvar2 = allripplemod(match).varRespAmp2;
        allmod(cntcells).pvar2 = allripplemod(match).rasterShufP2;
        
        allmod(cntcells).type = allripplemod(match).type;  % exc or inh
        
        allmod(cntcells).FStag = allripplemod(match).FStag; % FS neuron or Not

        %allmod(cntcells).mvar3 = allripplemod(match).var_changerespbck;
        
        % Prop
        match;
        allmod(cntcells).cellfr = allripplemod(match).cellfr;
        
        % Rdm resp modln
%        allmod(cntcells).rdmmodln_peak = allripplemod(match).modln_peak_rdm; % % peak change above baseline
 %       allmod(cntcells).rdmmodln = allripplemod(match).modln_rdm; % Mean change over baseline
        
    end
    
end

% ------------------
% Population Figures
% ------------------
forppr = 0; 
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

%figdir = '/data25/sjadhav/HPExpt/Figures/ThetaMod/2015/Summary_PhaseLocking/';
figdir = '/data25/sjadhav/HPExpt/Figures/ThetaMod/2015/Summary_New/';
summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 24;
    xfont = 24;
    yfont = 24;
end

% Get data: Get rid of FS neurons

cnt=0;
for i=1:length(allmod)
    
    currFS = allmod(i).FStag;
    
    if strcmp(currFS,'n')
        cnt=cnt+1;
        
        % Theta
        allidxs(cnt,:) = allmod(i).idx;
        allkappas(cnt) = allmod(i).kappa;
        allmodln(cnt) = allmod(i).modln;
        allmeanphase(cnt) = allmod(i).meanphase;
        allprayl(cnt) = allmod(i).prayl;      
        allthetahistnorm(cnt,:) = allmod(i).thetahistnorm;
        % Ripple
        allripmodln_peak(cnt) = allmod(i).ripmodln_peak;
        allripmodln_div(cnt) = allmod(i).ripmodln_div;
        allripmodln(cnt) = allmod(i).ripmodln;
        %    allripmodln_shuf(cnt) = allmod(i).ripmodln_shuf; %[]
        %allD(cnt) = allmod(i).D;
        %allpshuf(cnt) = allmod(i).pshuf;       % 37/120, ~30% [OLD 25/61 are ripple modulated. 41%]
        %    allsigshuf(cnt) = allmod(i).sig_shuf;
        allsigttest(cnt) = allmod(i).sig_ttest;
        
        % New
        %    allpvar(cnt) = allmod(i).pvar;
        %    allripmodln_var(cnt) = allmod(i).mvar;
        allripmodln_var2(cnt) = allmod(i).mvar2;
        %allripmodln_var3(cnt) = allmod(i).mvar3;
        allpvar2(cnt) = allmod(i).pvar2;
        %allmodln_raw(cnt) = allmod(i).modln_raw;
        
        alltype{cnt} = allmod(i).type;
        
        % Prop
        allcellfr(cnt) = allmod(i).cellfr;
        %allFS(cnt) = allmod(i).FStag;
        
    end
    
    % Rdm resp
    %    allrdmmodln_peak(i) = allmod(i).rdmmodln_peak;
    %   allrdmmodln(i) = allmod(i).rdmmodln;
    
end



% WhichSWR modln to use
% ------------------------
%allripmodln = allripmodln_peak; % Peak change over baseline 
%allripmodln = allripmodln; % Mean change over baseline
%allripmodln = abs(allripmodln); % Mean change over baseline

%allripmodln = abs(allripmodln_peak); sigrip = find(allsigshuf==1); sigboth = find(allprayl<0.05 & allsigshuf==1); allrdmmodln = abs(allrdmmodln_peak); % 
allripmodln = abs(allripmodln_var2); 
% %Remove outlier in allripmodln_var
% if ~strcmp(state,'sleep')
%     rem = find(allripmodln>12);
%     allripmodln(rem)=[]; allmodln(rem)=[]; allkappas(rem)=[]; allprayl(rem)=[]; allpvar(rem)=[]; allpvar2(rem)=[]; allsigshuf(rem)=[];
%     allcellfr(rem) = [];
% end

sigrip = find(allpvar2<0.05); % Sig. Rip mod
nonsigrip = find(allpvar2>=0.05); % Non-sig Rip Mod
sigripexc = find(allpvar2<0.05 & strcmp(alltype,'exc'));
sigripinh = find(allpvar2<0.05 & strcmp(alltype,'inh'));

sigtheta = find(allprayl<0.05);  %All Theta Mod
nonsigtheta = find(allprayl>=0.05);
sigripthetaexc = find(allprayl<0.05 & allpvar2<0.05 & strcmp(alltype,'exc')); % Rip and theta: Exc
sigripthetainh = find(allprayl<0.05 & allpvar2<0.05 & strcmp(alltype,'inh')); % Rip and theta: Inh
nonsigthetaexc = find(allprayl>=0.05 & allpvar2<0.05 & strcmp(alltype,'exc'));
nonsigthetainh = find(allprayl>=0.05 & allpvar2<0.05 & strcmp(alltype,'inh'));


sigboth = find(allprayl<0.05 & allpvar2<0.05==1); % Both Rip and Theta Mod
allsig = union(sigrip, sigtheta); 
sigripnontheta = find(allpvar2<0.05 & allprayl>=0.05);
sigthetanonrip = find(allpvar2>=0.05 & allprayl<0.05); % Neutral but theta modulated


% Vector of 0s and 1s - sig ripple vs sig theta
ripvec = zeros(size(allripmodln)); ripvec(sigrip)=1;
thetavec = zeros(size(allripmodln)); thetavec(sigtheta)=1;
[rvec,pvec] = corrcoef(ripvec,thetavec);

%x = find(allmodln > 0.65 & allmodln < 0.7 & allripmodln > 80);
%allidxs(x,:);



 

% -------------------------------% -------------------------------% -------------------------------
% -------------------------------% -------------------------------% -------------------------------


% BAR PLOTS FOR SWR CATEGORIES: Written for PFC specifically: strcmp(area,'PFC')
% -------------------------------------------------------------------------------

if ~strcmp(area,'PFC'),
    disp('This is CA1 data');
    
    % Get Polar plot for CA1, and save distribution of phases for comparison with PFC
    
    % In thetamod file, 479/586 are theta modulated
    % Here, combination of thetamod and ripplemod, there are 531 CA1 cells, and 
    % 431/532 are sigtheta
    
    % Plotting phase    
    aCA1 = allmeanphase(sigtheta);  % 
    
    % Plot Polar for CA1
    [t,r] = rose(aCA1);
    figure;  redimscreen_figforppt1;
    polar(t,r,'g'); hold on;
    [mCA1, phCA1, mCA12] = modulation(aCA1);  %[mexc, phexc, mexc2] = modulation(aexc,'stdev',pi/8);
    phdegCA1 = phCA1*(180/pi);
    % The peak phase angle
    lims = get(gca,'XLim');
    radius = lims(2);
    xx = radius .* cos(phCA1); yy = radius .* sin(phCA1);
    line([0 mCA1*xx], [0 mCA1*yy],'LineWidth',4,'Color','g'); %plot(xx,yy,'ko','MarkerSize',4);
    
    if savefig1
        savefigfilename = 'CA1_All_PhasePolarN_X8_sigtheta_431neu';
        figfile = [figdir,savefigfilename]
        print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
    figure; hold on;
    nbins=50;  bins = -pi:(2*pi/nbins):pi; cntsig = length(sigtheta);
    % Plot matrix plot also
    histth = allthetahistnorm(sigtheta,:);
    [sortedph, order] = sort(aCA1,2,'descend');
    sort_histnorm = histth(order,:);
    smsort=[];    bins_plot = bins(1:(end-1));
    % nstd=1: gaussian of length 4. nstd = 2: gaussian of length 7, nstd=3: gaussian of length 10.
    nstd = 3; g1 = gaussian(nstd, 3*nstd+1);
    
    for n =1:length(order),
        curr = sort_histnorm(n,1:50); % Last bin should be skipped
        curr = smoothvect(curr,g1);
        smsort(n,:) = curr(2:end-1);
        smsort(n,:) = smsort(n,:)./max(smsort(n,:)); % Renormalize
    end
    bins_plot = bins_plot(2:end-1);
    
    imagesc(bins_plot,1:n,flipud(smsort)); colorbar;
    set(gca,'XLim',[-pi pi]); set(gca,'YLim',[0 n]);
    a = num2str([-180,0,180]');
    set(gca, 'XTick', [-pi:pi:pi], 'XTickLabel',a);
    xlabel(['Phase'],'FontSize',xfont,'Fontweight','normal');
    ylabel(['Cell no'],'FontSize',yfont,'Fontweight','normal');
    title(sprintf('Phase-locked units aligned by Pref Phase: %d',cntsig),'FontSize',tfont,'FontWeight','normal');
    
    figfile = [figdir,'CA1_Thetamod_MatrixAlignPrefPhase_X8_sigtheta431neu']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
        
    end
    
    % Save CA1 file
    savefile = [savedir,'CA1_ThetaAndRipmod_X8']
    if savefile1==1
        save(savefile);
    end
    

    
    
    
    keyboard;  % Don't go ahead to PFC
end


% ------------------
% FOR PFC DATA ONLY
% ----------------

ptype=1; % plot Modln (1) or phase (2)?

switch ptype
    case 1
        allmodln = allkappas; % Use kappas for Modln
    case 2
        allmodln = allmeanphase; % Compare Mean Phase (Best Phase)
end

figure; hold on;
if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end

[ht,pt] = ttest2(allmodln(sigrip),allmodln(nonsigrip)),
[htei,ptei] = ttest2(allmodln(sigripexc),allmodln(sigripinh)),


% A) Theta Modln/Phase for different categories
% ------------------------------------

switch ptype
    case 1        
        % Thes Defns include non-sig theta modulated. Change Below
        a1 = allmodln; % All PFC
        a1sig = allmodln(sigrip); % Sig. Rip Modulated
        aexc = allmodln(sigripexc); % PFC Excited
        ainh = allmodln(sigripinh); % PFC Inhibited
        aneu = allmodln(nonsigrip); % PFC Unmodulated
        
        % Only Sig:
        %a1 = allmodln(sigtheta); %
        %a1sig = allmodln(sigboth); %
        %aexc = allmodln(sigripthetaexc);
        %ainh = allmodln(sigripthetainh);
        %aneu = allmodln(sigthetanonrip);
        
    case 2
        %If phase, want to quantify only significant theta modulation
        % Different categories, but only significantly theta modulated
        a1 = allmodln(sigtheta); %
        a1sig = allmodln(sigboth); %
        aexc = allmodln(sigripthetaexc);
        ainh = allmodln(sigripthetainh);
        aneu = allmodln(sigthetanonrip);
end

%bar(1,nanmean(a1),'facecolor','k'); hold on;

%bar(1,nanmean(allmodln(sigrip)),'facecolor','k'); hold on;

bar(1,nanmean(aexc),'facecolor','r'); hold on;
bar(2,nanmean(ainh),'facecolor','b'); hold on;
bar(3,nanmean(aneu),'facecolor','c'); hold on;
%legend('PFCAll','PFCExc','PFCInh','PFCNonMod');
legend('PFCExc','PFCInh','PFCUnMod');

set(gca,'xtick',[], 'xticklabel', [])
% errorbar2([1 2 3 4], [nanmean(a1) nanmean(allmodln(sigripexc)) nanmean(allmodln(sigripinh)) nanmean(allmodln(nonsigrip))],...
%     [stderr(a1) stderr(allmodln(sigripexc)) stderr(allmodln(sigripinh)) stderr(allmodln(nonsigrip))], 0.3, 'k');
 
errorbar2([1 2 3], [nanmean(aexc) nanmean(ainh) nanmean(aneu)],...
    [stderr(aexc) stderr(ainh) stderr(aneu)], 0.3, 'k');
 


switch ptype
    case 1
        title('Theta Modln of PFC neuron categories');
        figfile = [figdir,'PFC_Thetamod_SWR_CategoriesN']
        ylabel('K (Concentration parameter)');
    case 2
        title('Theta Phase of PFC neuron categories');
        figfile = [figdir,'PFC_ThetaPhase_SWR_CategoriesN']
        alabel = {'-pi/4','-pi/8', '0','pi/8'};
        atick = [-pi/4:pi/8:pi/8];
        set(gca, 'YTick', atick, 'YTickLabel',alabel);
end


if savefig1
    %figfile = [figdir,savefigfilename,'/',sprintf('pfcripmodPEC_norm')];
    print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end


% B) Distribution of theta modultion/phase
% ------------------------------------

switch ptype
    case 1
        range = 0:0.075:1;   
        %range = [0,0.1,0.2,0.3,0.4,0.6,0.8,1];   
    case 2 
        range = -pi:pi/4:pi;
        %a = num2str([-180,0,180,0,180]'); a(3,:) = '+-pi';
        %set(gca, 'XTick', [-pi:pi:3*pi], 'XTickLabel',a);
        alabel = {'-pi','pi/2','0','pi/2','pi'};
        atick = [-pi:pi/2:pi];     
end

a1hist=histc(a1,range); a1histnorm = a1hist./max(a1hist);
a1sighist=histc(a1sig,range); a1sighistnorm = a1sighist./max(a1sighist);
aexchist=histc(aexc,range); aexchistnorm = aexchist./max(aexchist);
ainhhist=histc(ainh,range); ainhhistnorm = ainhhist./max(ainhhist);
aneuhist=histc(aneu,range); aneuhistnorm = aneuhist./max(aneuhist);

% Cumulative plots
ca1 = cumsum(a1hist);
ca1sig = cumsum(a1sighist);
caexc = cumsum(aexchist); caexcnorm = caexc./caexc(end);
cainh = cumsum(ainhhist); cainhnorm = cainh./cainh(end);
caneu = cumsum(aneuhist); caneunorm = caneu./caneu(end);

figure; hold on; 
%plot(range,ca1,'k-','LineWidth',2); 
plot(range,caexcnorm,'r-','LineWidth',2); 
plot(range,cainhnorm,'b-','LineWidth',2); 
plot(range,caneunorm,'c-','LineWidth',2); 
%legend('PFCAll','PFCExc','PFCInh','PFCNonMod');
legend('PFCExc','PFCInh','PFCNonMod');
ylabel('Cumulative Fraction of neurons');

ylim([0 1]);
                            % All / only theta mod    
[h,p] = ttest2(a1sig, aneu) % p<10^-5   / 0.001
[h1,p1] = ttest2(aexc, ainh) % p = 0.25 / 0.11
[h2,p2] = ttest2(aexc, aneu) % p = 3.8X10-4 / 0.03
[h3,p3] = ttest2(ainh, aneu) % p < 1.1X10-5 / <10^-3


[h,p] = kstest2(a1sig, aneu) %  p = 0.01 /     /  0.02
[h1,p1] = kstest2(aexc, ainh) % p = 0.19/ 0.11  / 0.4
[h2,p2] = kstest2(aexc, aneu) % p = 0.0027/ 0.03 /0.12 
[h3,p3] = kstest2(ainh, aneu) % p = 0.1348/ 0.003 /0.06

                            % All / Only Sig Theta Modulated
[pr] = ranksum(a1sig, aneu) %  p = 7.6x10-4 / 0.015
[pr1] = ranksum(aexc, ainh) % p = 0.5 / 0.91
[pr3] = ranksum(aexc, aneu) % p = 0.0012/ 0.067
[pr4] = ranksum(ainh, aneu) % p = 0.009/ 0.04


%aexc = 0.2+-0.02
%ainh = 0.25 +- 0.04
%aneu = 0.12+-0.01

[pKW table statsKW] = kruskalwallis([aexc ainh aneu], [ones(1, length(aexc)) ones(1,length(ainh)).*2 ones(1,length(aneu)).*3]); % 0.0024
[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'off', 'alpha', 0.05); % change the alpha values to determine significance range.



% figure; hold on; 
% %plot(range,a1histnorm,'k-','LineWidth',2); 
% plot(range,aexchistnorm,'r-','LineWidth',2); 
% plot(range,ainhhistnorm,'b-','LineWidth',2); 
% plot(range,aneuhistnorm,'c-','LineWidth',2); 
% %legend('PFCAll','PFCExc','PFCInh','PFCNonMod');
% legend('PFCExc','PFCInh','PFCNonMod');
% ylabel('Fraction of neurons');

switch ptype
    case 1
         title('Theta Modln Distr of PFC neuron categories');  
         figfile = [figdir,'PFC_ThetamodDistr_SWR_Categories2N']
         xlabel('K (Concentration parameter)');
    case 2 
         title('Theta Phase Distr of PFC neuron categories');
         set(gca, 'XTick', atick, 'XTickLabel',alabel);
         figfile = [figdir,'PFC_ThetaPhaseDistr_SWR_Categories2N']
         xlabel('Peak Theta Phase');
end


if savefig1
    %figfile = [figdir,savefigfilename,'/',sprintf('pfcripmodPEC_norm')];
    print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end



% ------------------------------------
% C) IF PHASE, DO POLAR PLOT
% ------------------------------------

if ptype==2  

    % All PFC
    [t,r] = rose(a1);
    figure;  redimscreen_figforppt1;
    polar(t,r,'k'); hold on;
    [mall, phall, mall2] = modulation(a1);  %[mexc, phexc, mexc2] = modulation(aexc,'stdev',pi/8);
    phdegall = phall*(180/pi);
    % The peak phase angle
    lims = get(gca,'XLim');
    radius = lims(2);
    xx = radius .* cos(phall); yy = radius .* sin(phall);
    line([0 mall*xx], [0 mall*yy],'LineWidth',4,'Color','k'); %plot(xx,yy,'ko','MarkerSize',4);
    
    if savefig1
        savefigfilename = 'PFC_All_PhasePolarN';
        figfile = [figdir,savefigfilename]
        print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
    
    % All Sig
    [t,r] = rose(a1sig);
    figure;  redimscreen_figforppt1;
    polar(t,r,'k'); hold on;
    [msig, phsig, msig2] = modulation(a1sig);  %[mexc, phexc, mexc2] = modulation(aexc,'stdev',pi/8);
    phdegsig = phsig*(180/pi);
    % The peak phase angle
    lims = get(gca,'XLim');
    radius = lims(2);
    xx = radius .* cos(phsig); yy = radius .* sin(phsig);
    line([0 msig*xx], [0 msig*yy],'LineWidth',4,'Color','k'); %plot(xx,yy,'ko','MarkerSize',4);
    
    if savefig1
        savefigfilename = 'PFC_Sig_PhasePolarN';
        figfile = [figdir,savefigfilename]
        print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
    
    % EXC
    [t,r] = rose(aexc);
    figure;  redimscreen_figforppt1;
    polar(t,r,'r'); hold on;
    [mexc, phexc, mexc2] = modulation(aexc);  %[mexc, phexc, mexc2] = modulation(aexc,'stdev',pi/8);
    phdegexc = phexc*(180/pi);
    % The peak phase angle
    lims = get(gca,'XLim');
    radius = lims(2);
    xx = radius .* cos(phexc); yy = radius .* sin(phexc);
    line([0 mexc*xx], [0 mexc*yy],'LineWidth',4,'Color','r'); %plot(xx,yy,'ko','MarkerSize',4);
    
    if savefig1
        savefigfilename = 'PFC_Exc_PhasePolarN';
        figfile = [figdir,savefigfilename]
        print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
    
    % INH
    [t,r] = rose(ainh);        
    figure;  redimscreen_figforppt1;
    polar(t,r,'b'); hold on;
    [minh, phinh, minh2] = modulation(ainh);
    phdeginh = phinh*(180/pi);
    % The peak phase angle
    lims = get(gca,'XLim');
    radius = lims(2);
    xx = radius .* cos(phinh); yy = radius .* sin(phinh);
    line([0 minh*xx], [0 minh*yy],'LineWidth',4,'Color','b');
    
    if savefig1
        savefigfilename = 'PFC_Inh_PhasePolarN';
        figfile = [figdir,savefigfilename]
        print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
    
%     if savefig1
%         savefigfilename = 'PFC_ExcInh_PhasePolar';
%         figfile = [figdir,savefigfilename]
%         print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
%     end
    
    % NEU
    [t,r] = rose(aneu);
    figure;  redimscreen_figforppt1;
    polar(t,r,'c'); hold on;
    [mneu, phneu, mneu2] = modulation(aneu);
    phdegneu = phneu*(180/pi);
    % The peak phase angle
    lims = get(gca,'XLim');
    radius = lims(2);
    xx = radius .* cos(phneu); yy = radius .* sin(phneu);
    line([0 mneu*xx], [0 mneu*yy],'LineWidth',4,'Color','c');

    if savefig1
        savefigfilename = 'PFC_Neu_PhasePolarN';
        figfile = [figdir,savefigfilename]
        print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
    
    % Statistics: 
    
    % watson williams test criterion?(1-wayanova)
    % cmtest like kruskal-wallis, medians. nn-parametric multi-sample test
    % ktest: parametric 2 sample test to test conc parameter
    % kuipertest: like kstest. 2 sample testto test distribution, any propoerty mean or dispersion 
  
    
    % These tests include cells with are not significantly theta modulated
    % -----------------------------------------------------------------------------
        
    % Exc-Neu
    alp = [aexc,aneu]; idx=[ones(1,length(aexc)),2*ones(1,length(aneu))]; 
    [pval_excneu table_excneu] = circ_wwtest(alp,idx) % 0.037 / 10^-4
    pvalk = circ_ktest(aexc,aneu) %0.018
    pvalkupier = circ_kuipertest(aexc,aneu,20) %p=0.001
    
    %Inh-Neu
    alp = [ainh,aneu]; idx=[ones(1,length(ainh)),2*ones(1,length(aneu))];  
    [pval_inhneu table_inhneu] = circ_wwtest(alp,idx) % 0.83/ 10^-5
    pvalk = circ_ktest(ainh,aneu) %0.57
    pvalkupier = circ_kuipertest(ainh,aneu,20) %p>0.1 (1)
    
    %Exc-Inh
    alp = [aexc,ainh]; idx=[ones(1,length(aexc)),2*ones(1,length(ainh))];  
    [pval_excinh table_excinh] = circ_wwtest(alp,idx) % 0.096/ 0.33
    pvalk = circ_ktest(aexc,ainh) %0.133
    pvalkupier = circ_kuipertest(aexc,ainh,20) %p>0.1 (1)
       
    %Sig vs. Neu
    alp = [a1sig,aneu]; idx=[ones(1,length(a1sig)),2*ones(1,length(aneu))];
    [pval_signeu table_signeu] = circ_wwtest(alp,idx) %0.10
    pvalk = circ_ktest(a1sig,aneu) %0.07
    pvalkupier = circ_kuipertest(a1sig,aneu,20) %p=0.005
    
    % Exc-Inh-Neu
    alp = [aexc,ainh,aneu]; idx=[ones(1,length(aexc)),2*ones(1,length(ainh)),3*ones(1,length(aneu))];  % 
    [pval_excinhneu table_excinhneu] = circ_wwtest(alp,idx)  %0.07/ 10^-5
    
    
    % Comparing CA1 and PFC
    % Within PFC file, do this:
    loadfile = [savedir,'CA1_ThetaAndRipmod_X6'];
    load(loadfile,'aCA1');
    
    % Compare PFC ripandthetamodulated with CA1, and PFCall with CA1
    
    %Sig vs. Neu
    alp = [a1sig,aCA1], idx=[ones(1,length(a1sig)),2*ones(1,length(aCA1))];
    [pval table] = circ_wwtest(alp,idx) %p=0
    pvalk = circ_ktest(a1sig,aCA1) %0.37
    pvalkupier = circ_kuipertest(a1sig,aCA1,20) %p=0.001
    
    alp = [a1,aCA1], idx=[ones(1,length(a1)),2*ones(1,length(aCA1))];
    [pval table] = circ_wwtest(alp,idx) %p=0
    pvalk = circ_ktest(a1,aCA1) %p=9.1*10-4
    pvalkupier = circ_kuipertest(a1,aCA1,20) %p=0.001
    
    
    
end


% % a)
% [pKW table statsKW] = kruskalwallis([allmodln(sigrip) allmodln(nonsigrip)],...
%     [ones(1, length(allmodln(sigrip))) ones(1,length(allmodln(nonsigrip))).*2]);
% [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); 
% 
% % b)
% [pKW table statsKW] = kruskalwallis([allmodln(sigripexc) allmodln(sigripinh) allmodln(nonsigrip)],...
%     [ones(1,length(allmodln(sigripexc))) ones(1,length(allmodln(sigripinh))).*2 ones(1,length(allmodln(nonsigrip))).*3]);
% [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
% 
% 
% % c)
% [pKW table statsKW] = kruskalwallis([allmodln(sigrip) allmodln(sigripexc) allmodln(sigripinh) allmodln(nonsigrip)],...
%     [ones(1, length(allmodln(sigrip))) ones(1,length(allmodln(sigripexc))).*2 ones(1,length(allmodln(sigripinh))).*3 ones(1,length(allmodln(nonsigrip))).*4]);
% [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.



% BAR PLOTS FOR Theta Categories
% -----------------------------

allmodln = allripmodln; % Plot rip modln for different theta categories

[ht,pt] = ttest2(allmodln(sigtheta),allmodln(nonsigtheta)), %0.0087
[hk,pk] = kstest2(allmodln(sigtheta),allmodln(nonsigtheta)), %0.0028
pr = ranksum(allmodln(sigtheta),allmodln(nonsigtheta)) %0.0018

sigm = allmodln(sigtheta); nonsigm = allmodln(nonsigtheta);

% Rip Mod for Thetamod and Theta Unmod
figure; hold on;
if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
bar(1,nanmean(allmodln(sigtheta)),'facecolor','k'); hold on;
bar(2,nanmean(allmodln(nonsigtheta)),'facecolor','c'); hold on;
legend('ThetaMod', 'NonThetaMod');
title('SWR Modln of PFC theta categories'); set(gca,'xtick',[], 'xticklabel', [])
ylabel('SWR Modulation');
errorbar2([1 2], [nanmean(allmodln(sigtheta)) nanmean(allmodln(nonsigtheta))],...
     [stderr(allmodln(sigtheta)) stderr(allmodln(nonsigtheta))], 0.3, 'k');

figfile = [figdir,'PFC_SWRmodln_Theta_Categories1']
if savefig1
    print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');

end

% Distr

range = 0:0.3:10;   
a1 = allmodln(sigtheta); a1hist=histc(a1,range); a1histnorm = a1hist./max(a1hist);
a2 = allmodln(nonsigtheta); a2hist=histc(a2,range); a2histnorm = a2hist./max(a2hist);


% Cumulative plots
ca1 = cumsum(a1hist); ca1norm = ca1./ca1(end);
ca2 = cumsum(a2hist); ca2norm = ca2./ca2(end);

figure; hold on; 
plot(range,ca1norm,'k-','LineWidth',2); 
plot(range,ca2norm,'c-','LineWidth',2); 
legend('ThetaMod','NonThetaMod');
ylabel('Cumulative Fraction of neurons');
title('SWR Modln Distr of PFC theta categories'); %set(gca,'xtick',[], 'xticklabel', [])
xlabel('SWR Modulation');

ylim([0 1])

%[h1,p1] = kstest2(ca1norm, ca2norm) % p = 



% Save PFC separately: Need to compare phase with CA1
savefile = [savedir,'PFC_ThetaAndRipmod_X6'];
if savefile1==1
    save(savefile);
end
    






% figure; hold on; 
% plot(range,a1histnorm,'k.-','LineWidth',2); 
% plot(range,a2histnorm,'c.-','LineWidth',2); 
% legend('ThetaMod','NonThetaMod');
% ylabel('Fraction of neurons');
% title('SWR Modln Distr of PFC theta categories'); set(gca,'xtick',[], 'xticklabel', [])
% xlabel('SWR Modulation');

figfile = [figdir,'PFC_SWRmodlnDistr_Theta_Categories']

if savefig1
    %figfile = [figdir,savefigfilename,'/',sprintf('pfcripmodPEC_norm')];
    print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end



% Rip Modln for different categories
figure; hold on;
if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
bar(1,nanmean(allmodln(sigtheta)),'facecolor','k'); hold on;
bar(2,nanmean(allmodln(nonsigtheta)),'facecolor','c'); hold on;
%bar(3,nanmean(allmodln(sigboth)),'facecolor','m'); hold on;
bar(3,nanmean(allmodln(sigthetaexc)),'facecolor','r'); hold on;
bar(4,nanmean(allmodln(sigthetainh)),'facecolor','b'); hold on;

%bar(6,nanmean(allmodln(sigthetanonrip)),'facecolor','g'); hold on;

legend('ThetaMod', 'NonThetaMod', 'Theta&RipMod', 'ThetaMod-RipExc','ThetaMod-RipInh');
title('SWR Modln of PFC theta categories'); set(gca,'xtick',[], 'xticklabel', [])
ylabel('SWR Modulation');

%title('Mean Theta Phase of PFC neuron categories'); set(gca,'xtick',[], 'xticklabel', [])

% errorbar2([1 2 3 4 5], [nanmean(allmodln(sigtheta)) nanmean(allmodln(nonsigtheta)) nanmean(allmodln(sigboth)) nanmean(allmodln(sigthetaexc)) nanmean(allmodln(sigthetainh))],...
%      [stderr(allmodln(sigtheta)) stderr(allmodln(nonsigtheta)) stderr(allmodln(sigboth)) stderr(allmodln(sigthetaexc)) stderr(allmodln(sigthetainh)) ], 0.3, 'k');

errorbar2([1 2 3 4], [nanmean(allmodln(sigtheta)) nanmean(allmodln(nonsigtheta)) nanmean(allmodln(sigthetaexc)) nanmean(allmodln(sigthetainh))],...
     [stderr(allmodln(sigtheta)) stderr(allmodln(nonsigtheta)) stderr(allmodln(sigthetaexc)) stderr(allmodln(sigthetainh)) ], 0.3, 'k');

figfile = [figdir,'PFC_SWRmodln_Theta_Categories']
%figfile = [figdir,'PFC_ThetaPhase_SWR_Categories']




if savefig1
    %figfile = [figdir,savefigfilename,'/',sprintf('pfcripmodPEC_norm')];
    print('-dpdf', figfile);  print('-dpng', figfile, '-r300'); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end

% a)
[pKW table statsKW] = kruskalwallis([allmodln(sigtheta) allmodln(nonsigtheta)],...
    [ones(1, length(allmodln(sigtheta))) ones(1,length(allmodln(nonsigtheta))).*2]);
[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); 

% b)
[pKW table statsKW] = kruskalwallis([allmodln(sigthetaexc) allmodln(sigthetainh) allmodln(nonsigtheta)],...
    [ones(1,length(allmodln(sigthetaexc))) ones(1,length(allmodln(sigthetainh))).*2 ones(1,length(allmodln(nonsigtheta))).*3]);
[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.


% c)
[pKW table statsKW] = kruskalwallis([allmodln(sigtheta) allmodln(sigthetaexc) allmodln(sigthetainh) allmodln(nonsigtheta)],...
    [ones(1, length(allmodln(sigtheta))) ones(1,length(allmodln(sigthetaexc))).*2 ones(1,length(allmodln(sigthetainh))).*3 ones(1,length(allmodln(nonsigtheta))).*4]);
[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.









% SCATTER PLOTS
% -------------

% -------------------------------
%Theta Modln vs. Ripple Modulation
%-------------------------------------
figure; hold on;
if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end

plot(allmodln(sigtheta), allripmodln(sigtheta), 'go','MarkerSize',8,'LineWidth',8);
plot(allmodln(sigrip), allripmodln(sigrip), 'ro','MarkerSize',4,'LineWidth',4);
plot(allmodln, allripmodln, 'ko','MarkerSize',8);

% figure; hold on;
% 
% plot(allmodln, allripmodln, 'k.','MarkerSize',24); % Plot all if you want to
% 
% %plot(allmodln(sigtheta), allripmodln(sigtheta), 'k.','MarkerSize',24);
% %plot(allmodln(sigrip), allripmodln(sigrip), 'k.','MarkerSize',24);
% plot(allmodln(sigtheta), allripmodln(sigtheta), 'cs','MarkerSize',22,'LineWidth',2);
% plot(allmodln(sigrip), allripmodln(sigrip), 'ro','MarkerSize',20,'LineWidth',2);
title(sprintf('%s: Theta vs %s Ripple Modulation', area, statename),'FontSize',tfont,'Fontweight','normal');
xlabel(['Theta Modln'],'FontSize',xfont,'Fontweight','normal');
ylabel(sprintf('%s Ripple Modln',statename),'FontSize',yfont,'Fontweight','normal');
legend('Sig Theta Phlock',sprintf('Sig %s Ripple',statename),'All Units');

[r2,p2] = corrcoef(allmodln,allripmodln)  
[r2rt,p2rt] = corrcoef(allmodln(allsig),allripmodln(allsig)) 
[r2t,p2t] = corrcoef(allmodln(sigtheta),allripmodln(sigtheta))
[r2r,p2r] = corrcoef(allmodln(sigrip),allripmodln(sigrip)) 

xaxis = 0.5:0.1:1;
plot(xaxis,zeros(size(xaxis)),'k--','LineWidth',2);

str = ''; rstr = ''; tstr = ''; rtstr = '';
if p2(1,2)<0.05, str = '*'; end
if p2(1,2)<0.01, str = '**'; end
if p2(1,2)<0.001, str = '***'; end
if p2r(1,2)<0.05, rstr = '*'; end
if p2r(1,2)<0.01, rstr = '**'; end
if p2r(1,2)<0.001, rstr = '***'; end
if p2t(1,2)<0.05, tstr = '*'; end
if p2t(1,2)<0.01, tstr = '**'; end
if p2t(1,2)<0.001, tstr = '***'; end
if p2rt(1,2)<0.05, rtstr = '*'; end
if p2rt(1,2)<0.01, rtstr = '**'; end
if p2rt(1,2)<0.001, rtstr = '***'; end


if strcmp(area,'CA1')
    if ~strcmp(state,'sleep')
        set(gca,'XLim',[0.55 1])
        text(0.56,1000,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
        text(0.56,900,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
        text(0.56,800,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
        text(0.56,700,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
    else
        set(gca,'XLim',[0.55 1])
        text(0.8,1450,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
        text(0.8,1300,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
        text(0.8,1200,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
        text(0.8,1100,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
    end
else
    if ~strcmp(state,'sleep'); 
        set(gca,'XLim',[0.5 1]); 
        %set(gca,'YLim',[-120 200]); % Modln peak
        %set(gca,'YLim',[-80 100]); % Modln
%        text(0.9,35,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
        text(0.9,30,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
        text(0.9,25,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
        text(0.9,20,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
        text(0.9,15,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
        text(0.9,11,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
        text(0.9,7,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
        text(0.9,3,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');


    else
        set(gca,'XLim',[0.5 1.]);  
        %set(gca,'YLim',[-120 500]); % Modln peak
        % set(gca,'YLim',[-80 250]); % Modln
        text(0.85,35,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
        text(0.85,30,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
        text(0.85,25,sprintf('Signf in Sleep SWR: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
        text(0.85,20,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
        text(0.85,15,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
        text(0.85,10,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
        text(0.85,5,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
        text(0.85,0,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');


    end
end

figfile = [figdir,area,sprintf('_ThetaModlnVs%sRipModln',statename)]
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end



 
% Do a shuffle test - both a normal shuffle test, and a regression shuffle
% ------------------------------------------------------------------------

rshuf=[]; pshuf=[];
for i = 1:1000
    rorder = randperm(length(allmodln));
    randmodln = allmodln(rorder);
    [rtmp, ptmp] = corrcoef(randmodln,allripmodln);
    rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
end

length(find(pshuf<0.05));
prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
if r2(1,2)>prctile95,
    Sig95 = 1,
else
    Sig95 = 0,
end
if r2(1,2)>prctile99,
    Sig99 = 1,
else
    Sig99 = 0,
end



% Regression
% -----------
[b00,bint00,r00,rint00,stats00] = regress(allripmodln', [ones(size(allmodln')) allmodln']);
xpts = min(allmodln):0.01:max(allmodln);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'k-','LineWidth',4);  % Theta vs Rip
rsquare = stats00(1);

% Regression for only SWR modulated cells
% --------------------------------------------
[b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigrip)', [ones(size(allmodln(sigrip)')) allmodln(sigrip)']);
xpts = min(allmodln):0.01:max(allmodln);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'r-','LineWidth',4);  % Theta vs Rip - Only SWR significant
rsquare = stats00(1);

% Regression for only theta modulated cells
% --------------------------------------------
[b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigtheta)', [ones(size(allmodln(sigtheta)')) allmodln(sigtheta)']);
xpts = min(allmodln):0.01:max(allmodln);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'c-','LineWidth',4);  % Theta vs Rip - Only Theta significant
rsquare = stats00(1);

% Regression for both modulated cells
% --------------------------------------------
[b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigboth)', [ones(size(allmodln(sigboth)')) allmodln(sigboth)']);
xpts = min(allmodln):0.01:max(allmodln);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'b-','LineWidth',4);  % Theta vs Rip - Both SWR and Theta significant
rsquare = stats00(1);



% Do regression after shifting data to make intercept 0?
% ------------------------------------------------------
allripmodln_0 = allripmodln-mean(allripmodln);
allmodln_0 = allmodln-mean(allmodln);
[b0,bint0,r0,rint0,stats0] = regress(allripmodln_0',[ones(size(allmodln_0')) allmodln_0']);
bfit0 = b0(1)+b0(2)*xpts;
 
rshuffle = []; pshuffle = []; rsquare_shuffle = []; psquare_shuffle = []; b_shuffle = [];

% % Shuffling
% % ---------
for n=1:1000
    rorder = randperm(length(allmodln));
    randmodln = allmodln(rorder);
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, allripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(allripmodln', [ones(size(randmodln')) randmodln']);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end

% Significance from shuffle
prctile(rsquare_shuffle,99); prctile(r_shuffle,99); %figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
pshuf2 = length(find(r2(1,2)<r_shuffle))/n

% Get regression corresponding to 99 percentile
idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,99));
idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
plot(xpts,bfitsh,'k--','LineWidth',2);  % Theta vs Rip - 99% shuffle line
% 
% % and 95 %tile
% idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,95));
% idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
% bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
% plot(xpts,bfitsh,'g--','LineWidth',2);  % Theta vs Rip - 95% shuffle line
























% -------------------------------% -------------------------------% -------------------------------
% -------------------------------% -------------------------------% -------------------------------



%Kappas vs. Ripple Modulation
%-------------------------------------
figure; hold on;
if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
figure; hold on;
if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end

plot(allkappas(sigtheta), allripmodln(sigtheta), 'go','MarkerSize',8,'LineWidth',8);
plot(allkappas(sigrip), allripmodln(sigrip), 'ro','MarkerSize',4,'LineWidth',4);
plot(allkappas, allripmodln, 'ko','MarkerSize',8);

% plot(allkappas, allripmodln, 'k.','MarkerSize',24); % Plot all
% %plot(allkappas(sigtheta), allripmodln(sigtheta), 'k.','MarkerSize',24);
% %plot(allkappas(sigrip), allripmodln(sigrip), 'k.','MarkerSize',24);
% plot(allkappas(sigtheta), allripmodln(sigtheta), 'cs','MarkerSize',22,'LineWidth',2);
% plot(allkappas(sigrip), allripmodln(sigrip), 'ro','MarkerSize',20,'LineWidth',2);
title(sprintf('%s: Theta vs %s Ripple Modulation', area, statename),'FontSize',tfont,'Fontweight','normal');
xlabel(['Kappa (Theta Conc Parm)'],'FontSize',xfont,'Fontweight','normal');
ylabel(sprintf('%s Ripple Modln',statename),'FontSize',yfont,'Fontweight','normal');
legend('Sig Theta Phlock',sprintf('Sig %s Ripple',statename),'All Units');

[r1,p1] = corrcoef(allkappas,allripmodln) 
[r1rt,p1rt] = corrcoef(allkappas(sigboth),allripmodln(sigboth))
[r1t,p1t] = corrcoef(allkappas(sigtheta),allripmodln(sigtheta))
[r1r,p1r] = corrcoef(allkappas(sigrip),allripmodln(sigrip)) 

xaxis = 0:0.1:1.5;
plot(xaxis,zeros(size(xaxis)),'k--','LineWidth',2);

str = ''; rstr = ''; tstr = ''; rtstr = '';
if p1(1,2)<0.05, str = '*'; end
if p1(1,2)<0.01, str = '**'; end
if p1(1,2)<0.001, str = '***'; end
if p1r(1,2)<0.05, rstr = '*'; end
if p1r(1,2)<0.01, rstr = '**'; end
if p1r(1,2)<0.001, rstr = '***'; end
if p1t(1,2)<0.05, tstr = '*'; end
if p1t(1,2)<0.01, tstr = '**'; end
if p1t(1,2)<0.001, tstr = '***'; end
if p1rt(1,2)<0.05, rtstr = '*'; end
if p1rt(1,2)<0.01, rtstr = '**'; end
if p1rt(1,2)<0.001, rtstr = '***'; end


if strcmp(area,'CA1')
    %set(gca,'XLim',[0 2200]); set(gca,'YLim',[0 1600]);
    if ~strcmp(state,'sleep')
        text(1.1,900,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
        text(1.1,800,sprintf('Signf in Run: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
        text(1.1,700,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
        text(1.1,600,sprintf('R = %0.2f%s',r1(1,2), str),'FontSize',30,'Fontweight','normal');
    end
else % PFC
    if ~strcmp(state,'sleep')
        set(gca,'XLim',[-0.02 1.5]); 
        %set(gca,'YLim',[-120 200]); % Modln peak
        %set(gca,'YLim',[-80 100]); % Modln
      %  text(1.1,35,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
        text(1.1,30,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
        text(1.1,25,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
        text(1.1,20,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
        text(1.1,15,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
        text(1.1,11,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
        text(1.1,7,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
        text(1.1,3,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');
       
%         set(gca,'XLim',[-0.02 1]);  
%         %set(gca,'YLim',[-120 200]); % Modln peak
%         %set(gca,'YLim',[-80 90]); % Modln
%         text(0.05,40,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
%         text(0.62,35,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
%         text(0.62,30,sprintf('Signf in Ripple: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
%         text(0.62,25,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
%         text(0.62,20,sprintf('R = %0.2f%s',r1(1,2), str),'FontSize',30,'Fontweight','normal');
%         text(0.62,5,sprintf('Rr = %0.2f%s',r1r(1,2), rstr),'FontSize',30,'Fontweight','normal');
%         text(0.62,10,sprintf('Rt = %0.2f%s',r1t(1,2), tstr),'FontSize',30,'Fontweight','normal');
%         text(0.62,15,sprintf('Rrt = %0.2f%s',r1rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');

    else
        set(gca,'XLim',[-0.02 1.2]);  
        %set(gca,'YLim',[-120 500]); % Modln peak
        %set(gca,'YLim',[-80 250]); % Modln
        text(0.2,40,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
        text(0.4,35,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
        text(0.4,30,sprintf('Signf in Sleep SWR: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
        text(0.4,25,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
        text(0.4,20,sprintf('R = %0.2f%s',r1(1,2), str),'FontSize',30,'Fontweight','normal');
        text(0.4,5,sprintf('Rr = %0.2f%s',r1r(1,2), rstr),'FontSize',30,'Fontweight','normal');
        text(0.4,10,sprintf('Rt = %0.2f%s',r1t(1,2), tstr),'FontSize',30,'Fontweight','normal');
        text(0.4,15,sprintf('Rrt = %0.2f%s',r1rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');

    end
end
 A = [length(sigrip) length(sigtheta)]; I = length(sigboth);
        figure; venn(A,I,'FaceColor',{'r','g'},'FaceAlpha',{1,0.6},'EdgeColor','black')
    axis off;
figfile = [figdir,area,sprintf('_ThetaKappaVs%sRipModln',statename)];
if savefig1==1,   
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end



% Do a shuffle test - both a normal shuffle test, and a regression shuffle
% ------------------------------------------------------------------------
rshuf=[]; pshuf=[];
for i = 1:1000
    rorder = randperm(length(allkappas));
    randmodln = allkappas(rorder);
    [rtmp, ptmp] = corrcoef(randmodln,allripmodln);
    rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
end

length(find(pshuf<0.05));
prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
if r1(1,2)>prctile95,
    Sig95 = 1,
else
    Sig95 = 0,
end
if r1(1,2)>prctile99,
    Sig99 = 1,
else
    Sig99 = 0,
end

% Regression
% -----------
[b00,bint00,r00,rint00,stats00] = regress(allripmodln', [ones(size(allkappas')) allkappas']);
xpts = min(allkappas):0.01:max(allkappas);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'k-','LineWidth',4);  % Theta vs Rip
rsquare = stats00(1);


% Regression for only SWR modulated cells
% --------------------------------------------
[b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigrip)', [ones(size(allkappas(sigrip)')) allkappas(sigrip)']);
xpts = min(allkappas):0.01:max(allkappas);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'r-','LineWidth',4);  % Theta vs Rip - Only SWR significant
rsquare = stats00(1);


% Do regression after shifting data to make intercept 0
% ------------------------------------------------------
allripmodln_0 = allripmodln-mean(allripmodln);
allkappas_0 = allkappas-mean(allkappas);
[b0,bint0,r0,rint0,stats0] = regress(allripmodln_0',[ones(size(allkappas_0')) allkappas_0']);
bfit0 = b0(1)+b0(2)*xpts;
 
rshuffle = []; pshuffle = []; rsquare_shuffle = []; psquare_shuffle = []; b_shuffle = [];

% % Shuffling
% % ---------
for n=1:1000
    rorder = randperm(length(allkappas));
    randmodln = allkappas(rorder);
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, allripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allkappas_0')) allkappas_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(allripmodln', [ones(size(randmodln')) randmodln']);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end

% Significance from shuffle
prctile(rsquare_shuffle,99); prctile(r_shuffle,99); %figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
pshuf1 = length(find(r2(1,2)<r_shuffle))/n

% Get regression corresponding to 99 percentile
idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,99));
idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
plot(xpts,bfitsh,'k--','LineWidth',2);  % Theta vs Rip - 99% shuffle line

% % and 95 %tile
% idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,95));
% idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
% bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
% plot(xpts,bfitsh,'g--','LineWidth',2);  % Theta vs Rip - 95% shuffle line





figure; hold on;
plot(allcellfr,allripmodln,'bo');
plot(allcellfr(sigrip),allripmodln(sigrip),'rx');
title('Rip Modln vs Fir Rate')
[r,p] = corrcoef(allcellfr,allripmodln)
[rs,ps] = corrcoef(allcellfr(sigrip),allripmodln(sigrip))

% figure; hold on;
% plot(allcellfr,allrdmmodln,'bo');
% plot(allcellfr(sigrip),allrdmmodln(sigrip),'rx');
% title('Rdm Resp Modln vs Fir Rate')
% 
% 
% figure; hold on;
% plot(allcellfr,allmodln,'bo');
% plot(allcellfr(sigrip),allmodln(sigrip),'rx');
% title('Theta Modln vs Fir Rate')
% 
% 
% figure; hold on;
% plot(allcellfr,allkappas,'bo');
% plot(allcellfr(sigrip),allkappas(sigrip),'rx');
% title('Theta Kappas vs Fir Rate')






%keyboard;




% -------------------------------
% -------------------------------
% -------------------------------
% -------------------------------
% -------------------------------


% -------------------------------
%Theta Kappas vs. Ripple Modulation - Radom Resp Data
%-------------------------------------
figure; hold on;
if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end

%plot(allkappas, allrdmmodln, 'k.','MarkerSize',24); % Plot all if you want to

%plot(allkappas(sigtheta), allrdmmodln(sigtheta), 'k.','MarkerSize',24);
%plot(allkappas(sigrip), allrdmmodln(sigrip), 'k.','MarkerSize',24);
%plot(allkappas(sigtheta), allrdmmodln(sigtheta), 'cs','MarkerSize',22,'LineWidth',2);
%plot(allkappas(sigrip), allrdmmodln(sigrip), 'ro','MarkerSize',20,'LineWidth',2);
title(sprintf('%s: Theta vs %s Rdm Resp Modulation', area, statename),'FontSize',tfont,'Fontweight','normal');
xlabel(['Theta Modln'],'FontSize',xfont,'Fontweight','normal');
ylabel(sprintf('%s Ripple Modln',statename),'FontSize',yfont,'Fontweight','normal');
legend('All Units','Sig Theta Phlock',sprintf('Sig %s Ripple',statename));

%[r2,p2] = corrcoef(allkappas,allrdmmodln)  
%[r2rt,p2rt] = corrcoef(allkappas(allsig),allrdmmodln(allsig)) 
%[r2t,p2t] = corrcoef(allkappas(sigtheta),allrdmmodln(sigtheta))
%[r2r,p2r] = corrcoef(allkappas(sigrip),allrdmmodln(sigrip)) 

xaxis = 0.5:0.1:1;
plot(xaxis,zeros(size(xaxis)),'k--','LineWidth',2);

str = ''; rstr = ''; tstr = ''; rtstr = '';
if p2(1,2)<0.05, str = '*'; end
if p2(1,2)<0.01, str = '**'; end
if p2(1,2)<0.001, str = '***'; end
if p2r(1,2)<0.05, rstr = '*'; end
if p2r(1,2)<0.01, rstr = '**'; end
if p2r(1,2)<0.001, rstr = '***'; end
if p2t(1,2)<0.05, tstr = '*'; end
if p2t(1,2)<0.01, tstr = '**'; end
if p2t(1,2)<0.001, tstr = '***'; end
if p2rt(1,2)<0.05, rtstr = '*'; end
if p2rt(1,2)<0.01, rtstr = '**'; end
if p2rt(1,2)<0.001, rtstr = '***'; end


if strcmp(area,'CA1')
    if ~strcmp(state,'sleep')
        set(gca,'XLim',[0.55 1])
    
        text(0.56,700,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
    else
        set(gca,'XLim',[0.55 1])
        text(0.8,1450,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
        text(0.8,1300,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
        text(0.8,1200,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
        text(0.8,1100,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
    end
else
    if ~strcmp(state,'sleep'); 
        set(gca,'XLim',[0.5 1]); 
        %set(gca,'YLim',[-120 200]); % Modln peak
        %set(gca,'YLim',[-80 100]); % Modln
        text(0.82,20,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
        text(0.82,5,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
        text(0.72,5,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
        text(0.92,5,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');


    else
        set(gca,'XLim',[0.5 1.]);  
        %set(gca,'YLim',[-120 500]); % Modln peak
        % set(gca,'YLim',[-80 250]); % Modln
        text(0.75,20,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
        text(0.75,5,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
        text(0.65,5,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
        text(0.85,5,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');


    end
end

figfile = [figdir,area,sprintf('_ThetaModlnVs%sRdmModln',statename)]
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end



 
% Do a shuffle test - both a normal shuffle test, and a regression shuffle
% ------------------------------------------------------------------------

% rshuf=[]; pshuf=[];
% for i = 1:1000
%     rorder = randperm(length(allkappas));
%     randmodln = allkappas(rorder);
%     [rtmp, ptmp] = corrcoef(randmodln,allrdmmodln);
%     rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
% end
% 
% length(find(pshuf<0.05));
% prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
% if r2(1,2)>prctile95,
%     Sig95 = 1,
% else
%     Sig95 = 0,
% end
% if r2(1,2)>prctile99,
%     Sig99 = 1,
% else
%     Sig99 = 0,
% end



% Regression
% -----------
[b00,bint00,r00,rint00,stats00] = regress(allrdmmodln', [ones(size(allkappas')) allkappas']);
xpts = min(allkappas):0.01:max(allkappas);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'k-','LineWidth',4);  % Theta vs Rip
rsquare = stats00(1);

% Regression for only SWR modulated cells
% --------------------------------------------
[b00,bint00,r00,rint00,stats00] = regress(allrdmmodln(sigrip)', [ones(size(allkappas(sigrip)')) allkappas(sigrip)']);
xpts = min(allkappas):0.01:max(allkappas);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'r-','LineWidth',4);  % Theta vs Rip - Only SWR significant
rsquare = stats00(1);

% Regression for only theta modulated cells
% --------------------------------------------
[b00,bint00,r00,rint00,stats00] = regress(allrdmmodln(sigtheta)', [ones(size(allkappas(sigtheta)')) allkappas(sigtheta)']);
xpts = min(allkappas):0.01:max(allkappas);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'c-','LineWidth',4);  % Theta vs Rip - Only Theta significant
rsquare = stats00(1);

% Regression for both modulated cells
% --------------------------------------------
[b00,bint00,r00,rint00,stats00] = regress(allrdmmodln(sigboth)', [ones(size(allkappas(sigboth)')) allkappas(sigboth)']);
xpts = min(allkappas):0.01:max(allkappas);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'b-','LineWidth',4);  % Theta vs Rip - Both SWR and Theta significant
rsquare = stats00(1);



% Do regression after shifting data to make intercept 0?
% ------------------------------------------------------
allrdmmodln_0 = allrdmmodln-mean(allrdmmodln);
allkappas_0 = allkappas-mean(allkappas);
[b0,bint0,r0,rint0,stats0] = regress(allrdmmodln_0',[ones(size(allkappas_0')) allkappas_0']);
bfit0 = b0(1)+b0(2)*xpts;
 
rshuffle = []; pshuffle = []; rsquare_shuffle = []; psquare_shuffle = []; b_shuffle = [];

% % Shuffling
% % ---------
for n=1:1000
    rorder = randperm(length(allkappas));
    randmodln = allkappas(rorder);
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, allrdmmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allkappas_0')) allkappas_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(allrdmmodln', [ones(size(randmodln')) randmodln']);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end

% Significance from shuffle
prctile(rsquare_shuffle,99); prctile(r_shuffle,99); %figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
pshuf2 = length(find(r2(1,2)<r_shuffle))/n

% Get regression corresponding to 99 percentile
idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,99));
idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
plot(xpts,bfitsh,'k--','LineWidth',2);  % Theta vs Rip - 99% shuffle line
% 
% % and 95 %tile
% idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,95));
% idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
% bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
% plot(xpts,bfitsh,'g--','LineWidth',2);  % Theta vs Rip - 95% shuffle line








% -------------------------------
%Theta Modln vs. Ripple Modulation - Radom Resp Data
%-------------------------------------
figure; hold on;
if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end

plot(allmodln, allrdmmodln, 'k.','MarkerSize',24); % Plot all if you want to

%plot(allmodln(sigtheta), allrdmmodln(sigtheta), 'k.','MarkerSize',24);
%plot(allmodln(sigrip), allrdmmodln(sigrip), 'k.','MarkerSize',24);
plot(allmodln(sigtheta), allrdmmodln(sigtheta), 'cs','MarkerSize',22,'LineWidth',2);
plot(allmodln(sigrip), allrdmmodln(sigrip), 'ro','MarkerSize',20,'LineWidth',2);
title(sprintf('%s: Theta vs %s Rdm Resp Modulation', area, statename),'FontSize',tfont,'Fontweight','normal');
xlabel(['Theta Modln'],'FontSize',xfont,'Fontweight','normal');
ylabel(sprintf('%s Ripple Modln',statename),'FontSize',yfont,'Fontweight','normal');
legend('All Units','Sig Theta Phlock',sprintf('Sig %s Ripple',statename));

[r2,p2] = corrcoef(allmodln,allrdmmodln)  
[r2rt,p2rt] = corrcoef(allmodln(allsig),allrdmmodln(allsig)) 
[r2t,p2t] = corrcoef(allmodln(sigtheta),allrdmmodln(sigtheta))
[r2r,p2r] = corrcoef(allmodln(sigrip),allrdmmodln(sigrip)) 

xaxis = 0.5:0.1:1;
plot(xaxis,zeros(size(xaxis)),'k--','LineWidth',2);

str = ''; rstr = ''; tstr = ''; rtstr = '';
if p2(1,2)<0.05, str = '*'; end
if p2(1,2)<0.01, str = '**'; end
if p2(1,2)<0.001, str = '***'; end
if p2r(1,2)<0.05, rstr = '*'; end
if p2r(1,2)<0.01, rstr = '**'; end
if p2r(1,2)<0.001, rstr = '***'; end
if p2t(1,2)<0.05, tstr = '*'; end
if p2t(1,2)<0.01, tstr = '**'; end
if p2t(1,2)<0.001, tstr = '***'; end
if p2rt(1,2)<0.05, rtstr = '*'; end
if p2rt(1,2)<0.01, rtstr = '**'; end
if p2rt(1,2)<0.001, rtstr = '***'; end


if strcmp(area,'CA1')
    if ~strcmp(state,'sleep')
        set(gca,'XLim',[0.55 1])
    
        text(0.56,700,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
    else
        set(gca,'XLim',[0.55 1])
        text(0.8,1450,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
        text(0.8,1300,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
        text(0.8,1200,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
        text(0.8,1100,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
    end
else
    if ~strcmp(state,'sleep'); 
        set(gca,'XLim',[0.5 1]); 
        %set(gca,'YLim',[-120 200]); % Modln peak
        %set(gca,'YLim',[-80 100]); % Modln
        text(0.82,20,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
        text(0.82,5,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
        text(0.72,5,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
        text(0.92,5,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');


    else
        set(gca,'XLim',[0.5 1.]);  
        %set(gca,'YLim',[-120 500]); % Modln peak
        % set(gca,'YLim',[-80 250]); % Modln
        text(0.75,20,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
        text(0.75,5,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
        text(0.65,5,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
        text(0.85,5,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');


    end
end

figfile = [figdir,area,sprintf('_ThetaModlnVs%sRdmModln',statename)]
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end



 
% Do a shuffle test - both a normal shuffle test, and a regression shuffle
% ------------------------------------------------------------------------

rshuf=[]; pshuf=[];
for i = 1:1000
    rorder = randperm(length(allmodln));
    randmodln = allmodln(rorder);
    [rtmp, ptmp] = corrcoef(randmodln,allrdmmodln);
    rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
end

length(find(pshuf<0.05));
prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
if r2(1,2)>prctile95,
    Sig95 = 1,
else
    Sig95 = 0,
end
if r2(1,2)>prctile99,
    Sig99 = 1,
else
    Sig99 = 0,
end



% Regression
% -----------
[b00,bint00,r00,rint00,stats00] = regress(allrdmmodln', [ones(size(allmodln')) allmodln']);
xpts = min(allmodln):0.01:max(allmodln);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'k-','LineWidth',4);  % Theta vs Rip
rsquare = stats00(1);

% Regression for only SWR modulated cells
% --------------------------------------------
[b00,bint00,r00,rint00,stats00] = regress(allrdmmodln(sigrip)', [ones(size(allmodln(sigrip)')) allmodln(sigrip)']);
xpts = min(allmodln):0.01:max(allmodln);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'r-','LineWidth',4);  % Theta vs Rip - Only SWR significant
rsquare = stats00(1);

% Regression for only theta modulated cells
% --------------------------------------------
[b00,bint00,r00,rint00,stats00] = regress(allrdmmodln(sigtheta)', [ones(size(allmodln(sigtheta)')) allmodln(sigtheta)']);
xpts = min(allmodln):0.01:max(allmodln);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'c-','LineWidth',4);  % Theta vs Rip - Only Theta significant
rsquare = stats00(1);

% Regression for both modulated cells
% --------------------------------------------
[b00,bint00,r00,rint00,stats00] = regress(allrdmmodln(sigboth)', [ones(size(allmodln(sigboth)')) allmodln(sigboth)']);
xpts = min(allmodln):0.01:max(allmodln);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'b-','LineWidth',4);  % Theta vs Rip - Both SWR and Theta significant
rsquare = stats00(1);



% Do regression after shifting data to make intercept 0?
% ------------------------------------------------------
allrdmmodln_0 = allrdmmodln-mean(allrdmmodln);
allmodln_0 = allmodln-mean(allmodln);
[b0,bint0,r0,rint0,stats0] = regress(allrdmmodln_0',[ones(size(allmodln_0')) allmodln_0']);
bfit0 = b0(1)+b0(2)*xpts;
 
rshuffle = []; pshuffle = []; rsquare_shuffle = []; psquare_shuffle = []; b_shuffle = [];

% % Shuffling
% % ---------
for n=1:1000
    rorder = randperm(length(allmodln));
    randmodln = allmodln(rorder);
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, allrdmmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(allrdmmodln', [ones(size(randmodln')) randmodln']);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end

% Significance from shuffle
prctile(rsquare_shuffle,99); prctile(r_shuffle,99); %figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
pshuf2 = length(find(r2(1,2)<r_shuffle))/n

% Get regression corresponding to 99 percentile
idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,99));
idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
plot(xpts,bfitsh,'k--','LineWidth',2);  % Theta vs Rip - 99% shuffle line
% 
% % and 95 %tile
% idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,95));
% idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
% bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
% plot(xpts,bfitsh,'g--','LineWidth',2);  % Theta vs Rip - 95% shuffle line




% 


