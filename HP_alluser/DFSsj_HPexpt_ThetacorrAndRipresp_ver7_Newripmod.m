% Ver 4: Sync with everyone

% Ver3: For ripple corrln, combine the ripplemod across epochs, and then do the corrln.
% So the filter function will simply return the correlation function

% Ver2, Dec 2013 - Implement combining data across epochs
% Raw Data Files Stay Same - Only Gatherdata Files will change


% For ripple modulated and unmodulated cells, get corrcoeff and coactivez using the ripple response
% Instead of aligning to ripples again, use the saved output oin ripplemod files

% For consolidating across epochs:
% If I run corrcoeff and coactivez using DFAsj_getripresp_corrandcoactz, I will have to take mean when
% consolidating across epochs. Instead, I can run a new functions "DFAsj_gettrialResps", then combine these
% across epochs, and run corrcoeff and coactivez here in this function.


clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
%savedir = '/opt/data15/gideon/HP_ProcessedData/';


[y, m, d] = datevec(date);
% Version 4 onwards
% -----------------
%if runscript==1
    % %IMP! CA1 all and PFC ripmod vs ripunmod.
    % %-----------------------------------------------------------
    
    % X6:val=1 does the main plot, and val=2 does RipUnmod
     %val=1;savefile = [savedir 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_X6']; area = 'CA1allPFCripmod'; clr = 'r';
     %val=2;savefile = [savedir 'HP_ThetacovAndRipUnmodcorr_std3_speed4_ntet2_alldata_X6']; area = 'CA1allPFCripUnmod'; clr = 'b';
    
    % IMP! PFC ripmod and PFC ripmod;
    % %------------------------------
    %val=3;savefile = [savedir 'HP_PFCripmod_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_',num2str(m),'-',num2str(d),'-',num2str(y)]; area = 'PFCripmod'; clr = 'c';
% else
%     val=1;savefile = [savedir 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_3-7-2014']; area = 'CA1allPFCripmod'; clr = 'r';
%     %val=2;savefile = [savedir 'HP_ThetacovAndRipUnmodcorr_std3_speed4_ntet2_alldata_3-7-2014']; area = 'CA1allPFCripUnmod'; clr = 'b';
%     %val=3;savefile = [savedir 'HP_PFCripmod_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_3-7-2014']; area = 'PFCripmod'; clr = 'c';
% end


% X6: val=4 and 5 does PFC Excited and Inhibited
% -----------------------------------------------
%val=4;savefile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCExc_X6']; area = 'CA1allPFCExc'; clr = 'r';
%val=5;savefile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCInh_X6']; area = 'CA1allPFCInh'; clr = 'b';

% %val=0,6, and 7 will do new CA1: rpmodtag
% val=0;savefile = [savedir 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_X7']; area = 'CA1allPFCripmod'; clr = 'r';
%val=6;savefile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCExc_X7'], area = 'CA1allPFCExc'; clr = 'r';
%val=7;savefile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCInh_X7']; area = 'CA1allPFCExc'; clr = 'r';

% -------
% X8: IMP
% -------

% val=8 etc: same as val=7, but CA2 has been removed
 val=8; savefile = [savedir 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_X9_Newripmod']; area = 'CA1allPFCripmod'; clr = 'r';
% val=9;savefile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCExc_X9_Newripmod'], area = 'CA1allPFCExc'; clr = 'r';%
% val=10;savefile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCInh_X9_Newripmod']; area = 'CA1allPFCInh'; clr = 'b';

%val=11;savefile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCExc_X8_Wtr_noiCA1']; area = 'CA1PFCExc'; clr = 'r';
%val=12;savefile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCInh_X8_Wtr_noiCA1']; area = 'CA1PFCInh'; clr = 'b';

% Rip Unmod 

%%val=13;savefile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCUnmod_X8']; area = 'CA1allPFCUnmod'; clr = 'c';



% Misc - Old
% ------------
% savefile = [savedir 'HP_thetaandripmodcorr_corrandcoactz_PFC']; area = 'PFC'; clr = 'b'; % PFC
% savefile = [savedir 'HP_thetaandripunmodcorr_corrandcoactz_PFC']; area = 'PFC'; clr = 'b'; % PFC
% savefile = [savedir 'HP_thetaandripmodcorr_corrandcoactz_CA1']; area = 'CA1'; clr = 'r';
% savefile = [savedir 'HP_thetaandripunmodcorr_corrandcoactz_CA1']; area = 'CA1'; clr = 'r';
% savefile = [savedir 'HP_thetaandripmodcorr_corrandcoactz_CA1PFC']; area = 'CA1PFC'; clr = 'c';
% savefile = [savedir 'HP_thetaandripunmodcorr_corrandcoactz_CA1PFC']; area = 'CA1PFC'; clr = 'c';
% savefile = [savedir 'HP_thetaandripmodcorr_corrandcoactz_CA1allPFC']; area = 'CA1allPFC'; clr = 'c';
% savefile = [savedir 'HP_thetaandripunmodcorr_corrandcoactz_CA1allPFC']; area = 'CA1allPFC'; clr = 'c';



savefig1=0;


% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    %animals = {'HPa','HPb','HPc','Nadal','Rosenthal'}; %BORG NOT HERE COZ NO CA1 UNITS
    animals = {'HPa','HPb','HPc','Ndl','Rtl'}; %BORG NOT HERE COZ NO CA1 UNITS
    
    %animals = {'HPa','HPb','HPc'};
    
 %       animals = {'Borg'};
%animals={'HPa'};
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    %dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
   % runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
            runepochfilter = 'isequal($type, ''run'')';

    % %Cell filter
    % %-----------
    
    % %IMP! CA1theta-PFCRipmodulated
    % %--------------------------------------------------------------
  
    
    switch val
        case 1 
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100)  && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'')'};
        case 8 
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100)  && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'')'};
        
        case 2
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100)  && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''n'') && strcmp($FStag, ''n'')'};
            % %IMP! PFCRipmodulated only
            % %------------------------
        case 3
            cellpairfilter = {'allcomb','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'')'};
        case 4
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc'') && strcmp($FStag, ''n'')'}
        case 9
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc'') && strcmp($FStag, ''n'')'}
                 
        case 5
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh'') && strcmp($FStag, ''n'')'};
        case 10
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh'') && strcmp($FStag, ''n'')'};
            
%         case 13 % SAME AS CASE 2
%             cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''n'') && strcmp($FStag, ''n'')'};
%             
            
            % New CA1 criterion
        case 6
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc'') && strcmp($FStag, ''n'')'}
            
        case 7
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh'') && strcmp($FStag, ''n'')'}
        
        case 0
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'')'};
         
            
              
        case 11
            cellpairfilter = {'allcomb','strcmp($area, ''CA1'') && ($numspikes > 100) && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc'') && strcmp($FStag, ''n'')'};
            
        case 12
            cellpairfilter = {'allcomb','strcmp($area, ''CA1'') && ($numspikes > 100) && strcmp($FStag, ''n'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh'') && strcmp($FStag, ''n'')'};
            
    
            
    end
    
    
    % Misc - OLD
    % ----
    
    % %PFC
    % %----
    % cellpairfilter = {'allcomb','strcmp($area, ''PFC'') && strcmp($ripmodtag, ''y'')','strcmp($area, ''PFC'') && strcmp($ripmodtag, ''y'')'}; % Ripple mod
    %cellpairfilter = {'allcomb','strcmp($area, ''PFC'') && strcmp($ripmodtag, ''n'')','strcmp($area, ''PFC'') && strcmp($ripmodtag, ''n'')'}; % Ripple unmod
    % %CA1
    % %----
    %cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($ripmodtag, ''y'')','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($ripmodtag, ''y'')'};
    %cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($ripmodtag, ''n'')','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($ripmodtag, ''n'')'};
    % %CA1-PFC
    % %--------
    %cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($ripmodtag, ''y'')','strcmp($area, ''PFC'') && strcmp($ripmodtag, ''y'')'};
    %cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($ripmodtag, ''n'')','strcmp($area, ''PFC'') && strcmp($ripmodtag, ''n'')'};
    
    % %CA1all-PFCmodulated
    % %------------------------
    %cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($meanrate > 0.2)','strcmp($area, ''PFC'') && strcmp($ripmodtag, ''y'')'};
    %cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($meanrate > 0.2)','strcmp($area, ''PFC'') && strcmp($ripmodtag, ''n'')'};
    
    
    
    
    
    % Time filter - none.
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    %     timefilter_place = { {'DFTFsj_getlinstate', '(($state ~= -1) & (abs($linearvel) >= 5))', 6},...
    %         {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',2} };
    
    % Use absvel instead of linearvel
    
    timefilter_place_new = { {'DFTFsj_getvelpos', '(($absvel >= 5))'},...
        {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',3} };
    
    % Iterator
    % --------
    iterator = 'singlecellanal';  % Have defined cellpairfilter. Can also use cellpair iterator with cell defn
    
    % Filter creation
    % ----------------
    
    % Ripp corrcoeff and coactivez
    %     modf = createfilter('animal',animals,'days',dayfilter,'epochs',runepochfilter, 'cellpairs',...
    %         cellpairfilter, 'iterator', iterator);
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'cellpairs',...
        cellpairfilter, 'iterator', iterator);
    
    %     %thetaf = createfilter('animal',animals,'days',dayfilter,'epochs',runepochfilter, 'cellpairs',...
    %         cellpairfilter, 'excludetime', timefilter_place, 'iterator', iterator);
    
    %     %thetaf = createfilter('animal',animals,'days',dayfilter,'epochs',runepochfilter, 'cellpairs',...
    %         cellpairfilter, 'excludetime', timefilter_place_new, 'iterator', iterator);
    
    thetaf = createfilter('animal',animals,'epochs',runepochfilter, 'cellpairs',...
        cellpairfilter, 'excludetime', timefilter_place_new, 'iterator', iterator);
    
    
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    
    % % Need only the ripplemod structure for all of this.
    % % For calculating ripplealigned resp from scratch, you will need spikes, ripples, tetinfo, and pos
    modf = setfilterfunction(modf,'DFAsj_getripmoddata4',{'ripplemod'}); % ONLY RETURN RIPPLEMOD/TRIALRESPS DATA
    %modf = setfilterfunction(modf,'DFAsj_getripresp_corrandcoactz',{'ripplemod'}); %
    %modf = setfilterfunction(modf,'DFAsj_getripresp_corrandcoactz',{'ripplemod','cellinfo','spikes', 'ripples', 'tetinfo', 'pos'}); %
    
    % %thetaf = setfilterfunction(thetaf,'DFAsj_calcxcorrmeasures', {'spikes'},'forripples',0);
    % %thetaf = setfilterfunction(thetaf,'DFAsj_getthetacovariogram', {'spikes'});
    % %thetaf = setfilterfunction(thetaf,'DFAsj_getthetacrosscov', {'spikes'});
    
    thetaf = setfilterfunction(thetaf,'DFAsj_getthetacrosscov_timecondition4', {'spikes'},'thrstime',1); % With includetime condition
    
    
    
    % Run analysis
    % ------------
    modf = runfilter(modf);
    thetaf = runfilter(thetaf);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata savedir
        save(savefile);
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------



% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
[y, m, d] = datevec(date);

% Version 4onwards
% Filename of gatherdata. If generating, put current date. If not, then load a file from previous date
% ----------------------------------------------------------------------------------------------------

% % IMP! - CA1 (theta modulated only) and PFC ripmod vs ripunmod. Also Compare to Sleep Ripmod computed in other Script
% ------------------------------------------------------------------------
switch val
    case 1
     %   if gatherdata
            gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_gather_X6']; area = 'CA1allPFCripmod'; kind = 'ripmod'; state = '';
    
    case 2
        %if gatherdata
            gatherdatafile = [savedir 'HP_ThetacovAndRipUnmodcorr_std3_speed4_ntet2_alldata_gather_X6']; area = 'CA1allPFCUnripmod'; kind = 'ripUnmod'; state = '';
        %else
         %   gatherdatafile = [savedir 'HP_ThetacovAndRipUnmodcorr_std3_speed4_ntet2_alldata_gather_3-7-2014'];
        %end
        % % IMP! PFC Ripmod Only
        % -----------------------
%     case 3
%         if gatherdata
%             gatherdatafile = [savedir 'HP_PFCripmod_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_gather_',num2str(m),'-',num2str(d),'-',num2str(y)]; area = 'PFCripmod'; kind = 'ripmod'; state = '';
%         else
%             gatherdatafile = [savedir 'HP_PFCripmod_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_gather_3-7-2014'];
%         end
    case 4
        gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCExc_gather_X6']; area = 'CA1allPFCExc'; kind = 'PFCExc'; state = '';
  
    case 5
        gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCInh_gather_X6']; area = 'CA1allPFCInh'; kind = 'PFCInh'; state = '';
   
        
    % New CA1
    case 6
        gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCExc_gather_X7'], area = 'CA1allPFCExc'; kind = 'PFCExc'; state = '';
    case 7
        gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCInh_gather_X7'], area = 'CA1allPFCInh'; kind = 'PFCInh'; state = '';
    case 0
          gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_gather_X7']; area = 'CA1allPFCripmod'; kind = 'ripmod'; state = '';
    case 8
     %   if gatherdata
            gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_gather_X9_Newripmod']; area = 'CA1allPFCripmod'; kind = 'ripmod'; state = '';
      %  else
       %     gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_gather_5-2-2014']
       % end
    case 9
        gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCExc_gather_X9_Newripmod']; area = 'CA1allPFCExc'; kind = 'PFCExc'; state = '';
        
    case 10
        gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCInh_gather_X9_Newripmod']; area = 'CA1allPFCInh'; kind = 'PFCInh'; state = '';
        

        
    case 11
        gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCExc_gather_X8_Wtr_noiCA1']; area = 'CA1PFCExc'; kind = 'PFCExc'; state = '';
        

    case 12
        gatherdatafile = [savedir 'HP_ThetacovAndRipmodcorr_CA1_PFCInh_gather_X8_Wtr_noiCA1']; area = 'CA1PFCInh'; kind = 'PFCInh'; state = '';
        


end

% Pre-version4
% ------------
%gatherdatafile = [savedir 'HP_thetaandripmodcorr_corrandcoactz_PFC_gather']; area = 'PFC'; kind = 'mod'; state = '';
% gatherdatafile = [savedir 'HP_thetaandripunmodcorr_corrandcoactz_PFC_gather']; area = 'PFC'; kind = 'unmod'; state = '';
% gatherdatafile = [savedir 'HP_thetaandripmodcorr_corrandcoactz_CA1_gather']; area = 'CA1'; kind = 'mod'; state = '';
% gatherdatafile = [savedir 'HP_thetaandripunmodcorr_corrandcoactz_CA1_gather']; area = 'CA1'; kind = 'unmod'; state = '';
% gatherdatafile = [savedir 'HP_thetaandripmodcorr_corrandcoactz_CA1PFC_gather']; area = 'CA1PFC'; kind = 'mod'; state = '';
% gatherdatafile = [savedir 'HP_thetaandripunmodcorr_corrandcoactz_CA1PFC_gather']; area = 'CA1PFC'; kind = 'unmod'; state = '';
%gatherdatafile = [savedir 'HP_thetaandripmodcorr_corrandcoactz_CA1allPFC_gather']; area = 'CA1allPFC'; kind = 'mod'; state = '';
%gatherdatafile = [savedir 'HP_thetaandripunmodcorr_corrandcoactz_CA1allPFC_gather']; area = 'CA1allPFC'; kind = 'unmod'; state = '';


if gatherdata
    
    % Parameters if any
    % -----------------
    
    % -------------------------------------------------------------
    
    cnt=0;
    allanimindex=[];
    allZcrosscov_runtheta=[]; allcrosscov_runtheta_totalcorr=[]; allrawcorr_runtheta=[];
    allZcrosscov_sm_runtheta=[]; allcrosscov_sm_runtheta_totalcorr=[]; allrawcorr_sm_runtheta=[];
    allNeventscorr_runtheta=[];allxcorr_runtheta=[]; allT_runtheta=[]; allp1p2_runtheta=[];
    
    all_nsimul=[]; all_nsimul_rdm=[];
    all_trialResps1 = []; all_trialResps2 = []; all_trialResps1_rdm = []; all_trialResps2_rdm = [];
    runcorrtime=[];
    corrwin = 0.2; %Window for theta corrln
    
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            cnt=cnt+1;
            anim_index{an}(cnt,:) = modf(an).output{1}(i).index;
            % Only indexes
            animindex=[an modf(an).output{1}(i).index]; % Put animal index in front
            allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet1 Cell1 Tet2 Cell2 Index
            % Data - Ripmoddata
            all_trialResps1{cnt} = modf(an).output{1}(i).trialResps1;
            all_trialResps2{cnt} = modf(an).output{1}(i).trialResps2;
            all_trialResps1rdm{cnt} = modf(an).output{1}(i).trialResps1_rdm;
            all_trialResps2rdm{cnt} = modf(an).output{1}(i).trialResps2_rdm;
            all_nsimul(cnt) = modf(an).output{1}(i).nsimul;
            all_nsimulrdm(cnt) = modf(an).output{1}(i).nsimul_rdm;
            
            % Data - Theta
            allZcrosscov_runtheta(cnt,:) = thetaf(an).output{1}(i).Zcrosscov;
            allcrosscov_runtheta(cnt,:) = thetaf(an).output{1}(i).crosscov;
            allrawcorr_runtheta(cnt,:) = thetaf(an).output{1}(i).rawcorr;
            allZcrosscov_sm_runtheta(cnt,:) = thetaf(an).output{1}(i).Zcrosscov_sm;
            allcrosscov_sm_runtheta(cnt,:) = thetaf(an).output{1}(i).crosscov_sm;
            allrawcorr_sm_runtheta(cnt,:) = thetaf(an).output{1}(i).rawcorr_sm;
            allNeventscorr_runtheta(cnt) = thetaf(an).output{1}(i).Neventscorr;
            allxcorr_runtheta{cnt} = thetaf(an).output{1}(i).corr;
            allT_runtheta(cnt) = thetaf(an).output{1}(i).T;
            allp1p2_runtheta(cnt) = thetaf(an).output{1}(i).p1p2;
            
            
            % Update on 2 May 2014. The Zsm through the function was with a
            % Gaussian of sd 3 and length 3. g1 = gaussian(nstd, nstd); with nstd=3 
            % Change it to make the smoothing more aggressive
            nstd=3; g1 = gaussian(nstd, 2*nstd+1); %sigma=3, npoints=7 (+/- 3 bins) 
            currZcc = thetaf(an).output{1}(i).Zcrosscov;
            currZcc_sm = smoothvect(currZcc, g1);
            allZcrosscov_sm_runtheta(cnt,:) = currZcc_sm; % UPDATED SMOOTH ZCROSSCOV
            
            
            %Time base for theta correlations - only once
            if isempty(runcorrtime)
                if isfield(thetaf(an).output{1}(i).corr,'time');
                    runcorrtime =  thetaf(an).output{1}(i).corr.time;
                end
                bins_run = find(abs(runcorrtime)<=corrwin); % +/- Corrln window
            end
            
            % SHIFT ALL CALCULATIONS TO AFTER DATA COMBINED ACROSS EPOCHS
            % -------------------------------------------------------------          
%             % Calculate a number for theta corr - Total probor pek  in -/+corrwin
%             currthetacorr = allZcrosscov_runtheta(cnt,:);
%             currthetacorr_sm = allZcrosscov_sm_runtheta(cnt,:);
%             % Sum of values in window
%             alltheta_totalcorr(cnt) = nansum(currthetacorr_sm(bins_run))./length(bins_run); % per bin
%             % Peak value in window +/- corrwin
%             alltheta_peakcorr(cnt) = nanmax(currthetacorr_sm(bins_run)); % Already smoothened, or can take +/-3 bins around peak
%             if (~isnan(alltheta_peakcorr(cnt)) && ~isempty(alltheta_peakcorr(cnt)))
%                 alltheta_peaklag_idx(cnt) = min(find(currthetacorr_sm(bins_run) == nanmax(currthetacorr_sm(bins_run)))); % in ms
%                 alltheta_peaklag(cnt) = runcorrtime(bins_run(alltheta_peaklag_idx(cnt)))*1000; %in ms
%             else
%                 alltheta_peaklag_idx(cnt)=0;
%                 alltheta_peaklag(cnt)=0;
%             end
%             % Trough value in window +/- corrwin
%             
%             alltheta_troughcorr(cnt) = nanmin(currthetacorr_sm(bins_run)); % Already smoothened, or can take +/-3 bins around peak
%             if (~isnan(alltheta_troughcorr(cnt)) && ~isempty(alltheta_troughcorr(cnt)))
%                 alltheta_troughlag_idx(cnt) = min(find(currthetacorr_sm(bins_run) == nanmin(currthetacorr_sm(bins_run)))); % in ms
%                 alltheta_troughlag(cnt) = runcorrtime(bins_run(alltheta_troughlag_idx(cnt)))*1000; %in ms
%             else
%                 alltheta_troughlag_idx(cnt)=0;
%                 alltheta_troughlag(cnt)=0;
%             end
%             
%             %alltheta_totalcorr(cnt) = nanmax(currthetacorr(bins_run));
%             %alltheta_peaklag(cnt) = find (currthetacorr(bins_run) == nanmax(currthetacorr(bins_run)));
            
        end
        
    end
    
    
    % CONSOLIDATE ACROSS EPOCHS FOR SAME CELL PAIRS
    % ----------------------------------------------------------
    runpairoutput = struct;
    dummyindex=allanimindex;  % all anim-day-epoch-tet1-cell1-tet2-cell2 indices
    cntpairs=0;
    
    for ii=1:size(allanimindex)
        animdaytetcell=allanimindex(ii,[1 2 4 5 6 7]);
        %animdaytetcell= [1 1 1 4 18 3];
        %         if sum(animdaytetcell(3:end)-[1 2 15 4])==0
        %
        %             keyboard
        %         end
        ind=[];
        while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5 6 7]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5 6 7]))];        % finds the first matching row
            dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5 6 7])),:)=[0 0 0 0 0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one if it exists
        end
        
        % Gather everything for the current cell across epochs
        % Theta corr variables
        allZcrosscov_runtheta_epcomb = []; allZcrosscov_sm_runtheta_epcomb = [];
        allNeventscorr_runtheta_epcomb = []; alltheta_peakcorr_epcomb = []; alltheta_peaklag_epcomb = [];
        % Ripcorr variables
        all_nsimul_epcomb=0; all_nsimulrdm_epcomb=0;
        all_trialResps1_epcomb=[]; all_trialResps2_epcomb=[]; all_trialResps1rdm_epcomb=[]; all_trialResps2rdm_epcomb=[];
        r=[]; p=[]; r_rdm=[]; p_rdm=[];
        
        for ri=ind
            % Theta Corr variables
            allZcrosscov_sm_runtheta_epcomb = [allZcrosscov_sm_runtheta_epcomb; allZcrosscov_sm_runtheta(ri,:)];
            allZcrosscov_runtheta_epcomb = [allZcrosscov_runtheta_epcomb; allZcrosscov_runtheta(ri,:)];
            %alltheta_peakcorr_epcomb = [alltheta_peakcorr_epcomb; alltheta_peakcorr(ri)];
            %alltheta_peaklag_epcomb = [alltheta_peaklag_epcomb; alltheta_peaklag(ri)];
            allNeventscorr_runtheta_epcomb = [allNeventscorr_runtheta_epcomb; allNeventscorr_runtheta(ri)];
            % Rip Corr variables
            all_trialResps1_epcomb = [all_trialResps1_epcomb; all_trialResps1{ri}];
            all_trialResps2_epcomb = [all_trialResps2_epcomb; all_trialResps2{ri}];
            all_trialResps1rdm_epcomb = [all_trialResps1rdm_epcomb; all_trialResps1rdm{ri}];
            all_trialResps2rdm_epcomb = [all_trialResps2rdm_epcomb; all_trialResps2rdm{ri}];
            all_nsimul_epcomb = all_nsimul_epcomb + all_nsimul(ri);
            all_nsimulrdm_epcomb = all_nsimulrdm_epcomb + all_nsimulrdm(ri);
        end
        
        % Calculate corrln for combined epoch data
        % -----------------------------
        [r, p] = corrcoef(all_trialResps1_epcomb,all_trialResps2_epcomb);
        %requiring at least swrthresh swrs and spikethresh spikes for each
        %cell
        swrthresh=30;
        spikethresh=30;
        enoughData=length(all_trialResps1_epcomb)>swrthresh&min(sum(all_trialResps1_epcomb),sum(all_trialResps2_epcomb))>spikethresh;
        % Attempting a new alternative measure for link between single CA1 and PFC cells.
        % Instead of correlation between spike counts of CA1 and PFC cells across
        % SWRs (and since in most cases numspikes of CA1 cell=0), I now look at whether
        % the PFC cell fires significantly more/less when the CA1 cells
        % fires 1 or more spikes compared to when it is silent. As measure
        % I look at modulation depth:
        % [(numspikes when CA1>0)-(numspikes when CA1=0)]/[(numspikes when CA1>0)+(numspikes when CA1=0)]
        
        if length(all_trialResps2_epcomb)>0
            try
                p10=ranksum(all_trialResps2_epcomb(all_trialResps1_epcomb>0),all_trialResps2_epcomb(all_trialResps1_epcomb==0));
                r10=(mean(all_trialResps2_epcomb(all_trialResps1_epcomb>0))-mean(all_trialResps2_epcomb(all_trialResps1_epcomb==0)))/(mean(all_trialResps2_epcomb(all_trialResps1_epcomb>0))+mean(all_trialResps2_epcomb(all_trialResps1_epcomb==0)));
                p10_rdm=ranksum(all_trialResps2rdm_epcomb(all_trialResps1_epcomb>0),all_trialResps2rdm_epcomb(all_trialResps1_epcomb==0));
                r10_rdm=(mean(all_trialResps2rdm_epcomb(all_trialResps1_epcomb>0))-mean(all_trialResps2rdm_epcomb(all_trialResps1_epcomb==0)))/(mean(all_trialResps2_epcomb(all_trialResps1_epcomb>0))+mean(all_trialResps2_epcomb(all_trialResps1_epcomb==0)));
                numswrs10=sum(all_trialResps1_epcomb>0);
                numswrswpfcspikes10=min(sum(all_trialResps2_epcomb(all_trialResps1_epcomb>1)), sum(all_trialResps2_epcomb(all_trialResps1_epcomb==0)));
            catch
                p10=nan;
                r10=nan;
                p10_rdm=nan;
                r10_rdm=nan;
                numswrs10=nan;
                numswrswpfcspikes=nan;
            end
           
        else
            p10=nan;
            r10=nan;
            p10_rdm=nan;
            r10_rdm=nan;
            numswrs10=nan;
            numswrswpfcspikes=nan;
        end
        % if r10<-0.9
%     keyboard;
% end
        % random is actually the back window
        [r_rdm, p_rdm] = corrcoef(all_trialResps1rdm_epcomb,all_trialResps2rdm_epcomb);
        r = r(1,2); p = p(1,2);
        r_rdm = r_rdm(1,2); p_rdm = p_rdm(1,2);
        
       
        
        % Shuffling and stability
        % -----------------------------
        % this is the true shuffling:
        shufT1=all_trialResps1_epcomb(randperm(length(all_trialResps1_epcomb)));
        shufT2=all_trialResps2_epcomb(randperm(length(all_trialResps2_epcomb)));
        [r_shuf, p_shuf] = corrcoef(shufT1,shufT2);
        r_shuf = r_shuf(1,2); p_shuf = p_shuf(1,2);
        % stability measure:
        %permDataInd=randperm(length(all_trialResps1_epcomb));
        permDataInd=1:length(all_trialResps1_epcomb);
        
        midD=round(length(all_trialResps1_epcomb)/2);
        [rhalf1, phalf1] = corrcoef(all_trialResps1_epcomb(permDataInd(1:midD)),all_trialResps2_epcomb(permDataInd(1:midD)));
        [rhalf2, phalf2] = corrcoef(all_trialResps1_epcomb(permDataInd(midD+1:end)),all_trialResps2_epcomb(permDataInd(midD+1:end)));
        stabilityM=abs(rhalf1(1,2)-rhalf2(1,2));
        % stability measure for shuf:
        [rhalf1shuf, phalf1shuf] = corrcoef(shufT1(1:midD),shufT2(1:midD));
        [rhalf2shuf, phalf2shuf] = corrcoef(shufT1(midD+1:end),shufT2(midD+1:end));
        stabilityMshuf=abs(rhalf1shuf(1,2)-rhalf2shuf(1,2));
        
        
        anim1=animdaytetcell(1);
        day1=animdaytetcell(2);
        tetHC=animdaytetcell(3);
        cellHC=animdaytetcell(4);
        tetPFC=animdaytetcell(5);
        cellPFC=animdaytetcell(6);
        epochs1=allanimindex(ind,3);
        
        
%         %     if anim1==4
%         if p<0.05 &abs(r)>0.2
%             %  keyboard
%             switch anim1(1)
%                 case 1
%                     load(['/data15/gideon/ripplemod/HParipplemod0' num2str(day1) '.mat'])
%                 case 2
%                     load(['/data15/gideon/ripplemod/HPbripplemod0' num2str(day1) '.mat'])
%                 case 3
%                     load(['/data15/gideon/ripplemod/HPcripplemod0' num2str(day1) '.mat'])
%                 case 4
%                     if day1<10
%                         load(['/data15/gideon/ripplemod/Ndlripplemod0' num2str(day1) '.mat'])
%                     else
%                         load(['/data15/gideon/ripplemod/Ndlripplemod' num2str(day1) '.mat'])
%                     end
%             end
%             
%             plotRipRasters=0;
%             if plotRipRasters
%                 allhistHC=[];for cc=1:length(epochs1),allhistHC=[allhistHC;ripplemod{1,day1}{1,epochs1(cc)}{1,tetHC}{1,cellHC}.hist];end
%                 allhistPFC=[];for cc=1:length(epochs1),allhistPFC=[allhistPFC;ripplemod{1,day1}{1,epochs1(cc)}{1,tetPFC}{1,cellPFC}.hist];end
%                 scrsz = get(0,'ScreenSize');
%                 figure('Position',[scrsz(3)/5 scrsz(4)/2 scrsz(3)/2 scrsz(4)/1.2])
%                 subplot(1,3,1)
%                 imagesc(allhistHC)
%                 title(['a' num2str(anim1) ' d' num2str(day1) ' t' num2str(tetHC) ' c' num2str(cellHC)])
%                 subplot(1,3,2)
%                 imagesc(allhistPFC)
%                 title(['t' num2str(tetPFC) ' c' num2str(cellPFC)])
%                 subplot(1,3,3)
%                 plot(all_trialResps1_epcomb,length(all_trialResps1_epcomb):-1:1);hold on
%                 ylim([1,length(all_trialResps1_epcomb)])
%                 hold on
%                 plot(all_trialResps2_epcomb,length(all_trialResps2_epcomb):-1:1,'r');hold on
%                 if p<0.05
%                     title(['r=' num2str(round(r*100)/100) '*'])
%                 else
%                     title(['r=' num2str(round(r*100)/100)])
%                     
%                 end
%                 keyboard
%             end
%             %   end
%         end
        
        
        if ~isempty(allZcrosscov_runtheta_epcomb)
            cntpairs=cntpairs+1;
            runpairoutput_idx(cntpairs,:)=animdaytetcell;
            runpairoutput(cntpairs).index=animdaytetcell; % This is anim-day-tet1-cell1-tet2-cell2. No epoch
            
            currthetacorr_sm=[]; currpeakcorr=[]; currpeaklag_idx=[]; currpeaklag =[];            
            
            % Theta corr variables
            % --------------------
            runpairoutput(cntpairs).allZcrosscov_sm_runtheta_epcomb = nanmean(allZcrosscov_sm_runtheta_epcomb,1);
            runpairoutput(cntpairs).allZcrosscov_runtheta_epcomb = nanmean(allZcrosscov_runtheta_epcomb,1);
            runpairoutput(cntpairs).allNeventscorr_runtheta_epcomb = nansum([0;allNeventscorr_runtheta_epcomb]);
            % Get the peakcorr and peak lag from the combined normcorr_sm  
            currthetacorr_sm = nanmean(allZcrosscov_sm_runtheta_epcomb,1);
            currpeakcorr = nanmax(currthetacorr_sm(bins_run)); % Already smoothened, or can take +/-3 bins around peak
            if (~isnan(currpeakcorr) && ~isempty(currpeakcorr))
                currpeaklag_idx = min(find(currthetacorr_sm(bins_run) == nanmax(currthetacorr_sm(bins_run))));
                currpeaklag = runcorrtime(bins_run(currpeaklag_idx)); %in sec
            else
                currpeaklag_idx=NaN;
                currpeaklag=NaN;
            end
            runpairoutput(cntpairs).alltheta_peakcorr_epcomb = currpeakcorr;
            runpairoutput(cntpairs).alltheta_peaklag_epcomb = currpeaklag;  
            %runpairoutput(cntpairs).alltheta_peakcorr_epcomb = nanmean(alltheta_peakcorr_epcomb,1);
            %runpairoutput(cntpairs).alltheta_peaklag_epcomb = nanmean(alltheta_peaklag_epcomb,1);
            
            % Rip Corr variables
            % --------------------
            runpairoutput(cntpairs).all_nsimul_epcomb = all_nsimul_epcomb;
            runpairoutput(cntpairs).all_nsimulrdm_epcomb = all_nsimulrdm_epcomb;
            runpairoutput(cntpairs).all_trialResps1_epcomb = all_trialResps1_epcomb;
            runpairoutput(cntpairs).all_trialResps2_epcomb = all_trialResps2_epcomb;
            runpairoutput(cntpairs).all_trialResps1rdm_epcomb = all_trialResps1rdm_epcomb;
            runpairoutput(cntpairs).all_trialResps2rdm_epcomb = all_trialResps2rdm_epcomb;
            runpairoutput(cntpairs).allr_epcomb = r;
            runpairoutput(cntpairs).allp_epcomb = p;
            runpairoutput(cntpairs).enoughData = enoughData;
            
            runpairoutput(cntpairs).allr10_epcomb = r10;
            runpairoutput(cntpairs).allp10_epcomb = p10;
            runpairoutput(cntpairs).allnumswrs10 = numswrs10;
            runpairoutput(cntpairs).numswrswpfcspikes10 = numswrswpfcspikes10;
            runpairoutput(cntpairs).allr_rdm_epcomb = r_rdm;
            runpairoutput(cntpairs).allp_rdm_epcomb = p_rdm;
            runpairoutput(cntpairs).allr10_rdm_epcomb = r10_rdm;
            runpairoutput(cntpairs).allp10_rdm_epcomb = p10_rdm;
            % Save outside of structure format
            % Theta corr variables
            % --------------------
            SallZcrosscov_sm_runtheta_epcomb(cntpairs,:) = nanmean(allZcrosscov_sm_runtheta_epcomb,1);
            SallZcrosscov_runtheta_epcomb(cntpairs,:) = nanmean(allZcrosscov_runtheta_epcomb,1);
            Salltheta_peakcorr_epcomb(cntpairs) = currpeakcorr;
            Salltheta_peaklag_epcomb(cntpairs) = currpeaklag;
            SallNeventscorr_runtheta_epcomb(cntpairs) = nansum([0;allNeventscorr_runtheta_epcomb]);
            % Rip Corr variables
            % --------------------
            Sallr_epcomb(cntpairs) = r;
            Sallr_rdm_epcomb(cntpairs) = r_rdm;
            Sallr_shuf_epcomb(cntpairs) = r_shuf;          
            Sallp_epcomb(cntpairs) = p;
            Sallp_rdm_epcomb(cntpairs) = p_rdm;
            Sallp_shuf_epcomb(cntpairs) = p_shuf;        
            Sall_nsimul_epcomb(cntpairs) = all_nsimul_epcomb;
            Sall_nsimulrdm_epcomb(cntpairs) = all_nsimul_epcomb;
            Sstability(cntpairs) = stabilityM;
            SstabilityShuf(cntpairs)=stabilityMshuf;
            SenoughData(cntpairs)=enoughData;

            Sallr10_epcomb(cntpairs) = r10;
            Sallp10_epcomb(cntpairs) = p10;
            Sallnumswrs10_epcomb(cntpairs) = numswrs10;
            Sallnumswrswpfcspikes10_epcomb(cntpairs) = numswrswpfcspikes10;

            Sallr10_rdm_epcomb(cntpairs) = r10_rdm;
            Sallp10_rdm_epcomb(cntpairs) = p10_rdm;
            
        end
    end
    
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data


figdir = '/data25/sjadhav/HPExpt/Figures/Theta_SWR_Correlation/2015/July/';

set(0,'defaultaxesfontsize',20);
tfont = 20;
xfont = 20;
yfont = 20;
% Plotting for indiv pairs
% --------------------------
if 0
    %    for i=1:cnt
    %         if allp_shuf(i)<0.05
    for i=1:cntpairs
        
        curridx = runpairoutput_idx(i,:);
        if curridx(1)==1 && curridx(2)==2 && curridx(3)==1 && curridx(4)==1 && curridx(5)==17 && curridx(6)==2
            %if Sallp_epcomb(i)<0.05
            idx = runpairoutput_idx(i,:)
            
            switch idx(1)
                case 1
                    pre ='HPa';
                case 2
                    pre = 'HPb';
                case 3
                    pre = 'HPc';
                case 4
                    pre = 'Ndl';
                case 5
                    pre = 'Rtl';
                    
            end
            
            figure; hold on;
            plot(runcorrtime, SallZcrosscov_runtheta_epcomb(i,:),'k--','LineWidth',2);
            plot(runcorrtime, SallZcrosscov_sm_runtheta_epcomb(i,:),'r','LineWidth',4);
            line([0 0], [min(SallZcrosscov_sm_runtheta_epcomb(i,:)) max(SallZcrosscov_sm_runtheta_epcomb(i,:))],'Color',[0.5 0.5 0.5],'LineWidth',2);
            %             line([100 100], [min(SallZcrosscov_sm_runtheta_epcomb(i,:)) max(SallZcrosscov_sm_runtheta_epcomb(i,:))],'Color',[0.5 0.5 0.5],'LineWidth',2);
            %             line([-100 -100], [min(SallZcrosscov_sm_runtheta_epcomb(i,:)) max(SallZcrosscov_sm_runtheta_epcomb(i,:))],'Color',[0.5 0.5 0.5],'LineWidth',2);
            %
            plot(Salltheta_peaklag_epcomb(i),Salltheta_peakcorr_epcomb(i),'ro','MarkerSize',12,'LineWidth',2);
            
            title(sprintf('Day%d Tet%d Cell%d, Tet%d Cell%d', idx(2), idx(3), idx(4), idx(5), idx(6)),'FontSize',20)
            if Sallp_epcomb(i) <0.05, str = '*'; else, str = ''; end
            text(0.2, 1*max(SallZcrosscov_sm_runtheta_epcomb(i,:)),sprintf('ripcc %0.2f%s',Sallr_epcomb(i),str),'FontSize',20);
            set(gca,'XLim',[-0.4 0.4]);
            
            xlabel('Time (sec)','FontSize',20);
            ylabel('Std. CrossCov - Run','FontSize',20);
            
            keyboard;
        end
    end
end



corrRateRipples = nanmean(Sallp_epcomb < 0.05)
corrRateRdm = nanmean(Sallp_rdm_epcomb < 0.05)
% are there more correlated pairs during ripples than before?
[r_realrdm, p_realrdm] = ttest(Sallp_epcomb<0.05,Sallp_rdm_epcomb<0.05)
figure;bar([nanmean(Sallp_epcomb<0.05),nanmean(Sallp_rdm_epcomb<0.05)]);ylim([0 0.15])
if  p_realrdm<0.05
    text(1.4,0.12,'*','fontsize',25);
else
    text(1.4,0.12,'NS','fontsize',25);
end
set(gca,'XTickLabel',{'Ripples','Pre-ripples'})
ylabel('% sig rip-cor pairs')
% are there more correlated pairs during ripples than random?
%[rqq pqq]=ttest(Sallp_epcomb<0.05,Sallp_shuf_epcomb<0.05)

% test for proportions, from http://courses.washington.edu/dphs568/course/day12.htm
% apply elsewhere too
a1=Sallp_epcomb<0.05;
a2=Sallp_shuf_epcomb<0.05;
mean(a1)
mean(a2)
p1minp2=mean(a1)-mean(a2);
phat=(sum(a1)+sum(a2))/(length(a1)+length(a2));
sep1p2=sqrt(phat*(1-phat)*(1/length(a1)+1/length(a2)));
z1=p1minp2/sep1p2;
pqq=2*(1-normcdf(z1))

figure;bar([nanmean(Sallp_epcomb<0.05),nanmean(Sallp_shuf_epcomb<0.05)]);ylim([0 0.15])
if  pqq<0.05
    text(1.4,0.12,'*','fontsize',45);
else
    text(1.4,0.12,'NS','fontsize',25);
end
set(gca,'XTickLabel',{'Ripples','Trial-shuff'})
ylabel('% sig rip-cor pairs')
figfile = [figdir,'Number_SigSWRCorrPairs',area]
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
end
    

% correlation between correlation during ripples and correlations before
good2=(~isnan(Sallr_epcomb)&~isnan(Sallr_rdm_epcomb));
[rr1 pp1]=corrcoef(Sallr_epcomb(good2),Sallr_rdm_epcomb(good2))
qq=polyfit(Sallr_epcomb(good2)', Sallr_rdm_epcomb(good2)',1);
xpts = -0.4:0.01:0.6;
f= polyval(qq,xpts);
figure;plot(Sallr_epcomb,Sallr_rdm_epcomb,'x')
hold on
plot(xpts,f,'-','linewidth',3)
title(['r= ' num2str(rr1(2,1)) ' p= ' num2str(pp1(2,1))])
xlabel('Corr during ripples')
ylabel('Corr pre-ripples')
% correlation between correlation during ripples and random
good1=(~isnan(Sallr_epcomb)&~isnan(Sallr_shuf_epcomb));
[rr2 pp2]=corrcoef(Sallr_epcomb(good1),Sallr_shuf_epcomb(good1))
qq=polyfit(Sallr_epcomb(good1)', Sallr_shuf_epcomb(good1)',1);
xpts = -0.4:0.01:0.6;
f= polyval(qq,xpts);
figure;plot(Sallr_epcomb,Sallr_shuf_epcomb,'x')
% hold on
% plot(xpts,f,'-','linewidth',3)
title(['r= ' num2str(rr2(2,1)) ' p= ' num2str(pp2(2,1))])
xlabel('Corr during ripples')
ylabel('Corr, shuffled')


% stability
% s1=(SstabilityShuf(Sallp_shuf_epcomb<0.05));
% s2=(Sstability(Sallp_epcomb<0.05));
% [rx px]=ttest2(s1(~isnan(s1)),s2(~isnan(s2)))
%
%
% figure;bar([nanmean(s2(~isnan(s2))),nanmean(s1(~isnan(s1)))]);ylim([0 0.3])
% if  px<0.05
% text(1.4,0.22,'*','fontsize',45);
% else
% text(1.4,0.22,'NS','fontsize',25);
% end
%     set(gca,'XTickLabel',{'Ripples','Trial-shuff'})
% ylabel('% change in corr')
% title('Stability of correlations')
%
%
% % ------------------
% Population Figures
% ------------------

forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

%figdir = '/data25/sjadhav/HPExpt/Figures/RevFig1/';
figdir = '/data25/sjadhav/HPExpt/Figures/Theta_SWR_Correlation/2015/July/';


summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 24;
    xfont = 24;
    yfont = 24;
end

if strcmp(state,'sleep'),
    statename = 'Sleep';
else
    statename = 'Run';
end

%clr = 'c'


newmeasure = 0;
% Change variable names for epoch-combined data so that you can use the same code to plot as in Version 1
% ----------------------------------------------------------------------------------------------------------
% Theta Corr variables
allZcrosscov_sm_runtheta = SallZcrosscov_sm_runtheta_epcomb;
allZcrosscov_runtheta = SallZcrosscov_runtheta_epcomb;
alltheta_peakcorr = Salltheta_peakcorr_epcomb;
alltheta_peaklag = Salltheta_peaklag_epcomb;
% Rip Corr variables
allr = Sallr_epcomb;
allr_rdm = Sallr_rdm_epcomb;
allp = Sallp_epcomb;
allp_rdm = Sallp_rdm_epcomb;
if newmeasure
    allr = Sallr10_epcomb;
    allr_rdm = Sallr10_rdm_epcomb;
    allp = Sallp10_epcomb;
    allp_rdm = Sallp10_rdm_epcomb;
end
all_nsimul = Sall_nsimul_epcomb;
all_nsimulrdm = Sall_nsimulrdm_epcomb;



switch val
    case 8
        area = 'PFCripmod'; clr = 'k';
    case 9
        area = 'PFCExc'; clr = 'r';
    case 10
        area = 'PFCInh'; clr = 'b';
end
        
        
% Plot Mean Standardized Cross-Cov for Entire Popln, and use same conditions as below for scatter plot
% -------------------------------------------------------------------------------------------------------

if 1
    rnan = find(isnan(allr) | isnan(alltheta_peakcorr));
    %tnan = find(isnan(alltheta_peakcorr));
    nocc = find(all_nsimul<10);
    % DECIDE HERE ON EXACT THRESHOLDS
             %noswrs=find(Sallnumswrs10_epcomb<30|Sallnumswrswpfcspikes10_epcomb<30);
    removeidxs = union(rnan,nocc);    
    tallZcrosscov_sm_runtheta=allZcrosscov_sm_runtheta; tallZcrosscov_runtheta=allZcrosscov_runtheta; %tallZcrosscov_sm_runthetarip=allZcrosscov_sm_runthetarip;
    tallZcrosscov_runtheta(removeidxs,:)=[]; tallZcrosscov_sm_runtheta(removeidxs,:)=[]; %tallZcrosscov_sm_runthetarip(removeidxs,:)=[];
    tallr = allr; tallp=allp;
    tallr(removeidxs)=[];tallp(removeidxs)=[];    
    if size(tallZcrosscov_sm_runtheta,1) ~= length(tallr)
        keyboard;
    end
    % Try different smoothing 
    % ------------------------
    tallZcrosscov_runthetaN=[]; 
    % Re-compute peak-lag as well, and separate by direction of correlation
    % ---------------------------
         
    peaklagN=[]; peaklagNpos=[]; peaklagNneg=[];
    nstd=2;
    g1 = gaussian(nstd, nstd); 
    %g1 = gaussian(nstd, 2*nstd+1);
    for i=1:size(tallZcrosscov_sm_runtheta,1)
        tallZcrosscov_runthetaN(i,:) = smoothvect(tallZcrosscov_runtheta(i,:), g1); 
        currcorrtmp = tallZcrosscov_runthetaN(i,:);
         
         currpeaklag_idx = min(find(currcorrtmp(bins_run) == nanmax(currcorrtmp(bins_run))));
         currpeaklag = runcorrtime(bins_run(currpeaklag_idx));
         peaklagN(i) = currpeaklag;
         if tallr(i)>=0
             peaklagNpos = [peaklagNpos; currpeaklag];
         else
             peaklagNneg = [peaklagNneg; currpeaklag];
         end
    end
    length(peaklagN), meanlag=1000*nanmean(peaklagN), 1000*nansem(peaklagN), 
    length(peaklagNpos), meanlag=1000*nanmean(peaklagNpos), 1000*nansem(peaklagNpos), 
    length(peaklagNneg), meanlag=1000*nanmean(peaklagNneg), 1000*nansem(peaklagNneg), 
  
    runthetaZsm = nanmean(tallZcrosscov_sm_runtheta,1);
    runthetaZsmerr = nansem(tallZcrosscov_sm_runtheta,1);
    runthetaZ = nanmean(tallZcrosscov_runtheta,1);
    %runthetaripZ = nanmean(tallZcrosscov_sm_runthetarip,1);
    figure; hold on;
    %plot(runcorrtime, runthetaZ,'r--','LineWidth',3);
    %plot(runcorrtime, runthetaZsm,'k','LineWidth',3);
    %plot(runcorrtime, runthetaZsm-runthetaZsmerr,'k--','LineWidth',2);
    %plot(runcorrtime, runthetaZsm+runthetaZsmerr,'k--','LineWidth',2);
    line([0 0], [min(runthetaZsm) max(runthetaZsm+runthetaZsmerr)],'Color',[0.5 0.5 0.5],'LineWidth',2);
    %line([0.1 0.1], [min(runthetaZ) max(runthetaZ)],'Color',[0.5 0.5 0.5],'LineWidth',1);
    %line([-0.1 -0.1], [min(runthetaZ) max(runthetaZ)],'Color',[0.5 0.5 0.5],'LineWidth',1);
    
    % Try different smoothing    
    %nstd=2;
    %g1 = gaussian(nstd, nstd); 
    %g1 = gaussian(nstd, 2*nstd+1);
    %runthetaZsm = smoothvect(runthetaZ, g1); 
    runthetaZsmN = nanmean(tallZcrosscov_runthetaN,1);runthetaZsmNerr = nansem(tallZcrosscov_runthetaN,1);
    plot(runcorrtime*1000, runthetaZsm,clr,'LineWidth',3);
    plot(runcorrtime*1000, runthetaZsm-runthetaZsmerr,[clr,'--'],'LineWidth',2);
    plot(runcorrtime*1000, runthetaZsm+runthetaZsmerr,[clr,'--'],'LineWidth',2);
    
    title(sprintf('Mean Std. CrossCov - CA1-Rip %s Cells',...
        kind),'FontSize',20)
    set(gca,'XLim',[-300 300]);
    ylim([-0.58 0.2])
    xlabel('Time (sec)','FontSize',20);
    ylabel('Population standardized cross covariances','FontSize',20);
    %legend('Theta','ThetaSm');
    figdir = '/data25/sjadhav/HPExpt/Figures/Theta_SWR_Correlation/ThetaCovTiming/';
    figfile = [figdir,'MeanThetaCrossCov_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    % Zoomed in version
    % -----------------
    figure; hold on;
    line([0 0], [min(runthetaZsm) max(runthetaZsm+runthetaZsmerr)],'Color',[0.5 0.5 0.5],'LineWidth',2);
    plot(runcorrtime*1000, runthetaZsm,clr,'LineWidth',3);
    axis([-80 80 0.06 0.11])
    ylim([-0.1 0.1]); xlim ([-100 100]);
    figfile = [figdir,'MeanThetaCrossCovZoom_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    % Don't shade for making final figure in illustrator
    % -------------
    figure; hold on;
    line([0 0], [min(runthetaZsm) max(runthetaZsm+runthetaZsmerr)],'Color',[0.5 0.5 0.5],'LineWidth',2);
    plot(runcorrtime*1000, runthetaZsm, [clr,'-'],'LineWidth',2);
    jbfill(runcorrtime*1000,runthetaZsm+runthetaZsmerr,runthetaZsm-runthetaZsmerr,clr,clr,1,1);
    
    title(sprintf('Mean Std. CrossCov - CA1-Rip %s Cells',...
        kind),'FontSize',20)
    set(gca,'XLim',[-300 300]);
    ylim([-0.6 0.2])
    xlabel('Time (ms)')
    ylabel('Population standardized cross covariance')
    figfile = [figdir,'MeanThetaCrossCovFill_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    
    % Plot Mean Standardized Cross-Cov for Sig Ripcorr, and use same conditions as below for scatter plot
    % --------------------------------------------------------------------------------------------------------------
    
    removeidxs = union(rnan,nocc);  
    nonsig = find (allp >= 0.05); removeidxs = union(removeidxs,nonsig); 
    tallZcrosscov_runtheta = allZcrosscov_runtheta; tallZcrosscov_sm_runtheta = allZcrosscov_sm_runtheta;
    tallZcrosscov_runtheta(nonsig,:)=[]; tallZcrosscov_sm_runtheta(nonsig,:)=[];
    runthetaZsm = nanmean(tallZcrosscov_sm_runtheta,1);
    runthetaZsmerr = nansem(tallZcrosscov_sm_runtheta,1);
    runthetaZ = nanmean(tallZcrosscov_runtheta,1);
    %runthetaripZ = nanmean(allZcrosscov_sm_runthetarip,1);
    figure; hold on;
    plot(runcorrtime, runthetaZ,'r--','LineWidth',3);
    plot(runcorrtime, runthetaZsm,'k','LineWidth',3);
    plot(runcorrtime, runthetaZsm-runthetaZsmerr,'k--','LineWidth',2);
    plot(runcorrtime, runthetaZsm+runthetaZsmerr,'k--','LineWidth',2);
    line([0 0], [min(runthetaZ) max(runthetaZ)],'Color',[0.5 0.5 0.5],'LineWidth',2);
    line([0.1 0.1], [min(runthetaZ) max(runthetaZ)],'Color',[0.5 0.5 0.5],'LineWidth',1);
    line([-0.1 -0.1], [min(runthetaZ) max(runthetaZ)],'Color',[0.5 0.5 0.5],'LineWidth',1);
    
    % Try different smoothing    
    nstd=2;
    g1 = gaussian(nstd, nstd); 
    %g1 = gaussian(nstd, 2*nstd+1);
    runthetaZsm = smoothvect(runthetaZ, g1); 
    plot(runcorrtime, runthetaZsm,'g','LineWidth',3);
    
    title(sprintf('Mean Std. CrossCov - CA1-Rip %s Cells. Sig RipCorr Only',...
        kind),'FontSize',20)
    set(gca,'XLim',[-0.4 0.4]);
    xlabel('Time (sec)','FontSize',20);
    ylabel('Std. CrossCov','FontSize',20);
    %legend('Theta','ThetaSm');
    figdir = '/data25/sjadhav/HPExpt/Figures/Theta_SWR_Correlation/ThetaCovTiming/';
    figfile = [figdir,'MeanThetaCrossCov_SigOnly_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
end



g1 = gaussian(nstd, nstd);

figdir = '/data25/sjadhav/HPExpt/Figures/Theta_SWR_Correlation/2015/July/';


if 1
    
    % 1a) Rip corrcoeff vs Total thetacorr for SWR Response
    % -----------------------------------------------------
    % Get rid of NaNs / Take Only Significant
    % Save temp
    alltheta_peakcorr_tmp = alltheta_peakcorr; % Take Peak Corrln
    allr_tmp = allr; allp_tmp = allp;
    
    rnan = find(isnan(allr) | isnan(alltheta_peakcorr));
    %tnan = find(isnan(alltheta_peakcorr));
    nocc = find(all_nsimul<10);
    % DECIDE HERE ON EXACT THRESHOLDS
             %noswrs=find(Sallnumswrs10_epcomb<30|Sallnumswrswpfcspikes10_epcomb<30);
    removeidxs = union(rnan,nocc);
    
    if newmeasure
        removeidxs=union(removeidxs,noswrs);
    end
    
    runpairoutput_idx_good=runpairoutput_idx;
    runpairoutput_idx_good(removeidxs,:)=[];

    allr(removeidxs)=[]; alltheta_peakcorr(removeidxs)=[]; allp(removeidxs)=[];
    sigidx = find(allp<0.05); nosigidx = find(allp>=0.05);
    %allr = allr(sigidx); alltheta_peakcorr = alltheta_peakcorr(sigidx);
    
    % Hoe=w many unique CA1 and PFC cells?
    % All pairs
    ca1idxs = runpairoutput_idx_good(:,[1 2 3 4]);
    pfcidxs = runpairoutput_idx_good(:,[1 2 5 6]);
    ca1idxs_uniq = unique(ca1idxs,'rows'); length(ca1idxs_uniq) 
    pfcidxs_uniq = unique(pfcidxs,'rows'); length(pfcidxs_uniq)
    
    % Sig pairs
    ca1sigidxs = runpairoutput_idx_good(sigidx,[1 2 3 4]);
    pfcsigidxs = runpairoutput_idx_good(sigidx,[1 2 5 6]);
    ca1sigidxs_uniq = unique(ca1sigidxs,'rows');  length(ca1sigidxs_uniq)
    pfcsigidxs_uniq = unique(pfcsigidxs,'rows'); length(pfcsigidxs_uniq)
    
    % ------------------
     % For Demetris: For spatial Corrln Code. Save Only for all the PFC CELLS!
    corrindsForSpatial=runpairoutput;
    corrindsForSpatial(removeidxs)=[];
    %save corrindsForSpatial_X8 corrindsForSpatial
    % -----------------
    
    
    %% ALL ANIMALS
    figure; hold on; redimscreen_figforppt1;
    %set(gcf, 'Position',[205 136 723 446]);
    %xaxis = min(allr):0.1:max(allr);
    plot(alltheta_peakcorr, allr,'k.','MarkerSize',24);
    plot(alltheta_peakcorr(sigidx), allr(sigidx),'r.','MarkerSize',24);
    % legend('Theta Cov vs SWR CorrCoeff');
    
    title(sprintf('%s %s units', area, statename),'FontSize',tfont,'Fontweight','normal')
    title(sprintf('CA1-PFC SWR modulated pairs'),'FontSize',tfont,'Fontweight','normal')
    ylabel('SWR Response Correlation','FontSize',yfont,'Fontweight','normal');
    xlabel('Peak Theta Covariance','FontSize',xfont,'Fontweight','normal');
    
    % Do statistics on this popln plot
    [r_thetavsrip,p_thetavsrip] = corrcoef(allr, alltheta_peakcorr);
    [rsig,psig] = corrcoef(allr(sigidx), alltheta_peakcorr(sigidx));
    [rnosig,pnosig] = corrcoef(allr(nosigidx), alltheta_peakcorr(nosigidx));
    
    length(sigidx)
    length(alltheta_peakcorr)
    r_thetavsrip
    p_thetavsrip
    
    % Regression
    % -----------
    [b00,bint00,r00,rint00,stats00] = regress(allr', [ones(size(alltheta_peakcorr')) alltheta_peakcorr']);
    xpts = min(alltheta_peakcorr):0.01:max(alltheta_peakcorr);
    bfit00 = b00(1)+b00(2)*xpts;
    plot(xpts,bfit00,'k-','LineWidth',4);  % Theta vs Rip
    % Do regression after shifting data to make intercept 0
    % ------------------------------------------------------
    allr_0 = allr-mean(allr);
    alltheta_peakcorr_0 = alltheta_peakcorr-mean(alltheta_peakcorr);
    [b0,bint0,r0,rint0,stats0] = regress(allr_0',[ones(size(alltheta_peakcorr_0')) alltheta_peakcorr_0']);
    bfit0 = b0(1)+b0(2)*xpts;
    
    rval = roundn(r_thetavsrip(1,2),-2);
    pval = roundn(p_thetavsrip(1,2),-4);
    rsigval = roundn(rsig(1,2),-2);
    psigval = roundn(psig(1,2),-4);
    rnosigval = roundn(rnosig(1,2),-2);
    pnosigval = roundn(pnosig(1,2),-4);
    rsquare = roundn(stats0(1),-2);
    preg = roundn(stats0(3),-4);
    
    % Shuffling
    % ---------
    for n=1:1000
        idxs = randperm(length(allr));
        shuffle = allr(idxs);
        % Get corrcoeff of shuffle
        [rsh,psh] = corrcoef(alltheta_peakcorr, shuffle);
        r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
        % Get regression of shuffle after making intercept 0 / Or Not
        %shuffle_0 = shuffle - mean(shuffle);
        %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(alltheta_peakcorr_0')) alltheta_peakcorr_0']);
        [bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle', [ones(size(alltheta_peakcorr')) alltheta_peakcorr']);
        rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
        b_shuffle(n,:) = bsh;
    end
    prctile(rsquare_shuffle,99); prctile(r_shuffle,99); %figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
    % Get regression corresponding to 99 percentile
    idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,99));
    idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
    bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
    plot(xpts,bfitsh,'k--','LineWidth',2);  % Theta vs Rip - 99% shuffle line
    
    % Add Info to Figure
    % ------------------
    set(gca,'XLim',[-4.5 11]); set(gca,'YLim',[-0.35 0.55]);
    text(4,-0.07,['Npairs:' num2str(length(allr))],'FontSize',24,'Fontweight','normal');
    text(4,-0.14,sprintf('R: %0.2f, pval: %0.3f, preg: %0.3f',rval,pval,preg),'FontSize',24,'Fontweight','normal');
    text(4,-0.21,sprintf('Nsig: %g, Rsig: %0.2f, pval: %0.3f, ', length(sigidx), rsigval,psigval),'FontSize',24,'Fontweight','normal');
    text(4,-0.3,sprintf('Rnosig: %0.2f, pval: %0.3f',rnosigval,pnosigval),'FontSize',24,'Fontweight','normal');
    
    if newmeasure
        figfile = [figdir,statename,'_ThetaCovVsRipCorr_',area,'_newmeasure']
    else
        figfile = [figdir,statename,'_ThetaCovVsRipCorr_',area]
    end
    if savefig1==1,
         print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    %% ONLY ANIMALS 1-3
    
    figure; hold on; redimscreen_figforppt1;
    wanims=find(runpairoutput_idx_good(:,1)<4);
    yanims=find(runpairoutput_idx_good(:,1)>=4);
    
    %set(gcf, 'Position',[205 136 723 446]);
    %xaxis = min(allr):0.1:max(allr);
    plot(alltheta_peakcorr(wanims), allr(wanims),'k.','MarkerSize',24);
    plot(alltheta_peakcorr(intersect(sigidx,wanims)), allr(intersect(sigidx,wanims)),'r.','MarkerSize',24);
    % legend('Theta Cov vs SWR CorrCoeff');
    
    title(sprintf('%s %s units', area, statename),'FontSize',tfont,'Fontweight','normal')
    title(sprintf('CA1-PFC SWR modulated pairs'),'FontSize',tfont,'Fontweight','normal')
    ylabel('SWR Response Correlation','FontSize',yfont,'Fontweight','normal');
    xlabel('Peak Theta Covariance','FontSize',xfont,'Fontweight','normal');
    
    % Do statistics on this popln plot
    [r_thetavsripw,p_thetavsripw] = corrcoef(allr(wanims), alltheta_peakcorr(wanims));
    [rsigw,psigw] = corrcoef(allr(intersect(sigidx,wanims)), alltheta_peakcorr(intersect(sigidx,wanims)));
    [rnosigw,pnosigw] = corrcoef(allr(intersect(nosigidx,wanims)), alltheta_peakcorr(intersect(nosigidx,wanims)));
    rvalw = roundn(r_thetavsripw(1,2),-2);
    pvalw = roundn(p_thetavsripw(1,2),-4);
     rsigvalw = roundn(rsigw(1,2),-2);
    psigvalw = roundn(psigw(1,2),-4);
    rnosigvalw = roundn(rnosigw(1,2),-2);
    pnosigvalw = roundn(pnosigw(1,2),-4);
    % didn't do the regression here
    
    % Add Info to Figure
    % ------------------
    set(gca,'XLim',[-4.5 10]); set(gca,'YLim',[-0.32 0.45]);
    text(-3.8,0.4,['Npairs:' num2str(length(wanims))],'FontSize',30,'Fontweight','normal');
    text(-3.8,0.35,sprintf('R: %0.2f, pval: %0.3f',rvalw,pvalw),'FontSize',30,'Fontweight','normal');
    text(-3.8,0.3,sprintf('Rsig: %0.2f, pval: %0.3f, Nsig: %g',rsigvalw,psigvalw,length(intersect(sigidx,wanims))),'FontSize',30,'Fontweight','normal');
    text(-3.8,0.25,sprintf('Rnosig: %0.2f, pval: %0.3f',rnosigvalw,pnosigvalw),'FontSize',30,'Fontweight','normal');
    
    
    
    %% ONLY ANIMALS 4-5
     figure; hold on; redimscreen_figforppt1;
       
    %set(gcf, 'Position',[205 136 723 446]);
    %xaxis = min(allr):0.1:max(allr);
    plot(alltheta_peakcorr(yanims), allr(yanims),'k.','MarkerSize',24);
    plot(alltheta_peakcorr(intersect(sigidx,yanims)), allr(intersect(sigidx,yanims)),'r.','MarkerSize',24);
    % legend('Theta Cov vs SWR CorrCoeff');
    
    title(sprintf('%s %s units', area, statename),'FontSize',tfont,'Fontweight','normal')
    title(sprintf('CA1-PFC SWR modulated pairs'),'FontSize',tfont,'Fontweight','normal')
    ylabel('SWR Response Correlation','FontSize',yfont,'Fontweight','normal');
    xlabel('Peak Theta Covariance','FontSize',xfont,'Fontweight','normal');
    
    % Do statistics on this popln plot
    [r_thetavsripy,p_thetavsripy] = corrcoef(allr(yanims), alltheta_peakcorr(yanims));
    [rsigy,psigy] = corrcoef(allr(intersect(sigidx,yanims)), alltheta_peakcorr(intersect(sigidx,yanims)));
    [rnosigy,pnosigy] = corrcoef(allr(intersect(nosigidx,yanims)), alltheta_peakcorr(intersect(nosigidx,yanims)));
    rvaly = roundn(r_thetavsripy(1,2),-2);
    pvaly = roundn(p_thetavsripy(1,2),-4);
     rsigvaly = roundn(rsigy(1,2),-2);
    psigvaly = roundn(psigy(1,2),-4);
    rnosigvaly = roundn(rnosigy(1,2),-2);
    pnosigvaly = roundn(pnosigy(1,2),-4);
    % didn't do the regression here
    
    % Add Info to Figure
    % ------------------
    set(gca,'XLim',[-4.5 10]); set(gca,'YLim',[-0.32 0.45]);
    text(-3.8,0.4,['Npairs:' num2str(length(yanims))],'FontSize',30,'Fontweight','normal');
    text(-3.8,0.35,sprintf('R: %0.2f, pval: %0.3f',rvaly,pvaly),'FontSize',30,'Fontweight','normal');
    text(-3.8,0.3,sprintf('Rsig: %0.2f, pval: %0.3f, Nsig: %g',rsigvaly,psigvaly,length(intersect(sigidx,yanims))),'FontSize',30,'Fontweight','normal');
    text(-3.8,0.25,sprintf('Rnosig: %0.2f, pval: %0.3f',rnosigvaly,pnosigvaly),'FontSize',30,'Fontweight','normal');
    
    
    
    
    %%
    
    %RESTOR VALUES AFTER PLOT
    alltheta_peakcorr = alltheta_peakcorr_tmp;
    allr=allr_tmp;
    allp = allp_tmp;
    
    
    
    % 1b) Rip corrcoeff vs Total thetacorr for SWR BACKGROUND
    % -----------------------------------------------------
    % Get rid of NaNs / ONLY SIG CORR
    alltheta_peakcorr_tmp = alltheta_peakcorr; % Take Peak Corrln
    allr_rdm_tmp = allr_rdm; allp_rdmtmp = allp_rdm;
    
    rnan = find(isnan(allr_rdm) | isnan(alltheta_peakcorr));
    %tnan = find(isnan(alltheta_peakcorr));
    nocc = find(all_nsimulrdm<5);
    removeidxs = union(rnan,nocc);
    
    allr_rdm(removeidxs)=[]; alltheta_peakcorr(removeidxs)=[]; allp_rdm(removeidxs)=[];
    sigidx = find(allp_rdm<0.05); nosigidx = find(allp_rdm>=0.05);
    %allr_rdm = allr_rdm(sigidx); alltheta_peakcorr = alltheta_peakcorr(sigidx);
    
    figure; hold on; redimscreen_figforppt1;
    %set(gcf, 'Position',[205 136 723 446]);
    %xaxis = min(allr_rdm):0.1:max(allr_rdm);
    plot(alltheta_peakcorr, allr_rdm,['k.'],'MarkerSize',24);
    plot(alltheta_peakcorr(sigidx), allr_rdm(sigidx),['r.'],'MarkerSize',24);
    %legend('Theta Cov vs SWR BCKGND CorrCoeff');
    
    title(sprintf('%s %s units', area, statename),'FontSize',tfont,'Fontweight','normal')
    title(sprintf('CA1-PFC SWR modulated pairs'),'FontSize',tfont,'Fontweight','normal')
    ylabel('SWR BCKGND Correlation','FontSize',yfont,'Fontweight','normal');
    xlabel('Peak Theta Covariance','FontSize',xfont,'Fontweight','normal');
    
    % Do stattistics on this popln plot
    [r_thetavsrip,p_thetavsrip] = corrcoef(allr_rdm, alltheta_peakcorr);
    [rsig,psig] = corrcoef(allr_rdm(sigidx), alltheta_peakcorr(sigidx));
    [rnosig,pnosig] = corrcoef(allr_rdm(nosigidx), alltheta_peakcorr(nosigidx));
    
    % Regression
    % -----------
    [b00,bint00,r00,rint00,stats00] = regress(allr_rdm', [ones(size(alltheta_peakcorr')) alltheta_peakcorr']);
    xpts = min(alltheta_peakcorr):0.01:max(alltheta_peakcorr);
    bfit00 = b00(1)+b00(2)*xpts;
    plot(xpts,bfit00,'k-','LineWidth',4);  % Theta vs Rip
    % Do regression after shifting data to make intercept 0
    % ------------------------------------------------------
    allr_rdm_0 = allr_rdm-mean(allr_rdm);
    alltheta_peakcorr_0 = alltheta_peakcorr-mean(alltheta_peakcorr);
    [b0,bint0,r0,rint0,stats0] = regress(allr_rdm_0',[ones(size(alltheta_peakcorr_0')) alltheta_peakcorr_0']);
    bfit0 = b0(1)+b0(2)*xpts;
    
    rval = roundn(r_thetavsrip(1,2),-2);
    pval = roundn(p_thetavsrip(1,2),-4);
    rsigval = roundn(rsig(1,2),-2);
    psigval = roundn(psig(1,2),-4);
    rnosigval = roundn(rnosig(1,2),-2);
    pnosigval = roundn(pnosig(1,2),-4);
    rsquare = roundn(stats0(1),-2);
    preg = roundn(stats0(3),-4);
    
    % Shuffling
    % ---------
    for n=1:1000
        idxs = randperm(length(allr_rdm));
        shuffle = allr_rdm(idxs);
        % Get corrcoeff of shuffle
        [rsh,psh] = corrcoef(alltheta_peakcorr, shuffle);
        r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
        % Get regression of shuffle after making intercept 0 / Or Not
        %shuffle_0 = shuffle - mean(shuffle);
        %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(alltheta_peakcorr_0')) alltheta_peakcorr_0']);
        [bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle', [ones(size(alltheta_peakcorr')) alltheta_peakcorr']);
        rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
        b_shuffle(n,:) = bsh;
    end
    prctile(rsquare_shuffle,99); prctile(r_shuffle,99); %figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
    % Get regression corresponding to 99 percentile
    idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,99));
    idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
    bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
    plot(xpts,bfitsh,'k--','LineWidth',2);  % Theta vs Rip - 99% shuffle line
    
    % Add Info to Figure
    % ------------------
    set(gca,'XLim',[-4.5 8]); set(gca,'YLim',[-0.2 0.45]);
    text(-3.8,0.4,['Npairs:' num2str(length(allr_rdm))],'FontSize',30,'Fontweight','normal');
    text(-3.8,0.35,sprintf('R: %0.2f, pval: %0.3f, preg: %0.3f',rval,pval,preg),'FontSize',30,'Fontweight','normal');
    text(-3.8,0.3,sprintf('Rsig: %0.2f, pval: %0.3f, Nsig: %g', rsigval,psigval,length(sigidx)),'FontSize',30,'Fontweight','normal');
    text(-3.8,0.25,sprintf('Rnosig: %0.2f, pval: %0.3f',rnosigval,pnosigval),'FontSize',30,'Fontweight','normal');
    
    
    figfile = [figdir,statename,'_ThetaCovVsRipBCKCorr_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
end


alltheta_peakcorr = alltheta_peakcorr_tmp;



if 0
    % 1) Rip Coactivez vs Total thetacorr
    % -----------------------------------------------------
    % Get rid of NaNs
    rnan = find(isnan(allcoactivez));
    tnan = find(isnan(alltheta_peakcorr));
    removeidxs = [rnan,tnan];
    allcoactivez(removeidxs)=[]; alltheta_peakcorr(removeidxs)=[];
    
    figure; hold on; redimscreen_figforppt1;
    %set(gcf, 'Position',[205 136 723 446]);
    %xaxis = min(allcoactivez):0.1:max(allcoactivez);
    plot(alltheta_peakcorr, allcoactivez,[clr '.'],'MarkerSize',20);
    legend('Theta corr vs Rip CoactiveZ');
    
    title(sprintf('%s %s - Rip%s units', area, statename, kind),'FontSize',tfont,'Fontweight','normal')
    ylabel('Rip Resp CoactiveZ','FontSize',xfont,'Fontweight','normal');
    xlabel('Total Theta Corr','FontSize',yfont,'Fontweight','normal');
    
    % Do stattistics on this popln plot
    [r_thetavsrip,p_thetavsrip] = corrcoef(allcoactivez, alltheta_peakcorr);
    
    % Regression
    % -----------
    [b00,bint00,r00,rint00,stats00] = regress(allcoactivez', [ones(size(alltheta_peakcorr')) alltheta_peakcorr']);
    xpts = min(alltheta_peakcorr):0.01:max(alltheta_peakcorr);
    bfit00 = b00(1)+b00(2)*xpts;
    plot(xpts,bfit00,'k-','LineWidth',4);  % Theta vs Rip
    % Do regression after shifting data to make intercept 0
    % ------------------------------------------------------
    allcoactivez_0 = allcoactivez-mean(allcoactivez);
    alltheta_peakcorr_0 = alltheta_peakcorr-mean(alltheta_peakcorr);
    [b0,bint0,r0,rint0,stats0] = regress(allcoactivez_0',[ones(size(alltheta_peakcorr_0')) alltheta_peakcorr_0']);
    bfit0 = b0(1)+b0(2)*xpts;
    
    rval = roundn(r_thetavsrip(1,2),-2);
    pval = roundn(p_thetavsrip(1,2),-4);
    rsquare = roundn(stats0(1),-2);
    preg = roundn(stats0(3),-4);
    
    % Shuffling
    % ---------
    for n=1:1000
        idxs = randperm(length(allcoactivez));
        shuffle = allcoactivez(idxs);
        % Get corrcoeff of shuffle
        [rsh,psh] = corrcoef(alltheta_peakcorr, shuffle);
        r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
        % Get regression of shuffle after making intercept 0 / Or Not
        %shuffle_0 = shuffle - mean(shuffle);
        %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(alltheta_peakcorr_0')) alltheta_peakcorr_0']);
        [bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle', [ones(size(alltheta_peakcorr')) alltheta_peakcorr']);
        rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
        b_shuffle(n,:) = bsh;
    end
    prctile(rsquare_shuffle,99); prctile(r_shuffle,99); %figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
    % Get regression corresponding to 99 percentile
    idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,99));
    idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
    bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
    plot(xpts,bfitsh,'k--','LineWidth',2);  % Theta vs Rip - 99% shuffle line
    
    % Add Info to Figure
    % ------------------
    set(gca,'XLim',[-4.5 8]); set(gca,'YLim',[-4 5]);
    text(4,5,['Npairs:' num2str(length(allcoactivez))],'FontSize',30,'Fontweight','normal');
    text(4,5,sprintf('R: %0.2f, pval: %0.3f, preg: %0.3f',rval,pval,preg),'FontSize',30,'Fontweight','normal');
    
    figfile = [figdir,area,'_',statename,'_ThetaCorrVsRip',kind,'_CoactiveZ']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
end


alltheta_peakcorr = alltheta_peakcorr_tmp;






% ------------------------------------------------------------------
% COMBINING PLOTS ACROSS FILES - DO MANUALLY
% ------------------------------------------------------------------


keyboard;

savedir = '/data25/sjadhav/HPExpt/ProcessedData/';
figdir = '/data25/sjadhav/HPExpt/Figures/RippleMod/Popln/';

% Define area
area = 'CA1allPFC'; state ='sleep'; % state = '';, or state = 'sleep';

if strcmp(state,'sleep'),
    statename = 'Sleep';
else
    statename = 'Run';
end


% Modulated Units
load([savedir 'HP_ripmod',state,'_corrandcoactz_',area,'_gather'])
% Corr Fig
figure(1); hold on; redimscreen_figforppt1;
set(gcf, 'Position',[205 136 723 446]);
%xaxis = min(allr):0.1:max(allr);
xaxis = -1:0.05:1;
h = histc(allr,xaxis); normh = h./max(h);
plot(xaxis,normh,clr,'Linewidth',3);

% % Coactive Z Fig
% figure(2); hold on; redimscreen_figforppt1;
% set(gcf, 'Position',[205 136 723 446]);
% xaxis = min(allcoactivez):0.5:max(allcoactivez);
% h = histc(allcoactivez,xaxis); normh = h./max(h);
% plot(xaxis,normh,clr,'Linewidth',3);

allrm = allr; allpm = allp; allcozm = allcoactivez;


% UnModulated Units
load([savedir 'HP_ripunmod',state,'_corrandcoactz_',area,'_gather'])
% Corr Fig
figure(1); hold on;
xaxis = -1:0.05:1;
h = histc(allr,xaxis); normh = h./max(h);
plot(xaxis,normh,[clr '--'],'Linewidth',3);

% % Coactive Z Fig
% figure(2); hold on; redimscreen_figforppt1;
% set(gcf, 'Position',[205 136 723 446]);
% xaxis = min(allcoactivez):0.5:max(allcoactivez);
% h = histc(allcoactivez,xaxis); normh = h./max(h);
% plot(xaxis,normh,[clr '--'],'Linewidth',3);

legend('Rip Mod','Rip Unmod');
title(sprintf('%s %s - units: Corr Coeff Hist', area, statename),'FontSize',tfont,'Fontweight','normal')
xlabel('Corr Coeff','FontSize',xfont,'Fontweight','normal');
ylabel('Fraction of cells','FontSize',yfont,'Fontweight','normal');

corrRateRipMod = nanmean(allpm < 0.05), corrRateRipUnMod = nanmean(allp < 0.05),
[r_modunmod p_modunmod] = ttest2(allpm<0.05,allp<0.05)

set(gca,'XLim',[-0.2 0.25]);
text(0.07,0.7,sprintf('Corr Rate Mod: %0.2f',corrRateRipMod),'FontSize',30,'Fontweight','normal');
text(0.07,0.6,sprintf('Corr Rate Unmod: %0.2f',corrRateRipUnMod),'FontSize',30,'Fontweight','normal');
text(0.07,0.5,sprintf('Diff Sig: %0.3f',p_modunmod),'FontSize',30,'Fontweight','normal');

figfile = [figdir,area,'_',statename,'_RippleModvsUnmod_CorrCoeffHist']
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end











