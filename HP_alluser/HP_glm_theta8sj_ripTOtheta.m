
% Load output of glm, and add one more thing: predicting theta form ripple model. Plot on original figure


clear;
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
val = 1;
switch val
    case 1
        savefile2 = [savedir 'HP_ripmod_glmfit_theta_gather6allripmod_X8']; area = 'PFCripmod'; clr = 'k'; % PFC
    case 2
        savefile2 = [savedir 'HP_ripmod_glmfit_theta_gather6ripexc_X8']; area = 'PFCexc'; clr = 'r'; % PFCexc
    case 3
        savefile2 = [savedir 'HP_ripmod_glmfit_theta_gather6ripinh_X8']; area = 'PFCinh'; clr = 'b'; % PFCinh
    case 4
        savefile2 = [savedir 'HP_ripmod_glmfit_theta_gather6unripmod_X8']; area = 'PFCunmod'; clr = 'c'; % PFCinh
end
load(savefile2);

rungather=0; % Run part, or go straight to plot

if rungather
    allErrReal_ripTOtheta=[];
    allErrShuf_ripTOtheta=[];
    allPs_ripTOtheta=[];
    numTrain=1000;
    counterT=0;
    for ii=1:size(XYmats2,2)
        ii
        currXY=XYmats2{ii};
        currX=currXY(:,1:end-1);
        currY=currXY(:,end);
        
        currXYTheta=XYmats2Theta{ii};
        currXTheta=currXYTheta(:,1:end-1);
        currYTheta=currXYTheta(:,end);
        
        lastwarn('');
        
        [btrall, ~, statsall] = glmfit(currX,currY,'poisson');
        %     if isempty(lastwarn)
        %         converged1=[converged1 1];
        %     else
        %         converged1=[converged1 0];
        %
        %     end
        % continue only if converged
        if isempty(lastwarn)
            
            % add the reverse: predicting PFC theta firing from training on ripples
            % just in case, redo the rip fit
            %[btrall, ~, statsall] = glmfit(currX,currY,'poisson');
            yfit_ripTOtheta = glmval(btrall, currXTheta,'log');
            errReal_ripTOtheta=nanmean(abs(yfit_ripTOtheta-currYTheta));
            allErrShufTmp=[];
            for pp=1:numTrain
                errShuf=nanmean(abs(yfit_ripTOtheta-currYTheta(randperm(length(currYTheta)))));
                allErrShufTmp=[allErrShufTmp errShuf];
            end
            currP=nanmean(errReal_ripTOtheta>allErrShufTmp);
            allPs_ripTOtheta=[allPs_ripTOtheta currP];
            allErrReal_ripTOtheta=[allErrReal_ripTOtheta errReal_ripTOtheta];
            allErrShuf_ripTOtheta=[allErrShuf_ripTOtheta nanmean(allErrShufTmp)];
            
            counterT=counterT+1;
        end
    end
    
    counterT % number of pfc cells with converged models
    
    % save the new gatherdatafile with this information added in
    % ------------------------------------------------------------
    save(savefile2);
    
    
    
end

%%
% ------------
% PLOTTING, ETC
% ------------
figdir = '/data25/sjadhav/HPExpt/Figures/GLM/ErrDecrease/'; 
forppr=0; set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
if forppr==1
    set(0,'defaultaxesfontsize',20);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end

switch val
    case 1
        area = 'PFCripmod'
    case 2
        area = 'PFCexc'
    case 3
        area = 'PFCinh'    
    case 4
        area = 'PFCunmod'
end
    

fractionSigPredictableK=mean(allPsK<0.05)
fractionSigPredictableTheta=mean(allPs_theta<0.05)

% These alternatives yield almost identical result:
% 1.
%errDecrease=(1-allErrReal./allErrShuf);
%errDecrease_theta=(1-allErrReal_theta./allErrShuf_theta);
% 2.
errDecrease=(allErrShuf./allErrReal-1);
errDecrease_theta=(allErrShuf_theta./allErrReal_theta-1);
errDecrease_ripTOtheta=(allErrShuf_ripTOtheta./allErrReal_ripTOtheta-1);


% sig rate SWRs
sigratebycells=[];
sigrate1to5=mean(allPsK(allnumcells<=5)<0.05); numsig1to5=sum(allPsK(allnumcells<=5)<0.05);numcells1to5=length(allPsK(allnumcells<=5)<0.05);
sigrate6to10=mean(allPsK(allnumcells>5&allnumcells<=10)<0.05);numsig6to10=sum(allPsK(allnumcells>5&allnumcells<=10)<0.05);numcells6to10=length(allPsK(allnumcells>5&allnumcells<=10)<0.05);
sigrate11to30=mean(allPsK(allnumcells>10&allnumcells<=30)<0.05);numsig11to30=sum(allPsK(allnumcells>10&allnumcells<=30)<0.05);numcells11to30=length(allPsK(allnumcells>10&allnumcells<=30)<0.05);
sigratebycells=[sigrate1to5 sigrate6to10 sigrate11to30];
numsigbycells = [numsig1to5 numsig6to10 numsig11to30];
numcellsbycells = [numcells1to5 numcells6to10 numcells11to30]
Ncells = sum(numcellsbycells) % should be same as counter

% sig rate theta
sigratebycellstheta=[];
sigrate1to5T=mean(allPs_theta(allnumcells<=5)<0.05); numsig1to5T=sum(allPs_theta(allnumcells<=5)<0.05);numcells1to5T=length(allPs_theta(allnumcells<=5)<0.05);
sigrate6to10T=mean(allPs_theta(allnumcells>5&allnumcells<=10)<0.05);numsig6to10T=sum(allPs_theta(allnumcells>5&allnumcells<=10)<0.05);numcells6to10T=length(allPs_theta(allnumcells>5&allnumcells<=10)<0.05);
sigrate11to30T=mean(allPs_theta(allnumcells>10&allnumcells<=30)<0.05);numsig11to30T=sum(allPs_theta(allnumcells>10&allnumcells<=30)<0.05);numcells11to30T=length(allPs_theta(allnumcells>10&allnumcells<=30)<0.05);
sigratebycellstheta=[sigrate1to5T sigrate6to10T sigrate11to30T];

% sig rate ripTOtheta
sigratebycellsripTOtheta=[];
sigrate1to5rT=mean(allPs_ripTOtheta(allnumcells<=5)<0.05); numsig1to5rT=sum(allPs_ripTOtheta(allnumcells<=5)<0.05);numcells1to5rT=length(allPs_ripTOtheta(allnumcells<=5)<0.05);
sigrate6to10rT=mean(allPs_ripTOtheta(allnumcells>5&allnumcells<=10)<0.05);numsig6to10rT=sum(allPs_ripTOtheta(allnumcells>5&allnumcells<=10)<0.05);numcells6to10rT=length(allPs_ripTOtheta(allnumcells>5&allnumcells<=10)<0.05);
sigrate11to30rT=mean(allPs_ripTOtheta(allnumcells>10&allnumcells<=30)<0.05);numsig11to30rT=sum(allPs_ripTOtheta(allnumcells>10&allnumcells<=30)<0.05);numcells11to30rT=length(allPs_ripTOtheta(allnumcells>10&allnumcells<=30)<0.05);
sigratebycellsripTOtheta=[sigrate1to5rT sigrate6to10rT sigrate11to30rT];


% both
figure;bar([sigratebycells;sigratebycellstheta;sigratebycellsripTOtheta]')
text(0.65,1.1,[num2str(numsig1to5) '/' num2str(numcells1to5)])
text(1.65,1.1,[num2str(numsig6to10) '/' num2str(numcells6to10)])
text(2.65,1.1,[num2str(numsig11to30) '/' num2str(numcells11to30)])

text(0.95,1.1,[num2str(numsig1to5T) '/' num2str(numcells1to5T)])
text(1.95,1.1,[num2str(numsig6to10T) '/' num2str(numcells6to10T)])
text(2.95,1.1,[num2str(numsig11to30T) '/' num2str(numcells11to30T)])

text(1.2,1.1,[num2str(numsig1to5rT) '/' num2str(numcells1to5rT)])
text(2.2,1.1,[num2str(numsig6to10rT) '/' num2str(numcells6to10rT)])
text(3.2,1.1,[num2str(numsig11to30rT) '/' num2str(numcells11to30rT)])

ylim([0 1.2])
set(gca,'XTickLabel',{'1-5','6-10','11-30'})

%--------- error rate
% err rate SWRs
errratebycells=[];
errrate1to5=mean(errDecrease(allnumcells<=5)); errnumcells1to5=sum(allnumcells<=5);
errrate6to10=mean(errDecrease(allnumcells>5&allnumcells<=10));errnumcells6to10=sum(allnumcells>5&allnumcells<=10);
errrate11to30=mean(errDecrease(allnumcells>10&allnumcells<=30));errnumcells11to30=sum(allnumcells>10&allnumcells<=30);
errratebycells=[errrate1to5 errrate6to10 errrate11to30];
% SEM:
errrate1to5SEM=std(errDecrease(allnumcells<=5))/sqrt(sum(allnumcells<=5));
errrate6to10SEM=std(errDecrease(allnumcells>5&allnumcells<=10))/sqrt(sum(allnumcells>5&allnumcells<=10));
errrate11to30SEM=std(errDecrease(allnumcells>10&allnumcells<=30))/sqrt(sum(allnumcells>10&allnumcells<=30));
errratebycellsSEM=[errrate1to5SEM errrate6to10SEM errrate11to30SEM];


% err rate theta
errratebycellstheta=[];
errrate1to5=mean(errDecrease_theta(allnumcells<=5));
errrate6to10=mean(errDecrease_theta(allnumcells>5&allnumcells<=10));
errrate11to30=mean(errDecrease_theta(allnumcells>10&allnumcells<=30));
errratebycellstheta=[errrate1to5 errrate6to10 errrate11to30];
% SEM:
errrate1to5SEM=std(errDecrease_theta(allnumcells<=5))/sqrt(sum(allnumcells<=5));
errrate6to10SEM=std(errDecrease_theta(allnumcells>5&allnumcells<=10))/sqrt(sum(allnumcells>5&allnumcells<=10));
errrate11to30SEM=std(errDecrease_theta(allnumcells>10&allnumcells<=30))/sqrt(sum(allnumcells>10&allnumcells<=30));
errratebycellsthetaSEM=[errrate1to5SEM errrate6to10SEM errrate11to30SEM];


% err rate ripTOtheta
errratebycellsripTOtheta=[];
errrate1to5=mean(errDecrease_ripTOtheta(allnumcells<=5));
errrate6to10=mean(errDecrease_ripTOtheta(allnumcells>5&allnumcells<=10));
errrate11to30=mean(errDecrease_ripTOtheta(allnumcells>10&allnumcells<=30));
errratebycellsripTOtheta=[errrate1to5 errrate6to10 errrate11to30];
% SEM:
errrate1to5SEM=std(errDecrease_ripTOtheta(allnumcells<=5))/sqrt(sum(allnumcells<=5));
errrate6to10SEM=std(errDecrease_ripTOtheta(allnumcells>5&allnumcells<=10))/sqrt(sum(allnumcells>5&allnumcells<=10));
errrate11to30SEM=std(errDecrease_ripTOtheta(allnumcells>10&allnumcells<=30))/sqrt(sum(allnumcells>=10&allnumcells<=30));
errratebycellsripTOthetaSEM=[errrate1to5SEM errrate6to10SEM errrate11to30SEM];

% both
figure;
bar(0.9:2:6,errratebycells,0.25,'linewidth',2);
hold on;
errorbar(0.9:2:6,errratebycells,errratebycellsSEM,'.k','linewidth',2)

bar(1.5:2:6,errratebycellstheta,0.25,'y','linewidth',2);
errorbar(1.5:2:6,errratebycellstheta,errratebycellsthetaSEM,'.k','linewidth',2)

bar(2.1:2:7,errratebycellsripTOtheta,0.25,'g','linewidth',2);
errorbar(2.1:2:7,errratebycellsripTOtheta,errratebycellsripTOthetaSEM,'.k','linewidth',2)


% text(0.7,.085,[num2str(errnumcells1to5)])
% text(1.7,.085,[num2str(errnumcells6to10)])
% text(2.7,.085,[num2str(errnumcells11to30)])
% 
% text(1.1,.085,[num2str(errnumcells1to5)])
% text(2.1,.085,[num2str(errnumcells6to10)])
% text(3.1,.085,[num2str(errnumcells11to30)])
% 
ylim([0 0.09])
set(gca,'XTick',[1.5:2:6],'XTickLabel',{'1-5','6-10','11-30'})
title(['rip/ thetaTOrip/ ripTOtheta'],'Fontsize',24)
xlabel('Number of CA1 predictors')
ylabel('% improvement over shuffled','FontSize',24,'FontWeight','normal');

figdir = '/data25/sjadhav/HPExpt/Figures/GLM/ErrDecrease/'; 
figfile = [figdir,'ErrDecreaseWithTheta_',area]
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
end


% Compare errDecrease to each other and to 0
% ---------------------------------------------
% For allripmod
100*nanmean(errDecrease), 100*nansem(errDecrease)
100*nanmean(errDecrease_theta), 100*nansem(errDecrease_theta)
100*nanmean(errDecrease_ripTOtheta), 100*nansem(errDecrease_ripTOtheta)
ranksum(errDecrease,errDecrease_theta) %p=0.04   ttest2 p=0.11
ranksum(errDecrease,errDecrease_ripTOtheta) %p=6e-4  ttest2 p=0.001
ranksum(errDecrease_ripTOtheta,errDecrease_theta) %p=0.06   ttest2 p=0.11

% Compare the HIGH-predictors bar
errrate11to30_theta=errDecrease_theta(allnumcells>10&allnumcells<=30);
errDecreaseHigh_theta = errrate11to30_theta;
errrate11to30_ripTOtheta = errDecrease_ripTOtheta(allnumcells>=10&allnumcells<=30); % mean = 2.4%%
errDecreaseHigh_ripTOtheta = errrate11to30_ripTOtheta;
ranksum(errDecreaseHigh_ripTOtheta,errDecreaseHigh_theta) % 0.03


%% Alltypes. % p = 0.006
alldata=[errDecrease errDecrease_theta errDecrease_ripTOtheta];
numcells=ones(1,length(errDecrease));
numcellgroup = [numcells 2*numcells 3*numcells];
p=anovan(alldata,{numcellgroup});

%% Two types % p = 0.11
alldata=[errDecrease_theta errDecrease_ripTOtheta];
numcells=ones(1,length(errDecrease));
numcellgroup = [numcells 2*numcells];
p=anovan(alldata,{numcellgroup});

%% Two types with numcells.  theta-TO-rip vs rip-TO-theta
numcellgroupAllripmod=zeros(1,length(allnumcells));
numcellgroupAllripmod(allnumcells<=5)=1; Ncells_mod1to5 = length(find(allnumcells<=5));
numcellgroupAllripmod(allnumcells>5&allnumcells<=10)=2; Ncells_mod6to10 = length(find(allnumcells>5&allnumcells<=10));
numcellgroupAllripmod(allnumcells>=10&allnumcells<=30)=3; Ncells_mod11to30 = length(find(allnumcells>=10&allnumcells<=30));

alldata=[errDecrease_theta errDecrease_ripTOtheta];
modunmod=[ones(1,length(errDecrease_theta)) 2*ones(1,length(errDecrease_ripTOtheta))];
numcellgroup=[numcellgroupAllripmod  numcellgroupAllripmod];
p=anovan(alldata,{modunmod,numcellgroup},'model','interaction')  %p=0.08/0.11, p=0, p=0.01 for interaction
%p=anovan(alldata,{modunmod})
numcellsubgroup = [[numcellgroupAllripmod  3+numcellgroupAllripmod]]
[p,t,stats] = anovan(alldata,{modunmod,numcellgroup, numcellsubgroup}) % p=0.01
[c,m,h,nms] = multcompare(stats);
[p,t,stats] = anovan(alldata,{numcellsubgroup}) % groups 1to6. 1vs3=0, 2vs3=0.0001/ IMP- across grps. 3 vs 6 0.0076
[c,m,h,nms] = multcompare(stats); %



% Is errdecrease significantly greater than 0?
% ---------------------------------------------
% a) errDecrease
% Get normal ditr with same st dev as your distr
stdev = std(errDecrease); n = length(errDecrease);
dist = normrnd(0,stdev,n,1); % or dist = randn(n,1) + stdev;
%[dist1, dist2] = hist(dist,[-0.4:binsize:0.4]);
% Get random distr with same st dev as your distr 100 times, and check mean or median

mediandist=[];
for s = 1:1000
    dist = normrnd(0,stdev,n,1);
    mediandist(s) = median(dist);
end
pval_rip = length(find(median(errDecrease)<mediandist))./1000, % p = 0.003  // 
%figure; hold on; hist(mediandist);

% b) errDecrease_theta
stdev = std(errDecrease_theta); n = length(errDecrease_theta);
dist = normrnd(0,stdev,n,1); % or dist = randn(n,1) + stdev;
mediandist=[];
for s = 1:1000
    dist = normrnd(0,stdev,n,1);
    mediandist(s) = median(dist);
end
pval_thetaTOrip = length(find(median(errDecrease_theta)<mediandist))./1000, % p = 0.056 // %unmod 

% B2) errDecrease_theta for Npredictors=11-30
errrate11to30_theta=errDecrease_theta(allnumcells>10&allnumcells<=30); % n=27, mean=5.8%%
errDecreaseHigh_theta = errrate11to30_theta;
stdev = std(errDecreaseHigh_theta); n = length(errDecreaseHigh_theta);
dist = normrnd(0,stdev,n,1); % or dist = randn(n,1) + stdev;
mediandist=[];
for s = 1:1000
    dist = normrnd(0,stdev,n,1);
    mediandist(s) = median(dist);
end
pval_thetaTOripHigh = length(find(median(errDecreaseHigh_theta)<mediandist))./1000, % p=0.003  // % unmod



% c) errDecrease_ripTOtheta
stdev = std(errDecrease_ripTOtheta); n = length(errDecrease_ripTOtheta);
dist = normrnd(0,stdev,n,1); % or dist = randn(n,1) + stdev;
mediandist=[];
for s = 1:1000
    dist = normrnd(0,stdev,n,1);
    mediandist(s) = median(dist);
end
pval_ripTOtheta = length(find(median(errDecrease_ripTOtheta)<mediandist))./1000, % p = 0.21

% C2) errDecrease_ripTOtheta for Npredictors=11-30
errrate11to30_ripTOtheta = errDecrease_ripTOtheta(allnumcells>=10&allnumcells<=30); % mean = 2.4%%
errDecreaseHigh_ripTOtheta = errrate11to30_ripTOtheta;
stdev = std(errDecreaseHigh_ripTOtheta); n = length(errDecreaseHigh_ripTOtheta);
dist = normrnd(0,stdev,n,1); % or dist = randn(n,1) + stdev;
mediandist=[];
for s = 1:1000
    dist = normrnd(0,stdev,n,1);
    mediandist(s) = median(dist);
end
pval_ripTOthetaHigh = length(find(median(errDecreaseHigh_ripTOtheta)<mediandist))./1000, % p = 0.005







keyboard;


%% Statistics between groups
%savedir = '/opt/data15/gideon/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';

gatherdatafileAllripmod = [savedir 'HP_ripmod_glmfit_theta_gather6allripmod_X8'];
gatherdatafileExc = [savedir 'HP_ripmod_glmfit_theta_gather6ripexc_X8'];
gatherdatafileInh = [savedir 'HP_ripmod_glmfit_theta_gather6ripinh_X8'];
gatherdatafileUnmod = [savedir 'HP_ripmod_glmfit_theta_gather6unripmod_X8'];


load(gatherdatafileAllripmod);
errDecreaseAllripmod=(allErrShuf./allErrReal-1);
errDecrease_thetaAllripmod=(allErrShuf_theta./allErrReal_theta-1);
errDecrease_ripTOthetaAllripmod=(allErrShuf_ripTOtheta./allErrReal_ripTOtheta-1);
numcellgroupAllripmod=zeros(1,length(allnumcells));
numcellgroupAllripmod(allnumcells<=5)=1; Ncells_mod1to5 = length(find(allnumcells<=5));
numcellgroupAllripmod(allnumcells>5&allnumcells<=10)=2; Ncells_mod6to10 = length(find(allnumcells>5&allnumcells<=10));
numcellgroupAllripmod(allnumcells>=10&allnumcells<=30)=3; Ncells_mod11to30 = length(find(allnumcells>=10&allnumcells<=30));

errDecreaseHigh_Allripmod = (errDecreaseAllripmod(allnumcells>=10&allnumcells<=30));
errDecreaseHigh_thetaAllripmod = (errDecrease_thetaAllripmod(allnumcells>=10&allnumcells<=30));
errDecreaseHigh_ripTOthetaAllripmod = (errDecrease_ripTOthetaAllripmod(allnumcells>=10&allnumcells<=30));


load(gatherdatafileExc);
errDecreaseExc=(allErrShuf./allErrReal-1);
errDecrease_thetaExc=(allErrShuf_theta./allErrReal_theta-1);
errDecrease_ripTOthetaExc=(allErrShuf_ripTOtheta./allErrReal_ripTOtheta-1);
numcellgroupExc=zeros(1,length(allnumcells));
numcellgroupExc(allnumcells<=5)=1; Ncells_exc1to5 = length(find(allnumcells<=5));
numcellgroupExc(allnumcells>5&allnumcells<=10)=2; Ncells_exc6to10 = length(find(allnumcells>5&allnumcells<=10));
numcellgroupExc(allnumcells>10&allnumcells<=30)=3; Ncells_exc11to30 = length(find(allnumcells>10&allnumcells<=30));

load(gatherdatafileInh);
errDecreaseInh=(allErrShuf./allErrReal-1);
errDecrease_thetaInh=(allErrShuf_theta./allErrReal_theta-1);
errDecrease_ripTOthetaInh=(allErrShuf_ripTOtheta./allErrReal_ripTOtheta-1);
numcellgroupInh=zeros(1,length(allnumcells));
numcellgroupInh(allnumcells<=5)=1; Ncells_inh1to5 = length(find(allnumcells<=5));
numcellgroupInh(allnumcells>5&allnumcells<=10)=2; Ncells_inh6to10 = length(find(allnumcells>5&allnumcells<=10));
numcellgroupInh(allnumcells>10&allnumcells<=30)=3; Ncells_inh11to30 = length(find(allnumcells>10&allnumcells<=30));

load(gatherdatafileUnmod);
errDecreaseUnmod=(allErrShuf./allErrReal-1);
errDecrease_thetaUnmod=(allErrShuf_theta./allErrReal_theta-1);
errDecrease_ripTOthetaUnmod=(allErrShuf_ripTOtheta./allErrReal_ripTOtheta-1);
numcellgroupUnmod=zeros(1,length(allnumcells));
numcellgroupUnmod(allnumcells<=5)=1; Ncells_unmod1to5 = length(find(allnumcells<=5));
numcellgroupUnmod(allnumcells>5&allnumcells<=10)=2; Ncells_unmod6to10 = length(find(allnumcells>5&allnumcells<=10));
numcellgroupUnmod(allnumcells>10&allnumcells<=30)=3; Ncells_unmod11to30 = length(find(allnumcells>10&allnumcells<=30));

errDecreaseHigh_Unmod = (errDecreaseUnmod(allnumcells>=10&allnumcells<=30));
errDecreaseHigh_thetaUnmod = (errDecrease_thetaUnmod(allnumcells>=10&allnumcells<=30));
errDecreaseHigh_ripTOthetaUnmod = (errDecrease_ripTOthetaUnmod(allnumcells>10&allnumcells<=30));


% do direct comparison of errDecrease for ripmod and unmod cells
ranksum(errDecreaseAllripmod, errDecreaseUnmod) %p=0.004
ranksum(errDecrease_thetaAllripmod, errDecrease_thetaUnmod) %p=0.06
ranksum(errDecrease_ripTOthetaAllripmod, errDecrease_ripTOthetaUnmod) %p=0.7


% do ranksum for only 3rd bar, when no. of predcitors is high
ranksum(errDecreaseHigh_Allripmod, errDecreaseHigh_Unmod) % p=0.0023
ranksum(errDecreaseHigh_thetaAllripmod, errDecreaseHigh_thetaUnmod) % p=0.0104
ranksum(errDecreaseHigh_ripTOthetaAllripmod, errDecreaseHigh_ripTOthetaUnmod) %p=0.22   % 2.4+/-0.87  vs 1.1+/-0.37
ttest2(errDecreaseHigh_ripTOthetaAllripmod, errDecreaseHigh_ripTOthetaUnmod) %p=0.13   

figure; hold on; 
bar([nanmean(errDecreaseHigh_ripTOthetaAllripmod), nanmean(errDecreaseHigh_ripTOthetaUnmod)])
errorbar([nanmean(errDecreaseHigh_ripTOthetaAllripmod), nanmean(errDecreaseHigh_ripTOthetaUnmod)],[nansem(errDecreaseHigh_ripTOthetaAllripmod), nansem(errDecreaseHigh_ripTOthetaUnmod)],'.k','linewidth',2)

n1 = histc(errDecreaseHigh_ripTOthetaAllripmod,[-0.15:0.02:0.15]);
n2 = histc(errDecreaseHigh_ripTOthetaUnmod,[-0.15:0.02:0.15]);
figure; hold on; 
bar([-0.15:0.02:0.15],[n1',n2'],'grouped');

%% effct of cell number on only ripmod cells, SWR-trained and then theta-trained separately
alldata=[errDecreaseAllripmod];
%modunmod=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecreaseUnmod))];
numcellgroup=[numcellgroupAllripmod];
p=anovan(alldata,{numcellgroup})
%% theta-trained data
alldata=[errDecrease_thetaAllripmod];
numcellgroup=[numcellgroupAllripmod ];
p=anovan(alldata,{numcellgroup})
%%  ripTOtheta-trained data
alldata=[errDecrease_ripTOthetaAllripmod];
numcellgroup=[numcellgroupAllripmod];
p=anovan(alldata,{numcellgroup})




%% testing effect of cell number
alldata=[errDecreaseAllripmod errDecreaseUnmod];
%modunmod=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecreaseUnmod))];
numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
p=anovan(alldata,{numcellgroup})

%% testing effect of cell number on theta-trained data
alldata=[errDecrease_thetaAllripmod errDecrease_thetaUnmod];
numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
p=anovan(alldata,{numcellgroup})

%% testing effect of cell number on ripTOtheta-trained data
alldata=[errDecrease_ripTOthetaAllripmod errDecrease_ripTOthetaUnmod];
numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
p=anovan(alldata,{numcellgroup})



%% comparing SWR-modulated and SWR-unmodulated
alldata=[errDecreaseAllripmod errDecreaseUnmod];
modunmod=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecreaseUnmod))];
numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
p=anovan(alldata,{modunmod,numcellgroup})

%% comparing thetaTOrip SWR-modulated and SWR-unmodulated
alldata=[errDecrease_thetaAllripmod errDecrease_thetaUnmod];
modunmod=[ones(1,length(errDecrease_thetaAllripmod)) 2*ones(1,length(errDecrease_thetaUnmod))];
numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
p=anovan(alldata,{modunmod,numcellgroup})

%% comparing ripTOtheta SWR-modulated and SWR-unmodulated
alldata=[errDecrease_ripTOthetaAllripmod errDecrease_ripTOthetaUnmod];
modunmod=[ones(1,length(errDecrease_ripTOthetaAllripmod)) 2*ones(1,length(errDecrease_ripTOthetaUnmod))];
numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
p=anovan(alldata,{modunmod,numcellgroup})



%% comparing SWR-exc and SWR-inh
alldata=[errDecreaseExc errDecreaseInh];
excinh=[ones(1,length(errDecreaseExc)) 2*ones(1,length(errDecreaseInh))];
numcellgroup=[numcellgroupExc numcellgroupInh ];
p=anovan(alldata,{excinh,numcellgroup})

%% Test effect of cell number for exc and inh
alldata=[errDecreaseExc errDecreaseInh];
excinh=[ones(1,length(errDecreaseExc)) 2*ones(1,length(errDecreaseInh))];
numcellgroup=[numcellgroupExc numcellgroupInh ];
p=anovan(alldata,{numcellgroup})




%% comparing theta trained/SWR trained for swr-modulated cells
alldata=[errDecreaseAllripmod errDecrease_thetaAllripmod errDecreaseUnmod errDecrease_thetaUnmod];
modunmod=[ones(1,length(errDecreaseAllripmod)+length(errDecrease_thetaAllripmod)) 2*ones(1,length(errDecreaseUnmod)+length(errDecrease_thetaUnmod))];
riptheta=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecrease_thetaAllripmod)) ones(1,length(errDecreaseUnmod)) 2*ones(1,length(errDecrease_thetaUnmod)) ];
numcellgroup=[numcellgroupAllripmod numcellgroupAllripmod numcellgroupUnmod numcellgroupUnmod];
p=anovan(alldata,{modunmod,riptheta,numcellgroup})

%% comparing theta trained/SWR trained for swr-modulated cells, with mod/unmod distinction
alldata=[errDecreaseAllripmod errDecrease_thetaAllripmod errDecreaseUnmod errDecrease_thetaUnmod];
modunmod=[ones(1,length(errDecreaseAllripmod)+length(errDecrease_thetaAllripmod)) 2*ones(1,length(errDecreaseUnmod)+length(errDecrease_thetaUnmod))];
riptheta=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecrease_thetaAllripmod)) ones(1,length(errDecreaseUnmod)) 2*ones(1,length(errDecrease_thetaUnmod)) ];
numcellgroup=[numcellgroupAllripmod numcellgroupAllripmod numcellgroupUnmod numcellgroupUnmod];
p=anovan(alldata,{modunmod,riptheta,numcellgroup})

%% comparing rip-to-theta trained/SWR trained for swr-excited and swr-inhibited cells
alldata=[errDecreaseExc errDecrease_thetaExc errDecreaseInh errDecrease_thetaInh];
excinh=[ones(1,length(errDecreaseExc)+length(errDecrease_thetaExc)) 2*ones(1,length(errDecreaseInh)+length(errDecrease_thetaInh))];
riptheta=[ones(1,length(errDecreaseExc)) 2*ones(1,length(errDecrease_thetaExc)) ones(1,length(errDecreaseInh)) 2*ones(1,length(errDecrease_thetaInh)) ];
numcellgroup=[numcellgroupExc numcellgroupExc numcellgroupInh numcellgroupInh];
p=anovan(alldata,{excinh,riptheta,numcellgroup})

[p tbl stats] = anovan(alldata,{excinh,riptheta,numcellgroup},'model','interaction','varnames',{'excinh','riptheta','numcellgroup'})
results = multcompare(stats,'Dimension',[1])

%%
alldata=[errDecreaseExc errDecrease_thetaExc errDecreaseInh errDecrease_thetaInh errDecreaseUnmod errDecrease_thetaUnmod];
excinhunmod=[ones(1,length(errDecreaseExc)+length(errDecrease_thetaExc)) 2*ones(1,length(errDecreaseInh)+length(errDecrease_thetaInh)) 3*ones(1,length(errDecreaseUnmod)+length(errDecrease_thetaUnmod))];
riptheta=[ones(1,length(errDecreaseExc)) 2*ones(1,length(errDecrease_thetaExc)) ones(1,length(errDecreaseInh)) 2*ones(1,length(errDecrease_thetaInh)) ones(1,length(errDecreaseUnmod)) 2*ones(1,length(errDecrease_thetaUnmod))];
numcellgroup=[numcellgroupExc numcellgroupExc numcellgroupInh numcellgroupInh numcellgroupUnmod numcellgroupUnmod];
p=anovan(alldata,{excinhunmod,riptheta,numcellgroup})
[p tbl stats] = anovan(alldata,{excinhunmod,riptheta,numcellgroup},'model','interaction','varnames',{'excinhunmod','riptheta','numcellgroup'})
results = multcompare(stats,'Dimension',[1])


