

% Get fraction of SWRs in which cells are active: for CA1, PFC. Makes most sense for CA1
% Similar pattern in
% CA1PFCcrosscorrelograms_SJ.m
% plotCorrRasters.m
% ripTrigTimingAnalysisHCPFC_SJ.m

clear; %pack;
figdir = '/data25/sjadhav/HPExpt/Figures/RipProp//';
savefig1=0;
set(0,'defaultaxesfontsize',24);

savedirX = '/data25/sjadhav/HPExpt/HP_ProcessedData/';

% CA1 ripplemod file
load([savedirX 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6_CA2tag'])
allripplemodCA1 = allripplemod;
allripplemod_idxCA1 = allripplemod_idx;

% PFC ripplemod file
load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6'])
allripplemodPFC = allripplemod;
allripplemod_idxPFC = allripplemod_idx;


% -------------
% 1. Single Cells
% ------------

% Do CA1 first:
% ------------
fracresp_CA1 = [];
nCA1 = length(allripplemod_idxCA1);
for i=1:nCA1
    curridx = allripplemod_idxCA1(i,:);
    
    if allripplemodCA1(i).rasterShufP2<0.05 & strcmp(allripplemodCA1(i).CA2tag,'n') & strcmp(allripplemodCA1(i).FStag,'n')
        CA1hist=rast2mat(allripplemodCA1(i).raster);
        CA1amp=sum(CA1hist(:,500:700),2);
        CA1fires=find(CA1amp>0);
        CA1silent=find(CA1amp==0);
        
        Nswr = length(CA1amp);
        Nswrresp = length(find(CA1amp>0));
        fracresp_CA1 = [fracresp_CA1; Nswrresp./Nswr];
        
    end
end

% Do PFC, and separate by PFCexc, PFCinh and PFCunmod
% --------------------------------------------------
fracresp_PFCexc = [];
fracresp_PFCinh = [];
fracresp_PFCunmod = [];
nPFC = length(allripplemod_idxPFC);

for i=1:nPFC
    curridx = allripplemod_idxCA1(i,:);
    
    % PFC
    if strcmp(allripplemodPFC(i).FStag,'n')
        PFChist=rast2mat(allripplemodPFC(i).raster);
        PFCamp=sum(PFChist(:,500:700),2);
        PFCfires=find(PFCamp>0);
        PFCsilent=find(PFCamp==0);
        
        Nswr = length(PFCamp);
        Nswrresp = length(find(PFCamp>0));
        
        % PFCexc
        if allripplemodPFC(i).rasterShufP2<0.05 & strcmp(allripplemodPFC(i).type,'exc')
            fracresp_PFCexc = [fracresp_PFCexc; Nswrresp./Nswr];
        end
        
        % PFCinh
        if allripplemodPFC(i).rasterShufP2<0.05 & strcmp(allripplemodPFC(i).type,'inh')
            fracresp_PFCinh = [fracresp_PFCinh; Nswrresp./Nswr];
        end
        
        % PFCunmod
        if allripplemodPFC(i).rasterShufP2>=0.05
            fracresp_PFCunmod = [fracresp_PFCunmod; Nswrresp./Nswr];
        end
        
        
    end
end

% Plot Bar Graph for this
figure; hold on;
%barwitherr([std(excexcthetapeakcorr)/sqrt(length(excexcthetapeakcorr)) std(excinhthetapeakcorr)/sqrt(length(excinhthetapeakcorr)) std(inhinhthetapeakcorr)/sqrt(length(inhinhthetapeakcorr))],[mean(excexcthetapeakcorr) mean(excinhthetapeakcorr) mean(inhinhthetapeakcorr)])
%set(gca,'xticklabel',{'EXC/EXC','EXC/INH','INH/INH'})
bar(1, nanmean(fracresp_CA1),'k');
bar(2, nanmean(fracresp_PFCexc),'r');
bar(3, nanmean(fracresp_PFCinh),'b');
bar(4, nanmean(fracresp_PFCunmod),'c');

%legend({'Ca1','PFC-Exc','PFC-Inh','PFC-Unmod'})
errorbar2(1, nanmean(fracresp_CA1), nanstderr(fracresp_CA1), 0.3, 'k');
errorbar2(2, nanmean(fracresp_PFCexc), nanstderr(fracresp_PFCexc), 0.3, 'r');
errorbar2(3, nanmean(fracresp_PFCinh), nanstderr(fracresp_PFCinh), 0.3, 'b');
errorbar2(4, nanmean(fracresp_PFCunmod), nanstderr(fracresp_PFCunmod), 0.3, 'c');

title('Fraction of SWRs with spikes','FontSize',24,'FontWeight','normal');
ylabel('Fraction active SWRs','FontSize',24,'FontWeight','normal')

set(gca,'XTick',[]);

figdir = '/data25/sjadhav/HPExpt/Figures/RipProp/';
figfile = [figdir,'FractionActiveSWRs']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end


[p1,h1] = ranksum(fracresp_PFCexc, fracresp_PFCunmod)  %
[p2,h2] = ranksum(fracresp_PFCinh, fracresp_PFCunmod) %
[p3,h3] = ranksum(fracresp_PFCexc, fracresp_PFCinh)

[p4,h4] = ranksum(fracresp_PFCexc, fracresp_CA1)
[p5,h5] = ranksum(fracresp_PFCinh, fracresp_CA1) 
[p6,h6] = ranksum(fracresp_PFCunmod, fracresp_CA1) %
x=1;


% 
% 
% 
% 
% 
% 
% 
% 
% 
% % ------------------------------------------------------------------------
% % 2. Extend to Pairs based on CA1PFCcrosscorrelograms_SJ and plotCorrRasters: Can also use for other things
% % -----------------------------------------------------------------------
% combined_idx=unique([allripplemod_idxPFC(:,1:2)],'rows');
% numTrain=1000;
% scrsz = get(0,'ScreenSize');
% xx=[-500:500];
% 
% 
% 
% % Parameters just for no. of SWRs with coactive cell pais
% % --------------------------------------------------
% fracresp_CA1PFCexc = [];
% fracresp_CA1PFCinh = [];
% fracresp_CA1PFCunmod = [];
% 
% Nresp_CA1PFCexc = [];
% Nresp_CA1PFCinh = [];
% Nresp_CA1PFCunmod = [];
% 
% for v=1:size(combined_idx,1)
%     curidx=combined_idx(v,[1:2]);%animal day
%     PFCidx=find(ismember(allripplemod_idxPFC(:,1:2),curidx,'rows'));
%     CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
%     PFCmat=[];
%     for j=1:size(PFCidx,1)
%         for k=1:size(CA1idx,1)
%             PFCind1=PFCidx(j);
%             CA1ind1=CA1idx(k);
%             %if allripplemodPFC(1,PFCind1).rasterShufP2<0.05 & strcmp(allripplemodPFC(1,PFCind1).FStag,'n') & allripplemodCA1(1,CA1ind1).rasterShufP2<0.05 & strcmp(allripplemodCA1(1,CA1ind1).CA2tag,'n') & strcmp(allripplemodCA1(1,CA1ind1).FStag,'n')
%             if  strcmp(allripplemodPFC(1,PFCind1).FStag,'n') & allripplemodCA1(1,CA1ind1).rasterShufP2<0.05 & strcmp(allripplemodCA1(1,CA1ind1).CA2tag,'n') & strcmp(allripplemodCA1(1,CA1ind1).FStag,'n')
%                 
%                 fullPFCind=allripplemod_idxPFC(PFCind1,:);
%                 fullCA1ind=allripplemod_idxCA1(CA1ind1,:);
%                 
%                 PFChist=rast2mat(allripplemodPFC(1,PFCind1).raster);
%                 CA1hist=rast2mat(allripplemodCA1(1,CA1ind1).raster);
%                 PFCamp=sum(PFChist(:,500:700),2);
%                 CA1amp=sum(CA1hist(:,500:700),2);
%                 
%                 
%                 if strcmp(allripplemodCA1(1,CA1ind1).type,'exc')
%                     pfcSWRexcinh=strcmp(allripplemodPFC(1,PFCind1).type,'exc');
%                     
%                     CA1fires=find(CA1amp>0);
%                     CA1silent=find(CA1amp==0);
%                     PFCfires=find(PFCamp>0);
%                     PFCsilent=find(PFCamp==0);
%                     
%                     if length(PFCamp)==length(CA1amp)
%                         Nswr = length(PFCamp);
%                         Nswrresp = length(find(PFCamp>0 & CA1amp>0));
%                         
%                         % PFCexc
%                         if allripplemodPFC(1,PFCind1).rasterShufP2<0.05 & strcmp(allripplemodPFC(1,PFCind1).type,'exc')
%                             fracresp_CA1PFCexc = [fracresp_CA1PFCexc; Nswrresp./Nswr];
%                             Nresp_CA1PFCexc = [Nresp_CA1PFCexc; Nswrresp];
%                         end
%                         
%                         % PFCinh
%                         if allripplemodPFC(1,PFCind1).rasterShufP2<0.05 & strcmp(allripplemodPFC(1,PFCind1).type,'inh')
%                             fracresp_CA1PFCinh = [fracresp_CA1PFCinh; Nswrresp./Nswr];
%                             Nresp_CA1PFCinh = [Nresp_CA1PFCinh; Nswrresp];
%                         end
%                         
%                         % PFCunmod
%                         if allripplemodPFC(1,PFCind1).rasterShufP2>=0.05
%                             fracresp_CA1PFCunmod = [fracresp_CA1PFCunmod; Nswrresp./Nswr];
%                             Nresp_CA1PFCunmod = [Nresp_CA1PFCunmod; Nswrresp];
%                         end
%                         
%                         
%                         %if allripplemodPFC(1,PFCind1).rasterShufP2<0.05
%                         
%                         %                   % From plotCorrRasters: ONLY if: if allripplemodPFC(1,PFCind1).rasterShufP2<0.05
%                         %                    % --------------------
%                         %                     if length(PFCamp)==length(CA1amp)
%                         %                         [ripcorR ripcorP]=corrcoef(PFCamp,CA1amp);
%                         %                         CA1fires=find(CA1amp>0);
%                         %                         CA1silent=find(CA1amp==0);
%                         %                         [s1 s2]=sort(CA1amp);
%                         %                         PFCsort = PFChist(s2,:);
%                         %                         CA1sort = CA1hist(s2,:);
%                         %                     end
%                         
%                         
%                         %                     % FROM CA1PFCcrosscorrelograms_SJ: ONLY if: if allripplemodPFC(1,PFCind1).rasterShufP2<0.05
%                         %                     % -----------------------------------------------------------------------------------------
%                         %                     %now columns are ripples
%                         %                     CA1hist2=CA1hist';
%                         %                     PFChist2=PFChist';
%                         %
%                         %                     if size(PFChist2,2)==size(CA1hist2,2)
%                         %                         PFCh=PFChist2(:);
%                         %                         CA1h=CA1hist2(:);
%                         %                         currawcrosscorr=xcorr(PFCh,CA1h,500,'coeff')';
%                         %                         curcrosscorr=filtfilt(b,1,currawcrosscorr);
%                         %
%                         %                         % this outputs the same as currawcrosscor except the
%                         %                         % units, which I think are just co-occurrences
%                         %                         spikexcorrOut=spikexcorr(find(PFCh),find(CA1h),1,500);
%                         %
%                         %                         % cross-cov calculation from Siapas paper- verify
%                         %                         c1vsc2=spikexcorrOut.c1vsc2;
%                         %                         T=length(PFCh)/1000;
%                         %                         bin=0.001;
%                         %                         spikerate1=sum(PFCh)/T;
%                         %                         spikerate2=sum(CA1h)/T;
%                         %                         xc = (c1vsc2/(bin*T))-(spikerate1*spikerate2);
%                         %                         xcQ= xc*(sqrt((bin*T)./(spikerate1*spikerate2)));
%                         %                         nstd=round(0.01/bin); % 10ms/bin
%                         %                         g1 = gaussian(nstd, 5*nstd+1);
%                         %                         crosscov1 = smoothvect(xcQ, g1);
%                         %
%                         %                         % the PFC cell is SWR-excited
%                         %                         if pfcSWRexcinh
%                         %                             %allcrosscorrsSWRexc=[allcrosscorrsSWRexc; curcrosscorr];
%                         %                             allcrosscovsSWRexc=[allcrosscovsSWRexc; crosscov1];
%                         %                             % find peak time
%                         %                             [a maxind]=max(curcrosscorr(251:750));
%                         %                             allpeaksSWRexc=[allpeaksSWRexc maxind+250];
%                         %
%                         %                         else % the PFC cell is SWR-inhibited
%                         %                             %allcrosscorrsSWRinh=[allcrosscorrsSWRinh; curcrosscorr];
%                         %                             allcrosscovsSWRinh=[allcrosscovsSWRinh; crosscov1];
%                         %                             % find trough time
%                         %                             [a minind]=min(curcrosscorr(251:750));
%                         %                             allpeaksSWRinh=[allpeaksSWRinh minind+250];
%                         %                         end
%                         %
%                         %
%                         %                         % Timing relationship for early vs late CA1-PFC spikes like in other code
%                         %                         timer=timer+1
%                         %                         g2=gaussian(10,51);
%                         %                         g3=gaussian(3,3);
%                         %                         xaxis=-499:500;
%                         %                         % CA1 spiking vs not spiking
%                         %                         [firstSpike swrNum]=find(cumsum(cumsum(CA1hist(:,500:700)'))==1);
%                         %                         CA1wspikes=CA1hist(swrNum,:);
%                         %                         CA1wNospikes=CA1hist(setdiff(1:size(CA1hist,1),swrNum),:);
%                         %                         PFCwCA1spikes=PFChist(swrNum,:);
%                         %                         PFCwNoCA1spikes=PFChist(setdiff(1:size(CA1hist,1),swrNum),:);
%                         %                         pfcampwspikes=sum(PFCwCA1spikes(:,500:700),2);
%                         %                         pfcampnospikes=sum(PFCwNoCA1spikes(:,500:700),2);
%                         %                         %does the PFC cell fire more on SWRs that the CA1
%                         %                         %fired than when the CA1 was silent?
%                         %                         pfcsigdif=ranksum(pfcampwspikes,pfcampnospikes);
%                         %                         pfcsigdifs=[pfcsigdifs pfcsigdif];
%                         %                         % depth of modulation of PFC cell
%                         %                         pfcmod=(mean(pfcampwspikes)-mean(pfcampnospikes))/(mean(pfcampwspikes)+mean(pfcampnospikes));
%                         %                         pfcmods=[pfcmods pfcmod];
%                         %
%                         %                         % CA1-PFC CrossCorrln
%                         %                         CA1nspk = sum(CA1hist(:,500:700),2);
%                         %                         PFCnspk = sum(PFChist(:,500:700),2);
%                         %                         [rtmp, ptmp] = corrcoef(CA1nspk, PFCnspk);
%                         %                         r = rtmp(1,2); p = ptmp(1,2);
%                         %                         allr = [allr; r];
%                         %                         allp = [allp; p];
%                         %                         % CA1 spiking temporal order
%                         %                         [sortedFirstSpike sortedFirstSpikeX]=sort(firstSpike);
%                         %                         CA1wspikesSorted=CA1wspikes(sortedFirstSpikeX,:);
%                         %                         PFCwCA1spikesSorted=PFCwCA1spikes(sortedFirstSpikeX,:);
%                         %                         numswrsWspikes=size(CA1wspikes,1);
%                         %                         pfcearly=smoothvect(mean(PFCwCA1spikesSorted(1:round(numswrsWspikes/2),:)),g2);
%                         %                         pfclate=smoothvect(mean(PFCwCA1spikesSorted(round(numswrsWspikes/2)+1:end,:)),g2);
%                         %
%                         %                         pfcmatsmooth=gaussian(40,50);
%                         %                         smoothedpfc=(filtfilt(pfcmatsmooth,1,PFCwCA1spikes')');
%                         %                         smoothedpfc=zscore(smoothedpfc')';
%                         %
%                         %
%                         %                     end % size PFChist and CA1hist is the same
%                         %
%                         
%                         %end % PFC ripmod is significant
%                         
%                     end % length(PFCamp) = length(CA1amp)
%                     
%                     
%                 end %strcmp  CA1-exc
%                 
%             end
%         end
%     end
% end
% 
% 
% % Plot Bar Graph for pairs
% figure; hold on;
% %barwitherr([std(excexcthetapeakcorr)/sqrt(length(excexcthetapeakcorr)) std(excinhthetapeakcorr)/sqrt(length(excinhthetapeakcorr)) std(inhinhthetapeakcorr)/sqrt(length(inhinhthetapeakcorr))],[mean(excexcthetapeakcorr) mean(excinhthetapeakcorr) mean(inhinhthetapeakcorr)])
% %set(gca,'xticklabel',{'EXC/EXC','EXC/INH','INH/INH'})
% %bar(1, nanmean(fracresp_CA1),'k');
% bar(1, nanmean(fracresp_CA1PFCexc),'r');
% bar(2, nanmean(fracresp_CA1PFCinh),'b');
% bar(3, nanmean(fracresp_CA1PFCunmod),'c');
% 
% %legend({'Ca1','PFC-Exc','PFC-Inh','PFC-Unmod'})
% %errorbar2(1, nanmean(fracresp_CA1), nanstderr(fracresp_CA1), 0.3, 'k');
% errorbar2(1, nanmean(fracresp_CA1PFCexc), nanstderr(fracresp_CA1PFCexc), 0.3, 'r');
% errorbar2(2, nanmean(fracresp_CA1PFCinh), nanstderr(fracresp_CA1PFCinh), 0.3, 'b');
% errorbar2(3, nanmean(fracresp_CA1PFCunmod), nanstderr(fracresp_CA1PFCunmod), 0.3, 'c');
% 
% title('Fraction of co-active SWRs with spikes ','FontSize',24,'FontWeight','normal');
% ylabel('Fraction co-active SWRs','FontSize',24,'FontWeight','normal')
% 
% set(gca,'XTick',[]);
% 
% figdir = '/data25/sjadhav/HPExpt/Figures/RipProp/';
% figfile = [figdir,'FractionCo-ActiveSWRs']
% if savefig1==1,
%     print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
% end
% 
% 
% [p5,h5] = ranksum(fracresp_CA1PFCexc, fracresp_CA1PFCunmod)  %
% [p6,h6] = ranksum(fracresp_CA1PFCinh, fracresp_CA1PFCunmod) %
% [p7,h7] = ranksum(fracresp_CA1PFCexc, fracresp_CA1PFCinh) %0.11
% 
% 
% 
% figure; hold on;
% %barwitherr([std(excexcthetapeakcorr)/sqrt(length(excexcthetapeakcorr)) std(excinhthetapeakcorr)/sqrt(length(excinhthetapeakcorr)) std(inhinhthetapeakcorr)/sqrt(length(inhinhthetapeakcorr))],[mean(excexcthetapeakcorr) mean(excinhthetapeakcorr) mean(inhinhthetapeakcorr)])
% %set(gca,'xticklabel',{'EXC/EXC','EXC/INH','INH/INH'})
% bar(1, nanmean(Nresp_CA1PFCexc),'r');
% bar(2, nanmean(Nresp_CA1PFCinh),'b');
% bar(3, nanmean(Nresp_CA1PFCunmod),'c');
% 
% %legend({'PFC-Exc','PFC-Inh','PFC-Unmod'})
% errorbar2(1, nanmean(Nresp_CA1PFCexc), nanstderr(Nresp_CA1PFCexc), 0.3, 'r');
% errorbar2(2, nanmean(Nresp_CA1PFCinh), nanstderr(Nresp_CA1PFCinh), 0.3, 'b');
% errorbar2(3, nanmean(Nresp_CA1PFCunmod), nanstderr(Nresp_CA1PFCunmod), 0.3, 'c');
% 
% title('Number of co-active SWRs with spikes ','FontSize',24,'FontWeight','normal');
% ylabel('Number co-active SWRs','FontSize',24,'FontWeight','normal')
% 
% set(gca,'XTick',[]);
% 
% figdir = '/data25/sjadhav/HPExpt/Figures/RipProp/';
% figfile = [figdir,'NumberCo-ActiveSWRs']
% if savefig1==1,
%     print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
% end
% 
% [p8,h8] = ranksum(Nresp_CA1PFCexc, Nresp_CA1PFCunmod)  %
% [p9,h9] = ranksum(Nresp_CA1PFCinh, Nresp_CA1PFCunmod) %
% [p10,h10] = ranksum(Nresp_CA1PFCexc, Nresp_CA1PFCinh) %








% --------------------------------------------------------------------------------
% 3. Do Pairs like CA1-PFC Cross-Correlograms to look at pos and neg correlated pairs
% --------------------------------------------------------------------------------

combined_idx=unique([allripplemod_idxPFC(:,1:2)],'rows');
numTrain=1000;
scrsz = get(0,'ScreenSize');
xx=[-500:500];
converged1=[];
n = [];
nsig = [];
fracsig=[];
allErrReal=[];
allErrShuf=[];
allPs=[];
allPsK=[];
allinds=[];
allbetas={};
allsigs={};
allsigbetasvec=[];
allcrosscorrs=[];
allcrosscorrsSWRexc=[];
allcrosscorrsSWRinh=[];
allcrosscovsSWRexc=[];
allcrosscovsSWRinh=[];
plotpairs=0;
%b=fir1(51,0.05);
b=gaussian(20,61);
allpeaks=[];
allpeaksS=[];
allpeaksSWRexc=[];
allpeaksSWRinh=[];
timer=0;
plotres=0
crosscorrpeaktimes=[];
pfcsigdifs=[];
pfcmods=[];
pfclatepeakinds=[];
pfcearlypeakinds=[];
pfclatetroughinds=[];
pfcearlytroughinds=[];
pcorrs=[];
rcorrs=[];
% swr-exc =1
% swr-inh =2
rtimings=[];
ptimings=[];
b=gaussian(30,41);
% Do Corrlns
allr=[]; allp=[];

currSWRmod=[];
SWRmod_PFCexcpos = []; SWRmod_PFCexcneg = [];
SWRmod_PFCinhpos = []; SWRmod_PFCinhneg = [];

for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    PFCidx=find(ismember(allripplemod_idxPFC(:,1:2),curidx,'rows'));
    CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
    PFCmat=[];
    for j=1:size(PFCidx,1)
        for k=1:size(CA1idx,1)
            PFCind1=PFCidx(j);
            CA1ind1=CA1idx(k);
            if allripplemodPFC(1,PFCind1).rasterShufP2<0.05 & strcmp(allripplemodPFC(1,PFCind1).FStag,'n') & allripplemodCA1(1,CA1ind1).rasterShufP2<0.05 & strcmp(allripplemodCA1(1,CA1ind1).CA2tag,'n') & strcmp(allripplemodCA1(1,CA1ind1).FStag,'n')
                %if  strcmp(allripplemodPFC(1,PFCind1).FStag,'n') & allripplemodCA1(1,CA1ind1).rasterShufP2<0.05 & strcmp(allripplemodCA1(1,CA1ind1).CA2tag,'n') & strcmp(allripplemodCA1(1,CA1ind1).FStag,'n')
                
                fullPFCind=allripplemod_idxPFC(PFCind1,:);
                fullCA1ind=allripplemod_idxCA1(CA1ind1,:);
                
                PFChist=rast2mat(allripplemodPFC(1,PFCind1).raster);
                CA1hist=rast2mat(allripplemodCA1(1,CA1ind1).raster);
                PFCamp=sum(PFChist(:,500:700),2);
                CA1amp=sum(CA1hist(:,500:700),2);
                
                currSWRmod = allripplemodPFC(1,PFCind1).varRespAmp2; % Strength of SWR Modln for PFC neuron
                
                if strcmp(allripplemodCA1(1,CA1ind1).type,'exc')
                    pfcSWRexcinh=strcmp(allripplemodPFC(1,PFCind1).type,'exc');
                    
                    CA1fires=find(CA1amp>0);
                    CA1silent=find(CA1amp==0);
                    PFCfires=find(PFCamp>0);
                    PFCsilent=find(PFCamp==0);
                    
                    
                    % From plotCorrRasters: ONLY if: if allripplemodPFC(1,PFCind1).rasterShufP2<0.05
                    % --------------------
                    if length(PFCamp)==length(CA1amp)
                        [ripcorR ripcorP]=corrcoef(PFCamp,CA1amp);
                        CA1fires=find(CA1amp>0);
                        CA1silent=find(CA1amp==0);
                        [s1 s2]=sort(CA1amp);
                        PFCsort = PFChist(s2,:);
                        CA1sort = CA1hist(s2,:);
                        
                        % CA1 spiking vs not spiking
                        [firstSpike swrNum]=find(cumsum(cumsum(CA1hist(:,500:700)'))==1);
                        CA1wspikes=CA1hist(swrNum,:);
                        CA1wNospikes=CA1hist(setdiff(1:size(CA1hist,1),swrNum),:);
                        PFCwCA1spikes=PFChist(swrNum,:);
                        PFCwNoCA1spikes=PFChist(setdiff(1:size(CA1hist,1),swrNum),:);
                        pfcampwspikes=sum(PFCwCA1spikes(:,500:700),2);
                        pfcampnospikes=sum(PFCwNoCA1spikes(:,500:700),2);
                        
                        % CA1-PFC CrossCorrln
                        CA1nspk = sum(CA1hist(:,500:700),2);
                        PFCnspk = sum(PFChist(:,500:700),2);
                        [rtmp, ptmp] = corrcoef(CA1nspk, PFCnspk);
                        r = rtmp(1,2); p = ptmp(1,2);
                        allr = [allr; r];
                        allp = [allp; p];
                        
                        
                        % FROM CA1PFCcrosscorrelograms_SJ: ONLY if: if allripplemodPFC(1,PFCind1).rasterShufP2<0.05
                        % -----------------------------------------------------------------------------------------
                        %now columns are ripples
                        %                         CA1hist2=CA1hist';
                        %                         PFChist2=PFChist';
                        %
                        %                         %if size(PFChist2,2)==size(CA1hist2,2)
                        %                         PFCh=PFChist2(:);
                        %                         CA1h=CA1hist2(:);
                        %                         currawcrosscorr=xcorr(PFCh,CA1h,500,'coeff')';
                        %                         curcrosscorr=filtfilt(b,1,currawcrosscorr);
                        %
                        %                         % this outputs the same as currawcrosscor except the
                        %                         % units, which I think are just co-occurrences
                        %                         spikexcorrOut=spikexcorr(find(PFCh),find(CA1h),1,500);
                        %
                        %                         % cross-cov calculation from Siapas paper- verify
                        %                         c1vsc2=spikexcorrOut.c1vsc2;
                        %                         T=length(PFCh)/1000;
                        %                         bin=0.001;
                        %                         spikerate1=sum(PFCh)/T;
                        %                         spikerate2=sum(CA1h)/T;
                        %                         xc = (c1vsc2/(bin*T))-(spikerate1*spikerate2);
                        %                         xcQ= xc*(sqrt((bin*T)./(spikerate1*spikerate2)));
                        %                         nstd=round(0.01/bin); % 10ms/bin
                        %                         g1 = gaussian(nstd, 5*nstd+1);
                        %                         crosscov1 = smoothvect(xcQ, g1);
                        %
                        %                         % the PFC cell is SWR-excited
                        %                         if pfcSWRexcinh
                        %                             %allcrosscorrsSWRexc=[allcrosscorrsSWRexc; curcrosscorr];
                        %                             allcrosscovsSWRexc=[allcrosscovsSWRexc; crosscov1];
                        %                             % find peak time
                        %                             [a maxind]=max(curcrosscorr(251:750));
                        %                             allpeaksSWRexc=[allpeaksSWRexc maxind+250];
                        %
                        %                         else % the PFC cell is SWR-inhibited
                        %                             %allcrosscorrsSWRinh=[allcrosscorrsSWRinh; curcrosscorr];
                        %                             allcrosscovsSWRinh=[allcrosscovsSWRinh; crosscov1];
                        %                             % find trough time
                        %                             [a minind]=min(curcrosscorr(251:750));
                        %                             allpeaksSWRinh=[allpeaksSWRinh minind+250];
                        %                         end
                        
                        
                        %                         % Timing relationship for early vs late CA1-PFC spikes like in other code
                        %                         timer=timer+1
                        %                         g2=gaussian(10,51);
                        %                         g3=gaussian(3,3);
                        %                         xaxis=-499:500;
                        %                         % CA1 spiking vs not spiking
                        %                         [firstSpike swrNum]=find(cumsum(cumsum(CA1hist(:,500:700)'))==1);
                        %                         CA1wspikes=CA1hist(swrNum,:);
                        %                         CA1wNospikes=CA1hist(setdiff(1:size(CA1hist,1),swrNum),:);
                        %                         PFCwCA1spikes=PFChist(swrNum,:);
                        %                         PFCwNoCA1spikes=PFChist(setdiff(1:size(CA1hist,1),swrNum),:);
                        %                         pfcampwspikes=sum(PFCwCA1spikes(:,500:700),2);
                        %                         pfcampnospikes=sum(PFCwNoCA1spikes(:,500:700),2);
                        %                         %does the PFC cell fire more on SWRs that the CA1
                        %                         %fired than when the CA1 was silent?
                        %                         pfcsigdif=ranksum(pfcampwspikes,pfcampnospikes);
                        %                         pfcsigdifs=[pfcsigdifs pfcsigdif];
                        %                         % depth of modulation of PFC cell
                        %                         pfcmod=(mean(pfcampwspikes)-mean(pfcampnospikes))/(mean(pfcampwspikes)+mean(pfcampnospikes));
                        %                         pfcmods=[pfcmods pfcmod];
                        %
                        %                         % CA1-PFC CrossCorrln
                        %                         CA1nspk = sum(CA1hist(:,500:700),2);
                        %                         PFCnspk = sum(PFChist(:,500:700),2);
                        %                         [rtmp, ptmp] = corrcoef(CA1nspk, PFCnspk);
                        %                         r = rtmp(1,2); p = ptmp(1,2);
                        %                         allr = [allr; r];
                        %                         allp = [allp; p];
                        %                         % CA1 spiking temporal order
                        %                         [sortedFirstSpike sortedFirstSpikeX]=sort(firstSpike);
                        %                         CA1wspikesSorted=CA1wspikes(sortedFirstSpikeX,:);
                        %                         PFCwCA1spikesSorted=PFCwCA1spikes(sortedFirstSpikeX,:);
                        %                         numswrsWspikes=size(CA1wspikes,1);
                        %                         pfcearly=smoothvect(mean(PFCwCA1spikesSorted(1:round(numswrsWspikes/2),:)),g2);
                        %                         pfclate=smoothvect(mean(PFCwCA1spikesSorted(round(numswrsWspikes/2)+1:end,:)),g2);
                        %
                        %                         pfcmatsmooth=gaussian(40,50);
                        %                         smoothedpfc=(filtfilt(pfcmatsmooth,1,PFCwCA1spikes')');
                        %                         smoothedpfc=zscore(smoothedpfc')';
                        
                        
                        % Exc and Pos Corr
                        if pfcSWRexcinh & ~isnan(r) & r>0 & size(CA1wspikes,1)>20 & sum(sum(PFCwCA1spikes))>30
                            SWRmod_PFCexcpos = [SWRmod_PFCexcpos;currSWRmod];
                            % Exc and Neg Corr
                        elseif pfcSWRexcinh & ~isnan(r) & r<0 & size(CA1wspikes,1)>20 & sum(sum(PFCwCA1spikes))>30
                            SWRmod_PFCexcneg = [SWRmod_PFCexcneg;currSWRmod];
                            % Inh and Pos Corr
                        elseif ~pfcSWRexcinh & ~isnan(r) & r>0 & size(CA1wspikes,1)>20 %& sum(sum(PFCwCA1spikes))>30
                            SWRmod_PFCinhpos = [SWRmod_PFCinhpos;currSWRmod];
                            % Inh and Neg Corr
                        elseif ~pfcSWRexcinh & ~isnan(r) & r<0 & size(CA1wspikes,1)>20 %& sum(sum(PFCwCA1spikes))>30
                            SWRmod_PFCinhneg = [SWRmod_PFCinhneg;currSWRmod];
                        end
                        
                        
                        
                    end % size PFChist and CA1hist is the same
                    
                    
                    
                    
                end % length(PFCamp) = length(CA1amp)
                
                
            end %strcmp  CA1-exc
            
        end
    end
end


% Plot Bar Graph for this
figure; hold on;
%barwitherr([std(excexcthetapeakcorr)/sqrt(length(excexcthetapeakcorr)) std(excinhthetapeakcorr)/sqrt(length(excinhthetapeakcorr)) std(inhinhthetapeakcorr)/sqrt(length(inhinhthetapeakcorr))],[mean(excexcthetapeakcorr) mean(excinhthetapeakcorr) mean(inhinhthetapeakcorr)])
%set(gca,'xticklabel',{'EXC/EXC','EXC/INH','INH/INH'})
bar(1, nanmean(SWRmod_PFCexcpos),'r');
bar(2, nanmean(SWRmod_PFCexcneg),'r');
bar(3, nanmean(SWRmod_PFCinhpos),'b');
bar(4, nanmean(SWRmod_PFCinhneg),'b');

legend({'Exc-Pos','Exc-Neg','Inh-Pos','Inh-Neg'})
errorbar2(1, nanmean(SWRmod_PFCexcpos), nanstderr(SWRmod_PFCexcpos), 0.3, 'r');
errorbar2(2, nanmean(SWRmod_PFCexcneg), nanstderr(SWRmod_PFCexcneg), 0.3, 'r');
errorbar2(3, nanmean(SWRmod_PFCinhpos), nanstderr(SWRmod_PFCinhpos), 0.3, 'b');
errorbar2(4, nanmean(SWRmod_PFCinhneg), nanstderr(SWRmod_PFCinhneg), 0.3, 'b');

title('SWR MOdln for categoires','FontSize',24,'FontWeight','normal');
ylabel('SWR modulation','FontSize',24,'FontWeight','normal')

set(gca,'XTick',[]);

figdir = '/data25/sjadhav/HPExpt/Figures/RipProp/';
figfile = [figdir,'SWRModln_Categories']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end


[p21,h21] = ranksum(SWRmod_PFCexcpos, SWRmod_PFCexcneg)  %
[p22,h22] = ranksum(SWRmod_PFCinhpos, SWRmod_PFCinhneg) %







