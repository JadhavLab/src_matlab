% based on ripTrigFiringOnsetAnalysis3
scrsz = get(0,'ScreenSize');
%savedirX = '/opt/data15/gideon/HP_ProcessedData/';
savedirX = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
plotIndividualCells=0;
areas1={'CA1','PFC'}

usedata = 1; % 1=alldata, 2 = only wtrack, 3 = only y-track, 4 = skip iCA1)
switch usedata
    case 1
        filestr = '';
    case 2
        filestr = '_Wtr_noiCA1';
    case 3
        filestr = '_Ytr';
    case 4
        filestr = '_NOiCA1';
end


figdir = '/data25/sjadhav/HPExpt/Figures/RevFig1/';
%figdir = '/data25/sjadhav/HPExpt/Figures/RipTrig2015/';

savefig1=0;
%figcell=0;
set(0,'defaultaxesfontsize',16);

histOns=[];
histOnsW=[];
allxcorrLags={};
allallripmodhists={};
allallripmodhistsW={};
allallripmodhistssig={};
allallripmodhistsWsig={};
allanimdayvecRip={};
allanimdayvecRipW={};
allallripmodhistssigInd={};
allallripmodhiststype={};

allallsiginds={};

% how to smooth psths
%b=fir1(81,0.06);
b=gaussian(20,61);


% % Get distribution by animal: FS and CA2  removed. Below, you will get significantly ripmod. Here, get total
% % PFC
% load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6']);
% PFCanimidx  = []; PFCanimidxsig  = []; PFCanimidxnosig  = [];
% for i=1:length(allripplemod)
%     if strcmp(allripplemod(i).FStag,'n')
%         PFCanimidx = [PFCanimidx;allripplemod_idx(i,1)];
%     end
%     if allripplemod(i).rasterShufP2<0.05 && strcmp(allripplemod(i).FStag,'n')
%         PFCanimidxsig = [PFCanimidxsig;allripplemod_idx(i,1)];
%     end
%     if allripplemod(i).rasterShufP2>=0.05 && strcmp(allripplemod(i).FStag,'n')
%         PFCanimidxnosig = [PFCanimidxnosig;allripplemod_idx(i,1)];
%     end
% end
% ncellsPFC_anim=[]; ncellsPFCsig_anim=[]; ncellsPFCnsig_anim=[];
% for i=1:6
%     ncellsPFC_anim(i) = length(find(PFCanimidx==i))
% end
% sum(ncellsPFC_anim), sum(ncellsPFC_anim(1:3)), sum(ncellsPFC_anim(4:6))
% for i=1:6
%     ncellsPFCsig_anim(i) = length(find(PFCanimidxsig==i))
% end
% sum(ncellsPFCsig_anim), sum(ncellsPFCsig_anim(1:3)), sum(ncellsPFCsig_anim(4:6))
% for i=1:6
%     ncellsPFCnosig_anim(i) = length(find(PFCanimidxnosig==i))
% end
% sum(ncellsPFCnosig_anim), sum(ncellsPFCnosig_anim(1:3)), sum(ncellsPFCnosig_anim(4:6))
%
%
% % CA1
% load([savedirX 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6_CA2tag']);
% CA1animidx=[]; CA1animidxsig  = []; CA1animidxnosig  = [];
% cntFS=0;
% for i=1:length(allripplemod)
%     if strcmp(allripplemod(i).FStag,'y')
%         cntFS=cntFS+1;
%     end
% end
% cntCA2=0;
% for i=1:length(allripplemod)
%     if strcmp(allripplemod(i).CA2tag,'y')
%         cntCA2=cntCA2+1;
%     end
% end
%
% for i=1:length(allripplemod)
%     if strcmp(allripplemod(i).FStag,'n') && strcmp(allripplemod(i).CA2tag,'n')
%         CA1animidx = [CA1animidx;allripplemod_idx(i,1)];
%     end
%     if allripplemod(i).rasterShufP2<0.05 && strcmp(allripplemod(i).FStag,'n') && strcmp(allripplemod(i).CA2tag,'n')
%         CA1animidxsig = [CA1animidxsig;allripplemod_idx(i,1)];
%     end
%     if allripplemod(i).rasterShufP2>=0.05 && strcmp(allripplemod(i).FStag,'n') && strcmp(allripplemod(i).CA2tag,'n')
%         CA1animidxnosig = [CA1animidxnosig;allripplemod_idx(i,1)];
%     end
% end
% ncellsCA1_anim=[]; ncellsCA1sig_anim=[]; ncellsCA1nsig_anim=[];
% for i=1:6
%     ncellsCA1_anim(i) = length(find(CA1animidx==i))
% end
% sum(ncellsCA1_anim), sum(ncellsCA1_anim(1:3)), sum(ncellsCA1_anim(4:6))
% for i=1:6
%     ncellsCA1sig_anim(i) = length(find(CA1animidxsig==i))
% end
% sum(ncellsCA1sig_anim), sum(ncellsCA1sig_anim(1:3)), sum(ncellsCA1sig_anim(4:6))
% for i=1:6
%     ncellsCA1nosig_anim(i) = length(find(CA1animidxnosig==i))
% end
% sum(ncellsCA1nosig_anim), sum(ncellsCA1nosig_anim(1:3)), sum(ncellsCA1nosig_anim(4:6))




for areaInd=1:2
    area1=areas1{areaInd};
    switch area1
        
        case 'CA1'
            
            % Try CA2 file
            load([savedirX 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6_CA2tag'])
            %load([savedirX 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6'])
            allripplemod_idx=[];
            for w=1:length(allripplemod),allripplemod_idx=[allripplemod_idx;allripplemod(w).index];end
            
        case 'PFC'
            
            load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6']);
            %  load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6symmetricwindow']);
            
            allripplemod_idx=[];
            for w=1:length(allripplemod),allripplemod_idx=[allripplemod_idx;allripplemod(w).index];end
            
    end
    allinds=unique([allripplemod_idx],'rows');
    allripmodhists=[];
    allripmodhistssig=[];
    allripmodhistssigInd=[];
    allripmodonset3=[];
    allripmodhiststype=[];
    
    allsiginds=[];
    
    for i=1:length(allripplemod)
        %if ~isempty(allripplemod(i).ripmodonset3)
        %   allripmodonset3=[allripmodonset3 allripplemod(i).ripmodonset3];
        %        allripmodhists=[allripmodhists; zscore(mean(allripplemod(i).hist))];
        allripmodhists=[allripmodhists; zscore(filtfilt(b,1,mean(rast2mat(allripplemod(i).raster))))];
        
        
        %For CA2 tag file
        doflag=0;
        switch area1
            case 'CA1'
                
                % SJ: try plottinh all CA1, not just sig
                %if strcmp(allripplemod(i).CA2tag,'n')
                %    doflag=1;
                %end
                % to get rid of iCA1
                %if strcmp(allripplemod(i).CA2tag,'n') && strcmp(allripplemod(i).iCA1tag,'n')
                %    doflag=1;
                %end
                
                if allripplemod(i).rasterShufP2<0.05 && strcmp(allripplemod(i).CA2tag,'n')
                    doflag=1;
                end
                
            case 'PFC'
                if allripplemod(i).rasterShufP2<0.05
                    doflag=1;
                end
        end
        if doflag==1
            % If Not CA2 tag file, then use this:
            %if allripplemod(i).rasterShufP2<0.05
            
            %if allripplemod(i).rasterShufP2>=0.05   % for non-mod
            curranim = allripplemod(i).index(1);
            if strcmp(allripplemod(i).FStag,'n')
                switch usedata
                    case 1 % all data
                        %            allripmodhistssig=[allripmodhistssig; zscore(mean(allripplemod(i).hist))];
                        allripmodhistssig=[allripmodhistssig; zscore(filtfilt(b,1,mean(rast2mat(allripplemod(i).raster))))];
                        allripmodhistssigInd=[allripmodhistssigInd; allripplemod(i).index];
                        % 1 for swr-exc, 0 for swr-inh
                        allripmodhiststype=[allripmodhiststype strcmp(allripplemod(i).type, 'exc')];
                        allsiginds = [allsiginds; allripplemod(i).index];
                        
                    case 2 % only W track
                        %                         if curranim<=3
                        %                             allripmodhistssig=[allripmodhistssig; zscore(filtfilt(b,1,mean(rast2mat(allripplemod(i).raster))))];
                        %                             allripmodhistssigInd=[allripmodhistssigInd; allripplemod(i).index];
                        %                             % 1 for swr-exc, 0 for swr-inh
                        %                             allripmodhiststype=[allripmodhiststype strcmp(allripplemod(i).type, 'exc')];
                        %                             allsiginds = [allsiginds; allripplemod(i).index];
                        %                         end
                        
                        % For no iCA1
                        if curranim<=3
                            if areaInd==1 % if CA1 file
                                if strcmp(allripplemod(i).iCA1tag,'n')
                                    allripmodhistssig=[allripmodhistssig; zscore(filtfilt(b,1,mean(rast2mat(allripplemod(i).raster))))];
                                    allripmodhistssigInd=[allripmodhistssigInd; allripplemod(i).index];
                                    % 1 for swr-exc, 0 for swr-inh
                                    allripmodhiststype=[allripmodhiststype strcmp(allripplemod(i).type, 'exc')];
                                    allsiginds = [allsiginds; allripplemod(i).index];
                                end
                            else % Do usual if PFC file
                                allripmodhistssig=[allripmodhistssig; zscore(filtfilt(b,1,mean(rast2mat(allripplemod(i).raster))))];
                                allripmodhistssigInd=[allripmodhistssigInd; allripplemod(i).index];
                                % 1 for swr-exc, 0 for swr-inh
                                allripmodhiststype=[allripmodhiststype strcmp(allripplemod(i).type, 'exc')];
                                allsiginds = [allsiginds; allripplemod(i).index];
                            end
                        end
                        
                    case 3 % only Y track
                        if curranim>=4
                            allripmodhistssig=[allripmodhistssig; zscore(filtfilt(b,1,mean(rast2mat(allripplemod(i).raster))))];
                            allripmodhistssigInd=[allripmodhistssigInd; allripplemod(i).index];
                            % 1 for swr-exc, 0 for swr-inh
                            allripmodhiststype=[allripmodhiststype strcmp(allripplemod(i).type, 'exc')];
                            allsiginds = [allsiginds; allripplemod(i).index];
                        end
                        
                    case 4 % skip iCA1 if its the CA1 file,
                        if areaInd==1 % if CA1 file
                            if strcmp(allripplemod(i).iCA1tag,'n')
                                allripmodhistssig=[allripmodhistssig; zscore(filtfilt(b,1,mean(rast2mat(allripplemod(i).raster))))];
                                allripmodhistssigInd=[allripmodhistssigInd; allripplemod(i).index];
                                % 1 for swr-exc, 0 for swr-inh
                                allripmodhiststype=[allripmodhiststype strcmp(allripplemod(i).type, 'exc')];
                                allsiginds = [allsiginds; allripplemod(i).index];
                            end
                        else % Do usual if PFC file
                            allripmodhistssig=[allripmodhistssig; zscore(filtfilt(b,1,mean(rast2mat(allripplemod(i).raster))))];
                            allripmodhistssigInd=[allripmodhistssigInd; allripplemod(i).index];
                            % 1 for swr-exc, 0 for swr-inh
                            allripmodhiststype=[allripmodhiststype strcmp(allripplemod(i).type, 'exc')];
                            allsiginds = [allsiginds; allripplemod(i).index];
                        end
                end
            end
            %else
            %   allripmodonset3=[allripmodonset3 nan];
            %allripmodhists=[allripmodhists; nan(1,size(allripmodhists,2))];
            
            %    end;
        end
    end
    
    allallripmodhists{end+1}=allripmodhists;
    allallripmodhistssig{end+1}=allripmodhistssig;
    allallripmodhistssigInd{end+1}=allripmodhistssigInd;
    allallripmodhiststype{end+1}=allripmodhiststype;
    %allallsiginds{end+1} = allsiginds;
    
    cntcellsRip=length(allripplemod);
    
    %     if areaInd==2
    %     if plotIndividualCells
    %         for kk=1:length(allripplemod)
    %         figure('Position',[300 200 scrsz(3)/3 600])
    %
    %
    %             imagesc(rast2mat(allripplemod(kk).raster));colormap(gray)
    %
    %
    %         keyboard
    %         end
    %     end
    %  end
end
CA1psths=allallripmodhistssig{1};
CA1inds=allallripmodhistssigInd{1};
PFCpsths=allallripmodhistssig{2};
PFCinds=allallripmodhistssigInd{2};


% % Get distribution by animal: FS and CA2 have already been removed. These are only significantly ripmod. Infer ripunmod from them
% % PFC exc-inh, and small no of CA1 nh, already broken down. I have those nos. CA2
% CA1animidx = CA1inds(:,1); nanim = 1:6; %unique(CA1animidx); %1:6
% PFCanimidx = PFCinds(:,1);
% for i=1:max(nanim)
%     ncellsCA1_anim(i) = length(find(CA1animidx==i))
%     ncellsPFC_anim(i) = length(find(PFCanimidx==i))
% end
% sum(ncellsPFC_anim), sum(ncellsPFC_anim(1:3)), sum(ncellsPFC_anim(4:6))
% sum(ncellsCA1_anim), sum(ncellsCA1_anim(1:3)), sum(ncellsCA1_anim(4:6))

%% RATE OF SWR-MODULATED CELLS

CA1sleepSWRmodrate=size(CA1psths,1)./size(allallripmodhists{1},1)
PFCsleepSWRmodrate=size(PFCpsths,1)./size(allallripmodhists{2},1)


%% Try plotting non ripmod ca1 cells: have to set condition for rastershuf to >= 0.05 above
% xaxis=-499:500;
% CA1 non ripmod cells
% [tmp timingindCA1a]=max(CA1psths(:,251:750)');
% [A2 B2]=sort(timingindCA1a);
% figure;
% subplot(3,1,1)
% imagesc(xaxis,1:length(B2), CA1psths(B2,:));caxis([-3 3])
% xlabel('Time (ms)')
% ylabel('#Cell')
% title('CA1 SWR-nonmod cells')
% xlim([-400 400])
%
% PFC non ripmod cells
% [tmp timingindPFCa]=max(PFCpsths(:,251:750)');
% [A2 B2]=sort(timingindPFCa);
% figure;
% subplot(3,1,1)
% imagesc(xaxis,1:length(B2), PFCpsths(B2,:));caxis([-3 3])
% xlabel('Time (ms)')
% ylabel('#Cell')
% title('PFC SWR-nonmod cells')
% xlim([-400 400])

%% ripple-triggered spiking in all regions. Currently sorted by response amplitude, can
% also sort by days using:
% [sortedDays2 sortedDaysS2]=sort(allanimdayvecRip{2}(:,2),'descend');

% SJ - Comment Fig
% % sleep
% figure('Position',[900 100 scrsz(3)/5 scrsz(4)])
% xaxis=-500:10:500;
% subplot(2,1,1);
% [sorted1 sortedS1]=sort(mean(CA1psths(:,50:70),2),'descend');
% imagesc(xaxis,1:size(CA1psths,1),CA1psths(sortedS1,:));
% xlim(zoomWindow)
% title('Ripple-triggered PSTHs, all rip-mod cells, CA1');xlabel('Time (ms)');ylabel('Cells')
% subplot(2,1,2);
% [sorted2 sortedS2]=sort(mean(PFCpsths(:,50:70),2),'descend');
% imagesc(xaxis,1:size(PFCpsths,1),PFCpsths(sortedS2,:));
% xlim(zoomWindow)
% title('Ripple-triggered PSTHs, all rip-mod cells, PFC');xlabel('Time (ms)');ylabel('Cells')
% annotation('textbox', [0.5, 1, 0, 0], 'string', 'Sleep','fontsize',20)
%% unsorted
zoomWindow=[-450 450];
figure('Position',[900 100 scrsz(3)/5 scrsz(4)])
xaxis=-499:500;
subplot(2,1,1);
imagesc(xaxis,1:size(CA1psths,1),CA1psths);
xlim(zoomWindow)
title('Ripple-triggered PSTHs, all rip-mod cells, CA1');xlabel('Time (ms)');ylabel('Cells')
caxis([-5 5])
subplot(2,1,2);
imagesc(xaxis,1:size(PFCpsths,1),PFCpsths);
caxis([-4 4])
xlim(zoomWindow)
title('Ripple-triggered PSTHs, all rip-mod cells, PFC');xlabel('Time (ms)');ylabel('Cells')
%annotation('textbox', [0.5, 1, 0, 0], 'string', 'Sleep','fontsize',20)
annotation('textbox', [0.5, 1, 0, 0], 'string', 'Run','fontsize',20)

figfile = [figdir,'RipTrig_PoplnZscore_unsorted_CA1andPFC',filestr]
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end


%%  exc/inh

%excinh=sign(mean(PFCpsths(:,500:650),2)-mean(PFCpsths(:,250:500),2));
excinhPFC=allallripmodhiststype{2};
%excinhPFCinds=allallsiginds{2};

excPFCpsths=PFCpsths(excinhPFC>0,:);
%excPFCinds = excinhPFCinds(excinhPFC>0,:);

%removing psths with no clear peak
% noclearpeak=[11 25 36];
% excPFCpsths=excPFCpsths(setdiff(1:size(excPFCpsths,1),noclearpeak),:);

inhPFCpsths=PFCpsths(excinhPFC==0,:);
%inhPFCinds = excinhPFCinds(excinhPFC==0,:);

%excinh1=sign(mean(CA1psths(:,50:65),2)-mean(CA1psths(:,25:50),2));
% FOR DEMETRIS: INDICES OF SIG SWR-EXC/INH
PFCindsExc=PFCinds(excinhPFC>0,:);
PFCindsInh=PFCinds(excinhPFC==0,:);

% SJ - Comment Fig
% figure('Position',[900 100 scrsz(3)/5 scrsz(4)]);
%
% subplot(3,1,1)
% imagesc(xaxis,1:size(CA1psths,1),CA1psths)
% caxis([-4 4]);xlim([-400 400])
% title('CA1');xlabel('Time (ms)')
%
%
%
% subplot(3,1,2)
% imagesc(xaxis,1:size(excPFCpsths,1),excPFCpsths)
% title('sig rip-exc');xlabel('Time (ms)')
%
% caxis([-4 4]);xlim([-400 400])
% subplot(3,1,3)
% imagesc(xaxis,1:size(inhPFCpsths,1),inhPFCpsths)
% title('sig rip-inh');xlabel('Time (ms)')
%
% caxis([-4 4]);xlim([-400 400])
%%
% figure
% hold on
% shadedErrorBar(xaxis,mean(CA1psths,1),std(CA1psths)./sqrt(size(CA1psths,1)),'-k',1);
% shadedErrorBar(xaxis,mean(PFCpsths,1),std(PFCpsths)./sqrt(size(PFCpsths,1)),'-r',1);

figure
hold on
shadedErrorBar(xaxis,mean(CA1psths,1),std(CA1psths)./sqrt(size(CA1psths,1)),'-k',1);
%shadedErrorBar(xaxis,mean(PFCpsths,1),std(PFCpsths)./sqrt(size(PFCpsths,1)),'-r',1);
shadedErrorBar(xaxis,mean(excPFCpsths,1),std(excPFCpsths)./sqrt(size(excPFCpsths,1)),'-r',0);
shadedErrorBar(xaxis,mean(inhPFCpsths,1),std(inhPFCpsths)./sqrt(size(inhPFCpsths,1)),'-b',0);
xlim([-400 400])
ylabel('Mean z-scored psth')
xlabel('Time (ms)')
%legend('CA1','PFCexc','PFCinh');


% Use jbfill to avoid shaded area errors:
figure; hold on;
plot(xaxis,mean(CA1psths,1),'k-');
jbfill(xaxis,mean(CA1psths,1)+(std(CA1psths)./sqrt(size(CA1psths,1))),...
    mean(CA1psths,1)-(std(CA1psths)./sqrt(size(CA1psths,1))),'k','k',1,1);
plot(xaxis,mean(excPFCpsths,1),'r-');
jbfill(xaxis,mean(excPFCpsths,1)+(std(excPFCpsths)./sqrt(size(excPFCpsths,1))),...
    mean(excPFCpsths,1)-(std(excPFCpsths)./sqrt(size(excPFCpsths,1))),'r','r',1,1);
plot(xaxis,mean(inhPFCpsths,1),'b-');
jbfill(xaxis,mean(inhPFCpsths,1)+(std(inhPFCpsths)./sqrt(size(inhPFCpsths,1))),...
    mean(inhPFCpsths,1)-(std(inhPFCpsths)./sqrt(size(inhPFCpsths,1))),'b','b',1,1);
xlim([-400 400])
ylabel('Mean z-scored psth')
xlabel('Time (ms)')


figfile = [figdir,'RipTrig_MeanZscorePSTHN2',filestr]
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end


forFigIndsexc=[1 2 17 2; 2 2 12 1];
forFigIndsinh=[2 1 9 1; 4 9 21 2; ];
forFigIndsneu=[2 3 10 1];

if usedata ==1
    exc1=find(ismember(PFCindsExc,forFigIndsexc(1,:),'rows')); % OR, exc1=find(ismember(excPFCinds,forFigInds(1,:),'rows'));
    exc2=find(ismember(PFCindsExc,forFigIndsexc(2,:),'rows')); % OR, exc1=find(ismember(excPFCinds,forFigInds(1,:),'rows'));
    inh1=find(ismember(PFCindsInh,forFigIndsinh(1,:),'rows'));
    inh2=find(ismember(PFCindsInh,forFigIndsinh(2,:),'rows'));
end

%% another look at the SWR-modulated PFC cells, and CA1 cells

% CA1 all ripmod cells
[tmp timingindCA1a]=max(CA1psths(:,251:750)');
[A2 B2]=sort(timingindCA1a);
figure;
%subplot(3,1,1)
imagesc(xaxis,1:length(B2), CA1psths(B2,:));caxis([-3 3])
xlabel('Time (ms)')
ylabel('#Cell')
title('CA1 all SWR-mod cells')
xlim([-400 400])

figfile = [figdir,'CA1RipTrig_PoplnZscore_X8',filestr]
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end


% With CA1 (also looking only at SWR-excited)
excinhCA1=allallripmodhiststype{1};
excCA1psths=CA1psths(excinhCA1>0,:);
inhCA1psths=CA1psths(excinhCA1==0,:);
[tmp timingindCA1]=max(excCA1psths(:,251:750)');
[A2 B2]=sort(timingindCA1);
figure;
subplot(3,1,1)
imagesc(xaxis,1:length(B2),excCA1psths(B2,:));caxis([-3 3])
xlabel('Time (ms)')
ylabel('#Cell')
title('CA1 SWR-excited cells')
xlim([-400 400])
% another look at the SWR-excited PFC cells
[tmp timingind]=max(excPFCpsths(:,251:750)');
[A B]=sort(timingind);
subplot(3,1,2);imagesc(xaxis,1:length(B),excPFCpsths(B,:));caxis([-3 3])
xlabel('Time (ms)')
ylabel('#Cell')
title('PFC SWR-excited cells')

if usedata==1
    % find indices after sorting
    exc1s = find(B==exc1),
    exc2s = find(B==exc2),
end

% another look at the SWR-inhibited PFC cells
[tmp timingind2]=min(inhPFCpsths(:,251:750)');
[A2 B2]=sort(timingind2);
xlim([-400 400])
subplot(3,1,3);imagesc(xaxis,1:length(B2),inhPFCpsths(B2,:));caxis([-3 3])
xlim([-400 400])
xlabel('Time (ms)')
ylabel('#Cell')
title('PFC SWR-inhibited cells')

if usedata==1
    % find indices after sorting
    inh1s = find(B2==inh1),
    inh2s = find(B2==inh2),
end

figfile = [figdir,'RipTrig_PoplnZscore',filestr]
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end





%% unsorted 2
% SJ - Comment Fig

% figure;subplot(2,1,1);imagesc(xaxis,1:size(excPFCpsths,1),excPFCpsths);caxis([-3 3])
% xlabel('Time (ms)')
% ylabel('#Cell')
% title('PFC SWR-excited cells')
% subplot(2,1,2);imagesc(xaxis,1:size(inhPFCpsths,1),inhPFCpsths);caxis([-3 3])
% xlabel('Time (ms)')
% ylabel('#Cell')
% title('PFC SWR-inhibited cells')
% % With CA1 (also looking only at SWR-excited)
% excinhCA1=allallripmodhiststype{1};
%
% excCA1psths=CA1psths(excinhCA1>0,:);
% inhCA1psths=CA1psths(excinhCA1==0,:);
%
%
% [tmp timingindCA1]=max(excCA1psths(:,251:750)');
% [A2 B2]=sort(timingindCA1);
% figure;
% subplot(2,1,1)
% imagesc(xaxis,1:length(B2),excCA1psths(B2,:));caxis([-3 3])
% xlabel('Time (ms)')
% ylabel('#Cell')
% title('CA1 SWR-excited cells')
% xlim([-400 400])
%
% subplot(2,1,2)
% imagesc(xaxis,1:length(B),excPFCpsths(B,:));caxis([-3 3])
% xlabel('Time (ms)')
% ylabel('#Cell')
% xlim([-400 400])

%% peak timing
bins1=-500:40:500;
[pfc1 t]=hist(timingind-250,bins1);
[pfc2 t]=hist(timingind2-250,bins1);

[CA11 t]=hist(timingindCA1-250,bins1);
figure
plot(bins1,CA11/sum(CA11),'k','linewidth',2)
hold on;
plot(bins1,pfc1/sum(pfc1),'r','linewidth',2);
plot(bins1,pfc2/sum(pfc2),'linewidth',2)
ylabel('fraction of units')
xlabel('Peak time relative to SWR onset')
legend('CA1','PFC swr-exc','PFC swr-inh')

figfile = [figdir,'RipTrig_PeakTime',filestr]
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end

mean(timingind-250), sem(timingind-250),
mean(timingind2-250), sem(timingind2-250),
mean(timingindCA1-250), sem(timingindCA1-250),

p1 = ranksum(timingindCA1,timingind) %p=5X10-12
p2 = ranksum(timingindCA1,timingind2) %p=5.1X10-7
p3 = ranksum(timingind,timingind2) %p=0.138



%% timing of start of rise to peak
risestartsPFCexc=[];
risestartsPFCinh=[];
risestartsCA1=[];

for i=1:size(excPFCpsths,1)
    [a maxind]=max(excPFCpsths(i,251:750));
    maxind=maxind+250;
    aa=excPFCpsths(i,1:maxind);
    risestart=find(aa<mean(excPFCpsths(i,:))+std(excPFCpsths(i,:)),1,'last');
    risestartsPFCexc=[risestartsPFCexc risestart];
end
for i=1:size(inhPFCpsths,1)
    [a maxind]=min(inhPFCpsths(i,251:750));
    maxind=maxind+250;
    aa=inhPFCpsths(i,1:maxind);
    risestart=find(aa>mean(inhPFCpsths(i,:))-std(inhPFCpsths(i,:)),1,'last');
    risestartsPFCinh=[risestartsPFCinh risestart];
end
for i=1:size(excCA1psths,1)
    [a maxind]=max(excCA1psths(i,251:750));
    maxind=maxind+250;
    aa=excCA1psths(i,1:maxind);
    risestart=find(aa<mean(excCA1psths(i,:))+std(excCA1psths(i,:)),1,'last');
    risestartsCA1=[risestartsCA1 risestart];
end

%figure;plot(risestarts);hold on;plot(risestartsCA1,'r')
[risePFCexc t]=hist(risestartsPFCexc-500,bins1);
[risePFCinh t]=hist(risestartsPFCinh-500,bins1);
[riseCA1 t]=hist(risestartsCA1-500,bins1);

figure
plot(bins1,riseCA1/sum(riseCA1),'k','linewidth',2)
hold on;
plot(bins1,risePFCexc/sum(risePFCexc),'r','linewidth',2);
plot(bins1,risePFCinh/sum(risePFCinh),'linewidth',2)
ylabel('fraction of units')
xlabel('Rise time relative to SWR onset')
legend('CA1','PFC swr-exc','PFC swr-inh')

figfile = [figdir,'RipTrig_RiseTime',filestr]
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end

mean(risestartsPFCexc-500), sem(risestartsPFCexc-500),
mean(risestartsPFCinh-500), sem(risestartsPFCinh-500),
mean(risestartsCA1-500), sem(risestartsCA1-500),

p1 = ranksum(risestartsCA1,risestartsPFCexc) %p=
p2 = ranksum(risestartsCA1,risestartsPFCinh) %p=
p3 = ranksum(risestartsPFCexc,risestartsPFCinh) %p=


%% modulation duration
risedurPFCexc=[];
risedurPFCinh=[];
risedurCA1=[];

for i=1:size(excPFCpsths,1)
    [a maxind]=max(excPFCpsths(i,251:750));
    maxind=maxind+250;
    aa=excPFCpsths(i,1:maxind);
    risestart=find(aa<mean(excPFCpsths(i,:))+std(excPFCpsths(i,:)),1,'last');
    bb=excPFCpsths(i,maxind:end);
    riseend=maxind+find(bb<mean(excPFCpsths(i,:))+std(excPFCpsths(i,:)),1,'first');
    curdur=riseend-risestart;
    risedurPFCexc=[risedurPFCexc curdur];
end

for i=1:size(inhPFCpsths,1)
    [a maxind]=min(inhPFCpsths(i,251:750));
    maxind=maxind+250;
    aa=inhPFCpsths(i,1:maxind);
    risestart=find(aa>mean(inhPFCpsths(i,:))-std(inhPFCpsths(i,:)),1,'last');
    bb=inhPFCpsths(i,maxind:end);
    riseend=maxind+find(bb>mean(inhPFCpsths(i,:))-std(inhPFCpsths(i,:)),1,'first');
    curdur=riseend-risestart;
    risedurPFCinh=[risedurPFCinh curdur];
end

for i=1:size(excCA1psths,1)
    [a maxind]=max(excCA1psths(i,251:750));
    maxind=maxind+250;
    aa=excCA1psths(i,1:maxind);
    risestart=find(aa<mean(excCA1psths(i,:))+std(excCA1psths(i,:)),1,'last');
    bb=excCA1psths(i,maxind:end);
    riseend=maxind+find(bb<mean(excCA1psths(i,:))+std(excCA1psths(i,:)),1,'first');
    curdur=riseend-risestart;
    risedurCA1=[risedurCA1 curdur];
end

bins2=0:10:250;

%figure;plot(risestarts);hold on;plot(risestartsCA1,'r')
[risePFCexc t]=hist(risedurPFCexc,bins2)
[risePFCinh t]=hist(risedurPFCinh,bins2)
[riseCA1 t]=hist(risedurCA1,bins2)

figure
plot(bins2,riseCA1/sum(riseCA1),'k','linewidth',2)
hold on;
plot(bins2,risePFCexc/sum(risePFCexc),'r','linewidth',2);
plot(bins2,risePFCinh/sum(risePFCinh),'linewidth',2)
ylabel('fraction of units')
xlabel('SWR modulation duration (ms)')
legend('CA1','PFC swr-exc','PFC swr-inh')

figfile = [figdir,'RipTrig_ModlnDurn',filestr]
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end



mean(risedurPFCexc-500), sem(risedurPFCexc-500),
mean(risedurPFCinh-500), sem(risedurPFCinh-500),
mean(risedurCA1-500), sem(risedurCA1-500),

p1 = ranksum(risedurCA1,risedurPFCexc) %p=
p2 = ranksum(risedurCA1,risedurPFCinh) %p=
p3 = ranksum(risedurPFCexc,risedurPFCinh) %p=
