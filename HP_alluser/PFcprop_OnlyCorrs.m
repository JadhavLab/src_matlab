% From PFcprop_corrs: only do the pairwaire correlations

% % ------------
% % PLOTTING, ETC
% % PFC Properties - from Demetris' code: DFS_DRsj_spatialvsripcorr.m
% % ------------


plotSWRcorr=1; % Plot swrcorr vs spatialcorr
savefig1=0;

%figdir = '/data25/sjadhav/HPExpt/Figures/2015_ReplayandPFCprop/SpatialCorrAndProperties';
figdir = '/data25/sjadhav/HPExpt/Figures/SpatialCorr/2015/';
% % ----FIG PROP --------------
forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1
figdir = '/data25/sjadhav/HPExpt/Figures/SpatialCorr/2015/'; summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
if forppr==1
    set(0,'defaultaxesfontsize',20);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end
% % ---------------------

val=0; area='PFC_SWRmod_noFS'; % matpairs original with FS removed, from DFS_DRsj_spatialvsripcorr_FSemoved.m
%val=6; area='Wtr_noiCA1';

%val=1; area='PFC_SWRmod'; % matpairs original, from DFS_DRsj_spatialvsripcorr.m
%val=2; area = 'PFC_SWRmod_nospeed'; % matpairs_nospeed_X6, from DFS_DRsj_spatialvsripcorr_PlotFields.m
%val=3; area = 'PFC_onlytheta'; % matpairs_PFConlytheta_X6, from DFS_DRsj_spatialvsripcorr_PFCcategories.m
%val=4; area = 'PFC_SWRandTheta'; % matpairs_PFC_SWRandTheta_X6, from DFS_DRsj_spatialvsripcorr_PFCcategories.m
%val=5; area = 'PFC_onlySWR';  %matpairs_PFC_onlySWR_X6, from DFS_DRsj_spatialvsripcorr_PFCcategories.m 


if plotSWRcorr
    
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh Indices
    
    switch val
        case 0
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_noFS_X8.mat'; % Has SWRcorr and SpatCorr
        case 1
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs.mat'; % Has SWRcorr and SpatCorr
        case 2
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_nospeed_X6.mat'; % Has SWRcorr and SpatCorr: Spat Corr with no speed criterion
        case 3
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_PFConlytheta_X6.mat';
        case 4
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_PFC_SWRandTheta_X6.mat';
        case 5
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_PFConlySWR_X6.mat';
        case 6
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_Wtr_noiCA1_X8.mat';
    end
            
            % [An day CA1tet CA1cell PFCtet PFCcell SWRcorrPval SWRcorrRval SpatCorrRval]
    
    % 1.) Entire population: find sig SWR correlated pairs, and then pos or neg correlated
    % ------------------------------------------------------------------------------------
    
    allsigrippairs = matpairs(matpairs(:,7)<0.05,9);
    allnonsigrippairs = matpairs(matpairs(:,7)>0.05,9);
    mean(abs(allsigrippairs)), length(allsigrippairs)
    mean(abs(allnonsigrippairs)), length(allnonsigrippairs)
    
    possigrippairs = matpairs(matpairs(:,7)<0.05 & matpairs(:,8) > 0,9);
    negsigrippairs = matpairs(matpairs(:,7)<0.05 & matpairs(:,8) < 0,9);
    nonsigrippairs = matpairs(matpairs(:,7)>0.05, 9);
    
    % Get sig SWR pair idxs
    sigrippairs_idx = find(matpairs(:,7)<0.05);
    
    figure; hold on;
    bar([mean(negsigrippairs) mean(nonsigrippairs) mean(possigrippairs)]); hold on;
    legend
    errorbar2([1 2 3], [mean(negsigrippairs) mean(nonsigrippairs) mean(possigrippairs)],  [stderr(negsigrippairs) stderr(nonsigrippairs) stderr(possigrippairs)] , 0.3, 'k')
    title('Spatial Corrln for SWR-correlated pair categories','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2 3]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    
    figfile = [figdir,'SpatialvsSWRcorr_AllBar_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    %stats
    [pKW table statsKW] = kruskalwallis([negsigrippairs' nonsigrippairs' possigrippairs'], [ones(1, length(negsigrippairs)) ones(1,length(nonsigrippairs)).*2 ones(1,length(possigrippairs)).*3]);
    %[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'off', 'alpha', 0.01); % change the alpha values to determine significance range.
    %c
   
    
    % 2.) All Exc vs Inh vs Non-Modulated Neurons
    % -------------------------------------------
    % Get appropriate indices in matpairs. Can loop and use ismember, or just use intersect
    
    matpairs_PFC = matpairs(:,[1 2 5 6]);
    % [~,Exc_ind,ib_exc] = intersect(matpairs_PFC,PFCindsExc,'rows');
    % [~,Inh_ind,ib_inh] = intersect(matpairs_PFC,PFCindsInh,'rows');
    % Mod_ind = [Exc_ind;Inh_ind];
    % Neu_ind=[1:length(matpairs_PFC)];
    % Neu_ind(Mod_ind)=[];
    
    cntexccells=0; cntexcpairs=0; Excind_inpairs=[];
    for i=1:size(PFCindsExc,1)
        ind=[]; curridx=[];
        cntexccells = cntexccells+1;
        curridx = PFCindsExc(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntexcpairs = cntexcpairs+length(ind);
        Excind_inpairs=[Excind_inpairs;ind'];
    end
    cntinhcells=0; cntinhpairs=0; Inhind_inpairs=[];
    for i=1:size(PFCindsInh,1)
        ind=[]; curridx=[];
        cntinhcells = cntinhcells+1;
        curridx = PFCindsInh(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntinhpairs = cntinhpairs+length(ind);
        Inhind_inpairs=[Inhind_inpairs;ind'];
    end
    
    Modind_inpairs = [Excind_inpairs;Inhind_inpairs];
    Neuind_inpairs = [1:length(matpairs_PFC)];
    Neuind_inpairs(Modind_inpairs) = [];
    
    excrippairs = matpairs(Excind_inpairs,9);
    inhrippairs = matpairs(Inhind_inpairs,9);
    neurippairs = matpairs(Neuind_inpairs, 9);
    
    figure; hold on;
    bar([mean(inhrippairs) mean(excrippairs) mean(neurippairs)]); hold on;
    legend
    errorbar2([1 2 3], [mean(inhrippairs) mean(excrippairs) mean(neurippairs)],  [stderr(inhrippairs) stderr(excrippairs) stderr(neurippairs)] , 0.3, 'k')
    title('Spatial Corrln for Exc vs Inh AllSWRCorr pairs','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2 3]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    
    figfile = [figdir,'SpatialvsSWRcorr_CompExcInhNeuBar_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    %stats
    [pKW table statsKW] = kruskalwallis([inhrippairs' excrippairs' neurippairs'], [ones(1, length(inhrippairs)) ones(1,length(excrippairs)).*2 ones(1,length(neurippairs)).*3]);
    pKW
    %[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.01); % change the alpha values to determine significance range.
    %c
    
%     %3.) Excited vs Inh Significant SWR corr neurons
    matpairs_Exc = matpairs(Excind_inpairs,:);
    matpairs_Inh = matpairs(Inhind_inpairs,:);
    matpairs_Neu = matpairs(Neuind_inpairs,:);
    
    excsigrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05,9);
    inhsigrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 ,9);
    neusigrippairs = matpairs_Neu(matpairs_Neu(:,7)<0.05, 9);
% %     
%     figure; hold on;
%     bar([mean(inhsigrippairs) mean(excsigrippairs)  mean(neusigrippairs)]); hold on;
%     legend
%     errorbar2([1 2 3], [mean(inhsigrippairs) mean(excsigrippairs) mean(neusigrippairs)],  [stderr(inhsigrippairs) stderr(excsigrippairs) stderr(neusigrippairs)] , 0.3, 'k')
%     
%     title('Spatial Corrln for Inh vs Exc Sig SWRCorr pairs','FontSize',24,'FontWeight','normal');
%     set(gca,'XTick',[1 2]);
%     ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
%     %stats
%     [pKW table statsKW] = kruskalwallis([inhsigrippairs' excsigrippairs' neusigrippairs'], [ones(1, length(inhsigrippairs)) ones(1,length(excsigrippairs)).*2 ones(1,length(neusigrippairs)).*3]);
%     pKW
%     
    
    
    %*) SCATTER: ALL together
    figure; hold on;
    scatter(matpairs(:,8), matpairs(:,9),'.k'); hold on;
    scatter(matpairs(sigrippairs_idx,8), matpairs(sigrippairs_idx,9),'.g'); hold on;
    
    [b,bint,r,rint,stats] = regress(matpairs(:,9), [ones(size(matpairs(:,8))) matpairs(:,8)]);
    plot(min(matpairs(:,8)):.1:max(matpairs(:,8)), b(1)+b(2)*[min(matpairs(:,8)):.1:max(matpairs(:,8))])
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('All in Category: r^2(%0.3f) p(%0.3f)', stats(1), stats(3))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    figfile = [figdir,'SpatialvsSWRcorr_AllScatter_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
%     %4.) Excited vs Inh Significant +ve or -ve SWR corr neurons
%     
    excsigposrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05 & matpairs_Exc(:,8) > 0,9);
    excsignegrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05 & matpairs_Exc(:,8) < 0,9);
    excnonsigrippairs = matpairs_Exc(matpairs_Exc(:,7)>0.05,9);
    inhsigposrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 & matpairs_Inh(:,8) > 0,9);
    inhsignegrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 & matpairs_Inh(:,8) < 0,9);
    inhnonsigrippairs = matpairs_Inh(matpairs_Inh(:,7)>0.05,9);
    %Neusigrippairs = matpairs_Neu(matpairs_Neu(:,7)<0.05, 9);
    
    % Get indices of sig SWR pairs for exc and inh separately
    excsigrippairs_idx = find(matpairs_Exc(:,7)<0.05);
    inhsigrippairs_idx = find(matpairs_Inh(:,7)<0.05);
    
%     
%     %5.) POS
%     figure; hold on;
%     bar([mean(inhsigposrippairs) mean(excsigposrippairs)]); hold on;
%     legend
%     errorbar2([1 2], [mean(inhsigposrippairs) mean(excsigposrippairs)],  [stderr(inhsigposrippairs) stderr(excsigposrippairs)] , 0.3, 'k')
%     title('Spatial Corrln for Inh vs Exc Sig POS SWRCorr pairs','FontSize',24,'FontWeight','normal');
%     set(gca,'XTick',[1 2]);
%     ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
%     %stats
%     [pKW table statsKW] = kruskalwallis([inhsigposrippairs' excsigposrippairs'], [ones(1, length(inhsigposrippairs)) ones(1,length(excsigposrippairs)).*3]);
%     pKW
%     
%     %6.) NEG
%     figure; hold on;
%     bar([mean(inhsignegrippairs) mean(excsignegrippairs)]); hold on;
%     legend
%     errorbar2([1 2], [mean(inhsignegrippairs) mean(excsignegrippairs)],  [stderr(inhsignegrippairs) stderr(excsignegrippairs)] , 0.3, 'k')
%     title('Spatial Corrln for Inh vs Exc Sig NEG SWRCorr pairs','FontSize',24,'FontWeight','normal');
%     set(gca,'XTick',[1 2]);
%     ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
%     %stats
%     [pKW table statsKW] = kruskalwallis([inhsignegrippairs' excsignegrippairs'], [ones(1, length(inhsignegrippairs)) ones(1,length(excsignegrippairs)).*3]);
%     pKW
%     
    
    %7.) POS and NEG
    figure; hold on;
    bar([mean(inhsignegrippairs) mean(inhnonsigrippairs) mean(inhsigposrippairs) mean(excsignegrippairs) mean(excnonsigrippairs) mean(excsigposrippairs)]); hold on;
    legend
    errorbar2([1 2 3  4 5 6], [mean(inhsignegrippairs)  mean(inhnonsigrippairs) mean(inhsigposrippairs) mean(excsignegrippairs) mean(excnonsigrippairs) mean(excsigposrippairs)],...
        [stderr(inhsignegrippairs)  stderr(inhnonsigrippairs) stderr(inhsigposrippairs) stderr(excsignegrippairs) stderr(excnonsigrippairs) stderr(excsigposrippairs)] , 0.3, 'k')
    title('Spatial Corrln Separated by Pairs with Inh vs Exc PFC neurons','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[2 5],'XTickLabel',{'Inh';'Exc'});
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    
    figfile = [figdir,'SpatialvsSWRcorr_ExcInhBar_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    %stats
    [pKW table statsKW] = kruskalwallis([inhsignegrippairs' inhnonsigrippairs' inhsigposrippairs' excsignegrippairs' excnonsigrippairs' excsigposrippairs'], [ones(1, length(inhsignegrippairs)) ones(1, length(inhnonsigrippairs))*2 ones(1, length(inhsigposrippairs)).*3 ...
        ones(1,length(excsignegrippairs)).*4 ones(1,length(excnonsigrippairs)).*5 ones(1,length(excsigposrippairs)).*6]);
    pKW
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.01); % change the alpha values to determine significance range.
    % c
    
    
    %*) Scatter: Exc vs Inh
    figure; hold on;
    subplot(1,2,1)
    scatter(matpairs_Exc(:,8), matpairs_Exc(:,9),'.k'); hold on;
    scatter(matpairs_Exc(excsigrippairs_idx,8), matpairs_Exc(excsigrippairs_idx,9),'.r'); hold on;
    
    [b,bint,r,rint,stats] = regress(matpairs_Exc(:,9), [ones(size(matpairs_Exc(:,8))) matpairs_Exc(:,8)]);
    plot(min(matpairs_Exc(:,8)):.1:max(matpairs_Exc(:,8)), b(1)+b(2)*[min(matpairs_Exc(:,8)):.1:max(matpairs_Exc(:,8))],'r')
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Exc-PFC: r^2(%0.3f) p(%0.3f)', stats(1), stats(3))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    subplot(1,2,2)
    scatter(matpairs_Inh(:,8), matpairs_Inh(:,9),'.k'); hold on;
    scatter(matpairs_Inh(inhsigrippairs_idx,8), matpairs_Inh(inhsigrippairs_idx,9),'.b'); hold on;

    [b,bint,r,rint,stats] = regress(matpairs_Inh(:,9), [ones(size(matpairs_Inh(:,8))) matpairs_Inh(:,8)]);
    plot(min(matpairs_Inh(:,8)):.1:max(matpairs_Inh(:,8)), b(1)+b(2)*[min(matpairs_Inh(:,8)):.1:max(matpairs_Inh(:,8))])
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Inh-PFC: r^2(%0.3f) p(%0.3f)', stats(1), stats(3))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    figfile = [figdir,'SpatialvsSWRcorr_ExcInhScatter_color_X8',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-dpdf', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
end

[rexc, pexc] = corrcoef(matpairs_Exc(:,8), matpairs_Exc(:,9))
[rinh, pinh] = corrcoef(matpairs_Inh(:,8), matpairs_Inh(:,9))

keyboard;


% How many neurons participated in the pairs:
allsigpair_PFC = matpairs(sigrippairs_idx,[1,5,6]);
PFC_cells = unique(allsigpair_PFC,'rows');
nPFC = length(PFC_cells)
allsigpair_CA1 = matpairs(sigrippairs_idx,[1,3,4]);
CA1_cells = unique(allsigpair_CA1,'rows');
nCA1 = length(CA1_cells)


excsigpair_PFC = matpairs_Exc(excsigrippairs_idx,[1,5,6]);
PFC_cellsexc = unique(excsigpair_PFC,'rows');
nPFCexc = length(PFC_cellsexc)
excsigpair_CA1 = matpairs_Exc(excsigrippairs_idx,[1,3,4]);
CA1_cellsexc = unique(excsigpair_CA1,'rows');
nCA1exc = length(CA1_cellsexc)

inhsigpair_PFC = matpairs_Inh(inhsigrippairs_idx,[1,5,6]);
PFC_cellsinh = unique(inhsigpair_PFC,'rows');
nPFCinh = length(PFC_cellsinh)
inhsigpair_CA1 = matpairs_Inh(inhsigrippairs_idx,[1,3,4]);
CA1_cellsinh = unique(inhsigpair_CA1,'rows');
nCA1inh = length(CA1_cellsinh)


% Shuffle test between excited and inhibited

xexc = matpairs_Exc(:,8); yexc =  matpairs_Exc(:,9);
xinh = matpairs_Inh(:,8); yinh =  matpairs_Inh(:,9);

% First, try a shuffle for excited and inhibited separately to assess their individual significance

% % Shuffling Excited
% % ----------------
disp('Exc')
allmodln = xexc; allripmodln = yexc;
[bexc,bintexc,rexc,rintexc,statsexc] = regress(allripmodln, [ones(size(allmodln)) allmodln]);

for n=1:1000
    rorder = randperm(length(allmodln));
    randmodln = allmodln(rorder);
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, allripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(allripmodln, [ones(size(randmodln)) randmodln]);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end

% Significance from shuffle
rsquare_exc = statsexc(1)
rsq_shuf99_exc = prctile(rsquare_shuffle,99),
%figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
[r2,p2] = corrcoef(allmodln,allripmodln);  
r_exc = r2(1,2),
r_shuf99_exc = prctile(r_shuffle,99),
pshuf_r_exc = length(find(r2(1,2)<r_shuffle))/n



% % Shuffling Inhibted
% % ------------------
disp('Inh')
allmodln = xinh; allripmodln = yinh;
[binh,bintinh,rinh,rintinh,statsinh] = regress(allripmodln, [ones(size(allmodln)) allmodln]);

for n=1:1000
    rorder = randperm(length(allmodln));
    randmodln = allmodln(rorder);
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, allripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(allripmodln, [ones(size(randmodln)) randmodln]);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end

% Significance from shuffle
rsquare_inh = statsinh(1)
rsq_shuf99_inh = prctile(rsquare_shuffle,99),
%figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
[r2,p2] = corrcoef(allmodln,allripmodln);  
r_inh = r2(1,2),
r_shuf99_inh = prctile(r_shuffle,99),
pshuf_r_inh = length(find(r2(1,2)<r_shuffle))/n





% Mix excited and inhibited, shuffle 1000 times, and draw samples equal to length(inh) for each shuffle
% Compare r_inh with r_shuf from this distribution/ or rsquare instead
% -----------------------------------------------------------------------------------------------------
x=[xexc;xinh];
y=[yexc;yinh];
ninh = length(yinh); nexc = length(yexc);

disp('Mix ExcInh: Whether Inhibited is more correlated than mixed population')
allmodln = x; allripmodln = y;
[b00,bint00,r00,rint00,stats00] = regress(allripmodln, [ones(size(allmodln)) allmodln]);
rsquare00 = stats00(1)

for n=1:1000
    rorder = randperm(nexc+ninh); % Randomly pick ninh points out of nexc+ninh distribution
    randmodln = allmodln(rorder(1:ninh)); randripmodln = allripmodln(rorder(1:ninh));
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, randripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(randripmodln, [ones(size(randmodln)) randmodln]);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end


rsquare_inh, prc99_rsqshuf = prctile(rsquare_shuffle,99), prc95_rsqshuf = prctile(rsquare_shuffle,95),   
r_inh, prc99_rshuf = prctile(r_shuffle,99),  prc95_rshuf = prctile(r_shuffle,95),

pinh_r_shuf = length(find(r_inh<=r_shuffle))./n    % p = 0.04 / for X7: p=0.016/ for X8: 0.007
pinh_rsq_shuf = length(find(rsquare_inh<=rsquare_shuffle))./n  % p=0.04



disp('Mix ExcInh: Whether Excited is less correlated than mixed population')

rshuffle=[]; rsquare_shuffle=[];
for n=1:1000
    rorder = randperm(nexc+ninh); % Randomly pick ninh points out of nexc+ninh distribution
    randmodln = allmodln(rorder(1:nexc)); randripmodln = allripmodln(rorder(1:nexc));
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, randripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(randripmodln, [ones(size(randmodln)) randmodln]);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end


rsquare_exc, prc99_rsqshuf = prctile(rsquare_shuffle,99), prc95_rsqshuf = prctile(rsquare_shuffle,95),
r_exc, prc99_rshuf = prctile(r_shuffle,99), prc95_rshuf = prctile(r_shuffle,95),

pexc_r_shuf = length(find(r_exc>=r_shuffle))./n    % p = 0.09 / 0.065 for X7/ for X8: 0.01
pexc_rsq_shuf = length(find(rsquare_exc>=rsquare_shuffle))./n  % p=0.09








