function out = DFAsj_get_rewpos_alignspiking(index, excludetimes, spikes, pos, linpos, trajinfo, varargin)

% get reward data from trajinfo (=posn), and align spikes to it.
% Need trajinfo for each day. This is because DIO may have a problem

% out = DFAsj_getripalignspiking(spike_index, excludeperiods, spikes, ripples, tetinfo, options)



thrsdist=10; % cm
thrsspeed=2;% cm/sec
excludetimes = [];
figopt=0;

% For ripple trigger
% ------------------
binsize = 100; % ms
pret=4050; postt=4050; %% Times to plot
push = 500; % either bwin(2) or postt-trim=500. For jittered trigger in background window
trim = 50;
smwin=100; %Smoothing Window - along y-axis for matrix. Carry over from ...getrip4
plotline1 = 100;

if isempty(binsize)
    binsize = 100;  %% ms
end
binsize_plot=binsize; %% If you put binsize_plot=1000, then units are Nspikes/binsize, not inst. firing rate in Hz
timeaxis = -pret:binsize:postt;

% nstd=1: gaussian of length 4. nstd = 2: gaussian of length 7, nstd=3: gaussian of length 10.
nstd = round(binsize*2/binsize); g1 = gaussian(nstd, 3*nstd+1);
% Smooth over binsize*3 ms (30ms for 10ms; 15ms for 5 ms)
%nstd = round(binsize*3/binsize); g1 = gaussian(nstd, 5*nstd+1);
%nstd = round(binsize*2/binsize); g1 = gaussian(nstd, 1*nstd+1);

rwin = [0 2000];
bwin = [-3000 -1000];
push = 500; % either bwin(2) or postt-trim=500. If doing random events. See ... getrip4


for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'thrsdist'
            thrsdist = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'thrsspeed'
            thrsspeed = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

day = index(1);
epoch = index(2);

% Get reward times
% ----------------

% Posn data
postime = pos{day}{epoch}.data(:,(1));
posn = pos{day}{epoch}.data(:,2:3);
posx = posn(:,1); posy = posn(:,2);
speed = abs(pos{day}{epoch}.data(:,5));

% Record trajtime for our particular epoch
trajinfo_de = trajinfo{day}{epoch};
if isempty(trajinfo_de)
    trajtime=[];
    %continue;
else
    trajbound = trajinfo{day}{epoch}.trajbound;
    rewarded = trajinfo{day}{epoch}.rewarded;
    trajtime = trajinfo{day}{epoch}.trajtime;
    wellstend = trajinfo{day}{epoch}.wellstend;
end

% Only proceed if reward triggers exist
allrewtime=[]; allnorewtime=[];
allrew_spks=[]; allnorew_spks=[];
allrew_spkshist=[]; allnorew_spkshist=[];
trialResps_norew=[]; trialResps_bck_norew=[];

if ~isempty(trajtime)
    wellpos = linpos{day}{epoch}.wellSegmentInfo.wellCoord;
    
    % Crawl over each trajectory and select times within
    % spatial and temporal_proximity to the endpoint
    for i = 1:size(trajtime,1)
        
        currtrajtime = trajtime(i,:);
        curr_endwell = wellstend(i,2);
        curr_rewarded = rewarded(i);
        curr_trajtype = trajbound; % 0=out, 1=in
        
        startidx = find(currtrajtime(1)==postime);                                             % mcz start of positions
        endidx = find(currtrajtime(2)==postime);                                               % mcz end of positions
        currpos = posn(startidx:endidx,:); % All pons on current outbound lap
        currtime = postime(startidx:endidx,:);
        currspeed = speed(startidx:endidx,:);
        
        currwellpos = wellpos(curr_endwell,:);
        % Get distance from endwell
        alldist = dist(currpos,repmat(currwellpos,length(currpos),1));
        % Get dist less than threshold
        valid_idx = find(alldist<=thrsdist);
        subset_pos = currpos(valid_idx,:);
        subset_speed = currspeed(valid_idx);
        subset_time = currtime(valid_idx);
        % FInd speeds < thrs in this window, and take the first time as reward-time
        reward_idx = min(find(subset_speed<thrsspeed));
        if ~isempty(reward_idx)
            curr_rewardtime = subset_time(reward_idx);
            rewardpos = subset_pos(reward_idx,:);
        else
            [curr_rewardtime,minidx] = min(subset_time);
            rewardpos = subset_pos(minidx,:);
        end
        
        if curr_rewarded
            allrewtime=[allrewtime; curr_rewardtime];
        else
            allnorewtime=[allnorewtime; curr_rewardtime];
        end
        
        %              if figopt==1
        %             figure(1); hold on; plot(posx, posy, 'Color', Clgy); axis square;
        %             set(gca, 'LooseInset', get(gca,'TightInset'));
        %             plot(currwellpos(1),currwellpos(2),'ks','MarkerSize',8,'MarkerFaceColor','k');
        %             circles(currwellpos(1),currwellpos(2),thrsdist,'edgecolor','k','facecolor','none');
        %             plot(currpos(:,1),currpos(:,2),'c-','Linewidth',2);
        %             plot(subset_pos(:,1),subset_pos(:,2),'g-','Linewidth',2);
        %             plot(rewardpos(1),rewardpos(2),'ro', 'MarkerSize',12, 'MarkerFaceColor','m')
        
    end
    
    
    % Align to rew and norew
    % -------------------------
    
    sind = index;
    if ~isempty(spikes{sind(1)}{sind(2)}{sind(3)}{sind(4)}.data)
        spikeu = spikes{sind(1)}{sind(2)}{sind(3)}{sind(4)}.data(:,1)*1000;  % in ms
    else
        spikeu = [];
    end
    totaltime = diff(spikes{sind(1)}{sind(2)}{sind(3)}{sind(4)}.timerange)./10000;
    cellfr = length(spikeu)./totaltime;
    
    cntallrew=0; cntallnorew=0;
    % All Rewarded
    for i=1:length(allrewtime)
        cntallrew=cntallrew+1;
        currtime = (allrewtime(i))*1000; % ms
        currspks = spikeu(find( (spikeu>=(currtime-pret)) & (spikeu<=(currtime+postt)) ));
        %nspk = nspk + length(currspks);
        currspks = currspks-(currtime); % Set to 0 at reward
        histspks = histc(currspks,[-pret:binsize:postt]);
        histspks = smoothvect(histspks, g1); % Smmoth histogram
        % Save spktimes
        allrew_spks{cntallrew}=currspks;
        % Save histogram
        allrew_spkshist(cntallrew,:)=histspks;
        % Get no of spikes in response window, and back window
        trialResps(cntallrew) =  length(find(currspks>=rwin(1) & currspks<=rwin(2)));
        trialResps_bck(cntallrew) =  length(find(currspks>=bwin(1) & currspks<=bwin(2)));
    end
    
    
    % All UnRewarded
    for i=1:length(allnorewtime)
        cntallnorew=cntallnorew+1;
        currtime = (allnorewtime(i))*1000;
        currspks = spikeu(find( (spikeu>=(currtime-pret)) & (spikeu<=(currtime+postt)) ));
        %currspks = currspks-(currtime-pret); % Set to 0 at -pret
        %histspks = histc(currspks,[0:binsize:pret+postt]); % Make histogram
        currspks = currspks-(currtime); % Set to 0 at reward
        histspks = histc(currspks,[-pret:binsize:postt]); % Make histogram
        histspks = smoothvect(histspks, g1); % Smmoth histogram
        % Save spktimes
        allnorew_spks{cntallnorew}=currspks;
        % Save histogram
        allnorew_spkshist(cntallnorew,:)=histspks;
        % Get no of spikes in response window, and back window
        trialResps_norew(cntallrew) =  length(find(currspks>=rwin(1) & currspks<=rwin(2)));
        trialResps_bck_norew(cntallrew) =  length(find(currspks>=bwin(1) & currspks<=bwin(2)));
    end
    
else   % ~isempty(trajtime). Redundant, but do it again
    allrewtime=[]; allnorewtime=[];
    allrew_spks=[]; allnorew_spks=[];
    allrew_spkshist=[]; allnorew_spkshist=[];
    trialResps=[]; trialResps_bck=[];
    trialResps_norew=[]; trialResps_bck_norew=[];
    
    
end % end trajtime


% Output
% ------
out.index = sind;
% Propoerties
out.cellfr = cellfr;

% Reward resp, etc
out.allrewtime = allrewtime;
out.allnorewtime = allnorewtime;
out.allrew_spks = allrew_spks;
out.allnorew_spks = allnorew_spks;
out.allrew_spkshist = allrew_spkshist;
out.allnorew_spkshist = allnorew_spkshist;
out.trialResps = trialResps;
out.trialResps_bck = trialResps_bck;
out.trialResps_norew = trialResps_norew;
out.trialResps_bck_norew = trialResps_bck_norew;
% Nspikes summed across trials in responsewindow
out.Nspikes_rew = sum(trialResps);
out.Nspikes_norew = sum(trialResps_norew);

out.pret = pret;
out.postt = postt;
out.binsize = binsize;
out.rwin = rwin;
out.bckwin = bwin;
%out.bins_resp  = bins_resp;
%out.bins_bck = bins_bck;
%out.timeaxis = timeaxis;






