
%Based on sj_addFStag_to_ripplemod_file_X6.m
% Add iCA1tag ti the CA1 ripplemod file

% Load the "ripplemod" file and the "cellinfo" file for PFC cells, and add
% a "FStag" field to the allripplemod structure identifying the FS cells

clear;
%savedir = 'mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';


% ---------------------------------------------------------------------
% ---------------------------------------------------------------------

% CA1 Ripplemod file
% --------------
ripplefile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6']; 
%load(ripplefile, 'allripplemod','allripplemod_idx'); % load allripplemod and allripplemod_idx. 
load(ripplefile); % Load the entire file since you have to eventually save it

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
% Cellinfo file
% --------------
cellfile = [savedir, 'HP_cellinfo_getiCA1_gather_5-10-2015']; 
load(cellfile,'iCA1idx');

% First add a FS tag field for allripplemod
% ------------------------------------------
for i = 1:size(allripplemod_idx,1)
    allripplemod(i).iCA1tag = 'n';
end
   

cnt_match=0; match_idx=[];
for i = 1:size(iCA1idx,1)    
    curridx = iCA1idx(i,:);    
    match = rowfind(curridx, allripplemod_idx);
    
    if match~=0
        cnt_match = cnt_match+1,
        match_idx(cnt_match,:) = curridx,
        allripplemod(match).iCA1tag = 'y';
    end
end


savedata = 1;

% Total 54 iCA1 cells in the allripplemod file, from the pool of 70 iCA1 total

% Can Update filename by putting a "FStagged" in front of it
% ------------------------------------------------------
savefile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6']; 
if savedata
    save(savefile);
end
    
    
    





