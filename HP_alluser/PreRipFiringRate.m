savedir = '/opt/data15/gideon/HP_ProcessedData/';
load([savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6'])
preRipFRsRipExc=[];
preRipFRsRipInh=[];
b=gaussian(6,10);
ripexchists=[];
ripinhhists=[];

for i=1:length(allripplemod)
    if allripplemod(i).rasterShufP2<0.05
        currast=rast2mat(allripplemod(i).raster);
        preRipFRinHz=1000*mean(mean(currast(:,1:400)));
%         figure;
%         subplot(3,1,1:2)
%         imagesc(1-ceil(filter2(ones(1,5)/5,currast)));colormap(gray)
%         subplot(3,1,3)
%         plot(filtfilt(b,1,mean(currast)));
        curmean=filtfilt(b,1,mean(currast));
        if strcmp(allripplemod(i).type,'exc')
        preRipFRsRipExc=[preRipFRsRipExc preRipFRinHz];
        ripexchists=[ripexchists;  curmean];
        %   title('EXC')

        elseif strcmp(allripplemod(i).type,'inh')
                    preRipFRsRipInh=[preRipFRsRipInh preRipFRinHz];
                  ripinhhists=[ripinhhists;  curmean];

             %              title('INH')

        end
    %    keyboard
        close all
    end
        
        
    
    
end
%%
figure;
subplot(2,1,1);imagesc(ripexchists*1000);caxis([0 20]);title(['Rip-exc, mean pre-rip FR= ' num2str(mean(preRipFRsRipExc))])
colorbar
xlabel('Time (ms)')
subplot(2,1,2);imagesc(ripinhhists*1000);caxis([0 20]);title(['Rip-inh, mean pre-rip FR= ' num2str(mean(preRipFRsRipInh))])
colorbar
xlabel('Time (ms)')