%savedir = '/opt/data15/gideon/HP_ProcessedData/';

savedir = '/data25/sjadhav/HPexpt/HP_ProcessedData/';
xaxis=-500:10:499;
load([savedir 'HP_PFCPFCThetacov_std3_speed4_ntet2_alldata_excexc_gather_X6_noFS']);
Sexcexc=SallZcrosscov_sm_runtheta_epcomb;

savedir = '/data25/sjadhav/HPexpt/HP_ProcessedData/';
load([savedir 'HP_PFCPFCThetacov_std3_speed4_ntet2_alldata_excinh_gather_X6_noFS']);
Sexcinh=SallZcrosscov_sm_runtheta_epcomb;

savedir = '/data25/sjadhav/HPexpt/HP_ProcessedData/';
load([savedir 'HP_PFCPFCThetacov_std3_speed4_ntet2_alldata_inhinh_gather_X6_noFS']);
Sinhinh=SallZcrosscov_sm_runtheta_epcomb;

savedir = '/data25/sjadhav/HPexpt/HP_ProcessedData/';
load([savedir 'HP_PFCPFCThetacov_std3_speed4_ntet2_alldata_unmodunmod_gather_X6_noFS']);
Sunmodunmod=SallZcrosscov_sm_runtheta_epcomb;

% Nos. 193, 197,198,199 are NaNs
a = find(isnan(Sunmodunmod(:,1)))
Sunmodunmod(a,:)=[];


set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
    
%%
figure('Position',[1200 400 600 1200])
subplot(4,1,1);
ccampexcexc=mean(Sexcexc(:,40:60),2);
[xx orderexcexc]=sort(ccampexcexc,'descend');
Sexcexc=Sexcexc(orderexcexc,:);
imagesc(xaxis,1:size(Sexcexc,1),Sexcexc)
caxis([-10 10]);colorbar
title('EXC/EXC')
xlabel('Time (ms)')
ylabel('Pairs')
xlim([-450 450])

subplot(4,1,2);
ccampexcinh=mean(Sexcinh(:,40:60),2);
[xx orderexcinh]=sort(ccampexcinh,'descend');
Sexcinh=Sexcinh(orderexcinh,:);
imagesc(xaxis,1:size(Sexcinh,1),Sexcinh)
caxis([-10 10]);colorbar
title('EXC/INH')
xlabel('Time (ms)')
ylabel('Pairs')
xlim([-450 450])

ccmaxinhinh=max((Sinhinh(:,40:60)'));
% this is the same cell twice: originally was no 35, now is no 34 in noFS. Very big ccmaxinhinh value
% Will have to update for newest file
%Sinhinh=Sinhinh([1:33,35:end],:); 
ainh = find(ccmaxinhinh>10),
Sinhinh(ainh,:)=[];

ccampinhinh=mean(Sinhinh(:,40:60),2);
[xx orderinhinh]=sort(ccampinhinh,'descend');
Sinhinh=Sinhinh(orderinhinh,:);
subplot(4,1,3);
imagesc(xaxis,1:size(Sinhinh,1),Sinhinh)
caxis([-10 10]);colorbar
title('INH/INH')
xlabel('Time (ms)')
ylabel('Pairs')
xlim([-450 450])

subplot(4,1,4);
ccmaxunmodunmod=max((Sunmodunmod(:,40:60)'));
% this is the same cell twice: no 55 and 305 in noFS. Very big ccmaxunmodunmod value
aunm = find(ccmaxunmodunmod>10),
Sunmodunmod(aunm,:)=[];

ccampunmodunmod=mean(Sunmodunmod(:,40:60),2);
[xx orderunmodunmod]=sort(ccampunmodunmod,'descend');
Sunmodunmod=Sunmodunmod(orderunmodunmod,:);
imagesc(xaxis,1:size(Sunmodunmod,1),Sunmodunmod)
caxis([-10 10]);colorbar
title('Unmod/Unmod')
xlabel('Time (ms)')
ylabel('Pairs')
xlim([-450 450])

%% all cross-covs
figure('Position',[1200 400 600 1200])
subplot(4,1,1);
plot(xaxis,Sexcexc,'r')
title('EXC/EXC')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
axis([-450 450 -6 10])

subplot(4,1,2);
plot(xaxis,Sexcinh,'k')
title('EXC/INH')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
axis([-450 450 -6 10])

subplot(4,1,3);
plot(xaxis,Sinhinh,'b')
title('INH/INH')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
axis([-450 450 -6 10])

subplot(4,1,4);
plot(xaxis,Sunmodunmod,'c')
title('Unmod/Unmod')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
axis([-450 450 -6 10])

%%

ccmaxinhinh=max((Sinhinh(:,40:60)'));
[inhinhxx orderinhinh2]=sort(ccmaxinhinh,'descend');
Sinhinh2=Sinhinh(orderinhinh2,:);

ccmaxexcinh=max((Sexcinh(:,40:60)'));
[excinhxx orderexcinh2]=sort(ccmaxexcinh,'descend');
Sexcinh2=Sexcinh(orderexcinh2,:);

ccmaxexcexc=max((Sexcexc(:,40:60)'));
[excexcxx orderexcexc2]=sort(ccmaxexcexc,'descend');
Sexcexc2=Sexcexc(orderexcexc2,:);

ccmaxunmodunmod=max((Sunmodunmod(:,40:60)'));
[unmodunmodxx orderunmodunmod2]=sort(ccmaxunmodunmod,'descend');
Sunmodunmod2=Sunmodunmod(orderunmodunmod2,:);

figure('Position',[1200 400 600 1200])
subplot(4,1,1);
plot(xaxis,Sexcexc(1,:),'r','linewidth',2)
title('max EXC/EXC')
xlabel('Time (ms)')
ylabel('Standardized cross-cov Eg')
axis([-450 450 -6 10])

subplot(4,1,2);
plot(xaxis,Sexcinh(1,:),'k','linewidth',2)
title('max EXC/INH')
xlabel('Time (ms)')
ylabel('Standardized cross-cov Eg')
axis([-450 450 -6 10])

subplot(4,1,3);
plot(xaxis,Sinhinh(1,:),'b','linewidth',2)
title('max INH/INH')
xlabel('Time (ms)')
ylabel('Standardized cross-cov Eg')
axis([-450 450 -6 10])

subplot(4,1,4);
plot(xaxis,Sunmodunmod(1,:),'r','linewidth',2)
title('max Unmod/Unmod')
xlabel('Time (ms)')
ylabel('Standardized cross-cov Eg')
axis([-450 450 -6 10])

% Plot one example from each illustrating values
% -----------------------------------------------
figure; hold on;
plot(xaxis,Sexcexc(3,:),'r','linewidth',2)
plot(xaxis,Sinhinh(3,:),'b','linewidth',2)
plot(xaxis,Sexcinh(25,:),'k','linewidth',2)  %43
plot(xaxis,Sunmodunmod(75,:),'c','linewidth',2) % 50, 100
xlabel('Time (ms)')
ylabel('Standardized cross-cov Eg')
axis([-500 500 -1.9 5])

figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/PFCPFC_Corr/';
figfile = [figdir,'PFCPFC_ThetaCovEgN']    
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);  
end

% Plot max example from each 
% -----------------------------------------------
figure; hold on;
plot(xaxis,Sexcexc(1,:),'r','linewidth',2)
plot(xaxis,Sinhinh(1,:),'b','linewidth',2)
plot(xaxis,Sexcinh(1,:),'k','linewidth',2)  %43
plot(xaxis,Sunmodunmod(1,:),'c','linewidth',2) % 50, 100
xlabel('Time (ms)')
ylabel('Standardized cross-cov Eg')
axis([-500 500 -4 9])

figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/PFCPFC_Corr/';
figfile = [figdir,'PFCPFC_ThetaCovMaxEgN']    
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);  
end


%%
figure('Position',[1200 400 600 800]); hold on;
shadedErrorBar(xaxis,mean(Sexcexc),std(Sexcexc)./sqrt(size(Sexcexc,1)),'-r',1);
hold on
shadedErrorBar(xaxis,mean(Sexcinh),std(Sexcinh)./sqrt(size(Sexcinh,1)),'-k',1);
shadedErrorBar(xaxis,mean(Sinhinh),std(Sinhinh)./sqrt(size(Sinhinh,1)),'-b',1);
shadedErrorBar(xaxis,mean(Sunmodunmod),std(Sunmodunmod)./sqrt(size(Sunmodunmod,1)),'-c',1);

legend({'EXC/EXC','EXC/INH','INH/INH','Unmod/Unmod'})
axis([-450 450 -1 2.2])
box off
plot(xaxis,mean(Sexcexc),'r','linewidth',2);
plot(xaxis,mean(Sexcinh),'k','linewidth',2);
plot(xaxis,mean(Sinhinh),'b','linewidth',2);
plot(xaxis,mean(Sunmodunmod),'c','linewidth',2);
axis([-450 450 -1 2])
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')


% Dont Shade for making final figure in Illustrator
% -----------------------------------------------
figure; hold on;
plot(xaxis,mean(Sexcexc),'r-','Linewidth',2);
plot(xaxis,mean(Sinhinh),'b-','Linewidth',2);
plot(xaxis,mean(Sexcinh),'k-','Linewidth',2);
plot(xaxis,mean(Sunmodunmod),'c-','Linewidth',2);

legend({'Exc-Exc','Inh-Inh','Exc-Inh','Unmod/Unmod'})

jbfill(xaxis,mean(Sexcexc)+sem(Sexcexc),mean(Sexcexc)-sem(Sexcexc),'r','r',1,1); 
jbfill(xaxis,mean(Sinhinh)+sem(Sinhinh),mean(Sinhinh)-sem(Sinhinh),'b','b',1,1); 
jbfill(xaxis,mean(Sexcinh)+sem(Sexcinh),mean(Sexcinh)-sem(Sexcinh),'k','k',1,1); 
jbfill(xaxis,mean(Sunmodunmod)+sem(Sunmodunmod),mean(Sunmodunmod)-sem(Sunmodunmod),'c','c',1,1); 


ylim([-1.4 2])

figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/PFCPFC_Corr/';
figfile = [figdir,'PFCPFC_ThetaCorrN']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);  
end


%%

%legend({'EXC/EXC','EXC/INH','INH/INH'})

%%
excexcthetapeakcorr=max(Sexcexc');
excinhthetapeakcorr=max(Sexcinh');
inhinhthetapeakcorr=max(Sinhinh');
unmodunmodthetapeakcorr=max(Sunmodunmod');


figure; hold on;
%barwitherr([std(excexcthetapeakcorr)/sqrt(length(excexcthetapeakcorr)) std(excinhthetapeakcorr)/sqrt(length(excinhthetapeakcorr)) std(inhinhthetapeakcorr)/sqrt(length(inhinhthetapeakcorr))],[mean(excexcthetapeakcorr) mean(excinhthetapeakcorr) mean(inhinhthetapeakcorr)])
%set(gca,'xticklabel',{'EXC/EXC','EXC/INH','INH/INH'})
bar(1, nanmean(excexcthetapeakcorr),'r');
bar(2, nanmean(inhinhthetapeakcorr),'b');
bar(3, nanmean(excinhthetapeakcorr),'k');
bar(4, nanmean(unmodunmodthetapeakcorr),'c');

legend({'Exc-Exc','Inh-Inh','Exc-Inh','Unmod/Unmod'})
errorbar2(1, nanmean(excexcthetapeakcorr), nanstderr(excexcthetapeakcorr), 0.3, 'r');
errorbar2(2, nanmean(inhinhthetapeakcorr), nanstderr(inhinhthetapeakcorr), 0.3, 'b');
errorbar2(3, nanmean(excinhthetapeakcorr), nanstderr(excinhthetapeakcorr), 0.3, 'k');
errorbar2(4, nanmean(unmodunmodthetapeakcorr), nanstderr(unmodunmodthetapeakcorr), 0.3, 'c');

title('PeakCC for PFC-PFC neurons ','FontSize',24,'FontWeight','normal');
ylabel('Peak standardized cross-cov','FontSize',24,'FontWeight','normal')

set(gca,'XTick',[]);

figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/PFCPFC_Corr/';
figfile = [figdir,'PFCPFC_ThetaCorrMeanN']    
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);  
end

figure; hold on;
[x y]=hist(excexcthetapeakcorr,[-4:2:10]);
[x2 y2]=hist(excinhthetapeakcorr,[-4:2:10]);
[x3 y3]=hist(inhinhthetapeakcorr,[-4:2:10]);
[x4 y4]=hist(unmodunmodthetapeakcorr,[-4:2:10]);

plot(y,x/sum(x),'r','linewidth',2);hold on;
plot(y2,x2/sum(x2),'k','linewidth',2);
plot(y3,x3/sum(x3),'b','linewidth',2);
plot(y4,x4/sum(x4),'c','linewidth',2)

xlabel('Peak cross cov','FontSize',24,'FontWeight','normal')
ylabel('Fraction of pairs','FontSize',24,'FontWeight','normal')
legend({'Exc-Exc','Inh-Inh','Exc-Inh','Unmod/Unmod'})

figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/PFCPFC_Corr/';
figfile = [figdir,'PFCPFC_ThetaCorrDistrN']    
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);  
end

%ranksum, kstest2

[p1,h1] = ranksum(excexcthetapeakcorr, excinhthetapeakcorr) %4.1 x 10-4
[p2,h2] = ranksum(inhinhthetapeakcorr, excinhthetapeakcorr) % 3.0x10-5
[p3,h3] = ranksum(excexcthetapeakcorr, inhinhthetapeakcorr) % 0.22
[p4,h4] = ranksum(unmodunmodthetapeakcorr, inhinhthetapeakcorr)  %4.3 x 10-5
[p5,h5] = ranksum(unmodunmodthetapeakcorr, excexcthetapeakcorr)  % 0.0015
[p6,h6] = ranksum(unmodunmodthetapeakcorr, excinhthetapeakcorr)  % 0.23

[p1,h1] = kstest2(excexcthetapeakcorr, excinhthetapeakcorr) %0.0022
[p2,h2] = kstest2(inhinhthetapeakcorr, excinhthetapeakcorr) % 4.2x10-5
[p3,h3] = kstest2(excexcthetapeakcorr, inhinhthetapeakcorr) % 0.25
[p4,h4] = kstest2(unmodunmodthetapeakcorr, inhinhthetapeakcorr)  %6.3 x 10-5
[p5,h5] = kstest2(unmodunmodthetapeakcorr, excexcthetapeakcorr)  % 0.0029
[p6,h6] = kstest2(unmodunmodthetapeakcorr, excinhthetapeakcorr)  % 0.32

% 39, 72 and 35 + 369 for unmodunmodfor noFS // 42, 73 and 36 for Orig

X=nan(369,4);
X(1:39,1)=excexcthetapeakcorr';
X(1:72,2)=excinhthetapeakcorr';
X(1:35,3)=inhinhthetapeakcorr';
X(1:369,4)=unmodunmodthetapeakcorr';
%kruskalwallis(X)
[p table stats]=kruskalwallis(X)
c=multcompare(stats)   % 2.3X10-6

% No unmodunmod
X=nan(72,3);
X(1:39,1)=excexcthetapeakcorr';
X(:,2)=excinhthetapeakcorr';
X(1:35,3)=inhinhthetapeakcorr';
%anova1(X)
%[p table stats]=anova1(X)
[p table stats]=kruskalwallis(X)
  % 9.9 x 10-6c=multcompare(stats)
