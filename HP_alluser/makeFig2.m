
%% EXTRACTED FROM DFSsj_HPexpt_getripalignspiking_ver4:
% Making panels of rip-trig-spiking, their mean, and theta modulation
load('/mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014_FStagged.mat');
load('/mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/HP_thetamod_PFC_alldata_Nspk50_gather_4-3-2014_FSremoved.mat');
figdir = '/data15/gideon/FigsForPaper/Fig2/';
saveg1=1;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
set(0,'defaultaxesfontname','Arial');
set(0,'defaultAxesFontName','Arial');
set(0,'defaultTextFontName','Arial');
set(0,'defaultaxesfontsize',16);

forppr=1;
tfont = 14;
xfont = 12;
yfont = 12;

tmpcnt=0;

thetaindices=[];for kk=1:length(allthetamod),thetaindices=[thetaindices;allthetamod(kk).index];end
rippleindices=[];for km=1:length(allripplemod),rippleindices=[rippleindices;allripplemod(km).index];end
% the example cells (anim day tet cell):
forFigInds=[1 2 17 2;4 15 21 4;4 15 21 3;2 3 10 1];
for j=1:size(forFigInds,1)
    %RIPPLE
    [v w]=ismember(rippleindices,forFigInds(j,:),'rows');
    curridx=forFigInds(j,:);
    i=find(w);
    currhist = allripplemod(i).hist;
    currraster = allripplemod(i).raster;
    sig_shuf = allripplemod(i).sig_shuf;
    sig_ttest = allripplemod(i).sig_ttest;
    modln_shuf = allripplemod(i).modln_shuf;
    modln_peak = allripplemod(i).modln_peak;
    modln = allripplemod(i).modln;
    currNspk = allripplemod(i).Nspk;
    modln_var = allripplemod(i).varRespAmp;
    p_var = allripplemod(i).rasterShufP;
    modln_var2 = allripplemod(i).varRespAmp2;
    p_var2 = allripplemod(i).rasterShufP2;
    sigvar=0; if p_var<0.05, sigvar=1; end
    sigvar2=0; if p_var2<0.05, sigvar2=1; end
    cellfr = allripplemod(i).cellfr;
    rip_spkshist_cellsort_PFC = currhist; rip_spks_cellsort_PFC = currraster;
    anim=curridx(1);day = curridx(2); tet = curridx(3); cell = curridx(4);
    
    %THETA
    [v w]=ismember(thetaindices,curridx,'rows');
    curthetaind=find(w);
    if length(find(w))>0
        sph = allthetamod(curthetaind).sph;
        Nspk = allthetamod(curthetaind).Nspk;
        thetahist = allthetamod(curthetaind).thetahist; % Theta histogram plot
        thetahistnorm = allthetamod(curthetaind).thetahistnorm; % Normalized - Theta histogram plot
        thetaper = allthetamod(curthetaind).thetaper; % Histogram in units of percentage of spikes
        stats = allthetamod(curthetaind).stats;
        m = allthetamod(curthetaind).modln;
        phdeg = allthetamod(curthetaind).phdeg;
        kappa = allthetamod(curthetaind).kappa;
        thetahat_deg = allthetamod(curthetaind).thetahat_deg; % From von Mises fit - use this
        prayl = allthetamod(curthetaind).prayl;
        zrayl = allthetamod(curthetaind).zrayl;
        alpha = allthetamod(curthetaind).alpha;
        pdf = allthetamod(curthetaind).vmpdf;
        meanphase = allthetamod(curthetaind).meanphase;
        countper = thetaper;
        bins = -pi:(2*pi/nbins):pi;
        ph = phdeg*(pi/180);
        
        
        tmpcnt = tmpcnt+1;
        
        set(0,'DefaultFigurePaperPositionMode','manual')
        fig=figure('PaperPosition',[0 0 3 5],'PaperSize',[3 5]);
        hold on;
        set(gcf,'Position',[100 130 300 500]);
        subplot(3,1,1); hold on;
        spkcount = [];
        spikeMat=zeros(length(rip_spks_cellsort_PFC),110);
        for c=1:length(rip_spks_cellsort_PFC)
            tmps = rip_spks_cellsort_PFC{c};
            
            spikeMat(c,round(tmps/10)+55+1)=1;
            
            plot(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),'k.','MarkerSize',4);
            
            % Get count of spikes in response window
            if ~isempty(tmps)
                subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
                spkcount = [spkcount; length(subset_tmps)];
            end
        end
        set(gca,'XLim',[-pret postt]);
        set(gca,'XTick',[])
        xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
        ylabel('SWR number','FontSize',yfont,'Fontweight','normal');
        set(gca,'YLim',[0 size(rip_spkshist_cellsort_PFC,1)]);
        ypts = 0:1:size(rip_spkshist_cellsort_PFC,1);
        xpts = 0*ones(size(ypts));
        plot(xpts , ypts, 'k--','Linewidth',2);
        set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'),'fontsize',xfont);
        xaxis = -pret:binsize:postt;
        ax1 = gca;
        set(ax1,'XColor','k','YColor','k')
        subplot(3,1,2); hold on;
        plot(xaxis,mean(rip_spkshist_cellsort_PFC),'k-','Linewidth',3);
        set(gca,'XLim',[-pret postt]);
        xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
        ylabel('Firing rate (Hz)','FontSize',yfont,'Fontweight','normal');
        set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'),'fontsize',xfont);
        ylow = min(mean(rip_spkshist_cellsort_PFC)-sem(rip_spkshist_cellsort_PFC));
        ylow=0;
        yhigh = max(mean(rip_spkshist_cellsort_PFC)+sem(rip_spkshist_cellsort_PFC));
        set(gca,'YLim',[ylow yhigh+0.1]);
        ypts = ylow-0.1:0.1:yhigh+0.1;
        xpts = 0*ones(size(ypts));
        % Plot Line at 0 ms - Onset of stimulation
        plot(xpts , ypts, 'k--','Linewidth',2);
        
        if sig_ttest ==1, str = '*'; else, str = ''; end
        if sig_shuf ==1, str_shuf = '*'; else, str_shuf = ''; end
        if sigvar ==1, str_var = '*'; else, str_var = ''; end
        if sigvar2 ==1, str_var2 = '*'; else, str_var2 = ''; end
        
        subplot(3,1,3)
        hold on
        out = bar(bins, thetaper, 'hist');
        set(gca,'XLim',[-pi pi]);
        set(gca, 'XTick', [-pi, -pi/2, 0, pi/2, pi]);
        str={'-p','-0.5p','0','0.5p','p'};
        set(gca,'xticklabel',str,'fontname','symbol','fontsize',xfont)
        
        set(gca, 'YTick', [0 2 4]);
        str2={'  0 ','  2 ','  4 '};
        set(gca,'yticklabel',str2,'fontname','arial','fontsize',xfont)
        set(gca,'xticklabel',str,'fontname','symbol','fontsize',xfont)
        
        ylabel('% of Spikes','fontname','arial','fontsize',xfont)
        xlabel('Theta phase','fontname','arial','fontsize',xfont)
        
        set(out,'FaceColor','k'); set(out,'EdgeColor','k');
        binnum = lookup(thetahat,alpha);
        
        if saveg1==1,
            figfile = [figdir,'RipAndThetaExamples_anim',num2str(anim),'_Day',num2str(day),'_Tet',num2str(tet),'_Cell',num2str(cell)];
            print(fig,'-dpdf', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
        end
        keyboard
        
    end
end
%%

% Extracted from sj_combine_thetaandripplemod2_ver4:
% Theta modulation for rip-mod and ripunmod units
% Now unused: ripmod as a function of thetamod, and venn diagram

figdir = '/data15/gideon/FigsForPaper/Fig2/';
savefig1=0;
savedir = '/mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';
area = 'PFC';
thetafile = [savedir 'HP_thetamod_PFC_alldata_Nspk50_gather_4-3-2014_FSremoved.mat'];%[savedir 'HP_thetamod_',area,'_alldata_gather'];
state = ''; %state = 'sleep'; %or state = '';
ripplefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014_FStagged.mat'];%[savedir 'HP_ripplemod',state,'_',area,'_alldata_speed_minrip2_gather_var_feb14']; % ripple mod in awake or sleep

load(ripplefile, 'allripplemod','allripplemod_idx'); % load allripplemod and allripplemod_idx.
load(thetafile,'allthetamod','allthetamod_idx');

if strcmp(state,'sleep'),
    statename = 'Sleep';
else
    statename = 'Run';
end

% Match idxs as in xcorrmesaures2

cntcells=0; cnt_mismatch=0;


for i=1:length(allthetamod)
    i;
    curridx = allthetamod_idx(i,:);
    
    
    match = rowfind(curridx, allripplemod_idx);
    
    if match~=0,
        cntcells = cntcells+1;
        allmod(cntcells).idx = curridx;
        % Theta
        allmod(cntcells).sph = allthetamod(i).sph;
        allmod(cntcells).Nspk = allthetamod(i).Nspk;
        allmod(cntcells).kappa = allthetamod(i).kappa;
        allmod(cntcells).modln = allthetamod(i).modln;
        allmod(cntcells).meanphase = allthetamod(i).meanphase;
        allmod(cntcells).prayl = allthetamod(i).prayl;
        % Ripple
        allmod(cntcells).sig_shuf = allripplemod(match).sig_shuf; % Use this to determine significance
        allmod(cntcells).ripmodln_peak = allripplemod(match).modln_peak; % % peak change above baseline
        allmod(cntcells).ripmodln = allripplemod(match).modln; % Mean change over baseline
        allmod(cntcells).ripmodln_shuf = allripplemod(match).modln_shuf; % %value of significance
        allmod(cntcells).sig_ttest = allripplemod(match).sig_ttest;
        
        % New
        allmod(cntcells).ripmodln_div = allripplemod(match).modln_div; % % peak change above baseline
        allmod(cntcells).pvar = allripplemod(match).rasterShufP;
        allmod(cntcells).mvar = allripplemod(match).varRespAmp;
        allmod(cntcells).mvar2 = allripplemod(match).varRespAmp2;
        allmod(cntcells).pvar2 = allripplemod(match).rasterShufP2;
        
        % Prop
        allmod(cntcells).cellfr = allripplemod(match).cellfr;
        
        % Rdm resp modln
        allmod(cntcells).rdmmodln_peak = allripplemod(match).modln_peak_rdm; % % peak change above baseline
        allmod(cntcells).rdmmodln = allripplemod(match).modln_rdm; % Mean change over baseline
        allmod(cntcells).FStag=allripplemod(match).FStag;
    end
    
end


set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
set(0,'defaultaxesfontname','Arial');
set(0,'defaultAxesFontName','Arial');
set(0,'defaultTextFontName','Arial');
set(0,'defaultaxesfontsize',16);
tfont = 18; % title font
xfont = 12;
yfont = 12;



% Get data
for i=1:length(allmod)
    % Theta
    allidxs(i,:) = allmod(i).idx;
    allkappas(i) = allmod(i).kappa;
    allmodln(i) = allmod(i).modln;
    allmeanphase(i) = allmod(i).meanphase;
    allprayl(i) = allmod(i).prayl;       %[92/174 theta. ~53%]
    % Ripple
    allripmodln_peak(i) = allmod(i).ripmodln_peak;
    allripmodln_div(i) = allmod(i).ripmodln_div;
    allripmodln(i) = allmod(i).ripmodln;
    allripmodln_shuf(i) = allmod(i).ripmodln_shuf; %[]
    allsigshuf(i) = allmod(i).sig_shuf;
    allsigttest(i) = allmod(i).sig_ttest;
    
    % New
    allpvar(i) = allmod(i).pvar;
    allripmodln_var(i) = allmod(i).mvar;
    allripmodln_var2(i) = allmod(i).mvar2;
    allpvar2(i) = allmod(i).pvar2;
    
    % Prop
    allcellfr(i) = allmod(i).cellfr;
    
    % Rdm resp
    allrdmmodln_peak(i) = allmod(i).rdmmodln_peak;
    allrdmmodln(i) = allmod(i).rdmmodln;
    
    
    
end

allripmodln = abs(allripmodln_var2);
sigrip = find(allpvar2<0.05); sigtheta = find(allprayl<0.05); sigboth = find(allprayl<0.05 & allpvar2<0.05==1);
allsig = union(sigrip, sigtheta);
% Vector of 0s and 1s - sig ripple vs sig theta
ripvec = zeros(size(allripmodln)); ripvec(sigrip)=1;
thetavec = zeros(size(allripmodln)); thetavec(sigtheta)=1;
[rvec,pvec] = corrcoef(ripvec,thetavec);
x = find(allmodln > 0.65 & allmodln < 0.7 & allripmodln > 80);
allidxs(x,:);
fig3=figure('PaperPosition',[0 0 3 4.8],'PaperSize',[4 4.8]);
h=barwitherr([std(allkappas(find(ripvec==1)))/sqrt(sum(ripvec)), std(allkappas(find(ripvec==0)))/sqrt(sum(ripvec==0))],[mean(allkappas(find(ripvec==1))), mean(allkappas(find(ripvec==0)))],0.4)
set(h(1),'FaceColor',[1 1 1]*0.7)
set(gca,'XTickLabel',{'Rip-mod','Rip-unmod'},'fontname','arial','fontsize',xfont)
ylim([0 0.26])
ylabel('Theta modulation (kappa)','fontsize',xfont,'fontname','arial')
set(gca,'fontsize',xfont,'fontname','arial');
sig1=ranksum(allkappas(find(ripvec==0)),allkappas(find(ripvec==1)));
if sig1<0.05 & sig1>0.01
    text(1.5, 0.25,'*','FontSize',30)
elseif sig1<0.01 & sig1>0.005
    text(1.5, 0.25,'**','FontSize',30)
elseif sig1<0.005
    text(1.5, 0.25,'***','FontSize',30)
    
end
set(gca, 'box', 'off')
xlim([0.4 2.8])


figfile = [figdir,'ThetaVsRippleMod2'];
print(fig3,'-dpdf', figfile);
%%
load('/mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014_FStagged.mat');
forFigInds=[1 2 17 2;4 15 21 4;4 15 21 3;2 3 10 1];

allhists=[];
ripmod1=[];
allindsnoFS=[];
allPsnoFS=[];
for i=1:length(allripplemod)
    if strcmp(allripplemod(i).FStag,'n')
        allhists=[allhists;(mean(allripplemod(i).hist))];
        ripmod1=[ripmod1 allripplemod(i).rasterShufP2<0.05];
        allindsnoFS=[allindsnoFS; allripplemod(i).index];
        allPsnoFS=[allPsnoFS; allripplemod(i).rasterShufP2];
        
    end
    
end

% to add arrows to the imagesc pointing at the examples from above
ex1=find(ismember(allindsnoFS,forFigInds(1,:),'rows'));
ex2=find(ismember(allindsnoFS,forFigInds(2,:),'rows'));
ex3=find(ismember(allindsnoFS,forFigInds(3,:),'rows'));
ex4=find(ismember(allindsnoFS,forFigInds(4,:),'rows'));

allhistsZ=(allhists-repmat(mean(allhists')',1,101))./repmat(std(allhists')',1,101);
respamp=mean(allhistsZ(:,50:70),2);
[dd ww]=sort(respamp);
% finding the indices after sorting
ex1s=find(ww==ex1);
ex2s=find(ww==ex2);
ex3s=find(ww==ex3);
ex4s=find(ww==ex4);

figure;            set(gcf,'Position',[100 130 400 1000]);
xaxis = -pret:binsize:postt;

imagesc(xaxis,1:max(ww),allhistsZ(ww,:))
xlabel('Time (ms)')
ylabel('PFC cells')
%% population histograms, currently unused, can uncomment
% figure; set(gcf,'Position',[100 130 1000 1000]);
% plot(xaxis,mean(allhistsZ),'k--','linewidth',3);
% hold on;
% plot(xaxis,mean(allhistsZ(ripmod1==1,:)),'k','linewidth',3);
%
% plot(xaxis,mean(abs(allhistsZ)),'r--','linewidth',3);
% plot(xaxis,mean(abs(allhistsZ(ripmod1==1,:))),'r','linewidth',3);
%
% plot(xaxis,var(allhistsZ),'b--','linewidth',3)
% plot(xaxis,var(allhistsZ(ripmod1==1,:)),'b','linewidth',3)
%
% legend({'Mean, all','Mean, ripmod','Mean abs, all','Mean abs, ripmod','Var, all','Var, ripmod'})
% xlabel('Time (ms)')

%% currently unused, SWR modulation against Kappa, can uncomment
%    xaxis = -pret:binsize:postt;
%   res1=0.1;
%   meanvec1=[];for pp=0:res1:0.3,meanvec1=[meanvec1 mean(allripmodln_var2(allkappas>pp&allkappas<(pp+res1)))];end
% meanvec1=[meanvec1 mean(allripmodln_var2(allkappas>(0.3+res1)))]
% semvec1=[];for pp=0:res1:0.3,semvec1=[semvec1 std(allripmodln_var2(allkappas>pp&allkappas<(pp+res1)))/sqrt(sum(allkappas>pp&allkappas<(pp+res1)))];end
% semvec1=[semvec1 std(allripmodln_var2(allkappas>(0.3+res1)))/sqrt(sum(allkappas>(0.3+res1)))];
% figure;errorbar(1:5,meanvec1,semvec1,'k','linewidth',2)
% set(gca,'xticklabel',{'','0-0.1','0.1-0.2','0.2-0.3','0.3-0.4','>=0.4'})
% xlabel('Kappa')
% ylabel('SWR modulation')
%%
% Kappas vs. Ripple Modulation (now unused):
%-------------------------------------
%  fig1=figure('PaperPosition',[0 0 14 8],'PaperSize',[14 8])
%   hold on; redimscreen_figforppr1;
%   set(gcf,'Position',[100 130 1000 800]);
%
% plot(allkappas, allripmodln, 'ko','MarkerSize',4);
% hold on
%  plot(allkappas(sigrip), allripmodln(sigrip), 'o','markeredgecolor',[0.5 0.5 0.5],'markerfacecolor',[0.5 0.5 0.5],'MarkerSize',2,'LineWidth',2);
% hold on
% plot(allkappas(sigtheta), allripmodln(sigtheta), 'ro','MarkerSize',5,'LineWidth',1.5);
%
%
% %title(sprintf('%s: Theta vs %s Ripple Modulation', area, statename),'FontSize',tfont,'Fontweight','normal');
% xlabel(['Kappa'],'FontSize',xfont,'Fontweight','normal');
% ylabel(sprintf('%s Ripple Modln',statename),'FontSize',yfont,'Fontweight','normal');
% %legend('All','Rip-Mod','Theta-Mod','fontsize',xfont-2);
% xlim([0 1.1])
% set(gca,'fontsize',xfont);
% [r1,p1] = corrcoef(allkappas,allripmodln)
% [r1rt,p1rt] = corrcoef(allkappas(sigboth),allripmodln(sigboth))
% [r1t,p1t] = corrcoef(allkappas(sigtheta),allripmodln(sigtheta))
% [r1r,p1r] = corrcoef(allkappas(sigrip),allripmodln(sigrip))
%
% xaxis = 0:0.1:1.5;
% plot(xaxis,zeros(size(xaxis)),'k--','LineWidth',2);
%
% str = ''; rstr = ''; tstr = ''; rtstr = '';
% if p1(1,2)<0.05, str = '*'; end
% if p1(1,2)<0.01, str = '**'; end
% if p1(1,2)<0.001, str = '***'; end
% if p1r(1,2)<0.05, rstr = '*'; end
% if p1r(1,2)<0.01, rstr = '**'; end
% if p1r(1,2)<0.001, rstr = '***'; end
% if p1t(1,2)<0.05, tstr = '*'; end
% if p1t(1,2)<0.01, tstr = '**'; end
% if p1t(1,2)<0.001, tstr = '***'; end
% if p1rt(1,2)<0.05, rtstr = '*'; end
% if p1rt(1,2)<0.01, rtstr = '**'; end
% if p1rt(1,2)<0.001, rtstr = '***'; end
%
%
% if strcmp(area,'CA1')
%     %set(gca,'XLim',[0 2200]); set(gca,'YLim',[0 1600]);
%     if ~strcmp(state,'sleep')
%         text(1.1,900,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
%         text(1.1,800,sprintf('Signf in Run: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
%         text(1.1,700,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
%         text(1.1,600,sprintf('R = %0.2f%s',r1(1,2), str),'FontSize',30,'Fontweight','normal');
%     end
% else % PFC
%     if ~strcmp(state,'sleep')
% %         set(gca,'XLim',[-0.02 1.5]);
% %         %set(gca,'YLim',[-120 200]); % Modln peak
% %         %set(gca,'YLim',[-80 100]); % Modln
% %         text(1.1,35,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
% %         text(1.1,30,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
% %         text(1.1,25,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
% %         text(1.1,20,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
% %         text(1.1,15,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
% %         text(1.1,11,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
% %         text(1.1,7,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
% %         text(1.1,3,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');
%
% %         set(gca,'XLim',[-0.02 1]);
% %         %set(gca,'YLim',[-120 200]); % Modln peak
% %         %set(gca,'YLim',[-80 90]); % Modln
% %         text(0.05,40,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
% %         text(0.62,35,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
% %         text(0.62,30,sprintf('Signf in Ripple: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
% %         text(0.62,25,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
% %         text(0.62,20,sprintf('R = %0.2f%s',r1(1,2), str),'FontSize',30,'Fontweight','normal');
% %         text(0.62,5,sprintf('Rr = %0.2f%s',r1r(1,2), rstr),'FontSize',30,'Fontweight','normal');
% %         text(0.62,10,sprintf('Rt = %0.2f%s',r1t(1,2), tstr),'FontSize',30,'Fontweight','normal');
% %         text(0.62,15,sprintf('Rrt = %0.2f%s',r1rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');
%
%     else
%         set(gca,'XLim',[-0.02 1.2]);
%         %set(gca,'YLim',[-120 500]); % Modln peak
%         %set(gca,'YLim',[-80 250]); % Modln
%         text(0.2,40,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
%         text(0.4,35,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
%         text(0.4,30,sprintf('Signf in Sleep SWR: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
%         text(0.4,25,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
%         text(0.4,20,sprintf('R = %0.2f%s',r1(1,2), str),'FontSize',30,'Fontweight','normal');
%         text(0.4,5,sprintf('Rr = %0.2f%s',r1r(1,2), rstr),'FontSize',30,'Fontweight','normal');
%         text(0.4,10,sprintf('Rt = %0.2f%s',r1t(1,2), tstr),'FontSize',30,'Fontweight','normal');
%         text(0.4,15,sprintf('Rrt = %0.2f%s',r1rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');
%
%     end
% end
%
%
% % Regression
% % -----------
% % [b00,bint00,r00,rint00,stats00] = regress(allripmodln', [ones(size(allkappas')) allkappas']);
% % xpts = min(allkappas):0.01:max(allkappas);
% % bfit00 = b00(1)+b00(2)*xpts;
% % plot(xpts,bfit00,'k-','LineWidth',2);  % Theta vs Rip
% % rsquare = stats00(1);
%
%
% % Regression for only SWR modulated cells
% % --------------------------------------------
% [b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigrip)', [ones(size(allkappas(sigrip)')) allkappas(sigrip)']);
% xpts = min(allkappas):0.01:max(allkappas);
% bfit00 = b00(1)+b00(2)*xpts;
% plot(xpts,bfit00,'-','color',[0.5 0.5 0.5],'LineWidth',4);  % Theta vs Rip - Only SWR significant
% rsquare = stats00(1);
%
% % Regression for only theta modulated cells
% % --------------------------------------------
% [b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigtheta)', [ones(size(allkappas(sigtheta)')) allkappas(sigtheta)']);
% xpts = min(allkappas):0.01:max(allkappas);
% bfit00 = b00(1)+b00(2)*xpts;
% plot(xpts,bfit00,'r-','LineWidth',4);  % Theta vs Rip - Only SWR significant
% rsquare = stats00(1);
% figfile = [figdir,'ThetaVsRippleMod'];
%  print(fig1,'-dpdf', figfile);

% Venn diagram for theta and ripple modulated cells (now unused):
% A = [length(sigrip) length(sigtheta)]; I = length(sigboth);
% fig2b=figure('PaperPosition',[0 0 3 5],'PaperSize',[4 5]);
% venn(A,I,'FaceColor',{[0.5 0.5 0.5],'r'},'FaceAlpha',{0.4,0.4},'EdgeColor','black')
%
%   text(6, 8,'Theta modulated','FontSize',xfont)
% text(-14, 8,'Ripple modulated','FontSize',xfont)
% text(1, 0,[num2str(length(sigboth))],'FontSize',xfont+2,'fontweight','bold')
% text(7, 0,[num2str(length(sigtheta)-length(sigboth))],'FontSize',xfont+2,'fontweight','bold')
% text(-4.4, 0,[num2str(length(sigrip)-length(sigboth))],'FontSize',xfont+2,'fontweight','bold')
% axis equal
% axis([-15 20 -12 12])
% axis off;
% figfile = [figdir,'_ThetaRippleModVenn'];
%  print(fig2b,'-dpdf', figfile);




   