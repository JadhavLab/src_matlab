
%Similar to PFcprop_OnlyCorrs
%Take output of Presleep RipCorr, load matpairs as well, match indices, and plot, a) PreSleep RiCorr vs SpatialCorr, b) PreSleep RiCorr vs RunRipCorr
%Then also load output of ThetaCorrvRipCorr, and plot
%c) PreSleep_RipCorr vs ThetaCorr


Wtronly=1; % Plot only for Wtrack


savefig1=0
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
presleep_ripcorr_file = [savedir 'HP_RipCorr_Presleep_std3_speed4_ntet2_alldata_gather_X8'];
load(presleep_ripcorr_file, 'Preslp_pairoutput', 'Preslp_pairoutput_idx', 'Preslpallr_epcomb', 'Preslpallp_epcomb', 'Preslpall_nsimul_epcomb');

matpair_file = [savedir, 'matpairs_noFS_X8.mat'];
load(matpair_file);
% matpairs 1:6 = pairidxs
% matpairs 7 = p_ripcorr
% matpairs 8 = r_ripcorr
% matpairs 9 = spatialcorr
% matpairs 10:11 = place field peak PFC and CA1
matpair_idxs = matpairs(:,1:6);

% Find matches between matpair idxs (lth 713) and presleep idxs (lth 1019/537).
% Can use "rowfind" since rows are unique. Or can use "ismember"
% For each matpair_idx, find the corresponding index in Preslpidx, if it exists - otherwise returns 0
match_idx = rowfind(matpair_idxs, Preslp_pairoutput_idx); % length 713, non-zeros are 537
%match_exists = (match_idx>0); match_yes = find(match_idx>0); find(Preslpall_nsimul_epcomb<5);

cnt=0;
for i=1:length(match_idx)
    if match_idx(i)~=0
        nsimul_preslp = Preslpall_nsimul_epcomb(match_idx(i));
        if nsimul_preslp >= 5
            cnt = cnt+1;
            plotpairidxs(cnt,:) = matpairs(i,1:6);
            run_ripcorr(cnt) = matpairs(i,8);
            run_p(cnt) = matpairs(i,7);
            spatialcorr(cnt) = matpairs(i,9);
            presleep_ripcorr(cnt) = Preslpallr_epcomb(match_idx(i));
            presleep_p(cnt) = Preslpallp_epcomb(match_idx(i));
            
        end
    end
end

% Ends up with 465 matching pairs

if ~Wtronly
    
    % PLOTTING FOR ALL PAIRS
    % ------------------------
    figdir = '/data25/sjadhav/HPExpt/Figures/PreSleep/PreSleepCorr/';
    % FIG PROP
    forppr = 0; % If yes, everything set to redimscreen_figforppr1 % If not, everything set to redimscreen_figforppt1
    set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
    if forppr==1
        set(0,'defaultaxesfontsize',20);
    else
        set(0,'defaultaxesfontsize',24);
    end
    
    area = 'all';
    % 1) RUN ripcorr vs PreSleep RipCorr
    figure; hold on;
    plot(run_ripcorr, presleep_ripcorr,'k.','MarkerSize',8)
    title('Run Rip Corr vs PreSleep Rip Corr - All Pairs','FontSize',24,'FontWeight','normal');
    xlabel('Run Rip Corr','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr', [ones(size(run_ripcorr')) run_ripcorr']);
    plot(min(run_ripcorr):.1:max(run_ripcorr), b(1)+b(2)*[min(run_ripcorr):.1:max(run_ripcorr)],'k','LineWidth',2)
    [r, p] = corrcoef(presleep_ripcorr, run_ripcorr)
    title(sprintf('Run Rip Corr vs PreSleep Rip Corr - All Pairs: r(%0.2f) p(%0.3f)', r(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    ylim([-0.21 0.35]);
    xlim([-0.31 0.45]);
    
    figfile = [figdir,'Run_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    % 2) SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(spatialcorr, presleep_ripcorr,'k.','MarkerSize',8)
    title('Spatial Corr vs PreSleep Rip Corr - All Pairs','FontSize',24,'FontWeight','normal');
    xlabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr', [ones(size(spatialcorr')) spatialcorr']);
    plot(min(spatialcorr):.1:max(spatialcorr), b(1)+b(2)*[min(spatialcorr):.1:max(spatialcorr)],'k','LineWidth',2)
    [r, p] = corrcoef(presleep_ripcorr, spatialcorr)
    title(sprintf('Spatial Corr vs PreSleep Rip Corr - All Pairs: r(%0.2f) p(%0.3f)', r(2), stats(3)));
    ylim([-0.21 0.35]);
    xlim([-0.5 0.8]);
    
    figfile = [figdir,'Spatial_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    % SEPARATE INTO EXC AND INH PFC
    % -----------------------------
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh Indices
    matpairs_PFC = plotpairidxs(:,[1 2 5 6]);
    % [~,Exc_ind,ib_exc] = intersect(matpairs_PFC,PFCindsExc,'rows');
    % [~,Inh_ind,ib_inh] = intersect(matpairs_PFC,PFCindsInh,'rows');
    % Mod_ind = [Exc_ind;Inh_ind];
    % Neu_ind=[1:length(matpairs_PFC)];
    % Neu_ind(Mod_ind)=[];
    
    cntexccells=0; cntexcpairs=0; Excind_inpairs=[];
    for i=1:size(PFCindsExc,1)
        ind=[]; curridx=[];
        cntexccells = cntexccells+1;
        curridx = PFCindsExc(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntexcpairs = cntexcpairs+length(ind);
        Excind_inpairs=[Excind_inpairs;ind'];
    end
    cntinhcells=0; cntinhpairs=0; Inhind_inpairs=[];
    for i=1:size(PFCindsInh,1)
        ind=[]; curridx=[];
        cntinhcells = cntinhcells+1;
        curridx = PFCindsInh(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntinhpairs = cntinhpairs+length(ind);
        Inhind_inpairs=[Inhind_inpairs;ind'];
    end
    
    
    % PLOTTING FOR EXC and INH PAIRS
    % ---------------------------------
    area = 'PFCexc';
    % 3) EXC RUN ripcorr vs PreSleep RipCorr
    figure; hold on;
    plot(run_ripcorr(Excind_inpairs), presleep_ripcorr(Excind_inpairs),'k.','MarkerSize',8)
    title('CA1 vs PFC-EXC Run Rip Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Run Rip Corr','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Excind_inpairs)', [ones(size(run_ripcorr(Excind_inpairs)')) run_ripcorr(Excind_inpairs)']);
    plot(min(run_ripcorr(Excind_inpairs)):.1:max(run_ripcorr(Excind_inpairs)), b(1)+b(2)*[min(run_ripcorr(Excind_inpairs)):.1:max(run_ripcorr(Excind_inpairs))],'r','LineWidth',2)
    [rexc, pexc] = corrcoef(presleep_ripcorr(Excind_inpairs), run_ripcorr(Excind_inpairs))
    title(sprintf('Run vs PreSleepRipCorr PFC-EXC: r(%0.2f) p(%0.3f)', rexc(2), stats(3)));
    ylim([-0.21 0.35]);
    xlim([-0.31 0.45]);
    
    figfile = [figdir,'Run_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    % 4) EXC SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(spatialcorr(Excind_inpairs), presleep_ripcorr(Excind_inpairs),'k.','MarkerSize',8)
    %title('PFC-EXC Spatial Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Excind_inpairs)', [ones(size(spatialcorr(Excind_inpairs)')) spatialcorr(Excind_inpairs)']);
    plot(min(spatialcorr(Excind_inpairs)):.1:max(spatialcorr(Excind_inpairs)), b(1)+b(2)*[min(spatialcorr(Excind_inpairs)):.1:max(spatialcorr(Excind_inpairs))],'r','LineWidth',2)
    [rexc, pexc] = corrcoef(presleep_ripcorr(Excind_inpairs), spatialcorr(Excind_inpairs))
    title(sprintf('Spatial vs PreSleepRipCorr PFC-EXC: r(%0.2f) p(%0.3f)', rexc(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    ylim([-0.21 0.35]);
    xlim([-0.5 0.8]);
    
    figfile = [figdir,'Spatial_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    
    area = 'PFCinh';
    % 5) INH RUN ripcorr vs PreSleep RipCorr
    figure; hold on;
    plot(run_ripcorr(Inhind_inpairs), presleep_ripcorr(Inhind_inpairs),'k.','MarkerSize',8)
    title('CA1 vs PFC-INH Run Rip Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Run Rip Corr','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Inhind_inpairs)', [ones(size(run_ripcorr(Inhind_inpairs)')) run_ripcorr(Inhind_inpairs)']);
    plot(min(run_ripcorr(Inhind_inpairs)):.1:max(run_ripcorr(Inhind_inpairs)), b(1)+b(2)*[min(run_ripcorr(Inhind_inpairs)):.1:max(run_ripcorr(Inhind_inpairs))],'b','LineWidth',2)
    [rinh, pinh] = corrcoef(presleep_ripcorr(Inhind_inpairs), run_ripcorr(Inhind_inpairs))
    title(sprintf('Run vs PreSleep PFC-INH: r(%0.2f) p(%0.3f)', rinh(2), stats(3)));
    ylim([-0.21 0.25]);
    xlim([-0.31 0.4]);
    
    figfile = [figdir,'Run_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    % 6) INH SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(spatialcorr(Inhind_inpairs), presleep_ripcorr(Inhind_inpairs),'k.','MarkerSize',8)
    %title('PFC-EXC Spatial Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Inhind_inpairs)', [ones(size(spatialcorr(Inhind_inpairs)')) spatialcorr(Inhind_inpairs)']);
    plot(min(spatialcorr(Inhind_inpairs)):.1:max(spatialcorr(Inhind_inpairs)), b(1)+b(2)*[min(spatialcorr(Inhind_inpairs)):.1:max(spatialcorr(Inhind_inpairs))],'b','LineWidth',2)
    [rinh, pinh] = corrcoef(presleep_ripcorr(Inhind_inpairs), spatialcorr(Inhind_inpairs))
    title(sprintf('Spatial vs PreSleep PFC-INH: r(%0.2f) p(%0.3f)', rinh(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    ylim([-0.21 0.25]);
    xlim([-0.5 0.8]);
    
    figfile = [figdir,'Spatial_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    
    
    
    % PreSleep Ripcorr vs ThetaCorr
    % -----------------------------
    presleep_ripcorr_file = [savedir 'HP_RipCorr_Presleep_std3_speed4_ntet2_alldata_gather_X8'];
    load(presleep_ripcorr_file, 'Preslp_pairoutput', 'Preslp_pairoutput_idx', 'Preslpallr_epcomb', 'Preslpallp_epcomb', 'Preslpall_nsimul_epcomb');
    
    thetacorr_file = [savedir, 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_gather_X8.mat'];
    load(thetacorr_file);
    thetapair_idxs = runpairoutput_idx;
    all_nsimul = Sall_nsimul_epcomb;
    % Theta Corr variables
    allZcrosscov_sm_runtheta = SallZcrosscov_sm_runtheta_epcomb;
    allZcrosscov_runtheta = SallZcrosscov_runtheta_epcomb;
    alltheta_peakcorr = Salltheta_peakcorr_epcomb;
    alltheta_peaklag = Salltheta_peaklag_epcomb;
    % Rip Corr variables
    allr = Sallr_epcomb;
    allp = Sallp_epcomb;
    
    use=[];
    for i=1:size(thetapair_idxs,1)
        if ~isnan(allr(i)) && ~isnan(alltheta_peakcorr(i))
            if all_nsimul(i)>=5
                use = [use;thetapair_idxs(i,:)];
            end
        end
    end
    thetapair_idxs=use;
    
    % Find matches between thetapair idxs (lth 713/1370) and presleep idxs (lth 537).
    % Only 713/1370 thetapairidxs have condn satisfied of nsimul>=5
    % Can use "rowfind" since rows are unique. Or can use "ismember"
    % For each thetapair_idx, find the corresponding index in Preslpidx, if it exists - otherwise returns 0
    match_idx = rowfind(thetapair_idxs, Preslp_pairoutput_idx); % length 713
    %match_exists = (match_idx>0); match_yes = find(match_idx>0); find(Preslpall_nsimul_epcomb<5);
    
    cnt=0; thetacorr=[]; presleep_ripcorr=[]; presleep_p=[]; plotpairidxs=[]; run_ripcorr=[];run_p=[];
    for i=1:length(match_idx)
        if match_idx(i)~=0
            nsimul_preslp = Preslpall_nsimul_epcomb(match_idx(i));
            
            if nsimul_preslp >= 5
                cnt = cnt+1;
                plotpairidxs(cnt,:) = thetapair_idxs(i,:);
                run_ripcorr(cnt) = allr(i);
                run_p(cnt) = allp(i);
                thetacorr(cnt) = alltheta_peakcorr(i);
                presleep_ripcorr(cnt) = Preslpallr_epcomb(match_idx(i));
                presleep_p(cnt) = Preslpallp_epcomb(match_idx(i));
            end
            
            
        end
    end
    
    % Ends up with 465 matching pairs
    
    
    
    % SEPARATE INTO EXC AND INH PFC
    % -----------------------------
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh Indices
    matpairs_PFC = plotpairidxs(:,[1 2 5 6]);
    % [~,Exc_ind,ib_exc] = intersect(matpairs_PFC,PFCindsExc,'rows');
    % [~,Inh_ind,ib_inh] = intersect(matpairs_PFC,PFCindsInh,'rows');
    % Mod_ind = [Exc_ind;Inh_ind];
    % Neu_ind=[1:length(matpairs_PFC)];
    % Neu_ind(Mod_ind)=[];
    
    cntexccells=0; cntexcpairs=0; Excind_inpairs=[];  %254 exc
    for i=1:size(PFCindsExc,1)
        ind=[]; curridx=[];
        cntexccells = cntexccells+1;
        curridx = PFCindsExc(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntexcpairs = cntexcpairs+length(ind);
        Excind_inpairs=[Excind_inpairs;ind'];
    end
    cntinhcells=0; cntinhpairs=0; Inhind_inpairs=[]; %211 inh
    for i=1:size(PFCindsInh,1)
        ind=[]; curridx=[];
        cntinhcells = cntinhcells+1;
        curridx = PFCindsInh(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntinhpairs = cntinhpairs+length(ind);
        Inhind_inpairs=[Inhind_inpairs;ind'];
    end
    
    
    % Plotting for all pairs
    % ----------------------
    area = 'PFCripmod';
    % *) SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(thetacorr, presleep_ripcorr,'k.','MarkerSize',8)
    %title('PFC-EXC Spatial Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Peak theta cov','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr', [ones(size(thetacorr')) thetacorr']);
    plot(min(thetacorr):.1:max(thetacorr), b(1)+b(2)*[min(thetacorr):.1:max(thetacorr)],'k','LineWidth',2)
    [rexc, pexc] = corrcoef(presleep_ripcorr, thetacorr);
    title(sprintf('ThetaCov vs PreSleepRipCorr all: r(%0.2f) p(%0.3f)', rexc(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    ylim([-0.22 0.37]);
    xlim([-4 6]);
    
    figfile = [figdir,'ThetaCov_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    % PLOTTING FOR EXC and INH PAIRS
    % ---------------------------------
    area = 'PFCexc';
    % *) EXC SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(thetacorr(Excind_inpairs), presleep_ripcorr(Excind_inpairs),'k.','MarkerSize',8)
    %title('PFC-EXC Spatial Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Peak theta cov','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Excind_inpairs)', [ones(size(thetacorr(Excind_inpairs)')) thetacorr(Excind_inpairs)']);
    plot(min(thetacorr(Excind_inpairs)):.1:max(thetacorr(Excind_inpairs)), b(1)+b(2)*[min(thetacorr(Excind_inpairs)):.1:max(thetacorr(Excind_inpairs))],'r','LineWidth',2)
    [rexc, pexc] = corrcoef(presleep_ripcorr(Excind_inpairs), thetacorr(Excind_inpairs))
    title(sprintf('ThetaCov vs PreSleepRipCorr PFC-EXC: r(%0.2f) p(%0.3f)', rexc(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    figfile = [figdir,'ThetaCov_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    area = 'PFCinh';
    % *) INH SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(thetacorr(Inhind_inpairs), presleep_ripcorr(Inhind_inpairs),'k.','MarkerSize',8)
    %title('PFC-EXC Spatial Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Peak theta cov','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [rinh, pinh] = corrcoef(presleep_ripcorr(Inhind_inpairs), thetacorr(Inhind_inpairs))
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Inhind_inpairs)', [ones(size(thetacorr(Inhind_inpairs)')) thetacorr(Inhind_inpairs)']);
    plot(min(thetacorr(Inhind_inpairs)):.1:max(thetacorr(Inhind_inpairs)), b(1)+b(2)*[min(thetacorr(Inhind_inpairs)):.1:max(thetacorr(Inhind_inpairs))],'b','LineWidth',2)
    %title(sprintf('PFC-INH: r^2(%0.3f) p(%0.3f)', stats(1), stats(3)));
    title(sprintf('ThetaCov vs PreSleepRipCorr PFC-INH: r(%0.2f) p(%0.3f)', rinh(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    figfile = [figdir,'ThetaCov_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
end

% ---------------------------------% ---------------------------------% ---------------------------------














% Do only W-track: Get animals 1-3
% -------------------------------------

if Wtronly
    
    
    Wtridxs = find(matpair_idxs(:,1)<=3);
    matpair_idxs_Wtr = matpair_idxs(Wtridxs,:); % 527 out of 713 pairs
    
    
    
    % Find matches between matpair idxs (lth 527) and presleep idxs (lth 1019/537).
    % Can use "rowfind" since rows are unique. Or can use "ismember"
    % For each matpair_idx, find the corresponding index in Preslpidx, if it exists - otherwise returns 0
    match_idx = rowfind(matpair_idxs_Wtr, Preslp_pairoutput_idx); % length 527, non-zeros are 483
    %match_exists = (match_idx>0); match_yes = find(match_idx>0); find(Preslpall_nsimul_epcomb<5);
    
    cnt=0;
    plotpairidxs=[];
    run_ripcorr=[];
    run_p = [];
    spatialcorr=[];
    presleep_ripcorr=[];
    presleep_p=[];
    
    for i=1:length(match_idx)
        if match_idx(i)~=0
            nsimul_preslp = Preslpall_nsimul_epcomb(match_idx(i));
            if nsimul_preslp >= 5
                cnt = cnt+1;
                plotpairidxs(cnt,:) = matpairs(i,1:6);
                run_ripcorr(cnt) = matpairs(i,8);
                run_p(cnt) = matpairs(i,7);
                spatialcorr(cnt) = matpairs(i,9);
                presleep_ripcorr(cnt) = Preslpallr_epcomb(match_idx(i));
                presleep_p(cnt) = Preslpallp_epcomb(match_idx(i));
                
            end
        end
    end
    
    % Ends up with 415 matching pairs
    
    
    
    % PLOTTING FOR ALL PAIRS
    % ------------------------
    figdir = '/data25/sjadhav/HPExpt/Figures/PreSleep/PreSleepCorr/Wtronly/';
    % FIG PROP
    forppr = 0; % If yes, everything set to redimscreen_figforppr1 % If not, everything set to redimscreen_figforppt1
    set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
    if forppr==1
        set(0,'defaultaxesfontsize',20);
    else
        set(0,'defaultaxesfontsize',24);
    end
    
    area = 'all_Wtronly';
    % 1) RUN ripcorr vs PreSleep RipCorr
    figure; hold on;
    plot(run_ripcorr, presleep_ripcorr,'k.','MarkerSize',8)
    title('Run Rip Corr vs PreSleep Rip Corr - All Pairs','FontSize',24,'FontWeight','normal');
    xlabel('Run Rip Corr','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr', [ones(size(run_ripcorr')) run_ripcorr']);
    plot(min(run_ripcorr):.1:max(run_ripcorr), b(1)+b(2)*[min(run_ripcorr):.1:max(run_ripcorr)],'k','LineWidth',2)
    [r, p] = corrcoef(presleep_ripcorr, run_ripcorr)
    title(sprintf('Run Rip Corr vs PreSleep Rip Corr - All Pairs: r(%0.2f) p(%0.3f)', r(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    ylim([-0.21 0.25]);
    xlim([-0.31 0.45]);
    
    figfile = [figdir,'Run_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    % 2) SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(spatialcorr, presleep_ripcorr,'k.','MarkerSize',8)
    title('Spatial Corr vs PreSleep Rip Corr - All Pairs','FontSize',24,'FontWeight','normal');
    xlabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr', [ones(size(spatialcorr')) spatialcorr']);
    plot(min(spatialcorr):.1:max(spatialcorr), b(1)+b(2)*[min(spatialcorr):.1:max(spatialcorr)],'k','LineWidth',2)
    [r, p] = corrcoef(presleep_ripcorr, spatialcorr)
    title(sprintf('Spatial Corr vs PreSleep Rip Corr - All Pairs: r(%0.2f) p(%0.3f)', r(2), stats(3)));
    ylim([-0.21 0.25]);
    xlim([-0.5 0.8]);
    
    figfile = [figdir,'Spatial_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    % SEPARATE INTO EXC AND INH PFC
    % -----------------------------
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh Indices
    matpairs_PFC = plotpairidxs(:,[1 2 5 6]);
    % [~,Exc_ind,ib_exc] = intersect(matpairs_PFC,PFCindsExc,'rows');
    % [~,Inh_ind,ib_inh] = intersect(matpairs_PFC,PFCindsInh,'rows');
    % Mod_ind = [Exc_ind;Inh_ind];
    % Neu_ind=[1:length(matpairs_PFC)];
    % Neu_ind(Mod_ind)=[];
    
    cntexccells=0; cntexcpairs=0; Excind_inpairs=[];
    for i=1:size(PFCindsExc,1)
        ind=[]; curridx=[];
        cntexccells = cntexccells+1;
        curridx = PFCindsExc(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntexcpairs = cntexcpairs+length(ind);
        Excind_inpairs=[Excind_inpairs;ind'];
    end
    cntinhcells=0; cntinhpairs=0; Inhind_inpairs=[];
    for i=1:size(PFCindsInh,1)
        ind=[]; curridx=[];
        cntinhcells = cntinhcells+1;
        curridx = PFCindsInh(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntinhpairs = cntinhpairs+length(ind);
        Inhind_inpairs=[Inhind_inpairs;ind'];
    end
    
    
    % PLOTTING FOR EXC and INH PAIRS
    % ---------------------------------
    area = 'PFCexc_Wtronly';
    % 3) EXC RUN ripcorr vs PreSleep RipCorr
    figure; hold on;
    plot(run_ripcorr(Excind_inpairs), presleep_ripcorr(Excind_inpairs),'k.','MarkerSize',8)
    title('CA1 vs PFC-EXC Run Rip Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Run Rip Corr','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Excind_inpairs)', [ones(size(run_ripcorr(Excind_inpairs)')) run_ripcorr(Excind_inpairs)']);
    plot(min(run_ripcorr(Excind_inpairs)):.1:max(run_ripcorr(Excind_inpairs)), b(1)+b(2)*[min(run_ripcorr(Excind_inpairs)):.1:max(run_ripcorr(Excind_inpairs))],'r','LineWidth',2)
    [rexc, pexc] = corrcoef(presleep_ripcorr(Excind_inpairs), run_ripcorr(Excind_inpairs))
    title(sprintf('Run vs PreSleepRipCorr PFC-EXC: r(%0.2f) p(%0.3f)', rexc(2), stats(3)));
    ylim([-0.18 0.21]);
    xlim([-0.31 0.45]);
    
    figfile = [figdir,'Run_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    % 4) EXC SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(spatialcorr(Excind_inpairs), presleep_ripcorr(Excind_inpairs),'k.','MarkerSize',8)
    %title('PFC-EXC Spatial Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Excind_inpairs)', [ones(size(spatialcorr(Excind_inpairs)')) spatialcorr(Excind_inpairs)']);
    plot(min(spatialcorr(Excind_inpairs)):.1:max(spatialcorr(Excind_inpairs)), b(1)+b(2)*[min(spatialcorr(Excind_inpairs)):.1:max(spatialcorr(Excind_inpairs))],'r','LineWidth',2)
    [rexc, pexc] = corrcoef(presleep_ripcorr(Excind_inpairs), spatialcorr(Excind_inpairs))
    title(sprintf('Spatial vs PreSleepRipCorr PFC-EXC: r(%0.2f) p(%0.3f)', rexc(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    ylim([-0.18 0.21]);
    xlim([-0.5 0.8]);
    
    figfile = [figdir,'Spatial_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    
    area = 'PFCinh_Wtronly';
    % 5) INH RUN ripcorr vs PreSleep RipCorr
    figure; hold on;
    plot(run_ripcorr(Inhind_inpairs), presleep_ripcorr(Inhind_inpairs),'k.','MarkerSize',8)
    title('CA1 vs PFC-INH Run Rip Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Run Rip Corr','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Inhind_inpairs)', [ones(size(run_ripcorr(Inhind_inpairs)')) run_ripcorr(Inhind_inpairs)']);
    plot(min(run_ripcorr(Inhind_inpairs)):.1:max(run_ripcorr(Inhind_inpairs)), b(1)+b(2)*[min(run_ripcorr(Inhind_inpairs)):.1:max(run_ripcorr(Inhind_inpairs))],'b','LineWidth',2)
    [rinh, pinh] = corrcoef(presleep_ripcorr(Inhind_inpairs), run_ripcorr(Inhind_inpairs))
    title(sprintf('Run vs PreSleep PFC-INH: r(%0.2f) p(%0.3f)', rinh(2), stats(3)));
    ylim([-0.21 0.21]);
    xlim([-0.31 0.4]);
    
    figfile = [figdir,'Run_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    % 6) INH SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(spatialcorr(Inhind_inpairs), presleep_ripcorr(Inhind_inpairs),'k.','MarkerSize',8)
    %title('PFC-EXC Spatial Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Inhind_inpairs)', [ones(size(spatialcorr(Inhind_inpairs)')) spatialcorr(Inhind_inpairs)']);
    plot(min(spatialcorr(Inhind_inpairs)):.1:max(spatialcorr(Inhind_inpairs)), b(1)+b(2)*[min(spatialcorr(Inhind_inpairs)):.1:max(spatialcorr(Inhind_inpairs))],'b','LineWidth',2)
    [rinh, pinh] = corrcoef(presleep_ripcorr(Inhind_inpairs), spatialcorr(Inhind_inpairs))
    title(sprintf('Spatial vs PreSleep PFC-INH: r(%0.2f) p(%0.3f)', rinh(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    ylim([-0.21 0.21]);
    xlim([-0.5 0.8]);
    
    figfile = [figdir,'Spatial_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    
    
    
    % PreSleep Ripcorr vs ThetaCorr
    % -----------------------------
    presleep_ripcorr_file = [savedir 'HP_RipCorr_Presleep_std3_speed4_ntet2_alldata_gather_X8'];
    load(presleep_ripcorr_file, 'Preslp_pairoutput', 'Preslp_pairoutput_idx', 'Preslpallr_epcomb', 'Preslpallp_epcomb', 'Preslpall_nsimul_epcomb');
    
    thetacorr_file = [savedir, 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_gather_X8.mat'];
    load(thetacorr_file);
    thetapair_idxs = runpairoutput_idx;
    all_nsimul = Sall_nsimul_epcomb;
    % Theta Corr variables
    allZcrosscov_sm_runtheta = SallZcrosscov_sm_runtheta_epcomb;
    allZcrosscov_runtheta = SallZcrosscov_runtheta_epcomb;
    alltheta_peakcorr = Salltheta_peakcorr_epcomb;
    alltheta_peaklag = Salltheta_peaklag_epcomb;
    % Rip Corr variables
    allr = Sallr_epcomb;
    allp = Sallp_epcomb;
    
    use=[];
    for i=1:size(thetapair_idxs,1)
        if ~isnan(allr(i)) && ~isnan(alltheta_peakcorr(i))
            if all_nsimul(i)>=5
                use = [use;thetapair_idxs(i,:)];
            end
        end
    end
    thetapair_idxs=use;
    Wtridxs = find(thetapair_idxs(:,1)<=3);
    thetapair_idxs_Wtr = thetapair_idxs(Wtridxs,:); % 527 out of 713 pairs
    
    % Find Wtr animals only
    % ------------------
    
    % Find matches between thetapair idxs (lth 527 out of 713/1370) and presleep idxs (1019/lth 537).
    % Only 713/1370 thetapairidxs have condn satisfied of nsimul>=5
    % Can use "rowfind" since rows are unique. Or can use "ismember"
    % For each thetapair_idx, find the corresponding index in Preslpidx, if it exists - otherwise returns 0
    match_idx = rowfind(thetapair_idxs_Wtr, Preslp_pairoutput_idx); % length 527
    %match_exists = (match_idx>0); match_yes = find(match_idx>0); find(Preslpall_nsimul_epcomb<5);
    
    cnt=0; thetacorr=[]; presleep_ripcorr=[]; presleep_p=[]; plotpairidxs=[]; run_ripcorr=[];run_p=[];
    for i=1:length(match_idx)
        if match_idx(i)~=0
            nsimul_preslp = Preslpall_nsimul_epcomb(match_idx(i));
            
            if nsimul_preslp >= 5
                cnt = cnt+1;
                plotpairidxs(cnt,:) = thetapair_idxs(i,:);
                run_ripcorr(cnt) = allr(i);
                run_p(cnt) = allp(i);
                thetacorr(cnt) = alltheta_peakcorr(i);
                presleep_ripcorr(cnt) = Preslpallr_epcomb(match_idx(i));
                presleep_p(cnt) = Preslpallp_epcomb(match_idx(i));
            end
            
            
        end
    end
    
    % Ends up with 415 matching pairs
  
    
    
    % SEPARATE INTO EXC AND INH PFC
    % -----------------------------
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh Indices
    matpairs_PFC = plotpairidxs(:,[1 2 5 6]);
    % [~,Exc_ind,ib_exc] = intersect(matpairs_PFC,PFCindsExc,'rows');
    % [~,Inh_ind,ib_inh] = intersect(matpairs_PFC,PFCindsInh,'rows');
    % Mod_ind = [Exc_ind;Inh_ind];
    % Neu_ind=[1:length(matpairs_PFC)];
    % Neu_ind(Mod_ind)=[];
    
    cntexccells=0; cntexcpairs=0; Excind_inpairs=[];
    for i=1:size(PFCindsExc,1)
        ind=[]; curridx=[];
        cntexccells = cntexccells+1;
        curridx = PFCindsExc(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntexcpairs = cntexcpairs+length(ind);
        Excind_inpairs=[Excind_inpairs;ind'];
    end
    cntinhcells=0; cntinhpairs=0; Inhind_inpairs=[];
    for i=1:size(PFCindsInh,1)
        ind=[]; curridx=[];
        cntinhcells = cntinhcells+1;
        curridx = PFCindsInh(i,:);
        ind=find(ismember(matpairs_PFC,curridx,'rows'))';
        
        cntinhpairs = cntinhpairs+length(ind);
        Inhind_inpairs=[Inhind_inpairs;ind'];
    end
    
    
    % Plotting for all pairs
    % ----------------------
    area = 'PFCripmod_Wtronly';
    % *) SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(thetacorr, presleep_ripcorr,'k.','MarkerSize',8)
    %title('PFC-EXC Spatial Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Peak theta cov','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr', [ones(size(thetacorr')) thetacorr']);
    plot(min(thetacorr):.1:max(thetacorr), b(1)+b(2)*[min(thetacorr):.1:max(thetacorr)],'k','LineWidth',2)
    [rexc, pexc] = corrcoef(presleep_ripcorr, thetacorr);
    title(sprintf('ThetaCov vs PreSleepRipCorr all: r(%0.2f) p(%0.3f)', rexc(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    ylim([-0.19 0.21]);
    xlim([-4 6]);
    
    figfile = [figdir,'ThetaCov_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    % PLOTTING FOR EXC and INH PAIRS
    % ---------------------------------
    area = 'PFCexc_Wtronly';
    % *) EXC SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(thetacorr(Excind_inpairs), presleep_ripcorr(Excind_inpairs),'k.','MarkerSize',8)
    %title('PFC-EXC Spatial Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Peak theta cov','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Excind_inpairs)', [ones(size(thetacorr(Excind_inpairs)')) thetacorr(Excind_inpairs)']);
    plot(min(thetacorr(Excind_inpairs)):.1:max(thetacorr(Excind_inpairs)), b(1)+b(2)*[min(thetacorr(Excind_inpairs)):.1:max(thetacorr(Excind_inpairs))],'r','LineWidth',2)
    [rexc, pexc] = corrcoef(presleep_ripcorr(Excind_inpairs), thetacorr(Excind_inpairs))
    title(sprintf('ThetaCov vs PreSleepRipCorr PFC-EXC: r(%0.2f) p(%0.3f)', rexc(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    ylim([-0.19 0.21]);
    xlim([-4 6]);
    
    figfile = [figdir,'ThetaCov_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    area = 'PFCinh_Wtronly';
    % *) INH SpatialCorr vs PreSleep RipCorr
    figure; hold on;
    plot(thetacorr(Inhind_inpairs), presleep_ripcorr(Inhind_inpairs),'k.','MarkerSize',8)
    %title('PFC-EXC Spatial Corr vs PreSleep Rip Corr','FontSize',24,'FontWeight','normal');
    xlabel('Peak theta cov','FontSize',24,'FontWeight','normal')
    ylabel('PreSleep RipCorr','FontSize',24,'FontWeight','normal')
    
    [rinh, pinh] = corrcoef(presleep_ripcorr(Inhind_inpairs), thetacorr(Inhind_inpairs))
    [b,bint,r,rint,stats] = regress(presleep_ripcorr(Inhind_inpairs)', [ones(size(thetacorr(Inhind_inpairs)')) thetacorr(Inhind_inpairs)']);
    plot(min(thetacorr(Inhind_inpairs)):.1:max(thetacorr(Inhind_inpairs)), b(1)+b(2)*[min(thetacorr(Inhind_inpairs)):.1:max(thetacorr(Inhind_inpairs))],'b','LineWidth',2)
    %title(sprintf('PFC-INH: r^2(%0.3f) p(%0.3f)', stats(1), stats(3)));
    title(sprintf('ThetaCov vs PreSleepRipCorr PFC-INH: r(%0.2f) p(%0.3f)', rinh(2), stats(3)));
    %set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
     ylim([-0.19 0.21]);
    xlim([-4 6]);
    
    figfile = [figdir,'ThetaCov_vs_PreSleepRipCorr_',area]
    if savefig1==1,
        print('-dpdf', figfile); print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    
end













keyboard;

