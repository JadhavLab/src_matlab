% From PFcprop_corrs: only do the pairwaire correlations

% % ------------
% % PLOTTING, ETC
% % PFC Properties - from Demetris' code: DFS_DRsj_spatialvsripcorr.m
% % ------------


plotSWRcorr=1; % Plot swrcorr vs spatialcorr
savefig1=0;

%figdir = '/data25/sjadhav/HPExpt/Figures/2015_ReplayandPFCprop/SpatialCorrAndProperties';
figdir = '/data25/sjadhav/HPExpt/Figures/SpatialCorr/Learning/';
% % ----FIG PROP --------------
forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1
figdir = '/data25/sjadhav/HPExpt/Figures/SpatialCorr/Learning/'; summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
if forppr==1
    set(0,'defaultaxesfontsize',20);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end
% % ---------------------

val=0; area='PFC_SWRmod_noFS'; % matpairs original with FS removed, from DFS_DRsj_spatialvsripcorr_FSemoved.m
%val=1; area='PFC_SWRmod'; % matpairs original, from DFS_DRsj_spatialvsripcorr.m
%val=2; area = 'PFC_SWRmod_nospeed'; % matpairs_nospeed_X6, from DFS_DRsj_spatialvsripcorr_PlotFields.m
%val=3; area = 'PFC_onlytheta'; % matpairs_PFConlytheta_X6, from DFS_DRsj_spatialvsripcorr_PFCcategories.m
%val=4; area = 'PFC_SWRandTheta'; % matpairs_PFC_SWRandTheta_X6, from DFS_DRsj_spatialvsripcorr_PFCcategories.m
%val=5; area = 'PFC_onlySWR';  %matpairs_PFC_onlySWR_X6, from DFS_DRsj_spatialvsripcorr_PFCcategories.m


if plotSWRcorr
    
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_X6.mat'; % PFC Exc-Inh Indices
    
    switch val
        case 0
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_noFS_X6.mat'; % Has SWRcorr and SpatCorr
        case 1
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs.mat'; % Has SWRcorr and SpatCorr
        case 2
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_nospeed_X6.mat'; % Has SWRcorr and SpatCorr: Spat Corr with no speed criterion
        case 3
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_PFConlytheta_X6.mat';
        case 4
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_PFC_SWRandTheta_X6.mat';
        case 5
            load '/data25/sjadhav/HPExpt/HP_ProcessedData/matpairs_PFConlySWR_X6.mat';
    end
    
    % [An day CA1tet CA1cell PFCtet PFCcell SWRcorrPval SWRcorrRval SpatCorrRval] [10=fldspeakPFC 11=fldspeak CA1]
    
    
    % Learning from HPperfday, HPbperfday, etc
    HPa_perfin = [0.9,0.88,0.91,1,0.97,1,0.93,0.97];
    HPa_perfout = [0.5,0.81,0.74,0.76,0.78,0.84,0.81,0.89];
    HPa_perf = [0.7,0.84,0.82,0.88,0.88,0.92,0.87,0.93];
    
    HPb_perfin = [0.51,0.91,0.84,0.89,0.98,1,1,1];
    HPb_perfout = [0.7,0.63,0.68,0.57,0.70,0.81,0.56,0.76];
    HPb_perf = [0.61,0.77,0.76,0.73,0.84,0.9,0.78,0.88];
    
    HPc_perfin = [0.68,0.78,0.81,0.83,0.94,1,0.96,0.88];
    HPc_perfout = [0.53,0.6,0.68,0.73,0.66,0.69,0.7,0.87];
    HPc_perf = [0.61,0.69,0.75,0.78,0.80,0.84,0.83,0.88];
    
    
    HPall_perf = mean([HPa_perf;HPb_perf;HPc_perf]);
    HPall_perfin = mean([HPa_perfin;HPb_perfin;HPc_perfin]);
    HPall_perfout = mean([HPa_perfout;HPb_perfout;HPc_perfout]);
    
    
    
    % SEPARATE BY DAYS BELOW A PARTICULAR LEARNING RATE
    % --------------------------------------------------
    % OutboundPerf>=0.7, or OverallPerf>=0.8 
    getlow=[]; gethigh=[]; 
    HPa_low = find(HPa_perf<0.8); HPa_high = find(HPa_perf>=0.8);
    HPb_low = find(HPb_perf<0.8); HPb_high = find(HPb_perf>=0.8);
    HPc_low = find(HPc_perf<0.8); HPc_high = find(HPc_perf>=0.8);
    
    %tmp = find(matpairs(:,1)==1 & matpairs(:,2)<=n
    
    
    
    % SEPARATE BY DAY FOR W_TRACK ANIMALS / INSTEAD, CAN FIND DAYS BELOW A PARTICULAR LEARNING RATE
    % ---------------------------------------------------------------------------------------------
    
    n=3;
    %W-track animals, first n days
    possigrippairs1 = matpairs(matpairs(:,1)<=3 & matpairs(:,2)<=n & matpairs(:,7)<0.05 & matpairs(:,8) > 0,9);
    negsigrippairs1 = matpairs(matpairs(:,1)<=3 & matpairs(:,2)<=n & matpairs(:,7)<0.05 & matpairs(:,8) < 0,9);
    nonsigrippairs1 = matpairs(matpairs(:,1)<=3 & matpairs(:,2)<=n & matpairs(:,7)>0.05, 9);
    
    %W-track animals, Days n+1 to end
    possigrippairs2 = matpairs(matpairs(:,1)<=3 & matpairs(:,2)>=n+1 & matpairs(:,7)<0.05 & matpairs(:,8) > 0,9);
    negsigrippairs2 = matpairs(matpairs(:,1)<=3 & matpairs(:,2)>=n+1 & matpairs(:,7)<0.05 & matpairs(:,8) < 0,9);
    nonsigrippairs2 = matpairs(matpairs(:,1)<=3 & matpairs(:,2)>=n+1 & matpairs(:,7)>0.05, 9);
    
    % a) Early Learning
    figure; hold on;
    bar([mean(negsigrippairs1) mean(nonsigrippairs1) mean(possigrippairs1)]); hold on;
    legend
    errorbar2([1 2 3], [mean(negsigrippairs1) mean(nonsigrippairs1) mean(possigrippairs1)],  [stderr(negsigrippairs1) stderr(nonsigrippairs1) stderr(possigrippairs1)] , 0.3, 'k')
    title('Early Learning: Spatial Corrln for SWR-correlated pair categories','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2 3]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal');
    figfile = [figdir,'EarlyLearning_SpatialvsSWRcorr_AllBar_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    %stats
    [pKW table statsKW] = kruskalwallis([negsigrippairs1' nonsigrippairs1' possigrippairs1'], [ones(1, length(negsigrippairs1)) ones(1,length(nonsigrippairs1)).*2 ones(1,length(possigrippairs1)).*3]);
    %[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'off', 'alpha', 0.01); % change the alpha values to determine significance range.
    %c
    
    
    % b) Late Learning
    figure; hold on;
    bar([mean(negsigrippairs2) mean(nonsigrippairs2) mean(possigrippairs2)]); hold on;
    legend
    errorbar2([1 2 3], [mean(negsigrippairs2) mean(nonsigrippairs2) mean(possigrippairs2)],  [stderr(negsigrippairs2) stderr(nonsigrippairs2) stderr(possigrippairs2)] , 0.3, 'k')
    title('Late Learning: Spatial Corrln for SWR-correlated pair categories','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2 3]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal');
    figfile = [figdir,'LateLearning_SpatialvsSWRcorr_AllBar_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    %stats
    [pKW table statsKW] = kruskalwallis([negsigrippairs2' nonsigrippairs2' possigrippairs2'], [ones(1, length(negsigrippairs2)) ones(1,length(nonsigrippairs2)).*2 ones(1,length(possigrippairs2)).*3]);
    %[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'off', 'alpha', 0.01); % change the alpha values to determine significance range.
    %c
    
    
    
    % SCATTER:
    % -------
    
    earlypairs = matpairs(matpairs(:,1)<=3 & matpairs(:,2)<=n,:);
    latepairs = matpairs(matpairs(:,1)<=3 & matpairs(:,2)>=n+1,:);
    
    sigearlypairs_idx = find(earlypairs(:,7)<0.05);
    siglatepairs_idx = find(latepairs(:,7)<0.05);
    
    
    figure; hold on;
    scatter(earlypairs(:,8), earlypairs(:,9),'.k'); hold on;
    scatter(earlypairs(sigearlypairs_idx,8), earlypairs(sigearlypairs_idx,9),'.g'); hold on;
    [b,bint,r,rint,stats] = regress(earlypairs(:,9), [ones(size(earlypairs(:,8))) earlypairs(:,8)]);
    plot(min(earlypairs(:,8)):.1:max(earlypairs(:,8)), b(1)+b(2)*[min(earlypairs(:,8)):.1:max(earlypairs(:,8))])
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Early: r^2(%0.3f) p(%0.3f)', stats(1), stats(4))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    
    
    figure; hold on;
    scatter(latepairs(:,8), latepairs(:,9),'.k'); hold on;
    scatter(latepairs(siglatepairs_idx,8), latepairs(siglatepairs_idx,9),'.g'); hold on;
    [b,bint,r,rint,stats] = regress(latepairs(:,9), [ones(size(latepairs(:,8))) latepairs(:,8)]);
    plot(min(latepairs(:,8)):.1:max(latepairs(:,8)), b(1)+b(2)*[min(latepairs(:,8)):.1:max(latepairs(:,8))])
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Late: r^2(%0.3f) p(%0.3f)', stats(1), stats(4))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    
    
    % 1.) Entire population: find sig SWR correlated pairs, and then pos or neg correlated
    % ------------------------------------------------------------------------------------
    possigrippairs = matpairs(matpairs(:,7)<0.05 & matpairs(:,8) > 0,9);
    negsigrippairs = matpairs(matpairs(:,7)<0.05 & matpairs(:,8) < 0,9);
    nonsigrippairs = matpairs(matpairs(:,7)>0.05, 9);
    
    % Get sig SWR pair idxs
    sigrippairs_idx = find(matpairs(:,7)<0.05);
    
    figure; hold on;
    bar([mean(negsigrippairs) mean(nonsigrippairs) mean(possigrippairs)]); hold on;
    legend
    errorbar2([1 2 3], [mean(negsigrippairs) mean(nonsigrippairs) mean(possigrippairs)],  [stderr(negsigrippairs) stderr(nonsigrippairs) stderr(possigrippairs)] , 0.3, 'k')
    title('Spatial Corrln for SWR-correlated pair categories','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2 3]);
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    
    figfile = [figdir,'SpatialvsSWRcorr_AllBar_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    %stats
    [pKW table statsKW] = kruskalwallis([negsigrippairs' nonsigrippairs' possigrippairs'], [ones(1, length(negsigrippairs)) ones(1,length(nonsigrippairs)).*2 ones(1,length(possigrippairs)).*3]);
    %[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'off', 'alpha', 0.01); % change the alpha values to determine significance range.
    %c
    
    
    
    %*) SCATTER: ALL together
    figure; hold on;
    scatter(matpairs(:,8), matpairs(:,9),'.k'); hold on;
    scatter(matpairs(sigrippairs_idx,8), matpairs(sigrippairs_idx,9),'.g'); hold on;
    
    [b,bint,r,rint,stats] = regress(matpairs(:,9), [ones(size(matpairs(:,8))) matpairs(:,8)]);
    plot(min(matpairs(:,8)):.1:max(matpairs(:,8)), b(1)+b(2)*[min(matpairs(:,8)):.1:max(matpairs(:,8))])
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('All in Category: r^2(%0.3f) p(%0.3f)', stats(1), stats(4))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    figfile = [figdir,'SpatialvsSWRcorr_AllScatter_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    %     %3.) Excited vs Inh Significant SWR corr neurons
    matpairs_Exc = matpairs(Excind_inpairs,:);
    matpairs_Inh = matpairs(Inhind_inpairs,:);
    matpairs_Neu = matpairs(Neuind_inpairs,:);
    
    excsigrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05,9);
    inhsigrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 ,9);
    neusigrippairs = matpairs_Neu(matpairs_Neu(:,7)<0.05, 9);
    
    %4.) Excited vs Inh Significant +ve or -ve SWR corr neurons
    %
    excsigposrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05 & matpairs_Exc(:,8) > 0,9);
    excsignegrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05 & matpairs_Exc(:,8) < 0,9);
    excnonsigrippairs = matpairs_Exc(matpairs_Exc(:,7)>0.05,9);
    inhsigposrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 & matpairs_Inh(:,8) > 0,9);
    inhsignegrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 & matpairs_Inh(:,8) < 0,9);
    inhnonsigrippairs = matpairs_Inh(matpairs_Inh(:,7)>0.05,9);
    
    
    
    
    %7.) POS and NEG
    figure; hold on;
    bar([mean(inhsignegrippairs) mean(inhnonsigrippairs) mean(inhsigposrippairs) mean(excsignegrippairs) mean(excnonsigrippairs) mean(excsigposrippairs)]); hold on;
    legend
    errorbar2([1 2 3  4 5 6], [mean(inhsignegrippairs)  mean(inhnonsigrippairs) mean(inhsigposrippairs) mean(excsignegrippairs) mean(excnonsigrippairs) mean(excsigposrippairs)],...
        [stderr(inhsignegrippairs)  stderr(inhnonsigrippairs) stderr(inhsigposrippairs) stderr(excsignegrippairs) stderr(excnonsigrippairs) stderr(excsigposrippairs)] , 0.3, 'k')
    title('Spatial Corrln Separated by Pairs with Inh vs Exc PFC neurons','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[2 5],'XTickLabel',{'Inh';'Exc'});
    ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    
    figfile = [figdir,'SpatialvsSWRcorr_ExcInhBar_',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    %stats
    [pKW table statsKW] = kruskalwallis([inhsignegrippairs' inhnonsigrippairs' inhsigposrippairs' excsignegrippairs' excnonsigrippairs' excsigposrippairs'], [ones(1, length(inhsignegrippairs)) ones(1, length(inhnonsigrippairs))*2 ones(1, length(inhsigposrippairs)).*3 ...
        ones(1,length(excsignegrippairs)).*4 ones(1,length(excnonsigrippairs)).*5 ones(1,length(excsigposrippairs)).*6]);
    pKW
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.01); % change the alpha values to determine significance range.
    % c
    
    
    %*) Scatter: Exc vs Inh
    figure; hold on;
    subplot(1,2,1)
    scatter(matpairs_Exc(:,8), matpairs_Exc(:,9),'.k'); hold on;
    scatter(matpairs_Exc(excsigrippairs_idx,8), matpairs_Exc(excsigrippairs_idx,9),'.r'); hold on;
    
    [b,bint,r,rint,stats] = regress(matpairs_Exc(:,9), [ones(size(matpairs_Exc(:,8))) matpairs_Exc(:,8)]);
    plot(min(matpairs_Exc(:,8)):.1:max(matpairs_Exc(:,8)), b(1)+b(2)*[min(matpairs_Exc(:,8)):.1:max(matpairs_Exc(:,8))])
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Exc-PFC: r^2(%0.3f) p(%0.3f)', stats(1), stats(4))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    subplot(1,2,2)
    scatter(matpairs_Inh(:,8), matpairs_Inh(:,9),'.k'); hold on;
    scatter(matpairs_Inh(inhsigrippairs_idx,8), matpairs_Inh(inhsigrippairs_idx,9),'.b'); hold on;
    
    [b,bint,r,rint,stats] = regress(matpairs_Inh(:,9), [ones(size(matpairs_Inh(:,8))) matpairs_Inh(:,8)]);
    plot(min(matpairs_Inh(:,8)):.1:max(matpairs_Inh(:,8)), b(1)+b(2)*[min(matpairs_Inh(:,8)):.1:max(matpairs_Inh(:,8))])
    xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Inh-PFC: r^2(%0.3f) p(%0.3f)', stats(1), stats(4))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    
    figfile = [figdir,'SpatialvsSWRcorr_ExcInhScatter_color',area]
    if savefig1==1,
        print('-depsc2', figfile); print('-dpdf', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    %     % 2.) All Exc vs Inh vs Non-Modulated Neurons
    %     % -------------------------------------------
    %     % Get appropriate indices in matpairs. Can loop and use ismember, or just use intersect
    %
    %     matpairs_PFC = matpairs(:,[1 2 5 6]);
    %     % [~,Exc_ind,ib_exc] = intersect(matpairs_PFC,PFCindsExc,'rows');
    %     % [~,Inh_ind,ib_inh] = intersect(matpairs_PFC,PFCindsInh,'rows');
    %     % Mod_ind = [Exc_ind;Inh_ind];
    %     % Neu_ind=[1:length(matpairs_PFC)];
    %     % Neu_ind(Mod_ind)=[];
    %
    %     cntexccells=0; cntexcpairs=0; Excind_inpairs=[];
    %     for i=1:size(PFCindsExc,1)
    %         ind=[]; curridx=[];
    %         cntexccells = cntexccells+1;
    %         curridx = PFCindsExc(i,:);
    %         ind=find(ismember(matpairs_PFC,curridx,'rows'))';
    %
    %         cntexcpairs = cntexcpairs+length(ind);
    %         Excind_inpairs=[Excind_inpairs;ind'];
    %     end
    %     cntinhcells=0; cntinhpairs=0; Inhind_inpairs=[];
    %     for i=1:size(PFCindsInh,1)
    %         ind=[]; curridx=[];
    %         cntinhcells = cntinhcells+1;
    %         curridx = PFCindsInh(i,:);
    %         ind=find(ismember(matpairs_PFC,curridx,'rows'))';
    %
    %         cntinhpairs = cntinhpairs+length(ind);
    %         Inhind_inpairs=[Inhind_inpairs;ind'];
    %     end
    %
    %     Modind_inpairs = [Excind_inpairs;Inhind_inpairs];
    %     Neuind_inpairs = [1:length(matpairs_PFC)];
    %     Neuind_inpairs(Modind_inpairs) = [];
    %
    %     excrippairs = matpairs(Excind_inpairs,9);
    %     inhrippairs = matpairs(Inhind_inpairs,9);
    %     neurippairs = matpairs(Neuind_inpairs, 9);
    %
    % %     figure; hold on;
    % %     bar([mean(inhrippairs) mean(excrippairs) mean(neurippairs)]); hold on;
    % %     legend
    % %     errorbar2([1 2 3], [mean(inhrippairs) mean(excrippairs) mean(neurippairs)],  [stderr(inhrippairs) stderr(excrippairs) stderr(neurippairs)] , 0.3, 'k')
    % %     title('Spatial Corrln for Exc vs Inh AllSWRCorr pairs','FontSize',24,'FontWeight','normal');
    % %     set(gca,'XTick',[1 2 3]);
    % %     ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    % %
    % %     figfile = [figdir,'SpatialvsSWRcorr_CompExcInhNeuBar_',area]
    % %     if savefig1==1,
    % %         print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    % %     end
    % %
    % %     %stats
    % %     [pKW table statsKW] = kruskalwallis([inhrippairs' excrippairs' neurippairs'], [ones(1, length(inhrippairs)) ones(1,length(excrippairs)).*2 ones(1,length(neurippairs)).*3]);
    % %     pKW
    %     %[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.01); % change the alpha values to determine significance range.
    %     %c
    %
    % %     %3.) Excited vs Inh Significant SWR corr neurons
    %     matpairs_Exc = matpairs(Excind_inpairs,:);
    %     matpairs_Inh = matpairs(Inhind_inpairs,:);
    %     matpairs_Neu = matpairs(Neuind_inpairs,:);
    %
    %     excsigrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05,9);
    %     inhsigrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 ,9);
    %     neusigrippairs = matpairs_Neu(matpairs_Neu(:,7)<0.05, 9);
    % % %
    % %     figure; hold on;
    % %     bar([mean(inhsigrippairs) mean(excsigrippairs)  mean(neusigrippairs)]); hold on;
    % %     legend
    % %     errorbar2([1 2 3], [mean(inhsigrippairs) mean(excsigrippairs) mean(neusigrippairs)],  [stderr(inhsigrippairs) stderr(excsigrippairs) stderr(neusigrippairs)] , 0.3, 'k')
    % %
    % %     title('Spatial Corrln for Inh vs Exc Sig SWRCorr pairs','FontSize',24,'FontWeight','normal');
    % %     set(gca,'XTick',[1 2]);
    % %     ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    % %     %stats
    % %     [pKW table statsKW] = kruskalwallis([inhsigrippairs' excsigrippairs' neusigrippairs'], [ones(1, length(inhsigrippairs)) ones(1,length(excsigrippairs)).*2 ones(1,length(neusigrippairs)).*3]);
    % %     pKW
    % %
    %
    %
    %
    %
    % %     %4.) Excited vs Inh Significant +ve or -ve SWR corr neurons
    % %
    %     excsigposrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05 & matpairs_Exc(:,8) > 0,9);
    %     excsignegrippairs = matpairs_Exc(matpairs_Exc(:,7)<0.05 & matpairs_Exc(:,8) < 0,9);
    %     excnonsigrippairs = matpairs_Exc(matpairs_Exc(:,7)>0.05,9);
    %     inhsigposrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 & matpairs_Inh(:,8) > 0,9);
    %     inhsignegrippairs = matpairs_Inh(matpairs_Inh(:,7)<0.05 & matpairs_Inh(:,8) < 0,9);
    %     inhnonsigrippairs = matpairs_Inh(matpairs_Inh(:,7)>0.05,9);
    %     %Neusigrippairs = matpairs_Neu(matpairs_Neu(:,7)<0.05, 9);
    %
    %     % Get indices of sig SWR pairs for exc and inh separately
    %     excsigrippairs_idx = find(matpairs_Exc(:,7)<0.05);
    %     inhsigrippairs_idx = find(matpairs_Inh(:,7)<0.05);
    %
    % %
    % %     %5.) POS
    % %     figure; hold on;
    % %     bar([mean(inhsigposrippairs) mean(excsigposrippairs)]); hold on;
    % %     legend
    % %     errorbar2([1 2], [mean(inhsigposrippairs) mean(excsigposrippairs)],  [stderr(inhsigposrippairs) stderr(excsigposrippairs)] , 0.3, 'k')
    % %     title('Spatial Corrln for Inh vs Exc Sig POS SWRCorr pairs','FontSize',24,'FontWeight','normal');
    % %     set(gca,'XTick',[1 2]);
    % %     ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    % %     %stats
    % %     [pKW table statsKW] = kruskalwallis([inhsigposrippairs' excsigposrippairs'], [ones(1, length(inhsigposrippairs)) ones(1,length(excsigposrippairs)).*3]);
    % %     pKW
    % %
    % %     %6.) NEG
    % %     figure; hold on;
    % %     bar([mean(inhsignegrippairs) mean(excsignegrippairs)]); hold on;
    % %     legend
    % %     errorbar2([1 2], [mean(inhsignegrippairs) mean(excsignegrippairs)],  [stderr(inhsignegrippairs) stderr(excsignegrippairs)] , 0.3, 'k')
    % %     title('Spatial Corrln for Inh vs Exc Sig NEG SWRCorr pairs','FontSize',24,'FontWeight','normal');
    % %     set(gca,'XTick',[1 2]);
    % %     ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    % %     %stats
    % %     [pKW table statsKW] = kruskalwallis([inhsignegrippairs' excsignegrippairs'], [ones(1, length(inhsignegrippairs)) ones(1,length(excsignegrippairs)).*3]);
    % %     pKW
    % %
    %
    %     %7.) POS and NEG
    %     figure; hold on;
    %     bar([mean(inhsignegrippairs) mean(inhnonsigrippairs) mean(inhsigposrippairs) mean(excsignegrippairs) mean(excnonsigrippairs) mean(excsigposrippairs)]); hold on;
    %     legend
    %     errorbar2([1 2 3  4 5 6], [mean(inhsignegrippairs)  mean(inhnonsigrippairs) mean(inhsigposrippairs) mean(excsignegrippairs) mean(excnonsigrippairs) mean(excsigposrippairs)],...
    %         [stderr(inhsignegrippairs)  stderr(inhnonsigrippairs) stderr(inhsigposrippairs) stderr(excsignegrippairs) stderr(excnonsigrippairs) stderr(excsigposrippairs)] , 0.3, 'k')
    %     title('Spatial Corrln Separated by Pairs with Inh vs Exc PFC neurons','FontSize',24,'FontWeight','normal');
    %     set(gca,'XTick',[2 5],'XTickLabel',{'Inh';'Exc'});
    %     ylabel('Spatial Correlation','FontSize',24,'FontWeight','normal')
    %
    %     figfile = [figdir,'SpatialvsSWRcorr_ExcInhBar_',area]
    %     if savefig1==1,
    %         print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    %     end
    %
    %     %stats
    %     [pKW table statsKW] = kruskalwallis([inhsignegrippairs' inhnonsigrippairs' inhsigposrippairs' excsignegrippairs' excnonsigrippairs' excsigposrippairs'], [ones(1, length(inhsignegrippairs)) ones(1, length(inhnonsigrippairs))*2 ones(1, length(inhsigposrippairs)).*3 ...
    %         ones(1,length(excsignegrippairs)).*4 ones(1,length(excnonsigrippairs)).*5 ones(1,length(excsigposrippairs)).*6]);
    %     pKW
    %     [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.01); % change the alpha values to determine significance range.
    %     % c
    %
    %
    %     %*) Scatter: Exc vs Inh
    %     figure; hold on;
    %     subplot(1,2,1)
    %     scatter(matpairs_Exc(:,8), matpairs_Exc(:,9),'.k'); hold on;
    %     scatter(matpairs_Exc(excsigrippairs_idx,8), matpairs_Exc(excsigrippairs_idx,9),'.r'); hold on;
    %
    %     [b,bint,r,rint,stats] = regress(matpairs_Exc(:,9), [ones(size(matpairs_Exc(:,8))) matpairs_Exc(:,8)]);
    %     plot(min(matpairs_Exc(:,8)):.1:max(matpairs_Exc(:,8)), b(1)+b(2)*[min(matpairs_Exc(:,8)):.1:max(matpairs_Exc(:,8))])
    %     xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Exc-PFC: r^2(%0.3f) p(%0.3f)', stats(1), stats(4))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    %
    %     subplot(1,2,2)
    %     scatter(matpairs_Inh(:,8), matpairs_Inh(:,9),'.k'); hold on;
    %     scatter(matpairs_Inh(inhsigrippairs_idx,8), matpairs_Inh(inhsigrippairs_idx,9),'.b'); hold on;
    %
    %     [b,bint,r,rint,stats] = regress(matpairs_Inh(:,9), [ones(size(matpairs_Inh(:,8))) matpairs_Inh(:,8)]);
    %     plot(min(matpairs_Inh(:,8)):.1:max(matpairs_Inh(:,8)), b(1)+b(2)*[min(matpairs_Inh(:,8)):.1:max(matpairs_Inh(:,8))])
    %     xlabel('rip corr r'); ylabel('spat corr r'); title(sprintf('Inh-PFC: r^2(%0.3f) p(%0.3f)', stats(1), stats(4))); set(gcf,'PaperPositionMode','auto'); set(gcf, 'Position', [200 200 500 400])
    %
    %     figfile = [figdir,'SpatialvsSWRcorr_ExcInhScatter_color',area]
    %     if savefig1==1,
    %         print('-depsc2', figfile); print('-dpdf', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    %     end
    
end
