function plotNiceCorrGraphStraight2color(corMat,pMat,regionLabels,PFCind)
numPFCcells=sum(ismember(regionLabels,'PFC','rows'))
numCA1cells=sum(ismember(regionLabels,'CA1','rows'))
numCells=numPFCcells+numCA1cells;
scrsz = get(0,'ScreenSize');

%positions=[cos(0:2*pi/(numCells):2*pi)' sin(0:2*pi/(numCells):2*pi)'];
PFCpos=[ones(numPFCcells,1) 3*((1:numPFCcells)-(numPFCcells+1)/2)'];
CA1pos=[0*ones(numCA1cells,1) ((1:numCA1cells)-(numCA1cells+1)/2)'];
positions=[CA1pos;PFCpos];
uniqueLabels=(unique(regionLabels,'rows'));
numLabels=size(uniqueLabels,1);
curColors=[];
for d=1:numCells
    if strcmp(regionLabels(d),uniqueLabels(1))
        curColors=[curColors 'b'];
    elseif strcmp(regionLabels(d),uniqueLabels(2))
        curColors=[curColors 'm'];
    else
        curColors=[curColors 'g'];
        
    end
end

set(0,'DefaultFigurePaperPositionMode','manual')
fig=figure('PaperPosition',[0 0 3 5],'PaperSize',[6 8]);
set(gcf,'Position',[1 50 scrsz(3)/2 scrsz(4)/1.5])

%scrsz = get(0,'ScreenSize');
%figure('Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/1.5])
for i=1:numCells
    for j=1:i-1
        curVal=corMat(i,j);
        curP=pMat(i,j);
        if ~isnan(curVal)& curP>0.05
            plot([positions(i,1) positions(j,1)],[positions(i,2) positions(j,2)],'--','linewidth',1,'color','k');
            
            
        end
        hold on;
    end;
end
for i=1:numCells
    for j=1:i-1
        curVal=corMat(i,j);
        curP=pMat(i,j);
        scalingFactor=3;
        curValColorPos=[1 [1 1]*max((1-scalingFactor*curVal),0)];
        curValColorNeg=[[1 1]*max((1-scalingFactor*abs(curVal)),0) 1]; %[[1 1]*min((-scalingFactor*curVal),1) 1];
        if curVal>0 & curP<0.05
            plot([positions(i,1) positions(j,1)],[positions(i,2) positions(j,2)],'linewidth',4,'color','r');
        elseif curVal<0 & curP<0.05
            plot([positions(i,1) positions(j,1)],[positions(i,2) positions(j,2)],'linewidth',4,'color','b');
            %
        end
        hold on;
    end;
end
axis([-1 2 -1*(numCA1cells/2+2) (numCA1cells/2+4)])
text(-0.1, (numCA1cells/2)+2, 'CA1','color','k','fontsize',16)
text(1, (numCA1cells/2)+2, 'PFC','color','k','fontsize',16)

for i=1:numCA1cells
    % if i<10
    %    text(positions(i,1)-0.2, positions(i,2), [num2str(i) ' o'],'color','k','fontsize',15)
    text(positions(i,1)-0.1, positions(i,2), [' o'],'color','k','fontsize',15)
    
    %else
    %   text(positions(i,1)-0.245, positions(i,2), [num2str(i) ' o'],'color','k','fontsize',15)
    
    %end
end
if ~isempty(PFCind)
    counter=1+numCA1cells;
    
    text(positions(counter,1)+0.05, positions(counter,2), ' o','color','k','fontsize',15)
else
    for i=1:numPFCcells
        counter=i+numCA1cells;
        %text(positions(counter,1)+0.2, positions(counter,2), [' o' num2str(i)],'color','k','fontsize',15)
        text(positions(counter,1)+0.05, positions(counter,2), [' o' ],'color','k','fontsize',15)
        
    end
end
% plot([1.2 1.2],[-1*(numCA1cells/2) -1*(numCA1cells/2)+2],'linewidth',abs(40*0.1),'color','k')
% text(1.3, -1*(numCA1cells/2)+1,'0.1','fontsize',20)
axis off

% separate plots for each PFC cell

for i=(numCA1cells+1):numCells

fig=figure('PaperPosition',[0 0 3 5],'PaperSize',[6 8]);
set(gcf,'Position',[1 50 scrsz(3)/2 scrsz(4)/1.5])    
for j=1:i-1
        curVal=corMat(i,j);
        curP=pMat(i,j);
        if ~isnan(curVal)& curP>0.05
            plot([positions(i,1) positions(j,1)],[positions(i,2) positions(j,2)],'--','linewidth',1,'color','k');
            
            
        end
        hold on;
    end;
    

    for j=1:i-1
        curVal=corMat(i,j);
          scalingFactor=3;
        curValColorPos=[1 [1 1]*max((1-scalingFactor*curVal),0)];
        curValColorNeg=[[1 1]*max((1-scalingFactor*abs(curVal)),0) 1]; %[[1 1]*min((-scalingFactor*curVal),1) 1];
        curP=pMat(i,j);
        % abs(40*corMat(i,j))
        if curVal>0 & curP<0.05
            plot([positions(i,1) positions(j,1)],[positions(i,2) positions(j,2)],'linewidth',4,'color','r');
        elseif curVal<0 & curP<0.05
            plot([positions(i,1) positions(j,1)],[positions(i,2) positions(j,2)],'linewidth',4,'color','b');
            %
        end
        hold on;
    end;
    
    axis([-1 2 -1*(numCA1cells/2+2) (numCA1cells/2+4)])
    text(-0.1, (numCA1cells/2)+2, 'CA1','color','k','fontsize',16)
    text(1, (numCA1cells/2)+2, 'PFC','color','k','fontsize',16)
    
    for ii=1:numCA1cells
        text(positions(ii,1)-0.1, positions(ii,2), [' o'],'color','k','fontsize',15)
    end
    
    text(positions(i,1)+0.05, positions(i,2), ' o','color','k','fontsize',15)
   
%     plot([1.2 1.2],[-1*(numCA1cells/2) -1*(numCA1cells/2)+2],'linewidth',abs(40*0.1),'color','k')
%     text(1.3, -1*(numCA1cells/2)+1,'0.1','fontsize',20)
    axis off
end
