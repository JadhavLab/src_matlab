

runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells
plotGraphs=1;
%savedir = 'mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';

%savedir = '/opt/data15/gideon/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPexpt/HP_ProcessedData/';


%savedir = '/data15/gideon/ProcessedData/';
%val=1; savefile = [savedir 'HP_allspeedmod_noFS']; area = 'PFC'; clr = 'b'; % PFC

%val=2; savefile = [savedir 'HP_speedmodripexc_noFS']; area = 'PFC'; clr = 'b'; % PFC
val=3; savefile = [savedir 'HP_speedmodripinh_noFS']; area = 'PFC'; clr = 'b'; % PFC
%val=4; savefile = [savedir 'HP_speedmodripunmod_noFS']; area = 'PFC'; clr = 'b'; % PFC


savefig1=0;


% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Nadal','Rosenthal','Borg'};
    
    % animals = {'Borg'};
    
    %   animals = {'Nadal'};
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    % dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    % runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    %sleepepochfilter = 'isequal($type, ''sleep'')'; % Only pre and post sleep marked as sleep
    runepochfilter = 'isequal($type, ''run'')';
    
    % %Cell filter
    % %-----------
    % %PFC
    % %----
    % && strcmp($thetamodtag, ''y'')
    switch val
        case 1
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n''))';
        case 2
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc'') && strcmp($FStag, ''n''))';
        case 3
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh'') && strcmp($FStag, ''n''))';
        case 4
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''n'') && strcmp($FStag, ''n''))';
            
    end
    
    % Time filter - none.
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % Use absvel instead of linearvel
    timefilter_place_new = { {'DFTFsj_getvelpos', '(($absvel >= 0))'},...
        {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',3} };
    
    % Iterator
    % --------
    iterator = 'singlecellanal';
    
    % Filter creation
    % ----------------
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter,  'excludetime', timefilter_place_new, 'iterator', iterator);
    
    %     modg = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
    %         cellfilter, 'iterator', iterator);
    
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    
    % % Need spikes to get time series
    % % If it is across regions, you will need cellinfo to get area where cells are recorded from
    %  modg = setfilterfunction(modg,'DFAsj_glm_ripalign_dataForTheta',{'ripplemod','cellinfo'}); %
    modf = setfilterfunction(modf,'DFAgr_thetaSpikingVsSpeed',{'spikes','cellinfo','pos'},'thrstime',1); % With includetime condition
    
    
    % Run analysis
    % ------------
    modf = runfilter(modf);
    %  modg = runfilter(modg);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------

% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
switch val
    case 1
        gatherdatafile = [savedir 'HP_allspeedmod_gather_noFS'];
    case 2
        gatherdatafile = [savedir 'HP_speedmodripexc_gather_noFS'];
    case 3
        gatherdatafile = [savedir 'HP_speedmodripinh_gather_noFS'];
    case 4
        gatherdatafile = [savedir 'HP_speedmodripunmod_gather_noFS'];
        
end

allfits=[];
allspeedspikingstruct={};
if gatherdata
    
    allrs=[];allps=[];allinds=[];
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            indNoAnim1=modf(an).output{1}(i).indices;
            indwanim=[an indNoAnim1];
            allinds=[allinds;indwanim];
            allrs = [allrs modf(an).output{1}(i).FR_speed_corr_r];
            allps = [allps modf(an).output{1}(i).FR_speed_corr_p];
            prevsize=length(allspeedspikingstruct);
            allspeedspikingstruct(prevsize+1).ind=indwanim;
            allspeedspikingstruct(prevsize+1).speeds=modf(an).output{1}(i).speeds;
            allspeedspikingstruct(prevsize+1).spiking=modf(an).output{1}(i).spiking;
        end
    end
    
    
    
    
    % combining across epochs
    allspeedspikingstructEpCombined={};
    Uinds=unique(allinds(:,[1 2 4 5]),'rows');
    for i=1:length(Uinds)
        allepinds=find(ismember(allinds(:,[1 2 4 5]),Uinds(i,:),'rows'));
        tmpspeeds=[];
        tmpspiking=[];
        for j=1:length(allepinds)
            tmpspeeds=[tmpspeeds;allspeedspikingstruct(allepinds(j)).speeds];
            tmpspiking=[tmpspiking;allspeedspikingstruct(allepinds(j)).spiking];
            
        end
        prevsize2=length(allspeedspikingstructEpCombined);
        allspeedspikingstructEpCombined(prevsize2+1).ind=Uinds(i,:);
        allspeedspikingstructEpCombined(prevsize2+1).speeds=tmpspeeds;
        allspeedspikingstructEpCombined(prevsize2+1).spiking=tmpspiking;
    end
    
    allmobileFRs=[];
    allimmobileFRs=[];
    allrsepcomb=[];allpsepcomb=[]; 
    
    for i=1:length(allspeedspikingstructEpCombined)
        curspeeds=allspeedspikingstructEpCombined(i).speeds;
        curspiking=allspeedspikingstructEpCombined(i).spiking;
        [rep pep]=corrcoef(curspeeds,curspiking);
        immobileFR=nanmean(curspiking(curspeeds<5));
        mobileFR=nanmean(curspiking(curspeeds>5));
        
        pp=polyfit(curspeeds,curspiking,1);
        %xfit=[0:35];
        xfit=[-10:35];
        y1=polyval(pp,xfit);
        
        allrsepcomb=[allrsepcomb rep(1,2)];
        allpsepcomb=[allpsepcomb pep(1,2)];
        allfits=[allfits;y1];
        allimmobileFRs=[allimmobileFRs; immobileFR];
        allmobileFRs=[allmobileFRs; mobileFR];
        
        % Plot example
        
        % For Exc
        if rep(1,2) >= 0.2 % For Exc  (<0.2, for Inh) Need to histogram, like replay example. speedbins ([0:10:40])    
            
            spbins = [0:8:40];
            
            spkm=[]; spke=[];
            for i=1:length(spbins)-1
                idx = find(curspeeds>=spbins(i) & curspeeds<spbins(i+1));
                spkm(i) = nanmean(curspiking(idx)); spke(i) = sem(curspiking(idx));
            end
   
            figure; hold on;
            %plot(curspeeds, curspiking,'r.','MarkerSize',8)
            errorbar(spbins(1:end-1), spkm,spke,'ro', 'LineWidth',1);
            plot(xfit, y1, 'k', 'LineWidth',1);
            ylabel('Number of spikes')
            xlabel('Speed (cm/s)')
            title(['r = ' num2str(roundn(rep(1,2)))])
            
            keyboard;        
            figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/SpeedMod/';
            figfile = [figdir,'PFC_SpeedMod_PosCorrEg2']
            if savefig1==1,
                print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
            end   
        end
        
        % For INH
%         if rep(1,2) <= -0.2 % For INH  (<0.2, for Inh) Need to histogram, like replay example. speedbins ([0:10:40])    
%             
%             spbins = [0:8:40];
%             
%             spkm=[]; spke=[];
%             for i=1:length(spbins)-1
%                 idx = find(curspeeds>=spbins(i) & curspeeds<spbins(i+1));
%                 spkm(i) = nanmean(curspiking(idx)); spke(i) = sem(curspiking(idx));
%             end
%    
%             figure; hold on;
%             %plot(curspeeds, curspiking,'r.','MarkerSize',8)
%             errorbar(spbins(1:end-1), spkm,spke,'bo', 'LineWidth',1);
%             plot(xfit, y1, 'k', 'LineWidth',1);
%             ylabel('Number of spikes')
%             xlabel('Speed (cm/s)')
%             title(['r = ' num2str(roundn(rep(1,2)))])
%             
%             keyboard;        
%             figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/SpeedMod/';
%             figfile = [figdir,'PFC_SpeedMod_NegCorrEg']
%             if savefig1==1,
%                 print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
%             end   
%         end
        
    end
    
    save(gatherdatafile);
    
    % not gathering
else
    
    figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/SpeedMod/';

    
    
    load([savedir 'HP_speedmodripinh_gather_noFS']);
    allrsInh=allrsepcomb;
    allpsInh=allpsepcomb;
    allfitsInh=allfits;
    allmobileFRsInh=allmobileFRs;
    allimmobileFRsInh=allimmobileFRs;
    
    load([savedir 'HP_speedmodripexc_gather_noFS']);
    allrsExc=allrsepcomb;
    allpsExc=allpsepcomb;
    allfitsExc=allfits;
    allmobileFRsExc=allmobileFRs;
    allimmobileFRsExc=allimmobileFRs;
    load([savedir 'HP_speedmodripunmod_gather_noFS']);
    allrsripunmod=allrsepcomb;
    allpsripunmod=allpsepcomb;
    allfitsripunmod=allfits;
    [h p]=ttest2(allrsExc,allrsInh);
    
    binsize=0.04;
    
    [exc1 exc2]=hist(allrsExc,[-0.4:binsize:0.4]);
    [inh1 inh2]=hist(allrsInh,[-0.4:binsize:0.4]);
    [RU1 RU2]=hist(allrsripunmod,[-0.4:binsize:0.4]);
    
    p1 = ranksum(allrsExc, allrsInh),  %0.02 orig  // 0.01 noFS
    p2 = ranksum(allrsExc, allrsripunmod), %0.3 orig // 0.18 noFS
    p3 = ranksum(allrsripunmod, allrsInh), %0.07 orig // 0.05 noFS
    
    figure; hold on;
    %subplot(2,1,1)
    hold on;plot(exc2,exc1/sum(exc1),'r','linewidth',2);
    plot(inh2,inh1/sum(inh1),'b','linewidth',2);
    plot(RU2,RU1/sum(RU1),'c','linewidth',2);
    
    %legend({'Rip-exc','Rip-inh','Rip-unmod'})
    xlabel('Firing rate-speed correlation r value')
    ylabel('Fraction of PFC cells')
    title(['NExc= ' num2str(length(allrsExc)) ' NInh= ' num2str(length(allrsInh)) ' Nunmod= ' num2str(length(allrsripunmod)) ', P= ' num2str(p)])
    
    yaxis = 0:0.1:0.2;
    plot( median(allrsExc)*ones(size(yaxis)), yaxis,'r-','LineWidth',4);
    plot( median(allrsInh)*ones(size(yaxis)), yaxis,'b-','LineWidth',4);
    plot( median(allrsripunmod)*ones(size(yaxis)), yaxis,'c-','LineWidth',4);


    % Is Distribution different from 0?
    % ---------------------------------
    % Get normal ditr with same st dev as your distr
    stdev = std(allrsInh); n = length(allrsInh);
    dist = normrnd(0,stdev,n,1); % or dist = randn(n,1) + stdev;
    [dist1, dist2] = hist(dist,[-0.4:binsize:0.4]);
    %figure; hold on; 
    %plot(inh2,inh1/sum(inh1),'b','linewidth',2);
    %plot(dist2,dist1/sum(dist1),'k','linewidth',2);
    
    % Get random distr with same st dev as your distr 100 times, and check median
    mediandist=[];
    for s = 1:1000
        dist = normrnd(0,stdev,n,1); 
        mediandist(s) = median(dist);   
    end
    figure; hold on; hist(mediandist);
    pval_inh = length(find(median(allrsInh)>mediandist))./1000, % p = 0.013  
    %pval_inh = length(find(abs(median(allrsInh))<abs(mediandist)))./1000; one-sided p=0.037
    
    % Same for exc
    mediandist=[]; stdev = std(allrsExc); n = length(allrsExc);
    for s = 1:1000
        dist = normrnd(0,stdev,n,1); 
        mediandist(s) = median(dist);   
    end
    figure; hold on; hist(mediandist);
    pval_exc = length(find(median(allrsExc)<mediandist))./1000, % p = 0.26   
    
    % Same for neu
    mediandist=[]; stdev = std(allrsripunmod); n = length(allrsripunmod);
    for s = 1:1000
        dist = normrnd(0,stdev,n,1); 
        mediandist(s) = median(dist);   
    end
    figure; hold on; hist(mediandist);
    pval_neu = length(find(median(allrsripunmod)>mediandist))./1000, % p = 0.54/0.45   
    
    
%     % Mix 2 distr, and get shuffle, and get "n" values, get median of ditr
%     % Get 1000 such medians from mixed, shuffled poplns.
%     % Is real median > 99% of that?
%     mix = [allrsInh';dist]; mediandist=[];
%     for s = 1:1000
%         rorder = randperm(2*n);
%         mixdist = mix(rorder(1:n));
%         mediandist(s) = median(mixdist);   
%     end
%     figure; hold on; hist(mediandist);
%     pval = length(find(median(allrsInh)<=mediandist))./n;
    
        
    figfile = [figdir,'PFC_SpeedMod_AllDistr_withmedian']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    
    % with non-ripmod cells
    
    [hUM1 pUM1]=ttest2(allrsExc,allrsripunmod);
    [hUM2 pUM2]=ttest2(allrsInh,allrsripunmod);
    
    allrsExcsig = allrsExc(allpsExc<0.05);
    allrsInhsig = allrsInh(allpsInh<0.05);
    allrsripunmodsig = allrsripunmod(allpsripunmod<0.05);
    
    
    [hsig psig]=ttest2(allrsExc(allpsExc<0.05),allrsInh(allpsInh<0.05));
    [exc1sig exc2sig]=hist(allrsExc(allpsExc<0.05),[-0.4:binsize:0.4]);
    [inh1sig inh2sig]=hist(allrsInh(allpsInh<0.05),[-0.4:binsize:0.4]);
    [RU1sig RU2sig]=hist(allrsripunmod(allpsripunmod<0.05),[-0.4:binsize:0.4]);
    
    figure; hold on;
    %subplot(2,1,2)
    hold on;plot(exc2sig,exc1sig/sum(exc1sig),'r','linewidth',2);
    plot(inh2sig,inh1sig/sum(inh1sig),'b','linewidth',2);
    plot(RU2sig,RU1sig/sum(RU1sig),'c','linewidth',2);
    
    %legend({'Rip-exc','Rip-inh','Rip-unmod'})
    xlabel('Firing rate-speed correlation r value')
    ylabel('Fraction of PFC cells')
    title(['ONLY SIG Rs, NExcSig= ' num2str(sum(allpsExc<0.05)) ' NInhSig= ' num2str(sum(allpsInh<0.05)) ' NripunmodSig= ' num2str(sum(allpsripunmod<0.05)) ', P= ' num2str(psig)])
    
    
    figfile = [figdir,'PFC_SpeedMod_SigDistr']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    p1sig = ranksum(allrsExcsig, allrsInhsig),  %p= noFS 0.01
    p2sig = ranksum(allrsExcsig, allrsripunmodsig), %p= noFS  0.17
    p3sig = ranksum(allrsripunmodsig, allrsInhsig), %p= noFS  0.16
    
    %%
    figure; hold on;
    bar(1, nanmean(allrsExc),'r');
    bar(2, nanmean(allrsInh),'b');
    bar(3, nanmean(allrsripunmod),'c');
    %legend({'Exc-Exc','Inh-Inh','Exc-Inh'})
    errorbar2(1, nanmean(allrsExc), nanstderr(allrsExc), 0.3, 'r');
    errorbar2(2, nanmean(allrsInh), nanstderr(allrsInh), 0.3, 'b');
    errorbar2(3, nanmean(allrsripunmod), nanstderr(allrsripunmod), 0.3, 'c');
    title('All cells','FontSize',24,'FontWeight','normal');
    ylabel('Spiking/speed correlation R value','FontSize',24,'FontWeight','normal')
    
    figfile = [figdir,'PFC_SpeedMod_AllMean']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    %bar([1,2],[nanmean(allrsExc) nanmean(allrsInh)],'w','linewidth',2)
    %hold on;
    %errorbar([1,2],[nanmean(allrsExc) nanmean(allrsInh)],[nanstd(allrsExc)/sqrt(length(allrsExc)) nanstd(allrsInh)/sqrt(length(allrsInh))],'.k','linewidth',2)
    %set(gca, 'xticklabel',{'SWR-excited','SWR-inhibited'})
    %ylabel('Spiking/speed correlation R value')
    %title('All cells')
    
    %%
    
    
    figure; hold on;
    bar(1, nanmean(allrsExcsig),'r');
    bar(2, nanmean(allrsInhsig),'b');
    bar(3, nanmean(allrsripunmodsig),'c');
    %legend({'Exc-Exc','Inh-Inh','Exc-Inh'})
    errorbar2(1, nanmean(allrsExcsig), nanstderr(allrsExcsig), 0.3, 'r');
    errorbar2(2, nanmean(allrsInhsig), nanstderr(allrsInhsig), 0.3, 'b');
    errorbar2(3, nanmean(allrsripunmodsig), nanstderr(allrsripunmodsig), 0.3, 'c');
    title('Significantly speed modulated cells','FontSize',24,'FontWeight','normal');
    ylabel('Spiking/speed correlation R value','FontSize',24,'FontWeight','normal')

    
    figfile = [figdir,'PFC_SpeedMod_SigMean']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    
    %bar([1,2],[nanmean(allrsExc(allpsExc<0.05)) nanmean(allrsInh(allpsInh<0.05))],'w','linewidth',2)
    %hold on;
    %errorbar([1,2],[nanmean(allrsExc(allpsExc<0.05)) nanmean(allrsInh(allpsInh<0.05))],[nanstd(allrsExc(allpsExc<0.05))/sqrt(length(allrsExc(allpsExc<0.05))) nanstd(allrsInh(allpsInh<0.05))/sqrt(length(allrsInh(allpsInh<0.05)))],'.k','linewidth',2)
    %set(gca, 'xticklabel',{'SWR-excited','SWR-inhibited'})
    %ylabel('Spiking/speed correlation R value')
    %title('Significantly speed modulated cells')
    
    % these do not come out as significant
    % v=[allrsExc allrsInh allrsripunmod];
    % vg=[1*ones(1,length(allrsExc)) 2*ones(1,length(allrsInh)) 3*ones(1,length(allrsripunmod))];
    % [pa tablea statsa]=anova1(v,vg,'displayopt','off')
    % multcompare(statsa)
    %
    % v=[allrsExc(allpsExc<0.05) allrsInh(allpsInh<0.05) allrsripunmod(allpsripunmod<0.05)];
    % vg=[1*ones(1,sum(allpsExc<0.05)) 2*ones(1,sum(allpsInh<0.05)) 3*ones(1,sum(allpsripunmod<0.05))];
    % [pas tableas statsas]=anova1(v,vg,'displayopt','off')
    % multcompare(statsas)
end
%%
% [excS1 excS2]=sort(allfitsExc(:,1))
% [inhS1 inhS2]=sort(allfitsInh(:,1))
%
% figure;
% subplot(1,2,1)
% imagesc(allfitsExc(excS2,:));caxis([-2 10]);
% title('rip exc')
% subplot(1,2,2)
% imagesc(allfitsInh(inhS2,:));caxis([-2 10]);
% title('rip inh')


