function out = DFAsj_riphighspeedFR(index, excludetimes, spikes, cellinfo,pos,ripplemod, varargin)

% Starting with core of DFAgr_thetaSpikingVsSpeed
% Use "DFAsj_getripmoddata4" as an example to read "ripplemod" and get pre=SWR firing rates
% Next, need high speed firing rates. DFAsj_getripalignspiking: has speed condn for detecting SWRs
% Instead, use this core code to look into time bins (100ms? 200ms?) and corresponding firing rats.
% The thrstime condn is to look only at data that is at least 1 sec long. This was kept for 
% DFSsj_HPexpt_speedmod, and also for ThetaCov and for GLM code
% GLM model for theta times. Similar to DFAsj_glm_ripalign.m 
% Get the time series with theta times similar to the beginnin of the 
% function"DFAsj_getthetacrosscov_timecondition.m" 


tetfilter = '';
% gideon
%excludetimes = [];
thrstime = 1; % Minimum length of includetime segments in sec
binsize = 0.2; % 200ms bins?
highspeedthrs = 10; % 5 or 10 cm/sec


%acrossregions = 0;

% lowsp_thrs = 5; %cm/sec
% highsp_thrs = lowsp_thrs;
% dospeed = 0;


for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'thrstime'
            thrstime = varargin{option+1};  
        case 'binsize'
            binsize = varargin{option+1};
        case 'highspeedthrs'
            highspeedthrs = varargin{option+1};
        case 'acrossregions'
            acrossregions = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

day = index(1); epoch = index(2);






% --------------------------
% A. High Speed Firing Rate
% --------------------------


% % THIS IS NOT VALID FOR SINGLECELL ANAL First, get cell indices for CA1 and PFC
% % ----------------------------------------
% day = indices(1,1);
% epoch = indices(1,2);
% cellsi=[]; usecellsi=0; % CA1 cells
% cellsp=[]; usecellsp=0; % PFC cells
% totalcells = size(indices,1);
% for i=1:totalcells
%     currarea = cellinfo{indices(i,1)}{indices(i,2)}{indices(i,3)}{indices(i,4)}.area;
%     if strcmp(currarea,'PFC'),
%         cellsp = [cellsp; day epoch indices(i,3) indices(i,4)];
%         usecellsp = usecellsp+1;
% %     else
% %         cellsi = [cellsi; day epoch indices(i,3) indices(i,4)];
% %         usecellsi = usecellsi+1;
%     end
% end
% % nCA1cells = size(cellsi,1); 
% nPFCcells = size(cellsp,1);
% ind = cellsp(1,:); % day.ep.tet.cell for 1st CA1 cell 
% ttemp = spikes{ind(1)}{ind(2)}{ind(3)}{ind(4)}.data(:,1);
    

% Get epochtimes, and implement the timecondition
% ------------------------------------------------

ind = index;
day = index(1); epoch = index(2);
spikesp = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);

totaleptime = diff(spikes{ind(1)}{ind(2)}{ind(3)}{ind(4)}.timerange)./10000; % in secs
excltime = sum(diff(excludetimes'));
% -----------------------------
% 0. FIRING RATE OF CELL IN EPOCH
% -----------------------------

cellinfoFR = cellinfo{index(1)}{index(2)}{index(3)}{index(4)}.meanrate;
cellFR = length(spikesp)./totaleptime;



% Use Include periods
% --------------------------
% Get IncludeTimes from flanking edges in excludetimes and epoch start-end
epstend = spikes{ind(1)}{ind(2)}{ind(3)}{ind(4)}.timerange./10000;
incl=[];
incl(:,1) = excludetimes(1:end-1,2);
incl(:,2) = excludetimes(2:end,1);
incl = [epstend(1),incl(1,1) ;incl];
incl = [incl; incl(end,2),epstend(2)];

% Length of include periods
incl_lths = diff(incl')';
% Discard anything < thrstime
discard = find(incl_lths<thrstime);
incl(discard,:)=[];

% No Need to go over loop. Only 1 cell
% -----------------------------------
spikespinc{1}= spikesp(find(isIncluded(spikesp,incl)));
% for i=1:size(cellsp,1)
%     i;
%     eval(['spikesp{',num2str(i),'}= spikes{cellsp(',num2str(i),',1)}{cellsp(',num2str(i),',2)}'...
%         '{cellsp(',num2str(i),',3)}{cellsp(',num2str(i),',4)}.data(:,1);']);
%  % gideon added, verify with shantanu
%     eval(['spikespinc{',num2str(i),'}= spikesp{',num2str(i),'}(find(isIncluded(spikesp{',num2str(i),'},incl)));'])
% end

% Going over all incl intervals, and from each interval extracting bin
% times of 200ms going from the interval start, ending >=200ms from its end
% thetaBins will hold the start times of all 200ms bins
thetaBins=[];
thetaBinSize=0.2;
for ii=1:length(incl),
    curIntervalSize=incl(ii,2)-incl(ii,1);
    numCurBins=floor(curIntervalSize/thetaBinSize);
    curBins=incl(ii,1):0.2:(incl(ii,1)+0.2*(numCurBins-1));
    thetaBins=[thetaBins curBins];
end

spikeBinsPFC=[];

% associate speeds to theta bins
sptimes=pos{ind(1)}{ind(2)}.data(:,1);
sp=pos{ind(1)}{ind(2)}.data(:,5);
meanSpeedBins=[];
speedsToBins=lookup(sptimes,thetaBins,-1);
for jj=1:length(thetaBins)
    meanSpeedBins=[meanSpeedBins nanmean(sp(speedsToBins==jj))];
end
meanSpeedBins=meanSpeedBins';

nPFCcells=1;
for k=1:nPFCcells
    
    spikesToBins=lookup(spikespinc{k},thetaBins,-1);
    tabulateSpikes=tabulate(spikesToBins);
    spikesInBins=NaN(1,length(thetaBins));
    if ~isempty(tabulateSpikes)
        spikesInBins(1:length(tabulateSpikes))=tabulateSpikes(:,2);
    end
    spikeBinsPFC=[spikeBinsPFC spikesInBins'];
end

meanSpeedBinsNoNAN=meanSpeedBins(~isnan(spikeBinsPFC)&~isnan(meanSpeedBins));
spikeBinsPFCNoNAN=spikeBinsPFC(~isnan(spikeBinsPFC)&~isnan(meanSpeedBins));
[r, p]=corrcoef(meanSpeedBinsNoNAN,spikeBinsPFCNoNAN);

% Find Only bins > highspeedthrs
highspeedbins = find (meanSpeedBinsNoNAN>=highspeedthrs);
highspeedfr_bins = spikeBinsPFCNoNAN(highspeedbins);
if ~isempty(highspeedfr_bins) || ~isnan(highspeedfr_bins)
    meanspks_bins = nanmean(highspeedfr_bins);
    highspeedFR = meanspks_bins./binsize; % FR in spikes/sec 
else
    highspeedFR = nan;
end


% --------------------------
% B. Get pre-SWR Firing Rate
% --------------------------

trialResps = ripplemod{index(1)}{index(2)}{index(3)}{index(4)}.trialResps;  % 0 to 200ms
trialResps_bck = ripplemod{index(1)}{index(2)}{index(3)}{index(4)}.trialResps_bck; %-500 to -100 ms
raster = ripplemod{index(1)}{index(2)}{index(3)}{index(4)}.raster;

bcklth = 0.2; %bck window is 200 ms or 0.1 sec long [-500 to -300ms]
resplth = 0.2; %resp window is 200 ms or 0.2 sec long
if ~isempty(trialResps_bck)
    preripFR2 = nanmean(trialResps_bck)./bcklth;
else
    preripFR2 = nan;
end

% Alterntative way for prerip FR - directly from raster
rasthist = rast2mat(raster);
bckhist = sum(rasthist(:,1:400),2);% 400ms long bckwindow (-500 to -100 ms): 0.4sec
if ~isempty(bckhist)
    preripFR = nanmean(bckhist)./0.4;
else
    preripFR=nan;
end

if ~isempty(trialResps_bck)
    ripFR = nanmean(trialResps)./resplth;
else
    ripFR = [];
end


% --------------------
% ALSO GET MODULATION
% -------------------
FRmodln = (preripFR - highspeedFR)./(preripFR + highspeedFR); 
FRmodln2 = (preripFR2 - highspeedFR)./(preripFR2 + highspeedFR);
% varies betn -1 and 1. Positive values = preripFR > highspeedFR, and vice versa



% % ------ 
% % Output
% % ------
out.index = index;

out.preripFR = preripFR;
out.highspeedFR = highspeedFR;
out.FRmodln = FRmodln; 

out.preripFR2 = preripFR2;
out.FRmodln2 = FRmodln2;

out.cellinfoFR = cellinfoFR;
out.cellFR = cellFR;
out.ripFR = ripFR;

out.speeds=meanSpeedBinsNoNAN;
out.spiking=spikeBinsPFCNoNAN;
out.FR_speed_corr_r=r(1,2);
out.FR_speed_corr_p=p(1,2);
if isnan(r(1,2))
    keyboard
end


