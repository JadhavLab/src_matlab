function out = DFAsj_get_rewpos_ripspeedFR_alignspiking(index, excludetimes, spikes, pos, linpos, trajinfo,cellinfo,ripplemod, varargin)

% get reward data from trajinfo (=posn), and align spikes to it.
% Need trajinfo for each day. This is because DIO may have a problem

% out = DFAsj_getripalignspiking(spike_index, excludeperiods, spikes, ripples, tetinfo, options)


% for reward
thrsdist=10; % cm
thrsspeed=2;% cm/sec
%excludetimes = [];
figopt=0;

% for high speed
thrstime = 1; % Minimum length of includetime segments in sec
binsize = 0.2; % 200ms bins?
highspeedthrs = 10; % 5 or 10 cm/sec

% -------------------
% 1. Reward calculations
% -------------------

% For ripple trigger
% ------------------
binsize = 100; % ms
pret=5050; postt=5050; %% Times to plot
push = 500; % either bwin(2) or postt-trim=500. For jittered trigger in background window
trim = 50;
smwin=100; %Smoothing Window - along y-axis for matrix. Carry over from ...getrip4
plotline1 = 100;

if isempty(binsize)
    binsize = 100;  %% ms
end
binsize_plot=binsize; %% If you put binsize_plot=1000, then units are Nspikes/binsize, not inst. firing rate in Hz
timeaxis = -pret:binsize:postt;

% nstd=1: gaussian of length 4. nstd = 2: gaussian of length 7, nstd=3: gaussian of length 10.
nstd = round(binsize*2/binsize); g1 = gaussian(nstd, 3*nstd+1);
% Smooth over binsize*3 ms (30ms for 10ms; 15ms for 5 ms)
%nstd = round(binsize*3/binsize); g1 = gaussian(nstd, 5*nstd+1);
%nstd = round(binsize*2/binsize); g1 = gaussian(nstd, 1*nstd+1);

rwin = [0 2000]; resplth_rew=2;    % resplth = 2secs 
bwin = [-3000 -1000]; bcklth_rew=2; % bcklth = 2secs
push = 500; % either bwin(2) or postt-trim=500. If doing random events. See ... getrip4


for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        % Reward
        case 'thrsdist'
            thrsdist = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'thrsspeed'
            thrsspeed = varargin{option+1};
        % High speed FR
        case 'thrstime'
            thrstime = varargin{option+1};  
        case 'binsize'
            binsize = varargin{option+1};
        case 'highspeedthrs'
            highspeedthrs = varargin{option+1};
        case 'acrossregions'
            acrossregions = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

day = index(1);
epoch = index(2);

% Get reward times
% ----------------

% Posn data
postime = pos{day}{epoch}.data(:,(1));
posn = pos{day}{epoch}.data(:,2:3);
posx = posn(:,1); posy = posn(:,2);
speed = abs(pos{day}{epoch}.data(:,5));

% Record trajtime for our particular epoch
trajinfo_de = trajinfo{day}{epoch};
if isempty(trajinfo_de)
    trajtime=[];
    %continue;
else
    trajbound = trajinfo{day}{epoch}.trajbound;
    rewarded = trajinfo{day}{epoch}.rewarded;
    trajtime = trajinfo{day}{epoch}.trajtime;
    wellstend = trajinfo{day}{epoch}.wellstend;
end

% Only proceed if reward triggers exist
allrewtime=[]; allnorewtime=[];
allrew_spks=[]; allnorew_spks=[];
allrew_spkshist=[]; allnorew_spkshist=[];
trialResps_rew=[]; trialResps_bck_rew=[];
trialResps_norew=[]; trialResps_bck_norew=[];
rewFR = []; rewbckFR =[];
norewFR = []; norewbckFR =[];

if ~isempty(trajtime)
    wellpos = linpos{day}{epoch}.wellSegmentInfo.wellCoord;
    
    % Crawl over each trajectory and select times within
    % spatial and temporal_proximity to the endpoint
    for i = 1:size(trajtime,1)
        
        currtrajtime = trajtime(i,:);
        curr_endwell = wellstend(i,2);
        curr_rewarded = rewarded(i);
        curr_trajtype = trajbound; % 0=out, 1=in
        
        startidx = find(currtrajtime(1)==postime);                                             % mcz start of positions
        endidx = find(currtrajtime(2)==postime);                                               % mcz end of positions
        currpos = posn(startidx:endidx,:); % All pons on current outbound lap
        currtime = postime(startidx:endidx,:);
        currspeed = speed(startidx:endidx,:);
        
        currwellpos = wellpos(curr_endwell,:);
        % Get distance from endwell
        alldist = dist(currpos,repmat(currwellpos,length(currpos),1));
        % Get dist less than threshold
        valid_idx = find(alldist<=thrsdist);
        subset_pos = currpos(valid_idx,:);
        subset_speed = currspeed(valid_idx);
        subset_time = currtime(valid_idx);
        % FInd speeds < thrs in this window, and take the first time as reward-time
        reward_idx = min(find(subset_speed<thrsspeed));
        if ~isempty(reward_idx)
            curr_rewardtime = subset_time(reward_idx);
            rewardpos = subset_pos(reward_idx,:);
        else
            [curr_rewardtime,minidx] = min(subset_time);
            rewardpos = subset_pos(minidx,:);
        end
        
        if curr_rewarded
            allrewtime=[allrewtime; curr_rewardtime];
        else
            allnorewtime=[allnorewtime; curr_rewardtime];
        end
        
        %              if figopt==1
        %             figure(1); hold on; plot(posx, posy, 'Color', Clgy); axis square;
        %             set(gca, 'LooseInset', get(gca,'TightInset'));
        %             plot(currwellpos(1),currwellpos(2),'ks','MarkerSize',8,'MarkerFaceColor','k');
        %             circles(currwellpos(1),currwellpos(2),thrsdist,'edgecolor','k','facecolor','none');
        %             plot(currpos(:,1),currpos(:,2),'c-','Linewidth',2);
        %             plot(subset_pos(:,1),subset_pos(:,2),'g-','Linewidth',2);
        %             plot(rewardpos(1),rewardpos(2),'ro', 'MarkerSize',12, 'MarkerFaceColor','m')
        
    end
    
    
    % Align to rew and norew
    % -------------------------
    
    % DONT USE EXCLUDETIMES HERE
    
    sind = index;
    if ~isempty(spikes{sind(1)}{sind(2)}{sind(3)}{sind(4)}.data)
        spikeu = spikes{sind(1)}{sind(2)}{sind(3)}{sind(4)}.data(:,1)*1000;  % in ms
    else
        spikeu = [];
    end
    totaltime = diff(spikes{sind(1)}{sind(2)}{sind(3)}{sind(4)}.timerange)./10000;
    cellfr = length(spikeu)./totaltime;
    
    cntallrew=0; cntallnorew=0;
    % All Rewarded
    for i=1:length(allrewtime)
        cntallrew=cntallrew+1;
        currtime = (allrewtime(i))*1000; % ms
        currspks = spikeu(find( (spikeu>=(currtime-pret)) & (spikeu<=(currtime+postt)) ));
        %nspk = nspk + length(currspks);
        currspks = currspks-(currtime); % Set to 0 at reward
        histspks = histc(currspks,[-pret:binsize:postt]);
        histspks = smoothvect(histspks, g1); % Smmoth histogram
        % Save spktimes
        allrew_spks{cntallrew}=currspks;
        % Save histogram
        allrew_spkshist(cntallrew,:)=histspks;
        % Get no of spikes in response window, and back window
        trialResps_rew(cntallrew) =  length(find(currspks>=rwin(1) & currspks<=rwin(2)));
        trialResps_bck_rew(cntallrew) =  length(find(currspks>=bwin(1) & currspks<=bwin(2)));
    end
    
    
    % All UnRewarded
    for i=1:length(allnorewtime)
        cntallnorew=cntallnorew+1;
        currtime = (allnorewtime(i))*1000;
        currspks = spikeu(find( (spikeu>=(currtime-pret)) & (spikeu<=(currtime+postt)) ));
        %currspks = currspks-(currtime-pret); % Set to 0 at -pret
        %histspks = histc(currspks,[0:binsize:pret+postt]); % Make histogram
        currspks = currspks-(currtime); % Set to 0 at reward
        histspks = histc(currspks,[-pret:binsize:postt]); % Make histogram
        histspks = smoothvect(histspks, g1); % Smmoth histogram
        % Save spktimes
        allnorew_spks{cntallnorew}=currspks;
        % Save histogram
        allnorew_spkshist(cntallnorew,:)=histspks;
        % Get no of spikes in response window, and back window
        trialResps_norew(cntallnorew) =  length(find(currspks>=rwin(1) & currspks<=rwin(2)));
        trialResps_bck_norew(cntallnorew) =  length(find(currspks>=bwin(1) & currspks<=bwin(2)));
    end
    
    
    % Get Reward fir rate, and bck firing rate
    if ~isempty(trialResps_rew)
        rewFR = nanmean(trialResps_rew)./resplth_rew;
    else
        rewFR = [];
    end
    if ~isempty(trialResps_bck_rew)
        rewbckFR = nanmean(trialResps_bck_rew)./bcklth_rew;
    else
        rewbckFR = [];
    end
    if ~isempty(trialResps_norew)
        norewFR = nanmean(trialResps_norew)./resplth_rew;
    else
        norewFR = [];
    end
    if ~isempty(trialResps_bck_norew)
        norewbckFR = nanmean(trialResps_bck_norew)./bcklth_rew;
    else
        norewbckFR = [];
    end
    
else   % ~isempty(trajtime). Redundant, but do it again
    allrewtime=[]; allnorewtime=[];
    allrew_spks=[]; allnorew_spks=[];
    allrew_spkshist=[]; allnorew_spkshist=[];
    trialResps_rew=[]; trialResps_bck_rew=[];
    trialResps_norew=[]; trialResps_bck_norew=[];
    rewFR = []; rewbckFR =[];
    norewFR = []; norewbckFR =[];
    
end % end trajtime

% GET FR MODULATIONS FOR REWARD
% -------------------------------
if ~isempty(rewFR) && ~isempty(rewbckFR)
    FRmodln_rew = (rewFR - rewbckFR)./(rewFR + rewbckFR); 
else
    FRmodln_rew = nan;
end

if ~isempty(norewFR) && ~isempty(norewbckFR)
    FRmodln_norew = (norewFR - norewbckFR)./(norewFR + norewbckFR); 
else
    FRmodln_norew = nan;
end

if ~isempty(rewFR) && ~isempty(norewFR)
    FRmodln_rewnorew = (rewFR - norewFR)./(rewFR + norewFR); 
else
    FRmodln_rewnorew = nan;
end





% -------------------------% -------------------------% -------------------------

% -------------------------
% 2. HIGH SPEED FR
% -------------------------

ind = index;
day = index(1); epoch = index(2);
spikesp = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);

totaleptime = diff(spikes{ind(1)}{ind(2)}{ind(3)}{ind(4)}.timerange)./10000; % in secs
excltime = sum(diff(excludetimes'));

% -----------------------------
% 2A. FIRING RATE OF CELL IN EPOCH
% -----------------------------

cellinfoFR = cellinfo{index(1)}{index(2)}{index(3)}{index(4)}.meanrate;
cellFR = length(spikesp)./totaleptime;

% --------------------------
% 2B. Get high-speed Firing Rate
% --------------------------

% Use Include periods
% --------------------------
% Get IncludeTimes from flanking edges in excludetimes and epoch start-end
epstend = spikes{ind(1)}{ind(2)}{ind(3)}{ind(4)}.timerange./10000;
incl=[];
incl(:,1) = excludetimes(1:end-1,2);
incl(:,2) = excludetimes(2:end,1);
incl = [epstend(1),incl(1,1) ;incl];
incl = [incl; incl(end,2),epstend(2)];

% Length of include periods
incl_lths = diff(incl')';
% Discard anything < thrstime
discard = find(incl_lths<thrstime);
incl(discard,:)=[];

% No Need to go over loop. Only 1 cell
% -----------------------------------
spikespinc{1}= spikesp(find(isIncluded(spikesp,incl)));
% for i=1:size(cellsp,1)
%     i;
%     eval(['spikesp{',num2str(i),'}= spikes{cellsp(',num2str(i),',1)}{cellsp(',num2str(i),',2)}'...
%         '{cellsp(',num2str(i),',3)}{cellsp(',num2str(i),',4)}.data(:,1);']);
%  % gideon added, verify with shantanu
%     eval(['spikespinc{',num2str(i),'}= spikesp{',num2str(i),'}(find(isIncluded(spikesp{',num2str(i),'},incl)));'])
% end

% Going over all incl intervals, and from each interval extracting bin
% times of 200ms going from the interval start, ending >=200ms from its end
% thetaBins will hold the start times of all 200ms bins
thetaBins=[];
thetaBinSize=0.2;
for ii=1:length(incl),
    curIntervalSize=incl(ii,2)-incl(ii,1);
    numCurBins=floor(curIntervalSize/thetaBinSize);
    curBins=incl(ii,1):0.2:(incl(ii,1)+0.2*(numCurBins-1));
    thetaBins=[thetaBins curBins];
end

spikeBinsPFC=[];

% associate speeds to theta bins
sptimes=pos{ind(1)}{ind(2)}.data(:,1);
sp=pos{ind(1)}{ind(2)}.data(:,5);
meanSpeedBins=[];
speedsToBins=lookup(sptimes,thetaBins,-1);
for jj=1:length(thetaBins)
    meanSpeedBins=[meanSpeedBins nanmean(sp(speedsToBins==jj))];
end
meanSpeedBins=meanSpeedBins';


nPFCcells=1;
for k=1:nPFCcells
    
    spikesToBins=lookup(spikespinc{k},thetaBins,-1);
    tabulateSpikes=tabulate(spikesToBins);
    spikesInBins=NaN(1,length(thetaBins));
    if ~isempty(tabulateSpikes)
        spikesInBins(1:length(tabulateSpikes))=tabulateSpikes(:,2);
    end
    spikeBinsPFC=[spikeBinsPFC spikesInBins'];
end

meanSpeedBinsNoNAN=meanSpeedBins(~isnan(spikeBinsPFC)&~isnan(meanSpeedBins));
spikeBinsPFCNoNAN=spikeBinsPFC(~isnan(spikeBinsPFC)&~isnan(meanSpeedBins));
[r, p]=corrcoef(meanSpeedBinsNoNAN,spikeBinsPFCNoNAN);

% Find Only bins > highspeedthrs
highspeedbins = find (meanSpeedBinsNoNAN>=highspeedthrs);
highspeedfr_bins = spikeBinsPFCNoNAN(highspeedbins);
if ~isempty(highspeedfr_bins) || ~isnan(highspeedfr_bins)
    meanspks_bins = nanmean(highspeedfr_bins);
    highspeedFR = meanspks_bins./binsize; % FR in spikes/sec 
else
    highspeedFR = nan;
end


% --------------------------
% B. Get pre-SWR Firing Rate
% --------------------------

trialResps = ripplemod{index(1)}{index(2)}{index(3)}{index(4)}.trialResps;  % 0 to 200ms
trialResps_bck = ripplemod{index(1)}{index(2)}{index(3)}{index(4)}.trialResps_bck; %-500 to -100 ms
raster = ripplemod{index(1)}{index(2)}{index(3)}{index(4)}.raster;

bcklth = 0.2; %bck window is 200 ms or 0.1 sec long [-500 to -300ms]
resplth = 0.2; %resp window is 200 ms or 0.2 sec long
if ~isempty(trialResps_bck)
    preripFR2 = nanmean(trialResps_bck)./bcklth;
else
    preripFR2 = nan;
end

% Alterntative way for prerip FR - directly from raster
rasthist = rast2mat(raster);
bckhist = sum(rasthist(:,1:400),2);% 400ms long bckwindow (-500 to -100 ms): 0.4sec
if ~isempty(bckhist)
    preripFR = nanmean(bckhist)./0.4;
else
    preripFR=nan;
end

if ~isempty(trialResps)
    ripFR = nanmean(trialResps)./resplth;
else
    ripFR = [];
end



% --------------------
% FINALLY, ALSO GET MODULATION
% -------------------
FRmodln = (preripFR - highspeedFR)./(preripFR + highspeedFR); 
FRmodln2 = (preripFR2 - highspeedFR)./(preripFR2 + highspeedFR);
% varies betn -1 and 1. Positive values = preripFR > highspeedFR, and vice versa

% ------------------------------------------------------------
% FINALLY, ALSO GET MODULATION BETN REW and HIGHSPEED FR
% -----------------------------------------------------------
if ~isempty(rewFR)
    FRmodln_rewhighspeed = (rewFR - highspeedFR)./(rewFR + highspeedFR); 
    FRmodln_rewprerip = (rewFR - preripFR)./(rewFR + preripFR); 
else
    FRmodln_rewhighspeed = nan;
    FRmodln_rewprerip =  nan;
end

if ~isempty(rewFR) && ~isempty(ripFR)
    FRmodln_rewrip = (rewFR - ripFR)./(rewFR + ripFR); 
else
    FRmodln_rewrip =  nan;
end

% Output
% ------
out.index = sind;
% Propoerties
out.cellfr = cellfr;

% Reward resp, etc
% --------------------
out.allrewtime = allrewtime;
out.allnorewtime = allnorewtime;
out.allrew_spks = allrew_spks;
out.allnorew_spks = allnorew_spks;
out.allrew_spkshist = allrew_spkshist;
out.allnorew_spkshist = allnorew_spkshist;
out.trialResps_rew = trialResps_rew;
out.trialResps_bck_rew = trialResps_bck_rew;
out.trialResps_norew = trialResps_norew;
out.trialResps_bck_norew = trialResps_bck_norew;
% Nspikes summed across trials in responsewindow
out.Nspikes_rew = sum(trialResps);
out.Nspikes_norew = sum(trialResps_norew);

% rew FR
% --------------------
out.rewFR=rewFR;
out.rewbckFR=rewbckFR;
out.norewFR=norewFR;
out.norewbckFR=norewbckFR;
out.FRmodln_rew = FRmodln_rew;
out.FRmodln_norew = FRmodln_norew;
out.FRmodln_rewnorew = FRmodln_rewnorew;

out.FRmodln_rewhighspeed = FRmodln_rewhighspeed;
out.FRmodln_rewprerip = FRmodln_rewprerip;
out.FRmodln_rewrip = FRmodln_rewrip;

out.pret = pret;
out.postt = postt;
out.binsize = binsize;
out.rwin = rwin;
out.bckwin = bwin;
%out.bins_resp  = bins_resp;
%out.bins_bck = bins_bck;
%out.timeaxis = timeaxis;


% High speed and prerip FR
% -------------------------
out.preripFR = preripFR;
out.highspeedFR = highspeedFR;
out.FRmodln = FRmodln; 

out.preripFR2 = preripFR2;
out.FRmodln2 = FRmodln2;

out.cellinfoFR = cellinfoFR;
out.cellFR = cellFR;
out.ripFR = ripFR;
out.speeds=meanSpeedBinsNoNAN;
out.spiking=spikeBinsPFCNoNAN;
out.FR_speed_corr_r=r(1,2);
out.FR_speed_corr_p=p(1,2);



