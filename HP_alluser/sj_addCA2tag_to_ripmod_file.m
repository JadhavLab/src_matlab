

% Lod CA1 ripmod file and CA2 tag to Rtl, Tet 14 where it exists in allripplemod_idx

% From Gideons tagCA2CellsForShantanu
% Instead of changing "area", can add a tag field called CA2 tag, eg. sj_addFStag6.m
% For now, just change it for Ndl Tet 14


clear;
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
ripplefile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6'];
load(ripplefile); % Load the entire file since you have to eventually save it


% First add a CA2 tag field for allripplemod
% ------------------------------------------
for i = 1:size(allripplemod_idx,1)
    allripplemod(i).CA2tag = 'n';
end


% Find cells in index with anim = Rtl (No.5), and Tet = 14
anim = allripplemod_idx(:,1);
tet = allripplemod_idx(:,3);

target = find(anim==5 & tet==14);

allt=[]; allp=[];
for i = 1:length(target)
    allp = [allp; allripplemod(i).rasterShufP2];
    allt = [allt; strcmp(allripplemod(i).type,'exc')];
    
    target(i)
    allripplemod(target(i)).CA2tag = 'y';
end


% Can Update filename by putting a "CA2tag" in front of it
% ------------------------------------------------------
savefile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6_CA2tag']
if savedata
    save(savefile);
end




