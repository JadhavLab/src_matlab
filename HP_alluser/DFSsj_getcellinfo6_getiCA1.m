
% In version 2, get rid of csi and propbursts, since Ndl doesn't seem to have these.
% Also, you want to make an addition, where you go back to the params and matclust file, and load other parameters
% such as amplitude, ahp size, etc
% Just using the cellinfo structure to get and plot cell properties

% Based on DFSsj_getcellinfo6
% Add a tag for iCA1 cells, so you can add to allripplemod file like you do FStag


% Got total 70 iCA1 cells, and 54/70 end up eventually in the allripplemod file
% Follow this up with sj_addiCA1tag_to_ripmod_file_X6.m


clear;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 1; % Figure Options

%savedir = 'mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';

[y, m, d] = datevec(date);
if runscript == 1
    val=1; savefile = [savedir 'HP_cellinfo_getiCA1_',num2str(m),'-',num2str(d),'-',num2str(y)]; %
    %val=2; savefile = [savedir 'HP_cellinfo_PFC_',num2str(m),'-',num2str(d),'-',num2str(y)]; %
else
    %val=1; savefile = [savedir 'HP_cellinfo_CA1_3-7-2014']; %
    %val=2; savefile = [savedir 'HP_cellinfo_PFC_3-7-2014']; %
    
    val=1; savefile = [savedir 'HP_cellinfo_getiCA1_5-10-2015']; %
    %val=1; savefile = [savedir 'HP_cellinfo_CA1_4-22-2015']; %
    %val=2; savefile = [savedir 'HP_cellinfo_PFC_4-22-2015']; %
end


% If runscript, run Datafilter and save data
if runscript == 1
    
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Ndl','Rtl','Brg'};
    
    %Filter creation
    %--------------------------------------------------------
    
    % Epoch filter
    % -------------
    %dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    %epochfilter = 'isequal($type, ''run'')'; % All run environments
    epochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    
    % Cell filter
    % -----------
    
    % Any cell defined in at least one run in environment
    
    switch val
        case 2
            cells = '(strcmp($area, ''PFC'')) &&  ($numspikes > 100)';
        case 1
            cells = '( strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) &&  ($numspikes > 100)';
    end
    
    
    % Time filter
    % -----------
    % None
    
    
    % Iterator
    % --------
    iterator = 'singlecellanal';
    
    % Filter creation
    % ----------------
    cellsf = createfilter('animal',animals,'epochs',epochfilter,'cells',cells,'iterator', iterator);
    
    % Set analysis function
    % ----------------------
    cellsf = setfilterfunction(cellsf, 'DFAsj_getcellinfo6', {'cellinfo'}); % You can also use "cellprop" which uses spikes as the variable and calls getcellprop
    
    
    % Run analysis
    % ------------
    cellsf = runfilter(cellsf);  %
    
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end  % end runscript

if ~exist('savedata')
    return
end


% --------------------------
% Post-filter run analysis
% --------------------------

% Gatherdata and save
gatherdata = 1; savegatherdata = 1;
[y, m, d] = datevec(date);
switch val
    case 2
%         if gatherdata
%             gatherdatafile = [savedir 'HP_cellinfo_PFC_gather_',num2str(m),'-',num2str(d),'-',num2str(y)]
%         else
%             %gatherdatafile = [savedir 'HP_cellinfo_PFC_gather_3-7-2014']
%             gatherdatafile = [savedir 'HP_cellinfo_PFC_gather_4-22-2015']
%         end
        
    case 1
        if gatherdata
            gatherdatafile = [savedir 'HP_cellinfo_getiCA1_gather_',num2str(m),'-',num2str(d),'-',num2str(y)];
        else
            %gatherdatafile = [savedir 'HP_cellinfo_CA1_gather_3-7-2014'];
            gatherdatafile = [savedir 'HP_cellinfo_getiCA1_gather_5-10-2015'];
        end
end

if gatherdata
    
    % ---------------------------------------------------------
    % Gather data and append animal index to it as well
    % Unlike codes where you want to keep data for epochs separate for plotting (eg. place field code), here, combine everything for population analysis.
    % ---------------------------------------------------------
    
    alldata = []; alltags = []; allanimindex = []; % includes animal index
    cnt = 0; % For cnting across animals for tags
    for an = 1:length(animals)
        for i=1:length(cellsf(an).output{1}),
            anim_index{an}(i,:)=cellsf(an).output{1}(i).index;
            % Only indexes
            animindex=[an cellsf(an).output{1}(i).index]; % Put animal index in front
            allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
            
            % Indexes + Data [anim day epoch tet cell rate numspikes spikewidth
            append = [an cellsf(an).output{1}(i).index cellsf(an).output{1}(i).meanrate ...
                cellsf(an).output{1}(i).numspikes cellsf(an).output{1}(i).spikewidth cellsf(an).output{1}(i).is_iCA1];
            alldata = [alldata; append];
            
            % Make a tag field with the same length
            %cnt=cnt+1;
            %allrawtags(cnt).tag = cellsf(an).output{1}(i).tag;
            
        end
    end
    
    % -----------------------------------------------------------------
    % Consolidate single cells' across epochs in a day .celloutput field
    % Can do for each animal and then store separately by using the index from output field - Do in loop for each animal,
    % or can use the above appended animal+index field to parse data
    % -----------------------------------------------------------------
    
    % Save the consolidated data back in a structure - TO put back in filter
    % structure, has to be animal-wise. Sell alternate method below
    celloutput = struct;
    dummyindex=allanimindex;  % all anim-day-epoch indices
    
    
    cntcells=0; FSidx=[]; FSidxno=[]; iCA1idx=[]; iCA1idxno=[];
    for i=1:size(alldata,1)
        animdaytetcell=alldata(i,[1 2 4 5]);
        ind=[];
        while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
            dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one
        end
        
        % Gather everything for the current cell across epochs
        currrate=[]; currcsi=[]; currpropbursts=[]; currnumspikes=[]; currspikewidth=[]; currtag = [];
        curr_iCA1=[];
        for r=ind
            currrate=[currrate ; alldata(r,6)];
            currnumspikes=[currnumspikes;alldata(r,7)];
            currspikewidth=[currspikewidth;alldata(r,8)];
            curr_iCA1 = [curr_iCA1; alldata(r,9)];
        end
        
        if ~isempty(currrate)
            %currtag = allrawtags(ind(1)).tag; % Tag is same for all epochs
            cntcells = cntcells + 1;
            celloutput(cntcells).index=animdaytetcell;
            celloutput(cntcells).meanrate=mean(currrate); % Mean across epochs
            celloutput(cntcells).numspikes=mean(currnumspikes);
            celloutput(cntcells).spikewidth=mean(currspikewidth);
            celloutput(cntcells).FStag = 'n';
            
            celloutput(cntcells).is_iCA1 = curr_iCA1(1);
            celloutput(cntcells).iCA1tag = 'n';
            
            if curr_iCA1(1)==1
                iCA1idx  = [iCA1idx;animdaytetcell]
                iCA1idxno = [iCA1idxno;cntcells]
                celloutput(cntcells).iCA1tag = 'y';
            end
            
            
            % Firing rate and spikewidths
            % This is for PFC
            %----------------
            if val==2
                if (mean(currrate)>17) | (mean(currspikewidth)<6)
                    FSidx = [FSidx;animdaytetcell]
                    FSidxno = [FSidxno;cntcells]
                    celloutput(cntcells).FStag = 'y';
                end
            end
            % This is for CA1 - Not necessary, since you implement meanrate<7
            % condition already in the filter framework
            %------------------------------------------------
            if val==1
                if (mean(currrate)>7)
                    FSidx = [FSidx;animdaytetcell];
                    FSidxno = [FSidxno;cntcells];
                    celloutput(cntcells).FStag = 'y';
                end
            end
        end
    end
    
    % Alternate method - Keep Animal data separate. For each animal, look within dayeptetcell
    % ----------------------------------------------------------------------------------------
    % for an = 1:length(animals)
    %     dummyindex = anim_index{an};     % collect all day-epoch-tet-cell indices
    %     for i=1:length(cellsf(an).output{1})
    %         daytetcell=cellsf.output{1}(i).index([1 3 4]);
    %         ind=[];
    %         while rowfind(daytetcell,dummyindex(:,[1 3 4]))~=0          % collect all rows (epochs)
    %             ind = [ind rowfind(daytetcell,dummyindex(:,[1 3 4]))];
    %             dummyindex(rowfind(daytetcell,dummyindex(:,[1 3 4])),:)=[0 0 0 0];
    %         end
    %         % Gather everything for the current cell across epochs
    %         currrate=[]; currcsi=[]; currpropbursts=[]; currnumspikes=[]; currspikewidth=[]; currtag = [];
    %         for r=ind
    %             currrate=[currrate ; cellsf.output{1}(r).meanrate];
    %             currcsi=[currcsi; cellsf.output{1}(r).csi];
    %             currpropbursts=[currpropbursts; cellsf.output{1}(r).propbursts];
    %             currnumspikes=[currnumspikes;cellsf.output{1}(r).numspikes];
    %             currspikewidth=[currspikewidth;cellsf.output{1}(r).spikewidth];
    %         end
    %         if ~isempty(currrate)
    %             currtag = cellsf.output{1}(ind(1)).tag; % Tag is same for all epochs
    %             cellsf(an).celloutput.index=daytetcell;
    %             cellsf(an).celloutput.meanrate=currrate;
    %             cellsf(an).celloutput.csi=csi;
    %             cellsf(an).celloutput.propbursts=propbursts;
    %             cellsf(an).celloutput.numspikes=numspikes;
    %             cellsf(an).celloutput.spikewidth=spikewidth;
    %             cellsf(an).celloutput.tag=ncurrtag;
    %         end
    %     end
    % end
    
    
    % Values for cells - means across epochs already taken now. This gets it out of
    % structure format
    allmeanrate = []; allcsi = []; allpropbursts = []; allnumspikes = []; allspikewidth = [];
    alliCA1=[];
    for i=1:length(celloutput)
        allcellidx(i,:) = celloutput(i).index;
        allmeanrate = [allmeanrate; celloutput(i).meanrate];
        allspikewidth = [allspikewidth, celloutput(i).spikewidth];
        
        alliCA1 = [alliCA1; strcmp(celloutput(i).iCA1tag,'y')];
    end
    
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data



% ---------
% Plotting
% ---------

length(allmeanrate), length(find(allmeanrate<0.1));
%FS1 = allcellidx(find(allmeanrate>15),:), FS2 = allcellidx(find(allspikewidth<2),:),



%1) All Cells - Fir rates vs spikewidths
figure; hold on;
redimscreen_figforppt1;

%scatter(allspikewidth, allmeanrate, [],'r');
plot(allspikewidth, allmeanrate,'ko','MarkerSize',8,'LineWidth',1.5);
%plot(allspikewidth(Intidx), allmeanrate(Intidx),'kx','MarkerSize',8,'LineWidth',1.5);

xaxis = min(allspikewidth):0.1:max(allspikewidth); yaxis = min(allmeanrate):1:max(allmeanrate);
plot(xaxis,0.1*ones(size(xaxis)),'k-','Linewidth',1);
if val==2, thresh=17; else, thresh=7; end
plot(xaxis,thresh*ones(size(xaxis)),'k-','Linewidth',1);
%plot(8*ones(size(yaxis)),yaxis,'k-','Linewidth',1);


set(gca,'YLim',[0 1.05*max(allmeanrate)]);
set(gca,'XLim',[0.95*min(xaxis) 1.05*max(xaxis)]);

xlabel('Spike Widths','FontSize',24,'Fontweight','normal');
ylabel('Firing Rates (Hz)','FontSize',24,'Fontweight','normal');
title('Cell Properties','FontSize',24,'Fontweight','normal')


clr = ['r','b','c','m','g','y'];

% Mark animal nos
for an=1:6
    curridxs = find(allcellidx(:,1)==an);
    plot(allspikewidth(curridxs), allmeanrate(curridxs),[clr(an) 'o'],'MarkerSize',8,'LineWidth',1.5);
    i=1;
end

% Mark FS cells
plot(allspikewidth(FSidxno), allmeanrate(FSidxno),'kx','MarkerSize',12,'LineWidth',4);



% Histogram along the two dimensions
figure; hold on;
h = histc(allmeanrate,[0:31]);
bar(h);

figure; hold on;
h = histc(allspikewidth,[0:18]);
bar(h);








