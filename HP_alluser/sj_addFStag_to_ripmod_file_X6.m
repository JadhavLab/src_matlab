
% Load the "ripplemod" file and the "cellinfo" file for PFC cells, and add
% a "FStag" field to the allripplemod structure identifying the FS cells

clear;
%savedir = 'mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';





% PFC Ripplemod file
% --------------
ripplefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6'];
%ripplefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014'];
%load(ripplefile, 'allripplemod','allripplemod_idx'); % load allripplemod and allripplemod_idx.
load(ripplefile); % Load the entire file since you have to eventually save it

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';

anim = allripplemod_idx(:,1);
Wtr_PFC = length(find(anim <=3)),
Ytr_PFC = length(find(anim>3)),

% Cellinfo file
% --------------
cellfile = [savedir, 'HP_cellinfo_PFC_gather_4-22-2015'];
%cellfile = [savedir, 'HP_cellinfo_PFC_gather_3-7-2014'];
load(cellfile,'FSidx');

cntFS=0; FSripmod=[]; FSid=[];
for i = 1:size(FSidx,1)
    
    curridx = FSidx(i,:);
    
    match = rowfind(curridx, allripplemod_idx)
    
    if match~=0
        cntFS=cntFS+1;
        FSid=[FSid ;curridx],
        allripplemod(match).FStag = 'y';
        if allripplemod(match).rasterShufP2<0.05
            FSripmod = [FSripmod; 1];
        else
            FSripmod = [FSripmod; 0];
        end   
    end
end



savedata = 0;
% Can Update filename by putting a "FStagged" in front of it
% ------------------------------------------------------
savefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6'];
if savedata
    save(savefile);
end







% ---------------------------------------------------------------------
% ---------------------------------------------------------------------

% CA1 Ripplemod file
% --------------
ripplefile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6'];
%ripplefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014'];
%load(ripplefile, 'allripplemod','allripplemod_idx'); % load allripplemod and allripplemod_idx.
load(ripplefile); % Load the entire file since you have to eventually save it

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
% Cellinfo file
% --------------
cellfile = [savedir, 'HP_cellinfo_CA1_gather_4-22-2015'];
%cellfile = [savedir, 'HP_cellinfo_CA1_gather_3-7-2014'];
load(cellfile,'FSidx');

% First add a FS tag field for allripplemod
% ------------------------------------------
for i = 1:size(allripplemod_idx,1)
    allripplemod(i).FStag = 'n';
end

cntFS=0; FSripmod=[]; FSid=[];
for i = 1:size(FSidx,1)
    
    curridx = FSidx(i,:);
    
    match = rowfind(curridx, allripplemod_idx)
    
    if match~=0
        cntFS=cntFS+1;
        FSid=[FSid ;curridx],
        allripplemod(match).FStag = 'y';
        if allripplemod(match).rasterShufP2<0.05
            FSripmod = [FSripmod; 1];
        else
            FSripmod = [FSripmod; 0];
        end   
    end
end


savedata = 0;
% Can Update filename by putting a "FStagged" in front of it
% ------------------------------------------------------
savefile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6'];
if savedata
    save(savefile);
end








