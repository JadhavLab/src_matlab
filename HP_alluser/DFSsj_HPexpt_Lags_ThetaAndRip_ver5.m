% Ver 5: with new animals, for resubmission, Gideon Dec2014
% Ver4 : Starting 10Feb2014 - Sync codes with everyone

% Based on "DFSsj_HPexpt_ThetacorrAndRipresp_ver4.m". 
% Get time-lag of corrln peak for theta covariance and ripple periods (+/-500ms)
% Can do for epochs separately, avg the crosscorrlns, and then get the peak, peaklag
% Alternative - in next version, combine data from two epochs, and then do the crosscorrln. corsscov



% --- Notes from original code ------
% Ver3: For ripple corrln, combine the ripplemod across epochs, and then do the corrln.
% So the filter function will simply return the correlation function

% Ver2, Dec 2013 - Implement combining data across epochs 
% Raw Data Files Stay Same - Only Gatherdata Files will change


% For ripple modulated and unmodulated cells, get corrcoeff and coactivez using the ripple response
% Instead of aligning to ripples again, use the saved output oin ripplemod files

% For consolidating across epochs:
% If I run corrcoeff and coactivez using DFAsj_getripresp_corrandcoactz, I will have to take mean when
% consolidating across epochs. Instead, I can run a new functions "DFAsj_gettrialResps", then combine these
% across epochs, and run corrcoeff and coactivez here in this function.


clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells

%savedir = '/data15/gideon/ProcessedData/';
%savedir = '/opt/data15/gideon/HP_ProcessedData/';

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';


% PARAMETER
% ---------

% For Ripple Correlation, use either a window around ripples. Parameters define window around ripples
% OR just use ripple times and xcorr. Choose below
% pret and postt window around SWR to use for ripple correlation
rip_pret = 200/1000; rip_postt = 300/1000; % in secs


% Version 4 onwards
% ----------------
% IMP! CA1 (theta modulated only) and PFC ripmod vs ripunmod.
% -----------------------------------------------------------

% PUT MANUAL DATA
val=1;savefile = [savedir 'HP_CA1PFC_Lags_ThetaAndRip_std3_speed4_ntet2_alldata_X1']; area = 'CA1thetamodPFCripmod'; clr = 'r';
    
% IMP! CA1 (theta modulated only) 
% -------------------------------
%val=2;savefile = [savedir 'HP_CA1_Lags_ThetaAndRip_std3_speed4_ntet2_alldata_2-18-2014']; area = 'CA1thetamod'; clr = 'b';

% IMP! PFC ripmod and PFC ripmod; and PFCall .
% ------------------------------
%val=3;savefile = [savedir 'HP_PFCripmod_Lags_ThetaAndRip_std3_speed4_ntet2_alldata_2-18-2014']; area = 'PFCripmod'; clr = 'c';
%val=4;savefile = [savedir 'HP_PFCall_Lags_ThetaAndRip_std3_speed4_ntet2_alldata_2-18-2014']; area = 'PFCall'; clr = 'c';


savefig1=0;


% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    %animals = {'HPa','HPb','HPc','Nadal'};
%    animals = {'HPa','HPb','HPc','Ndl'};
    animals = {'HPa','HPb','HPc','Nadal','Rosenthal'};
% animals = {'Borg'};

    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    %dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day

    % MAKE SURE WITH SHANTANU THAT THIS DEFINITION OF RUN EPOCHS IS OK
    %    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
 
    runepochfilter = 'isequal($type, ''run'')';

    % %Cell filter
    % %-----------
    
    % %IMP! CA1theta-PFCRipmodulated
    % %------------------------
    switch val
        case 1  
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($thetamodtag, ''y'') ','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag2, ''y'')'};
    % %IMP! CA1theta
    % %------------------------
        case 2
            cellpairfilter = {'allcomb','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($thetamodtag, ''y'') ','(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($thetamodtag, ''y'') '};
    % %IMP! PFCRipmodulated only
    % %------------------------
        case 3
            cellpairfilter = {'allcomb','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag2, ''y'')','strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag2, ''y'')'};
    % %IMP! PFCall
    % %------------------------
        case 4
            cellpairfilter = {'allcomb','strcmp($area, ''PFC'') && ($numspikes > 100)','strcmp($area, ''PFC'') && ($numspikes > 100)'};
    end
    
    
    % Time filter - none.
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    timefilter_rip = {{'DFTFsj_getvelpos', '($absvel <= 4)'},{'DFTFsj_getriptimes4','($nripples > 1)','tetfilter',riptetfilter,'minstd',3}};
      
%     timefilter_place = { {'DFTFsj_getlinstate', '(($state ~= -1) & (abs($linearvel) >= 5))', 6},...
%         {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',2} };

    % Use absvel instead of linearvel
    timefilter_place_new = { {'DFTFsj_getvelpos', '($absvel >= 5)'},...
        {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',3} };
    
%     timefilter_place_new = { {'DFTFsj_getvelpos', '($absvel >= 5)'},{'DFTFsj_getlinstate', '($state ~= -1)', 6},...
%         {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',3} };
%     
    % Iterator
    % --------
    iterator = 'singlecellanal';  % Have defined cellpairfilter. Can also use cellpair iterator with cell defn
    
    % Filter creation
    % ----------------
    
    % Ripple Correlation
    % -
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'cellpairs',...
        cellpairfilter, 'excludetime', timefilter_rip, 'iterator', iterator);
    
    % Theta Correlation
    % -
    
    %     thetaf = createfilter('animal',animals,'days',dayfilter,'epochs',runepochfilter, 'cellpairs',...
    %         cellpairfilter, 'excludetime', timefilter_place_new, 'iterator', iterator);
    thetaf = createfilter('animal',animals,'epochs',runepochfilter, 'cellpairs',...
        cellpairfilter, 'excludetime', timefilter_place_new, 'iterator', iterator);

    % Place field overlap
    % -
    % Parameters
    norm_overlap = 1;  % For place-field overlap
    thresh_peakrate = 3; % For place-field overlap
    xrun = createfilter('animal',animals,'epochs',runepochfilter,'cellpairs',...
        cellpairfilter,'iterator', iterator);
    
    
    
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
   
    % For ripple corrln, CHOOSE
    % a) use window around ripple, OR
    % % For calculating ripplealigned resp from scratch, you will need spikes, ripples, tetinfo, and pos
    % modf = setfilterfunction(modf,'DFAsj_getrip_period_xcorr4',{'spikes', 'ripples', 'tetinfo', 'pos'},'dospeed',1,'lowsp_thrs',4,'minrip',2,'minstd',3,'pret',rip_pret,'postt',rip_postt); 
    % b) Use xcorrmeasure with riptimes
    modf = setfilterfunction(modf, 'DFAsj_HPexpt_calcxcorrmeasures5', {'spikes'});
    
    thetaf = setfilterfunction(thetaf,'DFAsj_getthetacrosscovLAG_timecondition4', {'spikes'},'thrstime',1); % With includetime condition
    
%     xrun = setfilterfunction(xrun, 'DFAsj_calcoverlap4', {'linfields', 'mapfields'},...
%         'normalize',norm_overlap,'thresh',thresh_peakrate,'minbins',0.5);
%     
    % Run analysis
    % ------------
    modf = runfilter(modf);
    thetaf = runfilter(thetaf);
%     xrun = runfilter(xrun);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile,'-v7.3');
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------



% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
[y, m, d] = datevec(date);

% Version 4 onwards
% Filename of gatherdata. If generating, put current date. If not, then load a file from previous date.
% -----------------------------------------------------------------------------------------------------

% PUT MANUAL DATES


switch val
    % % IMP! - CA1 (theta modulated only) and PFC ripmod vs ripunmod. Also Compare to Sleep Ripmod computed in other Script
    % ------------------------------------------------------------------------
    case 1       
        gatherdatafile = [savedir 'HP_CA1PFC_Lags_ThetaAndRip_std3_speed4_ntet2_alldata_gather_X1'], area = 'CA1thetamodPFCripmod'; kind = 'thetamodripmod'; state = '';
    case 2
        gatherdatafile = [savedir 'HP_CA1_Lags_ThetaAndRip_std3_speed4_ntet2_alldata_gather_X1'], area = 'CA1thetamodPFCripUnmod'; kind = 'thetamodripUnmod'; state = '';
   
%     case 3
%         % % IMP! PFC Ripmod Only
%         % -----------------------
%         gatherdatafile = [savedir 'HP_PFCripmod_Lags_ThetaAndRip_std3_speed4_ntet2_alldata_gather_2-18-2014'], area = 'PFCripmod'; kind = 'ripmod'; state = '';
%         
%     case 4      
%         gatherdatafile = [savedir 'HP_PFCall_Lags_ThetaAndRip_std3_speed4_ntet2_alldata_gather_2-18-2014'], area = 'PFCripmod'; kind = 'ripmod'; state = '';
%         
end


if gatherdata
    
    % Parameters if any
    % -----------------
    
    % -------------------------------------------------------------
    
    cnt=0;
    allanimindex=[]; 
    
    allZcrosscov_runtheta=[]; allcrosscov_runtheta_totalcorr=[]; allrawcorr_runtheta=[]; allnormcorr_runtheta=[];
    allZcrosscov_sm_runtheta=[]; allcrosscov_sm_runtheta_totalcorr=[]; allrawcorr_sm_runtheta=[]; allnormcorr_sm_runtheta=[];
    allNeventscorr_runtheta=[];allxcorr_runtheta=[]; allT_runtheta=[]; allp1p2_runtheta=[];
    runcorrtime=[];
    
    allrawcorr_rip=[]; allrawcorr_sm_rip=[];  allnormcorr_rip=[]; allnormcorr_sm_rip=[];
    allNeventscorr_rip=[]; allxcorr_rip=[];
    ripcorrtime=[];
    
    alloverlap=[]; allpeakcomb=[]; alltrajpeakcomb=[];
    alltrajdata1=[]; alltrajdata2=[]; allmapdata1=[]; allmapdata2=[];
    
    corrwin = 0.1; %Window for theta and ripple corrln
    corrwinrip = 0.1;
    
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            cnt=cnt+1;
            anim_index{an}(cnt,:) = modf(an).output{1}(i).index;
            % Only indexes
            animindex=[an modf(an).output{1}(i).index]; % Put animal index in front
            allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet1 Cell1 Tet2 Cell2 Index

            % Data - Ripple period corrln
            % ----------------------------
            allrawcorr_rip(cnt,:) = modf(an).output{1}(i).rawcorr;
            allrawcorr_sm_rip(cnt,:) = modf(an).output{1}(i).rawcorr_sm;
            allnormcorr_rip(cnt,:) = modf(an).output{1}(i).normcorr;
            allnormcorr_sm_rip(cnt,:) = modf(an).output{1}(i).normcorr_sm;
            allNeventscorr_rip(cnt) = modf(an).output{1}(i).Neventscorr;
            allxcorr_rip{cnt} = modf(an).output{1}(i).corrstruct;
            
            % Data - Theta Crosscov
            % ---------------------
            allZcrosscov_runtheta(cnt,:) = thetaf(an).output{1}(i).Zcrosscov;
            allcrosscov_runtheta(cnt,:) = thetaf(an).output{1}(i).crosscov;
            allZcrosscov_sm_runtheta(cnt,:) = thetaf(an).output{1}(i).Zcrosscov_sm;
            allcrosscov_sm_runtheta(cnt,:) = thetaf(an).output{1}(i).crosscov_sm;
            allrawcorr_runtheta(cnt,:) = thetaf(an).output{1}(i).rawcorr;
            allrawcorr_sm_runtheta(cnt,:) = thetaf(an).output{1}(i).rawcorr_sm;
            allnormcorr_runtheta(cnt,:) = thetaf(an).output{1}(i).normcorr;
            allnormcorr_sm_runtheta(cnt,:) = thetaf(an).output{1}(i).normcorr_sm;
            allNeventscorr_runtheta(cnt) = thetaf(an).output{1}(i).Neventscorr;
            allxcorr_runtheta{cnt} = thetaf(an).output{1}(i).corrstruct;
            allT_runtheta(cnt) = thetaf(an).output{1}(i).T;
            allp1p2_runtheta(cnt) = thetaf(an).output{1}(i).p1p2;
            
            % Data - Place field overlap
            % ---------------------
%             alloverlap(cnt) = xrun(an).output{1}(i).overlap;
%             allpeakcomb(cnt) = xrun(an).output{1}(i).peakcomb;
%             alltrajpeakcomb(cnt) = xrun(an).output{1}(i).trajpeakcomb;
            % trajdata and mapdata are not being returend - too big
            %alltrajdata1{an}{i} = xrun(an).output{1}(i).trajdata1;
            %alltrajdata2{an}{i} = xrun(an).output{1}(i).trajdata2;
            %allmapdata1{an}{i} = xrun(an).output{1}(i).mapdata1;
            %allmapdata2{an}{i} = xrun(an).output{1}(i).mapdata2;
            
            
            %Time base for theta and ripple correlations - only once
            % -----------------------------------------------------
            if isempty(runcorrtime)
                if isfield(thetaf(an).output{1}(i).corrstruct,'time');
                    runcorrtime =  thetaf(an).output{1}(i).corrstruct.time;
                end
                bins_run = find(abs(runcorrtime)<=corrwin); % +/- Corrln window
            end

            if isempty(ripcorrtime)
                if isfield(modf(an).output{1}(i).corrstruct,'time');
                    ripcorrtime =  modf(an).output{1}(i).corrstruct.time;
                end
                bins_rip = find(abs(ripcorrtime)<=corrwinrip); % +/- Corrln window
            end
            
            
            % SHIFT ALL CALCULATIONS TO AFTER DATA COMBINED ACROSS EPOCHS
            % -------------------------------------------------------------           
%             % Calculate a number for theta corr - Total probor pek  in -/+corrwin
%             currthetacorr = allZcrosscov_runtheta(cnt,:);
%             currthetacorr_sm = allZcrosscov_sm_runtheta(cnt,:);
%             % Sum of values in window
%             alltheta_totalcorr(cnt) = nansum(currthetacorr_sm(bins_run))./length(bins_run); % per bin
%             % Peak value in window +/- corrwin
%             alltheta_peakcorr(cnt) = nanmax(currthetacorr_sm(bins_run)); % Already smoothened, or can take +/-3 bins around peak
%             if (~isnan(alltheta_peakcorr(cnt)) && ~isempty(alltheta_peakcorr(cnt)))
%                 alltheta_peaklag_idx(cnt) = min(find(currthetacorr_sm(bins_run) == nanmax(currthetacorr_sm(bins_run)))); % in ms
%                 alltheta_peaklag(cnt) = runcorrtime(bins_run(alltheta_peaklag_idx(cnt)))*1000; %in ms
%             else
%                 alltheta_peaklag_idx(cnt)=0;
%                 alltheta_peaklag(cnt)=0;
%             end
%             % Trough value in window +/- corrwin
%             
%             alltheta_troughcorr(cnt) = nanmin(currthetacorr_sm(bins_run)); % Already smoothened, or can take +/-3 bins around peak
%             if (~isnan(alltheta_troughcorr(cnt)) && ~isempty(alltheta_troughcorr(cnt)))
%                 alltheta_troughlag_idx(cnt) = min(find(currthetacorr_sm(bins_run) == nanmin(currthetacorr_sm(bins_run)))); % in ms
%                 alltheta_troughlag(cnt) = runcorrtime(bins_run(alltheta_troughlag_idx(cnt)))*1000; %in ms
%             else
%                 alltheta_troughlag_idx(cnt)=0;
%                 alltheta_troughlag(cnt)=0;
%             end
%           
%             %alltheta_totalcorr(cnt) = nanmax(currthetacorr(bins_run));
%             %alltheta_peaklag(cnt) = find (currthetacorr(bins_run) == nanmax(currthetacorr(bins_run)));
            
        end
        
    end
    
    
    % CONSOLIDATE ACROSS EPOCHS FOR SAME CELL PAIRS
    % ----------------------------------------------------------
    runpairoutput = struct;
    dummyindex=allanimindex;  % all anim-day-epoch-tet1-cell1-tet2-cell2 indices
    cntpairs=0;
    
    for i=1:size(allanimindex)
        animdaytetcell=allanimindex(i,[1 2 4 5 6 7]);
%         if sum(animdaytetcell(3:end)-[1 2 15 4])==0
%             
%             keyboard
%         end
        ind=[];
        while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5 6 7]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5 6 7]))];        % finds the first matching row
            dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5 6 7])),:)=[0 0 0 0 0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one if it exists
        end
        
        % Gather everything for the current cell across epochs
        % Theta corr variables
        % ---------------------
        allnormcorr_runtheta_epcomb = []; allnormcorr_sm_runtheta_epcomb = []; 
        allNeventscorr_runtheta_epcomb = []; %%alltheta_peakcorr_epcomb = []; alltheta_peaklag_epcomb = [];
        % Ripcorr variables 
        % -----------------
        allnormcorr_rip_epcomb = []; allnormcorr_sm_rip_epcomb = []; 
        allNeventscorr_rip_epcomb = []; 
        % Place Overlap
        % -----------------
        alloverlap_epcomb=[];
        
        for r=ind
            % Theta Corr variables
            % --------------------
            allnormcorr_sm_runtheta_epcomb = [allnormcorr_sm_runtheta_epcomb; allnormcorr_sm_runtheta(r,:)];
            allnormcorr_runtheta_epcomb = [allnormcorr_runtheta_epcomb; allnormcorr_runtheta(r,:)];
            allNeventscorr_runtheta_epcomb = [allNeventscorr_runtheta_epcomb; allNeventscorr_runtheta(r)];
            %%alltheta_peakcorr_epcomb = [alltheta_peakcorr_epcomb; alltheta_peakcorr(r)];
            %%alltheta_peaklag_epcomb = [alltheta_peaklag_epcomb; alltheta_peaklag(r)];
            % Rip Corr variables
            % ------------------
            allnormcorr_sm_rip_epcomb = [allnormcorr_sm_rip_epcomb; allnormcorr_sm_rip(r,:)];
            allnormcorr_rip_epcomb = [allnormcorr_rip_epcomb; allnormcorr_rip(r,:)];
            allNeventscorr_rip_epcomb = [allNeventscorr_rip_epcomb; allNeventscorr_rip(r)];
            % Place Overlap
            % -----------------
%            alloverlap_epcomb = [alloverlap_epcomb; alloverlap(r)];
        end
        
        
        if ~isempty(allnormcorr_runtheta_epcomb)
            cntpairs=cntpairs+1;
            runpairoutput_idx(cntpairs,:)=animdaytetcell;
            runpairoutput(cntpairs).index=animdaytetcell; % This is anim-day-tet1-cell1-tet2-cell2. No epoch
            
            currthetacorr_sm=[]; currpeakcorr=[]; currpeaklag_idx=[]; currpeaklag =[];
            currnormcorr_sm=[]; currpeakripcorr=[]; currpeaklagrip_idx=[]; currpeaklagrip =[];
            
            % Theta corr variables
            % -------------------
            runpairoutput(cntpairs).allnormcorr_sm_runtheta_epcomb = nanmean(allnormcorr_sm_runtheta_epcomb,1);
            runpairoutput(cntpairs).allnormcorr_runtheta_epcomb = nanmean(allnormcorr_runtheta_epcomb,1);   
            runpairoutput(cntpairs).allNeventscorr_runtheta_epcomb = nansum([0;allNeventscorr_runtheta_epcomb]);
            % Get the peakcorr and peak lag from the combined normcorr_sm
            currthetacorr_sm = nanmean(allnormcorr_sm_runtheta_epcomb,1);
            currpeakcorr = nanmax(currthetacorr_sm(bins_run)); % Already smoothened, or can take +/-3 bins around peak
            if (~isnan(currpeakcorr) && ~isempty(currpeakcorr))
                currpeaklag_idx = min(find(currthetacorr_sm(bins_run) == nanmax(currthetacorr_sm(bins_run))));
                currpeaklag = runcorrtime(bins_run(currpeaklag_idx)); %in sec
            else
                currpeaklag_idx=NaN;
                currpeaklag=NaN;
            end         
            runpairoutput(cntpairs).alltheta_peakcorr_epcomb = currpeakcorr;
            runpairoutput(cntpairs).alltheta_peaklag_epcomb = currpeaklag;   
            %%runpairoutput(cntpairs).alltheta_peakcorr_epcomb = nanmean(alltheta_peakcorr_epcomb,1);
            %%runpairoutput(cntpairs).alltheta_peaklag_epcomb = nanmean(alltheta_peaklag_epcomb,1);                    
     
            
            % Rip Corr variables
            % -------------------
            runpairoutput(cntpairs).allnormcorr_rip_epcomb = nanmean(allnormcorr_rip_epcomb,1);  
            runpairoutput(cntpairs).allnormcorr_sm_rip_epcomb = nanmean(allnormcorr_sm_rip_epcomb,1);  
            runpairoutput(cntpairs).allNeventscorr_rip_epcomb = nansum([0;allNeventscorr_rip_epcomb]);
            % Get the peakcorr and peak lag from the combined normcorr_sm
            currnormcorr_sm = nanmean(allnormcorr_sm_rip_epcomb,1);  
            currpeakripcorr = nanmax(currnormcorr_sm(bins_rip)); % Already smoothened, or can take +/-3 bins around peak
            if (~isnan(currpeakripcorr) && ~isempty(currpeakripcorr))
                currpeaklagrip_idx = min(find(currnormcorr_sm(bins_rip) == nanmax(currnormcorr_sm(bins_rip))));
                currpeaklagrip = ripcorrtime(bins_rip(currpeaklagrip_idx)); %in sec
            else
                currpeaklagrip_idx = NaN;
                currpeaklagrip = NaN;
            end         
            runpairoutput(cntpairs).allrip_peakcorr_epcomb = currpeakripcorr;
            runpairoutput(cntpairs).allrip_peaklag_epcomb = currpeaklagrip;               
      
            % Place overlap 
            % --------------------
            runpairoutput(cntpairs).alloverlap_epcomb = nanmean(alloverlap_epcomb);
            
            
            % Save outside of structure format as well
            % Theta corr variables
            % ---------------------
            Sallnormcorr_sm_runtheta_epcomb(cntpairs,:) = nanmean(allnormcorr_sm_runtheta_epcomb,1);
            Sallnormcorr_runtheta_epcomb(cntpairs,:) = nanmean(allnormcorr_runtheta_epcomb,1);
            Salltheta_peakcorr_epcomb(cntpairs) = currpeakcorr;
            Salltheta_peaklag_epcomb(cntpairs) = currpeaklag;
            SallNeventscorr_runtheta_epcomb(cntpairs) = nansum([0;allNeventscorr_runtheta_epcomb]);
            % Rip Corr variables
            % -------------------
            Sallnormcorr_rip_epcomb(cntpairs,:) =  nanmean(allnormcorr_rip_epcomb,1);  
            Sallnormcorr_sm_rip_epcomb(cntpairs,:) =  nanmean(allnormcorr_sm_rip_epcomb,1);  
            Sallrip_peakcorr_epcomb(cntpairs) = currpeakripcorr;
            Sallrip_peaklag_epcomb(cntpairs) = currpeaklagrip;
            SallNeventscorr_rip_epcomb(cntpairs) = nansum([0;allNeventscorr_rip_epcomb]);
             % Place overlap 
            % --------------------
            Salloverlap_epcomb(cntpairs) = nanmean(alloverlap_epcomb);
        end
    end
    
    figure; hold on; hist(Salloverlap_epcomb);
    
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data

figdir = '/data25/sjadhav/HPExpt/Figures/Correlation/Egs/';
set(0,'defaultaxesfontsize',20);
tfont = 20;
xfont = 20;
yfont = 20;
% Plotting for indiv pairs
% --------------------------
if 1
%    for i=1:cnt       
%         if allp_shuf(i)<0.05   
     for i=1:cntpairs       
        %if Sallp_epcomb(i)<0.05 
            idx = runpairoutput_idx(i,:) 
            curroverlap = Salloverlap_epcomb(i);
            %idx = allanimindex(i,:);
            switch idx(1)
                case 1
                    pre ='HPa';
                case 2
                    pre = 'HPb';
                case 3
                    pre = 'HPc';
            end
            
            if ~isnan(curroverlap) && curroverlap>0.2 && SallNeventscorr_runtheta_epcomb(i)>=20 && SallNeventscorr_rip_epcomb(i)>=20
                
                figure; hold on; redimscreen_2versubplots;
                subplot(2,1,1); hold on;
                plot(runcorrtime, Sallnormcorr_sm_runtheta_epcomb(i,:),'r','LineWidth',3);
                plot(Salltheta_peaklag_epcomb(i), Salltheta_peakcorr_epcomb(i),'ko','MarkerSize',20,'LineWidth',2);
                
                line([0 0], [min(Sallnormcorr_sm_runtheta_epcomb(i,:)) max(Sallnormcorr_sm_runtheta_epcomb(i,:))],'Color',[0.5 0.5 0.5],'LineWidth',2);
                line([corrwin corrwin], [min(Sallnormcorr_sm_runtheta_epcomb(i,:)) max(Sallnormcorr_sm_runtheta_epcomb(i,:))],'Color',[0.5 0.5 0.5],'LineWidth',2);
                line([-corrwin -corrwin], [min(Sallnormcorr_sm_runtheta_epcomb(i,:)) max(Sallnormcorr_sm_runtheta_epcomb(i,:))],'Color',[0.5 0.5 0.5],'LineWidth',2);
                
                title(sprintf('%s Day%d Tet%d Cell%d, Tet%d Cell%d Overlap %g',...
                    pre, idx(2), idx(3), idx(4), idx(5), idx(6),roundn(curroverlap,-2)),'FontSize',20);
                %if allp(i) <0.05, str = '*'; else, str = ''; end
                %text(0.2, 1*max(Sallnormcorr_sm_runtheta_epcomb(i,:)),sprintf('ripcc %0.2f%s',allr(i),str),'FontSize',20);
                set(gca,'XLim',[-0.4 0.4]);
                xlabel('Time (sec)','FontSize',20);
                ylabel('Xcorr - RunTheta','FontSize',20);
                
                subplot(2,1,2); hold on;
                plot(ripcorrtime, Sallnormcorr_sm_rip_epcomb(i,:),'b','LineWidth',3);
                plot(Sallrip_peaklag_epcomb(i), Sallrip_peakcorr_epcomb(i),'ko','MarkerSize',20,'LineWidth',2);
                
                corrwinrip = corrwin;
                
                line([0 0], [min(Sallnormcorr_sm_rip_epcomb(i,:)) max(Sallnormcorr_sm_rip_epcomb(i,:))],'Color',[0.5 0.5 0.5],'LineWidth',2);
                line([corrwinrip corrwinrip], [min(Sallnormcorr_sm_rip_epcomb(i,:)) max(Sallnormcorr_sm_rip_epcomb(i,:))],'Color',[0.5 0.5 0.5],'LineWidth',2);
                line([-corrwinrip -corrwinrip], [min(Sallnormcorr_sm_rip_epcomb(i,:)) max(Sallnormcorr_sm_rip_epcomb(i,:))],'Color',[0.5 0.5 0.5],'LineWidth',2);
                
                title(sprintf('Nevs-rip %d ; Nevs-theta %d',...
                    SallNeventscorr_rip_epcomb(i),SallNeventscorr_runtheta_epcomb(i)),'FontSize',20);
                
                set(gca,'XLim',[-0.4 0.4]);
                xlabel('Time (sec)','FontSize',20);
                ylabel('Xcorr - Rip','FontSize',20);
                
                keyboard;
                
            end
        %end
    end
end





% ------------------
% Population Figures
% ------------------

forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

figdir = '/data25/sjadhav/HPExpt/Figures/Jan2014/';
summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',40);
    tfont = 40;
    xfont = 40;
    yfont = 40;
end

if strcmp(state,'sleep'),
    statename = 'Sleep';
else
    statename = 'Run';
end

%clr = 'c'


% Change variable names for epoch-combined data so that you can use the same code to plot as in Version 1
% ----------------------------------------------------------------------------------------------------------
% Theta Corr variables
% allnormcorr_sm_runtheta = Sallnormcorr_sm_runtheta_epcomb;
% allnormcorr_runtheta = Sallnormcorr_runtheta_epcomb;
% alltheta_peakcorr = Salltheta_peakcorr_epcomb;
% alltheta_peaklag = Salltheta_peaklag_epcomb;











if 1
    
    % 1a) Rip corrcoeff vs Total thetacorr for SWR Response
    % -----------------------------------------------------
    % Get rid of NaNs / Take Only Significant
    % Save temp
    alltheta_peaklag_tmp = Salltheta_peaklag_epcomb; % Take Peak Lag - thetacorr
    allrip_peaklag_tmp = Sallrip_peaklag_epcomb; % Take Peak Lag - ripcorr
    
    removeidxs = find((SallNeventscorr_rip_epcomb<20)|(SallNeventscorr_runtheta_epcomb<20) | (Salloverlap_epcomb<0.2)) ;    
    Sallrip_peaklag_epcomb(removeidxs)=[]; Salltheta_peaklag_epcomb(removeidxs)=[]; 
    
    figure; hold on; redimscreen_figforppt1;
    %set(gcf, 'Position',[205 136 723 446]);
    %xaxis = min(allr):0.1:max(allr);
    plot(Salltheta_peaklag_epcomb, Sallrip_peaklag_epcomb,'k.','MarkerSize',24);
    legend('Theta Xcorr Lag vs Rip Xcorr Lag');
    
    title(sprintf('Peak Lag Compare'),'FontSize',tfont,'Fontweight','normal')
    ylabel('Rip Xcorr Peak Lag ','FontSize',yfont,'Fontweight','normal');
    xlabel('Theta Xcorr Peak Lag','FontSize',xfont,'Fontweight','normal');
%     
%     % Do statistics on this popln plot
%     [r_thetavsrip,p_thetavsrip] = corrcoef(allr, alltheta_peakcorr);
%     [rsig,psig] = corrcoef(allr(sigidx), alltheta_peakcorr(sigidx));
%     [rnosig,pnosig] = corrcoef(allr(nosigidx), alltheta_peakcorr(nosigidx));
%     
%     % Regression
%     % -----------
%     [b00,bint00,r00,rint00,stats00] = regress(allr', [ones(size(alltheta_peakcorr')) alltheta_peakcorr']);
%     xpts = min(alltheta_peakcorr):0.01:max(alltheta_peakcorr);
%     bfit00 = b00(1)+b00(2)*xpts;
%     plot(xpts,bfit00,'k-','LineWidth',4);  % Theta vs Rip
%     % Do regression after shifting data to make intercept 0
%     % ------------------------------------------------------
%     allr_0 = allr-mean(allr);
%     alltheta_peakcorr_0 = alltheta_peakcorr-mean(alltheta_peakcorr);
%     [b0,bint0,r0,rint0,stats0] = regress(allr_0',[ones(size(alltheta_peakcorr_0')) alltheta_peakcorr_0']);
%     bfit0 = b0(1)+b0(2)*xpts;
%     
%     rval = roundn(r_thetavsrip(1,2),-2);
%     pval = roundn(p_thetavsrip(1,2),-4);
%     rsigval = roundn(rsig(1,2),-2);
%     psigval = roundn(psig(1,2),-4);
%     rnosigval = roundn(rnosig(1,2),-2);
%     pnosigval = roundn(pnosig(1,2),-4);
%     rsquare = roundn(stats0(1),-2);
%     preg = roundn(stats0(3),-4);
%     
%     % Shuffling
%     % ---------
%     for n=1:1000
%         idxs = randperm(length(allr));
%         shuffle = allr(idxs);
%         % Get corrcoeff of shuffle
%         [rsh,psh] = corrcoef(alltheta_peakcorr, shuffle);
%         r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
%         % Get regression of shuffle after making intercept 0 / Or Not
%         %shuffle_0 = shuffle - mean(shuffle);
%         %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(alltheta_peakcorr_0')) alltheta_peakcorr_0']);
%         [bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle', [ones(size(alltheta_peakcorr')) alltheta_peakcorr']);
%         rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
%         b_shuffle(n,:) = bsh;
%     end
%     prctile(rsquare_shuffle,99); prctile(r_shuffle,99); %figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
%     % Get regression corresponding to 99 percentile
%     idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,99));
%     idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
%     bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
%     plot(xpts,bfitsh,'k--','LineWidth',2);  % Theta vs Rip - 99% shuffle line
%     
%     % Add Info to Figure
%     % ------------------
%     set(gca,'XLim',[-4.5 8]); set(gca,'YLim',[-0.2 0.45]);
%     text(-3.8,0.4,['Npairs:' num2str(length(allr))],'FontSize',30,'Fontweight','normal');
%     text(-3.8,0.35,sprintf('R: %0.2f, pval: %0.3f, preg: %0.3f',rval,pval,preg),'FontSize',30,'Fontweight','normal');
%     text(-3.8,0.3,sprintf('Rsig: %0.2f, pval: %0.3f, Nsig: %g',rsigval,psigval,length(sigidx)),'FontSize',30,'Fontweight','normal');
%     text(-3.8,0.25,sprintf('Rnosig: %0.2f, pval: %0.3f',rnosigval,pnosigval),'FontSize',30,'Fontweight','normal');        
%     figfile = [figdir,statename,'_ThetaCovVsRipCorr_',area]
%     if savefig1==1,
%         print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
%     end

    
    
end
    
    







    
    

