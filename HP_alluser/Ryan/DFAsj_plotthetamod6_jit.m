function out = ...
    DFAsj_plotthetamod6_jit(sind, tind, excludetimes, spikes, theta, varargin)
% out = plotthetamod(spike_index, theta_index, excludeperiods, spikes, theta, options)
% Ver4 : Starting 10Feb2014 - Sync codes with everyone

% For filter framework. See also sj_plotthetamod

% plots the theta moduation histogram for the specified cell based on the
% specified index into the theta structure.
% Excluded time periods are not included in calculation.
%
% Options:
%   'nbins', # of bins (default 24)
%   'appendind', 1 or 0 -- set to 1 to append the cell ind to the
%   output [tetrode cell value].  Default 0.
%
% eg. sind = [ 2 2 18 1] or [2 2 15 1].  tind =[2 2 1]
% sj_plotthetamod([2 2 18 1], [2 2 1], [], spikes, theta);
%
% Ryan - Modified to get ideal phase-locked times based on jittered theta

%% Setup
try

figopt=0;

nbins = 24;
appendind = 0;
step_size_ms=10;

for option = 1:2:length(varargin)-1
    if isstr(varargin{option})
        switch(varargin{option})
            case 'appendind'
                appendind = varargin{option+1};
            case 'nbins'
                nbins = varargin{option+1};
            case 'step_size_ms'
                step_size_ms = varargin{option+1};
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end
    else
        error('Options must be strings, followed by the variable');
    end
end
step_size_ms;
% Spike times
if ~isempty(spikes{sind(1)}{sind(2)}{sind(3)}{sind(4)}.data)
    spiketimes = spikes{sind(1)}{sind(2)}{sind(3)}{sind(4)}.data(:,1);
else
    spiketimes=[];
end

% Eeg sample times and Phase
try
    eeg_times = geteegtimes(theta{tind(1)}{tind(2)}{tind(3)});
catch
    disp('Stopped in DFAsj_plotthetamod');
    [tind(1) tind(2) tind(3)]
    keyboard; 
   
end
theta_phases = theta{tind(1)}{tind(2)}{tind(3)}.data(:,2);


if ~isempty(spiketimes)
    goodspikes = ~isExcluded(spiketimes, excludetimes);
else
    goodspikes = [];
end

if length(goodspikes)~=0
    spike_phases = theta_phases(lookup(spiketimes, eeg_times));  
    spike_phases = double(spike_phases(goodspikes)) / 10000;  % If no spikes, this will be empty
else
    spike_phases = [];
end

numgoodspikes = sum(goodspikes);

if length(spike_phases)>1
	
%% "Jittered" Phase Section
% ---------------
% This section acquires kappa several different delays at the theta
% frequency.

min_delay = -100/1000; % 100ms, passsed as -0.1sec
step_size = step_size_ms/1000;
max_delay = 100/1000;

% Acquire jittered wave data ... jittered_wave_dat has a progression of
% shifted wave data (e.g. theta_phase), row by row. It's built in such a
% way the first row is time shifted by 
[delay_eeg_times, delays, new_boundary] = ...
    generateRowsWithDelayTimes(eeg_times, min_delay, step_size, max_delay);

goodspikes_with_boundary = goodspikes & ...
    (spiketimes > new_boundary.start & spiketimes < new_boundary.stop);
numgoodspikes_with_boundary = sum(goodspikes_with_boundary);

% Lookup each spike's time in the jittered times
phases_at_delay = zeros( numel(delays), numgoodspikes_with_boundary);
kappa_at_delay = zeros( numel(delays), 1);
for i = 1:numel( delays )
   
    % Time series is delayed by an index number equivalent to the time shift.
    % Whatever index is found, we pull the value on the normal eeg data time
    % series.
    time_series_at_delay = delay_eeg_times(i,:);
    
    % Get all the theta phases
    indices_to_grab = lookup(spiketimes(goodspikes_with_boundary), ...
        time_series_at_delay);
    phases_at_delay(i,:) = double(theta_phases(indices_to_grab)) / 10000;
	
	[~, kappa_at_delay(i)] = circ_vmpar(phases_at_delay(i,:));
    
end
 
%% Stats and Fit
% ---------------
    
    % Rayleigh and Modulation: Originally in lorenlab Functions folder
    stats = rayleigh_test(spike_phases); % stats.p and stats.Z, and stats.n
    [m, ph] = modulation(spike_phases);
    phdeg = ph*(180/pi);
    % Von Mises Distribution - From Circular Stats toolbox
    [thetahat, kappa] = circ_vmpar(spike_phases); % Better to give raw data. Can also give binned data.
    thetahat_deg = thetahat*(180/pi);
    
    
    [prayl, zrayl] = circ_rtest(spike_phases); % Rayleigh test for non-uniformity of circular data
    
    % Make finer polar plot and overlay Von Mises Distribution Fit.
    % Circ Stats Box Von Mises pdf uses a default of 100 angles/nbin
    % -------------------------------------------------------------
    nbins = 50;
    bins = -pi:(2*pi/nbins):pi;
    count = histc(spike_phases, bins);
    
    % Make Von Mises Fit
    alpha = linspace(-pi, pi, 50)';
    [pdf] = circ_vmpdf(alpha,thetahat,kappa);
    
else
    
    stats=0;
    m=0;
    phdeg=0;
    kappa=0;
    thetahat_deg=0;
    prayl=0;
    zrayl=0;
    alpha=0;
    pdf=0;
	
	phases_at_delay=[];
	kappa_at_delay=[];
	delays=[];
  
end


%% Output
% ------

out.index = sind;
out.tetindex = tind;
out.sph = spike_phases;
out.Nspikes = numgoodspikes;
% Output stats also, although you will have to redo this after combining epochs
% Rayleigh test
out.stats = stats;
out.modln = m;
out.phdeg = phdeg;
% Von Mises Fit
out.kappa = kappa;
out.thetahat_deg = thetahat_deg;
out.prayl = prayl;
out.zrayl = zrayl;
out.alpha = alpha;
out.vmpdf = pdf;
% Kappas based on time delays, Tau
out.phases_at_delay = phases_at_delay;      % pass all spike phases at particular delay, where each delay is on ith row
out.kappa_at_delay = kappa_at_delay;
out.time_delays = delays;                   % list of all time delays, including zero

    
% if ~isempty(sph)
%     out.sph = sph;   
% else
%     out.sph = zeros(0,1);
% end
    



if (figopt)

    
    % Plot
    % ----
    bins = -pi:(2*pi/nbins):pi;
    count = histc(spike_phases, bins);
    figure; redimscreen_2horsubplots
    subplot(1,2,1);
    hold on;
    out = bar(bins, count, 'hist'); set(gca, 'XTick', [-pi, -pi/2, 0, pi/2, pi]);
    set(gca,'XLim',[-pi-0.5 pi]); ylabel('NSpikes'); set(out,'FaceColor','r');
    title(sprintf('cell %d %d %d %d, eegtet %d theta, mod %f, peakph %g, pval %f', sind, tind(3), m, phdeg, stats.p));
    
    % Shantanu - add a plot with %tage of spikes
    % -------------------------------------------
    subplot(1,2,2);
    hold on;
    totalspks = sum(count);
    countper = (count./totalspks)*100;
    out = bar(bins, countper, 'hist');  set(gca, 'XTick', [-pi, -pi/2, 0, pi/2, pi]);
    set(gca,'XLim',[-pi-0.5 pi]); ylabel('% of Spikes'); set(out,'FaceColor','r');
    %title(sprintf('cell %d %d %d %d, eegtet %d theta, mod %f, peakph %g, pval %f', sind, tind(3), m, phdeg, stats.p));
    
    % Polar plot  - use rose or polar:
    % --------------------------------
    % [t,r] = rose(sph): input is angle in radians for each spike
    % polar(t,r);
    
    % My range is -pi to pi. This will plot from 0 to 2*pi, and it seems to do this automatically.
    % No need to adjust range %sphn = sph + pi;
    
    [eeg_times,r] = rose(spike_phases);
    figure;  redimscreen_figforppt1;
    polar(eeg_times,r,'r'); hold on;
    % The peak phase angle
    lims = get(gca,'XLim');
    radius = lims(2);
    xx = radius .* cos(ph); yy = radius .* sin(ph);
    line([0 xx], [0 yy],'LineWidth',4,'Color','k'); %plot(xx,yy,'ko','MarkerSize',4);
    title(sprintf('cell %d %d %d %d, eegtet %d theta, mod %f, peakph %g, pval %f', sind, tind(3), m, phdeg, stats.p));
    
    
    % Make finer polar plot and overlay Von Mises Distribution Fit.
    % Circ Stats Box Von Mises pdf uses a default of 100 angles/nbin
    % -------------------------------------------------------------
    
    figure; redimscreen_2horsubplots
    subplot(1,2,1);
    hold on;
    out = bar(bins, count, 'hist'); set(gca, 'XTick', [-pi, -pi/2, 0, pi/2, pi]);
    set(gca,'XLim',[-pi pi]); ylabel('NSpikes');
    set(out,'FaceColor','r'); set(out,'EdgeColor','r');
    %pdf = pdf.*(max(count)/max(pdf));
    % Instead of maximum - match values at a bin, maybe close to peak
    binnum = lookup(thetahat,alpha);
    pdf = pdf.*(count(binnum)/pdf(binnum));
    plot(alpha,pdf,'k','LineWidth',3,'Color','k');
    title(sprintf('cell %d %d %d %d, eegtet %d theta, Kappa %f, prefang %g, pval %f', sind, tind(3), kappa, thetahat_deg, prayl));
    
    subplot(1,2,2); hold on;
    
    totalspks = sum(count);
    countper = (count./totalspks)*100;
    out = bar(bins, countper, 'hist');  set(gca, 'XTick', [-pi, -pi/2, 0, pi/2, pi]);
    set(gca,'XLim',[-pi pi]); ylabel('% of Spikes');
    set(out,'FaceColor','r'); set(out,'EdgeColor','r');
    pdf = pdf.*(countper(binnum)/pdf(binnum));
    plot(alpha,pdf,'k','LineWidth',3,'Color','k');
    title(sprintf('Nspikes %d', totalspks));
    
end

catch ME
	save errorstate_plotthetamod
	throw(ME);
end
%pause
