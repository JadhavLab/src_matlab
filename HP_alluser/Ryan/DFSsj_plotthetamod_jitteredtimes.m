% Ver7 : Starting 10Feb2014 - Sync codes with everyone

% Theta modulation of cells.
% Also see sj_plotthetamod
% Gather data like DFSsj_getcellinfo and DFSsj_HPexpt_xcorrmeasures2

% Ryan - Script is undergoing modifcation in order to discover theta lag
% between the regions in theta,

%figfolder= input('Name your figure folder:','s');


% for val = [5]
% for runscript = [1 0]


clear; %close all;
%clearvars -except val runscript leg_txt

runscript=0;
% Major Options
if ~exist('runscript','var')
    runscript = true;
end
savedata = true; % save data option - only works if runscript is also on
figopt1 = false; % Figure Options - Indicidual cells


%% Selection of Cell Types

%d = filesep;
%savedir = ['~' d 'Data' d 'Local_Analysis' d];
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
%savedir = '/opt/data15/gideon/HP_ProcessedData/';

step_size_ms=10
val=5;
% if ~exist('val','var') || isempty(val)
%     fprintf(['\n\nArea NOT specified\n' ...
%         '1 = PFC_all \n2 = CA1 \n3 = PFC SWR-excited \n' ...
%         '4 = PFC SWR-inhibited \n5 = PFC SWR-modulated \n' ...
%         '6 = PFC ripple-unmodulated \n\n']);
%     val = input('Please provide a value, corresponding to the above menu:');
% end

%fprintf('Running with val %d, runscript %d ... ', val, runscript);

switch val
    case 1
        savefile = [savedir 'HP_thetamod_PFC_alldata_X6_jit',num2str(step_size_ms)]
        area = 'PFCall'; clr = 'k'; % PFC
    case 2
        savefile = [savedir 'HP_thetamod_CA1_alldata_X6_jit',num2str(step_size_ms)]
        area = 'CA1';  clr = 'g';% CA1
    case 3
        savefile = [savedir 'HP_thetamod_PFCripEXC_alldata_X6_jit',num2str(step_size_ms)]
        area = 'PFCExc'; clr = 'r'; % PFC SWR-excited
    case 4
        savefile = [savedir 'HP_thetamod_PFCripINH_alldata_X6_jit',num2str(step_size_ms)]
        area = 'PFCInh'; clr = 'b'; % PFC SWR-inhibited
    case 5
        savefile = [savedir 'HP_thetamod_PFCripMod_alldata_X6_jit',num2str(step_size_ms)]
        area = 'PFCripMod'; clr = 'c'; % PFC SWR-modulated
    case 6
        savefile = [savedir 'HP_thetamod_PFCripUnMod_alldata_X6_jit',num2str(step_size_ms)]
        area = 'PFCripUnMod'; clr = 'y'; % PFC SWR-Unmodulated
end

savefig1=0;

%% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days

%% Filtering
% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    %     animals = {'HPa','HPb','HPc','Nadal','Rosenthal','Borg'};
    animals = {'HPa','HPb','HPc','Ndl','Rtl','Brg'}; %WAITING FOR DATA TO
    %     STOP SYNCING TO SERVER
    %     animals = {'HPa'};
    %     animals = {'Rosenthal'};
    
    % animals = {'HPa','HPb', 'HPc','Ndl'};
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    %dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    %runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    runepochfilter = 'isequal($type, ''run'')';
    % Cell filter
    % -----------
    switch val
        case 1
            %cellfilter = 'strcmp($area, ''PFC'')'; % This includes all, including silent cells. Spike criterion later
            % 3 Apr 2014: Remove FS
            cellfilter = 'strcmp($area, ''PFC'') && strcmp($FStag, ''n'')'; % && strcmp($FStag, ''n'')';
        case 2
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && strcmp($FStag, ''n'')) '; % This includes all, including silent cells. Spike criterion later
            
        case 3
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc'') && strcmp($FStag, ''n''))';
        case 4
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh'') && strcmp($FStag, ''n''))';
        case 5
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n''))';
        case 6
            cellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''n'')  && strcmp($FStag, ''n'') )';
            
    end
    % For more exclusive choosing, use $tag
    % Take care of number of spikes while gathering data
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % Change on 02-12-2014, for ver4. Change to absvel criterion
    % ------------------------------------------------------------
    timefilter_theta = {{'DFTFsj_getvelpos', '(($absvel >= 5))'},...
        {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',2} };
    
    %     timefilter_theta = { {'DFTFsj_getlinstate', '(($state ~= -1) & (abs($linearvel) >= 5))', 6},...
    %         {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',2} };
    
    % Can also use gethighthetatimes2 -
    
    % EEG Filter
    % ----------
    % Options are sametet, tetfilter, maxvar, file. kk also added "static tet"
    % I am going to use CA1Ref tetrode. Can pass along the file, or use $descrip
    ca1reftetfilter = '( isequal($descrip, ''CA1Ref'') || isequal($descrip, ''reftet'') )';
    % Both the normal and the Gnd files are the same for Ref electrode
    % theta or thetagnd. Make sure to add thetagnd to "iseeg" field
    eegfilter = {'sj_geteegtet', 'theta', 'tetfilter',ca1reftetfilter};
    
    % Iterator
    % --------
    iterator = 'singlecelleeganal';
    
    % Filter creation
    % ----------------
    %modf = createfilter('animal',animals,'days',dayfilter,'epochs',runepochfilter, 'excludetimefilter', timefilter_theta, 'cells',...
    %    cellfilter,'eegtetrodes', eegfilter, 'iterator', iterator);
    modf = createfilter('animal',animals,'epochs',runepochfilter, ...
        'excludetimefilter', timefilter_theta, 'cells',...
        cellfilter,'eegtetrodes', eegfilter, 'iterator', iterator);
    
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    warning('off');
    modf = setfilterfunction(modf,...
        'DFAsj_plotthetamod6_jit',{'spikes','theta'},'step_size_ms',step_size_ms); % Corresponding function is sj_plotthetamod
    warning('on');
    
    % Run analysis
    % ------------
    modf = runfilter(modf);
    disp('Finished running filter script');
    %if runscript==1
    %    fprintf('\n Filter Format Done');
    %end
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile);
    end
    
else
    
    load(savefile);
end % end runscript

if ~exist('savedata')
    %     continue;
    return;
end


% -------------------------  Filter Format Done -------------------------


%% Gather Data?
% --------------------------------------------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------

gatherdata = 0; savegatherdata = 1;
[y, m, d] = datevec(date);

%variable_path_to_savedir = '/home/jadhavlab/Data/';
%savedir = [variable_path_to_savedir 'Local_Analysis/'];

% Select the type of data to gather, based on the filtering options
% provided above
switch val
    case 1
        gatherdatafile = [savedir ...
            'HP_thetamod_PFC_alldata_Nspk50_gather_X6_jit',num2str(step_size_ms)] % PFC cells to Hipp theta
    case 2
        gatherdatafile = [savedir ...
            'HP_thetamod_CA1_alldata_Nspk50_gather_X6_jit',num2str(step_size_ms)] % CA1 cells to Hipp theta
    case 3
        gatherdatafile = [savedir ...
            'HP_thetamod_PFC_ripEXC_alldata_Nspk50_gather_X6_jit',num2str(step_size_ms)] % CA1 cells to Hipp theta
    case 4
        gatherdatafile = [savedir ...
            'HP_thetamod_PFC_ripINH_alldata_Nspk50_gather_X6_jit',num2str(step_size_ms)] % CA1 cells to Hipp theta
    case 5
        gatherdatafile = [savedir ...
            'HP_thetamod_PFC_ripMod_alldata_Nspk50_gather_X6_jit',num2str(step_size_ms)] % CA1 cells to Hipp theta
    case 6
        gatherdatafile = [savedir ...
            'HP_thetamod_PFC_ripUnMod_alldata_Nspk50_gather_X6_jit',num2str(step_size_ms)] % CA1 cells to Hipp theta
end

%% Gather Data!
if gatherdata
    disp('Gathering data ...');
    % Parameters if any
    % -----------------
    nbins = 50;
    % Circ Stats Box Von Mises pdf uses a default of 100 angles/nbin
    % -------------------------------------------------------------
    
    
    %% Gather all cell counts and spike_phases for all animals
    % Ryan - modified this section to also gather all lists of phases at
    % specified delays
    
    total_cell=0; % Variable only counts to total number of cells processed into the all* data structures, not how many kept based on Nspike, so changed name to reflect seeming function - Ryan
    allanimindex=[]; alldata=[]; all_Nspk=[];
    
    for an = 1:length(modf)
        if ~isempty(modf(an).output)
            
            for i=1:length(modf(an).output{1})
                % Check for empty output
                if (modf(an).output{1}(i).Nspikes > 0)
                    total_cell=total_cell+1;
                    anim_index{an}(total_cell,:) = modf(an).output{1}(i).index;
                    % Only indexes
                    animindex=[an modf(an).output{1}(i).index]; % Put animal index in front
                    allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
                    % Data
                    alldata{total_cell} = modf(an).output{1}(i).sph; % Only get spike phases. Compute evrything else after combining epochs
                    all_Nspk(total_cell) = modf(an).output{1}(i).Nspikes;
                    all_prayl(total_cell) = modf(an).output{1}(i).prayl;
                    
                    allphases_at_delay{total_cell} = ...
                        modf(an).output{1}(i).phases_at_delay;
                    
                    delays = ...
                        modf(an).output{1}(i).time_delays;
                    
                end
            end
            
        end
        
        %% Consolidate single cells across epochs.
        % 2 methods: see DFSsj_getcellinfo
        % and DFSsj_xcorrmeasures2
        % Here, I will use combined animal-index. Prevents having to loop over animals
        % ----------------------------------------------------------------------------
        allthetamod = struct;
        dummyindex=allanimindex;  % all anim-day-epoch-tet-cell indices
        cntcells=0; cntdiscard=0;
        
        for i=1:size(alldata,2)
            animdaytetcell=allanimindex(i,[1 2 4 5]);
            ind=[];
            while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
                ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
                dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
                % so you could find the next one
            end
            
            % Gather everything for the current cell across epochs
            currsph=[]; currNspk=0;
            currSPH_at_delay = [];
            for r=ind
                
                %if all_prayl(r) > 0.05
                %    continue;
                %end
                
                currNspk = currNspk + all_Nspk(r);
                currsph = [currsph; alldata{r}];
                
                currSPH_at_delay = [currSPH_at_delay, allphases_at_delay{r}];
                
            end
            
            if currNspk >= 50 % based on Siapas 2005; maybe it should be 100
                cntcells = cntcells + 1;
                
                allthetamod_idx(cntcells,:)=animdaytetcell;
                allthetamod(cntcells).index=animdaytetcell;
                allthetamod(cntcells).sph=currsph; % Remember, this is seperated across epochs. Can take mean
                allthetamod(cntcells).Nspk=currNspk;
                
                allthetamod(cntcells).sph_at_delay = currSPH_at_delay;
                allthetamod(cntcells).delays = delays;
                
                
            else % Ryan - added else condition to count discarded cells -- the cntdiscard variable atop was unusued
                cntdiscard = cntdiscard + 1;
            end
        end
        
        
        %% Calculations
        % - can incorporate in loop above as well
        % ------------
        for i=1:cntcells
            
            %% Zero-delay calculations
            sph = allthetamod(i).sph;
            % Rayleigh test and Modulation:
            stats = rayleigh_test(sph); % stats.p and stats.Z, and stats.n
            [m, ph] = modulation(sph);
            phdeg = ph*(180/pi);
            % Von Mises Distribution - From Circular Stats toolbox
            [thetahat, kappa] = circ_vmpar(sph); % Better to give raw data. Can also give binned data.
            thetahat_deg = thetahat*(180/pi);
            [prayl, zrayl] = circ_rtest(sph); % Rayleigh test for non-uniformity of circular data
            % Von Mises Fit - Use nbins defined above
            bins = -pi:(2*pi/nbins):pi;
            count = histc(sph, bins);
            % Make Von Mises Fit
            alpha = linspace(-pi, pi, 50)';
            [pdf] = circ_vmpdf(alpha,thetahat,kappa);
            % Another way of doing mean phase
            A=mean(exp(j*sph)); % problem with i - clashes with variable. so use j.
            meancos=real(A);
            meansin=imag(A);
            meanphase=atan2(meansin,meancos); % Exactly the same as von mises fit
            
            %% All-delay calculations
            kappa_at_delay=[]; prayl_at_delay=[]; zrayl_at_delay=[];
            for j = 1:size(allthetamod(cntcells).sph_at_delay,1)
                
                spike_phases = allthetamod(i).sph_at_delay(j,:);
                
                % Compute kappas at delays
                [~, kappa_at_delay(j)] = ...
                    circ_vmpar(spike_phases);
                % Also do rayl computan just in case
                [prayl_at_delay(j), zrayl_at_delay(j)] = circ_rtest(sph);
                
            end
            
            % Find delay location of max kappa
            [max_kappa,delay_ind] = max(kappa_at_delay);
            if ~isempty(delay_ind), delay_ind = delay_ind(end); end
            
            % Get the spikes-thetaphase histogram at best delay
            count_bestdelay = histc(allthetamod(i).sph_at_delay(delay_ind,:), bins);
            
            
            %% Package Calculations
            
            allthetamod(i).thetahist = count; % Theta histogram plot
            allthetamod(i).thetahistnorm = count./max(count); % Normalized - Theta histogram plot
            allthetamod(i).thetaper = (count./sum(count))*100; % Histogram in units of percentage of spikes
            allthetamod(i).stats = stats;
            allthetamod(i).modln = m;
            allthetamod(i).phdeg = phdeg;
            allthetamod(i).kappa = kappa;
            allthetamod(i).thetahat = thetahat; % From von Mises fit - use this
            allthetamod(i).thetahat_deg = thetahat_deg; % From von Mises fit - use this
            allthetamod(i).prayl = prayl;
            allthetamod(i).zrayl = zrayl;
            allthetamod(i).alpha = alpha;
            allthetamod(i).vmpdf = pdf;
            allthetamod(i).meanphase = meanphase;
            
            allthetamod(i).kappa_at_delay = kappa_at_delay;
            allthetamod(i).best_delay = allthetamod(i).delays(delay_ind);
            allthetamod(i).best_delay_ind = delay_ind;
            allthetamod(i).max_kappa = max_kappa;
            allthetamod(i).count_bestdelay = count_bestdelay;
            allthetamod(i).prayl_at_delay = prayl_at_delay;
            allthetamod(i).zrayl_at_delay = zrayl_at_delay;
            
            allthetamod(i).anim = allthetamod(i).index(1); allanim(i) = allthetamod(i).index(1);
            allthetamod(i).days = allthetamod(i).index(2); alldays(i) = allthetamod(i).index(2);
            
        end
    end
    %% Save
    % -----
    if savegatherdata == 1
        % 		clear modf;
        save(gatherdatafile);
    end
    
else % gatherdata=0, import pre-gathered data for subsequent plotting!
    
    load(gatherdatafile);
    
end % end gather data

%add to "super" variable
% super{reg}=modf;

%% ------- PLOT SECTION ---------
% Population Data
% ----------------------------
switch val
    case 2
        area = 'CA1', clr = 'g';
    case 1
        area = 'PFCall', clr = 'k';
    case 3
        area = 'PFCExc', clr = 'r';
    case 4
        area = 'PFCInh', clr = 'b';
    case 5
        area = 'PFCripMod', clr = 'c';
    case 6
        area = 'PFCripUnMod', clr = 'y';
end



% Get data + only from cells that are significantly phase-locked at delay=0
% --------------------------------------------------------------------------
allkappamat=[]; allbestdelay = []; allbestdelay_ind=[];
allkappas = []; allsigkappas = [];

% The following are for only sig ph-locked cells
allsigphases = []; cntsig = 0;
allsigkappamat=[]; allsigbestdelay = []; allsigbestdelay_ind=[]; allcount_sigbestdelay=[];
allZ = []; allsigZ = []; allm=[];
allsph =[]; allthetahist=[]; allthetahistnorm=[]; allthetaper=[];
all_sph_at_delay=[];

for i = 1:numel(allthetamod)
    allkappas(i) = allthetamod(i).kappa;
    allZ(i) = allthetamod(i).zrayl;
    allkappamat = [allkappamat; allthetamod(i).kappa_at_delay];
    allbestdelay = [allbestdelay; allthetamod(i).best_delay];
    
    if (allthetamod(i).prayl < 0.05)
        cntsig = cntsig+1;
        %ncells_days_sig(currday) = ncells_days_sig(currday)+1;
        allsigphases(cntsig) = allthetamod(i).meanphase; % Can use mean phase (atan) or thetahat_deg (from von Mises distribution)
        allsigkappas(cntsig) = allthetamod(i).kappa;
        allsigZ(cntsig) = allthetamod(i).zrayl;
        allm(cntsig) = allthetamod(i).modln;
        allsph = [allsph; allthetamod(i).sph]; % All spikes pooled
        
        allthetahist(cntsig,:) = allthetamod(i).thetahist;
        allthetaper(cntsig,:) = allthetamod(i).thetaper;
        allthetahistnorm(cntsig,:) = allthetamod(i).thetahistnorm;
        allsigkappamat(cntsig,:) = allthetamod(i).kappa_at_delay;
        allsigbestdelay(cntsig) = allthetamod(i).best_delay;
        allsigbestdelay_ind(cntsig) = allthetamod(i).best_delay_ind;
        allcount_sigbestdelay(cntsig,:) = allthetamod(i).count_bestdelay;
        all_sph_at_delay{cntsig} = allthetamod(i).sph_at_delay;  % Can Use for plotting example
    end
    
end

cntsig 

%nbins=50;
%bins = -pi:(2*pi/nbins):pi;
%count = histc(sph, bins);

%% Plot distributions
savefig1=0;
forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

%figdir = '/data25/sjadhav/HPExpt/Figures/RevFig1/';
figdir = '/data25/sjadhav/HPExpt/Figures/ThetaMod/ThetaModJit/';
summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end

% All neurons
usedelays = flipud(allthetamod(1).delays);
figure; hold on; redimscreen_figforppt1;
alldistr = hist(allbestdelay,ceil(numel(usedelays)/1));
h=bar(usedelays*1000, alldistr);
set(h(1),'facecolor',clr); set(h(1),'edgecolor',clr)
%axis([min(allthetamod(1).delays)-0.02 max(allthetamod(1).delays)+0.02 -inf inf]);
axis([1000*min(allthetamod(1).delays)-20 1000*max(allthetamod(1).delays)+20 -inf max(alldistr)+1]);

%title([area ', All neu: Delay of max kappa']);
title([area ', All neu: Best phase-locking delay']);
xlabel('Delay (ms)'); ylabel('No. of neurons')

% Only significant neurons

figure; hold on; redimscreen_figforppt1;
alldistr = hist(allsigbestdelay,numel(usedelays));
h=bar(usedelays*1000, alldistr);
set(h(1),'facecolor',clr); set(h(1),'edgecolor',clr)
%axis([1000*min(allthetamod(1).delays)-20 1000*max(allthetamod(1).delays)+20 -inf inf]);
axis([1000*min(allthetamod(1).delays)-20 1000*max(allthetamod(1).delays)+20 -inf max(alldistr)+1]);
%title([area ', All neu: Delay of max kappa']);
title([area ', SIG Neu: Best phase-locking delay']);
xlabel('Delay (ms)'); ylabel('No. of neurons')


nanmean(allsigbestdelay)*1000, nansem(allsigbestdelay)*1000
figfile = [figdir,area,'_best_thetphase_delay']
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
end

keyboard;

% ranksum(allsigbestdelay, Unmodallsigbestdelay) % p = 0.81


if ~exist('leg_txt','var')
    leg_txt = {};
    leg_txt{1} = [area ' mean kappa curve' ];
else
    leg_txt{numel(leg_txt) + 1} = [area ' mean kappa curve' ];
end
legend(leg_txt);


% %% Alternative plots - plot the actual kappa
% % ------------------------------------------
% % Plot of all kappa values at delay per cell
%
% figure; gcf;
% for i = 1:cntcells
%     plot(allthetamod(i).delays, allthetamod(i).kappa_at_delay); hold on;
% end
% title([area ', All Cell Kappa @ Delay Values']);
% xlabel('milliseconds');
% ylabel('kappa value');
% axis([min(allthetamod(1).delays) max(allthetamod(1).delays) -inf inf]);
%
% % Plot meaned distribution
% %
%
% mean_K = mean(allkappamat,1);
% ste_K = std(allkappamat,1)/sqrt(size(allkappamat,2));
%
% figure(2);clf;hold on;
% errorbar(allthetamod(1).delays, mean_K, ste_K);
% xlabel('Delay (ms)'); ylabel('Kappa Concentration Value');
% axis([min(allthetamod(1).delays) max(allthetamod(1).delays) -inf inf]);





%
%     end; end;

% mkdir(figfolder);
% g=groot; fignum = numel(g.Children);
% for i = 1:fignum
%     figfile = [figfolder '/' 'fig' num2str(i)];
%     print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
% end





%% "Graveyard" Code
% collective section of unusued code

% %% Single Cell Options
% %%% ------------------------------
% % Individual Cells
% % ------------------------------
% figdir = '~/Data/Figures';
% set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
% forppr=1;
% if forppr==1
%     set(0,'defaultaxesfontsize',16);
%     tfont = 18; % title font
%     xfont = 16;
%     yfont = 16;
% else
%     set(0,'defaultaxesfontsize',24);
%     tfont = 28;
%     xfont = 20;
%     yfont = 20;
% end
%
% %% Figures: Single Cell and Theta
% figopt1=0;
% if (figopt1)
%     for i=1:cntcells
%
%         curridx = allthetamod(i).index;
%         switch curridx(1)
%             case 1
%                 prefix = 'HPa';
%             case 2
%                 prefix = 'HPb';
%             case 3
%                 prefix = 'HPc';
%             case 4
%                 prefix = 'Ndl';
%                 case 5
%                 prefix = 'Rtl';
%                 case 6
%                 prefix = 'Brg';
%         end
%         day = curridx(2); tet = curridx(3); cell = curridx(4);
%         % To control plotting
%         %if curridx(1)==3 && sig_shuf==1
%
%
%             sph = allthetamod(i).sph;
%             Nspk = allthetamod(i).Nspk;
%             thetahist = allthetamod(i).thetahist; % Theta histogram plot
%             thetahistnorm = allthetamod(i).thetahistnorm; % Normalized - Theta histogram plot
%             thetaper = allthetamod(i).thetaper; % Histogram in units of percentage of spikes
%             stats = allthetamod(i).stats;
%             m = allthetamod(i).modln;
%             phdeg = allthetamod(i).phdeg;
%             kappa = allthetamod(i).kappa;
%             thetahat_deg = allthetamod(i).thetahat_deg; % From von Mises fit - use this
%             prayl = allthetamod(i).prayl;
%             zrayl = allthetamod(i).zrayl;
%             alpha = allthetamod(i).alpha;
%             pdf = allthetamod(i).vmpdf;
%             meanphase = allthetamod(i).meanphase;
%
%             countper = thetaper;
%             bins = -pi:(2*pi/nbins):pi;
%             ph = phdeg*(pi/180)
%
%             figure; hold on;
%             set(gcf,'Position',[970 86 925 1000]); % Vertical figure at right edge of screen
%             %set(gcf,'Position',[750 90 1145 1000]); % For 2 X 2 plot
%             %redimscreen_2versubplots;
%             % subplot(2,2,1); hold on; % Raster
%             % Cant do raster without getting data for each individual theta cycle
%
%             %         subplot(3,1,1); hold on; % Hist with Nspikes
%             %         out = bar(bins, thetahist, 'hist'); set(gca, 'XTick', [-pi, -pi/2, 0, pi/2, pi]);
%             %         set(gca,'XLim',[-pi pi]); ylabel('NSpikes');
%             %         set(out,'FaceColor','r'); set(out,'EdgeColor','r');
%             %         %pdf = pdf.*(max(count)/max(pdf));
%             %         % Instead of maximum - match values at a bin, maybe close to peak
%             %         binnum = lookup(thetahat,alpha);
%             %         pdf = pdf.*(count(binnum)/pdf(binnum));
%             %         plot(alpha,pdf,'k','LineWidth',3,'Color','k');
%
%
%             %subplot(2,1,1); hold on; % Hist with percentage of spikes
%             out = bar(bins, thetaper, 'hist'); set(gca, 'XTick', [-pi, -pi/2, 0, pi/2, pi]);
%             set(gca,'XLim',[-pi pi]); ylabel('NSpikes');
%             set(gca,'XLim',[-pi pi]); ylabel('% of Spikes');
%             set(out,'FaceColor','r'); set(out,'EdgeColor','r');
%             binnum = lookup(thetahat,alpha);
%             pdf = pdf.*(countper(binnum)/pdf(binnum));
%             plot(alpha,pdf,'k','LineWidth',3,'Color','k');
%             title(sprintf('%d %d %d %d, Kappa%f, prefang%g, pval%f', curridx, kappa, thetahat_deg, prayl));
%             %title(sprintf('Nspikes %d', totalspks));
%
% %             subplot(2,1,2); hold on; % Polar plot
% %             [t,r] = rose(sph);
% %             polar(t,r,'r'); hold on;
% %             % The peak phase angle
% %             lims = get(gca,'XLim');
% %             radius = lims(2);
% %             xx = radius .* cos(ph); yy = radius .* sin(ph);
% %             line([0 xx], [0 yy],'LineWidth',4,'Color','k');
% %             title(sprintf('%s Day %d Tet %d Cell %d', prefix, day, tet, cell),'FontSize',tfont,'Fontweight','normal');
%
%
%             figfile = [figdir,area,'EgThetamod_',num2str(i)];
% %             keyboard;
%
%
%
%         %print('-dpdf', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
%
%
%
%     end % end cntcells
%
% end % end if figopt
%
%
% %%% ------------------------------
%
% %% Population Figure Options
% % ------------------------------
% savefig1=0;
% switch val
%     case 2
%         area = 'CA1', clr = 'g';
%     case 1
%         area = 'PFCall', clr = 'k';
%     case 3
%         area = 'PFCExc', clr = 'r';
%     case 4
%         area = 'PFCInh', clr = 'b';
%     case 5
%         area = 'PFCripMod', clr = 'c';
%     case 6
%         area = 'PFCripUnMod', clr = 'y';
% end
%
% %% Calculate Population Data
% % ------------------------------
%
% allsigphases = []; cntsig = 0;
% allkappas = []; allsigkappas = [];
% allZ = []; allsigZ = []; allm=[];
% allsph =[]; allthetahist=[]; allthetahistnorm=[];
%
% days = unique(alldays);
% anim = unique(allanim);
% % Force days to got from 1-10. For Ndl, you will push days by 7
% days = 1:12;
% ncells_days = zeros(length(days),1);
% ncells_days_sig = zeros(length(days),1);
%
% for i = 1:length(allthetamod)
%     allkappas(i) = allthetamod(i).kappa;
%     allZ(i) = allthetamod(i).zrayl;
%
%     curranim = allthetamod(i).anim;
%     currday = allthetamod(i).days;
%     if curranim==4
%         currday = currday-7; % For Ndl, days start from 8
%     end
%     ncells_days(currday) = ncells_days(currday)+1;
%
%     if (allthetamod(i).prayl < 0.05)
%         cntsig = cntsig+1;
%         ncells_days_sig(currday) = ncells_days_sig(currday)+1;
%         allsigphases(cntsig) = allthetamod(i).meanphase; % Can use mean phase (atan) or thetahat_deg (from von Mises distribution)
%         allsigkappas(cntsig) = allthetamod(i).kappa;
%         allsigZ(cntsig) = allthetamod(i).zrayl;
%         allm(cntsig) = allthetamod(i).modln;
%         allsph = [allsph; allthetamod(i).sph]; % All spikes pooled
%
%         allthetahist(cntsig,:) = allthetamod(i).thetahist;
%         allthetaper(cntsig,:) = allthetamod(i).thetaper;
%         allthetahistnorm(cntsig,:) = allthetamod(i).thetahistnorm;
%
%     end
% end
%
% %% Population Figures
% % --------------------
% forppr = 1;
% % If yes, everything set to redimscreen_figforppr1
% % If not, everything set to redimscreen_figforppt1
%
% figdir = '~/Data/Figures';
% summdir = figdir;
% set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
%
% if forppr==1
%     set(0,'defaultaxesfontsize',16);
%     tfont = 18; % title font
%     xfont = 16;
%     yfont = 16;
% else
%     set(0,'defaultaxesfontsize',24);
%     tfont = 28;
%     xfont = 20;
%     yfont = 20;
% end
%
% %% 1) Histogram of mean phases
% % ----------------------------
% if 1
%
%     figure; hold on;
%     if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
%
%     nbins=24; % For population plots, reduce nbins
%     binsp = -pi:(2*pi/nbins):pi;
%
%     N = histc(allsigphases,binsp);
%     N=N(1:(end-1));
%     bins_plot = binsp(1:(end-1));
%     bins_plot = bins_plot + (binsp(2)-binsp(1))/2;
%     h = bar([bins_plot bins_plot+2*pi],[N , N],'histc');
%     set(h(1),'facecolor',clr)
%     set(h(1),'edgecolor',clr)
%     xlabel(['Phase'],'FontSize',xfont,'Fontweight','normal');
%     ylabel(['No. of cells'],'FontSize',yfont,'Fontweight','normal');
%
%     title(sprintf('Mean phases of sig. locked units: %d',cntsig),'FontSize',tfont,'FontWeight','normal')
%     axis tight
%     hold on
%     plot([pi,pi],[0 999],'k--','LineWidth',1.5)
%     plot([-pi,-pi],[0 999],'k--','LineWidth',1.5)
%     plot([3*pi,3*pi],[0 999],'k--','LineWidth',1.5)
%     plot([0,0],[0 999],'k:','LineWidth',1.5)
%     plot([2*pi,2*pi],[0 999],'k:','LineWidth',1.5)
%
%     %set(gca, 'XTick', [-pi:pi:3*pi], 'XTickLabel',num2str([-180,0,180,0,-180]'));
%
%     a = num2str([-180,0,180,0,180]'); a(3,:) = '+-pi';
%     set(gca, 'XTick', [-pi:pi:3*pi], 'XTickLabel',a);
%     set(gca,'XLim',[-pi 3*pi]);
%     %set(gca, 'XTick', [-pi], 'XTickLabel',sprintf('%s','-pi'));
%     %set(gca, 'XTick', [0], 'XTickLabel',sprintf('%s','0'));
%
%     figfile = [figdir,area,'_Thetamod_MeanPhases']
%     if savefig1==1,
%         print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%
%         %figfile = [figdir,savefigfilename,'/',sprintf('pfcripmodISI%d', i)];
%     end
% end
%
% %% 2)plot phase histogram of aggregate spikes, sig units
% % ----------------------------------------------------
% if 1
%
%
%     norm = 1;
%
%     figure; hold on;
%     if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
%     phasehist=histc(allsph,bins);
%     phasehist = phasehist(1:(end-1));
%     phasehist_norm = phasehist/(sum(phasehist))*100; % percentage of spikes
%     bins_plot = bins(1:(end-1));
%     bins_plot = bins_plot + (bins(2)-bins(1))/2;
%     if norm == 1
%         h = bar([bins_plot bins_plot+2*pi],[phasehist_norm; phasehist_norm],'histc');
%         set(h(1),'facecolor',clr)
%         set(h(1),'edgecolor',clr)
%         axis tight
%         %ylim([0 .1])
%         set(gca,'YLim',[0 max(phasehist_norm)+0.5])
%         ylabel(['%tage of spikes'],'FontSize',yfont,'Fontweight','normal');
%     else
%         h = bar([bins_plot bins_plot+2*pi],[phasehist; phasehist],'histc');
%         set(h(1),'facecolor',clr)
%         set(h(1),'edgecolor',clr)
%         axis tight
%         ylim([0 max(phasehist)+1500])
%         ylabel(['No. of cells'],'FontSize',yfont,'Fontweight','normal');
%
%     end
%     xlabel(['Phase'],'FontSize',xfont,'Fontweight','normal');
%     title(sprintf('Phase hist of all spikes, sig units: %d',cntsig),'FontSize',tfont,'FontWeight','normal');
%     hold on
%     plot([pi,pi],[0 99999],'k--','LineWidth',1.5)
%     plot([-pi,-pi],[0 99999],'k--','LineWidth',1.5)
%     plot([3*pi,3*pi],[0 99999],'k--','LineWidth',1.5)
%     plot([0,0],[0 99999],'k:','LineWidth',1.5)
%     plot([2*pi,2*pi],[0 99999],'k:','LineWidth',1.5)
%
%     %set(gca, 'XTick', [-pi:pi:3*pi], 'XTickLabel',num2str([-180,0,180,0,-180]'));
%     a = num2str([-180,0,180,0,180]'); a(3,:) = '+-pi';
%     set(gca, 'XTick', [-pi:pi:3*pi], 'XTickLabel',a);
%     set(gca,'XLim',[-pi 3*pi]);
%
%     figfile = [figdir,area,'_Thetamod_PhaseHistAllSpks']
%     if savefig1==1,
%
%       print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%
%     end
%
%
% end
%
% %% 3) Matrix of normalized histograms aligned by peak phase
% if 1
%     figure; hold on;
%     if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
%
%    [sortedph, order] = sort(allsigphases,2,'descend');
%    sort_histnorm = allthetahistnorm(order,:);
%    sort_histper = allthetaper(order,:);
%
%    smsort=[];    bins_plot = bins(1:(end-1));
%    % nstd=1: gaussian of length 4. nstd = 2: gaussian of length 7, nstd=3: gaussian of length 10.
%     nstd = 3; g1 = gaussian(nstd, 3*nstd+1);
%
%    for n =1:length(order),
%        curr = sort_histnorm(n,1:50); % Last bin should be skipped
%        curr = smoothvect(curr,g1);
%        smsort(n,:) = curr(2:end-1);
%        smsort(n,:) = smsort(n,:)./max(smsort(n,:)); % Renormalize
%    end
%    bins_plot = bins_plot(2:end-1);
%
%    imagesc(bins_plot,1:n,smsort); colorbar;
%    set(gca,'XLim',[-pi pi]); set(gca,'YLim',[0 n]);
%    a = num2str([-180,0,180]');
%    set(gca, 'XTick', [-pi:pi:pi], 'XTickLabel',a);
%    xlabel(['Phase'],'FontSize',xfont,'Fontweight','normal');
%    ylabel(['Cell no'],'FontSize',yfont,'Fontweight','normal');
%    title(sprintf('Phase-locked units aligned by Pref Phase: %d',cntsig),'FontSize',tfont,'FontWeight','normal');
%
%    figfile = [figdir,area,'_Thetamod_MatrixAlignPrefPhase']
%    if savefig1==1,
%       print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%
%     end
%
% end
%
% %% 4) Matrix of normalized histograms aligned by concentration parameter
% if 1
%
%     figure; hold on;
%     if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
%
%    [sortedk, order] = sort(allsigkappas);
%    sort_histnorm = allthetahistnorm(order,:);
%    sort_histper = allthetaper(order,:);
%
%    smsort=[];    bins_plot = bins(1:(end-1));
%    % nstd=1: gaussian of length 4. nstd = 2: gaussian of length 7, nstd=3: gaussian of length 10.
%     nstd = 3; g1 = gaussian(nstd, 3*nstd+1);
%
%    for n =1:length(order),
%        curr = sort_histnorm(n,1:50); % Last bin should be skipped
%        curr = smoothvect(curr,g1);
%        smsort(n,:) = curr(2:end-1);
%        smsort(n,:) = smsort(n,:)./max(smsort(n,:)); % Renormalize
%    end
%    bins_plot = bins_plot(2:end-1);
%
%    imagesc(bins_plot,1:n,smsort); colorbar;
%    set(gca,'XLim',[-pi pi]); set(gca,'YLim',[0 n]);
%    a = num2str([-180,0,180]');
%    set(gca, 'XTick', [-pi:pi:pi], 'XTickLabel',a);
%    xlabel(['Phase'],'FontSize',xfont,'Fontweight','normal');
%    ylabel(['Cell no'],'FontSize',yfont,'Fontweight','normal');
%    title(sprintf('Phase-locked units aligned by Conc Parm : %d',cntsig),'FontSize',tfont,'FontWeight','normal');
%
%    figfile = [figdir,area,'_Thetamod_MatrixAlignModlnStrength']
%    if savefig1==1,
%
%        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%
%    end
%
% end
%
% %% 6) Plot distribution of Kappas
% % ------------------------------------
% if 1
%
%     figure; hold on;
%     if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
%     currbins = [0:0.2:max(allsigkappas)]
%     N = histc(allsigkappas,currbins);
%     h = bar(currbins,N,'histc');
%     set(h(1),'facecolor',clr);
%     set(h(1),'edgecolor',clr);
%     xlabel(['Conc parameter (Kappa)'],'FontSize',xfont,'Fontweight','normal');
%     ylabel(['No. of cells'],'FontSize',yfont,'Fontweight','normal');
%     title(sprintf('Conc par (kappa) of sig. locked units: %d',cntsig),'FontSize',tfont,'FontWeight','normal')
%     set(gca,'XLim',[-0.15 max(allsigkappas)+0.1])
%     if savefig1==1,
%         figfile = [figdir,area,'_Thetamod_KappaDistr'];
%         print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%
%    end
% end
%
% %% 7) Plot distribution of Rayleigh Z
% % ------------------------------------
% if 1
%
%     figure; hold on;
%     if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
%
%     logZ = log(allZ);
%     logsigZ = log(allsigZ);
%     currbins = min(logZ):0.8:max(logZ);
%     N = histc(logZ,currbins); Nsig = histc(logsigZ,currbins);
%     h = bar(currbins,N,'histc');
%     set(h(1),'facecolor',clr);
%     set(h(1),'edgecolor',clr);
%     h2 = bar(currbins,Nsig,'histc');
%     set(h2(1),'facecolor','k');
%     set(h2(1),'edgecolor','k');
%     xlabel(['log(Rayleigh Z)'],'FontSize',xfont,'Fontweight','normal');
%     ylabel(['No. of cells'],'FontSize',yfont,'Fontweight','normal');
%     title(sprintf('Rayleigh Z distribution'),'FontSize',tfont,'FontWeight','normal')
%     plot([1 1],[0 max(N)],'k--','LineWidth',1.5);
%     %set(gca,'XLim',[-0.15 max(allsigkappas)+0.1])
%     if savefig1==1,
%         figfile = [figdir,area,'_Thetamod_RayleighZDistr']
%         print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%
%    end
%
% end
%
% %% 8) plot normalized histogram sig units, SEM
% % ------------------------------------------
% if 1
%
%
%     figure; hold on;
%     if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
%
%     phasehist_mean=mean(allthetahist,1);
%     phasehist_sem=std(allthetahist,1)/sqrt(size(allthetahist,1));
%     phasehist_mean = phasehist_mean(1:(end-1));
%     bins_plot = bins(1:(end-1));
%     bins_plot = bins_plot + (bins(2)-bins(1))/2;
%
%     h = bar([bins_plot bins_plot+2*pi],[phasehist_mean phasehist_mean],'histc');
%     set(h(1),'facecolor',clr)
%     set(h(1),'edgecolor',clr)
%     axis tight
%     if strcmp(area,'PFC')
%         ylim([0 90])
%     else
%         ylim([0 200]);
%     end
%     hold on
%
%     % plot sem bars
%     for jj=1:length(bins_plot)
%         plot([bins_plot(jj),bins_plot(jj)],[phasehist_mean(jj)-phasehist_sem(jj) phasehist_mean(jj)+phasehist_sem(jj)],'k','LineWidth',1.5)
%         plot([bins_plot(jj)+2*pi,bins_plot(jj)+2*pi],[phasehist_mean(jj)-phasehist_sem(jj) phasehist_mean(jj)+phasehist_sem(jj)],'k','LineWidth',1.5)
%     end
%
%     titlestring=sprintf('Phase hist of all spikes, sig units: %d',cntsig);
%     title(titlestring,'FontSize',14,'FontWeight','bold')
%     hold on
%     plot([pi,pi],[0 99999],'k--','LineWidth',1.5)
%     plot([-pi,-pi],[0 99999],'k--','LineWidth',1.5)
%     plot([3*pi,3*pi],[0 99999],'k--','LineWidth',1.5)
%     plot([0,0],[0 99999],'k:','LineWidth',1.5)
%     plot([2*pi,2*pi],[0 99999],'k:','LineWidth',1.5)
%     %set(gca, 'XTick', [-pi:pi:3*pi], 'XTickLabel',num2str([-180,0,180,0,-180]'));
%     a = num2str([-180,0,180,0,180]'); a(3,:) = '+-pi';
%     set(gca, 'XTick', [-pi:pi:3*pi], 'XTickLabel',a);
%     set(gca,'XLim',[-pi 3*pi]);
%
%      if savefig1==1,
%         figfile = [figdir,area,'_Thetamod_PhaseHistAllSpksWithErr']
%         print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%
%     end
% end
%
% %% 9) plot distribution of modulation depths
% % ------------------------------------
% if 1
%
%     figure; hold on;
%     if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
%
%     hist(allm,10,clr);
%     title(sprintf('Distribution of modulation depths, nunits: %d',cntsig))
%     xlabel(['Modulation Depth'],'FontSize',xfont,'Fontweight','normal');
%     ylabel(['No. of cells'],'FontSize',yfont,'Fontweight','normal');
%
%     if savefig1==1,
%         figfile = [figdir,area,'_Thetamod_ModlnDepthDistr'];
%         print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%
%     end
%
% end
%
% %% 10) No of sig phase locked cells over days: %tage and number
% % ------------------------------------
% if 1
%
%     figure; hold on;
%     if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
%
%     persig_days = 100*ncells_days_sig./ncells_days;
%     plot(persig_days,[clr 'o'],'MarkerSize',18,'LineWidth',2);
%     title(sprintf('%tage of sig phase locked units'));
%     xlabel(['Day'],'FontSize',xfont,'Fontweight','normal');
%     ylabel(['Percentage of cells'],'FontSize',yfont,'Fontweight','normal');
%     set(gca,'YLim',[0 max(persig_days)+5]);
%
%     if savefig1==1,
%         figfile = [figdir,area,'_Thetamod_PerSigDays'];
%         print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%
%     end
%
%
%     figure; hold on;
%     if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
%
%     Nsig_days = ncells_days_sig;
%     plot(Nsig_days,[clr 'o'],'MarkerSize',18,'LineWidth',2);
%     title(sprintf('No. of sig phase locked units'));
%     xlabel(['Day'],'FontSize',xfont,'Fontweight','normal');
%     ylabel(['Number of cells'],'FontSize',yfont,'Fontweight','normal');
%     set(gca,'YLim',[0 max(Nsig_days)+2]);
%
%     if savefig1==1,
%         figfile = [figdir,area,'_Thetamod_NSigDays']
%         print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
%     end
%
%
% end


% if 0
%     % plot individual phase histogram of all units
%
%     norm = 1;
%
%     figure
%     titlestring=sprintf('%s %s phase hist of individual units // %s',animals{1},region,referencestring);
%     title(titlestring,'FontSize',14,'FontWeight','bold')
%     counter=1;
%     for k=1:length(caf.celloutput)
%         if counter==81
%             counter=1;
%             figure
%             titlestring=sprintf('%s %s phase hist of individual units // %s',animals{1},region,referencestring);
%             title(titlestring,'FontSize',14,'FontWeight','bold')
%         end
%         subplot(8,10,counter)
%         bins_plot = caf.celloutput(k).bins(1:(end-1));
%         bins_plot = bins_plot + (bins(2)-bins(1))/2;
%         phasehist = caf.celloutput(k).phasehist(1:(end-1));
%         phasehist_norm = phasehist/sum(phasehist);
%         if norm == 1
%             if size(phasehist_norm,1) < size(phasehist_norm,2)
%                 phasehist_norm = phasehist_norm';
%             end
%             %plot
%             h = bar([bins_plot bins_plot+2*pi],[phasehist_norm ; phasehist_norm],'histc');
%             title(num2str(caf.celloutput(k).index))
%             axis tight
%             ylim([0 .2])
%         else
%             if size(phasehist,1) < size(phasehist,2)
%                 phasehist = phasehist';
%             end
%             %plot
%             h = bar([bins_plot bins_plot+2*pi],[phasehist ; phasehist],'histc');
%             title(num2str(caf.celloutput(k).index),'FontSize',12,'FontWeight','bold')
%             axis tight
%             ylim([0 250])
%         end
%
%         set(h(1),'facecolor',clr)
%         set(h(1),'edgecolor',clr)
%
%         % plot guide lines
%         hold on
%         plot([pi,pi],[0 9999],'k--','LineWidth',1.5)
%         plot([-pi,-pi],[0 9999],'k--','LineWidth',1.5)
%         plot([3*pi,3*pi],[0 9999],'k--','LineWidth',1.5)
%         plot([0,0],[0 9999],'k:','LineWidth',1.5)
%         plot([2*pi,2*pi],[0 9999],'k:','LineWidth',1.5)
%
%         counter=counter+1;
%     end
% end
%



% % bar
% count = histc(allspikephases, bins);
% out = bar(bins, count, 'hist');
% set(out,'facecolor','k')
% title('aggregate theta modulation');
%
% % lineplot
% dischargeprob=count./sum(count);
% plot(bins(1:(end-1)),dischargeprob(1:(end-1)),'k','LineWidth',2);
%
% [m ph] = modulation(allspikephases);






