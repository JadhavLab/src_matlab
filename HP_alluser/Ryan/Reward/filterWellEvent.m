% -----------------------------------------------------------------
% Name:			filterWellEvent		
% Parent:		(undefined)
%
% Purpose:		Ceate a structure of time-length variables to filter
% trial and well contingent events.
% 
% Input:		animaldir,animalprefix,epochs,varargin
%
% Output:		.rewarded			= bool, time point in rewarded trial
%				.trajtype			= bool, trajtype inbound at
%				.trajnum			= int, trial num at time; 
%				.wellend			= end well at time;
%				.normalized_traj	= normalized position in curr trial
%				.spat_near			= bool, spatially near endwell of currtrial?
%				.temp_near			= bool, temp near trial end
%				.absvel				= absolute velocity
%				.abspos				= absolute position
% -----------------------------------------------------------------
function out  = filterWellEvent(animaldir,animalprefix,epochs,varargin)

	%%% Preprocessing: Optional Args and Velpos Import
	% -----------------------------------------------------------------
	
	% Create initial structural scaffold that includes velocity of the
	% animal at all times
	velpos = DFTFsj_getvelpos(animaldir,animalprefix,epochs);
	
	% Set optional arguments. User can elect spatial proximity to well and
	% temporal proximity to end of trial
	for i = 1:2:numel(varargin)
		switch varargin{i}
			case 'temporal_proximity'
				temporal_proximity = varargin{i+1};
			case 'spatial_proximity'
				spatial_proximity = varargin{i+1};	
		end
	end
	
	%%% Compute Trajectory-based Time Information
	% -----------------------------------------------------------------
	
	% Agglomerate master cell of trajinfo, pos, linpos
	trajinfo = loaddatastruct(animaldir,animalprefix,'trajinfo',...
		unique(epochs(:,1)));
	pos = loaddatastruct(animaldir,animalprefix,'pos', ...
		unique(epochs(:,1)));
	linpos = loaddatastruct(animaldir,animalprefix,'linpos',...
		unique(epochs(:,1)));
	
	for i = 1:size(epochs,1)	% crawl along epochs, and compute
		
		d = epochs(i,1); e = epochs(i,2);
		fprintf('\nTime filtering day %d, epoch %d\n',d,e);
		
		% Record trajtime for our particular epoch
		trajinfo_de = trajinfo{d}{e};
		if isempty(trajinfo_de)
			continue;
		else
			trajtime = trajinfo_de.trajtime;
		end
		
		% Inintialize vector to store matching times
		zeros_sizeof_vec	= zeros(numel(velpos{d}{e}.time),1);
		all_times_near_end	= zeros_sizeof_vec;
		
		% Initialize vectors to hold other information about trajectory
		out{d}{e}.rewarded			= zeros_sizeof_vec;
		out{d}{e}.trajtype			= zeros_sizeof_vec;
		out{d}{e}.trajnum			= zeros_sizeof_vec;
		out{d}{e}.wellend			= zeros_sizeof_vec;
		out{d}{e}.normalized_traj	= NaN*ones(size(velpos{d}{e}.time));
		
		% Crawl over each trajectory and select times within
		% temporal_proximity to the endpoint
		for j = 1:size(trajtime,1)
			
			% Find matching times for this trajectory
			time_near_end = ...
				(velpos{d}{e}.time - temporal_proximity <= trajtime(j,2) ) & ...
				velpos{d}{e}.time >= (trajtime(j,1));
			% Add to record of all matching times
			all_times_near_end = all_times_near_end | time_near_end;
			
			% Add normalized path setting
			% Decide times in trajectory
			times_in_traj = (velpos{d}{e}.time < trajtime(j,2)) & ...
				(velpos{d}{e}.time > trajtime(j,1));
			% Store normalized path along correct location in time-sized vector
			[normPath,initial,final] = markNormalizedPath(times_in_traj);
			out{d}{e}.normalized_traj(initial:final) = normPath;
			
			% Store rewarded, trajtype,trajbound, and wellend variables at
			% correct time slots in time-sized vector
			out{d}{e}.rewarded = out{d}{e}.rewarded + ...
				times_in_traj * trajinfo_de.rewarded(j);
			out{d}{e}.trajtype = out{d}{e}.trajtype + ...
				times_in_traj * trajinfo_de.trajbound(j);
			out{d}{e}.trajnum = out{d}{e}.trajnum + ...
				times_in_traj * j;
			out{d}{e}.wellend = out{d}{e}.wellend + ...
				times_in_traj * trajinfo{d}{e}.wellstend(j,2);
			
		end
		
		% Update the output with our findings and information from trajinfo
		out{d}{e}.temp_near = all_times_near_end;
		out{d}{e}.abspos = velpos{d}{e}.abspos;
		out{d}{e}.absvel = velpos{d}{e}.absvel;
		
		% if spatial proximity option, add it
		if exist('spatial_proximity','var')
			wellCoord = linpos{d}{e}.wellSegmentInfo.wellCoord;
			nearWell = markPointsNearWell(...
				spatial_proximity,wellCoord,velpos{d}{e}.time, ...
				pos{d}{e}.data(:,2), pos{d}{e}.data(:,3), ...
				out{d}{e}.wellend);
			
			out{d}{e}.spat_near = nearWell;
		end
		
		out{d}{e}.time = velpos{d}{e}.time;
		
		
	end

end

%% HELPER FUNCTIONS 

% -----------------------------------------------------------------
% Name:				
% Parent:	
%
% Purpose:			
% 
% Input:			
%
% Output: 
% -----------------------------------------------------------------
function [normPath,initial,final] = markNormalizedPath(inTrajLogical)
	
	diff_logical = diff(inTrajLogical);
	initial = find(diff_logical == 1) + 1;
	if isempty(initial); initial = 1; end;
	final = find(diff_logical == -1);
	
	lengthTraj = sum(inTrajLogical);
	normPath = linspace(0,1,lengthTraj);
	
end

% ----------------------------------------------------------------
% Name:				markPointsNearWell
% Parent:			filterWellEvent
%
% Purpose:			Sets a vector with true at all time points that are
% near the current end well of the current trial at a particular point in
% time, and false at all points not near the current end well within each
% trial at all time points.
%
% Inputs:			howClose=detection distance to end well, wellCoord=2xN
% matrix of well coordinates, times=all times in epoch, x=all x coord in
% epoch, y=all x coord in epoch, wellend=vector of each wellend per trial
% 
% Outputs:			nearWell=vector of whether animal is near target well
% per time for that time's trial.
% ----------------------------------------------------------------
function nearWell = markPointsNearWell(howClose, wellCoord, times,x,y, ...
	wellend)

	% Create vector to hold result
	nearWell = zeros(size(times));
	
	sizeof_wellCoord = size(wellCoord);
	
	%% Calculate euclidean distances to each well
	% The final result will hold per time point a euclidean distance to
	% each well, with each well in a column
	c = [x y]';
	z(1,:,:) = c;
	z = repmat(z,[3,1,1]);
	
	wellcount = size(wellCoord,1);
	wellCoord = repmat(wellCoord,[1,1,numel(times)]);
	d = (z - wellCoord); d = d.^2; d = d(:,1,:) + d(:,2,:); d = sqrt(d);
	
	d = squeeze(d);
	d = d';
	
	%% Conditionally remove wells that do not map onto the end well
	% At each point in time, NaN out d columns elements that should not be
	% contributing to our nearWell measure ... if it's a starting well or
	% not a terminal well, NaN it.
	d = removeIrreleventWell(d,wellend,wellcount);
	
	nearWell = (d(:,1) < howClose | d(:,2) < howClose | d(:,3) < howClose);
	

end
	% ----------------------------------------------------------------
	% Name:				removeIrreleventWell
	% Parent:			filterWellEvent::markPointsNearWell
	%
	% Purpose:			removes from the distance calculation at every
	% point in time wells that are not the current end well of the current
	% trial at the current position in time.
	% ----------------------------------------------------------------
	function d = removeIrreleventWell(d,wellend,wellcount)
		
		wellid_matrix  = repmat(1:wellcount, numel(wellend), 1);
		wellend = repmat(wellend,1,wellcount);
		
		releventwell_matrix = (wellid_matrix == wellend);
		zero_locs = releventwell_matrix == 0;
		releventwell_matrix = cast(releventwell_matrix,'double');
		releventwell_matrix(zero_locs) = NaN;
		
		d = d .* releventwell_matrix;
		
	end