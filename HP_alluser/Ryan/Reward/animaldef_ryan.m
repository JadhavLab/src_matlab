function animalinfo = animaldef(animalname)

rootfolder = '/Volumes/jadhav-lab/DATA';

switch animalname
    
    % Ripple Disruption Expt Animals
    
    case 'sjc'
        animalinfo = {'sjc', [rootfolder '/sjadhav/RippleInterruption/sjc_direct/'], 'sjc'};
    case 'RE1'
        animalinfo = {'RE1', [rootfolder '/sjadhav/RippleInterruption/RE1_direct/'], 'RE1'};
    case 'RNa'
        animalinfo = {'RNa', [rootfolder '/sjadhav/RippleInterruption/RNa_direct/'], 'RNa'};
    case 'RNb'
        animalinfo = {'RNb', [rootfolder '/sjadhav/RippleInterruption/RNb_direct/'], 'RNb'};
    case 'RNc'
        animalinfo = {'RNc', [rootfolder '/sjadhav/RippleInterruption/RNc_direct/'], 'RNc'};
    case 'RNd'
        animalinfo = {'RNd', [rootfolder '/sjadhav/RippleInterruption/RNd_direct/'], 'RNd'};
    case 'RCa'
        animalinfo = {'RCa', [rootfolder '/sjadhav/RippleInterruption/RCa_direct/'], 'RCa'};
    case 'RCb'
        animalinfo = {'RCb', [rootfolder '/sjadhav/RippleInterruption/RCb_direct/'], 'RCb'};
    case 'RCc'
        animalinfo = {'RCc', [rootfolder '/sjadhav/RippleInterruption/RCc_direct/'], 'RCc'};
    case 'RCd'
        animalinfo = {'RCd', [rootfolder '/sjadhav/RippleInterruption/RCd_direct/'], 'RCd'};
    case 'REc'
        animalinfo = {'REc', [rootfolder '/sjadhav/RippleInterruption/REc_direct/'], 'REc'};
    case 'REd'
        animalinfo = {'REd', [rootfolder '/sjadhav/RippleInterruption/REd_direct/'], 'REd'};
    case 'REe'
        animalinfo = {'REe', [rootfolder '/sjadhav/RippleInterruption/REe_direct/'], 'REe'};
    case 'REf'
        animalinfo = {'REf', [rootfolder '/sjadhav/RippleInterruption/REf_direct/'], 'REf'};
    case 'REg'
        animalinfo = {'REg', [rootfolder '/sjadhav/RippleInterruption/REg_direct/'], 'REg'};
    case 'REh'
        animalinfo = {'REh', [rootfolder '/sjadhav/RippleInterruption/REh_direct/'], 'REh'};
    case 'HPa'
        animalinfo = {'HPa', [rootfolder '/sjadhav/HPExpt/HPa_direct/'], 'HPa'};
    case 'HPb'
        animalinfo = {'HPb', [rootfolder '/sjadhav/HPExpt/HPb_direct/'], 'HPb'};
    case 'HPc'
        animalinfo = {'HPc', [rootfolder '/sjadhav/HPExpt/HPc_direct/'], 'HPc'};
    case 'Ndl'
        animalinfo = {'Ndl', [rootfolder '/sjadhav/HPExpt/Ndl_direct/'], 'Ndl'};
    case 'Rtl'
        animalinfo = {'Rtl', [rootfolder '/sjadhav/HPExpt/Rtl_direct/'], 'Rtl'};
    case 'Brg'
        animalinfo = {'Brg', [rootfolder '/sjadhav/HPExpt/Brg_direct/'], 'Brg'};
        
    otherwise
        
        error(['Animal ',animalname, ' not defined.']);
end
