function fout = plotBehavior(cind,excperiods,animprefix,pos,linpos,varargin)

for i = 1:2:numel(varargin)
	switch varargin{i}
		case 'useGlobal'
			useGlobal = varargin{i+1};
			global firstPlot;
			if isempty(firstPlot)
				firstPlot=0;
			end
	end
end

%% Pre-processing, ready x,y,t vars
d = cind(1); e = cind(2);

pos_c = pos{cind(1)}{cind(2)};
x = colof('x',pos_c);
y = colof('y',pos_c);
t = colof('time',pos_c);

%% Get Include Periods
incperiods = ones(size(t));
for i = 1:size(excperiods,1)
	incperiods = incperiods & ...
		~( (t > excperiods(i,1) & t < excperiods(i,2) ));
end

%% Plot behavior 
positionVector = [(e/6 - 0.27) 1-(d/8 - 0.012) 0.24 0.1];
f = figure(1); hold on;
subplot('Position',positionVector); hold on;
if ~exist('firstPlot','var') | firstPlot == 0
	plot( x, y ); hold on; alpha(0.5);
end
plot( x(incperiods), y(incperiods),'ro','markersize',18);
alpha(0.5);
fprintf('\n\nFigure day %d, ep %e\n', d, e);
% input('Press any key to continue...');

fout = struct(); % return Null data
fout.filteredx = x(incperiods);
fout.filteredy = y(incperiods);

% ---------------------------------------------------------------------
% TODO?: Add ability to visualize spheres around wells and trajectories.
% Can be done by creating sets of filters with their own special switches
% to alter this functions plot behavior.