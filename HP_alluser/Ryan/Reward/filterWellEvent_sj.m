% -----------------------------------------------------------------
% Name:			filterWellEvent
% Parent:		(undefined)
%
% Purpose:		Ceate a structure of time-length variables to filter
% trial and well contingent events.
%
% Input:		animaldir,animalprefix,epochs,varargin
%
% Output:		.rewarded			= bool, time point in rewarded trial
%				.trajtype			= bool, trajtype inbound at
%				.trajnum			= int, trial num at time;
%				.wellend			= end well at time;
%				.normalized_traj	= normalized position in curr trial
%				.spat_near			= bool, spatially near endwell of currtrial?
%				.temp_near			= bool, temp near trial end
%				.absvel				= absolute velocity
%				.abspos				= absolute position
% -----------------------------------------------------------------
function out  = filterWellEvent(animaldir,animalprefix,epochs,varargin)

%%% Preprocessing: Optional Args and Velpos Import
% -----------------------------------------------------------------

% Create initial structural scaffold that includes velocity of the
% animal at all times
velpos = DFTFsj_getvelpos(animaldir,animalprefix,epochs);

% Set optional arguments. User can elect spatial proximity to well and
% temporal proximity to end of trial
for i = 1:2:numel(varargin)
    switch varargin{i}
        case 'temporal_proximity'
            temporal_proximity = varargin{i+1};
        case 'spatial_proximity'
            spatial_proximity = varargin{i+1};
    end
end

%%% Compute Trajectory-based Time Information
% -----------------------------------------------------------------

% Agglomerate master cell of trajinfo, pos, linpos
trajinfo = loaddatastruct(animaldir,animalprefix,'trajinfo',...
    unique(epochs(:,1)));
pos = loaddatastruct(animaldir,animalprefix,'pos', ...
    unique(epochs(:,1)));
linpos = loaddatastruct(animaldir,animalprefix,'linpos',...
    unique(epochs(:,1)));

thrsdist=10; % cm
thrsspeed=2;% cm/sec

for ep = 1:size(epochs,1)	% crawl along epochs, and compute
    
    d = epochs(ep,1); e = epochs(ep,2);

    %d=1; e=2;
    day=d; epoch=e;
    fprintf('\nTime filtering day %d, epoch %d\n',d,e);
    
    postime = pos{day}{epoch}.data(:,(1));
    posn = pos{day}{epoch}.data(:,2:3);
    posx = posn(:,1); posy = posn(:,2);
    speed = abs(pos{day}{epoch}.data(:,5));
    
    % Record trajtime for our particular epoch
    trajinfo_de = trajinfo{d}{e};
    if isempty(trajinfo_de)
        continue;
    else
        trajbound = trajinfo{day}{epoch}.trajbound;
        rewarded = trajinfo{day}{epoch}.rewarded;
        trajtime = trajinfo{day}{epoch}.trajtime;
        wellstend = trajinfo{day}{epoch}.wellstend;
    end
    %wellCoord = linpos{d}{e}.wellSegmentInfo.wellCoord;
    wellpos = linpos{day}{epoch}.wellSegmentInfo.wellCoord;
    
    
    % Crawl over each trajectory and select times within
    % spatial and temporal_proximity to the endpoint
    for i = 1:size(trajtime,1)
        
        currtrajtime = trajtime(i,:);
        curr_endwell = wellstend(i,2);
        curr_rewarded = rewarded(i);
        curr_trajtype = trajbound; % 0=out, 1=in
        
        startidx = find(currtrajtime(1)==postime);                                             % mcz start of positions
        endidx = find(currtrajtime(2)==postime);                                               % mcz end of positions
        currpos = posn(startidx:endidx,:); % All pons on current outbound lap
        currtime = postime(startidx:endidx,:);
        currspeed = speed(startidx:endidx,:);
        
        currwellpos = wellpos(curr_endwell,:);
        % Get distance from endwell
        alldist = dist(currpos,repmat(currwellpos,length(currpos),1));
        % Get dist less than threshold
        valid_idx = find(alldist<=thrsdist);
        subset_pos = currpos(valid_idx,:);
        subset_speed = currspeed(valid_idx);
        subset_time = currtime(valid_idx);
        % FInd speeds < thrs in this window, and take the first time as reward-time
        reward_idx = min(find(subset_speed<thrsspeed))
        if ~isempty(reward_idx)
            curr_rewardtime = subset_time(reward_idx);
            rewardpos = subset_pos(reward_idx,:);
        else
            [curr_rewardtime,minidx] = min(subset_time);
            rewardpos = subset_pos(minidx,:);
        end
        
        %figopt=1;
        if figopt==1
%             figure(1); hold on; plot(posx, posy, 'Color', Clgy); axis square;
%             set(gca, 'LooseInset', get(gca,'TightInset'));
%             plot(currwellpos(1),currwellpos(2),'ks','MarkerSize',8,'MarkerFaceColor','k');
%             circles(currwellpos(1),currwellpos(2),thrsdist,'edgecolor','k','facecolor','none');
%             plot(currpos(:,1),currpos(:,2),'c-','Linewidth',2);
%             plot(subset_pos(:,1),subset_pos(:,2),'g-','Linewidth',2);
%             plot(rewardpos(1),rewardpos(2),'ro', 'MarkerSize',12, 'MarkerFaceColor','m')
            
            
            
%             %scatter([choicept(1), choicept(1)-15, choicept(1)+15], [choicept(2)+25, choicept(2), choicept(2)],'vr','linewidth',2);
%             %     %%%% mcz test code %%%%
%             %     a= (SegLth1-ctrarmdist)<= lindists(:,1);                                             % get first part of valid_idx inequality
%             %     b= (lindists(:,1)<=(SegLth1+choicedist));                                            % get second part of valid_idx inequality
%             %     plot(currpos(:,1), currpos(:,2),'k','linewidth',4);                                  % plot whole trace
%             %     plot(currpos(a,1),currpos(a,2),'r','linewidth',3);                                   % plot inequality a
%             %     plot(currpos(b,1),currpos(b,2),'g','linewidth',2);                                   % plot inequality b
%             %     plot(currpos(valid_idx,1),currpos(valid_idx,2),'b','linewidth',1);                   % plot valid idx, make sure inequality makes sense
%             %     %%%% mcz test code %%%%
        end
        
       % keyboard;
        
        
    end
    
    
    % Update the output with our findings and information from trajinfo
    %     out{d}{e}.temp_near = all_times_near_end;
    %     out{d}{e}.abspos = velpos{d}{e}.abspos;
    %     out{d}{e}.absvel = velpos{d}{e}.absvel;
    %
    %     % if spatial proximity option, add it
    %     if exist('spatial_proximity','var')
    %         wellCoord = linpos{d}{e}.wellSegmentInfo.wellCoord;
    %         nearWell = markPointsNearWell(...
    %             spatial_proximity,wellCoord,velpos{d}{e}.time, ...
    %             pos{d}{e}.data(:,2), pos{d}{e}.data(:,3), ...
    %             out{d}{e}.wellend);
    %
    %         out{d}{e}.spat_near = nearWell;
    %     end
    %
    %     out{d}{e}.time = velpos{d}{e}.time;
    
    
end

% function ended





