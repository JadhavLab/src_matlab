% Awake Prefrontal, Rewarded Versus Non-Rewarded

%% FILTER SECTION

%%% Animal and Epoch Specification
% ------------------------------------ 
%

animalfilter = {	'HPa',...
% 					'HPb',...
% 					'HPc'...
				};
epochfilter = [...
	'isequal($type, ''run'') && '...
	'(isequal($environment, ''wtr1'') ||' ... 
	'isequal($environment, ''wtr2''))'
	];

%%% Time-based Filtration
% ------------------------------------ 
% Variables available to filter on: trajtype, absvel, abspos, rewarded,
% spat_near, and temp_near.

trajfilter = [
	'$trajtype			== 1		&'		...
	'$trajnum			>= 2		&'		...
 	'$rewarded			== true		&'		...
	'$absvel			<= 0.5		&'		...
	'$spat_near			== true		&'		...
	'$normalized_traj	> 0.75'
	];

% Time filter calls filterWellEvent function. Which, in turn calls
% DFTFsj_getvelpos, and adds to the struct, using the information
timefilter{1} = {'filterWellEvent_sj', trajfilter, ...
	'temporal_proximity', 5, 'spatial_proximity', 2.4};

%%% Cellular Filtration
% ------------------------------------ 
%
cellfilter =	'isequal($area, ''PFC'')';

%%% Iterator
% ------------------------------------ 
%
iterator =		'epochbehaveanal';

%% Filter Creation
%
disp('Creating filter ...');

f = createfilter( 'animal', animalfilter, 'epochs', epochfilter, ...
	'excludetimefilter', timefilter, 'cells', cellfilter,	...
	'iterator', iterator);

disp(' ... complete!');

%% Run Filter
% f = setfilterfunction(f,'spikehist',{'spikes'});
f = setfilterfunction(f,'plotBehavior',{'pos','linpos'},'useGlobal',true);
runfilter(f);