

function column_data =  colof(req, structdat)

	data = structdat.data;
	fieldparams = structdat.fields;
	
	% remove any parentheticals
	[a,b] = regexp(fieldparams,'\(.*\)');
	
	c=[];
 	for i = 1:numel(a)
		c = [c a(i):b(i)];
	end
	
	fieldparams(c) = [];
	
	fieldparams = strsplit(fieldparams);
	
	index = [];
	
	for i = 1:length(fieldparams)
		if strcmp(fieldparams{i},req)
			index = i;
		end
	end
	
	try
		column_data = data(:,index);
	catch ME
		save errorstate;
	end
end