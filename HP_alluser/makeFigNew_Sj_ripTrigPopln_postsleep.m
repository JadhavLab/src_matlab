

% SJ/ 2015
% Based on makeFig2New, which was originally makeFig2 for ist submission. 
% This generates the first plot in what is now Fig3: ripTrig Popln Hist as an image.
% Will have to find new examples to show on right, and mark with arrows in figure
% Can also do plotting on example raster-PSTHs in here.
% The top part of code also used to do plotting related to theta, but that is handled separately
% now in compare ripmod thetamod



% 
% %% EXTRACTED FROM DFSsj_HPexpt_getripalignspiking_ver4:
% % Making panels of rip-trig-spiking, their mean, and theta modulation
% load('/opt/data15/gideon/HP_ProcessedData/HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014_FStagged.mat');
% load('/opt/data15/gideon/HP_ProcessedData/HP_thetamod_PFC_alldata_Nspk50_gather_4-3-2014_FSremoved.mat');
% figdir = '/data15/gideon/FigsForPaper/Fig2/';
% saveg1=1;
% set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
% set(0,'defaultaxesfontname','Arial');
% set(0,'defaultAxesFontName','Arial');
% set(0,'defaultTextFontName','Arial');
% set(0,'defaultaxesfontsize',16);
% 
% forppr=1;
% tfont = 14;
% xfont = 12;
% yfont = 12;
% 
% tmpcnt=0;
% 
% thetaindices=[];for kk=1:length(allthetamod),thetaindices=[thetaindices;allthetamod(kk).index];end
% rippleindices=[];for km=1:length(allripplemod),rippleindices=[rippleindices;allripplemod(km).index];end
% % the example cells (anim day tet cell):
% forFigInds=[1 2 17 2;4 15 21 4;4 15 21 3;2 3 10 1];
% for j=1:size(forFigInds,1)
%     %RIPPLE
%     [v w]=ismember(rippleindices,forFigInds(j,:),'rows');
%     curridx=forFigInds(j,:);
%     i=find(w);
%     currhist = allripplemod(i).hist;
%     currraster = allripplemod(i).raster;
%     sig_shuf = allripplemod(i).sig_shuf;
%     sig_ttest = allripplemod(i).sig_ttest;
%     modln_shuf = allripplemod(i).modln_shuf;
%     modln_peak = allripplemod(i).modln_peak;
%     modln = allripplemod(i).modln;
%     currNspk = allripplemod(i).Nspk;
%     modln_var = allripplemod(i).varRespAmp;
%     p_var = allripplemod(i).rasterShufP;
%     modln_var2 = allripplemod(i).varRespAmp2;
%     p_var2 = allripplemod(i).rasterShufP2;
%     sigvar=0; if p_var<0.05, sigvar=1; end
%     sigvar2=0; if p_var2<0.05, sigvar2=1; end
%     cellfr = allripplemod(i).cellfr;
%     rip_spkshist_cellsort_PFC = currhist; rip_spks_cellsort_PFC = currraster;
%     anim=curridx(1);day = curridx(2); tet = curridx(3); cell = curridx(4);
%     
%     %THETA
%     [v w]=ismember(thetaindices,curridx,'rows');
%     curthetaind=find(w);
%     if length(find(w))>0
%         sph = allthetamod(curthetaind).sph;
%         Nspk = allthetamod(curthetaind).Nspk;
%         thetahist = allthetamod(curthetaind).thetahist; % Theta histogram plot
%         thetahistnorm = allthetamod(curthetaind).thetahistnorm; % Normalized - Theta histogram plot
%         thetaper = allthetamod(curthetaind).thetaper; % Histogram in units of percentage of spikes
%         stats = allthetamod(curthetaind).stats;
%         m = allthetamod(curthetaind).modln;
%         phdeg = allthetamod(curthetaind).phdeg;
%         kappa = allthetamod(curthetaind).kappa;
%         thetahat_deg = allthetamod(curthetaind).thetahat_deg; % From von Mises fit - use this
%         prayl = allthetamod(curthetaind).prayl;
%         zrayl = allthetamod(curthetaind).zrayl;
%         alpha = allthetamod(curthetaind).alpha;
%         pdf = allthetamod(curthetaind).vmpdf;
%         meanphase = allthetamod(curthetaind).meanphase;
%         countper = thetaper;
%         bins = -pi:(2*pi/nbins):pi;
%         ph = phdeg*(pi/180);
%         
%         
%         tmpcnt = tmpcnt+1;
%         
%         set(0,'DefaultFigurePaperPositionMode','manual')
%         fig=figure('PaperPosition',[0 0 3 5],'PaperSize',[3 5]);
%         hold on;
%         set(gcf,'Position',[100 130 300 500]);
%         subplot(3,1,1); hold on;
%         spkcount = [];
%         spikeMat=zeros(length(rip_spks_cellsort_PFC),110);
%         for c=1:length(rip_spks_cellsort_PFC)
%             tmps = rip_spks_cellsort_PFC{c};
%             
%             spikeMat(c,round(tmps/10)+55+1)=1;
%             
%             plot(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),'k.','MarkerSize',4);
%             
%             % Get count of spikes in response window
%             if ~isempty(tmps)
%                 subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
%                 spkcount = [spkcount; length(subset_tmps)];
%             end
%         end
%         set(gca,'XLim',[-pret postt]);
%         set(gca,'XTick',[])
%         xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
%         ylabel('SWR number','FontSize',yfont,'Fontweight','normal');
%         set(gca,'YLim',[0 size(rip_spkshist_cellsort_PFC,1)]);
%         ypts = 0:1:size(rip_spkshist_cellsort_PFC,1);
%         xpts = 0*ones(size(ypts));
%         plot(xpts , ypts, 'k--','Linewidth',2);
%         set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'),'fontsize',xfont);
%         xaxis = -pret:binsize:postt;
%         ax1 = gca;
%         set(ax1,'XColor','k','YColor','k')
%         subplot(3,1,2); hold on;
%         plot(xaxis,mean(rip_spkshist_cellsort_PFC),'k-','Linewidth',3);
%         set(gca,'XLim',[-pret postt]);
%         xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
%         ylabel('Firing rate (Hz)','FontSize',yfont,'Fontweight','normal');
%         set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'),'fontsize',xfont);
%         ylow = min(mean(rip_spkshist_cellsort_PFC)-sem(rip_spkshist_cellsort_PFC));
%         ylow=0;
%         yhigh = max(mean(rip_spkshist_cellsort_PFC)+sem(rip_spkshist_cellsort_PFC));
%         set(gca,'YLim',[ylow yhigh+0.1]);
%         ypts = ylow-0.1:0.1:yhigh+0.1;
%         xpts = 0*ones(size(ypts));
%         % Plot Line at 0 ms - Onset of stimulation
%         plot(xpts , ypts, 'k--','Linewidth',2);
%         
%         if sig_ttest ==1, str = '*'; else, str = ''; end
%         if sig_shuf ==1, str_shuf = '*'; else, str_shuf = ''; end
%         if sigvar ==1, str_var = '*'; else, str_var = ''; end
%         if sigvar2 ==1, str_var2 = '*'; else, str_var2 = ''; end
%         
%         subplot(3,1,3)
%         hold on
%         out = bar(bins, thetaper, 'hist');
%         set(gca,'XLim',[-pi pi]);
%         set(gca, 'XTick', [-pi, -pi/2, 0, pi/2, pi]);
%         str={'-p','-0.5p','0','0.5p','p'};
%         set(gca,'xticklabel',str,'fontname','symbol','fontsize',xfont)
%         
%         set(gca, 'YTick', [0 2 4]);
%         str2={'  0 ','  2 ','  4 '};
%         set(gca,'yticklabel',str2,'fontname','arial','fontsize',xfont)
%         set(gca,'xticklabel',str,'fontname','symbol','fontsize',xfont)
%         
%         ylabel('% of Spikes','fontname','arial','fontsize',xfont)
%         xlabel('Theta phase','fontname','arial','fontsize',xfont)
%         
%         set(out,'FaceColor','k'); set(out,'EdgeColor','k');
%         binnum = lookup(thetahat,alpha);
%         
%         if saveg1==1,
%             figfile = [figdir,'RipAndThetaExamples_anim',num2str(anim),'_Day',num2str(day),'_Tet',num2str(tet),'_Cell',num2str(cell)];
% %            print(fig,'-dpdf', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
%         end
%         keyboard
%         
%     end
% end
% %%
% 
% % Extracted from sj_combine_thetaandripplemod2_ver4:
% % Theta modulation for rip-mod and ripunmod units
% % Now unused: ripmod as a function of thetamod, and venn diagram
% 
% figdir = '/data15/gideon/FigsForPaper/Fig2/';
% savefig1=0;
% savedir = '/opt/data15/gideon/HP_ProcessedData/';
% area = 'PFC';
% thetafile = [savedir 'HP_thetamod_PFC_alldata_Nspk50_gather_4-3-2014_FSremoved.mat'];%[savedir 'HP_thetamod_',area,'_alldata_gather'];
% state = ''; %state = 'sleep'; %or state = '';
% ripplefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014_FStagged.mat'];%[savedir 'HP_ripplemod',state,'_',area,'_alldata_speed_minrip2_gather_var_feb14']; % ripple mod in awake or sleep
% 
% load(ripplefile, 'allripplemod','allripplemod_idx'); % load allripplemod and allripplemod_idx.
% load(thetafile,'allthetamod','allthetamod_idx');
% 
% if strcmp(state,'sleep'),
%     statename = 'Sleep';
% else
%     statename = 'Run';
% end
% 
% % Match idxs as in xcorrmesaures2
% 
% cntcells=0; cnt_mismatch=0;
% 
% 
% for i=1:length(allthetamod)
%     i;
%     curridx = allthetamod_idx(i,:);
%     
%     
%     match = rowfind(curridx, allripplemod_idx);
%     
%     if match~=0,
%         cntcells = cntcells+1;
%         allmod(cntcells).idx = curridx;
%         % Theta
%         allmod(cntcells).sph = allthetamod(i).sph;
%         allmod(cntcells).Nspk = allthetamod(i).Nspk;
%         allmod(cntcells).kappa = allthetamod(i).kappa;
%         allmod(cntcells).modln = allthetamod(i).modln;
%         allmod(cntcells).meanphase = allthetamod(i).meanphase;
%         allmod(cntcells).prayl = allthetamod(i).prayl;
%         % Ripple
%         allmod(cntcells).sig_shuf = allripplemod(match).sig_shuf; % Use this to determine significance
%         allmod(cntcells).ripmodln_peak = allripplemod(match).modln_peak; % % peak change above baseline
%         allmod(cntcells).ripmodln = allripplemod(match).modln; % Mean change over baseline
%         allmod(cntcells).ripmodln_shuf = allripplemod(match).modln_shuf; % %value of significance
%         allmod(cntcells).sig_ttest = allripplemod(match).sig_ttest;
%         
%         % New
%         allmod(cntcells).ripmodln_div = allripplemod(match).modln_div; % % peak change above baseline
%         allmod(cntcells).pvar = allripplemod(match).rasterShufP;
%         allmod(cntcells).mvar = allripplemod(match).varRespAmp;
%         allmod(cntcells).mvar2 = allripplemod(match).varRespAmp2;
%         allmod(cntcells).pvar2 = allripplemod(match).rasterShufP2;
%         
%         % Prop
%         allmod(cntcells).cellfr = allripplemod(match).cellfr;
%         
%         % Rdm resp modln
%         allmod(cntcells).rdmmodln_peak = allripplemod(match).modln_peak_rdm; % % peak change above baseline
%         allmod(cntcells).rdmmodln = allripplemod(match).modln_rdm; % Mean change over baseline
%         allmod(cntcells).FStag=allripplemod(match).FStag;
%     end
%     
% end
% 
% 
% set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
% set(0,'defaultaxesfontname','Arial');
% set(0,'defaultAxesFontName','Arial');
% set(0,'defaultTextFontName','Arial');
% set(0,'defaultaxesfontsize',16);
% tfont = 18; % title font
% xfont = 12;
% yfont = 12;
% 
% 
% 
% % Get data
% for i=1:length(allmod)
%     % Theta
%     allidxs(i,:) = allmod(i).idx;
%     allkappas(i) = allmod(i).kappa;
%     allmodln(i) = allmod(i).modln;
%     allmeanphase(i) = allmod(i).meanphase;
%     allprayl(i) = allmod(i).prayl;       %[92/174 theta. ~53%]
%     % Ripple
%     allripmodln_peak(i) = allmod(i).ripmodln_peak;
%     allripmodln_div(i) = allmod(i).ripmodln_div;
%     allripmodln(i) = allmod(i).ripmodln;
%     allripmodln_shuf(i) = allmod(i).ripmodln_shuf; %[]
%     allsigshuf(i) = allmod(i).sig_shuf;
%     allsigttest(i) = allmod(i).sig_ttest;
%     
%     % New
%     allpvar(i) = allmod(i).pvar;
%     allripmodln_var(i) = allmod(i).mvar;
%     allripmodln_var2(i) = allmod(i).mvar2;
%     allpvar2(i) = allmod(i).pvar2;
%     
%     % Prop
%     allcellfr(i) = allmod(i).cellfr;
%     
%     % Rdm resp
%     allrdmmodln_peak(i) = allmod(i).rdmmodln_peak;
%     allrdmmodln(i) = allmod(i).rdmmodln;
%     
%     
%     
% end
% 
% allripmodln = abs(allripmodln_var2);
% sigrip = find(allpvar2<0.05); sigtheta = find(allprayl<0.05); sigboth = find(allprayl<0.05 & allpvar2<0.05==1);
% allsig = union(sigrip, sigtheta);
% % Vector of 0s and 1s - sig ripple vs sig theta
% ripvec = zeros(size(allripmodln)); ripvec(sigrip)=1;
% thetavec = zeros(size(allripmodln)); thetavec(sigtheta)=1;
% [rvec,pvec] = corrcoef(ripvec,thetavec);
% x = find(allmodln > 0.65 & allmodln < 0.7 & allripmodln > 80);
% allidxs(x,:);
% fig3=figure('PaperPosition',[0 0 3 4.8],'PaperSize',[4 4.8]);
% h=barwitherr([std(allkappas(find(ripvec==1)))/sqrt(sum(ripvec)), std(allkappas(find(ripvec==0)))/sqrt(sum(ripvec==0))],[mean(allkappas(find(ripvec==1))), mean(allkappas(find(ripvec==0)))],0.4)
% set(h(1),'FaceColor',[1 1 1]*0.7)
% set(gca,'XTickLabel',{'Rip-mod','Rip-unmod'},'fontname','arial','fontsize',xfont)
% ylim([0 0.26])
% ylabel('Theta modulation (kappa)','fontsize',xfont,'fontname','arial')
% set(gca,'fontsize',xfont,'fontname','arial');
% sig1=ranksum(allkappas(find(ripvec==0)),allkappas(find(ripvec==1)));
% if sig1<0.05 & sig1>0.01
%     text(1.5, 0.25,'*','FontSize',30)
% elseif sig1<0.01 & sig1>0.005
%     text(1.5, 0.25,'**','FontSize',30)
% elseif sig1<0.005
%     text(1.5, 0.25,'***','FontSize',30)
%     
% end
% set(gca, 'box', 'off')
% xlim([0.4 2.8])
% 
% 
% figfile = [figdir,'ThetaVsRippleMod2'];
%print(fig3,'-dpdf', figfile);
%%
%load('/opt/data15/gideon/HP_ProcessedData/HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014_FStagged.mat');
%load('/opt/data15/gideon/HP_ProcessedData/HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X1.mat');

clear
savedirX = '/data25/sjadhav/HPExpt/HP_ProcessedData/';

figdir = '/data25/sjadhav/HPExpt/Figures/PreSleep/';
savefig1=0;
set(0,'defaultaxesfontsize',16); xfont=16; yfont=16; tfont=16;

areatype = 1;
plotegs = 0;

switch areatype
    case 1
        %load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6_postsleep']); 
        load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6_postsleep_sws_HP']); 

        filestr = 'PFC';
%     case 2
%         load([savedirX 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6']);
%         filestr = 'CA1';
end
        
% ----------------------------
% Individual examples
% ----------------------------

if plotegs==1 && areatype==1 %(Plot PFC example raster and psths)
    savefig=0;
    %figdir = '/data25/sjadhav/HPExpt/Figures/RipTrig2015/Examples';
    figdir = '/data25/sjadhav/HPExpt/Figures/RipTrig2015/Examples/Random_100SWRs/';
    pret=500; post=500;
    % 1st Submission EGs
    % forFigInds=[1 2 17 2; 4 15 21 4; 4 15 21 3;2 3 10 1];
    forFigInds = [1 2 17 2; 4 15 21 4; 2 2 12 1; 4 15 21 1; 2 1 9 1; 4 9 21 2; 4 15 21 3; 2 3 10 1;  1 8 17 6; 1 8 17 5];
    % Exc/Exc/Exc/Exc/Inh/Inh/Inh/Neu/Neu/Neu
    
    for n = 8:size(forFigInds,1)       
        ex=find(ismember(allripplemod_idx,forFigInds(n,:),'rows'));
        
        curridx = allripplemod(ex).index; anim = curridx(1); day = curridx(2); tet = curridx(3); cell = curridx(4);
        currhist = allripplemod(ex).hist; currhisterr = sem(currhist); currhist = mean(currhist);
        currraster = allripplemod(ex).raster;
        modln_var2 = allripplemod(ex).varRespAmp2;
        p_var2 = allripplemod(ex).rasterShufP2;
        sigvar2=0; if p_var2<0.05, sigvar2=1; end
        currtype = allripplemod(ex).type;
        cellfr = allripplemod(ex).cellfr;
        
        % Plot 1
        numRips=length(currraster);
        
        nlen = randperm(numRips); use=[];
        
        % Use random 100 SWRs
        % ------------------
        
        numRips=100;
        for i=1:numRips,
            use{i} = currraster{nlen(i)};
        end
        currraster = use;
        
        
        % creating a raster matrix of 0/1 with 1ms resolution it goes from -500 to +500ms relative to ripples
        curRastMat=zeros(numRips,1100);
        for i=1:numRips,curRastMat(i,round(currraster{i}+551))=1;end
        curRastMat = curRastMat(:,50:1050);
        
        
        % Histogram comment start
        % ------------------------
%         figure; hold on;  
%         %sm=10; im=1;
%         sm=50; im=2;
%         if sm==10
%             imagesc(flipud(filter2(ones(10)/100,curRastMat))); xlim([-1 1001]);
%         else
%             imagesc(flipud(filter2(ones(50)/100,curRastMat))); xlim([-1 1001]);
%         end
%         set(gca,'XTick',[0,250,500,750,1000],'XTickLabel',num2str([-500,-250,0,250,500]')); 
%         y2=numRips; ypts = 0:1:y2; xpts = 500*ones(size(ypts));
%         plot(xpts , ypts, 'k--','Linewidth',2);
%         title(['idx= ' num2str(curridx) ';  P= ' num2str(p_var2) ' ' allripplemod(ii).type])
% 
%         %subplot(2,1,2); hold on;
%         figfile = [figdir,'RippleAlignRasterImage',num2str(im),'_',num2str(anim),'-',num2str(day),'-',num2str(tet),'-',num2str(cell)]
%         if savefig==1,
%             print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
%         end
%         
%         figure; hold on;
%         plot(1000*filtfilt(b,1,mean(curRastMat)),'k-','LineWidth',2) ;xlim([0 1000]);
%         set(gca,'XTick',[0,250,500,750,1000],'XTickLabel',num2str([-500,-250,0,250,500]')); 
%         title(['idx= ' num2str(curridx) ';  P= ' num2str(p_var2) ' ' allripplemod(ii).type])
%         y2 = 1.1*1000*max(filtfilt(b,1,mean(curRastMat))); ypts = 0:1:y2; xpts = 500*ones(size(ypts));
%         plot(xpts , ypts, 'k--','Linewidth',2);
%         set(gca,'YLim',[0 y2])
%         
%         figfile = [figdir,'RippleAlignHist_',num2str(anim),'-',num2str(day),'-',num2str(tet),'-',num2str(cell)]
%         if savefig==1,
%             print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
%         end
        
        % Histogram comment end
        % ------------------------
        
        
        %keyboard
        %close all
        
        % Plot 2
        % -------
        % 1) Raster: 
        figure; hold on; %redimscreen_figforppt1;
        %set(gcf,'Position',[100 130 1000 950]);
        %subplot(2,1,1); hold on;
        spkcount = [];
        for c=1:length(currraster)
            tmps = currraster{c};
            %plotraster(tmps,(length(currraster)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
            
            %plot(tmps,(length(currraster)-c+1)*ones(size(tmps)),'ko','MarkerSize',4, 'MarkerFaceColor','k');
            
            sj_plotraster(tmps,(length(currraster)-c+1)*ones(size(tmps)),0.8,'k');            
            
            % Get count of spikes in response window
            if ~isempty(tmps)
                subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
                spkcount = [spkcount; length(subset_tmps)];
            end
        end
        set(gca,'XLim',[-550 550]); set(gca,'XTick',[])
        set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'));     
        %set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
        xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
        ylabel('SWR number','FontSize',yfont,'Fontweight','normal');
        set(gca,'YLim',[0 numRips]);
        % Plot Line at 0 ms 
        ypts = 0:1:length(currraster); xpts = 0*ones(size(ypts));
        plot(xpts , ypts, 'k--','Linewidth',2);
        %title(sprintf('%s Day %d Tet %d Cell %d Nspkwin %d FR %g', prefix, day, tet, cell, sum(spkcount), roundn(cellfr,-1)),'FontSize',tfont,'Fontweight','normal');
        title(['idx= ' num2str(curridx) ';  P= ' num2str(p_var2) ' ' allripplemod(ii).type ' FR = ' num2str(roundn(cellfr,-1))])
        
        figfile = [figdir,'RippleAlignRaster_',num2str(anim),'-',num2str(day),'-',num2str(tet),'-',num2str(cell)]
        if savefig==1,
            print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
        end
        
        % -----
        % Hist
        % ----
        figure; hold on; %redimscreen_figforppt1;   %set(gcf, 'Position',[205 136 723 446]); %subplot(2,1,2); hold on;
        %xaxis = -pret:binsize:postt; plot(xaxis,currhist,'k--','Linewidth',2);
        xaxis = 0:binsize:1000; plot(xaxis, currhist,'k--','Linewidth',2);
        xaxis2 = [1:length(mean(curRastMat))];     
        plot(xaxis2,1000*filtfilt(b,1,mean(curRastMat)),'k-','LineWidth',2);
        %plot(xaxis,mean(currhist)+sem(currhist),'b--','Linewidth',1);
        %plot(xaxis,mean(currhist)-sem(currhist),'b--','Linewidth',1);
        
        set(gca,'XLim',[0 1000]);
        %set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'));   
        set(gca,'XTick',[0,250,500,750,1000],'XTickLabel',num2str([-500,-250,0,250,500]'));     
        xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
        ylabel('Firing rate (Hz)','FontSize',yfont,'Fontweight','normal');
       
        yhigh = max(currhist);
        set(gca,'YLim',[0 1.1*yhigh]); %set(gca,'YLim',[ylow-0.1 yhigh+0.1]);
        ypts = 0:0.1:1.1*yhigh; xpts = 500*ones(size(ypts));
        plot(xpts , ypts, 'k--','Linewidth',2);
        title(['idx= ' num2str(curridx) ';  P= ' num2str(p_var2) ' ' allripplemod(ii).type ' FR = ' num2str(roundn(cellfr,-1))])

        figfile = [figdir,'RippleAlignHist2_',num2str(anim),'-',num2str(day),'-',num2str(tet),'-',num2str(cell)]
        if savefig==1,
            print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
        end
       
        
        
        % Raster: Can use spy for sparse matrix as well
        % ------------------------------------------------
        
        
        msize=3; %msize=3;
        figure; hold on;
        %spy(flipud(curRastMat),'ko',msize); % Change axis around
        spy(curRastMat,'ko',msize);
        xlim([0 1000]); set(gca,'XTick',[0,250,500,750,1000],'XTickLabel',num2str([-500,-250,0,250,500]'));     
        y=ylim; y2=y(2); maxi=floor(max(y2/100));       
        set(gca,'YTick',fliplr(y2:-100:0),'YTickLabel',num2str(100*(maxi:-1:0)'));
        title(['idx= ' num2str(curridx) ';  P= ' num2str(p_var2) ' ' allripplemod(ii).type ' FR = ' num2str(roundn(cellfr,-1))]);
        xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
        ylabel('SWR number','FontSize',yfont,'Fontweight','normal');
        ypts = 0:1:y2; xpts = 500*ones(size(ypts));
        plot(xpts , ypts, 'k--','Linewidth',2);
        
        figfile = [figdir,'RippleAlignRasterN_','Spy_Msize', num2str(msize),'_', num2str(anim),'-',num2str(day),'-',num2str(tet),'-',num2str(cell)]
        if savefig==1,
            print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
        end      
        
        
        keyboard;
        
        %close all
              
    end
    
end




% -----------------------
% All Popln
% -----------------------
% this is for the previous data, update.
%forFigInds=[1 2 17 2;4 15 21 4;4 15 21 3;2 3 10 1];

% New Fig Indices (2 exc, 2 inh, 1 Neu)

%forFigInds=[1 2 17 2; 2 2 12 1; 2 1 9 1; 4 9 21 2; 2 3 10 1];

allhists=[];
ripmod1=[];
allindsnoFS=[];
allPsnoFS=[];
for i=1:length(allripplemod)
    %if strcmp(allripplemod(i).FStag,'n')
        allhists=[allhists;(mean(allripplemod(i).hist))];
        ripmod1=[ripmod1 allripplemod(i).rasterShufP2<0.05];
        allindsnoFS=[allindsnoFS; allripplemod(i).index];
        allPsnoFS=[allPsnoFS; allripplemod(i).rasterShufP2];
        
    %end
end

% % to add arrows to the imagesc pointing at the examples from above
% ex1=find(ismember(allindsnoFS,forFigInds(1,:),'rows'));
% ex2=find(ismember(allindsnoFS,forFigInds(2,:),'rows'));
% ex3=find(ismember(allindsnoFS,forFigInds(3,:),'rows'));
% ex4=find(ismember(allindsnoFS,forFigInds(4,:),'rows'));
% ex5=find(ismember(allindsnoFS,forFigInds(5,:),'rows'));

allhistsZ=(allhists-repmat(mean(allhists')',1,101))./repmat(std(allhists')',1,101);
respamp=mean(allhistsZ(:,50:70),2);
[dd ww]=sort(respamp);
% % finding the indices after sorting
% ex1s=find(ww==ex1);
% ex2s=find(ww==ex2);
% ex3s=find(ww==ex3);
% ex4s=find(ww==ex4);
% ex5s=find(ww==ex5);



figure;            set(gcf,'Position',[100 130 400 1000]);
xaxis = -pret:binsize:postt;
imagesc(xaxis,1:max(ww),allhistsZ(ww,:)); colormap('jet');
xlabel('Time (ms)')
ylabel([filestr 'cells'])

figfile = [figdir,'RipTrig_',filestr,'PoplnNorm_PostSleep']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end


% Now Load Run RipTrig, and find commons neurons in run and presleep. Organize presleep by run ordering
% -----------------------------------------------------------------------------------------------------

allripplemod_presl = allripplemod;
allripplemod_idx_presl = allripplemod_idx;

load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6'],'allripplemod','allripplemod_idx'); 

% Find common indices. Can go one by one and use ismember
% like, ind1 = find(ismember(allripplemod_idx_presl,curr_PFCrun,'rows'));
% Or use rowfind, all atonce
match_idx = rowfind(allripplemod_idx, allripplemod_idx_presl);

cnt=0;
allhists=[]; allhists_presl=[];
ripmod1=[]; ripmod1_presl=[];
allindsnoFS=[];
allPsnoFS=[];
for i = 1:length(allripplemod)
    if strcmp(allripplemod(i).FStag,'n')
        if match_idx(i)~=0            
            allhists=[allhists;(mean(allripplemod(i).hist))];
            ripmod1=[ripmod1 allripplemod(i).rasterShufP2<0.05];
            allindsnoFS=[allindsnoFS; allripplemod(i).index];
            allPsnoFS=[allPsnoFS; allripplemod(i).rasterShufP2];  
            
            allhists_presl = [allhists_presl;(mean(allripplemod_presl(match_idx(i)).hist))];
            ripmod1_presl=[ripmod1_presl allripplemod_presl(match_idx(i)).rasterShufP2<0.05];
            
        end       
    end   
end

% Sort run
allhistsZ=(allhists-repmat(mean(allhists')',1,101))./repmat(std(allhists')',1,101);
respamp=mean(allhistsZ(:,50:70),2);
[dd ww]=sort(respamp);

% Sort presl using run indices
allhistsZ_presl=(allhists_presl-repmat(mean(allhists_presl')',1,101))./repmat(std(allhists_presl')',1,101);
[dd_presl ww_presl]=sort(respamp); % Sort using run indices

% Sort presl using presl indices, only for subset of neurons also defined
% in run
respamp_presl=mean(allhistsZ_presl(:,50:70),2);
[dd_presl_ori ww_presl_ori]=sort(respamp_presl); 

% Plot sorted run
% ---------------
figure; set(gcf,'Position',[100 130 400 1000]);
%subplot(1,2,1); hold on;
xaxis = -pret:binsize:postt;
imagesc(xaxis,1:max(ww),allhistsZ(ww,:)); colormap('jet');
xlabel('Time (ms)')
ylabel([filestr 'cells'])
title('Run')

figfile = [figdir,'RipTrig_',filestr,'PoplnNorm_Run']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end

% Plot presl sorted on run 
% -----------------------------
figure; set(gcf,'Position',[100 130 400 1000]);
%subplot(1,2,2); hold on;
xaxis = -pret:binsize:postt;
imagesc(xaxis,1:max(ww_presl),allhistsZ_presl(ww_presl,:)); colormap('jet');
xlabel('Time (ms)')
ylabel([filestr 'cells'])
title('Post-sleep based on run')

figfile = [figdir,'RipTrig_',filestr,'PoplnNorm_RunvsPostslBasedOnRun']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end

% Plot presl sorted by itself for subset also defined in run
% ------------------------------------
figure; set(gcf,'Position',[100 130 400 1000]);
%subplot(1,2,2); hold on;
xaxis = -pret:binsize:postt;
imagesc(xaxis,1:max(ww_presl_ori),allhistsZ_presl(ww_presl_ori,:)); colormap('jet');
xlabel('Time (ms)')
ylabel([filestr 'cells'])
title('Post-sleep sorted by itself')

figfile = [figdir,'RipTrig_',filestr,'PoplnNorm_RunvsPostslBasedOnSelf']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end


keyboard;




%% population histograms, currently unused, can uncomment
figure; set(gcf,'Position',[100 130 1000 1000]);
plot(xaxis,mean(allhistsZ),'k--','linewidth',3);
hold on;
plot(xaxis,mean(allhistsZ(ripmod1==1,:)),'k','linewidth',3);

plot(xaxis,mean(abs(allhistsZ)),'r--','linewidth',3);
plot(xaxis,mean(abs(allhistsZ(ripmod1==1,:))),'r','linewidth',3);

plot(xaxis,var(allhistsZ),'b--','linewidth',3)
plot(xaxis,var(allhistsZ(ripmod1==1,:)),'b','linewidth',3)

legend({'Mean, all','Mean, ripmod','Mean abs, all','Mean abs, ripmod','Var, all','Var, ripmod'})
xlabel('Time (ms)')
title([filestr 'cells']);

figfile = [figdir,'RipTrig_',filestr,'PoplnHist']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end


% Only W track
% ------------
allhists=[]; ripmod1=[]; allindsnoFS=[]; allPsnoFS=[];
for i=1:length(allripplemod)
    curranim = allripplemod(i).index(1);
    if strcmp(allripplemod(i).FStag,'n')
        if curranim<=3
            allhists=[allhists;(mean(allripplemod(i).hist))];
            ripmod1=[ripmod1 allripplemod(i).rasterShufP2<0.05];
            allindsnoFS=[allindsnoFS; allripplemod(i).index];
            allPsnoFS=[allPsnoFS; allripplemod(i).rasterShufP2];
        end      
    end
end

allhistsZ=(allhists-repmat(mean(allhists')',1,101))./repmat(std(allhists')',1,101);
respamp=mean(allhistsZ(:,50:70),2);
[dd ww]=sort(respamp);

figure;            set(gcf,'Position',[100 130 400 1000]);
xaxis = -pret:binsize:postt;
imagesc(xaxis,1:max(ww),allhistsZ(ww,:))
xlabel('Time (ms)')
ylabel([filestr 'cells'])
title('W track only')

figfile = [figdir,'RipTrig_',filestr,'PoplnNorm_Wtrack']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);  
end
    
% Only Y track
% ------------
allhists=[]; ripmod1=[]; allindsnoFS=[]; allPsnoFS=[];
for i=1:length(allripplemod)
    curranim = allripplemod(i).index(1);
    if strcmp(allripplemod(i).FStag,'n')
        if curranim>=4
            allhists=[allhists;(mean(allripplemod(i).hist))];
            ripmod1=[ripmod1 allripplemod(i).rasterShufP2<0.05];
            allindsnoFS=[allindsnoFS; allripplemod(i).index];
            allPsnoFS=[allPsnoFS; allripplemod(i).rasterShufP2];
        end      
    end
end

allhistsZ=(allhists-repmat(mean(allhists')',1,101))./repmat(std(allhists')',1,101);
respamp=mean(allhistsZ(:,50:70),2);
[dd ww]=sort(respamp);

figure;            set(gcf,'Position',[100 130 400 1000]);
xaxis = -pret:binsize:postt;
imagesc(xaxis,1:max(ww),allhistsZ(ww,:))
xlabel('Time (ms)')
ylabel([filestr 'cells'])
title('Y track only')

figfile = [figdir,'RipTrig_',filestr,'PoplnNorm_Ytrack']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);  
end























%% currently unused, SWR modulation against Kappa, can uncomment
%    xaxis = -pret:binsize:postt;
%   res1=0.1;
%   meanvec1=[];for pp=0:res1:0.3,meanvec1=[meanvec1 mean(allripmodln_var2(allkappas>pp&allkappas<(pp+res1)))];end
% meanvec1=[meanvec1 mean(allripmodln_var2(allkappas>(0.3+res1)))]
% semvec1=[];for pp=0:res1:0.3,semvec1=[semvec1 std(allripmodln_var2(allkappas>pp&allkappas<(pp+res1)))/sqrt(sum(allkappas>pp&allkappas<(pp+res1)))];end
% semvec1=[semvec1 std(allripmodln_var2(allkappas>(0.3+res1)))/sqrt(sum(allkappas>(0.3+res1)))];
% figure;errorbar(1:5,meanvec1,semvec1,'k','linewidth',2)
% set(gca,'xticklabel',{'','0-0.1','0.1-0.2','0.2-0.3','0.3-0.4','>=0.4'})
% xlabel('Kappa')
% ylabel('SWR modulation')
%%
% Kappas vs. Ripple Modulation (now unused):
%-------------------------------------
%  fig1=figure('PaperPosition',[0 0 14 8],'PaperSize',[14 8])
%   hold on; redimscreen_figforppr1;
%   set(gcf,'Position',[100 130 1000 800]);
%
% plot(allkappas, allripmodln, 'ko','MarkerSize',4);
% hold on
%  plot(allkappas(sigrip), allripmodln(sigrip), 'o','markeredgecolor',[0.5 0.5 0.5],'markerfacecolor',[0.5 0.5 0.5],'MarkerSize',2,'LineWidth',2);
% hold on
% plot(allkappas(sigtheta), allripmodln(sigtheta), 'ro','MarkerSize',5,'LineWidth',1.5);
%
%
% %title(sprintf('%s: Theta vs %s Ripple Modulation', area, statename),'FontSize',tfont,'Fontweight','normal');
% xlabel(['Kappa'],'FontSize',xfont,'Fontweight','normal');
% ylabel(sprintf('%s Ripple Modln',statename),'FontSize',yfont,'Fontweight','normal');
% %legend('All','Rip-Mod','Theta-Mod','fontsize',xfont-2);
% xlim([0 1.1])
% set(gca,'fontsize',xfont);
% [r1,p1] = corrcoef(allkappas,allripmodln)
% [r1rt,p1rt] = corrcoef(allkappas(sigboth),allripmodln(sigboth))
% [r1t,p1t] = corrcoef(allkappas(sigtheta),allripmodln(sigtheta))
% [r1r,p1r] = corrcoef(allkappas(sigrip),allripmodln(sigrip))
%
% xaxis = 0:0.1:1.5;
% plot(xaxis,zeros(size(xaxis)),'k--','LineWidth',2);
%
% str = ''; rstr = ''; tstr = ''; rtstr = '';
% if p1(1,2)<0.05, str = '*'; end
% if p1(1,2)<0.01, str = '**'; end
% if p1(1,2)<0.001, str = '***'; end
% if p1r(1,2)<0.05, rstr = '*'; end
% if p1r(1,2)<0.01, rstr = '**'; end
% if p1r(1,2)<0.001, rstr = '***'; end
% if p1t(1,2)<0.05, tstr = '*'; end
% if p1t(1,2)<0.01, tstr = '**'; end
% if p1t(1,2)<0.001, tstr = '***'; end
% if p1rt(1,2)<0.05, rtstr = '*'; end
% if p1rt(1,2)<0.01, rtstr = '**'; end
% if p1rt(1,2)<0.001, rtstr = '***'; end
%
%
% if strcmp(area,'CA1')
%     %set(gca,'XLim',[0 2200]); set(gca,'YLim',[0 1600]);
%     if ~strcmp(state,'sleep')
%         text(1.1,900,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
%         text(1.1,800,sprintf('Signf in Run: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
%         text(1.1,700,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
%         text(1.1,600,sprintf('R = %0.2f%s',r1(1,2), str),'FontSize',30,'Fontweight','normal');
%     end
% else % PFC
%     if ~strcmp(state,'sleep')
% %         set(gca,'XLim',[-0.02 1.5]);
% %         %set(gca,'YLim',[-120 200]); % Modln peak
% %         %set(gca,'YLim',[-80 100]); % Modln
% %         text(1.1,35,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
% %         text(1.1,30,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
% %         text(1.1,25,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
% %         text(1.1,20,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
% %         text(1.1,15,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
% %         text(1.1,11,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
% %         text(1.1,7,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
% %         text(1.1,3,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');
%
% %         set(gca,'XLim',[-0.02 1]);
% %         %set(gca,'YLim',[-120 200]); % Modln peak
% %         %set(gca,'YLim',[-80 90]); % Modln
% %         text(0.05,40,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
% %         text(0.62,35,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
% %         text(0.62,30,sprintf('Signf in Ripple: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
% %         text(0.62,25,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
% %         text(0.62,20,sprintf('R = %0.2f%s',r1(1,2), str),'FontSize',30,'Fontweight','normal');
% %         text(0.62,5,sprintf('Rr = %0.2f%s',r1r(1,2), rstr),'FontSize',30,'Fontweight','normal');
% %         text(0.62,10,sprintf('Rt = %0.2f%s',r1t(1,2), tstr),'FontSize',30,'Fontweight','normal');
% %         text(0.62,15,sprintf('Rrt = %0.2f%s',r1rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');
%
%     else
%         set(gca,'XLim',[-0.02 1.2]);
%         %set(gca,'YLim',[-120 500]); % Modln peak
%         %set(gca,'YLim',[-80 250]); % Modln
%         text(0.2,40,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
%         text(0.4,35,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
%         text(0.4,30,sprintf('Signf in Sleep SWR: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
%         text(0.4,25,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
%         text(0.4,20,sprintf('R = %0.2f%s',r1(1,2), str),'FontSize',30,'Fontweight','normal');
%         text(0.4,5,sprintf('Rr = %0.2f%s',r1r(1,2), rstr),'FontSize',30,'Fontweight','normal');
%         text(0.4,10,sprintf('Rt = %0.2f%s',r1t(1,2), tstr),'FontSize',30,'Fontweight','normal');
%         text(0.4,15,sprintf('Rrt = %0.2f%s',r1rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');
%
%     end
% end
%
%
% % Regression
% % -----------
% % [b00,bint00,r00,rint00,stats00] = regress(allripmodln', [ones(size(allkappas')) allkappas']);
% % xpts = min(allkappas):0.01:max(allkappas);
% % bfit00 = b00(1)+b00(2)*xpts;
% % plot(xpts,bfit00,'k-','LineWidth',2);  % Theta vs Rip
% % rsquare = stats00(1);
%
%
% % Regression for only SWR modulated cells
% % --------------------------------------------
% [b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigrip)', [ones(size(allkappas(sigrip)')) allkappas(sigrip)']);
% xpts = min(allkappas):0.01:max(allkappas);
% bfit00 = b00(1)+b00(2)*xpts;
% plot(xpts,bfit00,'-','color',[0.5 0.5 0.5],'LineWidth',4);  % Theta vs Rip - Only SWR significant
% rsquare = stats00(1);
%
% % Regression for only theta modulated cells
% % --------------------------------------------
% [b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigtheta)', [ones(size(allkappas(sigtheta)')) allkappas(sigtheta)']);
% xpts = min(allkappas):0.01:max(allkappas);
% bfit00 = b00(1)+b00(2)*xpts;
% plot(xpts,bfit00,'r-','LineWidth',4);  % Theta vs Rip - Only SWR significant
% rsquare = stats00(1);
% figfile = [figdir,'ThetaVsRippleMod'];
%  print(fig1,'-dpdf', figfile);

% Venn diagram for theta and ripple modulated cells (now unused):
% A = [length(sigrip) length(sigtheta)]; I = length(sigboth);
% fig2b=figure('PaperPosition',[0 0 3 5],'PaperSize',[4 5]);
% venn(A,I,'FaceColor',{[0.5 0.5 0.5],'r'},'FaceAlpha',{0.4,0.4},'EdgeColor','black')
%
%   text(6, 8,'Theta modulated','FontSize',xfont)
% text(-14, 8,'Ripple modulated','FontSize',xfont)
% text(1, 0,[num2str(length(sigboth))],'FontSize',xfont+2,'fontweight','bold')
% text(7, 0,[num2str(length(sigtheta)-length(sigboth))],'FontSize',xfont+2,'fontweight','bold')
% text(-4.4, 0,[num2str(length(sigrip)-length(sigboth))],'FontSize',xfont+2,'fontweight','bold')
% axis equal
% axis([-15 20 -12 12])
% axis off;
% figfile = [figdir,'_ThetaRippleModVenn'];
%  print(fig2b,'-dpdf', figfile);




   