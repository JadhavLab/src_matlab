
function phaseProc = statPhaseProcFile(phaseFile,dataAfterTimestamp,nTrode)

	normalize=@(x) x/max(x);

	if nargin < 1
		phaseFile = '~/Downloads/phaseProcess.csv';
	end

	pd = csvread(phaseFile);

	%% Parse columns
	
	if nargin<2
		dataAfterTimestamp=false; 
	end
	
	c=1;
	timestamp = pd(:,c);	c=c+1;
	if dataAfterTimestamp
		data = pd(:,c);		c=c+1;
	else
		data = zeros(size(pd(:,c))); c=c+1;
	end
	inLockout = pd(:,c);	c=c+1;
	masterStim = pd(:,c);	c=c+1;
	bufferSize = pd(:,c);	c=c+1;
	currInd = pd(:,c);		c=c+1;
	prevInd = pd(:,c);		c=c+1;
	currFiltVal = pd(:,c);	c=c+1;
	prevFiltVal = pd(:,c);	c=c+1;
	currPhase = pd(:,c);	c=c+1;
	currAmplitude = pd(:,c);c=c+1;
	specStimOn = pd(:,c);	c=c+1;
	enabled = pd(:,c);		c=c+1;
	nTrodeId = pd(:,c);
	fprintf('Unique nTrodeIds ... ');
	disp(unique(nTrodeId));
	
	if nargin < 3
		nTrode = sort(unique(nTrodeId));
		nTrode = nTrode(nTrode>0);
		nTrode = nTrode(1);
	end
	
	%% Construct a table
	
	phaseProc=table(timestamp,data,bufferSize,currInd,prevInd,currFiltVal,prevFiltVal,...
		currPhase,currAmplitude,specStimOn, enabled,nTrodeId);
	
	selector = phaseProc.nTrodeId == nTrode;
	time = phaseProc.timestamp(selector)/3e4;
    
    %% Calculate trough
    trough = phaseProc.currPhase(selector) == 4;
    peak = phaseProc.currPhase(selector) == 2;
	
	%% Plot Data and FS Filtered Data
	
	% Plot Relevant Values
	figure(200);
	clf
% 	subplot(2,1,1);
	plot(time, normalize(phaseProc.data(selector)),'k ','linewidth',2);
	hold on;
% 	plot(time, normalize(phaseProc.currPhase(selector)),'o-');
	p=plot(time, normalize(phaseProc.currFiltVal(selector)),'s-');
	plot(time, normalize(phaseProc.currAmplitude(selector)),'-');
	xlabel('Seconds');
% 	subplot(2,1,2);
% 	plot(phaseProc.timestamp(selector)/3e4, phaseProc.currInd(selector),'g--')
% 	xlabel('Seconds');
% 	legend('Current Index of Buffer');

	%% Highlight stimulated zones
	
	stimOn = logical([(diff(peak) == 1)' 0]);
	stimOff = logical([0 (diff(peak) == 1)']);
	
	stimTimesX = [time(stimOn)'; time(stimOff)'];
	stimTimesY = [ones(size(time(stimOn)))'; zeros(size(time(stimOff)))'];
	
	p=patch('XData',stimTimesX,'YData',stimTimesY);
	p.FaceAlpha=0.5; p.FaceColor=[0 1 0];p.EdgeAlpha=.7;p.EdgeColor=[0 1 0];
    
    stimOn = logical([(diff(trough) == 1)' 0]);
	stimOff = logical([0 (diff(trough) == 1)']);
	
	stimTimesX = [time(stimOn)'; time(stimOff)'];
	stimTimesY = [ones(size(time(stimOn)))'; zeros(size(time(stimOff)))'];
    
    p=patch('XData',stimTimesX,'YData',stimTimesY);
	p.FaceAlpha=0.5; p.FaceColor=[0 0 0];p.EdgeAlpha=.7;p.EdgeColor=[0 0 0];
    
	legend('Data','Current Filtered Value','Current Amplitude','Stimulation Peak','Stimulation Trough');
	axis([-inf inf -1 1]);
	
end