function [ pulse_on, pulse_off, ts_on, ts_off ] = pulseAnal( dio,timestamps,recurse)
% Purpose: Automatically figure out the timestamp times at which the laser pulse
% turns on and off .. without knowing anything about the pulse pattern
% uploaded through FSGui

    if sum(dio)==0
        pulse_on=[];
        pulse_off=[];
        ts_on=[];
        ts_off=[];
        return;
    end

	dio_on = dio;
	for r = 1:recurse
		
		%% Retrieve the smallest interpulse interval range
		smallest_jmp = findSmallestPulseDiff(dio_on);

		%% Calculate pulse ons

		ind_dio_ons = find(dio_on);
		indx = find( ...
			(smallest_jmp(1) <= diff(ind_dio_ons)) & (smallest_jmp(2) >= diff(ind_dio_ons))...
			) + 1;
		desired_set = setdiff(ind_dio_ons,ind_dio_ons([indx]));

		pulse_on = zeros(size(dio_on));
		pulse_on(desired_set) = 1;
		
		ts_on = timestamps(logical(pulse_on));
		
		%% Calculate pulse offs
		
		dio_on = pulse_on;
		
	end
	
	dio_off = dio;
	for r = 1:recurse
		
		%% Retrieve the smallest interpulse interval range
		smallest_jmp = findSmallestPulseDiff(dio_off);

		%% Calculate pulse ons

		ind_dio_offs = find(dio_off);
		indx = find( ...
			(smallest_jmp(1) <= diff(ind_dio_offs)) & (smallest_jmp(2) >= diff(ind_dio_offs))...
			);
		desired_set = setdiff(ind_dio_offs,ind_dio_offs([indx]));

		pulse_off = zeros(size(dio_off));
		pulse_off(desired_set) = 1;
		
		ts_off = timestamps(logical(pulse_off));
		
		%% Calculate pulse offs
		
		dio_off = pulse_off;
		
	end
	

    %% HELPER FUNCTIONS
    % -------------------------------------------------------------------------
    function smallest_jmp = findSmallestPulseDiff(dio)
        
		% Grab diff of the dio pattern
        dio_on_ind = find((dio == 1));
        diff_ind = diff(dio_on_ind);
        
        if isempty(dio_on_ind) || isempty(diff_ind)
            warning('No pulses in DIO');
            smallest_jmp=1;
            return;
        end
		
		min_diff = min(diff_ind);
		max_diff = max(diff_ind);
		
		% This function assumes reliability in the dio pulse time. A
		% quick sanity check is to make sure we have a very heavy tailed
		% distribution.
%  		assert( kurtosis(diff_ind) > 50 ); % should be in the 100s, actually
        
		% Bin counts to explore
        bincount = max_diff-min_diff;
		
		% Compute edges for the binning process
		edges = linspace(min_diff-1,max_diff+1,bincount+2);
		
		% Compute bin count
		[N, edges] = histcounts(diff_ind, edges);
		
		% Compute the idx containing the pulse count
		[~,pulse_idx]=max(N);
		
		while N(pulse_idx+1) > (0.025*N(pulse_idx))
			pulse_idx=pulse_idx+1;
		end
		
		% 
		smallest_jmp = [edges(1) edges(pulse_idx+1)];
		
	end
        
end
