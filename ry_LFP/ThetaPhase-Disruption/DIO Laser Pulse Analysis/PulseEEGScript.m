%% Shortcut functions and where to save

psm = @(x) fprintf([x '\n']);

%% User-based Save Directory and Options

[~,username] = unix('whoami');
switch deblank(username)
    case {'ss','ryoung'}
        where_to_save = '~/Data/Local/Current/';
		set(0,'DefaultFigureWindowStyle','docked');
    case 'jadhavlab'
        where_to_save = '~/Data/Local/Current/';
		set(0,'DefaultFigureWindowStyle','docked');
    otherwise
        where_to_save= input('Name your figure folder:','s');
end

%% Program Flags
remove_reference_channel = true;
plot_hilbert_envelope = false;
zerod_at_pulse = false;

downSampledRate = 1500;
filter_range=[6,12];
laserchannel=17;

%% Run Automated Extraction
% This segment rsyncs the relevent requested rec file in, then applies
% TrodesToMatlab on channels inputted. Made this a stand
% alone script because it can easily be used by itself, outside this
% analysis.
filepath='/Volumes/mordorDATA/Phase/CL1_StraightLaserPulse10ms.rec';
ff_save = Extraction(filepath);
load(ff_save);

%% Calculate downsampled ripple filtered power
% Same trick Daniel uses

% Downsample data and put into a new structure
samplingRateScale = continuous.samplingRate / downSampledRate;
downSampledData.channelData = downsample(continuous.channelData, samplingRateScale);
downSampledData.timestamps = downsample(continuous.timestamps, samplingRateScale);
downSampledData.samplingRate = downSampledRate;

% Copy data information into new structure
downSampledData.headerSize = continuous.headerSize;
downSampledData.numChannels = continuous.numChannels;

% Ripple filter
[rfilt_b, rfilt_a] = butter(4, filter_range/(downSampledRate/2));
downSampledData.filterData = filter(rfilt_b, rfilt_a, downSampledData.channelData);
clear rfilt_b rfilt_a;

%%  Create Shortcut Variables for Commonly Used Objects
% Otherwise, referencing giant structs becomes pretty tedious

% Two key sampling rates, the standard 30khz and downsampled
csr_c = continuous.samplingRate;
% csr_rf = rip_continuous.samplingRate;
csr_rf=downSampledData.samplingRate;

% Dio channel and timestamps
dio_chan = dio.channelData(laserchannel).data;
dio_ts = dio.timestamps;

% Continuous channel and continuos timestamps
cont_chan = continuous.channelData;
cont_ts = continuous.timestamps;

% Ripple power channel data and corresponding timestamps
filt_pow = downSampledData.filterData;
filt_ts = downSampledData.timestamps;

% Hilbert based envelope
y = hilbert(filt_pow);
filt_env = abs(y);
filt_phase = (unwrap(angle(y)))';
clear y;

%% Remove Reference Channels
% If user requests it!, then we remove the specified reference in the
% extraction phase

if remove_reference_channel
	cont_chan = cont_chan - repmat(continuous_ref.channelData, ...
		[1,size(cont_chan,2)]);
end

% % Plot DIO
% figure(1);clf;
% 
% plot(dio_ts,0.6*dio_chan,'b--');
% hold on;

% Extract Pulse Train Begnining and Ends, then plot

% The following function automatically looks at the session and determines
% pulse train begninnigs and ends. It doesn't require the user knowing the
% frequency of the pulse or any other details.
[logical_on, logical_off, onsets, offsets] = pulseAnal(dio_chan,dio_ts,2); % last number 2 for monophasic trains, and 3 for biphasic!
logical_on = cast(logical_on,'uint8');
stim_time_ind = find(diff(dio_chan>0));
clear('logical_off');

% % Plotting starts and stops of trains
% plot(onsets, 0.6*ones(size(onsets)),'g*');hold on;
% plot(offsets, 0.6*ones(size(offsets)),'r*');
% axis([-inf inf -0.2 1.1]);
% legend('DIO','Detected Pulse Train Onset','Detected Pulse Train Offset');
% 
% % Save fig file
% filename = 'DIO_pulsetrain_onsets_offsets';
% mkdir(fullfile(where_to_save, filename));
% saveas(gcf, fullfile(where_to_save, filename),'jpeg');

%% Acquire continuos data snippets before and after each pulse train

% Establish windows we'll be using for snippets of regular and downsampled
% data
secondsafter = 0.25;
secondsbefore = 0.75 + 200/1000; % adding 200 milliseconds
samples_win = [(secondsbefore*csr_c) (secondsafter*csr_c)];
samples_win_rf = [(secondsbefore*csr_rf) (secondsafter*csr_rf)];

% Initialize variables that will record snippets of each variable we care
% about
eeg_record = NaN * ones( size(cont_chan,2), numel(onsets), 1 + secondsafter*csr_c + secondsbefore*csr_c );
filt_record = NaN * ones( size(filt_pow, 2), numel(onsets), 1+ secondsafter*csr_rf + secondsbefore * csr_rf);
filt_env_record = filt_record; % initializes to the same blank slate state as the foregoing

dio_record = NaN * ones( numel(onsets), 1 + secondsafter*csr_c + secondsbefore*csr_c );

time_record = NaN * ones( numel(onsets), 1 + secondsafter*csr_c + secondsbefore*csr_c );
time_record_rf = NaN * ones( numel(onsets), 1 + secondsafter*csr_rf + secondsbefore*csr_rf );

%% Pull eeg snippets
% Ought to vectorize this as it's actually quite slow and has quite a bit
% of potential for vectorization.
set(0,'DefaultFigureWindowStyle','normal');
w = waitbar(0, ...
        'Snipping out EEG/DIO/RippleFiltered data ...');
for record = 1:numel(onsets)
	
	w = waitbar(record/numel(onsets), w);
	
	[~, ind] = min(abs(onsets(record)-cont_ts));
	[~, ind_rip] = min(abs(onsets(record)-filt_ts));
    [~, ind_dio] = min(abs(onsets(record)-dio_ts));
	
	beginsamp_c         = ind - samples_win(1);
	endsamp_c           = ind + samples_win(2);
	beginsamp_rip		= ind_rip - samples_win_rf(1);
	endsamp_rip         = ind_rip + samples_win_rf(2);
	
	if endsamp_c > size(cont_chan,1); continue; end
	if beginsamp_c < 1; continue; end
	
    %% Window channel data
    
	for chan_ind = size(cont_chan,2)
		eeg_record(chan_ind,record,:) = ...
			cont_chan(beginsamp_c:endsamp_c,chan_ind);
		filt_record(chan_ind,record,:) = ...
			filt_pow(beginsamp_rip:endsamp_rip, chan_ind);
		filt_env_record(chan_ind,record,:) = ...
			filt_env(beginsamp_rip:endsamp_rip, chan_ind);
        filt_phase_record(chan_ind,record,:) = ...
			filt_env(beginsamp_rip:endsamp_rip, chan_ind);
	end
    
	%% Window timestamps (probably dumb because it's uniform, but whatever)
    
	time_scaled = cont_ts - onsets(record);
	time_record(record,:) = time_scaled(beginsamp_c:endsamp_c); 

	timescaled = filt_ts - onsets(record);
	time_record_rf(record,:) = timescaled(beginsamp_rip:endsamp_rip);
    
	%% Window DIO stuffs
    
    dio_record(record,:) = dio_chan(beginsamp_c:endsamp_c)';
	offset_diff(record) = offsets(record) - onsets(record);
	
end; close(w)

%% Convert butterworthed ripple power into smoothed envelope

% % Standard smoothing of absolute value of butterworth filtered
% % channel using a MOVING AVERAGE
% % filt_record = abs(filt_record);
% for i = 1:size(filt_record,1)
% 	for j = 1:size(filt_record,2)
% 		filt_record(i,j,:) = smooth(filt_record(i,j,:),400);
% 	end
% end
% 
% % Standard smoothing of absolute value of butterworth-hilbert filtered
% % channel using a MOVING AVERAGE
% for i = 1:size(filt_record,1)
% 	for j = 1:size(filt_record,2)
% 		filt_env_record(i,j,:) = smooth(filt_env_record(i,j,:),5);
% 	end
% end


%% Acquire MultiUnit Binned Data
tic

% Bin parameters
binsize		= (50/1000)/4 %seconds
binshare	= (10/1000)/4 %seconds
	
% Initialize bin edges, before we push starts and stops ahead by the
% binshare variable
time_range = [min(dio_ts) max(dio_ts)];
edges_before_binshare = time_range(1):binsize:time_range(2);

% Generate matrix of starts and stops with binshare regions
starts_stops = [edges_before_binshare(1:end-1) - binshare; ...
	edges_before_binshare(2:end) + binshare]';

starts_stops(1,1) = time_range(1);
starts_stops(end,end) = time_range(2);

bincount = size(starts_stops,1);

% For every tetrode count the spikes in our range
spike_hist = cell(size(spikes_pfc));
count = 0;
w = waitbar(0,'Binning spikes ...');
for tet = 1:numel(spikes_pfc)
	spike_hist{tet} = zeros(bincount,1);
	spike_times = double(spikes_pfc{tet}.fields(1).data)/double(spikes_pfc{tet}.clockrate);
	total = numel(spike_times);
	for s = 1:numel(spike_times)
		% Simplify spike time name, prepare for vectorization, and pull spike
		% counts
		spikes = spike_times(s);
		spikes = repmat(spike,[bincount 1]);

		% Prepare for vectorized counts, we're going to simulateonously check
		% all spikes on all bins
		start_mat = starts_stops(:,1);
		end_mat = starts_stops(:,2);

		% Operate!
		spike_logical_mat = ...
			(spikes >= start_mat) & (spikes < end_mat);

		% Now update the number per bin
		spike_hist{tet} = spike_hist{tet} + spike_logical_mat;
		
		count = count + 1;
		waitbar(count/total, w, sprintf('Binning spikes on tet %d',tet));
		drawnow;
	end
end; close(w);
toc

%% Plot eeg records zero'd on pulse trains

set(0,'DefaultFigureWindowStyle','docked');
i = 1;
while (i > 0) && (i <= size(eeg_record,2))
	
    %% Subplot 1: Raw LFP
	
	figure(2);clf;
	
	subplot(3,1,1);
    for j = 1:size(cont_chan,2)
        eeg = plot( time_record(i,:), squeeze(eeg_record(j,i,:)), 'r:' ); hold on;
    end
	ax=gca;
	ax.XLim = [min(time_record(i,:)) max(time_record(i,:))];
	
	stimonoff_upper = area(time_record(i,:), ax.YLim(2) * dio_record(i,:));
	stimonoff_upper.FaceAlpha=0.1;stimonff_upper.FaceColor=[0 0 1];
	stimonoff_upper.EdgeAlpha=0.01; stimonoff_upper.LineWidth=0.0001;
	stimonoff_upper.LineStyle=':';
	
	stimonoff_lower = area(time_record(i,:), ax.YLim(1) * dio_record(i,:));
	stimonoff_lower.FaceAlpha=0.1;stimonoff_lower.FaceColor=[0 0 1];
	stimonoff_lower.EdgeAlpha=0.01; stimonoff_lower.LineWidth=0.0001;
	stimonoff_lower.LineStyle=':';
	
	% Labeling
	xlabel('Seconds'); ylabel('Raw EEG');
% 	legend(channel1, channel2, 'EEG Pulse Region','Location','SouthWest');
	title(sprintf('Pulse %d', i));
	
    %% Subplot 2: Ripple Power, from smooth/rectified butterworth
	
	subplot(3,1,2);
	
	for j = 1:size(cont_chan,2)
        filt1 = plot( time_record_rf(i,:), squeeze(filt_env_record(j,i,:)), 'b' ); hold on;
        filt2 = plot( time_record_rf(i,:), squeeze(filt_record(j,i,:)), 'r' );
        filt3 = plot( time_record_rf(i,:), squeeze(filt_phase_record(j,i,:)),'k');
    end
	
	ax=gca;
	ax.XLim = [min(time_record_rf(i,:)) max(time_record_rf(i,:))];
	storedLim = ax.XLim;
	
	stimonoff_upper = area(time_record(i,:), ax.YLim(2) * dio_record(i,:));
	stimonoff_upper.FaceAlpha=0.1;stimonff_upper.FaceColor=[0 0 1];
	stimonoff_upper.EdgeAlpha=0.01; stimonoff_upper.LineWidth=0.0001;
	stimonoff_upper.LineStyle=':';
	
	stimonoff_lower = area(time_record(i,:), ax.YLim(1) * dio_record(i,:));
	stimonoff_lower.FaceAlpha=0.1;stimonoff_lower.FaceColor=[0 0 1];
	stimonoff_lower.EdgeAlpha=0.01; stimonoff_lower.LineWidth=0.0001;
	stimonoff_lower.LineStyle=':';
	
	% Labeling
% 	channel1 = sprintf('RipChan %d', tetrode_ids(1));
% 	channel2 = sprintf('RipChan %d', tetrode_ids(2));
	xlabel('Seconds'); ylabel('Ripple Power, Smoothed/Rectified Butterworth');
 	legend('Envelope', 'Smoothed Signal', 'EEG Pulse Region','Location','SouthWest');
	title(sprintf('Pulse %d', i));
	
	%% 3rd Subplot
	if plot_hilbert_envelope
% 		%% Subplot 3: Ripple Env, from hilbert of butterworth
% 		subplot(3,1,3);
%         
%         for j = 1:size(cont_chan,2)
%             rip1 = plot( time_record_rf(i,:), squeeze(filt_env_record(j,i,:)), 'b' ); hold on;
%         end
% 		ax=gca;
% 		ax.XLim = [min(time_record_rf(i,:)) max(time_record_rf(i,:))];
%         storedLim = ax.XLim;
% 
% 		stimonoff_upper = area(time_record(i,:), ax.YLim(2) * dio_record(i,:));
% 		stimonoff_upper.FaceColor=[0 1 0];
% 		stimonoff_upper.LineWidth=0.0001;
% 		stimonoff_upper.LineStyle=':';
% 
% 		stimonoff_lower = area(time_record(i,:), ax.YLim(1) * dio_record(i,:));
% 		stimonoff_lower.FaceColor=[0 1 0];
% 		stimonoff_lower.LineWidth=0.0001;
% 		stimonoff_lower.LineStyle=':';
% 
% 		% Labeling
% % 		channel1 = sprintf('RipChan %d', tetrode_ids(1));
% % 		channel2 = sprintf('RipChan %d', tetrode_ids(2));
% 		xlabel('Seconds'); ylabel(sprintf('Ripple Hilbert Envelope of Butterworth \n Daniel''s Method'));
% % 		legend(channel1, channel2, 'EEG Pulse Region','Location','SouthWest');
% 		title(sprintf('Pulse %d', i));
	else
		%% Subplot 3: Multiunit activity
        subplot(3,1,3); cla
        
        timeLim = [min(time_record_rf(i,:)) ...
            max(time_record_rf(i,:))];
        
        [~,start_ind]   = min(abs(timeLim(1) - edges_before_binshare));
        [~,stop_ind]    = min(abs(timeLim(2) - edges_before_binshare));

        edges= edges_before_binshare(start_ind:stop_ind); %edges= edges_before_binshare(start_ind:stop_ind) - onsets(i);
        centers = edges(1:end-1)+(edges(2:end)-edges(1:end-1))/2;
        spikecounts = spike_hist{1}(start_ind:stop_ind-1)';
        
        b=bar(centers,spikecounts); hold on;
        b.EdgeAlpha=0.1; b.BarWidth=1;
		b.FaceColor=[0 0 1]; b.EdgeColor=[0 0 0];
        
        a=gca;
        a.XLim = storedLim;
		
		stimonoff_upper = area(time_record(i,:), max(spikecounts) * dio_record(i,:));
		stimonoff_upper.FaceAlpha=0.1;stimonff_upper.FaceColor=[0 0 1];
		stimonoff_upper.EdgeAlpha=0.01; stimonoff_upper.LineWidth=0.0001;
		stimonoff_upper.LineStyle=':';
        
		
	end
	
	%% Manual scrolling through data
	% You can hit 'enter' to advance, 'b' to go backwards, or enter a
	% number to advance to plotting that pulse
	res = input(sprintf('Enter to continue or ''b'' then enter to backup: \n'), 's');
	[num, isanumber] = str2num(res);
	if isanumber
		i = num;
	else
		switch res
			case 'b'
				i = i - 1;
			otherwise
				i = i + 1;
		end
	end
	
	%% Save file
	filename = sprintf('PulseTrain%d', i);
	saveas(gcf, fullfile(where_to_save, filename),'png');
	
	if ~exist('res','var')
		i = i + 1;
	end
	
end

%% Code Graveyard
% %% Multiunit
% % Imported from Daniel's code
% 
% % spike frequency range and filter order
% cutoff_low = 600;
% cutoff_high = 6000;
% filt_order = 4;
% 
% % spike filter
% [b_an,a_an] = besself(filt_order,[2*pi*cutoff_low,2*pi*cutoff_high]);
% [b,a] = bilinear(b_an,a_an,csr_c);
% spike_filter_data_all = filter(b,a,cont_chan);
% 
% samples_per_ms = 30;
% win_size = 4;           %ms
% win_center = 1;         %ms
% samples_behind = win_center*samples_per_ms;
% samples_ahead = (win_size-win_center)*samples_per_ms;
% 
% threshold = 40;         %uV
% refractory = 1*samples_per_ms;
% 
% all_waves = {};
% all_spktime_ind = {};
% disp('starting spike filter and thresholding');
% tic;
% for ii = 1:size(spike_filter_data_all,2)
%     spike_filter_data = spike_filter_data_all(:,ii);
%     spk_thresh = find(spike_filter_data > threshold);
%     spk_thresh_refractory_ind = [1 (find(diff(spk_thresh) > refractory) + 1)'];
% 
%     spktime_ind = spk_thresh(spk_thresh_refractory_ind);
% 
%     spk_ind = cell2mat(arrayfun(@(x)(x-samples_behind:x+samples_ahead-1),spktime_ind,'UniformOutput',false));
% 
%     spk_ind = spk_ind(find(~max(spk_ind <= 0 | spk_ind > size(spike_filter_data,1),[],2)),:);
% 
%     spk_trans = spike_filter_data';
%     waves = spk_trans(spk_ind);
%     
%     all_spktime_ind{ii} = spktime_ind;
%     all_waves{ii} = waves;
% end
% toc;

% %% Obtaining average multiunit activity right after stimulation, I think
% % Imported from Daniel's code
% 
% win_min = -500*30;
% win_max = 500*30;
% artifact_min = -2*30;
% artifact_max = 14*30;
% all_stim_spktime_ind = [];
% for ii=1:size(all_spktime_ind,2)
%     disp(['starting stim window multiunit calculation for ', ii]);
%     tic;
%     %spktime_ind = all_spktime_ind{ii}(max(all_waves{ii},[],2) < 600);
%     spktime_ind = all_spktime_ind{ii};
%     spk_near_stim = lookup(spktime_ind,stim_time_ind);
%     for stim_jj = 1:length(onsets)
%         all_stim_spktime_ind = [all_stim_spktime_ind;  ...
%             (spktime_ind((spktime_ind > stim_time_ind(stim_jj) + win_min & ... 
%             spktime_ind < stim_time_ind(stim_jj) + artifact_min) | ...
%             (spktime_ind > stim_time_ind(stim_jj) + artifact_max & ...
%             spktime_ind < stim_time_ind(stim_jj) + win_max)) - stim_time_ind(stim_jj))];
%     end
%     toc;
% end
% figure(4)
% subplot(3,1,1)
% hist_binsize = 2;
% hist_range = [-300, 300];
% %histogram(all_stim_spktime_ind/30, [-500:2:500]);
% [stim_mult_hist,stim_mult_hist_edges] = histcounts(all_stim_spktime_ind/30, [hist_range(1):hist_binsize:hist_range(2)]);
% bar(stim_mult_hist_edges(1:end-1),stim_mult_hist./length(onsets)/(hist_binsize/1000),0.5,'FaceColor',[0.5,0.5,1],'EdgeColor',[0,0,0],'LineWidth',1);
% % title(sprintf('%s (%d)',rec_filename, length(stim_time)));
% xlim(hist_range);


%% Plotting complete trace
% 
% figure(4); clf;
% 
% subplot(2,1,1);
% stim_upper = area(dio_ts,max(cont_chan(1:end))*dio_chan); hold on;
% stim_upper.FaceAlpha=0.1;stim_upper.FaceColor=[0 0 1];
% stim_upper.EdgeAlpha=0.01;
% stim_lower = area(dio_ts,-min(cont_chan(1:end))*dio_chan);
% stim_lower.FaceAlpha=0.1;stim_lower.FaceColor=[0 0 1];
% stim_lower.EdgeAlpha=0.01;
% 
% plot(cont_ts,cont_chan(:,1),'r--');
% plot(cont_ts,cont_chan(:,1),'b--');
% 
% subplot(2,1,2);
% stim_upper = area(dio_ts,max(max(max(rip_pow)))*dio_chan); hold on;
% stim_upper.FaceAlpha=0.1;stim_upper.FaceColor=[0 0 1];
% stim_upper.EdgeAlpha=0.01;
% stim_lower = area(dio_ts,max(max(max(rip_pow)))*dio_chan);
% stim_lower.FaceAlpha=0.1;stim_lower.FaceColor=[0 0 1];
% stim_lower.EdgeAlpha=0.01;
% 
% 
% plot(rip_ts,rip_pow(:,1),'r--');
% plot(rip_ts,rip_pow(:,1),'b--');