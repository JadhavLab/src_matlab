for sess = [7:8]

%% Shortcut functions and where to save

psm = @(x) fprintf([x '\n']);

%% User-based Save Directory and Options

[~,username] = unix('whoami');
switch deblank(username)
    case {'ss','ryoung'}
        where_to_save = '~/Data/Local/Current/ThetaRTFSmallerBins';
		set(0,'DefaultFigureWindowStyle','docked');
    case 'jadhavlab'
        where_to_save = '~/Data/Local/Current/ThetaRTFSmallerBins';
		set(0,'DefaultFigureWindowStyle','docked');% if saveprogress flag set
    otherwise
        where_to_save= input('Name your figure folder:','s');
end

%% Program Flags
remove_reference_channel = true;
plot_hilbert_envelope = false;
zerod_at_pulse = false;

downSampledRate = 1500;
filter_range=[6,12];
laserchannel=17;

%% Run Automated Extraction
% This segment rsyncs the relevent requested rec file in, then applies
% TrodesToMatlab on channels inputted. Made this a stand
% alone script because it can easily be used by itself, outside this
% analysis.
filepath=['/Volumes/mordorDATA/Phase/CL1/20160520/CL1_Phase' num2str(sess) '.rec'];
% filepath=['/Volumes/mordorDATA/Phase/CL1/20160520/CL1_master_' num2str(sess) 'ms.rec'];
% filepath='/Volumes/mordorDATA/Phase/CL1/20160520/CL1_master_15ms.rec';
configFile='/Volumes/mordorDATA/TrodeConfigurations_AllExperiments/Config128_TetrodesHWfix_CL1_12tetrodeDIOs.trodesconf';
% filepath=['/Volumes/mordorDATA/Phase/CL1/20160520/CL1_Phase' num2str(sess) '.rec'];
ff_save = Extraction(filepath,'configFileName',configFile);
load([ff_save '.mat']);
fprintf('Analyzing %s\n',filepath);

[~,file]=fileparts(filepath); where_to_save=fullfile(where_to_save,file);
mkdir(where_to_save);
mkdir(fullfile(where_to_save,'fig'));

%% Calculate downsampled ripple filtered power
% Same trick Daniel uses

if ~isfield(continuous,'downsampled') || continuous.downsampled==false
    % Downsample data and put into a new structure
    samplingRateScale = continuous.samplingRate / downSampledRate;
    downSampledData.data = downsample(continuous.data, samplingRateScale);
    downSampledData.timestamps = downsample(continuous.timestamps, samplingRateScale);
    downSampledData.samplingRate = downSampledRate;

    % Copy data information into new structure
    downSampledData.headerSize = continuous.headerSize;
    downSampledData.numChannels = continuous.numChannels;
else
    downSampledData=continuous;
end

% Ripple filter
[filt_b, filt_a] = butter(4, filter_range/(downSampledRate/2));
downSampledData.filterData = filter(filt_b, filt_a, downSampledData.data);
clear rfilt_b rfilt_a;


%%  Create Shortcut Variables for Commonly Used Objects
% Otherwise, referencing giant structs becomes pretty tedious

% Two key sampling rates, the standard 30khz and downsampled
csr_c   = continuous.samplingRate;
% csr_rf = rip_continuous.samplingRate;
csr_rf  = downSampledData.samplingRate;

% Dio channel and timestamps
dio_chan = dio.data(laserchannel).data;
dio_ts = dio.timestamps;

% Continuous channel and continuos timestamps
cont_chan = continuous.data;
cont_ts = continuous.timestamps;

% Ripple power channel data and corresponding timestamps
filt_pow = downSampledData.filterData;
filt_ts = downSampledData.timestamps;

% Hilbert based envelope
y = hilbert(filt_pow);
filt_env = abs(y);
filt_phase = (unwrap(angle(y)))';
clear y;

%% Remove Reference Channels
% If user requests it!, then we remove the specified reference in the
% extraction phase

if remove_reference_channel
	cont_chan = cont_chan - repmat(continuous_ref.data, ...
		[1,size(cont_chan,2)]);
end

% % Plot DIO
% figure(1);clf;
% 
% plot(dio_ts,0.6*dio_chan,'b--');
% hold on;

% Extract Pulse Train Begnining and Ends, then plot

% The following function automatically looks at the session and determines
% pulse train begninnigs and ends. It doesn't require the user knowing the
% frequency of the pulse or any other details.
[logical_on, logical_off, onsets, offsets] = pulseAnal(dio_chan,dio_ts,2); % last number 2 for monophasic trains, and 3 for biphasic!
logical_on = cast(logical_on,'uint8');
stim_time_ind = find(diff(dio_chan>0));
clear('logical_off');
fprintf('Number of pulses: %d \n', numel(onsets));
if isempty(onsets)
    continue;
end

% % Plotting starts and stops of trains
% plot(onsets, 0.6*ones(size(onsets)),'g*');hold on;
% plot(offsets, 0.6*ones(size(offsets)),'r*');
% axis([-inf inf -0.2 1.1]);
% legend('DIO','Detected Pulse Train Onset','Detected Pulse Train Offset');
% 
% % Save fig file
% filename = 'DIO_pulsetrain_onsets_offsets';
% mkdir(fullfile(where_to_save, filename));
% saveas(gcf, fullfile(where_to_save, filename),'jpeg');

%% Acquire continuos data snippets before and after each pulse train

% Establish windows we'll be using for snippets of regular and downsampled
% data
secondsafter = 0.25;
secondsbefore = 0.75 + 200/1000; % adding 200 milliseconds
samples_win = [(secondsbefore*csr_c) (secondsafter*csr_c)];
samples_win_rf = [(secondsbefore*csr_rf) (secondsafter*csr_rf)];

% Initialize variables that will record snippets of each variable we care
% about
eeg_record = NaN * ones( size(cont_chan,2), numel(onsets), 1 + secondsafter*csr_c + secondsbefore*csr_c );
filt_record = NaN * ones( size(filt_pow, 2), numel(onsets), 1+ secondsafter*csr_rf + secondsbefore * csr_rf);
filt_env_record = filt_record; % initializes to the same blank slate state as the foregoing

dio_record = NaN * ones( numel(onsets), 1 + secondsafter*csr_c + secondsbefore*csr_c );

time_record = NaN * ones( numel(onsets), 1 + secondsafter*csr_c + secondsbefore*csr_c );
time_record_rf = NaN * ones( numel(onsets), 1 + secondsafter*csr_rf + secondsbefore*csr_rf );

%% Pull eeg snippets
% Ought to vectorize this as it's actually quite slow and has quite a bit
% of potential for vectorization.
set(0,'DefaultFigureWindowStyle','normal');
w = waitbar(0, ...
        'Snipping out EEG/DIO/RippleFiltered data ...');
for record = 1:numel(onsets)
	
	w = waitbar(record/numel(onsets), w);
	
	[~, ind] = min(abs(onsets(record)-cont_ts));
	[~, ind_rip] = min(abs(onsets(record)-filt_ts));
    [~, ind_dio] = min(abs(onsets(record)-dio_ts));
	
	beginsamp_c         = ind - samples_win(1);
	endsamp_c           = ind + samples_win(2);
	beginsamp_rip		= ind_rip - samples_win_rf(1);
	endsamp_rip         = ind_rip + samples_win_rf(2);
	
	if endsamp_c > size(cont_chan,1); continue; end
	if beginsamp_c < 1; continue; end
	
    %% Window channel data
    
	for chan_ind = size(cont_chan,2)
		eeg_record(chan_ind,record,:) = ...
			cont_chan(beginsamp_c:endsamp_c,chan_ind);
		filt_record(chan_ind,record,:) = ...
			filt_pow(beginsamp_rip:endsamp_rip, chan_ind);
		filt_env_record(chan_ind,record,:) = ...
			filt_env(beginsamp_rip:endsamp_rip, chan_ind);
        filt_phase_record(chan_ind,record,:) = ...
			filt_env(beginsamp_rip:endsamp_rip, chan_ind);
	end
    
	%% Window timestamps (probably dumb because it's uniform, but whatever)
    
	time_scaled = cont_ts - onsets(record);
	time_record(record,:) = time_scaled(beginsamp_c:endsamp_c); 

	timescaled = filt_ts - onsets(record);
	time_record_rf(record,:) = timescaled(beginsamp_rip:endsamp_rip);
    
	%% Window DIO stuffs
    
    dio_record(record,:) = dio_chan(beginsamp_c:endsamp_c)';
% 	offset_diff(record) = offsets(record) - onsets(record);
	
end; close(w)

%% Convert butterworthed ripple power into smoothed envelope

% % Standard smoothing of absolute value of butterworth filtered
% % channel using a MOVING AVERAGE
% % filt_record = abs(filt_record);
% for i = 1:size(filt_record,1)
% 	for j = 1:size(filt_record,2)
% 		filt_record(i,j,:) = smooth(filt_record(i,j,:),400);
% 	end
% end
% 
% % Standard smoothing of absolute value of butterworth-hilbert filtered
% % channel using a MOVING AVERAGE
% for i = 1:size(filt_record,1)
% 	for j = 1:size(filt_record,2)
% 		filt_env_record(i,j,:) = smooth(filt_env_record(i,j,:),5);
% 	end
% end


%% Acquire MultiUnit Binned Data
tic

nTets=numel(spikes_pfc); nTets=1;

offset=(2/1000)/10;

% Bin parameters
binsize		= (50/1000)/9.5 %seconds
binshare	= (10/1000)/9.5 %seconds
	
% Initialize bin edges, before we push starts and stops ahead by the
% binshare variable
time_range = [min(dio_ts) max(dio_ts)];
edges_before_binshare = time_range(1)+offset:binsize:time_range(2)+offset;

% Generate matrix of starts and stops with binshare regions
starts_stops = [edges_before_binshare(1:end-1) - binshare; ...
	edges_before_binshare(2:end) + binshare]';

starts_stops(1,1) = time_range(1);
starts_stops(end,end) = time_range(2);

bincount = size(starts_stops,1);

% For every tetrode count the spikes in our range
spike_hist = cell(size(spikes_pfc));
count = 0;
step_size = 2000;	% number of spikes we process at a time, this controls how much vectorization happens -- we cannot vectorize all spikes at once because it would take about 100Gb of ram
total = sum(cellfun(@(x) numel(x.fields(1).data/double(x.clockrate)),spikes_pfc))/step_size;
w = waitbar(0,'Binning spikes ...');
for tet = 1:nTets
	spike_hist{tet} = zeros(bincount,1);
	spike_times = double(spikes_pfc{tet}.fields(1).data)/double(spikes_pfc{tet}.clockrate);
	for s = 1:step_size:numel(spike_times)
		fprintf('\nChunk %d\n',count);
		
		% Simplify spike time name, prepare for vectorization, and pull spike
		% counts
		if s+step_size < numel(spike_times)
			spikes		= spike_times(s:s+step_size-1);
		else
			spikes		= spike_times(s:end);
		end
		spikecount	= numel(spikes);
		spikes		= repmat(spikes',[bincount 1]);

		% Prepare for vectorized counts, we're going to simulateonously check
		% all spikes on all bins
		start_mat	= repmat(starts_stops(:,1),[1 spikecount]);
		end_mat		= repmat(starts_stops(:,2),[1 spikecount]);

		% Operate!
		spike_logical_mat = ...
			(spikes >= start_mat) & (spikes < end_mat);

		% Now update the number per bin
		spike_hist{tet} = spike_hist{tet} + sum(spike_logical_mat,2);
		
		count = count + 1;
		waitbar(count/total, w, sprintf('Binning spikes on %dst/nd/th selected tet',tet));
		drawnow;
    end
    
    % store max spike count
    max_spikecount{tet} = max(spike_hist{tet});
    
    % clear unused variables
	clear spikes spikecount start_mat end_mat;
end; close(w);
toc

%% Plot eeg records zero'd on pulse trains

plotPulseByPulse=false;

if ~zerod_at_pulse
	time_record = time_record  + repmat(onsets',[1 size(time_record,2)]);
	time_record_rf = time_record_rf  + repmat(onsets',[1 size(time_record_rf,2)]);
	zerod_at_pulse = 2;
end

transparency=0.4;
set(0,'DefaultFigureWindowStyle','docked');
i = 1;
while (i > 0) && (i <= record) && plotPulseByPulse==true
	
    %% Subplot 1: Raw LFP
	
	figure(2);clf;
	
	subplot(2+nTets,1,1);
    for j = 1:size(cont_chan,2)
        eeg = plot( time_record(i,:), squeeze(eeg_record(j,i,:)), 'r:' ); hold on;
    end
	ax=gca;
    if isnan(min(time_record(i,:))) || isnan(max(time_record(i,:)))
        i=i+1;
        continue;
    end
	ax.XLim = [min(time_record(i,:)) max(time_record(i,:))];
    ax.YLim = [max(ax.YLim(1),-500) min(ax.YLim(2),500)];
	
	stimonoff_upper = area(time_record(i,:), ax.YLim(2) * dio_record(i,:));
	stimonoff_upper.FaceAlpha=transparency;stimonff_upper.FaceColor=[0 0 1];
	stimonoff_upper.EdgeAlpha=transparency; stimonoff_upper.LineWidth=0.0001;
	stimonoff_upper.LineStyle=':';
	
	stimonoff_lower = area(time_record(i,:), ax.YLim(1) * dio_record(i,:));
	stimonoff_lower.FaceAlpha=transparency;stimonoff_lower.FaceColor=[0 0 1];
	stimonoff_lower.EdgeAlpha=transparency; stimonoff_lower.LineWidth=0.0001;
	stimonoff_lower.LineStyle=':';
	
	% Labeling
	xlabel('Seconds'); ylabel('Raw EEG');
% 	legend(channel1, channel2, 'EEG Pulse Region','Location','SouthWest');
	title(sprintf('Pulse %d', i));
	
    %% Subplot 2: Ripple Power, from smooth/rectified butterworth
	
	subplot(2+nTets,1,2);
	
	for j = 1:size(cont_chan,2)
        filt1 = plot( time_record_rf(i,:), squeeze(filt_env_record(j,i,:)), 'b' ); hold on;
        filt2 = plot( time_record_rf(i,:), squeeze(filt_record(j,i,:)), 'r' );
        filt3 = plot( time_record_rf(i,:), squeeze(filt_phase_record(j,i,:)),'k');
    end
	
	ax=gca;
	ax.XLim = [min(time_record_rf(i,:)) max(time_record_rf(i,:))];
	storedLim = ax.XLim;
	
	stimonoff_upper = area(time_record(i,:), ax.YLim(2) * dio_record(i,:));
	stimonoff_upper.FaceAlpha=transparency;stimonff_upper.FaceColor=[0 0 1];
	stimonoff_upper.EdgeAlpha=transparency; stimonoff_upper.LineWidth=0.0001;
	stimonoff_upper.LineStyle=':';
	
	stimonoff_lower = area(time_record(i,:), ax.YLim(1) * dio_record(i,:));
	stimonoff_lower.FaceAlpha=transparency;stimonoff_lower.FaceColor=[0 0 1];
	stimonoff_lower.EdgeAlpha=transparency; stimonoff_lower.LineWidth=0.0001;
	stimonoff_lower.LineStyle=':';
	
	% Labeling
% 	channel1 = sprintf('RipChan %d', tetrode_ids(1));
% 	channel2 = sprintf('RipChan %d', tetrode_ids(2));
	xlabel('Seconds'); ylabel('Ripple Power, Smoothed/Rectified Butterworth');
 	legend('Envelope', 'Filtered Signal', 'EEG Pulse Region','Location','SouthWest');
	title(sprintf('Pulse %d', i));
	
	%% 3rd Subplot
	if plot_hilbert_envelope
% 		%% Subplot 3: Ripple Env, from hilbert of butterworth
% 		subplot(3,1,3);
%         
%         for j = 1:size(cont_chan,2)
%             rip1 = plot( time_record_rf(i,:), squeeze(filt_env_record(j,i,:)), 'b' ); hold on;
%         end
% 		ax=gca;
% 		ax.XLim = [min(time_record_rf(i,:)) max(time_record_rf(i,:))];
%         storedLim = ax.XLim;
% 
% 		stimonoff_upper = area(time_record(i,:), ax.YLim(2) * dio_record(i,:));
% 		stimonoff_upper.FaceColor=[0 1 0];
% 		stimonoff_upper.LineWidth=0.0001;
% 		stimonoff_upper.LineStyle=':';
% 
% 		stimonoff_lower = area(time_record(i,:), ax.YLim(1) * dio_record(i,:));
% 		stimonoff_lower.FaceColor=[0 1 0];
% 		stimonoff_lower.LineWidth=0.0001;
% 		stimonoff_lower.LineStyle=':';
% 
% 		% Labeling
% % 		channel1 = sprintf('RipChan %d', tetrode_ids(1));
% % 		channel2 = sprintf('RipChan %d', tetrode_ids(2));
% 		xlabel('Seconds'); ylabel(sprintf('Ripple Hilbert Envelope of Butterworth \n Daniel''s Method'));
% % 		legend(channel1, channel2, 'EEG Pulse Region','Location','SouthWest');
% 		title(sprintf('Pulse %d', i));
	else
		%% Subplot 3: Multiunit activity
        
        timeLim = [min(time_record_rf(i,:)) ...
                max(time_record_rf(i,:))];
        [~,start_ind]   = min(abs(timeLim(1) - edges_before_binshare));
        [~,stop_ind]    = min(abs(timeLim(2) - edges_before_binshare));

        edges= edges_before_binshare(start_ind:stop_ind); %edges= edges_before_binshare(start_ind:stop_ind) - onsets(i);
        centers = edges(1:end-1)+(edges(2:end)-edges(1:end-1))/2;
        
        for tet = 1:nTets
            
            subplot(2+nTets,1,2+tet); cla
            
            spikecounts = spike_hist{tet}(start_ind:stop_ind-1)';
            b=bar(centers,spikecounts); hold on;
            b.EdgeAlpha=0.1; b.BarWidth=1;
            if nTets>1
                b.FaceColor=[heaviside(2-tet)*(nTets-tet)/nTets heaviside(1-tet) heaviside(tet)*(tet)/nTets]; 
            else
                b.FaceColor=[0 0 0];
            end
            b.EdgeColor=[0 0 0];
            b.FaceAlpha=0.45;
            
            a=gca;
            a.XLim = storedLim;

            stimonoff_upper = area(time_record(i,:), a.YLim(2) * dio_record(i,:));
            stimonoff_upper.FaceAlpha=transparency;stimonff_upper.FaceColor=[0 0 1];
            stimonoff_upper.EdgeAlpha=transparency; stimonoff_upper.LineWidth=0.0001;
            stimonoff_upper.LineStyle=':';
            
        end
        
		
	end
	
	%% Manual scrolling through data
	% You can hit 'enter' to advance, 'b' to go backwards, or enter a
	% number to advance to plotting that pulse
% 	res = input(sprintf('Enter to continue or ''b'' then enter to backup: \n'), 's');
% 	[num, isanumber] = str2num(res);
% 	if isanumber
% 		i = num;
% 	else
% 		switch res
% 			case 'b'
% 				i = i - 1;
% 			otherwise
% 				i = i + 1;
% 		end
% 	end
	
	%% Save file
	filename = sprintf('PulseTrain%d', i);
	saveas(gcf, fullfile(where_to_save, filename),'png');
    saveas(gcf, fullfile(where_to_save,'fig',filename),'fig');
	
	if ~exist('res','var')
		i = i + 1;
    end
    clear res;
	
end

% %% Plotting complete trace
% % 
% if exist('reduce_plot.m','file')
%     plot=@reduce_plot;
% end
% 
% figure(4); clf;
% 
% % LFP
% ax(1)=subplot(2+nTets,1,1);
% 
% stim_upper = area(dio_ts,max(cont_chan(1:end))*dio_chan); hold on;
% stim_upper.FaceAlpha=0.1;stim_upper.FaceColor=[0 0 1];
% stim_upper.EdgeAlpha=0.01;
% stim_lower = area(dio_ts,-min(cont_chan(1:end))*dio_chan);
% stim_lower.FaceAlpha=0.1;stim_lower.FaceColor=[0 0 1];
% stim_lower.EdgeAlpha=0.01;
% 
% plot(cont_ts,cont_chan(:,1),'r--');
% 
% % FILTERED LFP
% ax(2)=subplot(2+nTets,1,2);
% 
% stim_upper = area(dio_ts,max(max(max(filt_pow)))*dio_chan); hold on;
% stim_upper.FaceAlpha=0.1;stim_upper.FaceColor=[0 0 1];
% stim_upper.EdgeAlpha=0.01;
% stim_lower = area(dio_ts,max(max(max(filt_pow)))*dio_chan);
% stim_lower.FaceAlpha=0.1;stim_lower.FaceColor=[0 0 1];
% stim_lower.EdgeAlpha=0.01;
% 
% storedLim=get(gca,'XLim');
% 
% plot(filt_ts,filt_pow(:,1),'r--');
% 
% % MULTIUNIT
% 
% timeLim = [min(time_record_rf(1,:)) ...
% max(time_record_rf(1,:))];
% % [~,start_ind]   = min(abs(timeLim(1) - edges_before_binshare));
% % [~,stop_ind]    = min(abs(timeLim(2) - edges_before_binshare));
% 
% edges= edges_before_binshare;
% centers = edges(1:end-1)+(edges(2:end)-edges(1:end-1))/2;
% 
% for tet = 1:nTets
% 
%     ax(2+tet)=subplot(2+nTets,1,2+tet); cla
% 
%     spikecounts = spike_hist{tet}';
%     b=bar(centers,spikecounts); hold on;
%     b.EdgeAlpha=0.1; b.BarWidth=1;
%     b.FaceColor=[heaviside(2-tet)*(nTets-tet)/nTets heaviside(1-tet) heaviside(tet)*(tet)/nTets]; 
%     b.EdgeColor=[0 0 0];
%     b.FaceAlpha=0.45;
% 
%     a=gca;
%     a.XLim = storedLim;
% 
%     stimonoff_upper = area(time_record(i,:), a.YLim(2) * dio_record(i,:));
%     stimonoff_upper.FaceAlpha=transparency;stimonff_upper.FaceColor=[0 0 1];
%     stimonoff_upper.EdgeAlpha=transparency; stimonoff_upper.LineWidth=0.0001;
%     stimonoff_upper.LineStyle=':';
% 
% end
% 
% linkaxes(ax,'x');

%% Average together all of the dio centric records
% This section is to help extract information about multiunit activity when
% its more difficult to see, because maybe a minority of cells on the
% channel respond. Also just nice to see confirmation of strong effects.

% LFP
mu_filt_phase_record = squeeze(mean(filt_phase_record,2,'omitnan'));
mu_filt_env_record = squeeze(mean(filt_env_record,2,'omitnan'));
mu_filt_record = squeeze(mean(filt_record,2,'omitnan'));
mu_filt_time = linspace(-secondsbefore,secondsafter,numel(mu_filt_record));
mu_eeg_record = squeeze(mean(eeg_record,2,'omitnan'));

% DIO
mu_dio_record=squeeze(mean(dio_record,1,'omitnan'));
mu_eegdio_time = linspace(-secondsbefore,secondsafter,numel(mu_eeg_record));
onlytriggerdio = mu_dio_record/max(mu_dio_record) > 0.5;

% Multiunit
avgMultiunit={};
for tet = 1:nTets
    
    % initialize
    timeLim = [min(time_record_rf(1,:)) ...
                max(time_record_rf(1,:))];
    [~,start_ind]   = min(abs(timeLim(1) - edges_before_binshare));
    [~,stop_ind]    = min(abs(timeLim(2) - edges_before_binshare));
    avgMultiunit{tet}=spike_hist{tet}(start_ind:stop_ind);
    
    % count
    for i = 2:record
        timeLim = [min(time_record_rf(i,:)) ...
                    max(time_record_rf(i,:))];
        [~,start_ind]   = min(abs(timeLim(1) - edges_before_binshare));
        [~,stop_ind]    = min(abs(timeLim(2) - edges_before_binshare));
        if start_ind==stop_ind
            warning('Start-ind = stop-ind');
            continue;
        end
        
        if stop_ind-start_ind+1~=size(avgMultiunit{tet},1)
            endPortion=stop_ind-start_ind+1-size(avgMultiunit{tet},1);
            if endPortion>0
                stop_ind=stop_ind-endPortion;
            else
                if abs(endPortion)>5
                    continue;
                end
                avgMultiunit{tet}(end+endPortion:end)=[];
            end
            warning('Sizes not equal');
        end
        
        avgMultiunit{tet} = avgMultiunit{tet} + ...
            spike_hist{tet}(start_ind:stop_ind);
    end
    
    % divide by record to get average
    avgMultiunit{tet}=avgMultiunit{tet}/record;
    
    % if normalize flag, then normalize by the maximum count
    normalizeMax=true;
    if normalizeMax
        avgMultiunit{tet}=avgMultiunit{tet}/max_spikecount{tet};
    end
    
end
mu_multiunit_time = linspace(-secondsbefore,secondsafter,numel(avgMultiunit{1}));

%% Plot averaged data

figure;
subplot(2+nTets,1,1);
plot(mu_eegdio_time',mu_eeg_record);hold on;
a=gca;
stimonoff_upper = area(mu_eegdio_time, a.YLim(2) * onlytriggerdio);
stimonoff_upper.FaceAlpha=transparency;stimonff_upper.FaceColor=[0 0 1];
stimonoff_upper.EdgeAlpha=transparency; stimonoff_upper.LineWidth=0.0001;
stimonoff_upper.LineStyle=':';
stimonoff_lower = area(mu_eegdio_time, a.YLim(1) * onlytriggerdio);
stimonoff_lower.FaceAlpha=transparency;stimonff_upper.FaceColor=[0 0 1];
stimonoff_lower.EdgeAlpha=transparency; stimonoff_upper.LineWidth=0.0001;
stimonoff_lower.LineStyle=':';
title('Pulse-triggered average for Hippocampal LFP');
set(gca,'YLim',[-400 400]);
set(gca,'XLim',[-secondsbefore secondsafter]);
subplot(2+nTets,1,2);
plot(mu_filt_time',mu_filt_record);hold on;
a=gca;
stimonoff_upper = area(mu_eegdio_time, a.YLim(2) * onlytriggerdio);
stimonoff_upper.FaceAlpha=transparency;stimonff_upper.FaceColor=[0 0 1];
stimonoff_upper.EdgeAlpha=transparency; stimonoff_upper.LineWidth=0.0001;
stimonoff_upper.LineStyle=':';
stimonoff_lower = area(mu_eegdio_time, a.YLim(1) * onlytriggerdio);
stimonoff_lower.FaceAlpha=transparency;stimonff_upper.FaceColor=[0 0 1];
stimonoff_lower.EdgeAlpha=transparency; stimonoff_upper.LineWidth=0.0001;
stimonoff_lower.LineStyle=':';
title('Pulse-triggered average for \theta-Filtered Hippocampal LFP');
set(gca,'XLim',[-secondsbefore secondsafter]);
for tet = 1:nTets
    subplot(2+nTets,1,2+tet);
    b=bar(mu_multiunit_time,avgMultiunit{tet});
    hold on;
    b.EdgeAlpha=0.1; b.BarWidth=1;
    b.FaceColor=[heaviside(2-tet)*(nTets-tet)/nTets heaviside(1-tet) heaviside(tet)*(tet)/nTets]; 
    b.EdgeColor=[0 0 0];
    b.FaceAlpha=0.45;
    set(gca,'XLim',[-secondsbefore secondsafter]);
    
    title(sprintf('Pulse Triggered-Average for %dth PFC Tetrode Multiunit',tet));
    
    a=gca;
    stimonoff_upper = area(mu_eegdio_time, a.YLim(2) * onlytriggerdio);
    stimonoff_upper.FaceAlpha=transparency;stimonff_upper.FaceColor=[0 0 1];
    stimonoff_upper.EdgeAlpha=transparency; stimonoff_upper.LineWidth=0.0001;
    stimonoff_upper.LineStyle=':';
end

xlabel('Seconds from Laser Pulse');

f=gcf;
saveas(f,fullfile(where_to_save,'PULSE-TRIGGERED_AVERAGE.png'));
saveas(f,fullfile(where_to_save,'fig','PULSE-TRIGGERED_AVERAGE.fig'));

end