
%% Get all gamma modulations for all cells with particular conditions.

animals = {'HPa'};

runepochfilter = ['isequal($environment, ''wtr1'') ||' ...
	'isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')']; 

cellfilter = 'strcmp($area, ''PFC'') && strcmp($FStag, ''n'')';

timefilter_theta = {{'DFTFsj_getvelpos', '(($absvel >= 5))'...
%         {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',2} ...
		}};   
	
pfctetfilter = '( isequal($area, ''PFC''))';
eegfilter = {'sj_geteegtet', 'gamma', 'tetfilter',pfctetfilter,'maxvar',true};

iterator = 'singlecelleeganal';

% Filter creation
% ----------------
modf = createfilter('animal',animals,'epochs',runepochfilter, ...
	'excludetimefilter', timefilter_theta, 'cells',...
	cellfilter,'eegtetrodes', eegfilter, 'iterator', iterator);

modf = setfilterfunction(modf,...
        'plotgammamod',{'spikes','gamma'}); % Corresponding function is sj_ploteegmod
	
modf = runfilter(modf);
disp('Finished running filter script');