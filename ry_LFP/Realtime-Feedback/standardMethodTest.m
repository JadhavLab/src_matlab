function standardMethodTest(eeg,fs)

    buffersize = 1024;
    buffer=zeros(1,1024);
    bufferloc = buffersize;

    %% Iterate packet by packet
    for i = 1:numel(eeg)
        
        buffer(bufferloc) = eeg(i)
        
    end

end