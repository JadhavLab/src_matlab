% Name:         ThetaDisruption_ChenLeonMethod    
% Purpose:      Testing out the algorithms employed by Chen and Leon on our
%               EEG data, with an eye to accurately interrupting theta at
%               specific phases, at least, hopefully, better than Wilson's
%               interruption. If we can be more resolute than the peak and
%               trough, this may be worth it.

f_L = 6;
f_H = 12;

%% Shortcut functions and where to save

psm = @(x) fprintf([x '\n']);

%% User-based Save Directory and Options

[~,username] = unix('whoami');
switch deblank(username)
    case 'ss'
        where_to_save = '/Users/ss/Documents/MATLAB/Lab/Data/Local/Current/';
		set(0,'DefaultFigureWindowStyle','docked');
    case {'jadhavlab','ryoung'}
        where_to_save = '~/Data/Local/Current/';
		set(0,'DefaultFigureWindowStyle','docked');
    otherwise
        where_to_save= input('Name your figure folder:','s');
end

%% Program Flags
remove_reference_channel = true;
plot_hilbert_envelope = false;
zerod_at_pulse = false;

%% Run Automated Extraction
% This segment rsyncs the relevent requested rec file in, then applies
% TrodesToMatlab on channels inputted. Made this a stand
% alone script because it can easily be used by itself, outside this
% analysis.

animal = 'DM4';
animdat = animaldef(animal);
tetinfo = loaddatastruct(animdat{2},animdat{3},'tetinfo');

day = 3; epoch = 2;

tetinfo=tetinfo{day}{epoch};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFIY TETRODES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
area = {};
area{1} = 'PFC';
area{2} = 'dHPC_CA1';

tetareas = cellfetch(tetinfo,'area');
tetrode_list={};
for i = 1:numel(area)
    tetrode_list{i} = find(cellfun(@(x) isequal(area{i},x),tetareas.values));
end
ref_list{1} = 13;
ref_list{2} = ref_list{1};

% Load up tetrode data
eeg = loadlfpstruct(animdat{2},animdat{3},'lfp',3,2,1:30);
samprates = cellfetch(eeg,'samprate');

%% Store eeg variables for the particular area

clear area;
for i = 1:numel(tetrode_list)
    area(i).channelData = getchannlmatrix(eeg,[day epoch], tetrode_list{i})';
    area(i).samplingRate = samprates.values{tetrode_list{i}(1)};
    reference(i).channelData = getchannlmatrix(eeg,[day epoch],ref_list{i})';
    reference(i).samplingRate = area(i).samplingRate;
end




%% Remove Reference Channels
% If user requests it!, then we remove the specified reference in the
% extraction phase

if remove_reference_channel
	area(1).channelData = area(1).channelData - repmat(reference(1).channelData, ...
		[1,size(area(1).channelData,2)]);
end

%% Make CSV file
% this is done to allow test of the input in our C++ implementations

eeg2csv(area(1).channelData(:,1));

%% Calculate downsampled ripple filtered power
% Same trick Daniel uses

% EEG filter
% [tfilt_b, tfilt_a] = butter(4, [2,area(1).samplingRate/2-1e-7]/(area(1).samplingRate/2));
% area(1).filterData = filter(tfilt_b, tfilt_a, area(1).filterData);

% Theta filter
[tfilt_b, tfilt_a] = butter(4, [f_L,f_H]/(area(1).samplingRate/2));
area(1).fData = filter(tfilt_b, tfilt_a, area(1).channelData);
clear rfilt_b rfilt_a;

%%  Create Shortcut Variables for Commonly Used Objects
% Otherwise, referencing giant structs becomes pretty tedious

% Two key sampling rates, the standard 30khz and downsampled
csr_c = area(1).samplingRate;
% csr_rf = downSampledData.samplingRate;

% % Dio channel and timestamps
% dio_chan = dio.channelData(19).data;
% dio_ts = dio.timestamps;

% Continuous channel and continuos timestamps
cont_chan = area(1).channelData;
% cont_ts = area.timestamps;

% Theta power channel data and corresponding timestamps
theta_pow = area(1).fData;
% theta_ts = downSampledData.timestamps;

% Hilbert based envelope
theta_env = hilbert(theta_pow);
theta_env = abs(theta_env);

% Hilbert based phase!
theta_phase = angle(hilbert(theta_pow))';

%% Search parameter space for optimal detection set

tic;
if exist('ga','file')
	psm('...Initiating genetic algorithm ... ');
	[paramset, indexStimTimes, stimPhases, phaseLockingMetrics] = ...
		findOptParams(cont_chan(:,1),csr_c);
else
	disp('... do not have the right toolbox!');
	disp('Hard-coding parameters from paper');
	
	response=input( ...
		'Which parameter set? \n 1 - Seizure \n 2 - Subject 1 \n');
	
	switch response
		case 1
			paramset = [13 0.50 1 1 0.10];
		case 2
			paramset = [22 0.79 2 2 0.05];
	end
	
end
ARorder = paramset(1);
lambda = paramset(2);
filterOrder = paramset(3);
filterType = paramset(4);
t_stop = paramset(5);

% 

psm('Done!'); toc;

%% findBestChannel

signal_cell = arrayfun(@(x) cont_chan(:,x) ,1:size(cont_chan,2),'UniformOutput',false);
[bestChan_bandPower, bestChan_tempCoher, bestChan_combined] = ...
	findBestChannel(signal_cell,csr_c,f_L,f_H,2);

%% Cut up the seglment length that we are passing into simRT

% Calculate timestamp zero'd at the beginning of acquisition
norm_cont_ts = cont_ts - min(cont_ts);

% Segment window lengths to draw, in seconds
segmentlength = 1;

% How often to calculate windows, in seconds
period_calc		= 1/20; 

% Start and stop times of each sampling window
start_times		= 0:period_calc:(max(norm_cont_ts)-segmentlength);
stop_times		= start_times + segmentlength;

% Grab the indices for those corresponding start and stop times
start_ind = lookup(start_times,norm_cont_ts);
stop_ind = lookup(stop_times,norm_cont_ts);

%% FETCH THE STIMULATION TIMES, per window

targetphase = 0;

stimDelivered=zeros(size(t_start));t_delay=zeros(size(t_start));
t_start = 0.1;
clear w; w=waitbar(0,'Calculating stimulation times...');
for i = 1:numel(start_ind)
	
	[stimDelivered(i), t_delay(i)] = ...
		stimRT(signal_cell{bestChan_combined}(start_ind(i):stop_ind(i)),...
		csr_c,f_L,f_H,0.1,ARorder, lambda, ...
		filterOrder, filterType, 'yw', t_stop, targetphase);
	
	waitbar(i/numel(start_ind),w);
	
end

%% Get times back in reference with timestamp norm
