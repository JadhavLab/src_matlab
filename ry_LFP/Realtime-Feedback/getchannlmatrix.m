function chanmat = getchannlmatrix(eegstruct,index, tetindex)

d = index(1);
e = index(2);

eegstruct = eegstruct{d}{e};
out = cellfetch(eegstruct,'data');

chanmat=[];
for i = 1:numel(out.values)
    
    if nargin >=3 && ~isempty(tetindex)
        if ismember(i,tetindex)
            chanmat(end+1,:) = [out.values{i}];
        end
    else
       chanmat = [chanmat out.values{i}];
    end
    
end

end