function eeg2csv(eeg,filename)

    if nargin < 2; filename = 'in.csv'; end;
    waitbar(0,'Percentage complete...');
    fout = fopen(filename,'w');
    fwrite(fout,sprintf('%d,',numel(eeg)));
    for i = 1:numel(eeg)-1
        fwrite(fout,sprintf('%8.4f,',eeg(i)));
        waitbar(i/(numel(eeg)-1));
    end;
    fwrite(fout, sprintf( '%8.4f' , eeg(i+1) ) );
    fclose(fout);
end