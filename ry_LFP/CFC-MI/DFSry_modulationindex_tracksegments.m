close all;

%notes = input('Enter short note about this analysis:','s');
% notes = 'velgreaterthan5cm';

%% Apply ParameterExplorer params, if they exist or set variabl params
% These can overwrite any parameters in the variable initialization phases
% above, right before we create the filter. This is what gives
% ParameterExplorer the power to iterate over this script with different
% compinations.

savedir = '~/Data/Local/Current/ModulationIndex/TrackSegment/';
if exist('params','var') && isstruct(params)
	ParameterExplorer.swapParamSet(params);
    savedir = ParameterExplorer.savelocation(params,'projectfolder',projectdir);
    fprintf('\n\n===========================\n');
    fprintf('Starting to process in %s \n',savedir);
else
    areas_to_examine	= 3;
    velocity			= 2; % cm / s
    ripplethres			= 2; % threshold: standard dev above the mean
    dayfilter='4:5';
end
warning off; mkdir(savedir); warning on;

%% FLAGS

% Analysis flags
checkbehavior       		= false; % Whether or not to ensure behavior pulled each segment looks correct .. this intitiates a function that provides visuals of the maze and animal trajectory for all requested times.
checkMI				= true;  % whether or not to do MI on segments of behavior

% Plot flags
plotsessionaverages = true;
plotsummaryaverages  = true;


%% Filter Section -- Which data would you like?

% -----------------------------------------------
%%% ANIMAL SELECTION -- Which animals to include!
% -----------------------------------------------
animalfilter = {	...
					'HPa' ... Shantanu's HP animals
 					'HPb' ...
  					'HPc' ...
%					'ER1' ... Justin's 1 Day
% 					'JS7' ... 
	};

% -----------------------------------------------------------------
%%% EPOCH SELECTION  -- Which epochs to include, for a given animal
% -----------------------------------------------------------------
epochfilter = [...
	'isequal($type, ''run'') && '...
	'(isequal($environment, ''wtr1'') ||' ... 
	'isequal($environment, ''wtr2''))'
	];

dayfilter=num2str(dayfilter);

% -------------------------------------
%%% AREA AND CELL TYPE
% -------------------------------------

% Select the proper filter for the area that the user requested 
switch areas_to_examine
	case 1 % Hippocampal CA1 - All
        type = 'CA1-CA1';
		tetrode_filter = { ...
			'isequal($area, ''CA1'') && ($numcells > 1)' ...
			'isequal($area, ''CA1'') && ($numcells > 1)' ...
			};
	case 2 % Prefrontal Cortex - All
        type = 'PFC-PFC';
		tetrode_filter = { ...
			'isequal($area, ''PFC'') && ($numcells > 1)'...
			'isequal($area, ''PFC'') && ($numcells > 1)' };
	case 3 % CA1-Prefrontal Interaction
        type = 'CA1-PFC';
		tetrode_filter = { ...
			'isequal($area, ''CA1'') && ($numcells > 1)' ...
			'isequal($area, ''PFC'') && ($numcells > 1)'};
end

% -------------------------------------
%%% CONDITION OR BEHAVIOR SELECTION
% -------------------------------------

% Filter for the ripple tetrode
riptetfilter = '(isequal($descrip, ''riptet''))';

% Filter out times with velocities of interest
timefilter = cell(1);
timefilter{1}	= {'DFTFsj_getvelpos', ['(($absvel >= ' num2str(velocity) '))']}; % Filter velocities of interest
if ripplethres > 0  	% Option to remove ripple times
	timefilter{end+1}	= ...
    {'DFTFsj_getriptimes','($nripples == 0)',...
    'tetfilter',riptetfilter,'minthresh',ripplethres};
end

% -------------------------------------
%%% ITERATORs
% -------------------------------------
iterator_behavior			=	'epochbehaveanal';
iterator_coherence			=	'eeganal';


%% Creater Filters
	
f_base = createfilter('animal',animalfilter,'days',dayfilter,'epochs',epochfilter,...
	'excludetime',timefilter,'eegtetrodes',tetrode_filter{1},...
	'eegtetrodepairs',tetrode_filter);

% Since we will run 2+ filter functions, initialize a cell to keep track of
% outputs to save
nZone = 10;
zone_set = linspace(0,1,nZone + 1);
zone_set = [zone_set(1:end-1);zone_set(2:end)];

pushd(savedir);

for xbound = ['0', '1'] % outbound (0) or inbound (1)
for reward = ['0', '1'] % correct (1) or incorrect (0)
	for zone = zone_set % Iterates over each zone

		folder = sprintf( 't=%s r=%s %1.2f-%1.2f', xbound, reward, zone(:) );
		mkdir(folder);pushd(folder);
		
		fprintf('Computing zone, t=%s r=%s linpos=[%f %f]', xbound, reward, zone(:))
		
		filtertimes = ['$inZone == true & $trajbound == ' xbound ' & $rewarded == ' reward]
		timefilter{3} = {'DFS_linposZoneTrigger',filtertimes,'lindist', zone}; % This adds a third and last time filter of the track zone to velocity and ripple filters. Per epoch, linposZoneTrigger can select any linear zone or trigger on variety of conditions or wells.
		
		% tracks (based on which analyses asked for), which variables to save to disk
		savevar = {};

		if checkbehavior	== true
			disp('Setting up behavior filter...');tic;
			f_behavior	= modifyfilter(f_base,	'iterator','epochbehaveanal');
			f_behavior = setfilterfunction(f_behavior,'plotBehavior2',{'pos','linpos'},...
			'arrow',true);
			disp('...done!');toc;
			disp('Plotting behavior...');tic;
			runfilter(f_behavior);
			disp('...done!');toc
		    
		    savevar{end+1} = 'f_behavior';
		end

		if checkMI		== true
			bincount = 18;
			f_MI = modifyfilter(f_base,'iterator','epocheeganal');
			f_MI = setfilterfunction(f_MI,'ryDFA_calccomodulogram2',{'eeg'},...
			... CALCULATION OPTIONS
			'numbins', bincount, ...
			... PARALLELIZATION OPTION
			'batchmode', false, ...
			... OTHER OPTIONS
			'plotoption',true, 'struct_outputlevel', 1, 'save_detailedoutput', true);
			f_MI = runfilter(f_MI);
			disp(['Running ' pwd ' MI calculations ...']);tic;
			disp(' ... complete!');toc;
		    
		    savevar{end+1} = 'f_MI';
		end

		% Save filters
		filtersavefile = fullfile(pwd, 'modf');
		if ~exist(filtersavefile,'file')
		    save(filtersavefile, savevar{:},'-v7.3');
		else
		    save(filtersavefile, savevar{:},'-v7.3','-append');
		end
		fprintf('Save location --> %s\n', savedir);

		popd;

		message_finish(sprintf('Finished zone %f-%f',zone(:)),'modulation_index');
	end
end
end

clear;

%% Gather Data -- Gather across sessions
% TODO Fill in this

%% Plot -- Plot across session data
% TODO Fill in this

