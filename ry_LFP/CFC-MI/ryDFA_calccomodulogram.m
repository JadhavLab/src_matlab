function out = calceegxfreqcoupling(index, excludetimes, eeg, varargin)
% function out = calceegxfreqcoupling(index, excludetimes, eeg, varargin)
%
% NOTE : RUNS FAST THAN calceegxfreqcoupling ... but soaks up more RAM
%
%  Plots the MI for an eeg tetrode pair. If you use a time filter,
%  excluded times are removed and the includedtimes are averaged together.
%
%   out is a structure with the following fields
%       coherence-- This is the coherence for the tetrode pairs
%       frequency-- Frequency vector
%       index-- Only if appendindex is set to 1 (default)
% Ryan modified existing script to be able to grab cross-frequency-coupling
% and MI

% TODO -- Incorportate exclude times! -- need to be able to get MI index
% for certain locations on the track instead of just entire epoch

fprintf('Cross Frequency Coupling & MI, day %d , ep %d, tetX %d, tetY %d ...\n',...
    index(1),index(2),index(3),index(4)); tic

%% Internal Flags
debug = true;

%% Parse inputs and set defaults

% Default options
savedir = '~/Data/Local/Current/ModulationIndex/Prefrontal-Prefrontal/';
%%%%%%%%%%%%%%%%%%%%%%%%%%
srate           = 1500;
%%%%%%%%%%%%%%%%%%%%%%%%%%
phasepass			= [4 14];
amppass				= [20 200];
phasebinstepsize    = range(phasepass)/5;
ampbinstepsize      = range(amppass)/5;
%%%%%%%%%%%%%%%%%%%%%%%%%%
lag             = 100;
appendindex     = 0;
numsurrogate    = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default flags
batchmode = true;
plotoption = true;
extraoutput = true;
%%%%%%%%%%%%%%%%%%%%%%%%%%

for option = 1:2:length(varargin)-1   %% Compute Cross-frequency Coupling
    if isstr(varargin{option})       
        switch(varargin{option})
			% OUTPUT OPTIONS
            case 'extraoutput'
                % Turns off meaningful output into filterframework's
                % objects' RAM space. Useful for printing simply printing
                % the MI figure per each of these function calls and not
                % saving the detailed output which clogs RAM.
                extraoutput = varargin{option+1};
            case 'appendindex'
                appendindex = varargin{option+1};
			% BINNING AND STEP SIZE OPTIONS
            case 'overallphasepass'
                phasepass = varargin{option+1};
            case 'overallamppass'
                amppass = varargin{options+1};
			case 'phasepasspercalc'
				phasePassPerCalc = varargin{option+1};
			case 'amppasspercalc'
				ampPasspercalc = varargin{option+1};
            case 'numbins' % per cross-frequency coupling
                numbins = varargin{option+1};
			% LAG OPTION
            case 'lag'
                lag = varargin{option+1};
			% SURROGATE OPTION - for validation
            case 'numsurrogate'
                numsurrogate = varargin{option+1};
			% MODE OPTIONS
            case 'plotoption'
                plotoption = varargin{option+1};
            case 'savedir'
                savedir = varargin{option+1};
            case 'batchmode'
                % This controls whether or not each cross-frequency
                % coupling calculation is done as a batch, multicore across
                % all available processors or cluster components ...
                % definitely the fastest, but not supported on machines
                % without parallell processing toolbox
                batchmode = varargin{option+1}; 
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end   
    else
        error('Options must be strings, followed by the variable');
    end
end

%% Get eeg trace times in agreement
% assign a temporary variable for eeg
e1 = eeg{index(1)}{index(2)}{index(3)};
e2 = eeg{index(1)}{index(2)}{index(4)};

e1times = geteegtimes(e1);
e2times = geteegtimes(e2);

if length(e1times)		> length(e2times)
    temp = lookup(e2times,e1times);
    e1 = e1.data(temp);
    e2 = e2.data;
    e1times = e1times(temp);
elseif length(e2times)	> length(e1times)
    temp = lookup(e1times,e2times);
    e1 = e1.data;
    e2 = e2.data(temp);
elseif length(e1times)	== length(e2times)
    e1 = e1.data;
    e2 = e2.data;
end

%% Filter in all relevant bands
% [phasefilts,ampfilts,phase_edges,amp_edges] = ...
%     filter_all(e1,e2,srate,phasepass,phasestepsize,amppass,ampstepsize, varargin)


%% Calculation
if batchmode
	[detailedout, MI_out] = batchCalculate(e1,e2,excludetimes,e1times,phasepass,phasebinstepsize,amppass,ampbinstepsize,...
		srate,numbins,'extraoutput',extraoutput);
else
	[detailedout, MI_out] = regularCalculate(e1,e2,phasepass,phasebinstepsize,amppass,ampbinstepsize,...
		srate,numbins);
end

%% Output and Plotting
% Plot if requested
if plotoption
    
    % Obtain center frequencies per bin, to constitute x and y axes
    p_edges = phasepass(1):phasebinstepsize:phasepass(2);
    a_edges	= amppass(1):ampbinstepsize:amppass(2);
    p_centers = p_edges(1:end-1) + ...
        (1/2)*(p_edges(2:end)-p_edges(1:end-1));
    a_centers = a_edges(1:end-1) + ...
        (1/2)*(a_edges(2:end)-a_edges(1:end-1));

    % Plot MI index using computed x and y axes
    figure(350); clf;
    imagesc(p_centers,a_centers,MI_out);
    tstring = sprintf('MI, day %d, ep %d\n Phase of %d in %d - %d. Amp of %d in %d - %d',...
        index(1),index(2),index(3),phasepass(1),phasepass(2),index(4),amppass(1),amppass(2));
    set(gca,'YDir','normal');
    title(tstring);
    xlabel('Phase');
    ylabel('Amplitude');
    
    sstring = sprintf('MI, day %d, ep %d, Phase of %d in %d - %d. Amp of %d in %d - %d',...
        index(1),index(2),index(3),phasepass(1),phasepass(2),index(4),amppass(1),amppass(2));
    currdir=pwd;
    mkdir(savedir); cd(savedir);
    print([sstring '.png'], '-dpng');
    cd(currdir);
    
end

% Assign outputs
if extraoutput
    out.detailedout = detailedout;
    out.MI = MI_out;
else
    out.MI = MI_out;
end

fprintf(' \n... Done! \n');toc;
return

%% -------------------------------------------------------------------------
% HELPER FUNCTIONS

%% batchCaclulate
%  Purpose: Handles the batch calculation of cross-frequency coupling and
%  associated modulation index in several batches to take advantage of
%  either multiple cores on the computer or take advantage of a cluster
%  (your endogenous matlab settings determine if it will run on your cores
%  or a cluster you've predefined.)
% 
% TODO: Move the filtering step inside this scope .. that way waveforms
% won't have to be filtered multiple times in each band and also take up
% more RAM with parallel processing
% -------------------------------------------------------------------------
	function [results, MI] = ...
            batchCalculate(e1,e2,excludedtimes,times, ...
            phasepass,phasestepsize,amppass,ampstepsize,...
			srate,numbins,varargin)
        
        %% Optional arg handling
         added_output = true;
        for o = 1:2:numel(varargin)
            switch varargin{o}
                case 'extraoutput'
                    added_output = varargin{o};
                case 'phasepass_percalc'
                    phasepass_percalc = varargin{o};
                case 'amppass_percalc'
                    amppass_percalc = varargin{o};
                otherwise
                    error('Unrecognized input!');
            end
        end
        
       %% Initialize edges
		phase_edges = phasepass(1):phasestepsize:phasepass(2);
		amp_edges = amppass(1):ampstepsize:amppass(2);
		
		% If there is a phase pass per calculation P and amp pass per
		% calculation A, then for every step size, we bandpass +/- P/2 and
		% +/- A/2 
		if exist('amppass_percalc','var') && exist('phasepass_percalc','var')
			phase_centers = phase_edges(1:end-1) + ...
				(1/2)*(phase_edges(2:end)-phase_edges(1:end-1));
			amp_centers = amp_edges(1:end-1) + ...
				(1/2)*(amp_edges(2:end)-amp_edges(1:end-1));
			
			phase_edges = [phase_edges(1:end-1)'-phasepass_percalc/2 ...
				phase_edges(2:end)'+phasepass_percalc/2];
			amp_edges	= [amp_edges(1:end-1)'-amppass_percalc/2 ...
				amp_edges(2:end)'+amppass_percalc/2];
		% Else, the step size tells us the edges
		else
			phase_edges = [phase_edges(1:end-1)' phase_edges(2:end)'];
			amp_edges	= [amp_edges(1:end-1)' amp_edges(2:end)'];
		end

		%% Batch process
		% Farm out each combination of phase and amplitude slice to a batch
		% process
        job_count = 0;
        worker_pool = gcp;
        fprintf('\nJobs queued: ');
        for p = 1:size(phase_edges,1)
            for a = 1:size(amp_edges,1)

                % Select phases to analyze in this iteration
                fpassx = [phase_edges(p,1)  phase_edges(p,2)];
                fpassy = [amp_edges(a,1) amp_edges(a,2)];

                % Keep tabs on how many jobs we have out
                job_count = job_count + 1;
				% Initiate the batch job
                job(job_count) = parfeval(@calcxfreqcoupling,3,...
                    e1,e2, excludedtimes,times,fpassx, fpassy, srate, numbins);
                fprintf('%d ', job_count);
			end
        end

        %% Wait for all batches to finish
		% and collect outputs when finished

		% Here, I loop over the jobs and test if each is finished; if it
		% is, store the results
        all_finished = false;
        while ~all_finished,
            all_finished = true; % assume we finished
            for ii = 1:job_count,
                
				jobFinished = isequal(job(ii).State,'finished');
                jobError    = ~isempty(job(ii).Error);
                
                % If job throws an error, att
                if jobError
                    % Re-initiate the batch job
                    job(job_count) = parfeval(@calcxfreqcoupling,3,...
                        e1,e2,times,excludedtimes,fpassx, fpassy, srate, numbins,'extraoutput',added_output);
                    fprintf('(errorRestart %d) ', ii);   
                end
                
                all_finished = all_finished && jobFinished && ~jobError; % if not finished, flip it to false
			end
		end 
		
		%% Gather
		[batchout, fX, fY] = fetchOutputs(job);
        delete(job);
		
		%% Re-index
        unFreqX = sort(unique(fX)); unFreqY = sort(unique(fY));
        nUFX = numel(unFreqX); nUFY = numel(unFreqY);
        MI = zeros(nUFX,nUFY);
        results = batchout(1); %initialize
		for x = 1:nUFX
			for y = 1:nUFY
				
				indy = fY == unFreqY(y);
				indx = fX == unFreqX(x);
				ind = indx & indy;
				
				results(x,y) = batchout(ind);
                MI(x,y) = results(x,y).MI;
			end
        end
		
	end

%% regularCaclulate
%  Purpose: Handles all calculation of cross-frequency coupling and
%  associated modulation index in by sequentially iterating. Takes longer
%  than the batch version, but matlab users will be more comfortable
%  modifying this or thinking about what's going on than with the batch
%  version.
% -------------------------------------------------------------------------
    function [out, MI] = ...
            regularCalculate(e1,e2,...
			phasepass,phasestepsize,amppass,ampstepsize,...
			srate,numbins,varargin)

        %% Optional arg handling
         couplingoutput = true;
        for o = 1:2:numel(varargin)
            switch varargin{o}
                case 'extraoutput'
                    couplingoutput = varargin{o};
                case 'phasepass_percalc'
                    phasepass_percalc = varargin{o};
                case 'amppass_percalc'
                    amppass_percalc = varargin{o};
                otherwise
                    error('Unrecognized input!');
            end
        end
        
       %% Initialize edges
		phase_edges = phasepass(1):phasestepsize:phasepass(2);
		amp_edges = amppass(1):ampstepsize:amppass(2);
		
		% If there is a phase pass per calculation P and amp pass per
		% calculation A, then for every step size, we bandpass +/- P/2 and
		% +/- A/2 
		if exist('amppass_percalc','var') && exist('phasepass_percalc','var')
			phase_centers = phase_edges(1:end-1) + ...
				(1/2)*(phase_edges(2:end)-phase_edges(1:end-1));
			amp_centers = amp_edges(1:end-1) + ...
				(1/2)*(amp_edges(2:end)-amp_edges(1:end-1));
			
			phase_edges = [phase_edges(1:end-1)'-phasepass_percalc/2 ...
				phase_edges(2:end)'+phasepass_percalc/2];
			amp_edges	= [amp_edges(1:end-1)'-amppass_percalc/2 ...
				amp_edges(2:end)'+amppass_percalc/2];
		% Else, the step size tells us the edges
		else
			phase_edges = [phase_edges(1:end-1)' phase_edges(2:end)'];
			amp_edges	= [amp_edges(1:end-1)' amp_edges(2:end)'];
		end
        
		%% Calculate each bout of cross-frequency coupling and MI
		MI = zeros(size(phase_edges,1),size(amp_edges,1)); % stores results of Modulation Index
        for p = 1:size(phase_edges,1)
            for a = 1:size(amp_edges,1)

                % Select phases to analyze in this iteration
                fpassx = [phase_edges(p,1)  phase_edges(p,2)];
                fpassy = [amp_edges(a,1) amp_edges(a,2)];
				
				% Run cross-frequency coupling and capture outputs. The
				% "out" structure contains a ton of information about the
				% cross-frequency coupling calculation, some of which will
				% be useful above just having an MI
                out{p}{a} = ...
					calcxfreqcoupling(e1,e2, fpassx, fpassy, srate, numbins);
				MI(p,a) = out{p}{a}.MI;

            end
		end
        
        
    end

%% calcxfreqcoupling
%  Purpose: to carry out one calculation of cross frequency coupling.
%  Reaons this is not in the main function is two fold, (1) this will allow
%  the function to potentially batch a calculate, as in, farm many of these
%  out at once .. (2) it's nice to create an insular space for a different
%  island of computations
% -------------------------------------------------------------------------
    function [out, fx, fy] = ...
			calcxfreqcoupling(e1,e2,excludedtimes,times,fpassx,fpassy,srate,numbins,varargin)
        
        couplingoutput = true;
        for o = 1:2:numel(varargin)
            switch varargin{o}
                case 'extraoutput'
                    couplingoutput = varargin{o};
                otherwise
                    error('Input not understood!');
            end
        end
         
		%% Filter each signal
        % Filter for fpass
        e1 = eegfilt(e1',srate,fpassx(1),fpassx(2));
        e2 = eegfilt(e2',srate,fpassy(1),fpassy(2));

        %% Obtain Envelope and Phase
        hil_e1 = hilbert(e1);
        hil_e2 = hilbert(e2);
        envel_eeg2 = abs(hil_e2);
        phase_eeg1 = angle(hil_e1)';

        % Assign outputs
        out.phase_eeg1 = phase_eeg1;
        out.envel_eeg2 = envel_eeg2;
        
        phase_eeg1(logical(isExcluded(times,excludedtimes))) = NaN;
		
        %% Bin phases

        maxval = pi;
        minval = -pi;

        edges = minval : 2*pi/numbins : maxval;
        centers = edges(1:end-1) + (edges(2:end) - edges(1:end-1))/2;

        P = double(repmat(phase_eeg1', [numbins 1]));
        C = double(repmat(centers', [1 numel(phase_eeg1)]));

        difference				= abs(P - C);
        [~, bin_phase]			= min(difference,[],1,'omitnan');
        bin_phase(inan(phase_eeg1)) = NaN;

        % Stow away these bins into the output
		out.fpassx			= fpassx;
		out.fpassy			= fpassy;
        out.phase_edges		= edges;
        out.phase_centers	= centers;

        %% Compute the Cross-frequency Modulation

        % Compute the average amplitude per binned phase
        collected_amps = cell(1,numbins);
        mean_amps = zeros(1,numbins);
        for i = 1:numbins
            collected_amps{i} = envel_eeg2(bin_phase == i);
            mean_amps(i) = mean(collected_amps{i});
		end
		
		% Assign outputs
		out.collected_amps = collected_amps;
		out.mean_amps = mean_amps;
		
		%% Compute Modulation index
		MI = (log(numbins)-(-sum((mean_amps/sum(mean_amps))...
			.*log((mean_amps/sum(mean_amps))))))/log(numbins);
		
		% Assign output, passing the overall modulation for the centroid of
		% frequency pass of EEGX and centroid of the the frequency pass of
		% EEGY
		out.fx = fpassx(1) + range(fpassx)/2; fx = out.fx;
		out.fy = fpassy(1) + range(fpassy)/2; fy = out.fy;
		out.MI = MI;
        
        if ~couplingoutput
            clear out; out = [];
        end
        
    end

end