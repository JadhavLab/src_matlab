
psm = @(x) fprintf([x '\n']);

figfolder= input('Name your figure folder:','s');
if isempty(figfolder); figfolder = 'Data'; end

ff = @fullfile;
savedir = ff( '~','Data','Local','Current','Gamma-Lock',figfolder);

gtypes = '1 - Gamma \n 2 - Slow gamma \n 3 - Fast gamma \n ';
gam_type = input(sprintf('Which do you want to process?\n %s',gtypes));
if isempty(gam_type); gam_type = 1; end;


%% FILTER Creation

% Animal selection
%-----------------------------------------------------
animals = {'HPa'...
%   	,'HPb','HPc',...
	};

% Epoch filter
% -------------
runepochfilter = ...
	['(isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') ||'...
		'isequal($environment, ''ytr'')) && ($runsession == 2) && ($epoch == 4)']; 
dayfilter = '5';			% Ryan, day filter applied to 
							% this analysis because gamma not processed for
							% some animals on day 1

% Time filter
% -------------
timefilter_theta = {{'DFTFsj_getvelpos', '(($absvel >= 10))'}};

% EEG Filter: Phase 1
% ----------
ca1tetfilter	= ...
	'isequal($area, ''CA1'')';
eegfilter_ca1	= {'sj_geteegtet', 'theta', 'tetfilter',ca1tetfilter,...
	'maxvar',true};

% EEG Filter: Phase 2
% ----------
switch gam_type case 1, eeg='gamma'; case 2, eeg='lowgamma'; case 3, eeg='highgamma'; end;
pfctetfilter = '(isequal($area, ''PFC''))';
eegfilter_pfc = {'sj_geteegtet', eeg, 'tetfilter',pfctetfilter};

% Iterator
% --------
iterator = 'eeganal';

% Filter creation
% ----------------
modf_phase1 = createfilter('animal',animals,'days', dayfilter,'epochs',runepochfilter, ...
	'excludetimefilter', timefilter_theta,'eegtetrodes', eegfilter_ca1, 'iterator', iterator);

modf_phase2 = createfilter('animal',animals,'days', dayfilter,'epochs',runepochfilter, ...
	'excludetimefilter', timefilter_theta,'eegtetrodes', eegfilter_pfc, 'iterator', iterator);


%% Bin Theta! (Run First Filter)

psm('Beginning theta bin process ... '); tic;

modf_phase1 = setfilterfunction(modf_phase1, 'DFA_labelThetaBin_perTime', ...
	{'theta'});
modf_phase1 = runfilter(modf_phase1);

psm('...Done!'); toc; beep;

%% Assign Each Theta Phase a Gamma Phase (Run Second Filter)

psm('Beginning gamma matching process ... '); tic;

global collectgamma_initialized;
collectgamma_initialized = 0;
modf_phase2 = setfilterfunction(modf_phase2, 'DFAcollectGamma',{eeg},...
	'theta_bin_output', vertcat(modf_phase1.output),'epoch_list',...
	vertcat(modf_phase1.epochs),'gamma_threshold',130);
modf_phase2 = runfilter(modf_phase2);

psm('... Done!'); toc; beep;

%% SAVE!
clearvars -except modf_phase2 savedir figfolder ff
mkdir(ff(savedir));
save(ff(savedir, datestr(now)),'-v6');

%% GATHER!

matrix_outs = gatherFilterOutput(modf_phase2,...
	'dimension_list', [1 2 3 4 5], ...
	'field_names', {'bin_centers','bin_scheme','average_power','average_phase'}, ...
	'matrix_type',true);
cell_outs = gatherFilterOutput(modf_phase2,...
	'dimension_list', [1 2 3 4 5], ...
	'field_names', {'gamma_power_at_theta'}, ...
	'matrix_type',false,'subaddressing', true);


%% PLOT
currentfolder = pwd;
figurefolder = ff(savedir, datestr(now,30));
mkdir(figurefolder);
cd(figurefolder);

plot_average_per_theta = true;
if plot_average_per_theta
	
	%% Plot average per theta
	f = figure(1);clf;
	overall_average_power = mean(matrix_outs.average_power,2,'omitnan'); %TODO -- nans happen sometimes when gamma power is never found in a bin on a particular tetrode on a particular day
	plot(matrix_outs.bin_centers(:,1), overall_average_power,'r*-','linewidth',6);hold on;

	upper_overall_average_power = overall_average_power + std(matrix_outs.average_power,1,2,'omitnan')/...
		sqrt(size(matrix_outs.average_power,2));
	lower_overall_average_power = overall_average_power - std(matrix_outs.average_power,1,2,'omitnan')/...
		sqrt(size(matrix_outs.average_power,2));

	plot(matrix_outs.bin_centers(:,1), lower_overall_average_power,'k*:');
	plot(matrix_outs.bin_centers(:,1), upper_overall_average_power,'k*:');
	
	axis([-inf inf 0 1.2*(max(overall_average_power))]);
	title('Average Power per Bin');
	xlabel('Theta Bins in HPC');
	ylabel('Average Gamma Envelope Size in PFC');
	print(gcf,'-dpng','AveragePowerPerBin');
end; clear plot_average_per_theta;


plot_spread_per_theta = true;
if plot_spread_per_theta
	%% Plot average per theta
	
	alpha_val = 0.1;
	
	% Grab collected output
	powers_per_bin = cell_outs.gamma_power_at_theta;
	bin_centers= matrix_outs.bin_centers(:,1);
	
	random_samp_size = 5e4;
% 	w=waitbar(0,'Getting random sample of powers per bin ...');
	for i = 1:numel(bin_centers)
		
		number_of_points = numel(powers_per_bin{i});
		if random_samp_size > number_of_points; random_samp_size = number_of_points; end;
		random_samp = randperm(number_of_points);
		random_samp = random_samp(1:random_samp_size);
% 		waitbar(w,i/numel(bin_centers));

		f=figure(1);
		p=scatter( repmat(bin_centers(i),[1 random_samp_size]), ...
		powers_per_bin{i}(random_samp), 'bo','Filled','sizedata',80); hold on;
		p.MarkerEdgeAlpha = alpha_val; p.MarkerFaceAlpha= alpha_val;
		
		f=figure(2);
		p=scatter( repmat(bin_centers(i),[1 random_samp_size]), ...
		powers_per_bin{i}(random_samp), 'bo','Filled','sizedata',80); hold on;
		p.MarkerEdgeAlpha = alpha_val; p.MarkerFaceAlpha= alpha_val;
	end
		
	title('Spread of power in each bin');
	
	figure(1); axis([-inf inf -inf inf]);
	print(figure(1),'-dpng','PowerDistributionPerBin');
	figure(2); axis([-inf inf -inf inf]);
	print(figure(2),'-dpng','PowerDistributionPowerPerBin_withAverage');
	
end; clear plot_spread_per_theta;
	
cd(currentfolder);



