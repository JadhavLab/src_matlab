function output = DFA_labelThetaBin_perTime(tind, excludetimes, theta, varargin)
% Purpose:		This function creates a binning scenario for theta, then
%				rides along the eeg trace in time, and for every time,
%				assigns a bin id, i.e., which bin it belongs in. In my
%				current filter schema, this is so that a downstream filter
%				function can intercept this information and assign gamma
%				phases in accordance with theta.

	fprintf(' \n-----\nAnalyzing tind %d %d %d----', ...
	tind(1), tind(2), tind(3));

	%% Function Parameters and Binning
	% These functions for now will be tuned here. Later one can come in and
	% tune them through varargin.
	phase_amp = 10000; % related to how phase is stored in our eeg files
	
	theta_bins = 20;
	bin_scheme = -pi*phase_amp:(2*pi/theta_bins)*phase_amp:pi*phase_amp;
	bin_centers = bin_scheme(1:end-1) + (bin_scheme(2:end)-bin_scheme(1:end-1)); 
	
	%% Busy Work
	% If there's enough ram to support this, this should work.
	
	% Grab theta time and phase
	times			= geteegtimes(theta{tind(1)}{tind(2)}{tind(3)});
	theta_phase		= double(theta{tind(1)}{tind(2)}{tind(3)}.data(:,2));
	
	good_times			=	~isExcluded(times, excludetimes);
	logical_good		= ismember(times,times(good_times));
	theta_phase(~logical_good)	= NaN;
	NaN_locations = isnan(theta_phase)';
	
	% Prepare to run a vectorized bin detection
	mat_theta_phase = double(repmat(theta_phase', [numel(bin_centers) 1]));
	mat_theta_centers = double(repmat(bin_centers', [1 numel(theta_phase)]));
	
	difference				= abs(mat_theta_phase - mat_theta_centers);
% 	difference				= uint32(difference*1e12);
	[~, bin_ind]			= min(difference,[],1,'omitnan');
	bin_ind(NaN_locations) = NaN;
	
	%% Output
	
	output.theta_times	= times;
	output.theta_bins	= theta_bins;
	output.bin_scheme	= bin_scheme;
	output.bin_centers	= bin_centers;
	output.bin_ind		= bin_ind;

