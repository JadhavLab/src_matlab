function clearMI_HDD(which,animal)
% Simple function that clears  MI folder cache materials within each animal.

if ~exist('animal','var'),
	animal = {'HPa','HPb','HPc'};
end
if ~iscell(animal), animal = {animal}; end;

st = '';
switch which
	case 'mi', st='mi';
	case 'phase', st='phase';
	case 'amp', st='amp';
	case 'MI', st='MI';
	otherwise, error('Unrecognized input');
end

for an = animal, an=an{1};

	ainfo = animaldef(an);
	filestr = sprintf('%s/MI/%s%s??-??-??*',ainfo{2},an,st);
	eval(['!rm ' filestr])
end

