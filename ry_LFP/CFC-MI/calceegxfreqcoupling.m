function out = calceegxfreqcoupling(index, excludetimes, eeg, varargin)
% function out = calccoherence_test(index, excludetimes, eeg, varargin)
%
%  Plots the coherence for an eeg tetrode pair. If you use a time filter,
%  excluded times are removed and the includedtimes are averaged together.
%
%   out is a structure with the following fields
%       coherence-- This is the coherence for the tetrode pairs
%       frequency-- Frequency vector
%       index-- Only if appendindex is set to 1 (default)
% Ryan modified existing script to be able to autocorrelate phase and or
% power, and also replaced called eegfilt function with a nested internal:
% eegfilt.m could not be found anywhere in the path folders.

fprintf('Cross Correlations, day %d , ep %d, tetX %d, tetY %d ...\n',...
    index(1),index(2),index(3),index(4));

%% Internal Flags
debug = true;

%% Parse inputs and set defaults

% Default options
srate = 1500;
fpass1 = [6 12];
fpass2 = [100 150];
appendindex = 0;
lag = 100;
numsurrogate = 0;
numbins = 18;

% Default flags
plotoption = true;

for option = 1:2:length(varargin)-1   %% Compute Cross-frequency Coupling
    if isstr(varargin{option})       
        switch(varargin{option})
            case 'appendindex'
                appendindex = varargin{option+1};
            case 'fpass1'
                fpass1 = varargin{option+1};
            case 'fpass2'
                fpass2 = varargin{options+1};
            case 'lag'
                lag = varargin{option+1};
            case 'numbins'
                numbins = varargin{option+1};
            case 'numsurrogate'
                numsurrogate = varargin{option+1};
            case 'plotoption'
                plotoption = varargin{option+1}; 
            case 'animal'
                animal = varargin{option+1};
            otherwise
                warning(['Option ',varargin{option},' unknown.']);
        end   
    else
        error('Options must be strings, followed by the variable');
    end
end

%% Get times in agreement
% assign a temporary variable for eeg
e1 = eeg{index(1)}{index(2)}{index(3)};
e2 = eeg{index(1)}{index(2)}{index(4)};

e1times = geteegtimes(e1);
e2times = geteegtimes(e2);

if length(e1times)>length(e2times)
    temp = lookup(e2times,e1times);
    e1 = e1.data(temp);
    e2 = e2.data;
elseif length(e2times)>length(e1times)
    temp = lookup(e1times,e2times);
    e1 = e1.data;
    e2 = e2.data(temp);
elseif length(e1times)==length(e2times)
    e1 = e1.data;
    e2 = e2.data;
end

%% Filter each signal
% Filter for fpass
e1 = eegfilt(e1',srate,fpass1(1),fpass1(2));
e2 = eegfilt(e2',srate,fpass2(1),fpass2(2));

%% Obtain Envelope and Phase
% Obtain phasic information
% Hilbert based envelope
hil_e1 = hilbert(e1);
hil_e2 = hilbert(e2);
env_e2 = abs(hil_e2);
phase_e1 = angle(hil_e1)';

% Assign outputs
out.phase_eeg1 = phase_e1;
out.env_eeg2 = env_e2;

%% Bin phases

maxval = pi;
minval = -pi;

edges = minval : 2*pi/numbins : maxval;
centers = edges(1:end-1) + (edges(2:end) - edges(1:end-1))/2;

P = double(repmat(phase_e1', [numbins 1]));
C = double(repmat(centers', [1 numel(phase_e1)]));

difference				= abs(P - C);
[~, bin_phase]			= min(difference,[],1,'omitnan');

% Stow away these bins into the output
out.phase_edges = edges;
out.phase_centers = centers;

%% Compute the Cross-frequency Modulation

% Compute the average amplitude per binned phase
collect_amps = cell(1,numbins);
bin_amp_average = zeros(size(centers));
for i = 1:numbins
    
    collect_amps{i} = env_e2(bin_phase == i);
    bin_amp_average(i) = mean(collect_amps{i});
    
    
end

% Store variables
out.collected_amplitudes_per_bin = collect_amps;
out.amplitude_average_per_bin = bin_amp_average;

% Plot if requested
if plotoption
    figure(300); clf;
    plot(centers,bin_amp_average);
    tstring = sprintf('Animal, Cross-Frequency Coupling, day %d, ep %d\n Phase of %d in (%d-%d). Amp of %d in (%d-%d)',...
        index(1),index(2),index(3),fpass1(1),fpass1(2),index(4),fpass2(1),fpass2(2));
    title(tstring);
    xlabel('Phase');
    ylabel('Amplitude');
    print(tstring, '-dpng');
end


%% Compute surrogate if requested
% Comput surrogate cross frequency coupling
% TODO: Find out if this is something one can do! This surrogate is not
% correct for cross frequency coupling graph
if numsurrogate>0
        numpoints = length(e1);
        minskip = srate;
        maxskip = numpoints-srate;
        skip = ceil(numpoints.*rand(numsurrogate*2,1));
        skip(skip>maxskip)  =[];
        skip(skip<minskip)  =[];
        skip=skip(1:numsurrogate,1);
        surrogate = zeros(2*lag+1,numsurrogate);

    for s=1:numsurrogate
        surrogate_e1 = [e1(skip(s):end) e1(1:skip(s)-1)];
        surrogate(:,s) = xcorr(surrogate_e1,e2,lag);
    end

    out.surrogate = surrogate;
end

if (appendindex)
    out.index = index;
end

end