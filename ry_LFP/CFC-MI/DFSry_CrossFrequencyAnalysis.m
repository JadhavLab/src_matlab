clear all; close all;

%% FLAGS

% Analysis flags
checkbehavior       = true;
checkxfreqcoupling  = true;

% Plot flags
plotsessionaverages = true;
plotsummaryaverages  = true;


%% Filter Section

% -------------------------------------
%%% ANIMAL SELECTION
% -------------------------------------
animalfilter = {	...
					'HPa' ...
% 					'HPb' ...
%  					'HPc' ...
% 					'BRg' ...
% 					'Rtl' ...
% 					'Ada' ...
% 					'JW7' ...
				};

% -------------------------------------
%%% EPOCH SELECTION
% -------------------------------------
epochfilter = [...
	'isequal($type, ''run'') && '...
	'(isequal($environment, ''wtr1'') ||' ... 
	'isequal($environment, ''wtr2''))'
	];

dayfilter='5';

% -------------------------------------
%%% AREA AND CELL TYPE
% -------------------------------------

areas_to_examine = 3;
% Select the proper filter for the area that the user requested 
if isempty(areas_to_examine); areas_to_examine = 0; end;
switch areas_to_examine
	case 1 % Hippocampal CA1 - All
		tetrode_filter = { ...
			'isequal($area, ''CA1'') && ($numcells > 1)' ...
			'isequal($area, ''CA1'') && ($numcells > 1)' ...
			};
	case 2 % Prefrontal Cortex - All
		tetrode_filter = { ...
			'isequal($area, ''PFC'') && ($numcells > 1)'...
			'isequal($area, ''PFC'') && ($numcells > 1)' };
	case 3 % CA1-Prefrontal Interaction
		tetrode_filter = { ...
			'isequal($area, ''CA1'') && ($numcells > 1)' ...
			'isequal($area, ''PFC'') && ($numcells > 1)'};
	otherwise
		if exist('areas_to_examine','var') && ~isempty(areas_to_examine)
			fprintf('Using previous tetrode filter...\n');
		else
			error(['Error: Improper area selection! Please select a number '...
			'in accordance with the menu above.']);
		end
end

% -------------------------------------
%%% CONDITION OR BEHAVIOR SELECTION
% -------------------------------------

% Filter out times with velocities of interest
timefilter{1} = {'DFTFsj_getvelpos', '(($absvel >= 10))'};
% Filter out choice point zone, and apply sample window trigger properites
timefilter{2} = {'TF_trackZoneTrigger', '$inZone == 1', ...
    ... SPATIAL FILTERING CONTROL
	'spatial_proximity',7.5,'segment_id',1, ...
    'segment_address','final',...
    ... TRIGGER CONTROL
	'temporal_trigger_window', 0.4, 'entrance_or_exit', 'entrance'};

% -------------------------------------
%%% ITERATORs
% -------------------------------------
iterator_behavior			=	'epochbehaveanal';
iterator_coherence			=	'eeganal';


%% Creater Filters
	
f_base = createfilter('animal',animalfilter,'days',dayfilter,'epochs',epochfilter,...
	'excludetime',timefilter,'eegtetrodes',tetrode_filter{1},...
	'eegtetrodepairs',tetrode_filter);


if checkbehavior == true
	disp('Setting up behavior filter...');tic;
	f_behavior	= modifyfilter(f_base,	'iterator','epochbehaveanal');
	f_behavior = setfilterfunction(f_behavior,'plotBehavior2',{'pos','linpos'},...
        'arrow',true,'mode','filetrajmode','maxtraj_perplot',5);
	disp('...done!');toc;
	disp('Plotting behavior...');tic;
	runfilter(f_behavior);
	disp('...done!');toc
end

if checkxfreqcoupling == true
    bincount = 18;
	f_crossfreq = modifyfilter(f_base,	'iterator','epocheeganal');
	f_crossfreq = setfilterfunction(f_crossfreq,'calceegxfreqcoupling',{'eeg'},...
        ... CALCULATION OPTIONS
        'numbins', bincount, ...
        ... OTHER OPTIONS
        'plotoption',true);
	disp('Running coherence calculations...');tic;
	f_crossfreq = runfilter(f_crossfreq);
	disp(' ... complete!');toc;
end

%% Gather Data

if checkxfreqcoupling == true
    
    g_matfields = gatherFilterOutput(f_crossfreq,...
        ... WHICH FIELDS
        'field_names',{'amplitude_average_per_bin','phase_centers'}, ...
        ... HOW TO GATHER
        'dimension_list',[1 2 3 4 5], 'matrix_type', true, ...
        'matrix_singular', true);

    g_cellfields = gatherFilterOutput(f_crossfreq,...
        ... WHICH FIELDS
        'field_names',{'collected_amplitudes_per_bin'}, ...
        ... HOW TO GATHER
        'dimension_list',[1 2 3 4 5 6], 'matrix_type', false, ...
        'matrix_singular', false);
end


%% Plot

if plotsessionaverages
    % TODO: Normalize each of the different clusters
    
    for i = 1:numel(g_matfields)
        
        if isempty(g_matfields{i})
            continue;
        end
        
        average_amplitudes = g_matfields{i}.amplitude_average_per_bin;
        phase_centers = g_matfields{i}.phase_centers;
        
        % Normalize the average amplitudes, so we dont have to squint to
        % see changes because many of these taken on vastly different
        % average powered tetrodes
        average_amplitudes_norm = ...
            bsxfun(@rdivide, average_amplitudes, ...
            max(average_amplitudes,[],1));

        % Means of each session
        figure(150); clf;
        ind=randperm(size(phase_centers,2)); numsample = 30;
        plot(phase_centers(:,:),average_amplitudes_norm(:,:),'o-','linewidth',0.5);
        alpha 0.5
        tstring = sprintf('Tetrode %d, Average of Each Session Together', ...
            i);
        title(tstring);
        xlabel('Phase');
        
        ylabel('Amplitude');
        print(tstring,'-dpng');
    end
    
end

if plotsummaryaverages
    
    phase_centers = f_crossfreq(1).output{1}.phase_centers;
    
    amp_bin_collect = g_cellfields.collected_amplitudes_per_bin;
    collected_overall = {};
    average_overall = [];
    
    for i = 1:bincount
        collected_overall{i} = [];
        for j = 1:numel(amp_bin_collect)
            collected_overall{i} = [collected_overall{i} amp_bin_collect{j}{i}];
        end
        average_overall(i) = mean(collected_overall{i});
        ste_overall(i) = std(collected_overall{i})/numel(collected_overall{i});
    end
    
    figure(200);
    figure; bar(phase_centers, average_overall,0.95); 
    axis([-inf inf ...
        min(average_overall)-range(average_overall)*0.1 ...
        max(average_overall)+range(average_overall)*0.1]);
    
    tstring = sprintf('Average Over All Filtered Conditions');
    title(tstring);
    xlabel('Phase (Radians)');
    ylabel('Power');
    
    tstring = [date '_' tstring];
    print(tstring, '-dpng');
    
end
