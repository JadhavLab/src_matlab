function output = DFAcollectGamma(gind, excludetimes, gamma, varargin)

% Purpose:		Collect all the gamma that happen within each theta bin

fprintf(' \n-----\nAnalyzing gind %d %d %d ', ...
	gind(1), gind(2), gind(3));

%% Varargin

assert(numel(varargin) > 1);
for i = 1:2:numel(varargin)
	switch varargin{i}
		case 'gamma_threshold'
			gthresh = varargin{i+1};
		case 'theta_bin_output'
			tbo = varargin{i+1};
		case 'epoch_list'
			epoch_list = varargin{i+1};
		otherwise
			error('Have to provide modf output with theta binning.');
	end
end

%% Handle persistant variables that track 
persistent analysis_count;
persistent count_to_ind;
global collectgamma_initialized;
if isempty(collectgamma_initialized) || collectgamma_initialized == false
	
	fprintf('\n --- Starting to compute GAMMA POWER VERSUS THETA PHASE --- \n');
	
	count = 0;
	count_to_ind = {};
	for i = 1:numel(tbo)
	for j = 1:numel(tbo{i})
		count = count + 1;
		count_to_ind{count} = [i,j];
	end
	end
	
	analysis_count = 1;
	collectgamma_initialized = true;
end

%% Figure out which modf output to read from

% Get day epoch location of the theta data to be computed against
tind = count_to_ind{analysis_count};
dayepoch = epoch_list{tind(1)}(tind(2),:);

% If it does not agree with the current day epoch of this functional
% instantiation's gamma data's day epoch, then we need to increment our
% theta data index
if gind(1) ~= dayepoch(1) || gind(2) ~= dayepoch(2)
	analysis_count = analysis_count + 1;
	tind = count_to_ind{analysis_count};
	dayepoch = epoch_list{tind(1)}(tind(2),:);
	assert(gind(1) == dayepoch(1) || gind(2) == dayepoch(2));
end

% Output message to screen confirming theta that we are about to sample.
fprintf(' ... against tind %d %d \n-----\n', ...
	dayepoch(1),dayepoch(2));

an			= tbo{tind(1)}; to_analyze	= an(tind(2));
theta_bins	= to_analyze.theta_bins;
theta_times = to_analyze.theta_times;
bin_scheme	= to_analyze.bin_scheme;
bin_centers = to_analyze.bin_centers;
bin_ind		= to_analyze.bin_ind;

%% Get gamma data

% Grab theta time and phase
times			= geteegtimes(gamma{gind(1)}{gind(2)}{gind(3)});
gamma_power		= double(gamma{gind(1)}{gind(2)}{gind(3)}.data(:,3));
gamma_phase		= double(gamma{gind(1)}{gind(2)}{gind(3)}.data(:,2));

% Find  gamma times where power is greater than threshold
if exist('gthresh','var')
	selection_logical	= gamma_power > gthresh;
	gamma_power			= gamma_power(selection_logical);
	gamma_phase			= gamma_phase(selection_logical);
	times				= times(selection_logical);
end

% Create a mapping between times
gamma_to_theta = lookup(times,theta_times);

% Convert bin_ind so that each gamma time has a theta bin
bin_ind = bin_ind(gamma_to_theta);


gamma_power_at_theta = {};
average_power = [];
std_power = [];
median_power = [];
for i = 1:theta_bins
	
	this_bin = double(bin_ind' == i);
	this_bin(bin_ind~=i) = NaN;
	
	% Collect gamma powers per bin
	collect_power_at_bin = gamma_power .* this_bin;
	collect_power_at_bin(isnan(collect_power_at_bin)) = [];
	gamma_power_at_theta{i} = collect_power_at_bin;
	
	% Calc statistics on the powers collected
	average_power(i) = mean(collect_power_at_bin);
	std_power(i) = std(collect_power_at_bin);
	median_power(i) = median(collect_power_at_bin);
	
	% Collect gamma phase per bin
	collect_phase_at_bin = gamma_phase .* this_bin;
	collect_phase_at_bin(isnan(collect_phase_at_bin)) = [];
	gamma_phase_at_theta{i} = collect_phase_at_bin;
	
	% Calc statistics on the powers collected
	average_phase(i) = mean(collect_phase_at_bin);
	std_phase(i) = std(collect_phase_at_bin);
	median_phase(i) = median(collect_phase_at_bin);
	
	%% Assertions
	assert(sum(isnan(collect_power_at_bin)) <= 0);
	assert(sum(isnan(collect_phase_at_bin)) <= 0);
	
end


%% Output results

output.bin_scheme	= bin_scheme;
output.bin_centers	= bin_centers;
output.bin_ind		= bin_ind;

output.theta_bins		= theta_bins;
output.theta_at_times	= theta_times(gamma_to_theta);

output.gamma_power_at_theta = gamma_power_at_theta;
output.average_power		= average_power;
output.median_power			= median_power;
output.std_power			= std_power;

output.gamma_phase_at_theta = gamma_phase_at_theta;
output.average_phase		= average_phase;
output.median_phase			= median_phase;
output.std_phase			= std_phase;

end
