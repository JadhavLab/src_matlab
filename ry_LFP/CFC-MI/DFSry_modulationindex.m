close all;

%notes = input('Enter short note about this analysis:','s');
% notes = 'velgreaterthan5cm';

%% Apply ParameterExplorer params, if they exist or set variabl params
% These can overwrite any parameters in the variable initialization phases
% above, right before we create the filter. This is what gives
% ParameterExplorer the power to iterate over this script with different
% compinations.

projectdir = '~/Data/Local/Current/ModulationIndex/';
if exist('params','var') && isstruct(params)
	ParameterExplorer.swapParamSet(params);
    savedir = ParameterExplorer.savelocation(params,'projectfolder',projectdir);
    fprintf('\n\n===========================\n');
    fprintf('Starting to process in %s \n',savedir);
else
    areas_to_examine	= 1;
	velocity			= 10;
	ripplethres			= 2;
	choicepoint			= false;
	
    savedir				= projectdir;
end
clear('projectdir');

%% FLAGS

% Defaults
temporal_window = 1.5; % seconds

% Analysis flags
checkbehavior       = false;
checkMI				= true;

% Plot flags
plotsessionaverages = true;
plotsummaryaverages  = true;


%% Filter Section

% -----------------------------------------------
%%% ANIMAL SELECTION -- Which animals to include!
% -----------------------------------------------
animalfilter = {	...
					'HPa' ...
 					'HPb' ...
  					'HPc' ...
% 					'BRg' ...
% 					'Rtl' ...
% 					'Ada' ...
% 					'JW7' ...
				};

% -------------------------------------
%%% EPOCH SELECTION
% -------------------------------------
epochfilter = [...
	'isequal($type, ''run'') && '...
	'(isequal($environment, ''wtr1'') ||' ... 
	'isequal($environment, ''wtr2''))'
	];

dayfilter=num2str(dayfilter);

% -------------------------------------
%%% AREA AND CELL TYPE
% -------------------------------------

% Select the proper filter for the area that the user requested 
switch areas_to_examine
	case 1 % Hippocampal CA1 - All
        type = 'CA1-CA1';
		tetrode_filter = { ...
			'isequal($area, ''CA1'') && ($numcells > 1)' ...
			'isequal($area, ''CA1'') && ($numcells > 1)' ...
			};
	case 2 % Prefrontal Cortex - All
        type = 'PFC-PFC';
		tetrode_filter = { ...
			'isequal($area, ''PFC'') && ($numcells > 1)'...
			'isequal($area, ''PFC'') && ($numcells > 1)' };
	case 3 % CA1-Prefrontal Interaction
        type = 'CA1-PFC';
		tetrode_filter = { ...
			'isequal($area, ''CA1'') && ($numcells > 1)' ...
			'isequal($area, ''PFC'') && ($numcells > 1)'};
end

% -------------------------------------
%%% CONDITION OR BEHAVIOR SELECTION
% -------------------------------------

% Filter for the ripple tetrode
riptetfilter = '(isequal($descrip, ''riptet''))';

% Filter out times with velocities of interest
timefilter = cell(1);
timefilter{1}	= {'DFTFsj_getvelpos', ['(($absvel >= ' num2str(velocity) '))']};
if ripplethres > 0
	timefilter{end+1}	= ...
    {'DFTFsj_getriptimes','($nripples == 0)',...
    'tetfilter',riptetfilter,'minthresh',ripplethres};
end
if spacefilter
%     timefilter{end+1}	= ...
%     {};
end

% Filter out choice point zone, and apply sample window trigger properites
if choicepoint
	timefilter{end+1} = {'TF_trackZoneTrigger', '$inZone == 1', ...
		... SPATIAL FILTERING CONTROL
		'spatial_proximity',7.5,'segment_id',1, ...
		'segment_address','final'};
end

% -------------------------------------
%%% ITERATORs
% -------------------------------------
iterator_behavior			=	'epochbehaveanal';
iterator_coherence			=	'eeganal';


%% Creater Filters
	
f_base = createfilter('animal',animalfilter,'days',dayfilter,'epochs',epochfilter,...
	'excludetime',timefilter,'eegtetrodes',tetrode_filter{1},...
	'eegtetrodepairs',tetrode_filter);

% Since we will run 2+ filter functions, initialize a cell to keep track of
% outputs to save
savevar = {};

if checkbehavior	== true
	disp('Setting up behavior filter...');tic;
	f_behavior	= modifyfilter(f_base,	'iterator','epochbehaveanal');
	f_behavior = setfilterfunction(f_behavior,'plotBehavior2',{'pos','linpos'},...
        'arrow',true);
	disp('...done!');toc;
	disp('Plotting behavior...');tic;
	runfilter(f_behavior);
	disp('...done!');toc
    
    savevar{end+1} = 'f_behavior';
end

if checkMI		== true
    bincount = 18;
	f_MI = modifyfilter(f_base,'iterator','epocheeganal');
	f_MI = setfilterfunction(f_MI,'ryDFA_calcxfreqmodset2',{'eeg'},...
        ... CALCULATION OPTIONS
        'numbins', bincount, ...
		... PARALLELIZATION OPTION
		'batchmode', false, ...
        ... OTHER OPTIONS
        'plotoption',true, 'struct_outputlevel', 1, 'save_detailedoutput', true, ...
		'savedir', savedir);
	f_MI = runfilter(f_MI);
	disp(['Running ' savedir ' MI calculations ...']);tic;
	disp(' ... complete!');toc;
    
    savevar{end+1} = 'f_MI';
end

% Save filters
warning off; mkdir(savedir); warning on;
filtersavefile = fullfile(savedir, 'modf');
if ~exist(filtersavefile,'file')
    save(filtersavefile, savevar{:},'-v7.3');
else
    save(filtersavefile, savevar{:},'-v7.3','-append');
end
fprintf('Save location --> %s\n', savedir);

clear;

%% Gather Data


%% Plot

