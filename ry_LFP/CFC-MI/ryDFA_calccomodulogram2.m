function out = ryDFA_calccomodulogram2(index,excludetimes, eeg, varargin)
% function out = ryDFA_calccomodulogram2(index, excludetimes, eeg, varargin)
%
% NOTE : RUNS SLOWER THAN calceegxfreqcoupling ... but soaks up LESS RAM
% 
%  Plots comodulation for each requested period in INDEX of the form [
%  day epoch tetrode1 tetrode2]. If you use a time filter,
%  excluded times are removed and the includedtimes are averaged together.
%
%   out is a structure with the following fields
%       coherence-- This is the coherence for the tetrode pairs
%       frequency-- Frequency vector
%       index-- Only if appendindex is set to 1 (default)
% Ryan modified existing script to be able to autocorrelate phase and or
% power, and also replaced called eegfilt function with a nested internal:
% eegfilt.m could not be found anywhere in the path folders.
%
% TODO -- incorporate ability to separate into trials
% TODO -- separate output mode into alloutput and extraoutput, where
% TODO -- incorporate verbose save of information to file .. proper averaging of 
% sessions will require combining the binned information from each session instead
% of merely averaging each MI ... but binned info too large to hold in RAM
% extraoutput gives me just enough to gather and generate average pictures.

fprintf('Cross Frequency Coupling & MI, day %d , ep %d, tetX %d, tetY %d ...\n',...
    index(1),index(2),index(3),index(4)); tic
cdir=pwd;

%% Internal Flags
debug = true;

%% Parse inputs and set defaults

% Default options
savedir = pwd; 
animal = '';
%%%%%%%%%%%%%%%%%%%%%%%%%%
srate           = 1500;
%%%%%%%%%%%%%%%%%%%%%%%%%%
phasepass			= [0.5 30];
amppass				= [0.5 80];
phasebinstepsize    = range(phasepass)/35;
ampbinstepsize      = 2; %range(amppass)/45;
phasepass_percalc   = [];
amppass_percalc     = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%
numbins = 18; % number of bins per cross-frequency modulation calc
%%%%%%%%%%%%%%%%%%%%%%%%%%
lag             = 100;
appendindex     = 0;
numsurrogate    = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default flags
batchmode = true;
plotoption = true;
struct_outputlevel = false;
struct_outputlevel=false;
save_extraoutput=false;
%%%%%%%%%%%%%%%%%%%%%%%%%%
container=[];

for option = 1:2:length(varargin)-1   %% Compute Cross-frequency Coupling
    if isstr(varargin{option})       
        switch(varargin{option})
			% ------------------------------
			% OUTPUT OPTIONS
			% ------------------------------
            case 'struct_outputlevel', struct_outputlevel = varargin{option+1};
                % Turns off meaningful output into filterframework's
                % objects' RAM space. Useful for printing simply printing
                % the MI figure per each of these function calls and not
                % saving the detailed output which clogs RAM.
			case 'save_detailedoutput', save_extraoutput = varargin{option+1};
            case 'appendindex', appendindex = varargin{option+1};
			% ------------------------------
			% BINNING AND STEP SIZE OPTIONS
			% ------------------------------
            case 'overallphasepass', phasepass = varargin{option+1};
            case 'overallamppass', amppass = varargin{options+1};
			case 'phasepasspercalc', phasePassPerCalc = varargin{option+1};
			case 'amppasspercalc', ampPasspercalc = varargin{option+1};
            case 'numbins', numbins = varargin{option+1};
				% per cross-frequency coupling
            % ------------------------------    
			% LAG OPTION
			% ------------------------------
            case 'lag'
                lag = varargin{option+1};
			% SURROGATE OPTION - for validation
            case 'numsurrogate'
                numsurrogate = varargin{option+1};
			% MODE OPTIONS
            case 'plotoption'
                plotoption = varargin{option+1};
            case 'savedir'
                savedir = varargin{option+1};
            case 'batchmode'
                % This controls whether or not each cross-frequency
                % coupling calculation is done as a batch, multicore across
                % all available processors or cluster components ...
                % definitely the fastest, but not supported on machines
                % without parallell processing toolbox
                batchmode = varargin{option+1};
            case 'animal'
                animal = varargin{option+1};
	    case 'container'
		container = varargin{option+1};
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end   
    else
        error('Options must be strings, followed by the variable');
    end
end;
if ~(save_extraoutput || struct_outputlevel)
	% If output saving is not selected, then there's no point to enabling
	% higher levels of output
	struct_outputlevel = false;
end
if ~isempty(animal), ainfo = animaldef(animal); end;

%% Get eeg trace times in agreement
% assign a temporary variable for eeg
e1 = eeg{index(1)}{index(2)}{index(3)};
e2 = eeg{index(1)}{index(2)}{index(4)};

e1times = geteegtimes(e1);
e2times = geteegtimes(e2);

if length(e1times)		> length(e2times)
    temp = lookup(e2times,e1times);
    e1 = e1.data(temp);
    e2 = e2.data;
    e1times = e1times(temp);
elseif length(e2times)	> length(e1times)
    temp = lookup(e1times,e2times);
    e1 = e1.data;
    e2 = e2.data(temp);
    e2times = e2times(temp);
elseif length(e1times)	== length(e2times)
    e1 = e1.data;
    e2 = e2.data;
end

%% Filter in all relevant bands
warning off; mkdir(fullfile(ainfo{2},'MI')); warning on;
phasefile=fullfile(ainfo{2},'MI',sprintf('%sphase%02d-%02d-%02d.mat',ainfo{3},index(1:3)));
ampfile=fullfile(ainfo{2},'MI',sprintf('%samp%02d-%02d-%02d.mat',ainfo{3},index([1 2 4])));
if ~exist( phasefile, 'file' )
	[phasefilts,phase_edges] = ...
	    filter_phase(e1,srate,phasepass,phasebinstepsize,phasepass_percalc);
	save(phasefile,'phasefilts','phase_edges');
else
	load(phasefile);
end
if ~exist( ampfile , 'file' )
	[ampfilts,amp_edges] = ...
	    filter_amp(e2,srate,amppass,ampbinstepsize,amppass_percalc);
	save(ampfile,'ampfilts','amp_edges');
else
	load(ampfile);
end

%% Exclude times! -- Any time that falls outside filtered times is removed here

excluded_e1 = logical(isExcluded(e1times,excludetimes));
phasefilts(:,excluded_e1) = [];
ampfilts(:,excluded_e1) = [];
clear e1 e2 excluded_e1 e1times e2times

%% Calculation
tic
if batchmode 	% Each amplitude-phase combo is done in a parallelized batch
	fprintf('\nBatch mode...');
	[detailedout, MI_out] = batchCalculate(phasefilts,ampfilts,phase_edges,amp_edges,...
		numbins,'outputlevel',struct_outputlevel);
else		% Each amplitude-phase combo is done serially, one-by-one
	fprintf('\nSerial mode...');
	[detailedout, MI_out] = regularCalculate(phasefilts, ampfilts,phase_edges,amp_edges,numbins);
end
toc

%% Output and Plotting
% Plot if requested
if plotoption
    
    % Obtain center frequencies per bin, to constitute x and y axes
    p_edges = phasepass(1):phasebinstepsize:phasepass(2);
    a_edges	= amppass(1):ampbinstepsize:amppass(2);
    p_centers = p_edges(1:end-1) + ...
        (1/2)*(p_edges(2:end)-p_edges(1:end-1));
    a_centers = a_edges(1:end-1) + ...
        (1/2)*(a_edges(2:end)-a_edges(1:end-1));

    % Plot MI index using computed x and y axes
    f=figure(350); f.Visible='off'; hold off;
    imagesc(p_centers,a_centers,MI_out');
    tstring = sprintf('%s MI, day %d, ep %d\n Phase of %d in (%3.1f-%3.1f). Amp of %d in (%3.1f-%3.1f)',...
        animal,index(1),index(2),index(3),phasepass(1),phasepass(2),index(4),amppass(1),amppass(2));
    set(gca,'YDir','normal');
    title(tstring);
    xlabel('Frequency_{phase}');
    ylabel('Frequency_{amplitude}');
    
    sstring = sprintf('%s MI, day %d, ep %d, Phase of %d in (%3.1f-%3.1f). Amp of %d in (%3.1f-%3.1f)',...
        animal,index(1),index(2),index(3),phasepass(1),phasepass(2),index(4),amppass(1),amppass(2));
    currdir=pwd;
    mkdir(savedir); mkdir([savedir '/pdf']);mkdir([savedir '/fig']); cd(savedir);
    saveas(f,[sstring '.png']);
    saveas(f,['./pdf/' sstring '.pdf']);
    saveas(f,['./fig/' sstring '.fig']);
    cd(currdir);
    
end

% Save data to file system
sstring = strrep(sstring,', ','_');

% TODO change this to animaldef.m based save, so we don't have to
% rely on filter framework output (will be easier to manipulate)

MI_file = fullfile(ainfo{2},'mi',sstring);
if exist('MI_file','file'), 
	load(MI_file);
	temp.MI = MI;
	temp.detailedout=detailedout;
	if isempty(container)
		[~,loc]=fileparts(pwd);
		mi(loc) = temp;
	else
		loc = container;
		mi(loc) = temp;
	end
else
	if isempty(container)
		mi.MI = MI;
		mi.detailedout=detailedout;
	else
		loc = container;
		mi = containers.Map;
		temp.MI = MI;
		temp.detailedout=detailedout;
		mi(loc) = temp;
	end
end

if save_extraoutput
	save( MI_file, 'mi');
else
	save( MI_file, 'mi');
end

% Assign outputs
if struct_outputlevel
    out.detailedout = detailedout;
    out.MI = MI_out;
else
    out.MI = MI_out;
end

cd(cdir);
clearvars -except out

beep;

fprintf(' \n... Done! \n');toc;
return

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%						HELPER FUNCTIONS
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% CALCULATION MODES
% -------------------------------------------------------------------------
	function [results, MI] = ...
            batchCalculate(e1,e2,phase_edges,amp_edges,numbins,varargin)
	%% Name:		batchCaclulate
	%  Purpose:		Handles the batch calculation of cross-frequency coupling and
	%				associated modulation index in several batches to take
	%				advantage of either multiple cores on the computer or
	%				take advantage of a cluster (your endogenous matlab
	%				settings determine if it will run on your cores or a
	%				cluster you've predefined.)
	% 
	% TODO:			Move the filtering step inside this scope .. that way waveforms
	%				won't have to be filtered multiple times in each band
	%				and also take up more RAM with parallel processing.
        
        %% Optional arg handling
         outputlevel = true;
        for o = 1:2:numel(varargin)
            switch varargin{o}
                case 'outputlevel', outputlevel = varargin{o};
                otherwise
                    error('Unrecognized input!');
            end
        end

		%% Batch process
		% Farm out each combination of phase and amplitude slice to a batch
		% process
        tJob_Count = 0;
		set_count = 0;
        job_limit = 15;
        worker_pool = gcp;
        fprintf('\nMI jobs queued: ');
        for p = 1:size(phase_edges,1)
            for a = 1:size(amp_edges,1)

                % Select phases to analyze in this iteration
                fpassx = [phase_edges(p,1)  phase_edges(p,2)];
                fpassy = [amp_edges(a,1) amp_edges(a,2)];

                % Keep tabs on how many jobs we have out
                tJob_Count = tJob_Count + 1;
				set_count = set_count + 1;
				% Initiate the batch job
                job(tJob_Count) = parfeval(@calcxfreqcoupling,3,...
                    e1(p,:),e2(a,:), fpassx, fpassy,numbins,...
                    'outputlevel', outputlevel);
                fprintf('%d ', tJob_Count);
                
                % This is what protects us from using more RAM than is
                % available ... user should scale the job limit to match
                % their RAM
                if set_count >= job_limit
                    fprintf('Wait! ');
					A = tJob_Count-set_count+1; B = tJob_Count;
                    finished_ok = wait(job(A:B));
					[batchout(A:B), fX(A:B), fY(A:B)] = ...
						fetchOutputs(job(A:B));
					delete(job(A:B));
					set_count = 0;
                end
			end
        end

		
		%% Gather any outputs left
		if set_count > 0
			fprintf('Wait! ');
			A = tJob_Count-set_count+1; B = tJob_Count;
			finished_ok = wait(job(A:B));
			[batchout(A:B), fX(A:B), fY(A:B)] = ...
				fetchOutputs(job(A:B));
			delete(job(A:B));
			set_count = 0;
		end
		
		%% Re-index outputs
        unFreqX = sort(unique(fX)); unFreqY = sort(unique(fY));
        nUFX = numel(unFreqX); nUFY = numel(unFreqY);
        MI = zeros(nUFX,nUFY);
        results = batchout(1); %initialize
		for x = 1:nUFX
			for y = 1:nUFY
				
				indy = fY == unFreqY(y);
				indx = fX == unFreqX(x);
				ind = indx & indy;
				
				results(x,y) = batchout(ind);
                MI(x,y) = results(x,y).MI;
			end
        end
		
	end
% -------------------------------------------------------------------------
    function [out, MI] = ...
            regularCalculate(phase_eeg1,amp_eeg2,...
			phase_edges, amp_edges,numbins,varargin)
		%% regularCaclulate
		%  Purpose: Handles all calculation of cross-frequency coupling and
		%  associated modulation index in by sequentially iterating. Takes longer
		%  than the batch version, but matlab users will be more comfortable
		%  modifying this or thinking about what's going on than with the batch
		%  version.
        %% Optional arg handling
         outputlevel = true;
        for o = 1:2:numel(varargin)
            switch varargin{o}
                case 'outputlevel'
                    outputlevel = varargin{o};
            end
        end
        
        
		%% Calculate each bout of cross-frequency coupling and MI
		MI = zeros(size(phase_edges,1),size(amp_edges,1)); % stores results of Modulation Index
        out=cell(1,size(phase_edges,1));
        for p = 1:size(phase_edges,1)
            out{p}=cell(1,size(amp_edges,1));
            for a = 1:size(amp_edges,1)
		
		fprintf('(%d %d) ',p,a)

                % Select phases to analyze in this iteration
                fpassx = [phase_edges(p,1)  phase_edges(p,2)];
                fpassy = [amp_edges(a,1) amp_edges(a,2)];
				
				% Run cross-frequency coupling and capture outputs. The
				% "out" structure contains a ton of information about the
				% cross-frequency coupling calculation, some of which will
				% be useful above just having an MI
                [out{p}{a}, ~, ~] = ...
					calcxfreqcoupling(phase_eeg1(p,:),amp_eeg2(a,:),fpassx,fpassy,numbins,'outputlevel',outputlevel);
				MI(p,a) = out{p}{a}.MI;

            end
		end
        return;
        
	end
% -------------------------------------------------------------------------
% CROSS-FREQUENCY COUPLING
% -------------------------------------------------------------------------
    function [out, fx, fy] = ...
			calcxfreqcoupling(phase_eeg1,amp_eeg2,fpassx,fpassy,numbins,varargin)
		%% calcxfreqcoupling
		%  Purpose: to carry out one calculation of cross frequency coupling.
		%  Reaons this is not in the main function is two fold, (1) this will allow
		%  the function to potentially batch a calculate, as in, farm many of these
		%  out at once .. (2) it's nice to create an insular space for a different
		%  island of computations
		% ------------------------------------------------------------------
        outputlevel = true;
        for o = 1:2:numel(varargin)
            switch varargin{o}
                case 'outputlevel', outputlevel = varargin{o+1};
            end
        end

        if outputlevel > 1
            % Assign outputs
            out.phase_eeg1 = phase_eeg1;
            out.amp_eeg2 = amp_eeg2;
        end
		
        %% Bin phases

        maxval = pi;
        minval = -pi;

        edges = minval : 2*pi/numbins : maxval;
        centers = edges(1:end-1) + (edges(2:end) - edges(1:end-1))/2;

        P = double(repmat(phase_eeg1, [numbins 1]));
        C = double(repmat(centers', [1 numel(phase_eeg1)]));

        difference				= abs(P - C);
        [~, bin_phase]			= min(difference,[],1,'omitnan');

        % Stow away these bins into the output
        if outputlevel==1
            out.fpassx			= fpassx;
            out.fpassy			= fpassy;
            out.phase_edges		= edges;
            out.phase_centers	= centers;
        end

        %% Compute the Cross-frequency Modulation

        % Compute the average amplitude per binned phase
        collected_amps = cell(1,numbins);
        centroid_amps = zeros(1,numbins);
        parfor i = 1:numbins
            collected_amps{i} = amp_eeg2(bin_phase == i);
            centroid_amps(i) = mean(collected_amps{i});
			num_amps(i) = numel(collected_amps{i});
		end
		
		% If extraoutput, add collected and mean amps to out struct
        if outputlevel > 1
            out.collected_amps = collected_amps;
		elseif outputlevel > 0
            out.centroid_amps = centroid_amps;
			out.num_amps = cellfun(@numel,collected_amps);
        end
		
		%% Compute Modulation index
		tot_centamps = sum(centroid_amps);
		MI = (log(numbins)+...
			(sum((centroid_amps/tot_centamps)...
			.*log(centroid_amps/tot_centamps))))/log(numbins);
        fx = fpassx(1) + range(fpassx)/2;
        fy = fpassy(1) + range(fpassy)/2;
        
        if outputlevel>0
            % Assign output, passing the overall modulation for the centroid of
            % frequency pass of EEGX and centroid of the the frequency pass of
            % EEGY
            out.fx = fx;
            out.fy = fy;
            out.MI = MI;
        else
            out = [];
        end
        
        clear collected_amps P C difference bin_phase edges centers
        return;
        
	end

% -------------------------------------------------------------------------
% FILTERING EEGS
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
    function [phasefilts,phase_edges] = filter_phase(e,srate,phasepass,phasestepsize,phasepass_percalc)
        %% filter_all
		% Purpose: This function is called in regularMode and batchMode scopes,
		% and filters all eegs in the relevent bands, just once, so it doesn't
		% have to be redone
        
       %% Initialize edges
		phase_edges = phasepass(1):phasestepsize:phasepass(2);
		
		% If there is a phase pass per calculation P and amp pass per
		% calculation A, then for every step size, we bandpass +/- P/2 and
		% +/- A/2 
		if ~isempty(phasepass_percalc)
			phase_centers = phase_edges(1:end-1) + ...
				(1/2)*(phase_edges(2:end)-phase_edges(1:end-1));
			
			phase_edges = [phase_centers(1:end-1)'-phasepass_percalc/2 ...
				phase_centers(2:end)'+phasepass_percalc/2];
		% Else, the step size tells us the edges
		else
			phase_edges = [phase_edges(1:end-1)' phase_edges(2:end)'];
		end
        
        %% Compute each filtered trace per phase and amplitude
        nPhaseBin = size(phase_edges,1); 
        fprintf('\nFiltering phase range: ');
        for ii = 1:nPhaseBin
            job_phase(ii) = ...
                parfeval(@eegFiltPhase,1,...
                e',srate,phase_edges(ii,1),phase_edges(ii,2));
            fprintf(' %d',ii);
        end
        
        %% Wait
        wait(job_phase);
		
        %%  Collect Results
        phasefilts = fetchOutputs(job_phase);
        delete(job_phase);
    end

    function [ampfilts,amp_edges] = filter_amp(e,srate,amppass,ampstepsize,amppass_percalc)
        %% filter_all
		% Purpose: This function is called in regularMode and batchMode scopes,
		% and filters all eegs in the relevent bands, just once, so it doesn't
		% have to be redone
        
       %% Initialize edges
		amp_edges = amppass(1):ampstepsize:amppass(2);
		
		% If there is a phase pass per calculation P and amp pass per
		% calculation A, then for every step size, we bandpass +/- P/2 and
		% +/- A/2 
		if ~isempty(amppass_percalc)
			amp_centers = amp_edges(1:end-1) + ...
				(1/2)*(amp_edges(2:end)-amp_edges(1:end-1));
			
			amp_edges	= [amp_centers(1:end-1)'-amppass_percalc/2 ...
				amp_centers(2:end)'+amppass_percalc/2];
		% Else, the step size tells us the edges
        else
			amp_edges	= [amp_edges(1:end-1)' amp_edges(2:end)'];
		end
        
        %% Compute each filtered trace per phase and amplitude
        
        nAmpBin = size(amp_edges,1);
        fprintf('\nFiltering amp range: ');
        for jj = 1:nAmpBin
            job_amp(jj) = ...
                parfeval(@eegFiltAmp,1,...
                e',srate,amp_edges(jj,1),amp_edges(jj,2));
            fprintf(' %d',jj);
        end
        
        %% Wait
        wait(job_amp);
		
        %%  Collect Results
        ampfilts= fetchOutputs(job_amp);
        
        delete(job_amp(jj));
        
        return;
        
	end


    function out = eegFiltPhase(eeg,srate,fpass1,fpass2)
        out = angle(hilbert(eegfilt(eeg,srate,fpass1,fpass2)));
    end
% -------------------------------------------------------------------------
    function out = eegFiltAmp(eeg,srate,fpass1,fpass2)
        out = abs(hilbert(eegfilt(eeg,srate,fpass1,fpass2)));
	end

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%					END OF HELPER FUNCTIONS
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-------------------------------------------------------------------------

end
