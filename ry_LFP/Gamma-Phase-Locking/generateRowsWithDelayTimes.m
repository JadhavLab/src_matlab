% -----------------------------------------------------------------
% Name:			generateRowsWithDelayTimes
% Parent:		(undefined)
%
% Purpose:		Takes a spike time and generates a set of indices to sample
%               in front of and behind 
% 
% Input:		wave, time, min_move, step_size, max_move;
%                   ... where, wave is an 1xT vector of values
%                   ... and, time is a 1xT vector of time values
% 
%
% Output:		time_shifted_times,
%                   ... where, time_shifted_times is a N X T matrix, and
%                   each row K constitutes a min_move + K x step_size time
%                   shift, with 1 < K < N.
%				delays, a list of time delays
%				new_boundary, a start and end of samples to exclude 
%				... ... for spikes that carry over the periods we can assign
%				... ... phases at the boundary of the sample period
% -----------------------------------------------------------------
function [time_shifted_times, delays, new_boundary] = ...
    generateRowsWithDelayTimes(time,min_move,step_size,max_move)

% Decide number of rows and if it's not big enough to tile out the time
% space requested
num_of_rows = ((max_move - min_move)/step_size) + 1;
if strcmp(class(num_of_rows),'float')
    warning(['step_size not large enough to tile the space between ' ...
        'and max_move']);
    num_of_rows = int(floor(num_of_rows));
end

% Transpose wave into a row vector, if it's a column
if size(time,1) > 1
    time = time';
end

% Create a row per time entry
construct_times = repmat(time,num_of_rows,1);

% Create the matrix of time shifts to add or subtract to each matrix entry
delays = [min_move:step_size:max_move]';
if ~ismember(0, delays)
    positive_ind = delays>0;
    negative_ind = delays<0;
    delays = [delays(negative_ind) 0 delays(positive_ind)];
end
time_shifts = repmat(delays,1, size(time,2));

% Create matrix of time 
time_shifted_times = construct_times + time_shifts;

% Now acquire the matrix of indices for thesetimes, which spits out an
% ordered vector of the same size as the lookup matrix. Then reshape the
% matrix, so now we have a matrix of indices.
time_shifted_indices = lookup(time_shifted_times,time);
time_shifted_indices = reshape(time_shifted_indices, ...
    num_of_rows, numel(time));

new_boundary.start = time_shifted_times(1,1);
new_boundary.stop = time_shifted_times(end,end);