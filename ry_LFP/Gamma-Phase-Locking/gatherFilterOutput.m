%% -----------------------------------------------------------------------------
% Name:			gatherDimensions
% Purpose:		The purpose of this function is to automatically gather
%				data across any set of dimensions for any filter outputs. I
%				think the data gathering is such a frequently performed
%				step that it merits a function that can get the data
%				gathering up off the ground. This will be an ongoing
%				project, as no doubt the number of ways to gather data and
%				process it will be difficult to anticipate at the outset.
%
% Inputs:		
% Outputs:		
function fo = gatherFilterOutput(modf,varargin)

%% Parameter Parsing

% Variable storing the fields to gather
field_count = 0;
field_name = {};
% Variable storing the dimensions to gather across
dimension_list = [];
% Variable storing per dimension whether to gather via cell or matrix
matrix_output = false; % controls whether gather occurs into matrix or cell
subaddressing = false; % controls whether we gather within the addressing structure of each output object, for example if each output is 1x8 cell, then gather between objects but each (i,j) of object A combines with (i,j) of object B.
singular = false;
% Process the variable argument input list
for i = 1:2:numel(varargin)
	switch varargin{i}
		case 'field_names'
			field_count = numel(varargin{i+1});
			field_name = varargin{i+1};
		case 'dimension_list'
			dimension_list = varargin{i+1};
		case 'matrix_type'
			matrix_output = varargin{i+1};
		case 'matrix_singlular'
			singular = varargin{i+1};
		case 'subaddressing'
			subaddressing = varargin{i+1};
		otherwise
			warning('Not a recognized input ... skipping ...');
	end
end

%% Figure out available tuples and store up the outputs.
tuples = [];
filt_addr = [];
outputs = {};
all_count = 0;
within_animal_count = 0;

										num_animals= numel(modf);
for an = 1:num_animals;				num_filters = size(modf(an).epochs,1);
for filt = 1:num_filters;			tot_dayep = size(modf(an).epochs{filt},1);
for dayep = 1:tot_dayep;	%			efizz_sets = size(modf(an).eegdata{filt},2);  %TODO might be able to eliminate this loop level
% for alltetcell = 1:efizz_sets;          
efizz_set_per_ep = size(modf(an).eegdata{filt}{dayep},1);
for tc = 1:efizz_set_per_ep
	
		all_count = all_count + 1;
        
		tuples = [tuples; ...
			an filt modf(an).epochs{filt}(dayep,:) modf(an).eegdata{filt}{dayep}(tc,:)];
		filt_addr = [filt_addr; ...
			tot_dayep dayep efizz_set_per_ep tc]; % TODO check if this is correct
		
		
		within_animal_count = within_animal_count + 1;
		outputs{all_count} = modf(an).output{filt}(within_animal_count);
		
end
end
end
within_animal_count = 0;
end



outputs = cell2mat(outputs);

%% Clear up the memory held by the modf input
clear modf

%% Decide how to collapse the outputs
total_dims = size(tuples,2);
alldim = 1:total_dims;
gather = ismember(alldim,dimension_list);

unique_ungathered = unique( tuples(:,alldim(~gather)), 'rows' );
ungathered = tuples(:,alldim(~gather));

%% Create eval string that will use
finaloutput_str = 'fo';
for i = 1:size(unique_ungathered,2)
	finaloutput_str = [finaloutput_str sprintf('{unique_id(%d)}',i)];
end

set_string = @(x,y) strcat(x, '=', y, ';');

% Create left hand side and right hand side of equalities for the three
% major modes of operation

lhs{1} = '.(field_name{f})(:,j)';
lhs{1} = [finaloutput_str lhs{1}];
rhs{1} = 'outputs(curr_row).(field_name{f})';

lhs{2} = '.(field_name{f})(:,j)';
lhs{2} = [finaloutput_str lhs{2}];
rhs{2} = 'orient_to_row(outputs(curr_row).(field_name{f}))';

lhs{3} = '.(field_name{f}){j}';
lhs{3} = [finaloutput_str lhs{3}];
rhs{3} = 'outputs(curr_row).(field_name{f})';

formatrix = set_string(lhs{1},rhs{1});
forsingular = set_string(lhs{2},rhs{2});
forcell = set_string(lhs{3},rhs{3});

%% Gather!
fo={};
subaddressing_not_initialized = true;
clear w; w=waitbar(0,'Gathering input');
proc_count = 0;
for f = 1:field_count
% 	fo.(field_name{f}) = [];
	
	for i = 1:size(unique_ungathered,1)
		
		% Non-gather,i.e., unique id component, that ought to remain separate
		unique_id = unique_ungathered(i,:);
		
		
		matching_rows = find(ismember(ungathered,unique_ungathered,'rows'));
		for j = 1:numel(matching_rows)
			
			% Address the current matching row
			curr_row = matching_rows(j);
			
			% An important submode allows gathering within the actual indexing
			% structure of each output term. If the user calls for it, here we will
			% modify the lhs and rhs argument terms.
			if subaddressing && subaddressing_not_initialized
				[formatrix,forsingular,forcell,size_object] = ...
					initializeSubAddressing(curr_row,outputs,field_name{f},...
					lhs,rhs);
				subaddressing_not_initialized=false;
			end
			
			% Decide eval str
			if matrix_output
				if singular; eval_str = forsingular;
				else, eval_str = formatrix; end
			else, 
				eval_str = forcell; 
			end
			
			% Decide whether to gather each output element raw or a
			% subaddress within the element ... the subaddressing is
			% limited right now to one layer of cell or matrix.
			if subaddressing
				totalelem = prod(size_object);
				for i = 1:totalelem
					if j == 1; eval([eval_str(1:21) '=[];']); end;
					eval(eval_str);
					proc_count = proc_count+1;
					waitbar( proc_count/...
						(totalelem*numel(matching_rows)*field_count),w);
				end
			else
				eval(eval_str);
				proc_count = proc_count + 1;
				waitbar( proc_count/...
						(numel(matching_rows)*field_count),w);
			end
		end
	end
	
	% Uninitialize sub-addressing ... we have a different field
	subaddressing_not_initialized = true;
end; close(w); clear w;

%% HELPER FUNCTIONS

	function [formatrix,forsingular,forcell,object_size] = ...
			initializeSubAddressing(curr_row,outputs,field_name,lhs,rhs)
		
		persistent lhs_original;
		persistent rhs_original;
		persistent original_stored;
		
		if isempty(original_stored) || ~original_stored
			lhs_original = lhs;
			rhs_original = rhs;
			original_stored = true;
		end
		
		if iscell(outputs(curr_row).(field_name))
			left_bracket = '{';
			right_bracket = '}';
		elseif ismatrix(outputs(curr_row).(field_name))
			left_bracket = '(';
			right_bracket = ')';
		end
		
		object_size = size(outputs(curr_row).(field_name));	
		for elem = 1:numel(lhs)
			lhs{elem} = [lhs_original{elem} left_bracket 'i' right_bracket];
			rhs{elem} = ['[' lhs{elem} '; ' 'orient_to_row(' rhs_original{elem} left_bracket 'i' right_bracket ')]'];
			rhs{elem} = regexprep(rhs{elem},'([\(\{][:]?,?j[\)\}])','');
			lhs{elem} = regexprep(lhs{elem},'([\(\{][:]?,?j[\)\}])','');
		end
		
% 		dimension_count = 0;
% 		lhs=lhs_original; rhs=rhs_original;
% 		while dimension_count < numel(object_size);
% 			for elem = 1:numel(lhs)
% 				lhs = [lhs{elem} ...
% 					left_bracket char(105+dimension_count) right_bracket];
% 				rhs = [rhs{elem} ...
% 					left_bracker char(105+dimension_count) right_bracket];
% 			end
% 			dimension_count = dimension_count + 1;
% 		end
		
		formatrix = set_string(lhs{1},rhs{1});
		forsingular = set_string(lhs{2},rhs{2});
		forcell = set_string(lhs{3},rhs{3});
		
	end

	function out = orient_to_row(x)
		[p,q] = size(x);
		if q > p && p == 1
			out = x';
		else 
			out = reshape(x,[],1);
		end
	end
	
end
