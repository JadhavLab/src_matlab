function plotPointOnTrack(pos, indexPoint)
	
	data = pos.data;
    
    plot( data(:,2),        data(:,3)       );
    hold on;
     plot( data(indexPoint,2), data(indexPoint,3),'o','LineWidth',2,...
    'MarkerSize',10,...
    'MarkerEdgeColor','b',...
    'MarkerFaceColor',[0.5,0.5,0.5]);
    hold off;
	
end