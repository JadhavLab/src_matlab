function [ out ] = DFTF_selectcoherence( animaldir,animalprefix,epochs,varargin )
%DFTF_SELECTCOHERENCE Summary of this function goes here
%   Detailed explanation goes here

%% Parse Varargin
p=inputParser;
p.addParameter('band',[],@(x) isreal(x) && (isequal(size(x),[1 2])) );
p.addParameter('select_what','C',@ischar );
p.addParameter('select_quantile',[],@(x) isreal(x) && (x>=0 && x<=1) );
p.addParameter('select_raw',[],@isreal);
p.parse(varargin{:});
band = p.Results.band;
select_quantile = p.Results.select_quantile;
select_raw = p.Results.select_raw;
select_what = p.Results.select_what;

%% Load up variables
avgcoh = loaddatastruct(animaldir,animalprefix,'avgcoh',unique(epochs(:,1)));

%% Main
for i = 1:size(epochs,1)

    d=epochs(i,1);e=epochs(i,2);
    fprintf('DFTF_selectcoherence d=%d e=%d\n',d,e);
    
    out{d}{e} = avgcoh{d}{e};
    try
        out{d}{e} = rmfield(out{d}{e},{'f','Cavg','Cstderr','Cstd'});
    catch
        warning('Not all fields clearable');
    end
    out{d}{e}.time = out{d}{e}.t';
    
    % Calculate selected band and quantile if requested
    if ~isempty(band)
        
        f_select = out{d}{e}.faxis >= band(1) & out{d}{e}.faxis <= band(2);
        out{d}{e}.band_average = mean(out{d}{e}.(select_what)(:,f_select),2);
        
        if ~isempty(select_quantile)
            quantile_of_interest = quantile(out{d}{e}.band_average,select_quantile);
            out{d}{e}.select_quantile = repmat(quantile_of_interest,size(out{d}{e}.time));
        end
        
        if ~isempty(select_raw)
            out{d}{e}.select_raw = repmat(select_raw,size(out{d}{e}.time));
        end
        
    end
    
    out{d}{e}.faxis = repmat(out{d}{e}.faxis,size(out{d}{e}.C,1),1);
    
end


end

