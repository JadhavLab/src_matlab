function out = ryDFA_coherenceTrialExample(index, excludetimes, pos, linpos, eeg, varargin)

%% Pre-processing
% Call ry_calcoherence to obtain coherence measures then used to calculate
% occ norm measures

in = ryDFA_calccoherence(index,excludetimes,pos,eeg,varargin{:});

pos=pos.data;

% Bring all the structural variables into our function's workspace
for f = fields(in)'

    varname=f{1};
    assign(varname, in.(varname));

end; clear(in);

%% Parse optional input

p = inputParser;
p.addParameter('band',[6 12],@(x) isreal(x) && isequal(size(x),[1 2]));

%% Process

% Basic pos variables
tPos    = pos{s(3)}{s(4)};tLinPos=linpos{s(3)}{s(4)};
t = colof('time',tPos); t = tPos.data(:,t);
x = colof('x-sm',tPos); x = tPos.data(:,x);
y = colof('y-sm',tPos); y = tPos.data(:,y);

% Basic linpos variables
tL = tLinPos.statematrix.time;
xL = tLinPos.statematrix.XY_proj(:,1);
yL = tLinPos.statematrix.XY_proj(:,2);
traj = tLinPos.statematrix.traj;
lindist = tLinPos.statematrix.lindist;

coherence=tbt.C;
trialtime=tbt.t;
for tr = 1:numel(tbt.C)

    % Background - track skeleton
    for graph = graphcount,
        subplot(1,max(graphcount),graph)
        cla; hold off;
        legAxes=[];
        segCoords = tLinPos.segmentInfo.segmentCoords;
        for c = 1:size(segCoords,1)
            legAxes(1)=plot(segCoords(c,[1 3]), segCoords(c,[2 4]),'k-');
            hold on;
        end
    end

    % pull out coherence for trial then cut out range of interest
    trialCoh=coherence{tr};
    trialTime=trialtime{tr};
    trialFreq=tFreq;

    % Acquire pos times for every centroid of time-frequency bank in
    % the coherogram
    time_inds = lookup(trialTime,t);

    t_ofinterest = (t >= min(trialTime) & t <= max(trialTime));
    % Add faint arrow background
    X = x(t_ofinterest); Y = y(t_ofinterest);

    t_ofinterest_Ctimebase = lookup(t_ofinterest,in.t);

    out.x{tr} = X;
    out.y{tr} = Y;
    out.C{tr} =

    %% Plot! -- if option turned on
    if ploton

    end

end

end
