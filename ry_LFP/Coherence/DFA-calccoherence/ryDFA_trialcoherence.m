function out = ryDFA_trialcoherence(index, excludetimes, eeg, varargin)

    fprintf('ry_trialcoherence: day %d ep %d tet1 %d tet2 %d\n',...
	index(1),index(2),index(3),index(4));

%% Parse optional inputs -- all have default values, you don't need to provide
p = inputParser; p.CaseSensitive = false;
p.KeepUnmatched=true;

% Plotting Options
p.addParameter('waitplot',false,@islogical);    % Pause for keyboard input between plots
p.addParameter('ploton',false,@islogical);  % Plot trial by trial?

% Analysis Options
p.addParameter('eqTrials',false,@islogical);    % make sure trial samples equal length?
p.addParameter('zeroTrials',false,@islogical);  % 

% Output options
p.addParameter('animal','',@ischar);            % optional animal name, (affects save directory and is appended to output)
p.addParameter('savedir',pwd,@ischar);          % optional savedir, where things saved (i.e. figures)
p.addParameter('writefiles',0,@(x) isreal(x) && x <= 2);  % Write levels: 0 do not write outputs to files, 1 write cohgramc outputs to animaldef location, 2 write trial-by-trial to data folder

% Parse optional inputs
p.parse(varargin{:});
if ~isempty(fieldnames(p.Unmatched))==true; warning('Unmatched optional parameter'); end;

% Parse out parameters
% -------------------------------------------  
waitplot	= p.Results.waitplot;
ploton      = p.Results.ploton;
eqTrials    = p.Results.eqTrials;
zeroTrials	= p.Results.zeroTrials;
animal      = p.Results.animal;
savedir     = p.Results.savedir;
writefiles  = p.Results.writefiles;
% -------------------------------------------

%% Acquire coherence

out = ryDFA_calccoherence(index, excludetimes, eeg, varargin{:});
t = out.t; C=out.C; 

%% Trial-by-trial separation
% Next, we would like Zone-by-zone, Trial-by-trial (i.e. discontinuous
% periods) versions of the information we've already taken

% First, get the include time matrix
if ~isempty(excludetimes)
    includetimes = [-inf 0; excludetimes];
    includetimes(1:end-1,2) = excludetimes(:,1);
    includetimes(2:end,1) = excludetimes(:,2);
    includetimes(end) = inf;
else
    includetimes=[-inf inf];
end

% Then, we compute coherence and times for each trial! ... note, these
% could be super short if your temporal samples are too small ... be wary!
% Increase your sample sizes if these are too small.
nPeriods = size(includetimes,1);
nTrial = 1;
tbt_coh = {}; tbt_ctime = {};
tbt_speed = {}; tbt_stime = {};
for ii = 1:nPeriods
    
    goodtimes = logical(isIncluded(t,includetimes(ii,:)));
    if sum(goodtimes) <= 1
        continue;
    end
    
    tbt_coh{nTrial} = C(goodtimes,:);
    tbt_ctime{nTrial} = t(goodtimes);
    
% 	if zeroTrials
% 		tbt_ctime{nTrial}=tbt_ctime{nTrial} ... 
% 			- ((tbt_ctime{nTrial}(end)-tbt_ctime{nTrial}(1))/2);
% 	end
    
%     goodtimes = logical(isIncluded(time, includetimes(ii,:)));
%     tbt_speed{nTrial} = speed(goodtimes);
%     tbt_stime{nTrial} = time(goodtimes);
    
% 	if zeroTrials
% 		tbt_stime{nTrial}=tbt_stime{nTrial} ... 
% 			- ((tbt_stime{nTrial}(end)-tbt_stime{nTrial}(1))/2);
% 	end

    nTrial = nTrial + 1;
    
end

%% Equalize trial sizes, if requested
if eqTrials % if option on, then trials are made sure to be about equal in length
    
    % acquire sampling rate
    spectral_samplingrate = mean(diff(t)) + 1e-12;
    
    % threshold durations
    durations = cellfun(@(x) x(end)-x(1), tbt_ctime);
    threshDurations = max(0.5*std(durations),spectral_samplingrate);
    modeDurations = mode(durations);
    indices = durations<(modeDurations-threshDurations) | ...
        durations>(modeDurations+threshDurations);
    if sum(indices)>0
        warning(['ry_calccoherence: %d trials found to be way shorter' ...
            ' than expected .. throwing out']);
        % empty out each that doesn't meet threshold criterion
        [tbt_coh{indices}]      = deal([]);
        [tbt_ctime{indices}]    = deal([]);
        [tbt_speed{indices}]    = deal([]);
        [tbt_stime{indices}]    = deal([]);
    end    
end


%% Plot -- (optional)

% Plot trial-by-trial    
if ploton
    figure(100); clf;
    plotTrialByTrial(tbt_coh,tbt_ctime,f,index,animal,...
        waitplot,fullfile(savedir,animal));
end

%% Output

out.tbt         = struct('C',[],'t',[]);

if nTrial > 1 % if it's more than 1 inclusion period

    % -- TRIAL BY TRIAL COHERENCE --
    out.tbt.C       = tbt_coh;
    out.tbt.t       = tbt_ctime;
end

% Write output to HD for later use inside/outside of filter-framework
if writefiles == 2
   if nTrial>1, writeTBT(out); end;
end

return;

    %----------------------------------------------------------------------
    function tbt_handle = ...
            plotTrialByTrial(tbt_coh,tbt_ctime,f,index,animal,...
            waitplot,savedir)
        
        tbt_handle=[];
        if nargin < 3
			animal = [];
		end;
        if nargin == 7
            savedir=fullfile(savedir,sprintf('Day%d-Ep%d',index(1),index(2)),...
                sprintf('Tet%d-Tet%d',index(3),index(4)));
        end
        for i = 1:numel(tbt_coh)
            if isempty(tbt_ctime{i}),
                warning('Time for trial %d is empty, skipping\n',i);
                continue;
            end
            tbt_handle = imagesc(tbt_ctime{i},f,tbt_coh{i}');
            a=gca; a.YDir='normal';
            tbt_title = sprintf(['SingleTrials, %s, Coherence, day%d, ep%d, tetX%d,' ...
                'tetY%d, Window-Trial %d'],...
			animal, index(1),index(2),index(3),index(4),i);
            title(tbt_title);
            xlabel('Time');
            ylabel('Frequency');
            
            fprintf('Trial %d: Stop-Start = %f, NumPoints = %d\n' ,...
                i, tbt_ctime{i}(end) - tbt_ctime{i}(1), ...
                numel(tbt_ctime{i}));
            
            if waitplot; [~] = input('Press enter to continue ...','s'); end;
            if nargin == 7
                warning('off'); mkdir(savedir); ...
                    mkdir(fullfile(savedir,'fig')); warning('on');
                saveas(gcf,fullfile(savedir,...
                    strrep(a.Title.String,', ','_')),'png');
                saveas(gcf,fullfile(savedir,'fig',...
                    strrep(a.Title.String,', ','_')),'fig');
            end
        end
    end

	%----------------------------------------------------------------------
	function desiredindex =  colof(req, structdat)
    % This looks at the 'fields' field of a struct and decides which column
    % to sample in .data ... this exists because I've seen some of the
    % columns shift around occassionally in the data
		fieldparams = structdat.fields;

		% remove any parentheticals
		[a,b] = regexp(fieldparams,'\(.*\)');

		c=[];
		for i = 1:numel(a)
			c = [c a(i):b(i)];
		end

		fieldparams(c) = [];

		fieldparams = strsplit(fieldparams);

		desiredindex = 0;

		for i = 1:length(fieldparams)
			if strcmp(fieldparams{i},req)
				desiredindex = i;
			end
		end
    end

    function writeTBT(out)
        % Writes trial-by-trial to a local folder outside animaldef folder
        % ... this holds spectral/speed quantities exclusively for the
        % times computed by the timefilter, before calccoherence is called.
        
        filename= sprintf('%strialcoherence%d-%d-%d-%d',animal,index(:));
        
        for c = 1:numel(out.tbt)
            trialcoherence{index(1)}{index(2)}{index(3)}{index(4)}{c}.C{c} = out.tbt.C{c}; %#ok<*AGROW>
            trialcoherence{index(1)}{index(2)}{index(3)}{index(4)}{c}.t{c} = out.tbt.t{c};
            trialcoherence{index(1)}{index(2)}{index(3)}{index(4)}{c}.f = out.f;
            trialcoherence{index(1)}{index(2)}{index(3)}{index(4)}{c}.index = out.index;
            trialcoherence{index(1)}{index(2)}{index(3)}{index(4)}{c}.animal = out.animal;
        end
        if isempty(c) % that is, there were no trials in the inclusion times
            trialcoherence{index(1)}{index(2)}{index(3)}{index(4)} = cell(0,0); %#ok<NASGU>
        end
        
        [~,trialcoherefolder] = datadef('trialcoherence');
        filename=fullfile(savedir,trialcoherefolder,filename);
        fprintf('Saving %s\n',filename);
        warning off; mkdir(fileparts(filename)); warning on;
        save(filename,'trialcoherence','-v6');
        
    end


end