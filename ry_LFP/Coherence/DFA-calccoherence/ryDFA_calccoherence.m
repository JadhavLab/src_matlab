 function out = ryDFA_calccoherence(index, excludetimes, eeg, varargin)

fprintf('ryDFA_calccoherence: day %d ep %d tet1 %d tet2 %d\n',...
	index(1),index(2),index(3),index(4));

%% Parse optional inputs -- all have default values, you don't need to provide
p = inputParser; p.CaseSensitive = false;
p.KeepUnmatched=true;

% Read options
p.addParameter('readHDD',false,@islogical); % if this is turned on, if ry_calccoherence can detect an already computed coherence for this animal,day,epoch,tetrode pair, then it will read it from the harddisk instead of computing it
HDDfiledatatype = 'cgramc'; % What datatype to save and read {animalprefix}{datatype}{day}-{epoch}-{tetrode1}-{tetrode2}

try
    samp_default = eeg{index(1)}{index(2)}{index(3)}.samprate;
catch
    samp_default = 1500;
end

% Chronux
p.addParameter('Fs',samp_default,@isreal);      % Sampling Rate
p.addParameter('fpass',[0 40],@isreal);         % Range of frequencies to band pass
p.addParameter('tapers',[4 7],@isinteger);      % Tapers for spectrogram/coherogram
p.addParameter('err',[2 0.05],@isreal);         % Controls how error calculated
p.addParameter('pad',1,@isinteger);             % Padding for fft (determines smoothing)

% Plotting Options
p.addParameter('waitplot',false,@islogical);    % Pause for keyboard input between plots
p.addParameter('ploton',false,@islogical);  % Plot trial by trial?

% Analysis Options
p.addParameter('zscore',false,@islogical);          % zscpre things?
p.addParameter('eqTrials',false,@islogical);        % make sure trial samples equal length?
p.addParameter('zeroTrials',false,@islogical);      % 
p.addParameter('errorstat',false,@islogical);

% Output options
p.addParameter('appendindex',true,@islogical);  % append d,e,t1,t2 index to output?
p.addParameter('animal','',@ischar);            % optional animal name, (affects save directory and is appended to output)
p.addParameter('savedir',pwd,@ischar);          % optional savedir, where things saved (i.e. figures)
p.addParameter('writefiles',0,@(x) isreal(x) && x <= 2);  % Write levels: 0 do not write outputs to files, 1 write cohgramc outputs to animaldef location, 2 write trial-by-trial to data folder

% Parse optional inputs
p.parse(varargin{:});
if ~isempty(fieldnames(p.Unmatched))==true; warning('Unmatched optional parameter'); end;

%%% Define Default Chronux params
% -------------------------------------------  
params.Fs       = p.Results.Fs;
params.fpass    = p.Results.fpass;
params.tapers   = p.Results.tapers; %[5 2/5 0]; % [time_bandwidth_prod, taper_count] .. or [bandwidth, time, taper-reduction]
params.err      = p.Results.err;
params.pad      = p.Results.pad;
% Print sanity check
% if exist('structstruct.m','file')
% 	% Print parameters to screen if pretty struct print is in path
%     structstruct(params);
% end

%%% Define Other Params
% -------------------------------------------  
waitplot	= p.Results.waitplot;
ploton      = p.Results.ploton;
zscore      = p.Results.zscore;
errorstat   = p.Results.errorstat;
eqTrials    = p.Results.eqTrials;
zeroTrials	= p.Results.zeroTrials;
appendindex	= p.Results.appendindex;
animal      = p.Results.animal;
savedir     = p.Results.savedir;
writefiles  = p.Results.writefiles;
readHDD     = p.Results.readHDD;

%% Compute Coherence

% Get directory where presumable harddrive file would be stored
try
    ainfo    = animaldef(animal);
catch
    ainfo{2} = datadef;
end
presumable_HDD_file = fullfile( ainfo{2}, 'chronux', sprintf('%s%s%02d-%02d-%02d-%02d.mat',animal,'cgramc',index(1:4)) );

% Acquire chronux-mediated coherence
if readHDD && exist(presumable_HDD_file,'file') % File-read mode
    
    fprintf('\nLoading...');
    out=load(presumable_HDD_file);
    out = out.cgramc{index(1)}{index(2)}{index(3)}{index(4)};
    
else % Compute mode
    
    fprintf('\nComputing...');
    
    %% Define a moving window 

    movingwin = getMovingwin( range(params.fpass), params);

    %% Pull & Equalize EEG Time Bases

    % assign a temporary variable for eeg
    e1 = eeg{index(1)}{index(2)}{index(3)};
    e2 = eeg{index(1)}{index(2)}{index(4)};

    e1start = e1.starttime;

    e1times = geteegtimes(e1);
    e2times = geteegtimes(e2);

    if length(e1times)>length(e2times)
        temp = lookup(e2times,e1times);
        e1 = e1.data(temp);
        e2 = e2.data;
    elseif length(e2times)>length(e1times)
        temp = lookup(e1times,e2times);
        e1 = e1.data;
        e2 = e2.data(temp);
    elseif length(e1times)==length(e2times)
        e1 = e1.data;
        e2 = e2.data;
    end
    
    %% Chronux
    if errorstat
        [C,phi,S12,S1,S2,t,f,confC,phistd,Cerr]=cohgramc(e1,e2,movingwin, params);
    else
        [C,phi,S12,S1,S2,t,f]=cohgramc(e1,e2,movingwin, params);
    end
    
    %% Post-processing
    % profile viewer;
    t=t+e1start; % add eeg time start offset back in

    % Obtain z-scoring variables
    power_mean = mean(C,1);
    power_std = std(C,1);

    % Z-score!
    if zscore
        C = bsxfun(@minus,C,power_mean);
        C = bsxfun(@rdivide,C,power_std);
    end

    %% Output
    % FREQUENCIES AXIS FOR SPECTROCOHEROGRAMS
    out.f = f;

    % -- COHERENCE / SPECTROGRAMS ALL --
    out.C           = C;
    out.phi         = phi;
    out.S1          = S1;
    out.S2          = S2;
    out.t           = t;
    
    % Save our parameters!
    params.movingwin = movingwin;
    out.params      = params;
    
    % If user requested statistical measures of error, generate
    if errorstat
        out.confC       = confC;
        out.phistd      = phistd;
        out.Cerr        = Cerr;
    end    

    % -- INDICES and ANIMAL INFO (Helps More Quickly Process Downstream) --
    if (appendindex)
        out.index       = index;
        out.animal      = animal;
    end
    if (exist('animal','var') && ~isempty(animal))
        out.animal      =animal;
    end
    
end

%% Inclusion Specific Output
% This section can be expanded with even more outputs .. we're not yet
% doing trial-by-trial for spectrograms here, nor returning phase-locking
% or error measures

% -- COHERENCE / SPECTROGRAMS INCLUDE PERIODS --
out.include     = struct('C',[],'t',[]);

if ~isempty(excludetimes) % if it's more than 1 inclusion period
    
    goodtimes = ~isExcluded(t, excludetimes);
    
    C_include = C(goodtimes,:);
    time_include = t(goodtimes);
    
    out.include.C = C_include;
    out.include.t = time_include;
end

%% Write to HDD
% Write output to HD for later use inside/outside of filter-framework
if writefiles>=1 && ~(readHDD && exist(presumable_HDD_file,'file'))
    writeCohgramc(out);
end

%% Plot
if ploton
    plotInclusionData(out);
end

return;

	%% HELPER FUNCTIONS
    %----------------------------------------------------------------------    
    function plotInclusionData(out)
        fig=figure(150);
        if ~isempty(out.include.t)
            imagesc(out.include.t,out.f,out.include.C');
        else
            imagesc(out.t,out.f,out.C');
        end
        set(gca,'ydir','normal');
        titlestr=sprintf('Coherence-%s-%02d-%02d-%02d-%02d',animal,index(:));
        title(titlestr);
        xlabel('Time (Seconds of Sequentially Included Time)');ylabel('Frequency');
        saveThis(fig,savedir,titlestr,{'png','fig'});
    end
    %----------------------------------------------------------------------    
    function movingwin = getMovingwin(fpassrange,params)
    % Just like the commented out routine below, the time*freq product
    % is kept constant and produces the same results as the above taken
    % from other code in our database; this is more flexible because it
    % works for any inputted frequency.

        if numel(params.tapers) == 2
            lengthmovwin = 40/fpassrange;
            stepsize = lengthmovwin/10;
            movingwin = [lengthmovwin stepsize];
        end
        if numel(params.tapers) == 3
            movingwin = [tapers(2) tapers(2)/10];
        end

    end
	%----------------------------------------------------------------------
    function writeCohgramc(out)
        % Function to store processed data for later use
        
        filename = sprintf(['%s' HDDfiledatatype '%02d-%02d-%02d-%02d'],animal,index(:));
        
        cgramc{index(1)}{index(2)}{index(3)}{index(4)} = rmfield(out,'include');
        
        try
            animalinfo = animaldef(animal);
        catch
            animalinfo{2} = datadef;
        end
        
        filename = fullfile(animalinfo{2},'chronux',filename);
        fprintf('Saving %s\n',filename);
        warning off; mkdir(fileparts(filename)); warning on;
        
        try
            save(filename,HDDfiledatatype,'-v6');
        catch
            save(filename,HDDfiledatatype);
        end
        
    end
end