function out = ryDFA_trajcoherence(index,excludetimes,trajinfo,eeg,varargin)
% This function takes the output from a trial-based coherence function and
% adds a bunch of trajectory related information to it. This is so that the
% downstream gather functions can have access to the trajectory style
% information.

% Obtain output from ry_calccoherence
warning off;
out = ryDFA_trialcoherence(index,excludetimes,eeg,varargin{:});
warning on;

% Ready trajinfo
trajinfo=trajinfo{index(1)}{index(2)};
trajtimes=trajinfo.trajtime;

% Iterate through trajinfo and add meta data to the trajectories coded in
% ry_calccoherence
for i = 1:numel(out.tbt.t)
    
    traj_num = which_traj(out.tbt.t{i},trajtimes);
    out.tbt.rewarded(i)=trajinfo.rewarded(traj_num);
    out.tbt.trajbound(i)=trajinfo.trajbound(traj_num);
    out.tbt.trajnum(i) = traj_num;
    
end

    function traj_num = which_traj(period,trajtimes)
        % Simple helper function that spits out which trajectory an
        % inclusion period belongs to. Also handles ambiguity, by either
        % throwing a warning or an error or by selecting the best match.
        %
        %  For right now, as long as each include period is inside or equal
        %  to the boundaries of a trajectory, it will be found. Else, it
        %  will error out. Eventually I can include partial matching.
        %
        % Eventually will include three different modes can be toggled by
        % ambigous_traj flag to ryDFA_caltrajcoherence
        
        if iscolumn(period);period=period';end;
        
        period=repmat( [period(1) period(end)], size(trajtimes,1),1);
        after = period(:,1) >= trajtimes(:,1);
        before  = period(:,end) <= trajtimes(:,2);
        
        match = after.*before;
%         match=cumprod(match,1);
        
        traj_num = find(match);
    end
end