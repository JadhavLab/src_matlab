function [ out ] = ryDFA_avgcoherence(index,excludetimes,animal,avgcoh,varargin)
% [ out ] = ryDFA_avgcoherence(index,excludetimes,avg_coh,varargin)
%
% This function relies on having avg_coh files in your animaldirectory,
% which are created by the filter script DFS_CreateAvgCgramcFiles. These
% files are averages over all relevant tetrode pairs, which has the effect
% of getting rid of fast varying local noise, indigenous to each pair.

fprintf('ry_avgcoherence: day %d ep %d\n',...
	index(1:2));

p=inputParser; p.KeepUnmatched=true;
% p.addParameter('animal','',@ischar);
p.addParameter('ploton',false,@islogical);
p.addParameter('savedir',pwd,@ischar);
p.parse(varargin{:});
% animal=p.Results.animal;
ploton=p.Results.ploton;
savedir=p.Results.savedir;


out = avgcoh{index(1)}{index(2)};
if isrow(out.t); out.t=out.t'; end;

if ~isempty(excludetimes)
    
    times = out.t;
    goodtimes = ~isExcluded(times, excludetimes);
    
    % Apply exlcude times to all fields
    for f = fields(out)'
        f=f{1};
        if size(out.(f),1) == numel(times)
            out.(f) = out.(f)(goodtimes,:);
        end
    end
    
end

if ploton
    plotInclusionData(out);
end

	%% HELPER FUNCTIONS
    %----------------------------------------------------------------------    
    function plotInclusionData(out)
        figure(150);
        imagesc(out.t,out.faxis,out.C');
        set(gca,'ydir','normal');
        titlestr=sprintf('Coherence-%s-%02d-%02d',animal,index(:));
        title(titlestr);
        xlabel('Time (Seconds of Sequentially Included Time)');ylabel('Frequency');
        saveThis(gcf,savedir,titlestr,'png');
    end

end