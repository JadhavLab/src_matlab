function out =  DFA_triggerCoherence(idx,excludeperiods,animalprefix,avgcoh,varargin)
% Collects triggered picutres of coherence, when a certain band goes either
% high or low, it collects a certain window around that event.

%% Parse optional input
ip=inputParser;
ip.addParameter('q_trigger',0.8); % if positive, takes an upper quantile, negative a lower; zero creates a special behavior, where the triggers are random samples
ip.addParameter('f_trigger',[6 12]);
ip.addParameter('window',[1 1]);
ip.addParameter('expansion_format',false);
ip.parse(varargin{:});

q_trigger=ip.Results.q_trigger;
f_trigger=ip.Results.f_trigger;
window=ip.Results.window;
expansion_format=ip.Results.expansion_format;

persistent diff_t;


%% Gather some preliminary stuff

% sampling period
if isempty(diff_t)
    diff_t = median( diff(avgcoh{idx(1)}{idx(2)}.t) );
end

% time excluded view of global coherence
avgcoh = exclude( avgcoh{idx(1)}{idx(2)}, excludeperiods, 't' );

% figure out the trigger crosspoints
f = avgcoh.faxis >= f_trigger(1) & avgcoh.faxis <= f_trigger(2);
crosspoints = find(triggers(mean( avgcoh.C(:,f) ,2 ),q_trigger));

%% Collect triggered views
t=avgcoh.t;
Xq = -window(1) : diff_t : window(2);
Yq = avgcoh.faxis;
[Xq,Yq] = meshgrid( Xq, Yq );
trigger_views = nan( numel(crosspoints), size(Xq,2), size(Yq,1) );
for loc = 1:numel(crosspoints)
    
    cp = crosspoints(loc);
    
    slice = t >= t(cp)-window(1)*3 & t <= t(cp)+window(2)*3;
    X = t(slice);
    X = X - median(X);
    Y = avgcoh.faxis;
    [X,Y]=meshgrid(X,Y);
    C = avgcoh.C( slice , : );
    trigger_views(loc,:,:) = interp2( X , Y , C' , Xq , Yq )';
    
end

%% Store!
runICA = true;
if expansion_format
    [out.t,out.f,out.trig] = meshgrid(Xq(1,:)',Yq(:,1),[1:numel(crosspoints)]');
    out.t =  reshape( out.t , [], 1);
    out.f =  reshape( out.f , [], 1);
    out.trig =  reshape( out.trig , [], 1);
    out.C = reshape( permute(trigger_views, [2 3 1] ) , [], 1);
    out = describe(out);
    out.day=repmat(out.day,numel(out.C),1);
    out.epoch=repmat(out.epoch,numel(out.C),1);
    out.animal=repmat(out.animal,numel(out.C),1);
else
    out.t = Xq(1,:)';
    out.f = Yq(:,1);
    out.trig = [1:numel(crosspoints)]';
    out.C = permute(trigger_views, [2 3 1] )
    out.Cavg = mean(out.C,3,'omitnan');
%     if runICA
%         [out.Cica,out.A,out.Z] = ICAcomponents(out.C,out.f,4);
%     end
    out = describe(out);
end

%% Helper functions

    function out=describe(out)
        % Finalizes data by describing it (useful at post-process phase)
        out.animal = animalprefix;
        out.day=idx(1);
        out.epoch=idx(2);
    end

    function out = exclude(obj,exclusion,c)
        % Excludes time points of all fields
        length_c = length(obj.(c));
        goodtimes = ~isExcluded(obj.(c),exclusion);
        
        for f = fields(obj)', f=f{1};
            if length( obj.(f) ) == length_c
                try out.(f) = obj.(f)(goodtimes,:); catch; out.(f) = obj.(f)(goodtimes); end;
            else
                out.(f) = obj.(f);
            end
        end
    end
    
    function crosspoints = triggers(meanC, q_trig)
        % Obtain trigger times
        if q_trig>0
            thresh = meanC > quantile(meanC,q_trig);
            crosspoints = [false; diff(thresh) == 1];
        elseif q_trig < 0
            thresh = meanC > quantile(meanC,q_trig);
            crosspoints = [false; diff(thresh) == -1];
        else
            crosspoints = randperm(numel(meanC));
            crosspoints = sort(crosspoints(1:2000));
        end
    end

    function [Cica,A,Z] = ICAcomponents(C,f,window,components)
        % Finds the ICA components of the different trigger samples
        
        if ~exist('components','var'); components=4; end;
        
        originaldim = size(C);
        C=reshape(C,[],originaldim(end))';
        t_this = out.t;
        
        [~,y_idx]=find(isnan(C));
        y_idx=unique(y_idx);
        y_idx = setdiff( 1:size(C,2) , y_idx );
        
        [Cica,A,Z] = fastica(C(:,y_idx),'numOfIC',components+1,'firstEig',1,'lastEig',components);
        Cica = reshape(Cica, size(Cica,1), [], originaldim(2) );
        
        ploton=true;
        if ploton
            for i = 1:size(Cica,1)                                                                                              
                subplot(round(sqrt(size(Cica,1))),ceil(sqrt(size(Cica,1))),i)                                                       
                imagesc( linspace(-window(1),window(2),size(Cica,2)) , f, abs(log(squeeze(Cica(i,:,:))))' ); set(gca,'ydir','normal'); pause(1);
                xlabel('Time');ylabel('Coherent Hz');                                                                                 
            end
            
            folder=fullfile(pwd,'PCA');
            mkdir(folder);
            pushd(folder);
            saveThis(gcf,pwd,sprintf('PCA %s%02d%02d',animalprefix,idx(1:2)),{'fig','png'});
            popd;
        end
    end

       

end