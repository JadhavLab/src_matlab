function out = ryDFA_correlate_measure2coherence(index,excludetimes,pos,eeg, varargin)
% This function collects coherence in a band at a time, and can then
% be used to check behavioral periods with certain properties.

norm = @(x) x/max(x);

%% Run calccohere
warning off;
out=ryDFA_calccoherence(index,excludetimes,pos,eeg,varargin{:});
warning on;
f=out.f;C=out.C;t=out.t;

%% Read optional/required varargin inputs
p = inputParser;
p.KeepUnmatched=true;
p.CaseSensitive=false;

% Plot options
p.addParameter('plotcorr',false,@islogical);
p.addParameter('plottitle','',@ischar);
% Which frequencies to examine
p.addParameter('band',[],@isreal);
p.addParameter('band2',[],@isreal);
% Behavior correlate?
p.addParameter('beh','',@ischar);
p.addParameter('beh_fields',{{}});
% Optional info passed in by FF1 iterator
p.addParameter('animal','',@ischar);

p.parse(varargin{:});
plotcorr=[];plottitle='';beh='';beh_fields={};band=[];band2=[];animal='';
try; for e = fields(p.Results)'; e=e{1}; assign(e,p.Results.(e)); end; catch; end;

%% First, derive the coherence measures

% Get the first band of activity
f1 = f >= band(1) & f <= band(2);
C1 = mean(C(:,f1),2);

% Get second band, if requested ...
if exist('band2','var')
    f2 = f >= band2(1) & f <= band2(2);
    C2 = mean(C(:,f2),2);
    
    % Calculate ratio
    C12ratio = C1./C2;
end

%% Second, simply isolate and correlctly time-base requested behavioral measures
% This can isolate things from pos, linpos, or any other behavioral
% structure to check the coherence against.

dyz=num2str(index(1));
if numel(dyz)==1
    dyz=['0' dyz];
end
ainfo=animaldef(animal);
[pathstr,beh]=fileparts(beh)
data=load(fullfile(ainfo{2},pathstr,[animal beh dyz]));
beh=data.(beh);

measures=cell(1,numel(beh_fields));
for b = 1:numel(beh_fields)
    
    one_measure_location = beh_fields{b};
    measures{b} = beh{index(1)}{index(2)};
    try mtime = measures{b}.time; catch; end;
    
    for loc = one_measure_location; loc=loc{1}; %#ok<FXSET>
        
        measures{b} = measures{b}.(loc);
        if ~iscell(measures{b})
            goodtimes=lookup(t,mtime);
            measures{b}=measures{b}(goodtimes);
        end
        
    end
    
end

%% Check derives measures against coherence

for b = 1:numel(measures)    
    
    fprintf('\n-------\n%s',cat(2,beh_fields{b}{:}));
    
%     figure;
    fprintf('\nBand %d-%d\n',band(1:2));
    checkMeasure(C1,measures{b});
    if exist('C2','var')
%         figure;
        fprintf('\nBand %d-%d\n',band2(1:2));
        checkMeasure(C2,measures{b});
%         figure;
        fprintf('\nBand(%d-%d):Band(%d-%d)\n',band(1:2),band2(1:2));
        checkMeasure(C12ratio,measures{b});
    end
    
    fprintf('-------\n');
    
end

%% Helper functions
    function checkMeasure(C,M)


        CM = [C, M];

        % Correlate time lag 0
        [c,pval] = corrcoef(C,M);
        fprintf('pearson''s r=%2.8f, pval=%1.10e\n',c(1,2),pval(1,2));

        % Run cross correlation
        [r,lags]=xcorr(C,M);
        [maxr,ind]=max(r);
        fprintf('Best r=%2.8f, best sequence lag = %d\n',maxr,lags(ind));
        
        % Run corrcoeff on lagged
        [c,pval]=corrcoef(C,sequenceShift(M,lags(ind)));
        fprintf('W/LAG->pearson''s r=%2.8f, pval=%1.10e\n',c(1,2),pval(1,2))
        
        % Run mutualinfo on C and M
        mi = mutualinfo(C,M);
        [mi_rand,mi_std]=randMutualInfo(C,100);
        fprintf('MutualInformation = %1.7e, versus randomMI=%1.7e\n',mi,mi_rand);
        fprintf('Stds above rand = %1.7e\n',(mi-mi_rand)/mi_std);
        
%         % Plot
%         subplot(2,2,[1]);
%         plot(norm(C));hold on;
%         plot(norm(M));
%         legend('C','M');
%         % Plot
%         subplot(2,2,[2]);
%         plot(lags,norm(r)); hold on;
%         plot(lags,xcorr(rand(size(C)),rand(size(C))),'r:');
%         axis([-1000 1000 -inf inf]);
%         % Plot
%         subplot(2,2,3);
%         sortrows(CM,1);
%         scatter(CM(:,2),abs(CM(:,1)),'c','filled');
%         title('Sort by coherence');xlabel('M');xlabel('C');
%         alpha(0.25);
%         % Plot
%         subplot(2,2,4);
%         sortrows(CM,2);
%         scatter(CM(:,1),abs(CM(:,2)),'m','filled');
%         title('Sort by measure'); xlabel('C');xlabel('M');
%         alpha(0.25);
%         
%         drawnow;


    end

    function S = sequenceShift(S,lag)
        if lag>0
            S=S(1+lag:end);S(end+1:end+lag)=0;
        elseif lag<0
            n=numel(S);
            S=[zeros(lag,1); S];
            S=S(1:n);
        end
    end

    function [m,stdev] = randMutualInfo(C,n)
        randMI=zeros(n,1);
        sizeC=size(C);
        for i = 1:n
            randMI(i)=mutualinfo(rand(sizeC),rand(sizeC));
        end
        m=mean(randMI);
        stdev = std(randMI);
    end

end