function out = ryDFA_occupancyCoherence(index, excludetimes, pos, linpos, eeg, varargin)

%% Pre-processing
% Call ry_calcoherence to obtain coherence measures then used to calculate
% occ norm measures

in = ryDFA_calccoherence(index,excludetimes,pos,eeg,varargin{:});

pos=pos.data;

% Bring all the structural variables into our function's workspace
for f = fields(in)'

    varname=f{1};
    assign(varname, in.(varname));

end; clear(in);

%% Parse optional input

p = inputParser;
p.addParameter('band',[6 12],@(x) isreal(x) && isequal(size(x),[1 2]));

%% Collect pos!
% Collect C-value at all x,y positions
X_coll = []; Y_coll = []; C_coll = [];
fprintf('Collecting pos%d%d trial ',index(1),index(2));
for i = 1:numel(trial_times)

    % Select start and end times
    t = tbt_coherence_time{i};

    % Get positions and lookup times
    fprintf(' %d',i);
    pos_inds = lookup(t,pos(:,1));

    % Store for all
    X_coll = [X_coll; pos(pos_inds,2)];
    Y_coll = [Y_coll; pos(pos_inds,3)];
    C_coll = [C_coll; in.C(pos_inds,:)];

end
fprintf('\n');

[out.pos.occmeas, out.pos.N] = ...
    computeOccNorm(X_coll,Y_coll,C_coll,f,band);


%% Collect linpos!
% Collect C-value at all x,y positions
linpos_proj = linpos.statematrix.XY_proj;
lindist=linpos.statematrix.lindist;
traj=linpos.statematrix.traj;
X_coll = []; Y_coll = [];
lindist_coll = []; traj_coll = [];

fprintf('Collecting linpos%d%d trial ',index(1),index(2));
for i = 1:numel(trial_times)

    % Select start and end times
    t = tbt_coherence_time{i};

    % Get positions and lookup times
    fprintf(' %d',i);
    pos_inds = lookup(t,linpos.statematrix.time);

    % Store for all
    X_coll = [X_coll; linpos_proj(pos_inds,1)];
    Y_coll = [Y_coll; linpos_proj(pos_inds,2)];

end
fprintf('\n');

[out.linpos.occmeas, out.linpos.N] = ...
    computeOccNorm(X_coll,Y_coll,C_coll,f,band,'mask',false);


%% Plot! -- if option turned on

if ploton
end

end
