function out = DFAsj_getcellinfo2(index, excludetimes, cellinfo, varargin);
% out = DFAsj_getcellinfo(index, excludetimes, cellinfo, varargin);
% Returns cell properties from cellinfo field
% Don't do csi and propbursts, since Ndl doesnt seem to have these
% Options:
%   'appendindex', 1 or 0 -- set to 1 to append the cell index to the
%   output [tetrode cell value].  Default 0.
%

% ver 6 - also return cell area to identify iCA1 cells

appendindex = 1;
for option = 1:2:length(varargin)-1   
    if isstr(varargin{option})       
        switch(varargin{option})
            case 'appendindex'
                appendindex = varargin{option+1};
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end        
    else
        error('Options must be strings, followed by the variable');
    end
end


info = cellinfo{index(1)}{index(2)}{index(3)}{index(4)};

is_iCA1 = strcmp(info.area,'iCA1');

out.index = index;
out.meanrate=info.meanrate; % All values for epoch
out.spikewidth=info.spikewidth;
out.area = info.area;
out.is_iCA1 = is_iCA1;

% try
%     out.csi=info.csi;
% catch
%     index, keyboard;
% end
% out.propbursts=info.propbursts;    
out.numspikes=info.numspikes;
%out.tag=info.tag;




% if (appendindex)
%     out = [index rate]; %append the cell index to the value
% else
%     out = rate;
% end