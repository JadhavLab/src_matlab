function out = sj_calcpopulationlinfields(indices, excludeperiods, spikes, linpos, binsize, peakthresh)
%trajdata = FILTERCALCLINFIELDS(index, excludeperiods, spikes, linpos)
%trajdata = FILTERCALCLINFIELDS(index, excludeperiods, spikes, linpos, binsize)
%
%Calculates the linear occupancy normalized firing rate for all the cells.
%
%spikes - the 'spikes' cell array for the day you are analyzing
%linpos - the output of LINEARDAYPROCESS for the day you are analyzing.
%indices - [day epoch tetrode cell]
%binsize- the length of each spatial bin (default 2cm)
%excludeperiods - [start end] times for each exlcude period
%
%The output is a structure.


warning('OFF','MATLAB:divideByZero');
if (nargin < 5)
    binsize = 2;
end

%Define linear position information
index = [indices(1,1) indices(1,2)];
statematrix = linpos{index(1)}{index(2)}.statematrix;
wellSegmentInfo = linpos{index(1)}{index(2)}.wellSegmentInfo;
segmentInfo = linpos{index(1)}{index(2)}.segmentInfo;
trajwells = linpos{index(1)}{index(2)}.trajwells; % [1 2; 1 3]  basis is 2 trajectories
statevector = statematrix.traj; % Traj the animal is on, this goes from 1 to 4 for 4 trajectories
lindist = statematrix.lindist;  % This is distance from Center Well
lindist = gaussSmooth(lindist, 30);

%Apply excludeperiods to find valid trajectories
if iscell(statevector)
    statevector = statevector{6};
end
statevector(find(isExcluded(statematrix.time, excludeperiods))) = -1;

%calculate information about the track and the behavior
for i = 1:size(trajwells,1) % 2 main trajec - right and left
    %calculate the linear length of the trajectory
    trajlength(i) = sum(segmentInfo.segmentLength(wellSegmentInfo.pathTable{trajwells(i,1),wellSegmentInfo.segmentIndex(trajwells(i,2))}));
    % Trajlength has 2 entries
end

try
    %Define the time, distance, and trajectory for all good times.
    goodlocationind = (find(statevector ~= -1 ));
    goodlocations = [statematrix.time(goodlocationind) lindist(goodlocationind) statevector(goodlocationind)];
catch
    keyboard;
end

% Initialize output
out.traj = [];
out.dist = [];
out.homesegmentlength = [];
out.index = [];
out.rates = [];
out.posprob = [];
out.cellpeakpos = []; % peak posn on all 4 trajectories for all valid cells

% SJ
trajnum = max(statevector);
out.traj_sep = [];
out.dist_sep = [];
for i = 1:trajnum
    storepeak{i} = [];
    storepeakpos{i} = [];
    cellcnt{i} = 0;
    out.index_sep{i} = [];
    out.rates_sep{i} = [];
    out.cellSequence{i} = [];
    out.Ncellsintraj{i} = [];
    out.cellpeakpos_sep{i} = []; % Separted by trajectory, and arranged in order of peaks to align with out.index_sep
end

%go through each cell and calculate the linearized rates
for cellcount = 1:size(indices,1)
    
    index = indices(cellcount,:);
    cellspikes = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data;
    
    timestep = statematrix.time(2,1) - statematrix.time(1,1);
    
    posindexfield = 7;
    if ~isempty(cellspikes)
        cellspikes = cellspikes(:,[1 posindexfield]);
        %Add the trajectory for each spike
        cellspikes(:,3) = statevector(cellspikes(:,2));
        %Add the linear distance for each spike
        cellspikes(:,4) = lindist(cellspikes(:,2));
    else
        cellspikes = [0 0 -1 0];
    end
    
    goodspikes = [];
    goodspikeind = (cellspikes(:,3) ~= -1);
    goodspikes = cellspikes(goodspikeind,:);
    
    tmprates = [];
    tmptraj = [];
    tmprates = [];
    tmpdist = [];
    tmpocc = [];
    tmppeakpos = [];
    tmppeak = [];
    
    for i = 1:trajnum
        
        %get all the linear locations when the animal was on the ith
        %trajectory
        tmplinloc = goodlocations(find(goodlocations(:,3) == i),2);
        tmpspikes = goodspikes(find(goodspikes(:,3) == i),:);
        if ~isempty(tmplinloc)
            findtrajnum = (i+rem(i,2))/2;   % Map the 4 trajectories on to 2 main ones for length
            minloc = 0;
            maxloc = trajlength(findtrajnum);
            
            tmpvec = [minloc:binsize:maxloc];
            binvector = zeros(length(tmpvec),5);
            %the 1st column of binvector is the location for that bin
            binvector(:,1) = tmpvec(1:end);
            %find which bins the linear locations and spikes fall into
            binind = lookup(tmplinloc,binvector(:,1));
            if ~isempty(tmpspikes)
                spikebinind = lookup(tmpspikes(:,4),binvector(:,1));
            else
                spikebinind = [];
            end
            %sum up the occupancy and spikes in each bin
            for j = 1:size(binvector,1)
                %the 2nd column of binvector is occupancy
                binvector(j,2) = sum(binind == j) * timestep;
                %the 3rd column of binvector is the spike count
                binvector(j,3) = sum(spikebinind == j);
            end
            %the 4th column of binvector is occ-normalized firing rate
            binvector(:,4) = binvector(:,3)./binvector(:,2);
            nonfinite = find(~isfinite(binvector(:,4)));
            %the 6th column of binvector is the smoothed occupancy
            binvector(:,6) = gaussSmooth(binvector(:,2),2);
            %the 7th column of binvector is the smoothed spike count
            binvector(:,7) = gaussSmooth(binvector(:,3),2);
            %the 5th column of binvector is the smoothed occ-normalized
            %firing rate (with the baseline rate added)
            binvector(:,5) = (binvector(:,7)./binvector(:,6))+.05;
            %Exclued bins with very low occupancy and replace with NaN
            lowocc = find(binvector(:,6) < binsize*.1);
            binvector(nonfinite,4) = nan;
            binvector(lowocc,5) = nan;
            
            tmprates = [tmprates binvector(:,5)'];
            tmptraj = [tmptraj ones(1,size(binvector,1))*i];
            tmpdist = [tmpdist binvector(:,1)'];
            tmpocc = [tmpocc binvector(:,6)'];
            
            % SJ
            currrates = binvector(:,5)';
            [currpeak, currpeakpos] = max(currrates);
            currpeak = currpeak(1); currpeakpos = currpeakpos(1);
            tmppeak = [tmppeak currpeak]; tmppeakpos = [tmppeakpos currpeakpos];
            %trajdata{i} = binvector;
            
            if currpeak >= peakthresh % For current trajectory
                cellcnt{i} = cellcnt{i}+1;
                out.dist_sep{i} = binvector(:,1)';
                out.traj_sep{i} = ones(1,size(binvector,1))*i; % This is not really necessary. Just what trajecotry it is
                out.rates_sep{i} = [out.rates_sep{i}; currrates];
                out.index_sep{i} = [out.index_sep{i}; index]; % Curr cell in sequence for curr trajectory
                storepeakpos{i} = [storepeakpos{i}; currpeakpos];
                storepeak{i} = [storepeak{i}; currpeak];
            end
            
        end % end isempty
    end % end trajnum
    
    % This looks across all 4 trajectories as in original
    if max(tmprates >= peakthresh)
        out.dist = tmpdist; % All 4 traj compressed in a row vector. Need only once for each trajectory
        out.traj = tmptraj; % All 4 traj compressed in a row vector
        out.homesegmentlength = segmentInfo.segmentLength(1); % Central segment, If location of peak is beyond segment, then replay includes a side trajectory
        % WHAT IS THE GOAL OF THE POSPROB FIELD?
        out.posprob = tmpocc/noNanSum(tmpocc);
        out.rates = [out.rates;tmprates]; % All 4 traj compressed , and rows are different cells
        out.index = [out.index; index]; % index of cells
        % SJ - also store the peak position of the cell - aligned with its index. To quickly check is its >homesegmentLength
        out.cellpeakpos = [out.cellpeakpos; tmppeakpos]; % This gives index of peakpos for cells in out.index. multiply by binzise to length of peak
                                                         % DONT index into dist, Index into dist_sep 
   
    end
    
end % end cell

% SJ- get order of spiking of cells in each of the 4 trajectories
for i = 1:trajnum
    if ~isempty(storepeakpos{i})
        [~,sortpeak] = sort(storepeakpos{i});
        cellsi = out.index_sep{i}; ratesi = out.rates_sep{i};
        out.cellSequence{i} = cellsi(sortpeak',:);
        % Also resort the rates of the cells for the trajectory according to this order: ready to plot
        out.index_sep{i} = cellsi(sortpeak',:);
        ratesi = ratesi(sortpeak',:);
        out.rates_sep{i} = ratesi;
        out.Ncellsintraj{i} = cellcnt{i};       
        out.cellpeakpos_sep{i} = storepeakpos{i}(sortpeak');
    end
end


% Test by plotting
% traj=1;
% ratesi = out.rates_sep{traj}; ncells = size(ratesi,1);
% xax = out.dist_sep{traj}+1;
% clr = {'b','m','g','y','c','k','r','b','g','y','b','m','g','y','c','k','r','b','g','y','b','m','g','y','c','k','r','b','g','y'};
% %Define color for replay plot
% color = zeros(19,3);
% color(1:12,1) = [0.2 0.4 0.6 0.8 1 1 1 1 1 1 1 0.2];
% color(6:15,2) = [0.2 0.4 0.6 0.8 1 1 1 1 0.8 0.2];
% color(11:19,3) = [0.2 0.8 1 1 1 1 0.8 0.6 0.4];
% color = color(end:-1:1,:);
% color(20:25,3) = 0;
% 
% figure; hold on;
% for c = 1:ncells
%     %subplot(ncells,1,i); hold on;  
%     %plot(xax,ratesi(c,:),[clr{i} '.-'],'Linewidth',2);
%     
%     % Norm rates
%     plot(xax,c*0.5+ratesi(c,:)./max(ratesi(c,:)),'color',color(c,:),'LineWidth',2);
%     % Or raw rates
%     %plot(xax,c*1+ratesi(c,:),'color',color(c,:),'LineWidth',2);
% end

warning('ON','MATLAB:divideByZero');









function  out = gaussSmooth(vector, binrange)

paddinglength = round(binrange*2.5);
padding = ones(paddinglength,1);

out = smoothvect([padding*vector(1) ; vector; padding*vector(end)],gaussian(binrange,binrange*7));
out = out(paddinglength+1:end-paddinglength);



