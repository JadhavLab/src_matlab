function p = lowerparams(obj)
%LOWERPARAMS Parameters of generalized Pareto distribution in lower tail.
%   PARAMS=LOWERPARAMS(OBJ) returns the vector PARAMS of parameters of the
%   generalized Pareto distribution for the lower tail of the
%   PARETOTAILS object OBJ.
%
%   See also PARETOTAILS, PARETOTAILS/UPPERPARAMS.

%   Copyright 2006-2007 The MathWorks, Inc. 
%   $Revision: 1.1.6.2.2.1 $  $Date: 2007/01/16 03:38:10 $

p = obj.lowerparams;
