%function [out_perfday, in_perfday] = sj_HPexpt_plot_getvsday(animdirect,prefix)

savefig1 = 0;

% Use saved "perfday" structure to get data from all animals and plot performance vs day
% --------------------------------------------------------------------------------------

out=[]; in =[];

load('/data25/sjadhav/HPExpt/HPa_direct/HPaperfday.mat');
out = [out; out_perfday];
in = [in; in_perfday];

load('/data25/sjadhav/HPExpt/HPb_direct/HPbperfday.mat');
out = [out; out_perfday];
in = [in; in_perfday];

load('/data25/sjadhav/HPExpt/HPc_direct/HPcperfday.mat');
out = [out; out_perfday];
in = [in; in_perfday];


outin = [out; in];

% Plot
% ----

forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1
figdir = '/data25/sjadhav/HPExpt/Figures/Behavior/Summary/'; summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
if forppr==1
    set(0,'defaultaxesfontsize',20);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end

% Out
% ----
figure; hold on; 
plot(mean(out),'r.-','LineWidth',2);
errorbar2([1:length(out)], mean(out), stderr(out),0.3,'r');
title('Outbound Performance','FontSize',24,'FontWeight','normal');
set(gca,'XTick',[1:8]);
ylim([0.4 1.05]);

figfile = [figdir,'HP_outperf']
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
end

% In on same plot
% ---------------
%figure; hold on; 
plot(mean(in),'b.-','LineWidth',2);
errorbar2([1:length(in)], mean(in), stderr(in),0.3,'b');
title('Outbound and Inbound Performance','FontSize',24,'FontWeight','normal');
set(gca,'XTick',[1:8]);
ylim([0.4 1.05]);

figfile = [figdir,'HP_outandinperf']
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
end


% Mean of Outbound and Inbound
% -----------------------------
figure; hold on;
plot(mean(outin),'k.-','LineWidth',2);
errorbar2([1:length(outin)], mean(outin), stderr(outin),0.3,'k');
title('Mean Performance','FontSize',24,'FontWeight','normal');
set(gca,'XTick',[1:8]);
ylim([0.4 1.05]);

figfile = [figdir,'HP_outinperf']
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
end


