function [out_perfday, in_perfday] = sj_HPexpt_get_perfvsday(animdirect,prefix)
%UNTITLED For a Given Animal, Use behavperform to get perf vs day for inbound vs outbound for HPexpt

% sj_HPexpt_get_perfvsday('/data25/sjadhav/HPExpt/HPa_direct','HPa');


% Detailed explanation goes here:
% behavperform is saved in, for eg,  HPabehavperform.mat
% behavperform is extracted from DFSsj_createbehavperform.m -> DFAsj_calcproprewarded -> sj_trackperformance
% It has list of correct or not for outbpund vs inbound, and number of trials in each epoch and each day
% In behavperform, index (1) is Wtrack1, which 2 epochs for days1-5, and 1 epoch for days 6-8
% index 2 is Wtrack 2, which is 2nd epoch for days 6-8
%

directoryname = animdirect;
if (animdirect(end) == '/')
    animdirect = animdirect(1:end-1);
end

perf_file = sprintf('%s/%sbehavperform.mat', directoryname, prefix);
load(perf_file);

% For W-track 1, mazeidx = 1; For W-track2, mazeidx = 2; 
% Find out corresponding epochs for Gideon's animals
mazeidx = 1;

% A) Outbound First
% ------------------

% 1) This is list of start trial and end trial for each epoch 
ntr_ep = behavperform(mazeidx).dayouttrials;

% 2) There are 13 epochs for Wtr1: days 1-5 have two each, and days 6-8 have one each 
%    Convert to ntr_day 

% Manually plug-in
% ----------------
%Wtr1_ep_endinds = [2,4,6,8,10,11,12,13];
%Wtr1_ep_stinds = [1,3,5,7,9,11,12,13];
%ntr_dayend = ntr_ep(Wtr1_ep_endinds,2);
%ntr_dayst = ntr_ep(Wtr1_ep_stinds,1);
%ntr_daystend = [ntr_dayst,ntr_dayend];


% Alternatively, extract from dayepoch field, so the code is generalized
% ----------------------------------------------------------------
dayep = behavperform(mazeidx).dayepoch;
days = unique(dayep(:,1));
ntr_dayst=[]; ntr_dayend=[];
for i=1:length(days),
    currday = days(i);
    dayidx = find(dayep(:,1)==currday);
    currtr = ntr_ep(dayidx,:);
    ntr_dayst(i) = min(currtr(:));
    ntr_dayend(i) = max(currtr(:));
end

% Now get average performance for day
% ------------------------------------
for i=1:8, 
    out_perfvec_day{i} = behavperform(mazeidx).outreward(ntr_dayst(i):ntr_dayend(i));
    out_perfday(i) = mean(behavperform(mazeidx).outreward(ntr_dayst(i):ntr_dayend(i))); 
end

figure; hold on; plot(out_perfday,'.-');
ylim([0.3 1]);


% A) Inbound - Same
% ------------------

% 1) This is list of start trial and end trial for each epoch 
ntr_ep = behavperform(mazeidx).dayintrials;

% 2) There are 13 epochs for Wtr1: days 1-5 have two each, and days 6-8 have one each 
%    Convert to ntr_day 

% Manually plug-in
% ----------------
%Wtr1_ep_endinds = [2,4,6,8,10,11,12,13];
%Wtr1_ep_stinds = [1,3,5,7,9,11,12,13];
%ntr_dayend = ntr_ep(Wtr1_ep_endinds,2);
%ntr_dayst = ntr_ep(Wtr1_ep_stinds,1);
%ntr_daystend = [ntr_dayst,ntr_dayend];


% Alternatively, extract from dayepoch field, so the code is generalized
% ----------------------------------------------------------------
dayep = behavperform(mazeidx).dayepoch;
days = unique(dayep(:,1));
ntr_dayst=[]; ntr_dayend=[];
for i=1:length(days),
    currday = days(i);
    dayidx = find(dayep(:,1)==currday);
    currtr = ntr_ep(dayidx,:);
    ntr_dayst(i) = min(currtr(:));
    ntr_dayend(i) = max(currtr(:));
end

% Now get average performance for day
% ------------------------------------
for i=1:8, 
    in_perfvecday{i} = behavperform(mazeidx).inreward(ntr_dayst(i):ntr_dayend(i));
    in_perfday(i) = mean(behavperform(mazeidx).inreward(ntr_dayst(i):ntr_dayend(i))); 
end

figure; hold on; plot(in_perfday,'.-');
ylim([0.3 1]);


eval(sprintf('save %s/%sperfday.mat out_perfday in_perfday', animdirect, prefix));

