
function [trajbound, trajtime, rewarded, wellstend] = sj_ExtractAndSaveTraj_Wtrack(directoryname, prefix, day, epochs, varargin);
% sj_ExtractAndSaveTraj_Wtrack('/data25/RippleDisruption_Sub/RCb_direct', 'RCb', 1, [2 4]);
% or, for d=2:8, sj_ExtractAndSaveTraj_Wtrack('/data25/RippleDisruption_Sub/RCb_direct', 'RCb', d, [2 4]); end
% Similar to sj_ExtractAndPlotTraj_Wtrack, but run it for all epochs in a day, and save a structure called trajinfo

% This can be called from, eg. sj_HPExpt_preprocess
% sj_ExtractAndPlottraj_Wtrack('/data25/sjadhav/HPExpt/HPa_direct', 'HPa', [2 2]);
% sj_ExtractAndPlotTraj_Wtrack('/data25/sjadhav/HPExpt/HPc_direct', 'HPc', [6 4]);

% 28 Dec 2014: Notes for sj_ExtractAndPlottraj_Wtrack
% Started as plotvariables_12262014. Run trackperformance, which extracts trajetories/laps from linpos, and gives
% outbound or inbound (trajbound), correct or incorrect (rewarded), and start and endtimes (time) of trajctories.
% Then plot them either in single figure, or in 2 subplot figure which will show previous and next trajetory.
% DO FOR SINGLE EPOCH HERE. SAVE A STRUCTURE WHICH STORES THE EXTRACTED TRAJECTORY DATA as "trajinfo" SHOULD BE DONE FOR DAY
% IN NEXT CODE: sj_ExtractAndSaveTraj_Wtrack.

% Originally written for RIPPLE DISRUPTION. Will use this to check the trajectories. Then will write a function to
% get position around the choice point for vicarious trial-and-error analysis

% Use the same for HP_Expt TO TEST TRAJECTORIES. Then use this later for REPLAY IN OUTBOUND analysis.
% Having a saved trajectory structure will help getting this in Filter Framework

% Input:
% "directoryname" = directory with processed data
% "prefix" = animal prefix
% "index" = day and all epochs in the day.
% options: figopt, savestruct,
%          turndist: This is focussed especially on center arm. Get distance from linpos to determine turnaround.
% My linpos has already limited turnarounds to 75% of arm distance, not just how mich time animals
% has been away from well. But for VIarious trial-and-error, need the animal to be in the zone around
% choice point. So for ripple disruption, make this within 5cm of intersection point, like welldist in
% linearizeposition.
% So the animal has to get close to choice point. Can choose to include this or not in analysis later
% Shantanu Jadhav, Mar 2015
% ----------------------------------------------------------------

% Initialize
% ----------

savestruct = 0; % Whether to save structure or not 

turndist = 5; %Within "x" cm of intersection point, or based on arm length (can be %tage of arm length as well - see code body below)
welldist = 5; intdist = 5; choicedist = 15;

figdir = '/data25/RippleDisruption_Sub/Figures/';
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
clr = {'b','r','g','c','m','y','k','r'};
wellclr = {'r','m','g'};
Clgy = [0.5 0.5 0.5];

%set variable options
for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'lowercasethree'
            lowercasethree = varargin{option+1};
        case 'figopt'
            figopt = varargin{option+1};
        case 'turndist'
            turndist = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

currdir = pwd; % to change to at end

%Initialize directory
% -------------------
animdirect = directoryname;
if (animdirect(end) == '/')
    animdirect = animdirect(1:end-1);
end
cd(animdirect);

% GET DATA
% --------
% Pos file
posfile = sprintf('%s/%spos%02d.mat', directoryname, prefix, day);
load(posfile);
% LinPos file
linposfile = sprintf('%s/%slinpos%02d.mat', directoryname, prefix, day);
load(linposfile);

% Get parameters from pos and linpos. Not all are necessry, but good list of existing structures
% --------------------------------------------------------------------------------------

% Linpos fields:

% statematrix - a structure with each field the same length as pos.data
%                    Contains information about the linear position of the anuimal
%                    for each time step.
%
% segmenttable - a lookup table with 3 columns: the first column
%                    is the linear trajectory number (the row number in
%                    lindistpos{i}{e}.traject), the second column is the segment number in
%                    that trajectory, and the third column is the unique indentifier number
%                    for that segment, which is used in the segmentIndex field of statematrix. Any segments
%                    that are used in multiple trajectories are given the same identifier number.
%
% trajwells - gives the start and end well numbers for each trajectory (each row is
%                    a trajectory).  Each trajecory is defined from the
%                    output of coordprogram.
% segmentInfo - gives information about the 5 segments on track. start and end points, length, and trjectories on segment
% wellSegmentInfo - infor on wells, and the three segments with wells on them (1,3,5)
%                   getSegmentTable in linearizeposition infers connectivity of segments, possible paths, distances, and direction
%                    of segments with respect to well. Everything based on the 2 outbound trajectories, and eventually used to
%                   to compute linear position and trajectories


% All epochs in a day
for ep=epochs
    epoch = ep;
    currdayep = [day ep]
    
    % Can plot to get an idea of track
    postime = pos{day}{ep}.data(:,(1)); %posx = pos{day}{ep}.data(:,(2)); posy = pos{day}{ep}.data(:,(3));
    posx = pos{day}{ep}.data(:,(2)); posy = pos{day}{ep}.data(:,(3)); % Use NORMAL position for saving, not all data has smoothened position
    % a) Get Well and Intersection positions
    wellpos = linpos{day}{epoch}.wellSegmentInfo.wellCoord;
    intpos(1,:) = linpos{day}{epoch}.segmentInfo.segmentCoords(1,3:4); % Center arm
    intpos(2,:) = linpos{day}{epoch}.segmentInfo.segmentCoords(2,3:4); % Left arm - our view
    intpos(3,:) = linpos{day}{epoch}.segmentInfo.segmentCoords(4,3:4); % right arm - our view
    % b) Segment Information
    segmentCoords = linpos{day}{epoch}.segmentInfo.segmentCoords; %x-y of start and end of segment
    segmentLength = linpos{day}{epoch}.segmentInfo.segmentLength; % length of segment
    segmentTraj = linpos{day}{epoch}.segmentInfo.segmentTraj; % Trajectory on segment. Only one (the first) trajectory on segment is reported
    
    centerarmlength = segmentLength(1); % Can pass this on to trackperformance as mindistarm, e. 0.9%senterarmlength
    
    % Get trajectories from "trackperformance". Use thrsdistint for turnaround
    [trajbound, rewarded, trajtime, wellstend] = sj_trackperformance([day epoch], [], linpos, pos, [2 1 3], 'thrsdistint',turndist);
    trajinfo{day}{epoch}.trajbound = trajbound;
    trajinfo{day}{epoch}.rewarded = rewarded;
    trajinfo{day}{epoch}.trajtime = trajtime;
    trajinfo{day}{epoch}.wellstend = wellstend;
    trajinfo{day}{epoch}.descript = 'trajbound: 1=inbound, 0=outbound; rewarded: 1=correct, 0=error; trajtime: start and end time of traj/lap; wellstend = start and end well of traj/lap';
end

% Both epochs done, save data

trajinfofile = sprintf('%s/%strajinfo%02d.mat', directoryname, prefix, day);
save(trajinfofile,'trajinfo');
cd(currdir); % get back to calling directory




% Plotting from the corresponding plotting function. Set to 0 here - works on epoch

figopt = 0;
savefig = 0;

% PLOTS
if figopt==1
    % PLOT TRACK PARAMETERS
    % --------------------
    figure; hold on; plot(posx, posy, 'Color', Clgy);
    % Plot Well and Intersection positions, and circles around it
    for i=1:3
        plot(wellpos(i,1),wellpos(i,2),[wellclr{i} 's'],'MarkerSize',12,'MarkerFaceColor',[wellclr{i}]);
        plot(intpos(i,1),intpos(i,2),[wellclr{i} 's'],'MarkerSize',12,'MarkerFaceColor',[wellclr{i}]);
    end
    
    % Plot circle
    for i=1:3
        N=256;
        circle(wellpos(i,:),welldist,256,[wellclr{i} '-']);
        circle(intpos(i,:),intdist,256,[wellclr{i} '-']);
    end
    % Choice point circles
    circle(intpos(1,:),choicedist,256,'k-');
    % Plot segments
    segmentclr = ['r','b','m','c','g'];
    for i = 1:size(segmentCoords,1)
        line([segmentCoords(i,1),segmentCoords(i,3)],[segmentCoords(i,2),segmentCoords(i,4)],'Color',segmentclr(i),'LineWidth',2);
    end
    
    
    % PLOT TRAJECTORIES one by one
    % ----------------------------
    figure; hold on; plot(posx, posy, 'Color', Clgy);
    ntr = length(rewarded);
    startt = trajtime(:,1); endt = trajtime(:,2);
    for i=1:ntr,
        startidx(i) = find(startt(i)==postime);
        endidx(i) = find(endt(i)==postime);
        % Or use lookup
        %startidx(i) = lookup(startt(i),postime);
        %endidx(i) = lookup(endt(i),postime);
        
        if rewarded(i) == 1,
            linewd = 2; % correct
            spec ='-';
        else
            linewd = 2; %error
            spec ='--';
        end
        
        if trajbound(i) == 1
            clr='b'; %inbound
        else
            clr='r'; %outbound
        end
        
        plot(posx(startidx(i):endidx(i)),posy(startidx(i):endidx(i)),[clr, spec],'LineWidth',linewd);
        keyboard;
    end
    
end % figopt


















