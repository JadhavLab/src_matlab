% Get dreadd exp and control animals and plot behavior summary
% --------------------------------------------------------------

% Experimental Animals: DM1, DM8, RR1, YD1
% -----------------------------------------
exp_prefix = {'RR1','YD1'};

Nexp = length(exp_prefix);
Expout_logic=[];
Expout_curve=[];
Expout_rawperf_sess=[]; %vector (eg. 8 sessions)
Expout_perf_sess=[]; %vector (eg. 8 sessions)
Expout_lt=[]; %number/NaN
Expout_lsess=[]; %number/NaN
Expout_Ntrials=[]; Expout_Nsess=[]; Expout_Nmovavg=[];

% Loop over animals
% ------------------
for i=1:Nexp
    currprefix = exp_prefix{i};
    switch currprefix
        case 'DM1'
            dir = 'D:\DREADDs\DREADDs_results\Experimental Group\DM1_direct\';
        case 'DM8'
            dir = 'D:\DREADDs\DREADDs_results\Experimental Group\DM8_direct\';
        case 'RR1'
            dir = 'D:\DREADDs\DREADDs_results\Experimental Group\RR1_direct\';
        case 'YD1'
            dir = 'D:\DREADDs\DREADDs_results\Experimental Group\YD1_direct\';
    end
        
    [all_outbound_logic,outbound_curve, outbound_rawperf_sess, outbound_perf_sess,perf_movavg, outbound_learningtrial, learning_sess,...
        ntrajs_persess,~,~] = sj_findoutbound_singleday(dir,[1],[1:8],currprefix,1,[],0,0);
    
    Expout_Ntrials(i) = length(all_outbound_logic);
    Expout_Nsess(i) = length(outbound_perf_sess);
    Expout_Nmovavg(i) = length(perf_movavg);
     
    Expout_logic{i} = all_outbound_logic;
    Expout_curve{i} = outbound_curve;
    Expout_rawperf_sess{i} = outbound_rawperf_sess;
    Expout_perf_sess{i} = outbound_perf_sess;
    Expout_perf_movavg{i} = perf_movavg;
    Expout_lt(i)= outbound_learningtrial;
    Expout_lsess(i)= learning_sess;
    
end

% Convert to matrix form using NaNs
% -----------------------------------
% variables for curve/ntrials
max_N = max(Expout_Ntrials);
Expout_curve_mat = [];
max_Nmovavg = max( Expout_Nmovavg);
Expout_perf_movavg_mat = [];

% variables for perf/sess
max_sess = max(Expout_Nsess);
Expout_rawperf_sess_mat = [];
Expout_perf_sess_mat = [];

for i=1:Nexp
    i
    % padarray for curves
    currvec = Expout_curve{i};
    currvec = padarray(currvec,[max_N-length(currvec)],NaN,'post');
    Expout_curve_mat(i,:) = currvec;
    
    % padarray for mov avg
    currvec = Expout_perf_movavg{i};
    currvec = padarray(currvec',[max_Nmovavg-length(currvec)],NaN,'post');
    Expout_perf_movavg_mat(i,:) = currvec;
    
    % padarray for rawperf/sess
    currvec = Expout_rawperf_sess{i};
    currvec = padarray(currvec,[max_sess-length(currvec)],NaN,'post');
    Expout_rawperf_sess_mat(i,:) = currvec;
    
    % padarray for perf/sess
    currvec = Expout_perf_sess{i};
    currvec = padarray(currvec,[max_sess-length(currvec)],NaN,'post');
    Expout_perf_sess_mat(i,:) = currvec;
    
end


% -----------------------------------------

%Control Animals: 'SJ1','SJ2','SJ3','SJ4','EG5'
% -----------------------------------------
con_prefix = {'SJ1','SJ2','SJ3','SJ4','EG5'};

Ncon = length(con_prefix);
Conout_logic=[];
Conout_curve=[];
Conout_rawperf_sess=[]; %vector (eg. 8 sessions)
Conout_perf_sess=[]; %vector (eg. 8 sessions)
Conout_lt=[]; %number/NaN
Conout_lsess=[]; %number/NaN
Conout_Ntrials=[]; Conout_Nsess=[]; Conout_Nmovavg=[];

% Loop over animals
% ------------------
for i=1:Ncon
    currprefix = con_prefix{i};
    switch currprefix
        case 'SJ1'
            dir = 'D:\DREADDs\DREADDs_results\Controls\SJ1_direct\';
        case 'SJ2'
            dir = 'D:\DREADDs\DREADDs_results\Controls\SJ2_direct\';
        case 'SJ3'
            dir = 'D:\DREADDs\DREADDs_results\Controls\SJ3_direct\';
        case 'SJ4'
            dir = 'D:\DREADDs\DREADDs_results\Controls\SJ4_direct\';
        case 'EG5'
            dir = 'D:\DREADDs\DREADDs_results\Controls\EG5_direct\';
    end
        
    [all_outbound_logic,outbound_curve, outbound_rawperf_sess, outbound_perf_sess,perf_movavg, outbound_learningtrial, learning_sess,...
        ntrajs_persess,~,~] = sj_findoutbound_singleday(dir,[1],[1:8],currprefix,1,[],0,0);
    
    Conout_Ntrials(i) = length(all_outbound_logic);
    Conout_Nsess(i) = length(outbound_perf_sess);
    Conout_Nmovavg(i) = length(perf_movavg);
     
    Conout_logic{i} = all_outbound_logic;
    Conout_curve{i} = outbound_curve;
    Conout_rawperf_sess{i} = outbound_rawperf_sess;
    Conout_perf_sess{i} = outbound_perf_sess;
    Conout_perf_movavg{i} = perf_movavg;
    Conout_lt(i)= outbound_learningtrial;
    Conout_lsess(i)= learning_sess;
    
end

% Convert to matrix form using NaNs
% -----------------------------------
% variables for curve/ntrials
max_N = max(Conout_Ntrials);
Conout_curve_mat = [];
max_Nmovavg = max( Conout_Nmovavg);
Conout_perf_movavg_mat = [];

% variables for perf/sess
max_sess = max(Conout_Nsess);
Conout_rawperf_sess_mat = [];
Conout_perf_sess_mat = [];

for i=1:Ncon
    i
    % padarray for curves
    currvec = Conout_curve{i};
    currvec = padarray(currvec,[max_N-length(currvec)],NaN,'post');
    Conout_curve_mat(i,:) = currvec;
    
    % padarray for mov avg
    currvec = Conout_perf_movavg{i};
    currvec = padarray(currvec',[max_Nmovavg-length(currvec)],NaN,'post');
    Conout_perf_movavg_mat(i,:) = currvec;
    
    % padarray for rawperf/sess
    currvec = Conout_rawperf_sess{i};
    currvec = padarray(currvec,[max_sess-length(currvec)],NaN,'post');
    Conout_rawperf_sess_mat(i,:) = currvec;
    
    % padarray for perf/sess
    currvec = Conout_perf_sess{i};
    currvec = padarray(currvec,[max_sess-length(currvec)],NaN,'post');
    Conout_perf_sess_mat(i,:) = currvec;
    
end


% -----------
% Plotting
% -----------

set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
tfont = 24; % title font
xfont = 20;
yfont = 20;

% 1a. Plot Perf per session
% -----------------------------
figure; hold on;
errorbar(1:length(Expout_perf_sess_mat),nanmean(Expout_perf_sess_mat),nansem(Expout_perf_sess_mat),'r-','LineWidth',2);
errorbar(1:length(Conout_perf_sess_mat),nanmean(Conout_perf_sess_mat),nansem(Conout_perf_sess_mat),'b-','LineWidth',2);
legend('Exp','Con');
xlim([0.5 8.5]);
ylim([0.4 0.9]);
plot([1:10],0.5*ones(size([1:10])),'k--');
xlabel('Session Number','FontSize',xfont);
ylabel('Performance','FontSize',yfont);

 p = anova_rm({Expout_perf_sess_mat Conout_perf_sess_mat},'on');
 
 Expout_perf_sess_matvec = Expout_perf_sess_mat(:);
 Conout_perf_sess_matvec = Conout_perf_sess_mat(:);
 alldata = [Expout_perf_sess_matvec;Conout_perf_sess_matvec];
 grp1 = ones(size(1:8*Nexp)); grp2 = 2*ones(size((8*Nexp)+1:8*(Nexp+Ncon)));
 grp = [grp1,grp2]'; 
 sess = 1:8; allsess= repmat(sess,1,(Nexp+Ncon));allsess=allsess';
p = anovan(alldata,{grp,allsess});
 
 
% % 1b. Plot Raw Perf per session
% % -----------------------------
% figure; hold on;
% errorbar(1:length(Expout_rawperf_sess_mat),nanmean(Expout_rawperf_sess_mat),nansem(Expout_rawperf_sess_mat),'r-','LineWidth',2);
% errorbar(1:length(Conout_rawperf_sess_mat),nanmean(Conout_rawperf_sess_mat),nansem(Conout_rawperf_sess_mat),'b-','LineWidth',2);
% legend('Exp','Con');
% xlim([0.5 8.5]);
% ylim([0.4 0.9]);
% plot([1:10],0.5*ones(size([1:10])),'k--');
% xlabel('Session Number','FontSize',xfont);
% ylabel('Raw Performance','FontSize',yfont);
% 
%  p = anova_rm({Expout_rawperf_sess_mat Conout_rawperf_sess_mat},'on');
%  
%  Expout_rawperf_sess_matvec = Expout_rawperf_sess_mat(:);
%  Conout_rawperf_sess_matvec = Conout_rawperf_sess_mat(:);
%  alldata = [Expout_rawperf_sess_matvec;Conout_rawperf_sess_matvec];
%  grp1 = ones(size(1:32)); grp2 = 2*ones(size(33:72));
%  grp = [grp1,grp2]'; 
%  sess = 1:8; allsess= repmat(sess,1,9);allsess=allsess';
%  p = anovan(alldata,{grp,allsess});

 

% 2. Plot Learning Session
% ----------------------
jit=[0.05,0,0.1,0.15];
jit=jit(1:Nexp);
figure; hold on;
plot(ones(size(Expout_lsess))+jit,Expout_lsess,'rsq','MarkerSize',16,'LineWidth',2);
jit=[0.05,0,0.1,0.15,0.15];
jit=jit(1:Ncon);
plot(2*ones(size(Conout_lsess))+jit,Conout_lsess,'bo','MarkerSize',16,'LineWidth',2);
xlim([0.5,2.5]);ylim([0 9])
ylabel('Learning Session')

[h,p] = kstest2(Conout_lsess,Expout_lsess);


% 3. Plot Beh Curve
% ----------------------

Exp_meancurve = nanmean(Expout_curve_mat);
Exp_errcurve = nansem(Expout_curve_mat);
Con_meancurve = nanmean(Conout_curve_mat);
Con_errcurve = nansem(Conout_curve_mat);

figure; hold on;
%errorbar(1:length(Expout_curve_mat),Exp_meancurve,Exp_errcurve,'r-','LineWidth',2);
%errorbar(1:length(Conout_curve_mat),Con_meancurve,Con_errcurve,'b','LineWidth',2);
plot(1:length(Expout_curve_mat),Exp_meancurve,'r-','LineWidth',2);
jbfill(1:length(Expout_curve_mat),Exp_meancurve-Exp_errcurve,Exp_meancurve+Exp_errcurve,'r','r',1,0.2);
plot(1:length(Conout_curve_mat),Con_meancurve,'b-','LineWidth',2);
jbfill(1:length(Conout_curve_mat),Con_meancurve-Con_errcurve,Con_meancurve+Con_errcurve,'b','b',1,0.2);
ylim([0.45 0.9])
xlim([0 150])
plot([1:150],0.5*ones(size([1:150])),'k--');
ylabel('Performance')

% 4. Plot moving average
% ----------------------

% Exp_meancurve = nanmean(Expout_perf_movavg_mat);
% Exp_errcurve = nansem(Expout_perf_movavg_mat);
% Con_meancurve = nanmean(Conout_perf_movavg_mat);
% Con_errcurve = nansem(Conout_perf_movavg_mat);
% 
% figure; hold on;
% %errorbar(1:length(Expout_curve_mat),Exp_meancurve,Exp_errcurve,'r-','LineWidth',2);
% %errorbar(1:length(Conout_curve_mat),Con_meancurve,Con_errcurve,'b','LineWidth',2);
% plot(1:length(Expout_perf_movavg_mat),Exp_meancurve,'r-','LineWidth',2);
% jbfill(1:length(Expout_perf_movavg_mat),Exp_meancurve-Exp_errcurve,Exp_meancurve+Exp_errcurve,'r','r',1,0.2);
% plot(1:length(Conout_perf_movavg_mat),Con_meancurve,'b-','LineWidth',2);
% jbfill(1:length(Conout_perf_movavg_mat),Con_meancurve-Con_errcurve,Con_meancurve+Con_errcurve,'b','b',1,0.2);
% %ylim([0.45 100])
% xlim([0 150])
% plot([1:150],0.5*ones(size([1:150])),'k--');
% ylabel('Moving average')
% 

