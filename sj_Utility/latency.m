
function [spktimes, peaklatency, latency] = latency(time,events, bckevents)

spktimes=[];
for i=1:length(time)
    spktimes=[spktimes time{i}];
end

%peaklatency = mean(spktimes);   % THIS IS EQVT TO PICKING PEAK OF PSTH

psth=sum(events,1);
peaklatency = min(find(psth==max(psth)));   
bckpsth=sum(bckevents,1);
bck=mean(bckpsth) + 3*std(bckpsth);

on=find(psth>bck);

if length(on)>=3
    latency = min(on(find( on(3:end)-on(2:end-1)==1 & on(2:end-1)-on(1:end-2)==1)));
end
%     latency = min(on);
% end

% if length(on)>=2
%     for bins=1:length(on)-1
%         if (on(bins+1)-on(bins)==1)
%             latency=on(bins);
%             break;
%         end
%     end
% end

if length(on)<3
    latency=0;
end


