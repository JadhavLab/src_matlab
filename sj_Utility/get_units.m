
function get_units(newfile,spec,figopt1)
% get_units('A08p2r3_1_thr5_new','A08p2r3C*',0)
load(newfile);
files = dir(spec);
for i=1:length(files)
  
    datafile=strtok(files(i).name,'.'); 
    if strcmp(files(i).name,[datafile,'.mat']) 
        filename = strtok(files(i).name,'.');
    else 
        filename = [];
    end
    
    cnt=1;
    if length(filename)>0
        [recording,cluster]=strtok(filename,'C');
        load ([filename]);
        for i=1:length(sfstimes)
            for j=1:length(fstimes)
                if fstimes(j)==sfstimes(i)
                    match(cnt)=j; cnt=cnt+1;
                end
            end
        end
        
        figure; plot(swaves','b'); hold on;
        plot(fwaves(match,:)','r','Linewidth', 2);
        plot(mean(fwaves(match,:))','y','Linewidth', 4);
        title ([cluster]);
        newwaves=fwaves(match,:);
        
        save ([[filename] 'new.mat'], 'newwaves', 'sfstimes', 'stimes', 'swaves')
        clear cnt match sfstimes swaves
    end
    
end
