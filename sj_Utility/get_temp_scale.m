function s = get_temp_scale(Z,L);
% s = get_temp_scale(Z,L)
% find the temporal scale for smoothing a PSTH
% based on trial to trial jitter of spikes between trials
%
% INPUTS
%	Z	= raster with at least 2 rows (computes xc btwn all)
%	L	= longest scale to consider (look at xcorr -L:L)
%			default L=128 bins
% OUTPUT
%	s	= std of gaussian to use for smoothing, in bins
%		= (1/sqrt2) * std of the gaussian fit to the xcorr
%
% as per MBerry and MMeister PNAS 1997
% REVISED 8/30/01
% instead of computing many xcorrs from 2 rpts and averaging,
% concatenate all and do one xcorr (better data adequacy!) 

if nargin<2, L=128; end

[nrpt nbin]=size(Z); 
z1=reshape(Z',1,nrpt*nbin); % concatenate all repeats
ind=([ ceil(nrpt/2)+1:nrpt 1:ceil(nrpt/2)]); % make ~middle rpt #1 in shift
z2=reshape(Z(ind,:)',1,nrpt*nbin); % concatenate all repeats shifted

ac= xcorr(full(z1),full(z2),L); ac=ac/sum(ac); % set to sum=1
[mu s] = fitgauss(-L:L,ac,1);

s=s/sqrt(2); % because jitter of both spikes contributes to s

