
function [task] = sj_addtaskenv_Ndl(animdirect,prefix,day,env)

% Shantanu 
% Adding task environment to Gideon's animal Nadal
% Based on sj_updatetaskenv. For epochs with type=run, add the env

% To put env = 'env' in task struct, eg. 'lin', 'wtr1', 'wtr2'
% In addition to type such as 'sleep' in task struct which only has run types

% Task file
taskfile = sprintf('%s/%stask%02d.mat', animdirect, prefix, day);
load(taskfile);

epochs = length(task{day});
for ep = 1:epochs
    
    if strcmp(task{day}{ep}.type,'run') % If a run epoch
        task{day}{ep}.environment = env;
    
    else % or else its a sleep epoch
        
        task{day}{ep}.environment = 'postsleep'; % Call all intermediate sessions "postsleep"
        
        if ep==1
           task{day}{ep}.environment = 'presleep_aud'; 
        end
        if ep==epochs
           task{day}{ep}.environment = 'lastsleep_aud'; 
        end
    end
end

% Save updated task file
save(taskfile,'task');
