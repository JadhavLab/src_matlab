function sj_matchlinpostime(directoryname,fileprefix,days, varargin)

% Jan 2014. "sj_matchlinposandtasklength"had an error, so have to update "time" in linpos 
% using the time field in pos. Load exactly like this code, and then simply replace time 
% field in linpos with that from pos

% 31 DEC 2013. Called from sj_HPExpt_preprocess. 
% Changing length of task and linpos to match that of pos/ rawpos
% The difference is due to running sj_velocitydayprocess, but taskstruct created using original pos
% Carried forward due to something in sj_linearizeposition1, which I will be fix for future use
% For here, instead of running crratetaskstruct, and/or linepos again, I will
% just match lengths by adding a few points in the start.


%LINEARDAYPROCESS(directoryname,fileprefix,days, options)
%
%Runs linearizeposition for all run epochs in each day and saves the data in
%'linpos' in the directoryname folder.  See LINEARIZEPOSITION for the definitions 
%of the options.
%
%directoryname - example 'data99/user/animaldatafolder/', a folder 
%                containing processed matlab data for the animal
%
%fileprefix -    animal specific prefix for each datafile
%
%days -          a vector of experiment day numbers 
%

lowercasethree = '';

%set variable options
for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'lowercasethree'
            lowercasethree = varargin{option+1};
        
    end
end

days = days(:)';

for day = days
   linpos = [];
   
   dsz = '';
   if (day < 10)
      dsz = '0';
   end

   eval(['load ',directoryname,fileprefix,'task',dsz,num2str(day), '.mat']);
   eval(['task = ',lowercasethree,'task;'])
   eval(['load ',directoryname,fileprefix,'linpos',dsz,num2str(day), '.mat']);
   eval(['linpos = ',lowercasethree,'linpos;'])
   eval(['load ',directoryname,fileprefix,'pos',dsz,num2str(day), '.mat']);
   eval(['pos = ',lowercasethree,'pos;'])
   
   for i = 1:length(task{day})   
      if ((~isempty(task{day}{i})) && (strcmp(task{day}{i}.type,'run')) )
            i;
            disp(['Day ',num2str(day), ', Epoch ',num2str(i)])
            index = [day i];
            posdata = pos{index(1)}{index(2)}.data;
            statematrix = linpos{index(1)}{index(2)}.statematrix; 
            
            % Update the time field in linpos
            statematrix.time = posdata(:,1);
            % Update the statemetrix in linpos, which is to be saved,
            linpos{index(1)}{index(2)}.statematrix = statematrix;   
       
      end
   end
   
   % Save the updated linpos
   eval([lowercasethree,'linpos = linpos;']);
   eval(['save ',directoryname,fileprefix,'linpos',dsz,num2str(day),' ',lowercasethree,'linpos']); 
   
end

