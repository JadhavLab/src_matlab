function sj_matchlinposandtasklength(directoryname,fileprefix,days, varargin)

% 31 DEC 2013. Called from sj_HPExpt_preprocess. 
% Changing length of task and linpos to match that of pos/ rawpos
% The difference is due to running sj_velocitydayprocess, but taskstruct created using original pos
% Carried forward due to something in sj_linearizeposition1, which I will be fix for future use
% For here, instead of running crratetaskstruct, and/or linepos again, I will
% just match lengths by adding a few points in the start.


%LINEARDAYPROCESS(directoryname,fileprefix,days, options)
%
%Runs linearizeposition for all run epochs in each day and saves the data in
%'linpos' in the directoryname folder.  See LINEARIZEPOSITION for the definitions 
%of the options.
%
%directoryname - example 'data99/user/animaldatafolder/', a folder 
%                containing processed matlab data for the animal
%
%fileprefix -    animal specific prefix for each datafile
%
%days -          a vector of experiment day numbers 
%

lowercasethree = '';

%set variable options
for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'lowercasethree'
            lowercasethree = varargin{option+1};
        
    end
end


days = days(:)';

for day = days
   linpos = [];
   
   dsz = '';
   if (day < 10)
      dsz = '0';
   end

   eval(['load ',directoryname,fileprefix,'task',dsz,num2str(day), '.mat']);
   eval(['task = ',lowercasethree,'task;'])
   eval(['load ',directoryname,fileprefix,'linpos',dsz,num2str(day), '.mat']);
   eval(['linpos = ',lowercasethree,'linpos;'])
   eval(['load ',directoryname,fileprefix,'pos',dsz,num2str(day), '.mat']);
   eval(['pos = ',lowercasethree,'pos;'])
   
   for i = 1:length(task{day})   
      if ((~isempty(task{day}{i})) && (strcmp(task{day}{i}.type,'run')) )
            i;
            disp(['Day ',num2str(day), ', Epoch ',num2str(i)])
            index = [day i];
            posdata = pos{index(1)}{index(2)}.data;
            linearcoord = task{index(1)}{index(2)}.linearcoord; 
            statematrix = linpos{index(1)}{index(2)}.statematrix; 
            
            % If mismatch in size - its always going to be pos>task/linpos           
            if size(pos,1)~=length(linearcoord{1}),
                rem = size(posdata,1) - length(linearcoord{1});               
                if rem<0 % Unexpected - if task/linpos > pos
                    disp('linpos length is greater than pos - Shouldnt happen');
                    index(1),index(2)
                    keyboard;
                else
                    % Deal with linpos
                    % ---------------
                    for n=1:rem
                        statematrix.segmentIndex = [statematrix.segmentIndex(1);statematrix.segmentIndex];
                        statematrix.wellExitEnter = [statematrix.wellExitEnter(1,:);statematrix.wellExitEnter];
                        statematrix.segmentHeadDirection = [statematrix.segmentHeadDirection(1,:);statematrix.segmentHeadDirection];
                        statematrix.linearVelocity = [statematrix.linearVelocity(1,:);statematrix.linearVelocity];
                        statematrix.referenceWell = [statematrix.referenceWell(1);statematrix.referenceWell];
                        statematrix.linearDistanceToWells = [statematrix.linearDistanceToWells(1,:);statematrix.linearDistanceToWells];
                        statematrix.traj = [statematrix.traj(1);statematrix.traj];
                        statematrix.lindist = [statematrix.lindist(1);statematrix.lindist];
                        % Time - First add rows, then replace with correct time from pos
                        statematrix.time = [statematrix.time(1);statematrix.time];
                    end
                    % Update time with correct time from pos
                    for n=1:rem
                        statematrix.time(n) = posdata(n,1);
                    end                      
                    
                    % Deal with task linearcoord 
                    % ----------------------------
                    
                    if ~(strcmp(task{day}{i}.environment,'lin')) % If not linear track                      
                        
                        usecoord1 = linearcoord{1}(:,:,1);
                        usecoord2 = linearcoord{2}(:,:,1);
                        tmpcoord1 = nan(size(linearcoord{1},1),size(linearcoord{1},2),size(linearcoord{1},3)+rem);
                        tmpcoord2 = nan(size(linearcoord{2},1),size(linearcoord{2},2),size(linearcoord{2},3)+rem);
                        
                        tmpcoord1(:,:,rem+1:end) = linearcoord{1};
                        tmpcoord2(:,:,rem+1:end) = linearcoord{2};
                        for n=1:rem
                            tmpcoord1(:,:,n) =  usecoord1;
                            tmpcoord2(:,:,n) =  usecoord2;
                        end
                        
                        linearcoord{1} = tmpcoord1;
                        linearcoord{2} = tmpcoord2;
                        
                    else % for linear track
                        
                        usecoord1 = linearcoord{1}(:,:,1);
                        tmpcoord1 = nan(size(linearcoord{1},1),size(linearcoord{1},2),size(linearcoord{1},3)+rem);                       
                        tmpcoord1(:,:,rem+1:end) = linearcoord{1};
                        for n=1:rem
                            tmpcoord1(:,:,n) =  usecoord1;
                        end                        
                        linearcoord{1} = tmpcoord1;                      
                        
                    end
                    
                end
                
            end % end mismatch for current epoch
            
            % Update the statemetrix in linpos, which is to be saved,
            linpos{index(1)}{index(2)}.statematrix = statematrix;   
            
            % AND ALSO BEHAVESTATE
            behavestate{index(1)}{index(2)}.state = linpos{day}{i}.statematrix.traj;
            behavestate{index(1)}{index(2)}.lindist = linpos{day}{i}.statematrix.lindist;
            
            % AND ALSO TASK
            task{index(1)}{index(2)}.linearcoord = linearcoord;          
          
      end
   end
   
   eval([lowercasethree,'linpos = linpos;']);
   eval(['save ',directoryname,fileprefix,'linpos',dsz,num2str(day),' ',lowercasethree,'linpos']);
   
   eval([lowercasethree,'task = task;']);
   eval(['save ',directoryname,fileprefix,'task',dsz,num2str(day),' ',lowercasethree,'task']);
   
   % Save behavestatefile
   eval([lowercasethree,'behavestate = behavestate;']);
   eval(['save ',directoryname,fileprefix,'behavestate',dsz,num2str(day),' ',lowercasethree,'behavestate']);  
   
end

