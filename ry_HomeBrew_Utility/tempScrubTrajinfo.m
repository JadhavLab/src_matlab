D = dir('*trajinfo*');

for d = {D.name}
    
    load(d{1});
    for day = 1:numel(trajinfo)
        for e = 1:numel(trajinfo{day})
            
            if isempty(trajinfo{day}{e})
                continue;
            else
                trajinfo{day}{e} = rmfield(trajinfo{day}{e},'C');
            end
            
        end
    end
    save(d{1},'trajinfo');
    
end