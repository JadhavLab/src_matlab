point=XY(i,:);
    if sum(isnan(point))
        XY_projected=[XY_projected; nan nan];
        continue;
    end
    
    if i == 1 || isempty(XY_projected) || sum(isnan(XY_projected(i-1,:)))
        point_previous=XY(i,:);
        XY_projected=[XY_projected; nan nan];
    else
        point_previous=XY_projected(i-1,:);
    end

    % Get record of point-closeness
    distances = bsxfun(@minus, point, pointlist);
    distances = distances.^2;
    distances = sqrt(distances(:,1)+distances(:,1));
    
    % Find the local maxima, and choose the local maxima closest to the
    % previous
    try
        [pks,locs] = findpeaks(-distances);
    catch
        [~,min_ind] = min(distances);
        XY_projected = [XY_projected; pointlist(min_ind,:)];
        continue;
    end
    
    % Choose the local maxima closest to the previous point
    prev_distances = bsxfun(@minus, point_previous, pointlist(locs,:));
    prev_distances = prev_distances.^2;
    prev_distances = sqrt(prev_distances(:,1)+prev_distances(:,2));
    
    [~,min_ind] = min(prev_distances);
    min_ind = locs(min_ind);
    
    XY_projected = [XY_projected; pointlist(min_ind,:)];