function [out] = lim(x)

out = [min(x) max(x)];

end