function x = prange(x)
% pythonic range function
x  = 1:numel(x);