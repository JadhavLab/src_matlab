

function desiredindex =  colof(req, structdat)
    % This looks at the 'fields' field of a struct and decides which column
    % to sample in .data ... this exists because I've seen some of the
    % columns shift around occassionally in the data
		fieldparams = structdat.fields;

		% remove any parentheticals
		[a,b] = regexp(fieldparams,'\(.*\)');

		c=[];
		for i = 1:numel(a)
			c = [c a(i):b(i)];
		end

		fieldparams(c) = [];

		fieldparams = strsplit(fieldparams);

		desiredindex = 0;

		for i = 1:length(fieldparams)
			if strcmp(fieldparams{i},req)
				desiredindex = i;
			end
		end
	end