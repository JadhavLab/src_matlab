function plot2dValue(x,y,vectors,speed)

if nargin ~= 4; speed=1; end
if ~iscell(vectors); vectors={vectors};end

mm_x = [min(x) max(x)];
mm_y = [min(y) max(y)];

colors=jet(numel(vectors)*2);


gcf;clf;
for k = 1:speed:numel(x)
    if k < 10
        continue;
    end
    hold off;
    scatter(x,y,'k','filled'); alpha(0.005);
    plot(x(1:k),y(1:k),'k-');hold on;
    
    % Here is where quivers will be placed ... from there the unit vector
    for i = 1:numel(vectors)
        hold on;
        quiver(x(1:k),y(1:k),vectors{i}(1:k,1),vectors{i}(1:k,2),'color',colors(i*2,:));
    end
    
    % Now scale axes appropriately
    axis([mm_x mm_y]);
    drawnow;
end