function doubleComet(xyA,xyB,varargin)

p=inputParser;
p.addParameter('savemovie','',@ischar);
p.addParameter('num',[],@isreal);
p.parse(varargin{:});
savemovie=p.Results.savemovie;
num=p.Results.num;

if ~isempty(savemovie)
    v=VideoWriter(savemovie);
    open(v);
end

figure;
lA = animatedline;
lB = animatedline;
pA = animatedline;
pB = animatedline;
set(pA,'color','b','marker','*');
set(pB,'color','r','marker','*');
set(lA,'color','b');
set(lB,'color','r');

bounds = [ ...
    min([xyA(:,1); xyB(:,1)]),max([xyA(:,1); xyB(:,1)]),   ...
    min([xyA(:,2); xyB(:,2)]),max([xyA(:,2); xyB(:,2)]),   ...
    ];
axis(bounds);

assert(isequal(size(xyA),size(xyB)));

txt=text(bounds(1)+5,bounds(3)+5,'');
for t=1:size(xyA)
    
    clearpoints(pA);
    clearpoints(pB);
    addpoints(pA,xyA(t,1),xyA(t,2));
    addpoints(pB,xyB(t,1),xyB(t,2));
    addpoints(lA,xyA(t,1),xyA(t,2));
    addpoints(lB,xyB(t,1),xyB(t,2));
    drawnow;
    
    if ~isempty(num)
        fprintf('printing num\n');
        txt.String=num2str(num(t));
    end
    
    if ~isempty(savemovie)
        writeVideo(v,getframe(gca));
    end
    
end

if ~isempty(savemovie)
    close(v);
end

end