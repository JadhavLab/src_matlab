function [yboot,xsub,yci] = bootstrap(x,y,ci,nbootstrap,nbin,varargin)
% Get bootstrapped curves and cis of value range ...
% TODO figure out WHY in the heck the boostrap curves appear to be shifted
% and out of place.

ploton=false;
optlistassign(who,varargin{:});

xshape=size(x);yshape=size(y);
x = x(:); y = y(:);

if ~exist('ci','var'), ci = 0.95; end
if ~exist('nbootstrap','var'), nbootstrap = 100; end
if ~exist('nbin','var'), nbin = numel(unique(x)); end

% Compute bin edges
xr = lim(x);
edges = linspace(xr(1),xr(2),nbin+1);

% Subsample and bin each subsample
for b = 1:nbootstrap
    subsamp   = randsample( numel(x) , numel(x) , true );     % generate subsample with replacement
    ss(b,:) = subsamp;
    xsub(b,:) =x(subsamp);                                   % #ok<*AGROW> % store subsample of x
    ysub(b,:) = y(subsamp);                                   % store subsample of y
    [~,~,bin_sub(b,:)] = histcounts(  x(subsamp) , edges  );
end

if ploton, fig 'Bootstrap checks'; end;

% Per bin, per subsampled bootstrap, and compute curve
% ------
Y_bootstrap = cell(nbootstrap,nbin);
[Y_bootstrap{:}] = deal(nan);
% ------
 for u = 1:nbin
    % Bootstrap via subsamples
    for b = 1:nbootstrap
        % Get subsample
        ythis = ysub(b,:);
        % Number of spikes in bin
        mean_y = nanmean(   ythis( bin_sub(b,:)==u  ) ,  2 );
        %  Store 
        Y_bootstrap{b,u} = mean_y;
        
        if ploton && u==1
            
            nestplot(nbootstrap,1,{b 1});
            check(xsub(b,:),ysub(b,:),ss(b,:));
            
        end
        
    end
end

% Convert edges to centers
center = mean(  [edges(1:end-1);edges(2:end)]  ,1);
yint_bootstrap = zeros(   nbootstrap  ,  size(Y_bootstrap,2)  );

% Interpolate each bootstrap
interp=true;

for b = 1:nbootstrap
    ynint_bootstrap(b,:) = cat( 1, Y_bootstrap{b,:} ); 
    % If interpolation is on
    %TODO -- this is buggy ... interp messed up looking
    if interp
        filled_idx = ~isnan(ynint_bootstrap(b,:));
        yint_bootstrap(b,:) = interp1(center(filled_idx),ynint_bootstrap(filled_idx)',center,'linear','extrap');
    end
end


% Calculate CI
where_ci = round(   nbootstrap *  (1 - ci)/2  );
syb = sort(  yint_bootstrap,1  );
yci = [ syb(where_ci,:); syb(end-where_ci+1,:) ];
yboot = yint_bootstrap;

    function check(xsub,ysub,inds)
        % Simply checks that the bootstraps are making sense
        
        inds = [[1:numel(inds)]',inds(:)];
        inds = sortrows(inds,2);
        inds = [inds,[1:size(inds,1)]'];
        inds = sortrows(inds,1);
        
        xc = zeros(size(inds(:,3)));
        yc = zeros(size(inds(:,3)));
        
        xc(inds(:,3)) = xsub;
        yc(inds(:,3)) = ysub;
        
        xc = reshape(xc,xshape);
        yc = reshape(yc,yshape);
        
        plot(xc',yc');
        
    end

end