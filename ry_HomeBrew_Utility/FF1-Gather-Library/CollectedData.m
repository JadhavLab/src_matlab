classdef CollectedData
    % CollectedData is a class that contains the output of the GatherData
    % class processes. GatherData is a class that automatically handles
    % taking filter/NQ output, for any mentioned field, and creates an ND
    % sparse repsentation automatically of it, where the subscripts are the
    % animal,filter,day,epoch,... prefield dimensions and whichever
    % post-field dimensions exist in the data type (for instance if each
    % output contains 18 bins of phase data, then those bins automatically
    % are a dimension indexed by their own subscript). At the CollectedData
    % level, this allows very fast averaging or statistical moments across
    % any dimension(s) or fast reshuffling of dimensions (e.g. grabbing
    % every 5th variable size bin of a filter output phase_bins). This
    % makes squezing results from your FilterFramekwork/NQ outputs way
    % faster.
	%
	% TODO -- add dimensional movement
	% TODO -- add moment function
	% TODO -- add "centralization" for post-field data
	% TODO -- change waitbar implementation to the more stable variety
	% found on matlab central
	% TODO -- add ability to specify slices into dimensions, not just all
	% elements of the dimension
	% TODO -- add nth root function so that can do things like root mean
	% square and standard deviation
	
	properties (SetAccess=private,GetAccess=public)
		data % Contains an NdSparse matrix containing all output gathered for a data type
		init % Contains all subscript information from when the object was first initialized
				% .subs, predim subscripts can find data
				% .postdimSize, size of the postfield dimensional space
	end
	
	properties (SetAccess=private)
		locations % NdSparseArray where 1's indicate data lives there and 0's that it does not -- this helps solve the problem of distinguishing which parts of ND gathered output are true data points and which are empty. It is also used to track how many elements to divide when meaning/summing a dimension(s) of the CollectedData, or when operating between CollectedData instances.
		messages=false; % controls whether or not progress messages appear
		waitbar_handle=[];
		waitbar_count = 0;
	end
	
	properties (Constant,GetAccess=private)
		sizeGuess = 1e2; % pre-allocated space when no size given for a blank class. 
	end
	
	methods (Access=public)
		% ====================================================================
		% Contructors and initialization methods
		% ====================================================================
		function obj = CollectedData(a,b,c,d)
            % Constructor supports three possible input scenarios right
            % now, which are
			
			if nargin == 0
				% Default Constructor
			elseif nargin == 1
				if isscalar(a)
					nd_allocate=a;
					obj.data = ndSparse.build([],[],[],nd_allocate);
					obj.locations = ndSparse.build([],[],[],nd_allocate);
				elseif isnumeric(a) && ~isscalar(a)
					nd_size = a;
					obj.data = ndSparse.build([],[],nd_size,1e4);
					obj.locations = ndSparse.build([],[],nd_size,1e4);
				elseif isequal(class(a),'CollectedData') % copy constructor
					obj.data = a.data;
					obj.locations=a.locations;
					obj.init = a.init;
				end
			elseif nargin == 2
				if isnumeric(a) && isscalar(b)
					nd_size=a;
					nd_preallocate=b;
					obj.data = ndSparse.build([],[],nd_size,nd_preallocate);
				end
			elseif nargin>=3
				obj.data=a;
				obj.locations=b;
				obj.init.subs=c;
			end
			
			if nargin == 4
				obj.init.postdimSize = d;
			end
			
		end
		% ====================================================================
		% Waitbar control
		% ====================================================================
		function obj = toggleWaitbar(obj,val)
			if nargin == 1
				obj.messages=~obj.messages;
			else
				obj.messages = logical(val);
			end
		end
		
		function obj = resetWaitbar(obj,hardreset)
			delete(obj.waitbar_handle);
			obj.waitbar_handle=[];
			if nargin ==2 && hardreset
				input('Warning, press enter to delete all figure handles...',...
					's');
				set(groot,'ShowHiddenHandles','on')
				delete(get(groot,'Children'));
				set(groot,'ShowHiddenHandles','off')
				close('all');
			end
		end
		% ====================================================================
		% External Operators
		% ====================================================================
		function B = plus(obj,A)
            % This allows CollectedData object to be added to a numeric
            % type, an ndSparseMatrix, or another CollectedData object.
            
			if isnumeric(A)
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B.locations = obj.locations;
				B(obj.locations) = obj.data(obj.locations) + A;
			elseif isequal('ndSparse',class(A))
				% TODO FIX THIS
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B = obj.data + A;
			elseif isequal('CollectedData',class(A))
				B.data = obj.data + A.data;
				B.locations = obj.locations + A.locations;
            end
            
        end
        % ---------------------------------------------------------------------
        function equality = isequal(obj,A)
            equality = isequal(obj.data,A.data) && isequal(obj.locations,A.locations);
        end
        % ---------------------------------------------------------------------
        function B = minus(obj,A)
            % This allows CollectedData object to be added to a numeric
            % type, an ndSparseMatrix, or another CollectedData object.
            
			if isnumeric(A)
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B.locations = obj.locations;
				B(obj.locations) = obj.data(obj.locations) - A;
			elseif isequal('ndSparse',class(A))
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B = obj.data - A;
                % TODO FIX THIS, take the set union of locations and ndSparse
                % filled in elements
			elseif isequal('CollectedData',class(A))
                B = CollectedData();
				B.data = obj.data - A.data;
				B.locations = obj.locations - A.locations;
            end
            
        end
        % ---------------------------------------------------------------------
		function B = mtimes(obj,A)
			if isnumeric(A) && isscalar(A)
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B.locations = obj.locations;
				B(obj.locations) = obj.data(obj.locations) * A;
			end
		end
		% ---------------------------------------------------------------------
        function B = times(obj,A)
            % This allows CollectedData object to be multiplied by a scalar
            % or multiplied element-by-element to another CollectedData
            % object.		
			if isnumeric(A) && isscalar(A)
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B.locations = obj.locations;
				B(obj.locations) = obj.data(obj.locations) * A;
			elseif isequal('ndSparse',class(A))
                assert(isequal(size(A),size(obj.data)));
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B = obj.data .* A;
                % TODO FIX THIS, take the set union of locations and ndSparse
                % filled in elements
			elseif isequal('CollectedData',class(A))
                assert(isequal(size(A.data),size(obj.data)));
                assert(isequal(size(A.locations),size(obj.locations)));
                
				B.data = obj.data .* A.data;
				B.locations = obj.locations .* A.locations;
            end
		end
		% ---------------------------------------------------------------------
		function B = rdivide(obj,A)
			if isnumeric(A) && isscalar(A)
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B.locations = obj.locations;
				B(obj.locations) = obj.data(obj.locations) / A;
			elseif isequal('ndSparse',class(A))
                assert(isequal(size(A),size(obj.data)));
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B = obj.data ./ A;
                % TODO FIX THIS, take the set union of locations and ndSparse
                % filled in elements
			elseif isequal('CollectedData',class(A))
                assert(isequal(size(A.data),size(obj.data)));
                assert(isequal(size(A.locations),size(obj.locations)));
                
				B.data = obj.data ./ A.data;
				B.locations = obj.locations ./ A.locations;
            end
		end
		% ---------------------------------------------------------------------
		function B = mrdivide(obj,A)
			if isnumeric(A) && isscalar(A)
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B.locations = obj.locations;
				B(obj.locations) = obj.data(obj.locations) / A;
			end
		end
		% ---------------------------------------------------------------------
		function B = power(A)
			if isnumeric(A) && isscalar(A)
				B = CollectedData();
				B.data = ndSparse.build([],[],size(obj.data),numel(obj.data));
				B.locations = obj.locations;
				B(obj.locations) = obj.data(obj.locations).^A;
			end
		end
		% ====================================================================
		% Internal Operators
		% ====================================================================
		function obj = sum(obj,dim)
			
			if obj.messages
				obj.waitbar_count=obj.waitbar_count+1;
				waitbar_text = ...
					'CollectedData.sum: percent complete ...\n';
				multiWaitbar(waitbar_text,0);
				total=numel(dim);
			end
			
			if isnumeric(dim)
				
				while numel(dim) > 0
					obj.data = sum(obj.data,dim(1),'omitnan');
					obj.locations = sum(obj.locations,dim(1),'omitnan');
					dim(1) = [];
					if obj.messages
						percent=1-numel(dim)/total;
						multiWaitbar(waitbar_text,percent);
					end
				end
			end
			
			if obj.messages
				multiWaitbar(waitbar_text,'Close');
				obj.waitbar_count=obj.waitbar_count-1;
			end
			
        end
		% ====================================================================
		% Statistical Functions
		% ====================================================================
		function objout = mean(obj,dim)
            % Carries out a mean across selected dimensions of the
            % CollectedData. Since this involves operatiosn on two
            % different sparse matrices, it hides the true complexity of
            % the method. TODO add cellular dimension input
			
			if isnumeric(dim)
				objout = sum(obj,dim);
				objout.data = objout.data./objout.locations;
				objout.locations = objout.locations>0;
			end
			
		end
		% ---------------------------------------------------------------------
		function obj = moment(obj,dim,nth)
            % Applies the nth moment operation to the selected dimensions.
            % Can be used to get variance and kurtosis across specificed
            % dimension(s) ... probably the most RAM hungry of all methods
            % TODO add cellular dimension input
			
            if isnumeric(dim)
				
				for i = 1:numel(dim)
					
					% acquire centrality of the moment
					m = mean(obj,dim(i));
					
					sizeTotal  = size(obj);
					sizeTotal(dim(i)) = 1;
					T = CollectedData(sizeTotal);
					
					subs = cell(1,ndims(obj));
					[subs{:}] = deal(':');
					for j = 1:size(obj,dim(i))
						
						subs{dim(i)} = num2str(j);
						S = substruct('()',subs);
						
						T = T + (subsref(obj,S) - m).^nth;
					end
					
					obj.locations = sum(obj.locations,dim(i));
					obj.data = T.data; clear('T');
					obj.data = obj.data./obj.locations;
				end	
            end
            
		end
		% =====================================================================
		% Dimensional Reshaping
		% =====================================================================
        function C= dim2dim(obj,dimSetInitial,dimFinal)
            % Places the dimensions in the intitial set into the final
            % dimension. This works for say collecting samples certain
            % subsets of a singular dimension into one dimension.
			
			C=CollectedData();
			
			if numeric(dimSetInitial)
				
				selector=cell(1,numel(size(obj.data)));
				[selector{:}] = deal(':');
			end
			
			
            
        end
        % -----------------------------------------------------------------
        function obj = squeeze(obj)
            % Squeezes out singular dimensions
            obj.data = squeeze(obj.data);
            obj.locations = squeeze(obj.locations);
        end
        function obj = fsqueeze(obj)
            obj.data = full(squeeze(obj));
        end
        % -----------------------------------------------------------------
        function [A, B] = sizeIntersect(A,B)
            % Scales A and B so that they tile the same element locations.
            
            A_size=size(A);
            B_size=size(B);
            
            max_size = max([A_size;B_size]);
            
            error('unfinished method');
            
        end
		% =====================================================================
		% Assignment and Referecing
		% =====================================================================
        function C = subsasgn(obj,S,B)
			% Subscript assignment overload
			
			C=CollectedData();
			
			C.data      = subsasgn(obj.data,S,B);
			C.locations = subsasgn(obj.locations,S,1);
			
		end
		% ---------------------------------------------------------------------
        function varargout = subsref(obj,S)
			% Subscript reference overload
			
			% User is requesting a functino be performed with java syntax
			if numel(S)==2 && isequal(S(1).type,'.') && isequal(S(2).type,'()')
				
				varargout = cell(nargout(S(1).subs),1);
				[varargout{:}] = feval([S(1).subs],obj,S(2).subs{:});
		
			% User is requesting a public data type with java syntax
			elseif isequal(S.type,'.')
				
				switch S.subs
					case 'data'
						varargout{1}=obj.data;
					case 'subs'
						varargout{1}=obj.subs;
					case 'locations'
						varargout{1}=obj.locations;
					otherwise
						error('Requesting unrecognized property of CollectedData object');
				end
			
			% User is trying to subref, i.e., slice the current
			% CollectedData oject along certain dimensions
            elseif numel(S) == 1
                
				varargout{1}            = CollectedData();
				varargout{1}.data       = subsref(obj.data,S);
				varargout{1}.locations  = subsref(obj.locations,S);
            elseif numel(S) > 1
                input = CollectedData();
                input.data = obj.data;
                input.locations = obj.locations;
                for i = 1:numel(S)                    
                    input = subsref(input,S(i+1:end));
                end
                varargout=input;
			end 
			
        end
        function out = subElem(obj,elem)
            S = obj.init.subs; S = mat2cell(S(elem,:),1,numel(S(elem,:)));
            evalstr = ['out = obj.data(S{:}' repmat(',:',1,ndims(obj.data)-numel(S))  ');'];
            eval(evalstr);
            out = squeeze(full(out));
        end
		% =====================================================================
		% Size
		% =====================================================================
		function C = numelVals(obj)
			
			if obj.messages
				waitmessage = ...
					'CollectedData.numelVals: percent complete ...\n';
				obj.waitbar_count = obj.waitbar_count + 1;
				total=ndims(obj.locations);
			end
			
			
			C = sum(obj.locations>0);
			while ndims(C)>2
				if obj.messages
					percent = 1-ndims(C)/total;				
					multiWaitbar(waitmessage,percent);
				end
				C = sum(C);
				C  = squeeze(C);
			end
			C=full(C);
			
			if size(C,1) > 1 || size(C,2) > 1
				if obj.messages
					percent=100*(1-2/total);
					multiWaitbar(waitmessage,percent);
				end
				C=sum(C,2);
				if obj.messages
					percent=100*(1-1/total);
					multiWaitbar(waitmessage,percent);
				end
				C=sum(C,1);
			end
			
			if obj.messages
				obj.waitbar_count = obj.waitbar_count - 1;
				multiWaitbar(waitmessage,'Close');
			end
				
		end
		% ---------------------------------------------------------------------
		function C = numel(obj)
			C = numel(obj.locations);
			assert(isequal(C,numel(obj.data)));
		end
		% ---------------------------------------------------------------------
		function C = size(obj,dim)
			if nargin == 1
				C = size(obj.locations);
			elseif nargin == 2
				C = size(obj.locations,dim);
			end
 			assert(isequal(size(obj.locations),size(obj.data)));
		end
		% =====================================================================
		% Convert to Standard Matrices
		% =====================================================================
		function [data_out, locations_out, subs_out] = full(obj)
			data_out = full(obj.data);
			locations_out = full(obj.locations);
		end
		
    end
    
    methods (Access=public,Static)
    end
	
	methods (Access=private)
		function subs = transformSubs(obj,dim)
			
		end
		function obj = pushWaitbar(obj)
			obj.waitbar_count = obj.waitbar_count + 1;
		end
		function popWaitbar(obj)
			obj.waitbar_count = obj.waitbar_count - 1;
		end
	end
	
end