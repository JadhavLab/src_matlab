function [type, dim] = probePostfield(outterm)
	% Name:		probePostField(modf, field)
	% Purpose:	This is part of a second attempt to create a generalized
	%			method of gathering filterframework/neuroquery output
	%			in the way that filterramework/neuroquery generalizes the
	%			execution of analysis on subsets of data. The point is to
	%			get our data out of these structures and coellesce them
	%			into matrices or cells in ways that are almost immediately
	%			plottable or averagable or normalizable.
	%
	%			This particular function gets the other "half of the
	%			equation", the index and datatype situation for a field! 

	%% Get dimensions of post-field term!
	
	type = class(outterm); 
	if ~isequal(type,'cell') && ~isequal(type,'struct')
		type = 'matrix';
	end
	dim = size(outterm);
	
	if ~isscalar(outterm)
		
		% Acquire the index for this subscript combo
		index = prod(dim);
		
		if		isequal(type, 'cell')
			for i = 1:index 
				[type_temp{i}, dim_temp{i}] = ...
					probePostfield(outterm{i});
			end
		elseif	isequal(type, 'matrix')
			[type_temp, dim_temp] = ...
				probePostfield(outterm(index));
		elseif	isequal(type, 'struct')
			error('Nested structs not supported yet.');
		end
		
		if ~isempty(type_temp) && ~isempty(dim_temp)
				
% 			objsize = {objsize, objsize_temp};
			objsize=dim;
			subs={};type={};dim={};
			for i=1:index
				[subs{1} subs{2} subs{3} subs{4} subs{5} subs{6} subs{7} subs{8}] ...
					= ind2sub(objsize,i);
				substructure=substruct('{}',subs);
				type=subsasgn(type,substructure,type_temp{i});
				dim=subsasgn(dim,substructure,dim_temp{i});
			end                
			
		end
	else
		type = [];
		dim = [];
		objsize = [];
	end
		


end
