function [result, result_locs]...
    = prepostFinalize(pre,post)
	
	% Obtain pre and post data
	output = pre.output; subs=pre.subs;
	postdim = post.dim;
	
	% Obtain max size of post-field levels - we need to know how big to
	% make our matrix to encompass its full dimensionality
	[~, dimrequest]= findDimensionsPerLevel(postdim); %TODO! first dimension is NOT relevant
	postinfo.type = post.type; postinfo.dimr = dimrequest;
	
	% Count the number of dimensions that exist "before" and "after" the
	% field that the user requested tgo gather in the parent function
	nPredim = size(subs,2);
	nPostdim = numel(dimrequest.raw);
	
	% Create sparse matrix to hold result
	prealloc = prod(dimrequest.raw)  * size(subs,1);
	result = ndSparse.build([],[],[size(pre.output) dimrequest.raw],prealloc);
	result_locs = ndSparse.build([],[],[size(pre.output) dimrequest.raw],prealloc);
	
	for s = 1:size(subs,1)
		global g;
		fprintf('Processing field %d, element %d ...\n',g,s);

        % These will be used to address our cell and array objects flexibly
        % with subsasgn and subsref
		warning off;
		cell_address = mat2cell(subs(s,:),1,numel(subs(s,:)));
		warning on;
		array_address = cell_address;
		[array_address{end+1:end+nPostdim}] = deal(':');
		
        substructure_array = substruct('()',array_address);
		substructure_cell = substruct('{}',cell_address);
		
		% Create specialized versions of our field elements transposed out
		% of the N-d axis of prefield terms, and also one for NaN elements
		% sized to the largest field element to enable use to place
		% vairably lengthed elements in our matrix
		[transformed_elements, logical_t] = ...
			transformDimension(subsref(output,substructure_cell),...
			nPredim,nPostdim,postinfo);
		
		% First population the entire new cellular structure with NaN
		% elements and then assign field elements onto that entirely naned
		% structure
		result		= subsasgn(result,substructure_array,...
			transformed_elements);
		result_locs = subsasgn(result_locs,substructure_array,logical_t);
		
	end
	return;

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% HELPER FUNCTIONS
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	function [output] = assignToAll(input,what)
		assert(iscell(input));
		
		for i = 1:numel(input)
			if iscell(input{i}) && (numel(input{i}) > 1 || iscell(input{i}{1}))
				assignToAll(input{i}, what)
				error('Nested output storage! Used to be supported but now is not.')
			else
				input{i} = what;
			end
		end
		
		output=input;
		
	end

	%---------------------------------------------------------------------------
	
	function [transformed logical_t]= ...
			transformDimension(thisoutput,nPredim,nPostdim,postinfo)
		
		type = postinfo.type;
		dimr = postinfo.dimr;
		
% 		if nargin <= 3 || isequal(type,'matrix')
			
		newrank = nPredim + nPostdim;
		newdim  = ones(1,newrank);
		warning off;
		newdim = mat2cell(newdim,1,newrank);
		warning on;
		[newdim{end-nPostdim+1:end}] = deal(':');

		S = substruct('()',newdim);

		% Shifts the matrix so that its n-dimensions express in the
		% last two dimensions of a matrix described by predim-dimension
		% prefield cell/matrix plus post-dimension of this output term.
		logical_t = []; transformed = [];
		logical_t	= subsasgn(logical_t,S, ones(dimr.raw));
		thisoutput = sizeAndConvert(thisoutput,dimr.perLevel);
		transformed = subsasgn(transformed,S,thisoutput); % TODO -- flesh out thisoutput to match the size of postinfo.raw!		
		
% 		% Untested!
% 		elseif isequal(type,'cell')
% 			
% 			
% 			
% 		end
		
	end

	function [output] = sizeAndConvert(what,desiredSize, level)
		% The point of this function is to take a tiered cell with a
		% certain dimensionality and obtain the same rank matrix with a
		% dimensionality equal to the desiredSize
		
		if nargin == 2
			level = 0;
		end
		level = level + 1;
		
		%% Fix sizes of child elements!
		if iscell(what)
			% First check if the size matches
			desiredLevelSize = desiredSize{level};
			if numel(desiredLevelSize) == 1
				desiredLevelSize = [1 desiredLevelSize];
			end
			
			currentLevelSize = size(what);
			
			sizeMismatch = ~isequal(currentLevelSize,desiredLevelSize);
			if sizeMismatch
				temp = cell(desiredLevelSize);
				sub_mesh = {};
				% TODO flesh out mismatch code! otherwise can't handle
				% outputs with cells of different sizes
				
			end
			
			for i = 1:numel(what)
				what{i} = sizeAndConvert(what{i}, desiredSize, level);
			end
			
			what = cell2mat(what); % TODO Make sure what dimensions orthogonal against enclosing cell dimesions
			output = what;
			
		else
			if iscell(desiredSize)
				desiredSize = cell2mat(desiredSize(level:end));
				if numel(desiredSize) == 1
					desiredSize = [1 desiredSize];
				end
			end
			% Store current size and assert that we're not being asked to
			% shrink the matrix -- only potentially grow it
			currentSize = size(what);
			assert(sum(currentSize > desiredSize)==0);

			% If the current size is not the desired size then embark on the
			% project of equalizing them
			if ~isequal(currentSize,desiredSize)
				output = NaN*ones(desiredSize);
				sub_prev = cell(1,numel(size(what)));
				[sub_prev{1:numel(size(what))}] = ind2sub(size(what),1:numel(what));
				ind_new = sub2ind(desiredSize,sub_prev{:});
				output(ind_new) = what(1:end);
				
			else
				output = what;
			end
		end
		
	end

	%---------------------------------------------------------------------------
	
	function [maxS,dimToRequest] = findDimensionsPerLevel(cellsizes, level)
        % TODO -- recast this for nested structure!!
		
		if nargin == 1
			level = 0;
			maxS=[];
		end
		
		if level ~= 0 
			maxS = [level size(cellsizes)];
		end
		
		level = level + 1;
		
		for i = 1:numel(cellsizes)
			
			%  Capture the current object!
            this = cellsizes{i};
			this = [level this];
            % 
            if ismatrix(this) && isreal(this) && ~isscalar(this)
                maxS = maxSizes(this,maxS);
            elseif iscell(this)
				
                this = findDimensionsPerLevel(cellsizes{i},level);
                maxS = maxSizes(this,maxS);
			elseif isstruct(this)
                error('Encountered unsupported obj class in the post-field gathering process');
			end		
		end
		
		level = level - 1;
		
		% If we're about to exit, go ahead and calculate the
		% postdimensionality that we will use to represent the post-field
		% dimension
		if level == 0
			if maxS(1,1)==0; maxS(1,:)=[]; end; % Kind of a janky fix, earlier version of this function counted the zeroth level, and I later removed it because I don't need the dimension that counts total number of subscripts
			dimToRequest.raw = ...
				size(squeeze(zeros(reshape(maxS(:,2:end)',1,[]))));
			perLevel = arrayfun( @(x) size(squeeze(zeros(maxS(x,2:end)))) ,...
				1:size(maxS,1), 'UniformOutput',false);
			for i=1:numel(perLevel)
				if perLevel{i}(1)==1;perLevel{i}(1)=[];end;
			end
			dimToRequest.perLevel=perLevel;
		end
			
	end

	function maxS = maxSizes(this,prev)
		
		if isempty(prev)
			maxS=this;
			return;
		end
		
		% Verify both arguments contain all previous levels
		level_list = this(:,1);
		if numel(level_list) < level_list(end)+1
			need_to_add = (level_list(end)-numel(level_list))+1;
			this = [zeros(need_to_add,size(this,2)); this];
			this(1:need_to_add,1) = 0:need_to_add-1;
		end
		% Verify both arguments contain all previous levels
		level_list = prev(:,1);
		if numel(level_list) < level_list(end)+1
			need_to_add = (level_list(end)-numel(level_list))+1;
			prev = [zeros(need_to_add,size(prev,2)); prev];
			prev(1:need_to_add,1) = 0:need_to_add-1;
		end
		
		% Expand column dimensional size if not big enough
		if size(this,2) > size(prev,2)
             dimension_diff = size(this,2)-size(prev,2);
             prev(:,end+1:end+dimension_diff) = 0;
		end
		% Expand row dimensional size if not big enough
		if size(this,1) > size(prev,1)
			dimension_diff = size(this,1)-size(prev,1);
			prev(end+1:end+dimension_diff,1) = ...
			size(prev,1):size(prev,1)+dimension_diff-1;
		end
		
		% Per dimension, compare and select the large of the two
		together(:,:,1) = this;
		together(:,:,2) = prev;
		maxS = max(together, [], 3);
		
	end

	%---------------------------------------------------------------------------
	
	function setOutput(output,sub,rhs_object)
		
		evalstr = 'output';
		for i=1:numel(sub)
			evalstr=[evalstr '{' num2str(sub(i)) '}'];
		end
		evalstr = [evalstr '=rhs_object'];
		eval(evalstr);
		
	end

	%---------------------------------------------------------------------------

	function thisoutput = thisOutput(sub)
		evalstr = 'thisoutput=output';
		for i=1:numel(sub)
			evalstr=[evalstr '{' num2str(sub(i)) '}'];
		end
		eval(evalstr);
	end

end