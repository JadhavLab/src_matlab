function gathered = gatherOutput(modf, gatherspecification)

	% Purpose:	This is part of a second attempt to create a generalized
	%			method of gathering filterframework/neuroquery output
	%			in the way that filterramework/neuroquery generalizes the
	%			execution of analysis on subsets of data. The point is to
	%			get our data out of these structures and coellesce them
	%			into matrices or cells in ways that are almost immediately
	%			plottable or averagable or normalizable.
	%
	%
	% Inputs:	gatherspecification
	%				A struct that follows a very specific forumula, more or
	%				less specifying the fields you want to gather, and any
	%				additional assumptions about how to gather them.

	gs = gatherspecification;
	gs = setdefaults(gs);
	
	global g;
	for g = 1:numel(gs)
		
		%% Obtain gathered pre-field per field
		% Obtain pre-field gather of data
		[output,subs,locmat] = prefieldGather(modf,gs(g).field);
		
		%% probe post field characteristics, per output, per field
		% If post-field gather processing requested, then initiate
		if gs(g).postfieldprocessing
            clear type dim objsize
			
			for s = 1:length(subs)
				thisoutput = thisOutput(subs(s,:));
				[type{s},dim{s}] = probePostfield(thisoutput);				
			end
		
			%% Have to convert to non-nested form if we plan to do post-field processing
			
			%% Execute final transformation, if requested
			if gs(g).postfieldprocessing
			
				output = cellnested2cellnormal(output,subs);

				pre.output = output; pre.subs = subs;
				post.type = type; post.dim = dim;

				[output,locmat] = prepostFinalize(pre,post);
			end
			
		
		end
		
		%% Store gathered field data
		gathered.(gs(g).field)=CollectedData(output,locmat,subs);
        
	end
	
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% HELPER FUNCTIONS
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	function gs = setdefaults(gs)
		
        if ~isfield(gs(1),'postfieldprocessing')
            for gg = 1:numel(gs)
                    gs(gg).postfieldprocessing = true;
                    gs(gg).convert2mat = true;
            end
        end
			
	end

	%---------------------------------------------------------------------------

	function thisoutput = thisOutput(sub)
		evalstr = 'thisoutput=output';
		for i=1:numel(sub)
			evalstr=[evalstr '{' num2str(sub(i)) '}'];
		end
		evalstr=[evalstr ';'];
		eval(evalstr);
    end

	%--------------------------------------------------------------------------
	
	function output = cellnested2cellnormal(input,subs)
		
		output = {};
		
		for i = 1:size(subs,1)
			
			for j = 1:size(subs,2)
				S_in(j) = substruct('{}',{subs(i,j)});
			end
			
			S_out = substruct('{}',mat2cell(subs(i,:),1,numel(subs(i,:))));
			
			output = subsasgn(output,S_out, subsref(input, S_in));
		end
	end

end