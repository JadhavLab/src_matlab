classdef BranchedCell
    % Branched cell x{}{}{}{} ... a class for simplifying using branched
    % cells in general.
    %
    % Inputs
    %
    %
    % The subsref and subsasgn are essentially the heart of this class
    % type, and build all the flexibility needed to address and assign to
    % this a BranchedCell.
    
    properties
        
        data
        size=[];
        dim=0;
        
        occupants
        
    end
    
    methods
        
        % Constructor
        function this = BranchedCell(varargin)
            p=inputParser;
            p.addParameter('preallocate_size',[],@(x) isequal(round(x),x) );
            
        end
        
        % Property setting methods
        function this = set(this,field,fval)
            this.(field)=fval;
        end
        function this = set.size(this,value)
            if iscolumn(value);value=value';end;
            this.size=value;
            this.dim=numel(value);
        end
        
        % Subscript assignment and reference methods
        function B=subsref(this,subscript_sets)
            
            % Re-index
            new_subscript_set=[];
            for subs = subscript_sets
                
                switch subs.type
                    case '()'
                        new_subscript_set = ...
                            [new_subscript_set; this.handle_array(subs)];
                    case '.'
                        new_subscript_set = ...
                            [new_subscript_set; this.handle_field(subs)];
                    case '{}'
                        new_subscript_set = ...
                            [new_subscript_set; this.handle_cell(subs)];
                end
                
            end
            
            if isempty(new_subscript_set)
                B=this.data;
            else
                B=subsref(this.data,new_subscript_set);
            end
            
            
        end
        function this=subsasgn(this,subscript_sets,target_to_assign)
           
            % Re-index
            new_subscript_set=[];
            for subs = subscript_sets
                
                switch subs.type
                    case '()'
                        new_subscript_set = ...
                            [new_subscript_set; this.handle_array(subs)];
                    case '.'
                        new_subscript_set = ...
                            [new_subscript_set; this.handle_field(subs)];
                    case '{}'
                        new_subscript_set = ...
                            [new_subscript_set; this.handle_cell(subs)];
                end
                
            end
            
            % Now use subsasgn
            try
                this.data=subsasgn(this.data,new_subscript_set,target_to_assign);
            catch
                % If we catch an error, some part of the branched cell or
                % struct data structure is unbuilt, so allocate
                
            end
            
        end
        
        % SUBSREF/SUBSASGN HELPER FUNCTIONS: How to handle each assignment op
        function subscript = handle_array(~,subscript)
            % Preallocate the new subscript structure
           new_subs(1:numel(subscript.subs))={struct('type','','subs',{})};
           new_subs=cat(1,new_subs{:});
           % Convert to branched cell
           for s = 1:numel(subscript.subs)
               new_subs(s) = struct('type','{}','subs',{s});
           end
        end
        function sub = handle_cell(~,sub)
            return;
        end
        function sub = handle_field(~,sub)
            return;
        end
        
    end
    
end