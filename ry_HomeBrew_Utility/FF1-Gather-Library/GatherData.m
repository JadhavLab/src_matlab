classdef GatherData
% This is part of a second attempt to create a generalized method of
% gathering filterframework / neuroquery output in the way that
% filterramework / neuroquery generalizes the execution of analysis on
% subsets of data. The point is to get our data out of these structures and
% coellesce them into matrices or cells in ways that are almost immediately
% averagable, manipulable, plottable or otherwise.
%
% This is a static method library, not a true object, in that it lacks
% instances with data. It started as a set of functions I wrote, and I
% eventually moved to make it into a static function object, to keep all of
% the code unified.
    
    %% Public Methods
    methods (Static,Access=public)
        
        %------------------------------------------------------------------
        function gathered = run(modf, gatherspecification)
        %
        % Inputs:       
		%				modf (type = struct)
		%				--------------------
		%				your FF/NQ struct after running the filter function
		%				call
		%				gatherspecification (type = struct)
        %               --------------------
        %				This struct follows a specific forumula.
        %				You provide the fields you wish to gather from
        %				FF/NQ output as field names. Any special options
        %				controlling how the gather works are given as
        %				sub-fields to that particular field requested.
		%
		%				If I want to gather all collected_phases, I say,
		%				I can pass in the filter output and
		%				gatherspecification.collected_phases. This will 

			gs = gatherspecification;
			gs = GatherData.setdefaults(gs);

			global g;
            global numfields;
            numfields = numel(gs);
			for g = 1:numfields

				% Obtain gathered pre-field per field
				% Obtain pre-field gather of data
				[output,subs,locmat] = ...
					GatherData.prefieldStandard(modf,gs(g).field);
				% Probe post field characteristics, per output, per field
				% If post-field gather processing requested, then initiate
				if gs(g).postfieldprocessing
					clear type dim objsize

					for s = 1:length(subs)
						thisoutput = GatherData.thisOutput(output,subs(s,:));
						[type{s},dim{s}] = GatherData.probePostfield(thisoutput);				
					end

					% Have to convert to non-nested form if we plan to do
					% post-field processing

					% Execute final transformation, if requested
					if gs(g).createCollectedData

						output = GatherData.cellnested2cellnormal(output,subs);

						pre.output = output; pre.subs = subs;
						post.type = type; post.dim = dim;

						[output,locmat] = GatherData.prepostFinalize(pre,post);
						
						% If user requests centralization of postfield outputs,
						% then do that
						if gs(g).postfieldcentralize
						end

						% Store gathered field data as instantiated CollectedData
						% object .. this collected data object is a very
						% generalized high dim object that can now be easily
						% algebraically manipulated across/within dimensions.
						gathered.(gs(g).field)=...
							CollectedData(output,locmat,subs,post.dim);
					end					
				end
				
				if ~(gs(g).postfieldprocessing && gs(g).createCollectedData)
					gathered.(gs(g).field).out	=	output;
					gathered.(gs(g).field).subs =	subs;
				end

			end
			return
			
        end   
        %------------------------------------------------------------------
        function [outputs,valid_subs,valid_logical] = ...
            prefieldStandard(modf,field,varargin)
            % Name:		probePreField(modf, field)
            % Purpose:	This is part of a second attempt to create a generalized
            %			method of gathering filterframework/neuroquery
            %			output in the way that filterramework/neuroquery
            %			generalizes the execution of analysis on subsets of
            %			data. The point is to get our data out of these
            %			structures and coellesce them into matrices or
            %			cells in ways that are almost immediately plottable
            %			or averagable or normalizable.
            %
            %			This particular function is part of that suite. It
            %			takes an output field, a field outputted by your
            %			filterfunction, and then finds all of the
            %			dimensions that index finding a single term of that
            %			field, as well as recasts the output so that its a
            %			cell containing that output, nested or not, where
            %			each i,j,k,... indexed cellular term contains that
            %			field.
            %			
            %			The followup to this function is probePostField
            %			which gathers dimensions within the collected
            %			fields gotten by this preField, containing
            %			dimensions A1,A2, ... , B1, B2, ... where A's are
            %			dimensions pre-output field, and B's are dimensions
            %			post-output field.


            % Set default parameters
            nested_output = true;
            convert2mat = false;
            % Read in optional arguments
            for i = 1:2:numel(varargin)		
                switch varargin{i}
                    case 'nested', nested_output = varargin{i+1};
                    case 'convert2mat', convert2mat = varargin{i+1};
                    otherwise, warning('Input not recognized.');
                end		
            end


            % Figure out the dimensionality of the prefield term!
            %  and re-cast the output for this field as a cell/matrix by itself

            % INITIALIZE TRACKERS
            valid_logical = zeros(1,1,1,1,1,1,1); 
            outputs = {};

            % Total number of animals that have been analyzed
            nAnim = numel(modf);

            for a = 1:nAnim

                % This variable keeps track of which row we're looking at in the
                % output of this animal ...
                outputCounter = 0;

                % Number of filters different filter sets that have been applied
                nFilt = numel( modf(a).epochs );
                outputs{a} = {};
                for f = 1:nFilt

                    % Number of unique [day, epoch] pairs that have been analyzed
                    % for this animal, this filter
                    nUniqueEpochs = size( modf(a).epochs{f}, 1);
                    ep = modf(a).epochs{f};
                    outputs{a}{f} = {};
                    uE1_notinitialized = true;
                    for uE = 1: nUniqueEpochs



                        % Number of unique electrophysiological parameters analyzed
                        % for this particular animal, filter, unique epoch
                        nEfizzElements_thisEpoch = size( modf(a).eegdata{f}{uE}, 1);
                        ef = modf(a).eegdata{f}{uE};
                        if uE1_notinitialized
                            outputs{a}{f}{ep(uE,1)} = {};
                            uE1_notinitialized = false;
                        end
                        outputs{a}{f}{ep(uE,1)}{ep(uE,2)} = {};
                        uF1_notinitialized = true;
                        for uF = 1:nEfizzElements_thisEpoch


                            outputCounter = outputCounter + 1;

                            % If this output contains something, then we ought to
                            % mark that we have a valid index and store the
                            % result!
                            this_output = modf(a).output{f}(outputCounter).(field);
                            if ~isempty(this_output)

                                % If nested output is requested, store nested cell,
                                % else as a singular rectangular cell
                                if nested_output
                                    if uF1_notinitialized
                                        uF1_notinitialized = false;
                                        outputs{a}{f}{ep(uE,1)}{ep(uE,2)}{ef(uF,1)} = {};
                                    end
                                    outputs{a}{f}{ep(uE,1)}{ep(uE,2)}{ef(uF,1)}{ef(uF,2)} = ...
                                        this_output;
                                else
                                    outputs{a,f,uE(1),uE(2),uF(1),uF(2)} = this_output
                                end

                                % Place a 1 down in our valid_location_matrix to
                                % indicate that, yes, this particular location is
                                % valid!
                                valid_logical(a,f,ep(uE,1),ep(uE,2),...
                                    ef(uF,1),ef(uF,2)) = 1;
                            end

                        end
                    end
                end
            end

            % Cast into logical if it's not already one
            valid_logical = logical(valid_logical);

            % Compute more information about the valid index locations!
            valid_inds = find(valid_logical == 1);
            valid_subs = cell(1,numel(size(valid_logical)));
            [valid_subs{:}] = ind2sub(size(valid_logical),valid_inds);
            valid_subs = cell2mat(valid_subs);

            % Handle optional convert2mat step
            if convert2mat
                outputs = cell2mat(outputs);
            end
        end
    end
    
    %% Private Methods
    methods (Static,Access=private)
        
        %------------------------------------------------------------------
        % (Dependencies) gather, Post/Pre, super methods
        %------------------------------------------------------------------
        function gs = setdefaults(gs)
            % Sets default options for the gatherspecification structure,
            % if user did not provide them
            for gg = 1:numel(gs)
                if ~isfield(gs(gg),'postfieldprocessing') ...
                        || isempty(gs(gg).postfieldprocessing)
                    gs(gg).postfieldprocessing      = true;
				end
				if ~isfield(gs(gg),'createCollectedData') || ...
                        isempty(gs(gg).createCollectedData)
                    gs(gg).createCollectedData      = true;
                end
                if ~isfield(gs(gg),'convert2mat') || ...
                        isempty(gs(gg).convert2mat)
                    gs(gg).convert2mat              = true;
                end
                if ~isfield(gs(gg),'postfieldcentralize') || ...
                        isempty(gs(gg).postfieldcentralize)
                    gs(gg).postfieldcentralize      = false;
                end
            end
        end
        %------------------------------------------------------------------
        function thisoutput = thisOutput(output,sub)
			% Purpose of this function is to be able to address a nested
			% cellular output contains FF/NQ outputs for a single field,
			% without visually busying the parent function. Doing this in
			% an automated fashion for a subscript list can look extremely
			% messy.
            evalstr = 'thisoutput=output';
            for i=1:numel(sub)
                evalstr=[evalstr '{' num2str(sub(i)) '}'];
            end
            evalstr=[evalstr ';'];
            eval(evalstr);
        end
        %------------------------------------------------------------------
        function output = cellnested2cellnormal(input,subs)
			% Purpose: Converts a nested cell type {}{}{} into a normal
			% cell type { , , ,}.
            output = {};
            for i = 1:size(subs,1)
                for j = 1:size(subs,2)
                    S_in(j) = substruct('{}',{subs(i,j)});
				end
				warning off;
                S_out = substruct('{}',mat2cell(subs(i,:),1,numel(subs(i,:))));
				warning on;
                output = subsasgn(output,S_out, subsref(input, S_in));
            end
        end

        %------------------------------------------------------------------
        % (Dependencies) prepostFinalize and related methods
        %------------------------------------------------------------------
        function [result, result_locs] = prepostFinalize(pre,post)

        % Obtain pre and post data
        output = pre.output; subs=pre.subs;
        postdim = post.dim;

        % Obtain max size of post-field levels - we need to know how big to
        % make our matrix to encompass its full dimensionality
        [~, dimrequest]= GatherData.findDimensionsPerLevel(postdim); %TODO! first dimension is NOT relevant
        postinfo.type = post.type; postinfo.dimr = dimrequest;

        % Count the number of dimensions that exist "before" and "after" the
        % field that the user requested tgo gather in the parent function
        nPredim = size(subs,2);
        nPostdim = numel(dimrequest.raw);

        % Create sparse matrix to hold result
        prealloc = prod(dimrequest.raw)  * size(subs,1);
        result = ndSparse.build([],[],[size(pre.output) dimrequest.raw],prealloc);
        result_locs = ndSparse.build([],[],[size(pre.output) dimrequest.raw],prealloc);

        global g;
        global numfields;
        total = size(subs,1);
        for s = 1:size(subs,1)
            fprintf('Processing field %d, element %d ...\n',g,s);
            if exist('multiWaitbar.m','file')
                multiWaitbar('Post-field Processing',...
                    (s+(g-1)*size(subs,1))/(numfields*size(subs,1)));
            end

            % These will be used to address our cell and array objects flexibly
            % with subsasgn and subsref
            warning off;
            cell_address = mat2cell(subs(s,:),1,numel(subs(s,:)));
            warning on;
            array_address = cell_address;
            [array_address{end+1:end+nPostdim}] = deal(':');

            substructure_array = substruct('()',array_address);
            substructure_cell = substruct('{}',cell_address);

            % Create specialized versions of our field elements transposed out
            % of the N-d axis of prefield terms, and also one for pad elements
            % sized to the largest field element to enable use to place
            % vairably lengthed elements in our matrix
            [transformed_elements, logical_t] = ...
                GatherData.transformDimension(subsref(output,substructure_cell),...
                nPredim,nPostdim,postinfo);

            % First population the entire new cellular structure with pad
            % elements and then assign field elements onto that entirely naned
            % structure
            result		= subsasgn(result,substructure_array,...
                transformed_elements);
            result_locs = subsasgn(result_locs,substructure_array,logical_t);

        end
        return;
        end  
        % -----------------------------------------------------------------
        function [transformed logical_transformed]= ...
                transformDimension(untransformed,nPredim,nPostdim,postinfo)
            % Purpose of this method is to transform the dimensions of the
            % post-field objects such that (A) they match the total
            % dimensionality of the gather object we're creating (pre and
            % postfield dimensions) and (B) such that their information
            % points is expressed orthogonal to the prefield dimensions,
            % that is, if the prefield contains 6 dimensions, (animal
            % filter day ... blah blah blah) and the post field contains 2
            % (phase-bin specific-phase-sample), then the post-field term
            % is remade into a (1,1,1,1,1,1,:,:) ND sparse matrix, with its
            % data subspaced along the last two dimensions.

            newrank = nPredim + nPostdim;
            newdim  = ones(1,newrank);
            warning off;
            newdim = mat2cell(newdim,1,newrank);
            warning on;
            [newdim{end-nPostdim+1:end}] = deal(':');

            S = substruct('()',newdim);

            % Shifts the matrix so that its n-dimensions express in the
            % last two dimensions of a matrix described by predim-dimension
            % prefield cell/matrix plus post-dimension of this output term.
            logical_transformed = []; transformed = [];
%             logical_t	= subsasgn(logical_t,S, ones(dimr.raw));
            [untransformed, logical_untransformed] = ...
				GatherData.sizeAndConvert(untransformed,postinfo.dimr.perLevel);
            transformed = subsasgn(transformed,S,untransformed); % TODO -- flesh out thisoutput to match the size of postinfo.raw!		
			logical_transformed = ...
				subsasgn(logical_transformed,S,logical_untransformed);

        end
        % -----------------------------------------------------------------
        function [what,whereMarker] = sizeAndConvert(what,desiredSize,level,higherDim)
            % The point of this function is to take a tiered cell with a
            % certain dimensionality and obtain the same rank matrix with a
            % dimensionality equal to the desiredSize

            if nargin == 2
                level = 0;
                higherDim = 0;
            end
            level = level + 1;

            % Fix sizes of child elements!
            if iscell(what) % CELL MODE
                % First check if the size matches!
				
				% Here, we take information about the size
                desiredLevelSize = desiredSize{level};
                if numel(desiredLevelSize) == 1, % if there's only one level, then we do something special, adding a dimension
                    desiredLevelSize = [1 desiredLevelSize];
				end
				% Store the current size
                currentLevelSize = size(what);

				% Now, detect a mismatch, and if so handle it
                sizeMismatch = ~isequal(currentLevelSize,desiredLevelSize);
                if sizeMismatch
                    temp = cell(desiredLevelSize);
                    sub_mesh = {};
                    % TODO flesh out mismatch code! otherwise can't handle
                    % outputs with cells of different sizes

                end
                % Now that things can be assumed to match, let's focus on
                % properly sizing the children elements, recursively!
                dimensionsHere = numel(desiredSize{level});
                for i = 1:numel(what)
                    [what{i},whereMarker{i}] = GatherData.sizeAndConvert(what{i}, ...
						desiredSize, level, higherDim+dimensionsHere);
				end
				
                % Now that we have all the re-sized children, combine them!
				
				% Special case: 1xN vectors
                if ndims(what)==2 && sum(size(what)==1)==1
                    what			= permute(what,[2 1]);
					whereMarker		= permute(whereMarker,[2 1]);
				end
				% Everything else:
                for i = 1:dimensionsHere
                    pad				= desiredSize{level}(i)-size(what,i);
                    what			= cat(i,what{:});
                    what			= padarray(what,pad,0,'post');
					whereMarker		= cat(i,whereMarker{:});
					whereMarker		= padarray(whereMarker,pad,0,'post');
				end
				% Now store the transformed result
				%what = what;

			else % NUMERIC/MATRIX MODE
                if iscell(desiredSize)
                    desiredSize = cell2mat(desiredSize(level:end));
                    if numel(desiredSize) == 1
                        desiredSize = [1 desiredSize];
                    end
                end
                % Store current size and assert that we're not being asked to
                % shrink the matrix -- only potentially grow it
                currentSize = size(what);
                assert(sum(currentSize > desiredSize)==0);

                % If the current size is not the desired size then embark on the
                % project of equalizing them
                if ~isequal(currentSize,desiredSize)
					%Preallocate the new matrix for the data, resized and
					%the logical matrix that keeps track of which spaces in
					%the resized matrix correctly house data, i.e., which
					%zeros are actually data zeros, and not just padding.
                    what = zeros(desiredSize);
					whereMarker = zeros(desiredSize);
					% Obtain the subscripts to assign to
                    sub_prev = cell(1,numel(size(what)));
                    [sub_prev{1:numel(size(what))}] = ...
						ind2sub(size(what),1:numel(what));
                    ind_new = sub2ind(desiredSize,sub_prev{:});
					% Assign the data to the correct subscripts, and mark
					% 1's where the data lives
                    what(ind_new) = what(1:end);
					whereMarker(ind_new)=1;
                else
                   whereMarker=ones(size(what));
                end
			end
            
			%POST-PROCESSING! -- whether it processed a cell or numeric,
			%this is run ... rotates data into the correct dimension so
			%they wind up expected locations when concatonate them into a
			%larger numeric matrix.
            %Make sure that the output points into the dimensions on its
            %level!
            newshape = zeros(1,ndims(what)+higherDim);
            newshape(higherDim+1:end) = 1:ndims(what);
            newshape(1:higherDim) = ndims(what)+1:ndims(what)+higherDim; 
            what = permute(what,newshape);
			whereMarker = permute(whereMarker,newshape);

        end
        %------------------------------------------------------------------
        function [maxS,dimToRequest] = findDimensionsPerLevel(cellsizes, level)
            % TODO -- recast this for nested structure!!

            if nargin == 1
                level = 0;
                maxS=[];
            end

            if level ~= 0 
                maxS = [level size(cellsizes)];
            end

            level = level + 1;

            for i = 1:numel(cellsizes)

                %  Capture the current object!
                this = cellsizes{i};
                this = [level this];
                % 
                if ismatrix(this) && isreal(this) && ~isscalar(this)
                    maxS = GatherData.maxSizes(this,maxS);
                elseif iscell(this)

                    this = GatherData.findDimensionsPerLevel(cellsizes{i},level);
                    maxS = GatherData.maxSizes(this,maxS);
                elseif isstruct(this)
                    error('Encountered unsupported obj class in the post-field gathering process');
                end		
            end

            level = level - 1;

            % If we're about to exit, go ahead and calculate the
            % postdimensionality that we will use to represent the post-field
            % dimension
            if level == 0
                if maxS(1,1)==0; maxS(1,:)=[]; end; % Kind of a janky fix, earlier version of this function counted the zeroth level, and I later removed it because I don't need the dimension that counts total number of subscripts
                dimToRequest.raw = ...
                    size(squeeze(zeros(reshape(maxS(:,2:end)',1,[]))));
                perLevel = arrayfun( @(x) size(squeeze(zeros(maxS(x,2:end)))) ,...
                    1:size(maxS,1), 'UniformOutput',false);
                for i=1:numel(perLevel)
                    if perLevel{i}(1)==1;perLevel{i}(1)=[];end;
                end
                dimToRequest.perLevel=perLevel;
            end

        end
        % -----------------------------------------------------------------
        function maxS = maxSizes(this,prev)

            if isempty(prev)
                maxS=this;
                return;
            end

            % Verify both arguments contain all previous levels
            level_list = this(:,1);
            if numel(level_list) < level_list(end)+1
                need_to_add = (level_list(end)-numel(level_list))+1;
                this = [zeros(need_to_add,size(this,2)); this];
                this(1:need_to_add,1) = 0:need_to_add-1;
            end
            % Verify both arguments contain all previous levels
            level_list = prev(:,1);
            if numel(level_list) < level_list(end)+1
                need_to_add = (level_list(end)-numel(level_list))+1;
                prev = [zeros(need_to_add,size(prev,2)); prev];
                prev(1:need_to_add,1) = 0:need_to_add-1;
            end

            % Expand column dimensional size if not big enough
            if size(this,2) > size(prev,2)
                 dimension_diff = size(this,2)-size(prev,2);
                 prev(:,end+1:end+dimension_diff) = 0;
            end
            % Expand row dimensional size if not big enough
            if size(this,1) > size(prev,1)
                dimension_diff = size(this,1)-size(prev,1);
                prev(end+1:end+dimension_diff,1) = ...
                size(prev,1):size(prev,1)+dimension_diff-1;
            end

            % Per dimension, compare and select the large of the two
            together(:,:,1) = this;
            together(:,:,2) = prev;
            maxS = max(together, [], 3);

		end
        %------------------------------------------------------------------
        % -----------------------------------------------------------------
        % Probe Post Field and related methods
        % -----------------------------------------------------------------
        function [type, dim] = probePostfield(outterm)
            % Name:		probePostField(modf, field)
            % Purpose:	This is part of a second attempt to create a generalized
            %			method of gathering filterframework/neuroquery output
            %			in the way that filterramework/neuroquery generalizes the
            %			execution of analysis on subsets of data. The point is to
            %			get our data out of these structures and coellesce them
            %			into matrices or cells in ways that are almost immediately
            %			plottable or averagable or normalizable.
            %
            %			This particular function gets the other "half of the
            %			equation", the index and datatype situation for a field! 

            % Get dimensions of post-field term!

            type = class(outterm); 
            if ~isequal(type,'cell') && ~isequal(type,'struct')
                type = 'matrix';
            end
            dim = size(outterm);

            if ~isscalar(outterm)
                % Acquire the index for this subscript combo
                index = prod(dim);

                if		isequal(type, 'cell')
                    for i = 1:index 
                        [type_temp{i}, dim_temp{i}] = ...
                            GatherData.probePostfield(outterm{i});
                    end
                elseif	isequal(type, 'matrix')
                    [type_temp, dim_temp] = ...
                        GatherData.probePostfield(outterm(index));
                elseif	isequal(type, 'struct')
                    error('Nested structs not supported yet.');
                end

                if ~isempty(type_temp) && ~isempty(dim_temp)

                    objsize=dim;
                    subs={};type={};dim={};
                    for i=1:index
                        [subs{1}, subs{2}, subs{3}, subs{4}, subs{5}, subs{6}, subs{7}, subs{8}] ... TODO - Make this variable length, right now can only handle postfields of up to 8 dimensions
                            = ind2sub(objsize,i);
                        substructure=substruct('{}',subs);
                        type=subsasgn(type,substructure,type_temp{i});
                        dim=subsasgn(dim,substructure,dim_temp{i});
                    end                

                end
            else
                type = [];
                dim = [];
                objsize = [];
            end
        end
    end
end
