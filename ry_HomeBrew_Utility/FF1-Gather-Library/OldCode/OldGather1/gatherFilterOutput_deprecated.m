%% -----------------------------------------------------------------------------
% Name:			gatherDimensions
% Purpose:		The purpose of this function is to automatically gather
%				data across any set of dimensions for any filter outputs. I
%				think the data gathering is such a frequently performed
%				step that it merits a function that can get the data
%				gathering up off the ground. This will be an ongoing
%				project, as no doubt the number of ways to gather data and
%				process it will be difficult to anticipate at the outset.
%
% Inputs:		
% Outputs:		
function fo = gatherFilterOutput(modf,varargin)

%% Parameter Parsing

% Variable storing the fields to gather
field_count = 0;
field_name = {};
% Variable storing the dimensions to gather across
dimension_list = [];
% Variable storing per dimension whether to gather via cell or matrix
matrix_output = 0; % controls whether gather occurs into matrix or cell
% Process the variable argument input list
for i = 1:2:numel(varargin)
	switch varargin{i}
		case 'field_names'
			field_count = numel(varargin{i+1});
			field_name = varargin{i+1};
		case 'dimension_list'
			dimension_list = varargin{i+1};
		case 'matrix_type'
			matrix_output = varargin{i+1};
		case 'matrix_singlular'
			singular = varargin{i+1};
		otherwise
			warning('Not a recognized input ... skipping ...');
	end
end

%% Figure out available tuples and store up the outputs.
tuples = [];
filt_addr = [];
outputs = {};
count = 0;

										animal_number= numel(modf);
for an = 1:animal_number;				number_of_filters = size(modf(an).epochs,1);
for filt = 1:number_of_filters;			tot_dayep = size(modf(an).epochs{filt},2);
for dayep = 1:tot_dayep;				tot_eegcell_set = size(modf(an).eegdata{filt},2);
for alltetcell = 1:tot_eegcell_set;		eegcell_in_set = size(modf(an).eegdata{filt}{alltetcell},1);
for tc = 1:eegcell_in_set
	
		count = count + 1;
		tuples = [tuples; ...
			an filt modf(an).epochs{filt}(dayep,:) modf(an).eegdata{filt}{alltetcell}(tc,:)];
		filt_addr = [filt_addr; ...
			tot_dayep dayep eegcell_in_set tc];
		
		
		which_output = sub2ind([tot_dayep,eegcell_in_set], dayep,tc);
		outputs{count} = modf(an).output{filt}(which_output);
		
end
end
end
end	
end

%% Clear up the memory held by the modf input
clear modf

%% Decide how to collapse the outputs
total_dims = size(tuples,2);
alldim = 1:total_dims;
gather = ismember(alldim,dimension_list);

unique_ungathered = unique( tuples(:,alldim(~gather)), 'rows' );
ungathered = tuples(:,alldim(~gather));

%% Create eval string that will use
finaloutput_str = 'fo';
for i = 1:size(unique_ungathered,2)
	finaloutput_str = [finaloutput_str sprintf('{unique_id(%d)}',i)];
end
formatrix = '.(field_name{f})(:,j) = outputs{curr_row}.(field_name{f});';
forsingular = ['.(field_name{f})(:,j) =',...
	'orient_to_row(outputs{curr_row}.(field_name{f}));'];
forcell = '.(field_name{f}){j} = outputs{curr_row}.(field_name{f});';

%% Gather!
fo={};
for f = 1:field_count
% 	fo.(field_name{f}) = [];
	
	for i = 1:size(unique_ungathered,1)
		
		% Non-gather,i.e., unique id component, that ought to remain separate
		unique_id = unique_ungathered(i,:);
		
		
		matching_rows = find(ismember(ungathered,unique_ungathered,'rows'));
		for j = 1:numel(matching_rows)
			
			% Address the current matching row
			curr_row = matching_rows(j);
			
			% Place gathered input into a matrix if the function input
			% calls for it, otherwise put into a cell
			if matrix_output
				if exist('singular','var') && singular
					eval([finaloutput_str formatrix]);
				else
					eval([finaloutput_str formatrix]);
				end
			else
				eval([finaloutput_str forcell]);
			end
			
		end
	end
end

%% HELPER FUNCTIONS

	function out = orient_to_row(x)
		[p,q] = size(x);
		if q > p && p == 1
			out = x';
		else 
			out = reshape(x,[],1);
		end
	end
	
end
