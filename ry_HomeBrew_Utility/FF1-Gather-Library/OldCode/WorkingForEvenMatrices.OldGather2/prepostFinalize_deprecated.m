function [cell_with_outdims_realigned, logical_output]...
    = prepostFinalize(pre,post)
	
	% Obtain pre and post data
	output = pre.output; subs=pre.subs;
	posttype=post.type; postdim = post.dim; postsize=post.size;
	
	% Obtain largest size of field elements
	largestSize = findLargestSizedLeaf(postsize);
	
	% Capture the number of dimensions that exist "before" and "after" the
	% field that the user requested to gather in the parent function
	nPredim = size(subs,2);
	nPostdim = numel(largestSize);
	
    % Fill a new output structure with nans sized to the maximum dimension
    % needed to hold any matrix element in the output
	[transformed_all_nan ~] = ...
			transformDimension( NaN * ones(largestSize),...
			nPredim,nPostdim,posttype{1});
	output_nan = assignToAll(output,transformed_all_nan);
	
	for s = 1:size(subs,1)
		

        % Populate the subsscripts we'll use to automatically assign
        % these variable length nested structures
        substructure_elementset = substruct('{}',...
            mat2cell(subs(s,:),1,numel(subs(s,:)))...
            ); 
        % TODO .. change this now to accomadate new non-nested cellular structure

		
		nPostdim = numel(postdim{s});
		
		% Create specialized versions of our field elements transposed out
		% of the N-d axis of prefield terms, and also one for NaN elements
		% sized to the largest field element to enable use to place
		% vairably lengthed elements in our matrix
		[transformed_elements logical_t] = ...
			transformDimension(subsref(output,substructure_elementset),...
			nPredim,nPostdim,posttype{s});
		
		% First population the entire new cellular structure with NaN
		% elements and then assign field elements onto that entirely naned
		% structure
		output_nan_withelem = subsasgn(output_nan,substructure_elementset,...
			transformed_elements);
		logical_output = subsasgn(output_nan,substructure_elementset,logical_t);
		
	end
	
	cell_with_outdims_realigned = output_nan_withelem;
	return;

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% HELPER FUNCTIONS
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	function [output] = assignToAll(input,what)
		assert(iscell(input));
		
		for i = 1:numel(input)
			if iscell(input{i}) && (numel(input{i}) > 1 || iscell(input{i}{1}))
				assignToAll(input{i}, what)
			else
				input{i} = what;
			end
		end
		
		output=input;
		
	end

	%---------------------------------------------------------------------------
	
	function [transformed logical_t]= ...
			transformDimension(thisoutput,nPredim,nPostdim,posttype)
		
		if nargin <= 3 || isequal(posttype,'matrix')
			
			newrank = nPredim + nPostdim;
			newdim  = ones(1,newrank);
			newdim = mat2cell(newdim,1,newrank);
			[newdim{end-nPostdim+1:end}] = deal(':');
			
			S = substruct('()',newdim);
			
			% Shifts the matrix so that its n-dimensions express in the
			% last two dimensions of a matrix described by predim-dimension
			% prefield cell/matrix plus post-dimension of this output term.
			logical_t = []; transformed = [];
			logical_t	= subsasgn(logical_t,S, ones(size(thisoutput)));
			transformed = subsasgn(transformed,S,thisoutput);
			
		
		% Untested!
		elseif isequal(posttype,'cell')
		end
		
	end

	%---------------------------------------------------------------------------
	
	function largestSize = findLargestSizedLeaf(cellsizes)
        % TODO -- recast this for nested structure!!
		
		largestSize = [0 0];
		for i = 1:numel(cellsizes)
            
            this = cellsizes{i};
            
            if ismatrix(this) && isreal(this)
                assert(isrow(this));

                if numel(this) > numel(largestSize)
                    dimension_diff = numel(this)-numel(largestSize);
                    largestSize(end+1:end+dimension_diff) = 0;
                end

                largestSize = max( [largestSize; this], [], 1);
            
            elseif iscell(this)
                this = findLargestSizedLeaf(cellsizes{i});
                largestSize = max( [largestSize; this], [], 1);
            else
                error('Encountered unsupported obj class in the post-field gathering process');
            end
                
			
		end
			
	end

	%---------------------------------------------------------------------------
	
	function setOutput(output,sub,rhs_object)
		
		evalstr = 'output';
		for i=1:numel(sub)
			evalstr=[evalstr '{' num2str(sub(i)) '}'];
		end
		evalstr = [evalstr '=rhs_object'];
		eval(evalstr);
		
	end

	%---------------------------------------------------------------------------

	function thisoutput = thisOutput(sub)
		evalstr = 'thisoutput=output';
		for i=1:numel(sub)
			evalstr=[evalstr '{' num2str(sub(i)) '}'];
		end
		eval(evalstr);
	end

end