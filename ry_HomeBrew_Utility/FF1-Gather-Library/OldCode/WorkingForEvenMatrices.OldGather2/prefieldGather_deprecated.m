function [outputs,valid_subs,valid_logical] = prefieldGather(modf,field,varargin)
	% Name:		probePreField(modf, field)
	% Purpose:	This is part of a second attempt to create a generalized
	%			method of gathering filterframework/neuroquery output
	%			in the way that filterramework/neuroquery generalizes the
	%			execution of analysis on subsets of data. The point is to
	%			get our data out of these structures and coellesce them
	%			into matrices or cells in ways that are almost immediately
	%			plottable or averagable or normalizable.
	%
	%			This particular function is part of that suite. It takes an
	%			output field, a field outputted by your filterfunction, and
	%			then finds all of the dimensions that index finding a
	%			single term of that field, as well as recasts the output so
	%			that its a cell containing that output, nested or not,
	%			where each i,j,k,... indexed cellular term contains that
	%			field.
	%			
	%			The followup to this function is probePostField which
	%			gathers dimensions within the collected fields gotten by
	%			this preField, containing dimensions A1,A2, ... , B1, B2,
	%			... where A's are dimensions pre-output field, and B's are
	%			dimensions post-output field.

	
	% Set default parameters
	nested_output = true;
	convert2mat = false;
	% Read in optional arguments
	for i = 1:2:numel(varargin)		
		switch varargin{i}
			case 'nested', nested_output = varargin{i+1};
			case 'convert2mat', convert2mat = varargin{i+1};
			otherwise, warning('Input not recognized.');
		end		
	end


	%% Figure out the dimensionality of the prefield term!
	%  and re-cast the output for this field as a cell/matrix by itself
	
	% INITIALIZE TRACKERS
	valid_logical = zeros(1,1,1,1,1,1,1); 
	outputs = {};
	
	% Total number of animals that have been analyzed
	nAnim = numel(modf);
	
	for a = 1:nAnim
		
		% This variable keeps track of which row we're looking at in the
		% output of this animal ...
		outputCounter = 0;
		
		% Number of filters different filter sets that have been applied
		nFilt = numel( modf(a).epochs );
		outputs{a} = {};
		for f = 1:nFilt
			
			% Number of unique [day, epoch] pairs that have been analyzed
			% for this animal, this filter
			nUniqueEpochs = size( modf(a).epochs{f}, 1);
			ep = modf(a).epochs{f};
			outputs{a}{f} = {};
			uE1_notinitialized = true;
			for uE = 1: nUniqueEpochs
				
				
				
				% Number of unique electrophysiological parameters analyzed
				% for this particular animal, filter, unique epoch
				nEfizzElements_thisEpoch = size( modf(a).eegdata{f}{uE}, 1);
				ef = modf(a).eegdata{f}{uE};
				if uE1_notinitialized
					outputs{a}{f}{ep(uE,1)} = {};
					uE1_notinitialized = false;
				end
				outputs{a}{f}{ep(uE,1)}{ep(uE,2)} = {};
				uF1_notinitialized = true;
				for uF = 1:nEfizzElements_thisEpoch
					
					
					outputCounter = outputCounter + 1;
					
					% If this output contains something, then we ought to
					% mark that we have a valid index and store the
					% result!
					this_output = modf(a).output{f}(outputCounter).(field);
					if ~isempty(this_output)
						
						% If nested output is requested, store nested cell,
						% else as a singular rectangular cell
						if nested_output
							if uF1_notinitialized
								uF1_notinitialized = false;
								outputs{a}{f}{ep(uE,1)}{ep(uE,2)}{ef(uF,1)} = {};
							end
							outputs{a}{f}{ep(uE,1)}{ep(uE,2)}{ef(uF,1)}{ef(uF,2)} = ...
								this_output;
						else
							outputs{a,f,uE(1),uE(2),uF(1),uF(2)} = this_output
						end
					
						% Place a 1 down in our valid_location_matrix to
						% indicate that, yes, this particular location is
						% valid!
						valid_logical(a,f,ep(uE,1),ep(uE,2),...
							ef(uF,1),ef(uF,2)) = 1;
					end
					
				end
				
				
			end
			
		end
		
	end
	
	% Cast into logical if it's not already one
	valid_logical = logical(valid_logical);
		
	%% Compute more information about the valid index locations!
	
	valid_inds = find(valid_logical == 1);
	valid_subs = cell(1,numel(size(valid_logical)));
	[valid_subs{:}] = ind2sub(size(valid_logical),valid_inds);
	valid_subs = cell2mat(valid_subs);
	
	%% Handle optional convert2mat step
	
	if convert2mat
		output = cell2mat(outputs);
	end
	

end