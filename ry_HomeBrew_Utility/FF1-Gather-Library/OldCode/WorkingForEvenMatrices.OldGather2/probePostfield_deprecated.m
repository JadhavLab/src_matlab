function [type, dimensionality, objsize] = probePostfield(outterm)
	% Name:		probePostField(modf, field)
	% Purpose:	This is part of a second attempt to create a generalized
	%			method of gathering filterframework/neuroquery output
	%			in the way that filterramework/neuroquery generalizes the
	%			execution of analysis on subsets of data. The point is to
	%			get our data out of these structures and coellesce them
	%			into matrices or cells in ways that are almost immediately
	%			plottable or averagable or normalizable.
	%
	%			This particular function gets the other "half of the
	%			equation", the index and datatype situation for a field! 

	%% Get dimensions of post-field term!
	
	type = class(outterm); 
	if ~isequal(type,'cell') && ~isequal(type,'struct')
		type = 'matrix';
	end
	dimensionality = size(outterm);
	
	if ~isscalar(outterm)
		
		% Acquire the index for this subscript combo
		objsize=dimensionality;
		index = prod(dimensionality);
		
		if		isequal(type, 'cell')
			[type_temp, dimensionality_temp,objsize_temp] = ...
				probePostfield(outterm{index});
		elseif	isequal(type, 'matrix')
			[type_temp, dimensionality_temp,objsize_temp] = ...
				probePostfield(outterm(index));
		elseif	isequal(type, 'struct')
			error('Nested structs not supported yet.');
		end
		
		if ~isempty(type_temp) && ~isempty(dimensionality_temp)
			type = strvcat(type, type_temp);
            if iscell(dimensionality_temp)
                newdimensionality = ...
                    {dimensionality, deal(dimensionality_temp{:}) };
            elseif ismatrix(dimensionality_temp)
                newdimensionality = {dimensionality, dimensionality_temp};
            end
            if iscell(objsize_temp)
                objsize = {objsize, deal(objsize_temp{:})};
            elseif ismatrix(objsize_temp)
                objsize = {objsize, objsize_temp};
            end
			
		end
	else
		type = [];
		dimensionality = [];
		objsize = [];
	end
		


end
