function [status filename_last filename_this]= checkpointQuery(sessionID,savedir,checkpointNum,varDelete)

    status = true;
    
    filename_this=fullfile(savedir,[sessionID '_checkpoint' num2str(checkpointNum) '.mat']);
    filename_last=fullfile(savedir,[sessionID '_checkpoint' num2str(checkpointNum-1) '.mat']);

    %% Save for query checpointNum-1
    if ~exist(filename_this,'file')
        status=true;
        if exist('varDelete','var')
            code = ['save(''test'', ''-regexp'', ''^(?!',varDelete, ',$).'');']
        else
            code=['save(''' filename_this '''' ');'];
        end
        evalin('caller',code);
    else
        status=false;
        code=['load(''' filename_this ''');'];
        evalin('caller',code);
    end
    
    if status && exist(filename_last,'file')
        status=false;
        code=['load(''' filename_last ''');'];
        evalin('caller',code);
    end
    
end