function savestruct(datastruct, animaldir, animalprefix, datatype,depth) 
% savedatastruct(datastruct, animaldir, animalprefix, datatype)
%
% Save the components of a data cell array.  Arbitrary depth in
% day,epoch,tetrodes,cells,trials, controlled by the depth variable

for d = 1:length(datastruct)
    if (~isempty(datastruct{d}))
	eval(['clear ', datatype]);
	eval(sprintf('%s{%d} = datastruct{%d};', datatype, d, d));
	%[datatype, '{', = ', datastruct, '{', num2str(d), '};']);
	eval(sprintf('save %s%s%s%02d.mat %s', animaldir, animalprefix, datatype, d, datatype));
    end
end
