function [eegvar] = iseeg2var(varname)
% eegvar = iseegvar(varname)
%
% returns 1 if varname is the name of an eeg2 variable and 0 otherwise
%
% eeg2 variables are variables computed using data from two tetrodes. So
% far the only such variable is cgramc, which contains most of the output
% from cohgramc, including spectrograms for both tetrodes and cohereograms.
% 
% eeg variables are
% 	'cgramc'
switch (varname)
case {'cgramc'}
    eegvar = 1;
otherwise
    eegvar = 0;
end