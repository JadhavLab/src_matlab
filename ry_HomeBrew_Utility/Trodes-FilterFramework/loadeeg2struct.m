function out = loadeeg2struct(animaldir, animalprefix, datatype, days, epochs, tet1,tet2)

% out = loadeegstruct(animaldir, animalprefix, datatype, days, epochs, tetrodes1, tetrodes2)
%
% Works like load eeg struct, except this is designed for data structs
% saved based on pairs of tetrodes.
%
% Load the components of an eeg cell array of combines then into one
% variable. 
%	datatype is a string with the base name of the files (e.g. 'theta') 
% 	days specifies the list of days to load 
% 	epochs specifies the list of epochs to load 
% 	tetrodes specifies the list of tetrodes to load 
%
%	if epochs is empty, all epochs (1-10) are loaded
%	if tetrodes is empty, all tetrodes (1-50) are loaded
%
% Be aware that eeg files tend to be large, so loading a large number of them
% is likely to lead to problems

% if ~isequal(fileparts(animaldir),'chronux')
%     animaldir=fullfile(animaldir,'chronux');
% end

% Default sets to consider
if ~exist('days','var') || isempty(days)
    days = 1:10;
end
if ~exist('epochs','var') || isempty(epochs)
    epochs = 1:10;
end
if ~exist('tet1','var') || isempty(tet1)
    tet1 = 1:50;
end
if ~exist('tet2','var') || isempty(tet2)
    tet2 = 1:50;
end

for d = days
for e = epochs
for t1 = tet1
for t2 = tet2
    
    fname = sprintf('%s/chronux/%s%s%02d-%02d-%02d-%02d.mat', animaldir, ...
	    		animalprefix, datatype, d, e, t1,t2);
            
    try
        load(fname);
        eval(['out = datavaradd(out,',datatype,');']);
    catch
        warning('File not found in loadeegstruct.m');
    end
    
end
end
end
end

% OLD METHOD -- Intelligent, but slower
% %%% Setup regular expression parsing machinery to detect potential hits in directory
% % Filter strings
% animalfilt='(?<animal>[A-Za-z0-9]{1,3})?';
% datatypefilt='(?<datatype>[A-Za-z]{1,10})?';
% dayf='(?<day>[0-9]{2,10})?';
% epf='-?(?<ep>[0-9]{2,10})?';
% tet1f='-?(?<tet1>[0-9]{2,10})?';
% tet2f='-?(?<tet2>[0-9]{2,10})?';
% propertyfilt = ...
%     [animalfilt datatypefilt dayf epf tet1f tet2f];
% % Figure out possible hits witin the directory
% potential_files = dir(sprintf('%schronux/%s%s*', animaldir, ...
%                     animalprefix, datatype));
% 
% % create the list of data files 
% out = [];
% for pf = potential_files'
% 
%     % Parse each directory hit with the appropriate filter string
%     result = regexp(pf.name,propertyfilt,'names');
%     
%     % Check if each property is on the list
%     daygood = any(ismember(str2double(result(1).day),days));
%     epochgood = any(ismember(str2double(result(1).ep),epochs));
%     tet1good = any(ismember(str2double(result(1).tet1),tet1));
%     tet2good = any(ismember(str2double(result(1).tet2),tet2));
%     
%     % IF it is, load it, and add it to the output cell
%     if daygood && epochgood && tet1good && tet2good
% 
%         try
%             load(fullfile(pf.folder, pf.name));
%             eval(['out = datavaradd(out,',datatype,');']);
%         catch
%             warning('File not found in loadeegstruct.m');
%         end
%         
%     end
% end


%--------------------------------------
function out = datavaradd(origvar, addcell)

out = origvar;
for d = 1:length(addcell)
    for e = 1:length(addcell{d})
	for t = 1:length(addcell{d}{e})
        for t2 = 1:length(addcell{d}{e}{t})
            if ~isempty(addcell{d}{e}{t}{t2})
                out{d}{e}{t}{t2} = addcell{d}{e}{t}{t2};
            end
        end
	end
    end
end
