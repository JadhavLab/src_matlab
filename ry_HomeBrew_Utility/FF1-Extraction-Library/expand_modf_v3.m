function modf = expand_modf_v3(modf,column,varargin)
% Searches output of modf and expands a chosen dimension common across a
% set of output columns. Expansion is a process whereby that dimension is
% collapses into the row dimension of the modf output table.
%
% Use I had for creating this -- expanding time length outputs such that
% each time point has it's own line in the output.
%
% This makes it easy downstream if one wants to (say, in my case) pop the
% output into a pandas dataframe.
% 
% V2 - Processes faster than V1, but consumes more memory while computing

ip = inputParser;
ip.addParameter('convert2table',true,@islogical); % This are more memory efficient than struct-arrays filter framework uses for output
ip.addParameter('avoid_cell',  true,@islogical);
ip.addParameter('avoid_struct',true,@islogical);
ip.addParameter('do_single',true,@islogical);
ip.parse(varargin{:});

convert2table = ip.Results.convert2table;
avoid_cell   = ip.Results.avoid_cell;
avoid_struct = ip.Results.avoid_struct;
do_single = ip.Results.do_single;

uniform_false = {'uniformoutput',false};

p=gcp;
c=p.Cluster;

for i = 1:numel(modf)
    
    % Old struct and new structure.
    modf_output = modf(i).output{1};
    new_modf_output = [];
    if istable(modf_output); modf_output=table2struct(modf_output) ; end
    
    % Expand each line
    nEntries = length(modf_output); job = [];
    for j = 1:nEntries

%       	if nEntries < 100 || mod((j/nEntries)*100,1)<=(1/nEntries)*100
            fprintf('(%d %2.3f ) ', i, j/nEntries * 100); %end
        
        %% Submit job
        tempm = modf_output(j);
        job = [job, parfeval(p,@expand_item,1,tempm,column)];
    end
    
%     for j = job; wait(j); end
    
    %% Query finished or failed

    for j = 1:numel(job)
        [idx,data] = fetchNext(job);
        new_modf_output=[ new_modf_output, data ]
        delete( job(idx) );
        job(idx)=[];
    end
    
    if convert2table
        modf(i).output{1} = struct2table(new_modf_output);
    else
        modf(i).output{1} = new_modf_output;
    end
    
end

disp('Done expanding!')


    % ---------------------------------------------------------------
    function out = expand_item(item,c)
        
        % Our rotation function
        function item=expand(item,dim,ntime)
            if ~isempty(dim)
                xdim = 1:ndims(item);
                dim = [ xdim(dim):xdim(end), 1:xdim(dim)-1 ];
                item = permute(item,dim);
                if do_single && isequal(class(item),'double'); item=single(item); end
                item = mat2cell( item , ones(1,ntime) );
            else
                if (avoid_struct && isstruct(item)) || (avoid_cell && iscell(item)) || isempty(item)
                    item = cell(ntime,1);
                    
                    [item{:}] = deal(single(NaN));
                else
                    temp = item;
                    item = cell(ntime,1);
                    if do_single && isequal(class(temp),'double'); temp=double(temp);end
                    [item{:}] = deal(temp);
                end
            end
            
            
            
        end
        
        out = [];
        
        % If user is passing in more than one line, error out
        if numel(item)>1; error('One at a time, please!'); end
        
        % Figure out the dimensions of each term in the structural item
        item_size = structfun( @size , item , uniform_false{:});
        item_dimeq = structfun(  @(x) eq( max(item_size.(c)) , x ) , item_size , uniform_false{:});
        item_find_dimeq = structfun( @find , item_dimeq , uniform_false{:});
        
        % Loop and rotate if it has a dimension equal to nTime length
        % ... otherwise, copy it to the length of nTime
        nTime = numel(item.(c));
        for f = fields(item)'
            f=f{1}; %#ok<FXSET>
            
            if nTime == 0; continue; end
            
            % Carry out nTime length expansion
            item.(f) = expand(item.(f), item_find_dimeq.(f), nTime);
            
            % Final step is to separate everything
            [out(1:nTime).(f)] = deal( item.(f){:} );
        end
       
    end

end
