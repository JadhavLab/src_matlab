function fset=findallfields(cstruct,fset)
    if nargin==1,fset={};end

    if iscell(cstruct)
        for i = 1:numel(cstruct)
            fset = findallfields(cstruct{i},fset);
        end
    elseif isstruct(cstruct)
        fset = union(fset,fields(cstruct));
        if isfield(cstruct,'fields')
            [~,terms] = figure_fields(cstruct.fields);
            fset = union(fset,terms);
        end
    end



         % Split fields properly
        % ---------------------------------------------
        function [string,terms] = figure_fields(string)
            parenth=[];
            spaces = strfind(string,' ');
            p1 = strfind(string,'(');
            p2 = strfind(string,')');
            if ~isempty(p1) && ~isempty(p2),
                parenth(:,1) = p1;
                parenth(:,2) = p2;
                % Change parenthesis
                string(p1) = '_'; string(p2) = '_';
                % Remove spacing in parentheticals
                for prange = parenth'
                    space_to_change = spaces( spaces > prange(1) & spaces < prange(2) );
                    string(space_to_change) = '_';
                end
            end

            string = strrep(string,'-','_');
            terms = strsplit(string,' ');
        end
end