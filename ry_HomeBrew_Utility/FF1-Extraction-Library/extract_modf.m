function [ varargout ] = extract_modf( modf , varargin)
% function [ varargout ] = extract_modf( modf )
%
% quick way for outputing modf components into a workspace or a file
%
% 'default' mode = returns each column of modf output in varargout, and a
% list of output names in the first input
%
% 'assign' mode = auatomatically assigns the columns of output into the
% calling function or scope.
%
% TODO provide automated tagging of animal name and number to the output

% ------- Manage Save Counter (For File Mode) -----------
persistent save_counter
persistent savedir
if ischar(modf)    
    if strcmp(modf,'reset')
        save_counter = 0;
        if ~isempty(savedir)
            d=dir(fullfile(savedir,'modf*'));
            if ~isempty(d); delete(d.name); end;
        end
    elseif strcmp(modf,'savedir')
        savedir = varargin{1};
    end
    return 
end
if isempty(save_counter), save_counter = 0; end
if isempty(savedir), savedir=pwd; end
% --------------------------------------

p=inputParser;
p.addParameter('mode','default',@ischar);
p.addParameter('savedir',savedir,@ischar);
p.addParameter('appendanimal',false ,@islogical); % only works if animal is already a field in modf.output
p.addParameter('append',{});
p.addParameter('suppress_messaging',false);
p.addParameter('type',{});
p.parse(varargin{:});
mode=p.Results.mode;
savedir=p.Results.savedir;
append=p.Results.append;
suppress_messaging=p.Results.suppress_messaging;
appendanimal=p.Results.appendanimal;

%% Add animal information to outputs
if appendanimal
    animal=cat(1,modf.animal);animal=cat(1,animal{:,1});
    for i = 1:numel(modf)
        [modf(i).output{1}.animal] = deal(animal(i,:));
    end
end

%% Pre-process modf
fprintf('Preprocess...');
modf={modf.output};
modf=cat(1,modf{:}); 
if istable(modf{1})
    modf=cat(1,modf{:});
else
    modf=cat(2,modf{:})';
end

%% Generate animalid
fprintf('Append...');
if appendanimal && isfield(modf,'index')
    adata = {modf.animal};
    newindex = arrayfun(@(x) [find(ismember(animal,adata{x} ,'rows')) modf(x).index], 1:numel(adata),'uniformoutput',false);
    [modf.index] = deal(newindex{:});
end

%% Append any user provided information
if ~isempty(append)
    for k = 1:2:numel(append)
        if isstruct(modf)
            [modf.(append{k})] = deal(append{k+1});
        elseif istable(modf)
            modf.(append{k}) = repmat(append{k+1},size(modf,1),1);
        end
    end
end

%% Process outputs
fprintf('Process...');
switch mode
    case {'default'}
        % Default mode: returns each of modf fields in varargout
        
        varargout = cell(1,numel(fields(modf))+1);
        varargout{1} = fields(modf);
        count = 1;
        for f = fields(modf)'
            f=f{1};

            count=count+1;
            varargout{count} = {modf.(f)};

        end

    case {'struct'}
        % Struct mode: returns modf output table as a struct
        
        varargout{1} = modf;
        
    case {'assign','assign_nocell'}
        % Assign mode: Assigns each variable present in the modf output
        % table into the workspace of the caller
        
       fprintf('Assigning ');
       for f = fields(modf)', f=f{1};

            fprintf( '%s ' , f );
            temp = {modf.(f)};
            if isequal(mode,'assign_nocell')
                temp = cat(1,temp{:});
            end
            assignin('caller',f, temp);

       end 
       fprintf('modf_fields');
       assignin('caller','modf_fields',fields(modf));
       fprintf('\n');
    
    case {'store','store_nocell','store_nosarray'}
        fprintf('Store...');
        % Store mode: stores modf as a file in struct mode
        save_counter=save_counter+1;
        
        if isstruct(modf)
            field_set=fields(modf)';
        elseif istable(modf)
            field_set=modf.Properties.VariableNames;
        elseif isempty(modf)
            return;
        end
        
        for f = field_set
            f=f{1};
            fprintf('prepping %s...',f);
            temp.(f) = {modf.(f)};
            if isequal(mode,'store_nocell') && ( all(cellfun(@isscalar, temp.(f))) || numel(temp.(f))==1 )
                try
                    temp.(f) = cat(1,temp.(f){:});
                catch
                    fprintf('...not convertable to mat...');
                end
            elseif isequal(mode,'store_nosarray')
                try
                    temp.(f) = cat(1,temp.(f){:});
                catch
                    fprintf('...not convertable to mat...');
                end
            end
        end 

        filename = sprintf('modf_%03d.mat', save_number() );
        fprintf('Saving %s...',filename);
        f = fields(temp);
        
        % --- SAVE ---
        try % v6
            save(fullfile(savedir,filename),'-struct','temp','-v6');
        catch e % if exception, try fast HDF
            disp(e)
            try
                savefast(fullfile(savedir,filename),'-struct','temp');
                fprintf('done');
            catch e % if exception, try regular HDF
                disp(e)
                fprintf('nocompression failed...compressing...');
                save(fullfile(savedir,filename),...
                '-struct','temp',f{:},'-v7.3');
                fprintf('done');
            end
        end
    otherwise
        error('Mode incorrect');
end

% function typecast(modf,types)
%     % Subfunctoin that can downcast all of the types that default to double
%     % into something more efficient
%     
% end

function out = save_number()
    file = dir(fullfile(savedir,'*modf*'));
    file = strvcat(file.name);
    nums = [0 arrayfun( @(x) parse_number(file(x,:)), 1:size(file,1) )];
    out = min( setdiff( 0:(max(nums)+1), nums ) ) ;
    
    function out=parse_number(string)
        pattern = '[0-9][0-9][0-9]';
        [start,stop]=regexp(string,pattern);
        out=int16(str2double(string(start:stop)));
    end
    
end

end