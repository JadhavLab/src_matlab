function modf = expand_and_extract_modf_v2b(modf,column,varargin)
% Searches output of modf and expands a chosen dimension common across a
% set of output columns. Expansion is a process whereby that dimension is
% collapses into the row dimension of the modf output table.
%
% Use I had for creating this -- expanding time length outputs such that
% each time point has it's own line in the output.
%
% This makes it easy downstream if one wants to (say, in my case) pop the
% output into a pandas dataframe.
% 
% V2 - Processes faster than V1, but consumes more memory while computing
%
% V2b -
% This version focuses on making the expand_item function more efficient
% and capable of expanding across more than one column at once. This route
% is being taken because expand_modf_v3/2 seem incapable of being called
% twice on two dimensions (costs too much time): the second call lasts
% beyond what anyone would reasonably wait for on EEG data.


ip = inputParser;

    ip.addParameter('avoid_cell',  true,@islogical); % Avoid expanding cell types
    ip.addParameter('avoid_struct',true,@islogical); % Avoid expanding struct types
    ip.addParameter('do_single',true,@islogical); % Convert double types to single types to save space
    
    
    ip.addParameter('convert2table',true,@islogical); % These are more memory efficient than struct-arrays filter framework uses for output
    ip.addParameter('force_numeric',false,@islogical); % Force numeric type at the end of expand_item and at the end of this function to save space
    ip.addParameter('remove_field',{},@iscell); % List of fields to remove at the end of processing
    ip.addParameter('match_field','',@ischar); % Remove any field that doesn't match the length of a specified field
    
    ip.addParameter('start_at',1) % If previously failed due to RAM being exceeded, this can be used to start the process at the jth nEntry term
    ip.addParameter('extract_when_change',{},@iscell); % Which fields trigger extraction&clear behavior ...
    
ip.parse(varargin{:});

convert2table = ip.Results.convert2table;
avoid_cell   = ip.Results.avoid_cell;
avoid_struct = ip.Results.avoid_struct;
do_single = ip.Results.do_single;
force_numeric = ip.Results.force_numeric;
remove_field = ip.Results.remove_field;
match_field = ip.Results.match_field;
extract_when_change = ip.Results.extract_when_change;
start_at = ip.Results.start_at;

uniform_false = {'uniformoutput',false};

state = [];

%% Mode funcs
% ------------
function temp = fnumeric(new_modf_output)
    % forces array of structs into struct array
    for f = fields(new_modf_output)', f=f{1};
        temp.(f) = cat(1, new_modf_output.(f));
        new_modf_output=rmfield(new_modf_output,f);
    end
end
% ------------
function temp = fremove(temp)
    fds = fields(temp);
    tomatch_length = length( temp.(match_field) );
    lengths = bsxfun(@eq, structfun( @length, temp ) , tomatch_length);
    for fd = 1:numel(fds)', f=fds{fd};
        if ~lengths(fd), temp=rmfield(temp,f); end;
    end
end
% ------------
function bool_answer = determine_change(item)
    
    % Null hypothesis: no change. If we see a change, this flips
    bool_answer=false;
    
    % If any array of structs, only look at first
    if numel(item)>1; item=item(1); end;
    
    if isempty(state) % First time calling the method
        state=cell2struct(cell(size(extract_when_change))', extract_when_change);
        for f = fields(state)',f=f{1};
            state.(f) = item.(f)(1,:);
        end
    else % Previously called, determine if data at specific columns has changed
        for f = fields(state)',f=f{1};
            if ~isequal(state.(f),item.(f)(1,:))
                bool_answer=true;
                state.(f) = item.(f)(1,:);
            end
        end
    end
end
% ------------

%% Iterate each modf belonging to separate animal
for i = start_at:numel(modf)
    
    % Old struct and new structure.
    modf_output = modf(i).output{1};
    new_modf_output = [];
    if istable(modf(i).output{1}); modf_output=table2struct(modf(i).output{1}) ; end
    
    %% PROCESS EACH LINE
    nEntries = length(modf_output);
    fprintf('Initializing ...');
    for j = 1:nEntries
        
        % Process
        tmp = modf_output(j);
        nPoint = points_per_dim(tmp,column);
        for col = 1:numel(column)
            if col==1
                tmp = expand_item_v2(tmp,column{col},nPoint(col),true);
            elseif col==numel(column)
                tmp = expand_modf(,column{col},nPoint(col),false);
            else
            end
        end
        fprintf('(%d %d %2.1f  ', i, j, j/nEntries * 100);
        
        %% Check Change
        if determine_change(tmp)
            modf.output{1}=new_modf_output;
            extract_modf(modf,'mode','store');
            new_modf_output=[];
            modf.output{1}=[];
        end
        fprintf(' change_check ');
        
        %% Append the expanded item
        if isempty(tmp)
            continue;
        else
            if isempty(new_modf_output)  % If do not have a special case for isempty, it will fail
                new_modf_output = tmp';
                fprintf(' new ');
            else
                new_modf_output = [ new_modf_output; tmp'];
                fprintf(' append ');
            end
        end
        
        %% NUMERIC MODE
        % If numeric mode (struct array instead of array of structs) mode is
        % on, force the final conversion here (this mode is not appropriate for
        % inputs who at the end of expansion are not scalar.
        if force_numeric
            new_modf_output = fnumeric(new_modf_output);
            fprintf(' numeric_exp) ');
        else
            fprintf(' no_numeric_exp) ');
        end
        
    end
    
    %% FIELD MATCH REMOVAL
    if ~isempty(match_field)
        new_modf_output = fremove(new_modf_output);
    end
    
    %% TABLE CONVERSION
    if convert2table
        fprintf('...table conversion ...');
        modf(i).output{1} = struct2table(new_modf_output);
    else
        modf(i).output{1} = new_modf_output;
    end
    
end

disp('Done expanding!')

% -----------------------------------------------------
% --------------- ROW EXPANSION METHODS ---------------
% -----------------------------------------------------
    function out = points_per_dim(item,cols)
        out = zeros(numel(cols,1));
        for c = 1:numel(cols)
            out(c) = numel(item.( cols{c} ));
        end
    end
    function out = expand_item_v2(item,col,nPoint,force_numeric)
        
        if isstr(col), col={col}; end
        assert( iscell(col) );
        
        out = [];
        
        % If user is passing in more than one line, error out
        if numel(item)>1; error('One at a time, please!'); end
        
        % Figure out the dimensions of each term in the structural item
        item_size = structfun( @size , item , uniform_false{:});
        
        
        % Loop and rotate if it has a dimension equal to nTime length
        % ... otherwise, copy it to the length of nTime
        
        for c = col, c=c{1};
            
            % Collect information about the expansion to be performed
            
             % Figure out the dimensions of each term in the structural item
            item_size = structfun( @size , item , uniform_false{:});
            [item_find_dimeq,~] = ...
                get_dim_info(item_size,item,c);
        
        % Expansion of each field
        for f = fields(item)'
            f=f{1}; %#ok<FXSET>
            
            if nPoint == 0; continue; end
            
            % Carry out nTime length expansion
            if isequal(f,c)
                item.(f) = expand(item.(f), item_find_dimeq.(f), nPoint,true);
            else
                item.(f) = expand(item.(f), item_find_dimeq.(f), nPoint,false);
            end
            
        end
        
        end
        
        % Format the expanded outputs
        for f = fields(item)',f=f{1};
            if iscell( item.(f) )
                
                % Collect all the results
                for ii = 1:numel(col)-1
                    item.(f) = cat(1,item.(f){:});
                end
                
                % Format the final result
                if force_numeric
                    out.(f) = cat(1,item.(f){:});
                else
                    [out(1:numel(item.(f))).(f)] = deal( item.(f){:} );
                end
                    
            elseif isnumeric(item.(f))
                out.(f) = item.(f);
            end
        end
       
        % ---------------------------------------------------------
        % Dimension figuring function
        function [item_find_dimeq,nTime] = get_dim_info(item_size,item,c)
            % Find the largest dimension of the field to expand
            max_itemsize = nPoint;
            lambda_largestExpandDim = @(x) find(eq(x,max_itemsize)) ;
            item_find_dimeq = structfun( ...
                lambda_largestExpandDim , ...
                item_size , uniform_false{:});
            % How many points are being expanded?
            nTime = numel(item.(c));
        end
        
        % ---------------------------------------------------------
        % Our rotation function
        function item=expand(item,dim,ntime,isdimexpand)
            if ~isempty(dim)
                xdim = 1:ndims(item);
                dim = [ xdim(dim):xdim(end), 1:xdim(dim)-1 ];
                item = permute(item,dim);
                if do_single && isequal(class(item),'double'); item=single(item); end
                item = mat2cell( item , ones(1,ntime) );
            else
                if (avoid_struct && isstruct(item)) || (avoid_cell && iscell(item)) || isempty(item)
                    item = cell(ntime,1);
                    
                    [item{:}] = deal(single(NaN));
                else
                    temp = item;
                    item = cell(ntime,1);
                    if do_single && isequal(class(temp),'double'); temp=double(temp); end
                    [item{:}] = deal(temp);
                end
            end
            
            
            
        end
        
    end

end
