function fset = findallfields_type(adir,aname,types,varargin)

ip=inputParser;
ip.addParameter('mangle',false,@islogical);
ip.parse(varargin{:});

mangle=ip.Results.mangle;

if isrow(types);types=types';end 

fset={};

for t = types'
    iseeg = iseegvar(t{1});
    if iseeg
        one_fset = findallfields( loadeegstruct(adir,aname,t{1},1:8) );
    else
        one_fset = findallfields( loaddatastruct(adir,aname,t{1}) );
    end
    if mangle; one_fset = mangle_names( t{1}, one_fset ); end;
    fset=union(fset,one_fset);
end

    function fset=mangle_names(type,fset)
        for f = 1:numel(fset)
            fset{f} = [fset{f}, '_', type];
        end
    end

end