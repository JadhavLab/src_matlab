function varargout = cat_cells(varargin)

for v = 1:numel(varargin)
    varargout{v} = cat(1,varargin{v}{:});
end