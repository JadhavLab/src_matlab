function plotTrackExclusion(animaldir,animname,index,exclusion)
%

dyz = sprintf('%02d',index(1));

load(fullfile(animaldir,sprintf('%spos%02d',animname,index(1)) ) );

pos = pos{index(1)}{index(2)};
pos.data(~isExcluded(pos.data(:,1),exclusion));

plot( pos.data(:,2), pos.data(:,3) );

