% Recursive Mat File Conversion
function recursiveMatFileConversion(folder)
    
    curr_dir = pwd;
    c = onCleanup(@()cd(curr_dir));
    
    if exist(folder,'dir')
        search = '*.mat';
    else
        [folder,search]=fileparts(folder);
        if isempty(folder); folder=pwd; end;
    end
    
    try
    
    cd(folder);
    
    M = dir(search);
    for m = 1:numel(M)
        
        filename = M(m).name;
        fprintf('Converting %s\n',filename);
        dat = load(filename);
        field_set = fields(dat);
        field_str = '';
        for i = 1:numel(field_set)
            field = field_set{i};
            eval([field '=' 'dat.(field);']);
            field_str = [field_str ', ' '''' field ''''];
        end

        try
            eval(['save(filename ' field_str ',''-v6'');']);
        catch % if file is too big to be uncompressed, then go ahead and recompress it into v7.3
            warning('Could not save file %s as -v6',field_str);
            eval(['save(filename ' field_str ',''-v7.3'');']);
        end
        
        clear(field_set{:});
        clear dat;
        
    end
    
    clear d D;
    D = dir();
    for d = 3:numel(D)
        
        file = D(d);
        
        if file.isdir
            recursiveMatFileConversion(fullfile(pwd,file.name,search));
        end
    end
    
    catch ME
        warning('Caught non-save issue exception in recursive crawl');
        cd(curr_dir);
        rethrow(ME);
    end
    
    cd(curr_dir);
end