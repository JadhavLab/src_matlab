function ax =suptitle(title,varargin)

ip = inputParser; ip.KeepUnmatched=true;
ip.addParameter('ax',[]);
ip.parse(varargin{:});
ax=ip.Results.ax;

% - Build title axes and title.
if isempty(ax)
    ax = axes( 'Position', [0, 0.95, 1, 0.05] );
else
    axes(ax, 'Position', [0, 0.95, 1, 0.05] );
end
set(ax, 'Color', 'None', 'XColor', 'White', 'YColor', 'White' );
text( 0.5, 0, title, 'FontSize', 14', 'FontWeight', 'Bold', ...
      'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom' );
