function f= plotSegments(animaldir,animalprefix,index,varargin)

p=inputParser;
p.addParameter('animal','',@ischar);
p.addParameter('colorSegment',false,@islogical);
p.parse(varargin{:});
animal=p.Results.animal;
colorSegment=p.Results.colorSegment;

if ~isempty(animal)
    ainfo=animaldef(animal);
    animaldir=ainfo{2};animalprefix=ainfo{3};
end

if ~isempty(index)
    linpos=loaddatastruct(animaldir,animalprefix,'linpos',index(1));
else
    linpos=loaddatastruct(animaldir,animalprefix,'linpos');
end

linpos=linpos{index(1)}{index(2)};

% Plot track skeleton
% Background - track skeleton
f=gcf;cla;hold off;
segCoords = linpos.segmentInfo.segmentCoords;
legendstr=[];

if colorSegment
    [ax,legendstr] = plotColorSegments(segCoords,legendstr);
else
    [ax,legendstr] = plotBWSegments(segCoords,legendstr);
end
legend(ax,legendstr');

    function [ax,legendstr] = plotColorSegments(segs,legendstr)
        f=gcf;legendstr={};
        for i = 1:size(segs,1)
            ax(i)=plot(segs(i,[1 3]), segs(i,[2 4]));hold on;
%             plot(segs(i,[1]), segs(i,[2]),'go');
%             plot(segs(i,[3]), segs(i,[4]),'r*');
            legendstr{i}=num2str(i);
        end
    end

    function [ax,legendstr]= plotBWSegments(segs,legendstr)
        ax=[]
        for c = 1:size(segs,1)
            ax(1)=plot(segs(c,[1 3]), segs(c,[2 4]),'k-');
            hold on;
        end
        legendstr='track skeleton';
    end

end


