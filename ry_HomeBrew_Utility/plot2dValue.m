function plot2dValue(currx,curry,phi,speed)

if nargin ~= 4; speed=1; end

min_phi = min(phi);
max_phi = max(phi);
mm_x = [min(currx) max(currx)];
mm_y = [min(curry) max(curry)];


gcf;clf;
for k = 1:speed:numel(currx)
    if k < 10
        continue;
    end
    hold off;
    plot(currx(1:k),curry(1:k),'k-');hold on;
    scatter(currx(1:k),curry(1:k),[],phi(1:k),'filled');
    caxis([min_phi, max_phi]);
    axis([mm_x mm_y]);
    drawnow;
end

end