
# coding: utf-8

# ###### External Libraries

# In[4]:

# Ipy preprocessor to make figures inline
# get_ipython().magic('matplotlib inline')

# Data Types
import numpy as np
import pandas as pd
from collections import OrderedDict

# Plotting
import matplotlib.pyplot as plt

# Ipython libraries
from ipywidgets import widgets
from ipywidgets import interactive
from IPython.display import display

# Ability to read csvfiles
import csv


# ___

# ###### Define Key Structures
# Classes, Callbacks

# In[13]:

## CLASSES ##
class Settings:
    """
    Contains a field storing all the relevant GUI objects that
    could be thought of as settings! (No methods for now, but
    maybe in the future.)
    """
    
    pdir=""
    sessionsID=""
    imageName=""
    
    # Constructor / Data
    def __init__(self):
        pass
    
class Params:
    """
    Class containing everything about parameters in the params file,
    and the GUI objects belonging thereto.
    """
    pdict = OrderedDict()
    pgui  = OrderedDict()
    
    # Contrstructor / Data
    def __init__(self):
        pass
    # Input method
    def readCSV(self,csvfile):
        
        print("File: " + csvfile)
        print()
        
        # Open CSV
        csvfile=open(csvfile,'r')
        reader = csv.reader(csvfile,delimiter=',')
        
        #Loop through file and write resulting dictionrary fields
        for row in reader:
            
            # Establish the general dictionary object recording the values
            Params.pdict[row[0]] = {}
            
            for r in row[1:]:
                print(r)
                Params.pdict[row[0]][str(r)] = r

        csvfile.close()
        
    def display(self,settings):
        """
        Function that displays all the GUI elements and resultant image
        """
        self.pdict['Settings']=settings
        interactive(self.displayCallback,**self.pdict)
        return Panels
    
    @staticmethod
    def displayCallback(**kwargs):
        for key, val in kwargs:
            print(key, val)

#         if len(V) > 0:
#             Params.pdict[k] = V;
#             # Generate the string from the current pdict
#             imagestr = Params.pdict['Settings'].pdir.value + '/'
#             for key,val in self.pdict.items():
#                 imagestr = imagestr + key + "=" + val + "/"
#             # 
#             image = plt.imread(imgstr)
#             imshow(image)
#         else:
#             print("Skipping ... received none.")


# ____

# ###### Instantiate GUI Objects & Pre-process

# In[14]:

# --- GUI OBJECT INSTANTIATIONS ---
# Project directory input
S = Settings()
S.pdir = widgets.Text("/Users/ryoung/Data/Miller/FindNetTone")
S.sessionID = widgets.Text("1stRun")
S.imageName = widgets.Text("")


# ***

# ##### Input Project Directory
# 

# In[15]:

display(S.pdir)
display(S.sessionID)
display(S.imageName)


# ##### Process Directory and Ready Visual

# In[16]:

# Establish a reader indexing into the params file, using information provided above
params_file = S.pdir.value + "/" + S.sessionID.value + "_params.log"
    
# Instantiate Params GUI objects
P = Params()
P.readCSV(params_file)


# ____

# ### EXAMINE IMAGES

# In[21]:

P.display(S)
# interactives = interactive(Params.displayCallback(),**Params.pdict)


# In[ ]:



