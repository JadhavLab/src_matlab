
% Script to be used with parameter explorer to backup results before doing
% something else

currdir=pwd;
savedir = ParameterExplorer.savelocation(params);
savedir=fullfile(projectfolder,savedir);
cd(savedir);

newname = ['modf-' datestr(datetime('now'),'mmmdd') '.mat'];
evalstr = ['!cp -f modf.mat ' newname '&'];
eval(evalstr);

cd(currdir);