% Simple tool to separate modf results into different files

input=load('modf.mat');
field = fields(input);

for f = 1:numel(field)
	evalstr=[field{f} '= input.(field{f});', ...
			'save(' field{f} '.mat, ''' field{f} ''');'];
	eval(evalstr);
end