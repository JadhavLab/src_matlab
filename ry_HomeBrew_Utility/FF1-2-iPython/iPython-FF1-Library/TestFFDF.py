# Purpose of this file is to test the FFDF module

import pandas as pd
import numpy as np
import os,pdb

%cd code
%run -i FFDF.py
%store -r

# Open the HDF store!
%cd pos

m = MatlabHDF('modf_01.mat').convert()
df = pd.DataFrame(data=m.out_data)

## HOW TO GET THE DATA? ##
process=True

if process:   
    p=HDFStore_Maker(folders=folders, keys=keys, HDF_Store_path=HDF_Store_path,store_dfs=True)
    p=p.HDF_Store
else:
    p=pd.HDFStore(path=os.path.join(os.curdir,'hdfstore.h5'),mode='r+')

# Selection of Data
df = p.select(key='timefilter',start=0,stop=3)

## MUNGE THE DATA ##
domunge = False
if domunge:
    def to_double(x):
        return x.astype('double')

## EXPAND THE TIME DIMENSION ##
dooperate = False
if dooperate:
    pdb.set_trace()
    Operate.expand_dimension_time(df,'t')
