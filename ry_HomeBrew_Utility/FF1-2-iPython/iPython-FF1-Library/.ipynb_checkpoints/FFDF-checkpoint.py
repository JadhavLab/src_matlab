# Pacakges from the standard python universe
import os,fnmatch,multiprocessing,h5py
import pandas as pd
import numpy as np


# -------------------------
# Table of Contents
#
# MatlabHDF is used for each .mat file, DataframeImporter handles the
# conversion of one to several .mat files into a data frame, who all live in one
# folder (and presumably describe similar data). Pandas_HDFStore_Maker navigates
# to multiple folders with slightly different flavors of data and creates a
# hierarchical database of dataframes that can be mixed and matched.
#
# Classes are written largely in the order the software needs
# to know about them. MatlabHDF is used by DataframeImporter, and
# Pandas_HDFStore_Maker uses the DataframeImporter.
#
# (0) MatlabSCIO
# (1) MatlabHDF
# (2) DataframeImporter
# (3) Pandas_HDFStore_Maker
# (4) Operate Static Library
#
# -------------------------

class MatlabSCIO:
    '''
    Purpose
    ---------
    Handles SCIO readable files with the same interface as
    MatlabHDF. This one functions more like a wrapper than
    code doing anything new, per se. MatlabHDF solves an issue
    for me.

    Constructor Input
    ---------
    filename - name of the HDF file to convert

    Use
    --------

    HDFD = HDF_DeRef(filename).convert()
       ... the .convert() method instructs the object
       ... to begin conversion to numpy
    '''

    def __init__(self,filename):
        import scipy.io as io
        self.out_data={}
        self.in_data = io.loadmat(filename, matlab_compatible=True)
    def convert(self):
        for f in self.in_data.keys():
            if f[0:2] == '__': continue
            self.out_data[f] = self.in_data[f].squeeze().tolist()
        return self
    def to_pandas(self):
        import pandas as pd
        df=pd.DataFrame(data=self.out_data)
        def filter_me():
             pass
        return df

class MatlabHDF:
    '''
    Purpose
    ---------
    Handles the annoying conversion to numpy array
    process entailed when importing HDF5 data from Matlab

    Constructor Input
    ---------
    filename - name of the HDF file to convert

    Use
    --------

    HDFD = HDF_DeRef(filename).convert()
       ... the .convert() method instructs the object
       ... to begin conversion to numpy
    '''

    level_counter = None
    debug = False

    def __init__(self,filename,debug=False):

        self.hdf_data = h5py.File(filename,'r')
        self.out_data = []
        self.level_counter = 0
        self.debug = debug
    def convert(self):
        '''
        Purpose
        --------
        Conversion method. Once you've constructed your object, call this
        to handle your conversion into numpy.
        '''

        # recursively de-reference the HDF data
        self.out_data = self.handle_type( self.out_data , self.hdf_data )

        # Now transform such that the index dimension of the data per column
        # are located in the same dimension
        ndim = np.array([self.out_data[x].ndim for x in self.out_data.keys()])
        if (ndim>1).any():
            #pdb.set_trace()
            ix = np.where(ndim>1)[0]
            keys = list(self.out_data.keys())
            for i in ix:
                #pdb.set_trace()
                self.out_data[keys[i]]=self.out_data[keys[i]].T.tolist()
        return self

    def handle_type(self,out,x):

        self.level_counter+=1

        if self.debug is True:
            print('%s at level %d' % (type(x).__name__,self.level_counter))
        if type(x) is h5py.Group or type(x) is h5py.File:
            out = {}
            out = self.handle_group(out,x)
        elif type(x) is h5py.Dataset:
            out = []
            out = self.handle_dataset(out,x)
        elif type(x) is h5py.Reference:
            out = self.handle_reference(out,x)
        elif type(x) is np.ndarray:
            out = self.handle_numpy(out,x)
        else:
            pass

        self.level_counter-=1

        return out

    def handle_dataset(self,out,x):
        out = self.handle_type(out,np.array(x).squeeze().copy())
        return out

    def handle_group(self,out,x):
        for key in x.keys():
            if key == '#refs#': continue
            out[key] = None
            out[key] = self.handle_type( out[key],x.get(key) )
        return out

    def handle_reference(self,out,x):
        out = self.handle_type(out,self.hdf_data[x])
        return out

    def handle_numpy(self,out,x):
        out = np.array(x).squeeze()
        if x.size > 1:
            for i in range(len(x)):
                out[i] = self.handle_type(out[i],out[i])
        elif x.size == 1 and x.dtype.hasobject:
            out = self.handle_type(out,out.tolist())
        return out

    def to_pandas(self):
        import pandas as pd

        df=pd.DataFrame(data=self.out_data)

        def filter_me():
             pass

        return df

    def close(self):
        self.hdf_data.close()

    def __del__(self):
        self.close()

    def __exit__(self):
        self.close()

# --------------------------------------------
# Munging FF processed files into a data frame
# --------------------------------------------
class DataframeImporter:
    '''
    Purpose
    -------
    Utilize to import matlab structs into pandas structures

    Todo
    -------
    Import data to see what it looks like, before continuing.
    '''
    import sys,pdb,os

    def __init__(self,path="",dataframe=pd.DataFrame(data=[]),hdf=True):
        '''Constructor'''
        self.path=os.path.abspath(path) # where to process data
        self.dataframe=dataframe # keeps track of processed output
        self.hdf=hdf # whether to use hdfmatlab reader or non-hdfmatlab reader

    # --- PROCESS METHODS ---
    # Serial/Parallel
    def process(self,parallel=os.cpu_count()>1,func=None,save_as_process=False):
        '''Starts processing all the files in the folder, given the type specified
        in the constructor (HDF or non-HDF)'''

        curdir = os.path.abspath(os.curdir)
        os.chdir(self.path)

        if parallel is True:
            self.dataframe=self.process_parallel(func,save_as_process)
            print(self.dataframe.size)
        else:
            print('serial processing ...')

            if os.path.isdir(self.path):
                files = os.listdir()
                for f in files:
                    if not fnmatch.fnmatch(f,'modf_[0-9][0-9]*.mat'): continue
                    df=self.process_file(f)
                    self.dataframe = self.dataframe.append(df)
            elif os.path.isfile(self.path):
                df=self.process_file(self.path)
                self.dataframe = self.dataframe.append(df)

        os.chdir(curdir)
        print(self.dataframe.size)
        return self.dataframe

    # Parallel Submethod
    def process_parallel(self,func=None,save_as_process=False,wait_time=1):
        '''Handles parallel processing and will be automatically called by
        .process() if more than one CPU exists.'''
        import sys,gc
        gc.enable()

        print('parallel processing ...')

        pool = multiprocessing.Pool( os.cpu_count() + 2 )

        if os.path.isdir(self.path):
            files = os.listdir()
            jobs=[]
            for f in files:
                if not fnmatch.fnmatch(f,'modf_[0-9][0-9]*.mat'): continue
                job = pool.apply_async(DataframeImporter.process_file,(self,f))
                jobs.append(job)

            while len(jobs):
                print('Total jobs = %d' % len(jobs))
                for j,job in enumerate(jobs):
                    sys.stdout.write('Waiting job %d ...' % j)
                    job.wait(wait_time)
                    if job.ready():
                        sys.stdout.write('ready...')
                        if job.successful():
                            sys.stdout.write('successfull...\n')
                            df = job.get()
                            if func is not None:
                                df=func(df)
                            if save_as_process:
                                pass
                            self.dataframe=self.dataframe.append( df ,ignore_index=True)
                        else:
                            sys.stdout.write('NOT successfull...\n')
                        jobs.pop(j)
                        del(job)
                        gc.collect()
                        sys.stdout.write('...now only %d jobs' % len(jobs))
                    else:
                        sys.stdout.write('not ready\n')

        elif os.path.isfile(self.path):
            df=self.process_file(self.path)
            self.dataframe=self.dataframe.append(df)

        #pdb.set_trace()
        print(self.dataframe.size)
        return self.dataframe

    # Basic fileprocess operation
    def process_file(self,filename):
        '''Processes a single file'''
        if self.hdf is True:
            processor = MatlabHDF(filename)
        else:
            processor = MatlabSCIO(filename)
        return processor.convert().to_pandas()

    # SAVE METHODS
    def to_pickle(self,kwargs,filename='modf_pickle.pck'):
        self.dataframe.to_pickle(filename=filename,**kwargs)

    def to_hdf(self,kwargs):
        self.dataframe.to_hdf(**kwargs)

##########################################################################################
class HDFStore_Maker:
    '''
    Purpose
    ----------
    Take a list of folders containing different tpyes of filter framework output,
    keys describing those output types, and build an HDF store containing those
    data, formatted to pandas (or sql) and indexible
    '''

    def __init__( self,folders,keys,HDF_Store_path,kwargs={'append':False,'format':'f'},store_dfs=False):

        #Format = table or fixed

        if os.path.isdir(HDF_Store_path): HDF_Store_path+='/hdfstore.h5'

        self.folders = folders
        self.keys = keys
        self.HDF_Store = pd.HDFStore(path=HDF_Store_path)
        self.df_list = []
        self.store_dfs = store_dfs

        # Now, store!
        for f,folder in enumerate(folders):
            df=DataframeImporter(folder).process()
            if self.store_dfs:
                self.df_list.append(df)
            self.HDF_Store.put(value=df,key=self.keys[f],**kwargs)

    def save(filename,):
        pass

    def __exit__(self):
        self.HDF_Store.close()

    def __del__(self):
        self.HDF_Store.close()


##########################################################################################
# The operate library may have been obviated by my expand_modf function in matlab. Now I'm
# ready to test whether expanded modf tables can be quickly indexed enough in pandas.
##########################################################################################

class Operate:
    '''
    Static library of functions to change columns whose elements
    hold ND matrices into a larger data frame with those ND elements
    raveled along the row.
    '''
    
    

    def concatentate(items,tossfield=None):
        '''Automatically reconcatonates data that has been separated and
        expanded'''
        
        def apply_permute(item,permute_instructions):
            temp=list(np.transpose(item[x],permute_instructions) for x in range(len(item)))
            return temp
        def figure_out_how_permute(items):
            '''Figures out how to permute each field
            TODO: Still a bug where it fails to decide permutation correctly on 2,6,7,8'''

            shapes = [np.vstack([y.shape for y in x]) for x in items]
            locs = np.array([])
            
            # Get list of offsets
            N=[]
            for j,shape in enumerate(shapes):
                n_guess = 0
                for n,i in enumerate(shape[0]):
                    (ix,iy)=np.where( shape == i )
                    if ix.size != shape.shape[0]:
                        n_guess=n
                        break
                N.append(n_guess)
            
            # Now apply offset to derive permute instruction
            permute_instructions=tuple(np.arange(shape[0].size) for shape in shapes)
            rowidx = range(len(permute_instructions))
            permute_instructions = tuple(np.roll(permute_instructions[x],N[x]) for x in rowidx)
            if permute_instructions is None: raise('Problem')
            return permute_instructions
        
        
        # This first subsection munges the data into an appropriate tuple(numpy(numpy)) format, and separates out the keys
        items=[ tuple(zip(*it.items())) for it in items ] # Change from dictionary to tuple format
        keys,items=list(zip(*items)) # Separate keys from items
        items = tuple(zip(*items)) # Now shift item tuples such that sub-items of the same type wind up in the same tuples
        items = tuple( np.array(it) for it in items ) # Now convert the list of tuples that contain matching sets of numpy arrays into numpy arrays themselves.So now it's tuple(np.array( np.array() np.array() ... ), np.array(...) )

            # Toss out sets who are empty according to the tossfield
        if tossfield:
            key_ind = keys[0].index(tossfield)
            throw_out_shape = np.array([it.any() for it in items[key_ind]])
            items = list(it[throw_out_shape] for it in items)
            
        # Figure out how to permute each element within each item
        # and then permute them pre-concatonation
        permute_instructions = figure_out_how_permute(items)
        for i,item in enumerate(items):
            items[i] = apply_permute(item,permute_instructions[i])
        
        # Recreate dictionary and concatonate simultaneously
        nItem = range(len(items))
        concat={}
        nonconcat = {}
        for n in nItem:
            try:
                concat[keys[0][n]] = np.vstack(items[n])
            except Exception:
                nonconcat[keys[0][n]] = items[n]
                
        return concat,nonconcat


    def separate_and_expand(obj,change,match):
        '''This requires a special operation that opens '''
        D = Operate.separate_dict(obj)
        dict_list =[]
        for d in D:
            print('-------------')
            dict_list.append(  Operate.expand_a2b(d,change,match)  )
        return dict_list

    def separate_dict(obj):
        '''Takes a dict whose items are equal sized lists of elements to
        to a list of dicts with singular fields, same keys'''

        for i in range(len(   obj[[ x for x in obj.keys() ][0]]  )):
            out={}
            print('At %d' % i)
            for key in obj.keys():
                print('At %s' % key)
                out[key] = obj[key][i]
            yield out

    def expand_a2b(obj,change,match):
        '''Takes a dict whose items are equal sized lists of elements to
        to a list of dicts with singular fields, same keys'''

        # Prepare
        out = obj # make a copy for the output
        if type(change) is not list:
            change=[change]

        # Process
        l = range(len(obj[match].squeeze()))
        if obj[match].size > 0: # What to do if match field has elements
            for c in change:
                out[c] = np.vstack([ np.repeat(obj[c],len(obj[match]),axis=0) for i in l ])
        else: # What to do if match field has no elements
            for c in change:
                out[c] = np.array([])

        # Output
        return out

    def index():
        '''Installs a hierarchical index around the specified columns'''


    def make_char(df,columns):
        '''Transform acii number columns into character columns'''
        ##### the lambda #####
        def convert_char(x):
            if x is None:
                return ""
            else:
                return "".join( [chr(y) for y in x.ravel() ] )
        ####################
        if type(columns) is not list:
            columns=[columns];
        for c in columns:
            df.loc[:,c] = df[c].map(convert_char)
        return df

    def expand_dimension(df,columns):
        '''
        Purpose
        --------
        Take some ND data in dataframes and reshape dimensionality along the row of the table. For example,
        taking a bunch of 2d spectra who are each stored in a column, and changing the data such that
        the row of the pandas dataframe is one row per time point, for all spectra, who are already uniquely
        identified by other columns.

        This is a super critical procedure for making FF data extremely explorable through Seaborn, Holoviews,
        et cetera.
        '''
        def expand_row(row,columns):
            # Set a blank slate to place our result
            expanded = pd.DataFrame(columns = row.index.values.astype('str'))
            #pdb.set_trace()
            row = pd.DataFrame(data=row[np.newaxis,:],columns=row.index.values)

            # Extract objects to be expanded
            matrix = []
            for c in columns:
                val=row[c].values
                matrix.append(val)

                # (A) Fill the redundant portion

                # (1) Obtain critical numbers guiding expansion
                row_expansion_amount = val[0].shape[0]
                columns_merely_copied = np.setdiff1d( expanded.columns.values , columns )

                # (2) Copy the row
                expanded = pd.concat( [row.loc[:,columns_merely_copied]] * row_expansion_amount )

                # (B) Fill the non-redundant expanded portion
                for i,c in enumerate(columns):
                    expanded[c] = matrix[i][0].tolist()

                return expanded

        import pandas as pd
        expanded = pd.DataFrame(columns = df.columns.values.astype('str'))
        for i,row in df.iterrows():
            pdb.set_trace()
            temp = expand_row(row,columns)
            expanded = expanded.append( temp )

        return expanded

    def permute_dimensions(df,columns,axis):

        def transpose(x):
            dim = x.ndim
            if not dim == 0:
                x = np.transpose(x,np.roll(np.arange(dim),-(axis)))
            return x

        df.loc[:,columns]=df.loc[:,columns].applymap(transpose)
        return df

    def expand_dimension_time(df,time_column):

        def get_permlist(df,time_column):
            row  = df.dropna(axis=0,how='any')[0]
            t = row[time_column]

            perm = {}
            for c in row.index:
                if c == time_column: continue
                x,y = np.where( row[c] == t.size )
                if not x.any(): continue
                perm[c] = x

            return x

        pdb.set_trace()
        permlist = get_permlist(df,timecolumn)
        for key in permlist.keys():
            df = permute_dimensions(df,key,permlist[key])

        df = expand_dimension(df,list(permlist.keys()))

        return df

if __name__ == '__main__':

    #%cd /home/ryoung/Data/Local/Current/Coherence/TimeLocked/AvgCoh/AllAnim/20bins/

    import os
    filename=os.path.abspath(os.curdir)

    filename+='/modf_01_new.mat'
