% Name
% -----
% DFS_OutputData
%
% Pupose
% -----
% One of my data analysis experiments is to pipe filter framework output
% into python for plotting, with the more powerful pandas and seaborn
% related packages.
%
% This script exists to funnel several different types of data into .mat
% files (in folders classified by data type, e.g., spikes, eeg, coherence)
% for subsequent conversion into a pandas DataFrame store object. We're
% talking conversion of almost an entire experiment worth into that format,
% because of how much easier it is to quickly permute different dimensions
% of analysis with seaborn.
% -------------------------------------------------------------

warning off;
projectfolder = [datadef '/Local/Current/OutputData/'];
mkdir(projectfolder);
fprintf('Save location --> %s\n', projectfolder);
warning on;

extract_modf('savedir',projectfolder);
extract_modf('reset');
% -------------------------------------------------------------

%% Filter

animalfilter = {'HPa'};
animalfilter={'HPa','HPb','HPc'};
dayfilter = '1:5';
epochfilter = 'isequal( $type , ''run'')';

f_base = createfilter('animal',animalfilter,'days',dayfilter,'epochs',epochfilter);

ainfo=animaldef(animalfilter{1});

% POS INFORMATION
modf = modifyfilter(f_base,'iterator','epochbehaveanal');
this_project_folder=[projectfolder 'pos'];
modf = setfilterfunction(modf,'ryDFA_getPos',{'pos'},...
    'fields',findallfields_type( ainfo{2:3},{'pos'})...
    );
modf = runfilter(modf);

    mkdir(this_project_folder);
    pushd(this_project_folder);
    modf = expand_modf( modf , 'time' );
    whos modf
    extract_modf( modf , 'mode', 'store', 'appendanimal',false );
    popd;

% SPIKES INFORMATION
this_project_folder=[projectfolder '/spikes'];
modf = setfilterfunction( modf,'ryDFA_getSpikes', {'spikes'} );
    
    mkdir(this_project_folder);
    pushd(this_project_folder);
    whos modf
    modf = expand_modf( modf , 't' );
    extract_modf( modf , 'append' , {'type',t,'time_slice',time_slice} , 'mode', 'store' );
    popd;

% EEG INFORMATION
this_project_folder=[projectfolder '/eeg'];
types = {'eeg','theta','beta','delta','avgcoh'};
modf = setfilterfunction(modf,'ryDFA_getEEG',types,...
    'types',types,'fields',findallfields_type(ainfo{2:3},types)...
    );

    mkdir(this_project_folder);
    pushd(this_project_folder);
    modf = expand_modf( modf , 't' );
    whos modf
    extract_modf( modf , 'append' , {'type',t,'time_slice',time_slice} , 'mode', 'store' );
    popd;

modf = runfilter(modf);
