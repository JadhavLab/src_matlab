% Name
% -----
% DFS_OutputData
%
% Pupose
% -----
% One of my data analysis experiments is to pipe filter framework output
% into python for plotting, with the more powerful pandas and seaborn
% related packages.
%
% This script exists to funnel several different types of data into .mat
% files (in folders classified by data type, e.g., spikes, eeg, coherence)
% for subsequent conversion into a pandas DataFrame store object. We're
% talking conversion of almost an entire experiment worth into that format,
% because of how much easier it is to quickly permute different dimensions
% of analysis with seaborn.
% -------------------------------------------------------------

warning off;
projectfolder = [datadef '/Local/Current/OutputData/'];
mkdir(projectfolder);
fprintf('Save location --> %s\n', projectfolder);
warning on;
% -------------------------------------------------------------

if usejava('desktop')
    animalfilter = {'HPa'}
%     animalfilter = {'HPa','HPb','HPc'};
    doall=3;
end

%% Filter

dayfilter = '1:5';
epochfilter = 'isequal( $type , ''run'')';
eegfilter = '  isequal($area,''CA1'') || isequal($area,''PFC'') || isequal($area,''iCA1'')  ';
cellfilter = ' isequal($area, ''PFC'') || isequal($area, ''iCA1'') || isequal($area, ''CA1'')';

f_base = createfilter('animal',animalfilter,'days',dayfilter,'epochs',epochfilter,'eegtetrodes',eegfilter);

ainfo=animaldef(animalfilter{1});

%% POS INFORMATION
if ~doall ; do = input('Do pos? 1-yes ... '); 
else; do = 0 ; end
if do || doall>3 || doall==1
    modf = modifyfilter(f_base,'iterator','epochbehaveanal');
    this_project_folder=[projectfolder 'pos'];
    modf = setfilterfunction(modf,'ryDFA_getPos',{'pos'},...
        'fields',findallfields_type( ainfo{2:3},{'pos'})...
        );
    modf = runfilter(modf);

    whos modf
    modf = expand_modf_v3( modf , 'time' );
    whos modf
    mkdir(this_project_folder);
    extract_modf('savedir',this_project_folder);
    if usejava('desktop'); disp('R!');extract_modf('reset'); end
    extract_modf( modf , 'mode', 'store_nocell', 'append', {'animal',animalfilter{1}});
    popd;
end
%% EEG INFORMATION
if ~doall ; do = input('Do eeg? 1-yes ... '); 
else; do = 0 ; end
if do || doall>3 || doall==2
    
    modf = modifyfilter(f_base,'iterator','epocheeganal');
    this_project_folder=[projectfolder 'eeg'];
    types = {'avgcoh'};
    % types = {'avgcoh','theta','beta','delta',};
    modf = setfilterfunction(modf,'ryDFA_getEEG',types,...
        'types',types,'fields',findallfields_type(ainfo{2:3},types)...
        );
    modf = runfilter(modf);

    mkdir(this_project_folder);
    extract_modf('savedir',this_project_folder);
    if usejava('desktop'); disp('R!'); extract_modf('reset'); end;
    pushd(this_project_folder);
    whos modf
    disp('Beginning extraction');
    expand_and_extract_modf_v2b(modf,{'t','faxis'},'force_numeric',true,'match_field','t','extract_when_change',{'day','epoch', 'tet'});
    popd;

end
%% SPIKES INFORMATION
if ~doall ; do = input('Do spikes? 1-yes ... ');
else; do = 0 ; end
if do || doall>=3
    disp('Got here');
    this_project_folder=[projectfolder 'spikes'];
    modf = modifyfilter(f_base, 'iterator' , 'singlecellanal', 'cells', cellfilter);
    disp('Got here');
    modf = setfilterfunction( modf,'ryDFA_getSpikes', {'spikes'} , 'fields',findallfields_type(ainfo{2:3},{'spikes'}) );
    disp('Got here');
    modf = runfilter(modf);
    disp('Got here');
    mkdir(this_project_folder);extract_modf('savedir',this_project_folder);
    if usejava('desktop'); disp('R!'); extract_modf('reset'); end
    pushd(this_project_folder);
    whos modf
    modf = expand_modf_v2( modf , 'time' );
    extract_modf( modf , 'mode', 'store', 'append', {'animal',animalfilter{1}} );
    popd;
end

