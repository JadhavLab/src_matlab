function out = ryDFA_getSpikes(index,~,varargin)
% Method for merely outputting spikes informtion to modf

ip = inputParser; ip.KeepUnmatched=true;
ip.addParameter('types',{});
ip.addParameter('fields',findallfields(varargin{1}));
ip.addParameter('mangle',false);
ip.addParameter('dosingle',true);
ip.parse(varargin{find( cellfun( @ischar , varargin ) , 1, 'first'):end});

preset_fields = ip.Results.fields;
mangle = ip.Results.mangle;
types = ip.Results.types;

% Acquire all the data structures into a single out struct
out = {};
for v = 1:numel(varargin)
    if ischar(varargin{v}), break; end
    out{v} = copyObj( dereference(varargin{v}, index) );   
end

% Mangle names by type if requested
if mangle
    out = mangle_names(out,types);
end

% Join structs together, add index descriptors
out = joinStructs(out);

% Make sure any/all preset outputs are written
if ~isempty(preset_fields)
    if ~isrow(preset_fields);preset_fields=preset_fields';end
    for fd = preset_fields
        if isvalid(fd{1}) && ~isfield(out,fd{1})
            [out.(fd{1})] = deal(single(nan));
        end
    end
end

% Describet the data
out = describe(out,index);

% -----------------------------------------------------------------------
    function out=isvalid(obj)
        switch obj
            case {'fields','data'}
                out=false;
            otherwise
                out=true;
        end
    end
% -----------------------------------------------------------------------
    function obj=describe(obj,index)
        obj.day     = uint8(index(1));
        obj.epoch   = uint8(index(2));
        obj.tet     = uint8(index(3));
        obj.cell    = uint8(index(4));
    end
% -----------------------------------------------------------------------
    function out=mangle_names(out,types)
        for i = 1:numel(out)
            sfields = fields(out{i});
            for f = 1:numel(sfields)
                out{i}.([sfields{f} '_' types{i}]) = out{i}.(sfields{f});
                rmfield(out{i},sfields{f});
            end
        end
    end
% -----------------------------------------------------------------------
    function out=dereference(obj,index)
        while iscell(obj)
            obj = obj{index(1)};
            index=index(2:end);
        end
        out=obj;
    end
% -----------------------------------------------------------------------
    function out = joinStructs(obj)
        for i = numel(obj):-1:1
            for fd = fields(obj{i})'
                out.(fd{1}) = obj{i}.(fd{1});
            end
        end
    end
% -----------------------------------------------------------------------
    function out = interpAll(obj)
        % This function itereates over all objects that might require
        % different time interpolation
        t_base = obj{1}.t;
        for o = 2:numel(obj)
            obj{o} = interpObj(t_base,out{o});
        end
    end

    function out = interpObj(global_t,obj)
        % This function interpolates all time lenght objects
        
        global_nTime = numel(global_t);
        try t = obj.t; catch; t=obj.time; end
        
        for f = fields(obj)'
            
            if isequal(f{1},'t') || isequal(f{1},'time'), continue; end
            
            component = obj.(f{1});
            if sum(size(component) == global_nTime)
                component = interp1(t,component,global_t);
                obj.(f{1}) = component;
            end
            
        end
    end

% -----------------------------------------------------------------------
    function out = copyObj(obj)
        
        obj = split_data_and_field(obj);

        for fied = fields(obj)'
            out.(fied{1}) = obj.(fied{1});
        end
            
    end

    % ------------------------------------------------------------
    function obj = split_data_and_field(obj)

        % Split fields properly
        % ---------------------------------------------
        function [string,terms] = figure_fields(string)
            parenth=[];
            spaces = strfind(string,' ');
            p1 = strfind(string,'(');
            p2 = strfind(string,')');
            if ~isempty(p1) && ~isempty(p2),
                parenth(:,1) = p1;
                parenth(:,2) = p2;
                % Change parenthesis
                string(p1) = '_'; string(p2) = '_';
                % Remove spacing in parentheticals
                for prange = parenth'
                    space_to_change = spaces( spaces > prange(1) & spaces < prange(2) );
                    string(space_to_change) = '_';
                end
            end

            string = strrep(string,'-','_');
            terms = strsplit(string,' ');
        end
        % ---------------------------------------------

        % Figure out what the fields are!
        [obj.fields,terms] = figure_fields(obj.fields);

        % Separate out fields
        if isfield(obj,'data') && ~isempty(obj.data)
            for t = terms
                obj.(t{1}) = obj.data(:,colof(t{1},obj));
            end
        end

        % Remove data and fields
        obj=rmfield(obj,{'fields','data'});
    end

end
