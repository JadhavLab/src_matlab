function out = ryDFA_getEEG(index,~,varargin)
% Method for merely outputting eeg informtion to modf

ip = inputParser;
ip.KeepUnmatched=true;
ip.addParameter('types',{});
ip.addParameter('fields',findallfields(varargin{1}));
ip.addParameter('mangle',false);
ip.addParameter('dosingle',true);
ip.parse(varargin{find( cellfun( @ischar , varargin ) , 1, 'first'):end});

preset_fields = ip.Results.fields;
mangle = ip.Results.mangle;
types = ip.Results.types;

% Acquire all the data structures into a single out struct
out = {};
for v = 1:numel(varargin)
    if ischar(varargin{v}), break; end
    out{v} = copyObj( dereference(varargin{v}, index) );   
end

% Mangle names by type if requested
if mangle
    out = mangle_names(out,types);
end

% Interpolate to the same time base if requested
if numel(out) > 1
    out = interpAll(obj);
end

% Join structs together, add index descriptors
out = joinStructs(out);

% Make sure any/all preset outputs are written
out = ensure_presets(out,preset_fields);

% Describe data in terms of its indices
out = describe(out,index);

% -----------------------------------------------------------------------
    function out=ensure_presets(out,preset_fields)
        if ~isempty(preset_fields)
            if ~isrow(preset_fields);preset_fields=preset_fields';end
            
            for fd = preset_fields
                if isvalid(fd{1}) && ~isfield(out,fd{1})
                    [out.(fd{1})] = deal(single(nan));
                end
            end
        end
    end
% -----------------------------------------------------------------------
    function out=isvalid(obj)
        switch obj
            case {'fields','data'}
                out=false;
            otherwise
                out=true;
        end
    end
% -----------------------------------------------------------------------
    function obj=describe(obj,index)
            obj.day     = index(1);
            obj.epoch   = index(2);
            obj.tet     = index(3);
            if numel(index)>3; obj.tet2; end
    end
% -----------------------------------------------------------------------
    function out=mangle_names(out,types)
        for i = 1:numel(out)
            sfields = fields(out{i});
            for f = 1:numel(sfields)
                out{i}.([sfields{f} '_' types{i}]) = out{i}.(sfields{f});
                rmfield(out{i},sfields{f});
            end
        end
    end
% -----------------------------------------------------------------------
    function out=dereference(obj,index)
        while iscell(obj)
            obj = obj{index(1)};
            index=index(2:end);
        end
        out=obj;
    end
% -----------------------------------------------------------------------
    function out = joinStructs(obj)
        empty_counter = 0;
        for i = numel(obj):-1:1
            if isempty(obj{i}), empty_counter = empty_counter + 1; continue; end
            for fd = fields(obj{i})'
                out.(fd{1}) = obj{i}.(fd{1});
            end
        end
        if empty_counter == numel(obj)
            out=[];
        end
    end
% -----------------------------------------------------------------------
    function out = interpAll(obj)
        % This function itereates over all objects that might require
        % different time interpolation
        t_base = obj{1}.t;
        for o = 2:numel(obj)
            obj{o} = interpObj(t_base,out{o});
        end
    end

    function out = interpObj(global_t,obj)
        % This function interpolates all time lenght objects
        
        if isempty(obj), obj=[]; return; end
        
        global_nTime = numel(global_t);
        try t = obj.t; catch; t=obj.time; end
        
        for f = fields(obj)'
            
            if isequal(f{1},'t') || isequal(f{1},'time'), continue; end
            
            component = obj.(f{1});
            if sum(size(component) == global_nTime)
                component = interp1(t,component,global_t);
                obj.(f{1}) = component;
            end
            
        end
    end

% -----------------------------------------------------------------------
    function out = copyObj(obj)
        
        if isempty(obj), out=[]; warning('Empty'); return; end
        
        obj = split_data_and_field(obj);

        for fied = fields(obj)'
            out.(fied{1}) = obj.(fied{1});
        end
            
    end

    % ------------------------------------------------------------
    function obj = split_data_and_field(obj)
        
        if isempty(obj), obj=[]; return; end

        % Split fields properly
        % ---------------------------------------------
        function [string,terms] = figure_fields(string)
            parenth=[];
            spaces = strfind(string,' ');
            p1 = strfind(string,'(');
            p2 = strfind(string,')');
            if ~isempty(p1) && ~isempty(p2),
                parenth(:,1) = p1;
                parenth(:,2) = p2;
                for prange = parenth'
                    space_to_change = spaces( spaces > prange(1) & spaces < prange(2) );
                    string(space_to_change) = '_';
                end
            end

            string = strrep(string,'-','_');
            terms = strsplit(string,' ');
        end
        % ---------------------------------------------

        % Figure out what the fields are!
        if isfield(obj,'fields')
            [obj.fields,terms] = figure_fields(obj.fields);

            % Separate out fields
            for t = terms
                obj.(t{1}) = obj.data(:,colof(t{1},obj));
            end

            % Remove data and fields
            obj=rmfield(obj,{'fields','data'});
        end
    end

end
