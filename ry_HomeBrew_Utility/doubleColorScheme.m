function colors = doubleColorScheme(colorf1,colorf2,numOfEach)
% Input, two function handles to two color schemes, followed by a vector
% indicating how many colors with each scheme.

set1 = colorf1(numOfEach(1));
% set1 = set1(size(set1,1):-1:1,:);
set2 = colorf2(numOfEach(2));
set2 = set2(size(set2,1):-1:1,:);
colors = [set1;set2];

end