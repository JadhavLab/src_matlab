function [objective_val,fitx,fity,fitparm] = fitnscore_cos(x,y,varargin)
% Fits and scores cosine

fprintf('fitting...');

x=x(:);y=y(:);

% Cosine parameters
amp = range(y);
base = nanmin(y);
freq = nanstd(x);
phase = nanmax(x)/2 + nanmin(x);

y(isnan(y))=0;

% User is allowed to use varargin to overwrite any default variables set
varargin = optlistassign(who,varargin{:});


% Write objective function
    function [out,curve] = objective(p)
        curve = p(2) + p(1)*cos(x * p(3) + p(4));
        curve(x< (pi*p(3)+p(4)) & x > -(pi*p(3)+p(4)) ) = p(2);
        out = nansum(  (y-curve).^2 );
    end

% Call minimizer
% options = optimoptions(@fminunc,'Algorithm','trust-region',...
%     'SpecifyObjectiveGradient',true);
options = optimset('maxiter',1e6,'maxfunevals',800,'display','iter','tolx',1e-5);
p0 = [amp, base, freq, phase];
[fitparm,objective_val]=fminsearch(@objective, p0,options);

% Generate fitted output
fitx = x;
[~,fity] = objective(fitparm);

end