function fitnscore(x,y,func,p0,varargin)
% Fits and scores any function

func = @(z) func(x,y,z);
fminunc(func, p0,varargin{:});


end