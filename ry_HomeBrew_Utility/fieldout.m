function out = fieldout(sarray, field,varargin)
% out = fieldout(sarray, field)
%
% This function will have the ability to take a structural array at output
% a field of that array into a matrix the same shape as the the structural
% array it's apart of.

p=inputParser;
p.CaseSensitive=false;
p.addParameter('uniformOutput',false,@islogical);
p.parse(varargin{:});
uniformOutput=p.Results.uniformOutput;

% First, obtain the shape of the sarray
shape_sarray = size(sarray);

% Next, spit out the elements of the field into a variable
collect = {sarray.(field)};

collect = reshape( collect, shape_sarray );

if uniformOutput
    
    collect = cat(1,collect{:});
    if isrow(shape_sarray), shape_sarray=shape_sarray'; end;
    if  iscolumn(shape_sarray)
        collect = reshape(collect, shape_sarray(1), [] );
    else
        collect = reshape(collect, shape_sarray );
    end
    
end

out = collect;

end