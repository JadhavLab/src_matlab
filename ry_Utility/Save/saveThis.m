function saveThis(handle,savedir,filename,type,nestFolder)
%function saveThis(handle,savedir,filename,type,nestFolder)
%
% Function for saving full-sized figures, with certain filetypes. I've
% expanded it also to work with matlab2tikz. To use tikz format, type tikz,
% and you will get a png+source file. If you compile that source file in
% latex, with pfgplots and tikz packages included, you get high quality
% figures.
%
% handle - handle to your figure
% savedir - directory to save to
% filename - name of file to save
% string or cell of file types to save as
% nestFolder - (Optional) nests the save into an additional folder.
%
% Example
% saveThis(gcf, '~/Data/Local/ThetaCoherence/', 'glm-results', {'png',
% 'tikz', 'fig'});
%
% This will save a figre as three file types, full screen.

if iscell(type)
    typecell = type;
else
    typecell = {type};
end

if iscolumn(typecell), typecell=typecell'; end;

for type = typecell, 
    type = type{1}; 

    if nargin < 5
        nestFolder='';
    end

    filename=[filename '.' type];
    if isequal(type,'png'); type=''; end;

    savestr = fullfile(savedir,nestFolder,type);
    warning off; mkdir(savestr); warning on;
    savestr=fullfile(savestr,filename);

    switch type
        case 'tikz'
            matlab2tikz('filename',savestr,'figurehandle',handle);
        otherwise
            saveFigureFull(handle,savestr);
    end

end