function animalToPath(animal)
% Adds an animals folder information into the path for this matlab session.

info = animaldef(animal);
directory = info{2};

% Add all folders in the project folder into the path
path(genpath(directory),path);

% Remove anything related to a .git repository in this folder
rmpath( genpath( fullfile(directory,'.git') ) );
