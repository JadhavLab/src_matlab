

Dout6 = readTrodesExtractedDataFile(strcat(fileNameMask,'.dio_Dout6.dat'));
IndsDout6 = find(Dout6.fields(2).data);
Dout6times = Dout6.fields(1).data(IndsDout6);
Dout6hits = Dout6.fields(2).data(IndsDout6);
leftsolzeros = zeros([length(Dout6hits),1]);
LeftSolenoid = [Dout6times, Dout6hits, leftsolzeros];

Dout7 = readTrodesExtractedDataFile(strcat(fileNameMask,'.dio_Dout7.dat'));
IndsDout7 = find(Dout7.fields(2).data);
Dout7times = Dout7.fields(1).data(IndsDout7);
Dout7hits = Dout7.fields(2).data(IndsDout7);
rightsolzeros = zeros([length(Dout7hits),1]);
RightSolenoid = [Dout7times, Dout7hits, rightsolzeros];

Dout9 = readTrodesExtractedDataFile(strcat(fileNameMask,'.dio_Dout9.dat'));
IndsDout9 = find(Dout9.fields(2).data);
Dout9times = Dout9.fields(1).data(IndsDout9);
Dout9hits = Dout9.fields(2).data(IndsDout9);
buzzerzeros = zeros([length(Dout9hits),1]);
Buzzer = [Dout9times, buzzerzeros, Dout9hits];

leftTrials = sortrows([LeftSolenoid; Buzzer]);

leftsolenoidInds = find(leftTrials(:,2)); %finds all the times where the left solenoid was triggered
leftRewardAvailInds = [];   
for i = [1:length(leftsolenoidInds)]; %each time left solenoid triggered, checks to see if buzzer went off immediately after. 
    currIndex = leftsolenoidInds(i);
    if currIndex ~= length(leftTrials),
        buzzerCheck = leftTrials(currIndex+1,3);
            if buzzerCheck == 1,
            leftRewardAvailInds = [leftRewardAvailInds; currIndex]; %beginning of trials (after successful nose pokes), next reward well triggered is correct or incorrect trial
            end
    end
end

lefttrials = leftTrials(:,1:2);
leftSolenoidTriggered = lefttrials(leftRewardAvailInds,1)/30000;

rightTrials = sortrows([RightSolenoid; Buzzer]);

rightsolenoidInds = find(rightTrials(:,2)); 
rightRewardAvailInds = [];   
for i = 1:length(rightsolenoidInds); 
    currIndex = rightsolenoidInds(i);
        if currIndex ~= length(rightTrials),
        buzzerCheck = rightTrials(currIndex+1,3);
            if buzzerCheck == 1,
            rightRewardAvailInds = [rightRewardAvailInds; currIndex]; 
            end
        end
end

righttrials = rightTrials(:,1:2);
rightSolenoidTriggered = righttrials(rightRewardAvailInds,1)/30000;

OdorTriggers = sortrows([leftSolenoidTriggered; rightSolenoidTriggered]);
savename = ('F:\Data\OdorPlaceAssociation\CS15_direct\OdorTriggers_all');
save(savename);



