%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Takes DIO data extracted from .rec files for one day of behavior on Odor
%%% Place Association Task (T-Maze) and outputs timestamps for each
%%% completed trial with 1s and 0s, where 1 = correct and 0 = incorrect.
%%%
%%% fileNameMask example = 'CS15_20161101_', change for each experiment day
%%% As written, port identities are:
%%%     Din1 = Left Reward Well
%%%     Din2 = Right Reward Well
%%%     Dout6 = Left Solenoid
%%%     Dout7 = Right Solenoid
%%%     Dout9 = buzzer
%%% Must change if different ports were used for reward wells, solenoids,
%%% or buzzer
%%%
%%% Trial initiated when either Dout6 or Dout7 is triggered (solenoid),
%%% followed by Dout9 (buzzer, indicating nose was held for long enough).
%%% Trials classified as correct or incorrect based on whether Din1 or Din2
%%% (reward wells) was triggered after  that.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [CorrectIncorrectBinary] = getPerfBinary(fileNameMask)


Din1 = readTrodesExtractedDataFile(strcat(fileNameMask,'.dio_Din1.dat'));
IndsDin1 = find(Din1.fields(2).data); %finds all points where state was "up"
Din1times = Din1.fields(1).data(IndsDin1);
Din1hits = Din1.fields(2).data(IndsDin1);
leftwellzeros = zeros([length(Din1hits),1]);
LeftWell = [Din1times, leftwellzeros, Din1hits, leftwellzeros];

Din2 = readTrodesExtractedDataFile(strcat(fileNameMask,'.dio_Din2.dat'));
IndsDin2 = find(Din2.fields(2).data); 
Din2times = Din2.fields(1).data(IndsDin2);
Din2hits = Din2.fields(2).data(IndsDin2);
rightwellzeros = zeros([length(Din2hits),2]);
RightWell = [Din2times, rightwellzeros, Din2hits];

Dout6 = readTrodesExtractedDataFile(strcat(fileNameMask,'.dio_Dout6.dat'));
IndsDout6 = find(Dout6.fields(2).data);
Dout6times = Dout6.fields(1).data(IndsDout6);
Dout6hits = Dout6.fields(2).data(IndsDout6);
leftsolzeros = zeros([length(Dout6hits),1]);
LeftSolenoid = [Dout6times, Dout6hits, leftsolzeros];

Dout7 = readTrodesExtractedDataFile(strcat(fileNameMask,'.dio_Dout7.dat'));
IndsDout7 = find(Dout7.fields(2).data);
Dout7times = Dout7.fields(1).data(IndsDout7);
Dout7hits = Dout7.fields(2).data(IndsDout7);
rightsolzeros = zeros([length(Dout7hits),1]);
RightSolenoid = [Dout7times, Dout7hits, rightsolzeros];

Dout9 = readTrodesExtractedDataFile(strcat(fileNameMask,'.dio_Dout9.dat'));
IndsDout9 = find(Dout9.fields(2).data);
Dout9times = Dout9.fields(1).data(IndsDout9);
Dout9hits = Dout9.fields(2).data(IndsDout9);
buzzerzeros = zeros([length(Dout9hits),1]);
Buzzer = [Dout9times, buzzerzeros, Dout9hits];

leftTrials = sortrows([LeftSolenoid; Buzzer]);

leftsolenoidInds = find(leftTrials(:,2)); %finds all the times where the left solenoid was triggered
leftRewardAvailInds = [];   
for i = [1:length(leftsolenoidInds)]; %each time left solenoid triggered, checks to see if buzzer went off immediately after. 
    currIndex = leftsolenoidInds(i);
    if currIndex ~= length(leftTrials),
        buzzerCheck = leftTrials(currIndex+1,3);
            if buzzerCheck == 1,
            leftRewardAvailInds = [leftRewardAvailInds; currIndex]; %beginning of trials (after successful nose pokes), next reward well triggered is correct or incorrect trial
            end
    end
end

lefttrials = leftTrials(:,1:2);
leftRewardAvail = lefttrials(leftRewardAvailInds,1:2);
leftRewzeros = zeros([length(leftRewardAvailInds),2]);
leftRewardAvail = [leftRewardAvail, leftRewzeros; LeftWell; RightWell];
leftTrialData = sortrows(leftRewardAvail); %creates matrix with each time a successful nose poke occured, and when each reward well was triggered

leftTrialStartInds = find(leftTrialData(:,2));
leftCorrectTrialIndex = [];
leftIncorrectTrialIndex = [];
for i = [1:length(leftTrialStartInds)]; 
    currIndex = leftTrialStartInds(i); %find the start of each trial
    leftChoice = leftTrialData(currIndex+1,3); 
    rightChoice = leftTrialData(currIndex+1,4);
    if leftChoice == 1, %if left, correct choice
        leftCorrectTrialIndex = [leftCorrectTrialIndex; currIndex+1];
    end
    if rightChoice == 1, %if right, incorrect choice
        leftIncorrectTrialIndex = [leftIncorrectTrialIndex; currIndex+1];
    end
end

leftones = ones([length(leftCorrectTrialIndex) 1]);
correctLeft = [leftTrialData(leftCorrectTrialIndex), leftones];
leftzeros = zeros([length(leftIncorrectTrialIndex) 1]);
incorrectLeft = [leftTrialData(leftIncorrectTrialIndex), leftzeros];

leftCorrectIncorrect = sortrows([correctLeft; incorrectLeft]); %matrix of ones and zeros for correct/incorrect left trials only

%%%%%
%Repeats for right trials
%%%%%

rightTrials = sortrows([RightSolenoid; Buzzer]);

rightsolenoidInds = find(rightTrials(:,2)); 
rightRewardAvailInds = [];   
for i = 1:length(rightsolenoidInds); 
    currIndex = rightsolenoidInds(i);
        if currIndex ~= length(rightTrials),
        buzzerCheck = rightTrials(currIndex+1,3);
            if buzzerCheck == 1,
            rightRewardAvailInds = [rightRewardAvailInds; currIndex]; 
            end
        end
end

righttrials = rightTrials(:,1:2);
rightRewardAvail = righttrials(rightRewardAvailInds,1:2);
rightRewzeros = zeros([length(rightRewardAvailInds),2]);
rightRewardAvail = [rightRewardAvail, rightRewzeros; RightWell; LeftWell];
rightTrialData = sortrows(rightRewardAvail);

rightTrialStartInds = find(rightTrialData(:,2));
rightCorrectTrialIndex = [];
rightIncorrectTrialIndex = [];
for i = [1:length(rightTrialStartInds)];
    currIndex = rightTrialStartInds(i);
    leftChoice = rightTrialData(currIndex+1,3);
    rightChoice = rightTrialData(currIndex+1,4);
    if rightChoice == 1,
        rightCorrectTrialIndex = [rightCorrectTrialIndex; currIndex+1];
    end
    if leftChoice == 1,
        rightIncorrectTrialIndex = [rightIncorrectTrialIndex; currIndex+1];
    end
end

rightones = ones([length(rightCorrectTrialIndex) 1]);
correctRight = [rightTrialData(rightCorrectTrialIndex), rightones];
rightzeros = zeros([length(rightIncorrectTrialIndex) 1]);
incorrectRight = [rightTrialData(rightIncorrectTrialIndex), rightzeros];

rightCorrectIncorrect = sortrows([correctRight; incorrectRight]);

%Put right and left together
LeftRightPerf = double(sortrows([leftCorrectIncorrect; rightCorrectIncorrect]));
CorrectIncorrectBinary = LeftRightPerf(:,2);
clear

