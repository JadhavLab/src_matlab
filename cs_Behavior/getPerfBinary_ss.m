%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Takes stateScript file for one epoch of behavior on Odor
%%% Place Association Task (T-Maze) and outputs timestamps for each
%%% completed trial with 1s and 0s, where 1 = correct and 0 = incorrect.
%%%
%%% fileNameMask example = 'CS15_20161101_OdorPlace1', change for each
%%% run epoch
%%% 
%%% As written, port identities are:
%%%     Din1 = Left Reward Well
%%%     Din2 = Right Reward Well
%%%     Dout6 = Left Solenoid
%%%     Dout7 = Right Solenoid
%%%     Dout9 = buzzer
%%% Must change if different ports were used for reward wells, solenoids,
%%% or buzzer
%%% 
%%% Uses parseStateScriptFile9 (takes into account 9 DIOs, instead of just
%%% 8. Can change back to original parseStateScript if reward wells,
%%% solenoids, and buzzer are all within first 8 DIOs.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [CorrectIncorrectBinary] = getPerfBinary_ss(fileNameMask)

stateScriptStruct = parseStateScriptFile9(strcat(fileNameMask,'.stateScriptLog'));

for i= 1:length(stateScriptStruct)
   want_idx(i)= ~isempty(stateScriptStruct(i).inState);
end
stateScriptStruct = stateScriptStruct(want_idx);


time = cat(1,stateScriptStruct.time);
outState = [time, cat(1,stateScriptStruct.outState)];
inState = [time, cat(1,stateScriptStruct.inState)];

inStates = inState;%(any(inState(:,2:3),2),:); %remove all rows that have all zeros
outStates = outState;%(any(outState(:,2:10),2),:);

leftRewInds = find(inStates(:,2));
leftRewards = inStates(leftRewInds,1:2);
leftRewZeros = zeros([length(leftRewards),1]);
rightRewInds = find(inStates(:,3));
rightRewards = [inStates(rightRewInds,1),inStates(rightRewInds,3)];
rightRewZeros = zeros([length(rightRewards),2]);

bothSols = [outStates(:,1),outStates(:,7), outStates(:,8), outStates(:,10)];
leftsolenoidInds = find(bothSols(:,2)); %finds all the times where the left solenoid was triggered
leftRewardAvailInds = [];   
for i = [1:length(leftsolenoidInds)]; %each time left solenoid triggered, checks to see if buzzer went off immediately after. 
    currIndex = leftsolenoidInds(i);
    buzzerCheck = bothSols(currIndex,4);
    if buzzerCheck == 1,
        leftRewardAvailInds = [leftRewardAvailInds; currIndex]; %beginning of trials (after successful nose pokes), next reward well triggered is correct or incorrect trial
    end
end

leftTrials = bothSols(leftRewardAvailInds,1:2);
leftSolZeros = zeros([length(leftTrials),2]);
leftTrialData = sortrows([leftTrials, leftSolZeros; leftRewards(:,1), leftRewZeros, leftRewards(:,2), leftRewZeros; rightRewards(:,1), rightRewZeros, rightRewards(:,2)]);

leftTrialStartInds = find(leftTrialData(:,2));
leftCorrectTrialIndex = [];
leftIncorrectTrialIndex = [];
for i = [1:length(leftTrialStartInds)]; 
    currIndex = leftTrialStartInds(i); %find the start of each trial
    if currIndex ~= length(leftTrialData),
        leftChoice = leftTrialData(currIndex+1,3); 
        rightChoice = leftTrialData(currIndex+1,4);
            if leftChoice == 1, %if left, correct choice
                leftCorrectTrialIndex = [leftCorrectTrialIndex; currIndex+1];
            end
            if rightChoice == 1, %if right, incorrect choice
                leftIncorrectTrialIndex = [leftIncorrectTrialIndex; currIndex+1];
            end
    end
end
leftones = ones([length(leftCorrectTrialIndex) 1]);
correctLeft = [leftTrialData(leftCorrectTrialIndex), leftones];
leftzeros = zeros([length(leftIncorrectTrialIndex) 1]);
incorrectLeft = [leftTrialData(leftIncorrectTrialIndex), leftzeros];

leftCorrectIncorrect = sortrows([correctLeft; incorrectLeft]); %matrix of ones and zeros for correct/incorrect left trials only

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rightsolenoidInds = find(bothSols(:,3)); %finds all the times where the right solenoid was triggered
rightRewardAvailInds = [];   
for i = [1:length(rightsolenoidInds)]; %each time left solenoid triggered, checks to see if buzzer went off immediately after. 
    currIndex = rightsolenoidInds(i);
    buzzerCheck = bothSols(currIndex,4);
    if buzzerCheck == 1,
        rightRewardAvailInds = [rightRewardAvailInds; currIndex]; %beginning of trials (after successful nose pokes), next reward well triggered is correct or incorrect trial
    end
end

rightTrials = [bothSols(rightRewardAvailInds,1), bothSols(rightRewardAvailInds, 3)];
rightSolZeros = zeros([length(rightTrials),2]);
rightTrialData = sortrows([rightTrials, rightSolZeros; leftRewards(:,1), leftRewZeros, leftRewards(:,2), leftRewZeros; rightRewards(:,1), rightRewZeros, rightRewards(:,2)]);

rightTrialStartInds = find(rightTrialData(:,2));
rightCorrectTrialIndex = [];
rightIncorrectTrialIndex = [];
for i = [1:length(rightTrialStartInds)]; 
    currIndex = rightTrialStartInds(i); %find the start of each trial
        if currIndex ~= length(rightTrialData),
            leftChoice = rightTrialData(currIndex+1,3); 
            rightChoice = rightTrialData(currIndex+1,4);
            if leftChoice == 1, %if left, correct choice
                rightIncorrectTrialIndex = [rightIncorrectTrialIndex; currIndex+1];
            end
            if rightChoice == 1, %if right, incorrect choice
                rightCorrectTrialIndex = [rightCorrectTrialIndex; currIndex+1];
            end
        end
end
rightones = ones([length(rightCorrectTrialIndex) 1]);
correctRight = [rightTrialData(rightCorrectTrialIndex), rightones];
rightzeros = zeros([length(rightIncorrectTrialIndex) 1]);
incorrectRight = [rightTrialData(rightIncorrectTrialIndex), rightzeros];

rightCorrectIncorrect = sortrows([correctRight; incorrectRight]); %matrix of ones and zeros for correct/incorrect left trials only



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Put right and left together
LeftRightPerf = double(sortrows([leftCorrectIncorrect; rightCorrectIncorrect]));
CorrectIncorrectBinary = LeftRightPerf(:,2);
