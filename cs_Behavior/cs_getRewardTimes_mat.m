
function [RewardTimes] = cs_getRewardTimes_mat(prefix, days, runEps)

dataDir = ['F:\Data\OdorPlaceAssociation\', prefix, '_direct'];


for d = 1:length(days)
    daynum = days(d);
    all_DIOs = load([prefix, 'DIO0' num2str(daynum) '.mat']);
    
    for ep = 1:length(runEps),
        epoch = runEps(ep);
        
        Dout3times = all_DIOs.dio{1,daynum}{1,epoch}{1,19}.time;
        Dout3state = all_DIOs.dio{1,daynum}{1,epoch}{1,19}.state;
        Dout3hits = find(Dout3state);
        LeftRewardTimes = Dout3times(Dout3hits);
        
        Dout4times = all_DIOs.dio{1,daynum}{1,epoch}{1,20}.time;    
        Dout4state = all_DIOs.dio{1,daynum}{1,epoch}{1,20}.state;
        Dout4hits = find(Dout4state);
        RightRewardTimes = Dout4times(Dout4hits);
        
        RewardTimes = sortrows([LeftRewardTimes;RightRewardTimes]);
       
        cd(dataDir);
        save(['RewardTimes0',num2str(daynum),'-0',num2str(epoch)],'RewardTimes');
    end
end;
