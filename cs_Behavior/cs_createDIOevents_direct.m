
day = 4; %change this
all_DIOs = load(['CS15DIO0' num2str(day) '.mat']);
numEpochs = length(all_DIOs.dio{1,day});
Dout6times = [];

for i = 1:numEpochs,
    ep = i;
        Dout6OneEpoch = all_DIOs.dio{1,day}{1,ep}{1,22}.time;
        Dout6times = [Dout6times; Dout6OneEpoch];
end
Dout6hits = ones([length(Dout6times),1]);
leftsolzeros = zeros([length(Dout6hits),1]);
LeftSolenoid = [Dout6times, Dout6hits, leftsolzeros];

Dout7times = [];
for i = 1:numEpochs,
    ep = i;
        Dout7OneEpoch = all_DIOs.dio{1,day}{1,ep}{1,23}.time;
        Dout7times = [Dout7times; Dout7OneEpoch];
end
Dout7hits = ones([length(Dout7times),1]);
rightsolzeros = zeros([length(Dout7hits),1]);
RightSolenoid = [Dout7times, Dout7hits, rightsolzeros];

Dout9times = [];
for i = 1:numEpochs,
    ep = i;
        Dout9OneEpoch = all_DIOs.dio{1,day}{1,ep}{1,25}.time;
        Dout9times = [Dout9times; Dout9OneEpoch];
end
Dout9hits = ones([length(Dout9times),1]);
buzzerzeros = zeros([length(Dout9hits),1]);
Buzzer = [Dout9times, buzzerzeros, Dout9hits];



leftTrials = sortrows([LeftSolenoid; Buzzer]);

leftsolenoidInds = find(leftTrials(:,2)); %finds all the times where the left solenoid was triggered
leftRewardAvailInds = [];   
for i = [1:length(leftsolenoidInds)]; %each time left solenoid triggered, checks to see if buzzer went off immediately after. 
    currIndex = leftsolenoidInds(i);
    if currIndex ~= length(leftTrials),
        buzzerCheck = leftTrials(currIndex+1,3);
            if buzzerCheck == 1,
            leftRewardAvailInds = [leftRewardAvailInds; currIndex]; %beginning of trials (after successful nose pokes), next reward well triggered is correct or incorrect trial
            end
    end
end

lefttrials = leftTrials(:,1:2);
leftSolenoidTriggered = lefttrials(leftRewardAvailInds,1);

rightTrials = sortrows([RightSolenoid; Buzzer]);

rightsolenoidInds = find(rightTrials(:,2)); 
rightRewardAvailInds = [];   
for i = 1:length(rightsolenoidInds); 
    currIndex = rightsolenoidInds(i);
        if currIndex ~= length(rightTrials),
        buzzerCheck = rightTrials(currIndex+1,3);
            if buzzerCheck == 1,
            rightRewardAvailInds = [rightRewardAvailInds; currIndex]; 
            end
        end
end

righttrials = rightTrials(:,1:2);
rightSolenoidTriggered = righttrials(rightRewardAvailInds,1);

OdorTriggers_all = sortrows([leftSolenoidTriggered; rightSolenoidTriggered]);
savename = (['F:\Data\OdorPlaceAssociation\CS15_direct\OdorTriggers_day',num2str(day)]);
save(savename);



