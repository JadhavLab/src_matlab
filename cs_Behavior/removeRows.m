%look at next index value in OdorRewardTimes after each solenoidOnTime Index
%Check to see if the value in the second column in that row is 1
%if it is, delete that row.
rowsToDelete = [];
for i = [1:103],
    currIndex = solenoidOnInds(i);
    value = OdorRewardTimes(currIndex+1,2);
    if value == 1,
        rowsToDelete(i) = currIndex+1;
    end
end
rowsToDelete = rowsToDelete(rowsToDelete~=0);
OdorRewardTimesFinal = removerows(OdorRewardTimes,rowsToDelete);


newSolenoidInds = find(OdorRewardTimesFinal(:,2));
correctTrialIndex = [];
incorrectTrialIndex = [];
for i = [1:length(newSolenoidInds)];
    currIndex = newSolenoidInds(i);
    leftValue = OdorRewardTimesFinal(currIndex+1,3);
    if leftValue == 1,
        correctTrialIndex = [correctTrials; currIndex+1];
    else
        incorrectTrialIndex = [incorrectTrials; currIndex+1];
    end
end