
function [params] = cs_eventTrigEEG(prefix, day, epoch, tet, timewin) %add trigtype


if (day<10)
    daystring = ['0',num2str(day)];
else
    daystring = num2str(day);
end

if (epoch<10)
    epochstring = ['0',num2str(epoch)];
else
    epochstring = num2str(epoch);
end

if (tet<10)
    tetstring = ['0',num2str(tet)];
else
    tetstring = num2str(tet);
end

EEGfile = load([prefix, 'eeg', daystring, '-', epochstring, '-', tetstring, '.mat']);
    eegdata = EEGfile.eeg{1,day}{1,epoch}{1,tet}.data;
    starttime = EEGfile.eeg{1,day}{1,epoch}{1,tet}.starttime;
    endtime = EEGfile.eeg{1,day}{1,epoch}{1,tet}.endtime;
    samprate = EEGfile.eeg{1,day}{1,epoch}{1,tet}.samprate;

eegtimes = [starttime:(1/samprate):endtime]';

trigfile = load(['OdorTriggers', daystring, '-', epochstring, '.mat']);
    triggers = trigfile.OdorTriggers_all;

figure,
for t = 1:length(triggers),
    trigtime = triggers(t);
    triggeredTimeWindowStart = trigtime - timewin(1,1);
    triggeredTimeWindowEnd = trigtime + timewin(1,2);
    triggeredEEGtimes = eegtimes(eegtimes>triggeredTimeWindowStart & eegtimes<triggeredTimeWindowEnd);
    eegTimeInds = find(eegtimes>triggeredTimeWindowStart & eegtimes<triggeredTimeWindowEnd);
    triggeredEEG = eegdata(eegTimeInds);
    
    normalizedEEGtimes = (triggeredEEGtimes - trigtime);
    
    
    subplot(6, 6, t)
    
    
    plot(normalizedEEGtimes,triggeredEEG)
   
    hold on
    ypts = (min(triggeredEEG):max(triggeredEEG));
    xpts = zeros(size(ypts));
    plot(xpts , ypts, 'k--', 'Linewidth',2);
    
end
