function [CorrectIncorrectBinary, OdorTriggers_all] = cs_getOdorTriggersAndBinaryPerf_mat(prefix, days, runEps)

dataDir = ['F:\Data\OdorPlaceAssociation\', prefix, '_direct'];


for d = 1:length(days),
    day = days(d);
    all_DIOs = load(['CS15DIO0' num2str(day) '.mat']);
    numEpochs = length(all_DIOs.dio{1,day});
   
    for ep = runEps,
        
        Din1times = all_DIOs.dio{1,day}{1,ep}{1,1}.time;      
        Din1hits = ones([length(Din1times),1]);
        leftwellzeros = zeros([length(Din1hits),1]);
        LeftWell = [Din1times, leftwellzeros, Din1hits, leftwellzeros];

        Din2times = all_DIOs.dio{1,day}{1,ep}{1,2}.time;
        Din2hits = ones([length(Din2times),1]);
        rightwellzeros = zeros([length(Din2hits),2]);
        RightWell = [Din2times, rightwellzeros, Din2hits];

        Dout6times = all_DIOs.dio{1,day}{1,ep}{1,22}.time;
        Dout6hits = ones([length(Dout6times),1]);
        leftsolzeros = zeros([length(Dout6hits),1]);
        LeftSolenoid = [Dout6times, Dout6hits, leftsolzeros];

        Dout7times = all_DIOs.dio{1,day}{1,ep}{1,23}.time;
        Dout7hits = ones([length(Dout7times),1]);
        rightsolzeros = zeros([length(Dout7hits),1]);
        RightSolenoid = [Dout7times, Dout7hits, rightsolzeros];

        Dout9times = all_DIOs.dio{1,day}{1,ep}{1,25}.time;
        Dout9hits = ones([length(Dout9times),1]);
        buzzerzeros = zeros([length(Dout9hits),1]);
        Buzzer = [Dout9times, buzzerzeros, Dout9hits];



        leftAttemps = sortrows([LeftSolenoid; Buzzer]);

        leftsolenoidInds = find(leftAttemps(:,2)); %finds all the times where the left solenoid was triggered
        leftRewardAvailInds = [];   
        for i = [1:length(leftsolenoidInds)]; %each time left solenoid triggered, checks to see if buzzer went off immediately after. 
            currIndex = leftsolenoidInds(i);
            if currIndex ~= length(leftAttemps),
                buzzerCheck = leftAttemps(currIndex+1,3);
                    if buzzerCheck == 1,
                    leftRewardAvailInds = [leftRewardAvailInds; currIndex]; %beginning of trials (after successful nose pokes), next reward well triggered is correct or incorrect trial
                    end
            end
        end

        leftTrials = leftAttemps(:,1:2);
        leftRewardAvail = leftTrials(leftRewardAvailInds,1:2);
        leftRewzeros = zeros([length(leftRewardAvailInds),2]);
        leftRewardAvail = [leftRewardAvail, leftRewzeros; LeftWell; RightWell];
        leftTrialData = sortrows(leftRewardAvail); %creates matrix with each time a successful nose poke occured, and when each reward well was triggered

        leftTrialStartInds = find(leftTrialData(:,2));
        leftCompleteTrialIndex = [];
        leftCorrectTrialIndex = [];
        leftIncorrectTrialIndex = [];
        for i = [1:length(leftTrialStartInds)]; 
            currIndex = leftTrialStartInds(i); %find the start of each trial
            leftChoice = leftTrialData(currIndex+1,3); 
            rightChoice = leftTrialData(currIndex+1,4);
            if leftChoice == 1 || rightChoice == 1, 
                leftCompleteTrialIndex = [leftCompleteTrialIndex; currIndex];
            end
            if leftChoice == 1,
                leftCorrectTrialIndex = [leftCorrectTrialIndex; currIndex+1];
            end
            if rightChoice == 1, %if right, incorrect choice
                leftIncorrectTrialIndex = [leftIncorrectTrialIndex; currIndex+1];
            end
        end

        leftCompleteTrials = leftTrialData(leftCompleteTrialIndex);
        leftones = ones([length(leftCorrectTrialIndex) 1]);
        correctLeft = [leftTrialData(leftCorrectTrialIndex), leftones];
        leftzeros = zeros([length(leftIncorrectTrialIndex) 1]);
        incorrectLeft = [leftTrialData(leftIncorrectTrialIndex), leftzeros];

        leftCorrectIncorrect = sortrows([correctLeft; incorrectLeft]); %matrix of ones and zeros for correct/incorrect left trials only




        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        rightAttempts = sortrows([RightSolenoid; Buzzer]);

        rightsolenoidInds = find(rightAttempts(:,2)); 
        rightRewardAvailInds = [];   
        for i = 1:length(rightsolenoidInds); 
            currIndex = rightsolenoidInds(i);
                if currIndex ~= length(rightAttempts),
                buzzerCheck = rightAttempts(currIndex+1,3);
                    if buzzerCheck == 1,
                    rightRewardAvailInds = [rightRewardAvailInds; currIndex]; 
                    end
                end
        end

        rightTrials = rightAttempts(:,1:2);
        rightRewardAvail = rightTrials(rightRewardAvailInds,1:2);
        rightRewzeros = zeros([length(rightRewardAvailInds),2]);
        rightRewardAvail = [rightRewardAvail, rightRewzeros; RightWell; LeftWell];
        rightTrialData = sortrows(rightRewardAvail);

        rightTrialStartInds = find(rightTrialData(:,2));
        rightCompleteTrialIndex = [];
        rightCorrectTrialIndex = [];
        rightIncorrectTrialIndex = [];
        for i = [1:length(rightTrialStartInds)];
            currIndex = rightTrialStartInds(i);
            leftChoice = rightTrialData(currIndex+1,3);
            rightChoice = rightTrialData(currIndex+1,4);
            if rightChoice == 1 || leftChoice == 1,
                rightCompleteTrialIndex = [rightCompleteTrialIndex; currIndex];
            end
            if rightChoice == 1,
                rightCorrectTrialIndex = [rightCorrectTrialIndex; currIndex+1];
            end
            if leftChoice == 1,
                rightIncorrectTrialIndex = [rightIncorrectTrialIndex; currIndex+1];
            end
        end

        rightCompleteTrials = rightTrialData(rightCompleteTrialIndex);
        rightones = ones([length(rightCorrectTrialIndex) 1]);
        correctRight = [rightTrialData(rightCorrectTrialIndex), rightones];
        rightzeros = zeros([length(rightIncorrectTrialIndex) 1]);
        incorrectRight = [rightTrialData(rightIncorrectTrialIndex), rightzeros];

        rightCorrectIncorrect = sortrows([correctRight; incorrectRight]);

        LeftRightPerf = double(sortrows([leftCorrectIncorrect; rightCorrectIncorrect]));
        CorrectIncorrectBinary = LeftRightPerf(:,2);


        OdorTriggers_all = sortrows([leftCompleteTrials; rightCompleteTrials]);
       
        cd(dataDir);
        save(['OdorTriggers0',num2str(day),'-0',num2str(ep)],'OdorTriggers_all');
        save(['BinaryPerf0',num2str(day),'-0',num2str(ep)],'CorrectIncorrectBinary');
    end
end