newSolenoidInds = find(OdorRewardTimesFinal(:,2));
correctTrialIndex = [];
incorrectTrialIndex = [];
for i = [1:length(newSolenoidInds)];
    currIndex = newSolenoidInds(i);
    leftValue = OdorRewardTimesFinal(currIndex+1,3);
    if leftValue == 1,
        correctTrialIndex = [correctTrialIndex; currIndex+1];
    else
        incorrectTrialIndex = [incorrectTrialIndex; currIndex+1];
    end
end