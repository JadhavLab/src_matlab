function ccg = kkxcorr(s1, s2, edges)

% INPUT:
% s1 - timestamp array, center of cross-correlogram  ("ref" below)
% s2 - timestamp array                               ("sig" below)
% window - the window of the timestamps to compute, e.g. [-0.1 0.1] for
%           100 milliseconds around each spike.
% binsize - in sec
%
% OUTPUT:
% offsets - the offsets from each spike in ts1 that has spikes nearby
% s1ind - the index into s1 for each offset in tsOffsets
% s2ind - the index into s2 for each offset in tsOffsets

s1 = sort(s1,'ascend');
s2 = sort(s2,'ascend');
	
numspikes1 = length(s1);
numspikes2 = length(s2);
win = edges(end);

% initialize outputs
offsets = [];
s1ind = [];
s2ind = [];

if isempty(s1) || isempty(s2)
    ccg = [];
    return
end

% iterate through each of cell1's spikes
i1 = 1;
while i1 <= numspikes1
    
    ref = s1(i1);   % time of cell1's spike
    
    % iterate thorugh cell2's spikes
    i2 = 1;
    while i2 <= numspikes2
     
        sig = s2(i2);   % time of cell2's spike
        
        if sig < ref-win
            i2 = i2 + 1;
        elseif (sig >= ref-win)  &&  (sig <= ref+win)
            offsets = [offsets ; sig-ref ];
            s1ind = [s1ind ; i1];
            s2ind = [s2ind ; i2];
            i2 = i2 + 1;
        elseif sig > ref+win 
            break
        end
        
    end

    i1 = i1 + 1;

end


% histogram offsets here
ccg = histc(offsets,edges);
ccg = ccg(:)';

end

