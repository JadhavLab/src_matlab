datadir = '/opt/data13/kkay/Superxcorr_data/';        % the datadir to store processed files

setparams = 1;
runscript = 1;
postprocessing = 0;
plotmanual = 0;
    if plotmanual
        % data collection
        collectdata = 1;            % this needs to run before plotting can occur
            autocorr_flag = 1;      % ==1 if plot only autocorr, ==0 if only xcorr    
            plot_win = [-500 500];          %[-100 100];  % in ms -- leave blank if want to do just bincenters
        % unit selection
        load('/opt/data13/kkay/Superlin_data/Classtable_06-Feb-2015.mat','classtable6','classtable6_adtc')
        animals_order = {'Government','Egypt','Chapati','Dave','Higgs','Frank','Bond','Corriander'}; 
        ca2p = {[classtable6_adtc{2}{1} ; classtable6_adtc{2}{4} ],'ca2p'} ;
        ca2n = {[classtable6_adtc{2}{2} ; classtable6_adtc{2}{3} ],'ca2n' };
        ca1  = {[classtable6_adtc{1}{1} ; classtable6_adtc{1}{2} ],'ca1' };
        ca3 =  {[classtable6_adtc{3}{1} ; classtable6_adtc{3}{2} ],'ca3' };
        %nonNwell = {[well_adtc_nonN],'nonNwell'};
        % unit selection (reference and signal groups)
        adtc_ref = ca2n ;   % adtc of reference units
        adtc_sig = ca2n ;   % adtc of 'signal' units
        % plot params
        smooth_flag = 1;  % toggle if want to gaussian kernel smooth each xcorr
            kernelsize = 5;
        states_toplot = [1:9] ;
        minspikes = 50 ;
        
        timeorder = 1;   % set ==1 if order by time of highest peak, ==0 if by center firing rate, specified by avgwin
                avgwin = [-50 50];
    end
if setparams
    numstates = 9;
    maxtime = 1;  %  0.5; % in sec
    binsize = 0.001;
        edges = (-maxtime-binsize/2):binsize:(maxtime+binssize/2);
        bincenters = -maxtime:binsize:maxtime;
        numbins = length(bincenters);
    % shift
    shiftsize = 0.1;
    % jitter 
    jitter_params = []; % [20 5 1000];  % maxtimejit (ms), jittersize (ms), and numperms, leave [] otherwise
        %edges_jit = (-jitter_params(1)-binsize/2):binsize:(jitter_params(1)+binsize/2);
end
if runscript
    animals_torun = {'Egypt','Chapati','Dave','Higgs','Frank','Bond','Corriander'}; 
    states_torun = [10];
end
if postprocessing
    animals_toprocess = {'Government','Egypt','Chapati','Dave','Higgs','Frank','Bond','Corriander'}; 
    states_toprocess = [1:10];
end

if runscript
    clear f
    iterator = 'kk_multicellanal';
    for aa = 1:length(animals_torun)
        for state = states_torun
            disp(['*************** STATE ' num2str(state) ' *************************'])
            
            timefilterscript   % contains timefilter cell entries for each condition
            
            if state == 1                                       % 1: LOCOMOTION
                timefilter = { locomotion };
                epochfilter = epochmaker('runlinear_all');
            elseif state == 2                                   % 2: HIGH SPEED
                timefilter = { highspeed };
                epochfilter = epochmaker('runlinear_all');
            elseif state == 3                                   % 3: IMMOBILE2, NORIP
                timefilter = { immobile2, norip };
                epochfilter = epochmaker('runlinear_all');
            elseif state == 4                                   % 4: IMMOBILE2
                timefilter = { immobile2 };
                epochfilter = epochmaker('runlinear_all');
            elseif state == 5                                   % 5: RIP
                timefilter = { rip2 };
                epochfilter = epochmaker('runlinear_all');
            elseif state == 6                                   % 6: RIPVEL
                timefilter = { ripvel };
                epochfilter = epochmaker('runlinear_all');
            elseif state == 7                                   % 7: WELL4
                timefilter = { well4 };
                epochfilter = epochmaker('runlinear_all');
            elseif state == 8                                   % 8: SLEEPC
                timefilter = { sleepc };
                epochfilter = epochmaker('sleep_inclusive');
            elseif state == 9                                   % 9: SLEEPC NORIP
                timefilter = { sleepc, norip };                 
                epochfilter = epochmaker('sleep_inclusive');
            elseif state == 10                                   % 10: ALL CLUSTERED EPOCHS + STATES
                timefilter = {};                 
                epochfilter = epochmaker('runlinear_all+sleep_inclusive');                     
                
            end
            datestring = date;
            runscript_params = paramsstruct(timefilter,epochfilter,datestring,edges,jitter_params,shiftsize);            
            disp(animals_torun{aa})    
            f{1}{state} = createfilter('animal',animals_torun{aa},'epochs',epochfilter,'cells','',...
                                        'excludetime', timefilter,'iterator', iterator);
            f{1}{state} = setfilterfunction(f{1}{state}, 'dfakk_getspikecorr', {'spikes'},'edges',edges,'shiftsize',shiftsize);
            f{1}{state} = runfilter(f{1}{state});
            f{1}{state}.runscript_params = runscript_params;
            cd(datadir)
            save(sprintf('Spikecorr_state%d_%s',state,animals_torun{aa}(1:3)),'f','-v7.3');
            clear f
        end
    end
end


if postprocessing
    
    cd(datadir)
    % consolidate pairs (within animals) across epochs to obtain .pairoutput
    for an = 1:length(animals_toprocess)
        
        dtctc_list = [];
        X = [];
        Xs = [];
        
        % First create an (orderly) list of cell pairs from cellinfo
            % (dtctc_list: day tet1 cell1 tet2 cell2)    
            % load cellinfo
        animalname = animals_toprocess{an};
            animalinfo = animaldef(animalname);
            cellinfo = loaddatastruct(animalinfo{2},animalinfo{3},'cellinfo');
                detc = evaluatefilter(cellinfo,'');
                    dtc = unique(detc(:,[1 3 4]),'rows');
                        days = unique(dtc(:,1))';
            % iterate days
        for d = days
            index = dtc(dtc(:,1)==d,:) ;   % indices of units for a day
            numunits = size(index,1);
            if numunits > 1
                pairlist = combnk([1:numunits],2);                    % combinations
                pairlist = [pairlist ; (1:numunits)' (1:numunits)'];  % and self-pairs too, for autocorrelograms
                for jj = 1:size(pairlist,1)
                    a = pairlist(jj,1);    % Reference neuron
                    t1 = index(a,2);
                    c1 = index(a,3);
                    b = pairlist(jj,2);    % Nonreference / 'signal' neuron
                    t2 = index(b,2);
                    c2 = index(b,3);
                    % order pairs such that lowest tet #'s and cell #'s go first
                    if t1 < t2
                        dtctc_list = [dtctc_list ; d t1 c1 t2 c2  ];
                    elseif t1 == t2
                        if c1 < c2
                            dtctc_list = [dtctc_list ; d t1 c1 t2 c2  ];
                        else
                            dtctc_list = [dtctc_list ; d t1 c2 t2 c1  ];
                        end
                    elseif t1 > t2
                        dtctc_list = [dtctc_list ; d t2 c2 t1 c1  ];
                    else
                        error('should not happen')
                    end
                end
            end
        end
        
        % Next, load each state
        totalnumpairs = size(dtctc_list,1);
        maxstate = max(states_toprocess);
        X = nan(maxstate,numbins,totalnumpairs);  % initialize output variable
        Xs = nan(maxstate,numbins,totalnumpairs);  % initialize output variable
        for state = states_toprocess
            load(sprintf('Spikecorr_state%d_%s.mat',state,animalname(1:3)),'f')
            o = f{1}{state}.output{1};
            % iterate through each multicellanal epoch dataset
            for ee = 1:length(o)
                if isempty(o(ee).CCG)
                    continue
                end
                dayep = o(ee).dayep;
                % iterate through each unit pair
                numpairs = size(o(ee).pairmatrix,3);
                for pp = 1:numpairs
                    data = o(ee).CCG{pp};
                    data_shift = o(ee).CCG_shift{pp};
                    if ~isempty(data)
                        flipflag = nan;
                        tctc = o(ee).pairmatrix(:,:,pp);
                        tctc_sort = sortrows(tctc,[3 4]);  % sort (if necessary) to put smaller tet-cell first
                        dtctc = [dayep(1) tctc_sort(1,3:4) tctc_sort(2,3:4)];  % the ordered version
                        
                        % if unit pair indices are flipped, then flip the data
                        if all(tctc_sort(:) ~= tctc(:))
                            data = fliplr(data);
                            data_shift = fliplr(data_shift);
                        end
                        
                        % Search for this pair's index in the output matrix
                        ii = rowfind(dtctc,dtctc_list);
                        % Actual data
                        if any(isnan(X(state,:,ii)))
                            X(state,:,ii) = data(1:(end-1));   % first entry
                        else
                            X(state,:,ii) = X(state,:,ii) + data(1:(end-1));  % add to existing histogram 
                        end
                        % Shifted data
                        if ~isempty(o(ee).CCG_shift{pp}(1:(end-1)))
                            if any(isnan(Xs(state,:,ii)))
                                Xs(state,:,ii) = data_shift(1:(end-1));   % first entry
                            else
                                Xs(state,:,ii) = Xs(state,:,ii) + data_shift(1:(end-1));  % add to existing histogram
                            end
                        end
                    end
                end
            end
        end
        
       C.animalname = animalname;
       C.states_processed = states_toprocess;
       C.adtctc = [an*ones(totalnumpairs,1)   dtctc_list];
       C.X = X;
       C.Xs = Xs;
       
       cd(datadir)
       save(sprintf('Spikecorr_%s.mat',animalname(1:3)),'C','-v7.3');
       clear C
end
end



if plotmanual
    
 if collectdata
    clear P
    P.adtctc_plot = [];
    P.X_plot = [];
    
    % First identify which animals to iterate through
    anims1 = unique(adtc_ref{1}(:,1));   % adtc of reference units
    anims2 = unique(adtc_sig{1}(:,1));   % adtc of 'signal' units

    anims = anims1(ismember(anims1,anims2));  % shared animals
    
    % iterate animals
    for an = 1:length(anims)
        
        % initialize
        dtctc_list = [];
        X_anim = [];
        
        animalname = animals_order{an};
        
        % load animal's data
        load(sprintf('%sSpikecorr_%s.mat',datadir,animalname(1:3)),'C');
        
        dtc_ref = adtc_ref{1}(adtc_ref{1}(:,1)==an,2:4);
        dtc_sig = adtc_sig{1}(adtc_sig{1}(:,1)==an,2:4);
       
        % identify days
        days1 = unique(dtc_ref(:,1));  
        days2 = unique(dtc_sig(:,1));   
        days = days1(ismember(days1,days2))';   % shared days
        
        % iterate days
        for d = days
            
            % identify pairs of cells
            tc_ref = dtc_ref(dtc_ref(:,1)==d,2:3);
            tc_sig = dtc_sig(dtc_sig(:,1)==d,2:3);
            
            numref = size(tc_ref,1);
            numsig = size(tc_sig,1);
            
            % iterate through pairs
            pairlist = combvec(1:numref,1:numsig)';
            numpairs = size(pairlist,1);
            for pp = 1:numpairs
                
                % i. determine dtctc
                a = pairlist(pp,1);   % a is index of REF unit
                t1 = tc_ref(a,1);
                c1 = tc_ref(a,2);
                b = pairlist(pp,2);  % b is index of SIG unit
                t2 = tc_sig(b,1);
                c2 = tc_sig(b,2);
                dtctc = [d t1 c1 t2 c2];
                
                % order the tet-cell pair such that lower tet # and cell # goes first
                dtctc_sort = [];
                if t1 < t2
                    dtctc_sort = [d t1 c1 t2 c2 ];
                elseif t1 == t2
                    if c1 < c2
                        dtctc_sort = [ d t1 c1 t2 c2  ];
                    else
                        dtctc_sort = [ d t1 c2 t1 c1  ];
                    end
                elseif t1 > t2
                    dtctc_sort = [ d t2 c2 t1 c1  ];
                else
                    error('should not happen')
                end
                
                % check if there's a need to flip the xcorr data
                flipflag = nan;
                if all(dtctc_sort == [d t1 c1 t2 c2])
                    flipflag = 0;
                else
                    %disp('flipped')
                    flipflag = 1;
                end
                    
                % skip unmatched pairs if doing xcorr vs. autocorr
                if autocorr_flag
                    if t1 ~= t2 || c1 ~= c2
                        continue
                    end
                else
                    if t1 == t2 && c1 == c2
                        continue
                    end
                end
                
                % ii. find the data and append to P.
                ind = rowfind(dtctc_sort,C.adtctc(:,2:end));
                    data = C.X(:,:,ind);
                    % flip the data if necessary
                    if flipflag
                        data = fliplr(data);
                    end
                P.adtctc_plot = [ P.adtctc_plot ;   an   dtctc   ];
                P.X_plot = cat(3,P.X_plot,data);
                
            end
        end
        
    end
    
 end

    % unit selection
    
    for state = states_toplot
        
        H = figure;
        
        % filter for pairs with at least minimum # spikes
        if isempty(plot_win)
            inds = (sum(P.X_plot(state,:,:),2) >= minspikes);
            I = squeeze(P.X_plot(state,:,inds))';  % I is pair# x timebin
        else
            aa = lookup(plot_win(1),bincenters*1000);
            bb = lookup(plot_win(2),bincenters*1000);            
            inds = (sum(P.X_plot(state,aa:bb,:),2) >= minspikes);
            I = squeeze(P.X_plot(state,aa:bb,inds))';  % I is pair# x timebin
        end
        
        %skip if only 1 xcorrelogram
        if isempty(I) || any(size(I) == 1)
            continue
        end
        
        % normalize by total # spikes
        Inorm = bsxfun(@rdivide,I,sum(I,2));
        imagesc(edges*1000,1:size(Inorm,1),Inorm)
        
        % if autocorr, empty out the center bin
        if autocorr_flag
            midind = lookup(0,bincenters);
            Inorm(:,midind) = 0;
        end
        
        if smooth_flag
            % smooth indivdual xcorrs
            Inormsmooth = [];
            kernel =  gaussian(kernelsize,kernelsize*8); % in units of binsize
            for rr = 1:size(Inorm,1)
                Inormsmooth = [Inormsmooth ; smoothvect(Inorm(rr,:),kernel)];
            end
            % scale to max
            Inormsmoothscale = bsxfun(@rdivide,Inormsmooth,max(Inormsmooth,[],2));
            Iplot = Inormsmoothscale;
        else
            % scale to max
            if 1
                Inormscale = bsxfun(@rdivide,Inorm,max(Inorm,[],2));
            end           
            Iplot = Inormscale;
 
        end
        
        if timeorder
            [~,maxind] = max(Iplot,[],2);
            [~,ord] = sort(maxind,1,'ascend');
            Iplot = Iplot(ord,:);
        else  %reorder from lowest to highest in avgwin ms window
            zeroind = lookup(0,edges);
                firstwin = zeroind+round(avgwin(1)/1000);
                lastwin = zeroind+round(avgwin(2)/1000);
            winmean = mean(Iplot(:,firstwin:lastwin),2); 
            [~,ord] = sort(winmean,1,'ascend');
            Iplot = Iplot(ord,:);
            
        end
        
        % plot 
        if isempty(plot_win)
            imagesc(bincenters*1000,1:size(Iplot,1),Iplot);
            title(sprintf('REF: %s // SIG: %s xcorr, state %d',adtc_ref{2},adtc_sig{2},state));
        else
            imagesc(bincenters(aa:bb)*1000,1:size(Iplot,1),Iplot);
            title(sprintf('REF: %s // SIG: %s xcorr, state %d',adtc_ref{2},adtc_sig{2},state));
        end
        
        % center line
        hold on
        plot([0 0],[0 size(Iplot,1)],'w--','linewidth',2)
        
    end
    
    
    
end










