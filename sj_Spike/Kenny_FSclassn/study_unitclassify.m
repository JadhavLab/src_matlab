%%% Classifies units into interneurons and principal  %%%%%%%%%%%%%%%

% outputs:

% MASTER
    % adtc
    % adtc_inter
% MASTERHPC
    % masterlist:   [<adtc> <-1/nan/1>]  where -1 is inter, +1 is principal
    % masterdata:   [ep_max  fr_max  spikewidth_max   ac_mean];

% unitdata : [ep_max  fr_max  spikewidth_max   ac_mean];    

%clear points;
setunits = 1;      % set up units to classify
    if setunits
        animals_order = {'Government','Egypt','Chapati','Dave','Higgs','Frank','Bond','Corriander'};
        adtc = All_W_active; % specify units
        autocorrtime = 0.04;  % how many sec of the autocorr to take mean of (up to 0.5 sec) % Csicsvari--Buzsaki-1999 is 40 ms     
        minspikes_ac = 50;
    end

calculate = 1;   % calculates "unitdata" variable
plot_all = 0;   
plot_adtc_inter = 0;
    if plot_adtc_inter
        anim_toplot = []  %1;
    end
hpc_filter = 1;

% Non-principal reverse cluster cutting
if 0
    % step A: after selecting non-principal "points", convert into npindsX
    
    %inds = rowfind(points,unitdata(:,[2 3]));  % x: fr // y: spikewidth 
    %inds = rowfind(points,unitdata(:,[2 4]));  % x: fr // y: meanac
    %inds = rowfind(points,unitdata(:,[3 4]));  % x: spikewidth // y: meanac
    %inds = rowfind(points,unitdata(:,[4 3 2]));  % x: meanac // y: spikewidth // x: fr 
    
        % interneuron inds
    points;
    interinds = rowfind(points,unitdata(:,[4 3 2]));  % based on 3D plot
    %interinds = rowfind(points,unitdata(:,[2 3]));  % based on 2D fr-spikewidth plot
        % odd inds
    points1;
    points2;
    oddinds1 = rowfind(points1,unitdata(:,[4 3 2]));  % odd unit cluster cut based on 3D plot
    oddinds2 = rowfind(points2,unitdata(:,[4 3 2]));  % odd unit cluster cut based on 3D plot

    % sort here
    adtc_inter = sortrows(   adtc(unique([  interinds(:) ]),:) ,[1 2 3 4]);
    adtc_odd = sortrows(   adtc(unique([ oddinds1(:) ; oddinds2(:) ]),:) ,[1 2 3 4]);
    
    cd('/opt/data13/kkay/Unitgroups/')
    save('MASTER','adtc','adtc_inter','adtc_odd','unitdata','-v7.3')
end

% Below, identify adtc of PROBE point
if 0
    probinds = rowfind(probe,unitdata(:,[2 3]));  % x: fr // y: spikewidth 
    probinds = rowfind(probe,unitdata(:,[2 4]));  % x: fr // y: meanac
    probinds = rowfind(probe,unitdata(:,[4 3 2]));  % x: fr // y: meanac
    %probinds = rowfind(probe,unitdata(:,[3 4]));  % x: spikewidth // y: ac
    
    adtc_probe = sortrows(   adtc(probinds,:) ,[1 2 3 4])
    unitdata(probinds,:)
end


if hpc_filter
    
    manual_inter = [    7 9 11 2; 1 7 20 1];   % manually specified hpc interneurons
    manual_principal = [ 7 8 4 2]; % two manually specified hpc principals
    manual_unknown = [5 11 9 4; 7 8 4 2; 7 9 11 2; 1 6 12 1; 1 7 12 1; ...
                      1 8 12 4; 1 9 12 4; 1 10 12 6; 1 11 12 4; 1 12 12 1; ...
                      6 5 10 3; 6 7 10 4; 6 8 10 4; 6 9 10 4; 6 11 10 4  ; ...
                      2 5 7 1; 5 9 4 1 ; 5 14 4 2; 5 15 4 1; 6 5 29 3; 2 5 16 8];  % weird cluster -- calling unknown
    if 1
        manual_inter = [];
        manual_principal = []; %[1 6 20 1];
        manual_unknown = [];
    end
    
    % filter for hpc
    masterlist = [];
    masterdata = [];
    for cc = 1:size(adtc,1)
        if unitdata(cc,5) == 1  %%% filter for HPC unit (5th column)
            if ~isnan(prod(unitdata(cc,[2 3 4])))   % check that we have all 3 data points (fr, ac mean, spikewidth)
                if rowfind(adtc(cc,:),[manual_unknown])
                    masterlist = [masterlist ; adtc(cc,:) nan];
                elseif rowfind(adtc(cc,:),[manual_principal])
                    masterlist = [masterlist ; adtc(cc,:) 1];  % principal == 1
                elseif rowfind(adtc(cc,:),[manual_inter])
                    masterlist = [masterlist ; adtc(cc,:) -1]; % manual interneuron == -1
                elseif rowfind(adtc(cc,:),[adtc_inter])
                    masterlist = [masterlist ; adtc(cc,:) -1]; % cluster interneuron  == -1
                elseif rowfind(adtc(cc,:),[adtc_odd])
                    masterlist = [masterlist ; adtc(cc,:) nan]; % odd neuron
                else
                    masterlist = [masterlist ; adtc(cc,:) 1]; % odd neuron
                end
                masterdata = [masterdata ; unitdata(cc,[1 2 3 4])];
            end
        end
    end
    
    cd('/opt/data13/kkay/Unitgroups/')
    save('MASTERHPC','masterlist','masterdata','-v7.3')
    
    % 3D plot masterlist / masterdata %%%%%%%%%%%%%%%%%%%%%%%%%%
    if 1
        figure('units','normalized','outerposition',[0 0 .5 .75])
        prinds = masterlist(:,5) == 1;
        intinds = masterlist(:,5) == -1;
        unkinds = isnan( masterlist(:,5) );
        
        scatter3(masterdata(unkinds,4),masterdata(unkinds,3),masterdata(unkinds,2),18,[0 0 0],'s'); hold on
        scatter3(masterdata(prinds,4),masterdata(prinds,3),masterdata(prinds,2),50,'k','+','linewidth',.6); hold on
        scatter3(masterdata(intinds,4),masterdata(intinds,3),masterdata(intinds,2),70,'MarkerEdgeColor',[0 0 0],'linewidth',.6); hold on
        %scatter3(masterdata(intinds,4),masterdata(intinds,3),masterdata(intinds,2),42,[0 0 0],'o','linewidth',1.2); hold on
        
        
        % select a unit to highlight %%%%%%%%%%%%%%%5
        if 0
        manual_highlight = [1 13 12 2];
        for mm = 1:size(manual_highlight,1)
           ind = rowfind(manual_highlight(mm,:),masterlist(:,[1 2 3 4]));
           scatter3(masterdata(ind,4),masterdata(ind,3),masterdata(ind,2),150,[1 0 0],'o'); hold on
        end
        %%%%%%%%%%%
        end
        xlabel('AC mean','fontsize',20);
        ylabel('Spike width','fontsize',20);
        zlabel('Firing rate','fontsize',20);
        set(gca,'fontsize',18)
        xlim([0 40])
        ylim([0 25])
        zlim([-30 60])
        set(gca,'xdir','normal')
        set(gca,'ydir','reverse')
        set(gca,'zdir','normal')
    end

   
    % mean-ac plot
    if 1
        figure
        pinds = masterlist(:,5) == 1;
        interinds = masterlist(:,5) == -1;
        
        scatter(masterdata(pinds,2),masterdata(pinds,4),50,'k','+','linewidth',.6); hold on
        scatter(masterdata(interinds,2),masterdata(interinds,4),70,'markeredgecolor',[0 0 0],'linewidth',.6); hold on
        scatter(masterdata(unkinds,2),masterdata(unkinds,4),18,[0 0 0],'s'); hold on
        
        xlabel('Firing rate');
        ylabel('AC Mean');
        xlim([0 60])
        ylim([0 40])
        zlim([-30 60])
        set(gca,'xdir','normal')
        set(gca,'ydir','normal')
    end    

    % spikewidth-fr plot
    if 1
        figure
        pinds = masterlist(:,5) == 1;
        interinds = masterlist(:,5) == -1;
        
        scatter(masterdata(pinds,2),masterdata(pinds,3),50,'k','+','linewidth',.6); hold on
        scatter(masterdata(interinds,2),masterdata(interinds,3),70,'markeredgecolor',[0 0 0],'linewidth',.6); hold on
        scatter(masterdata(unkinds,2),masterdata(unkinds,3),18,[0 0 0],'s'); hold on
        
        xlabel('Firing Rate');
        ylabel('Spike width');
        xlim([0 60])
        ylim([0 25])
        zlim([-30 60])
        set(gca,'xdir','normal')
        set(gca,'ydir','normal')
    end        
    
    % spikewidth-ac fr plot
    if 1
        figure
        pinds = masterlist(:,5) == 1;
        interinds = masterlist(:,5) == -1;
        
        scatter(masterdata(pinds,3),masterdata(pinds,4),50,'k','+','linewidth',.6); hold on
        scatter(masterdata(interinds,3),masterdata(interinds,4),70,'markeredgecolor',[0 0 0],'linewidth',.6); hold on
        scatter(masterdata(unkinds,3),masterdata(unkinds,4),18,[0 0 0],'s'); hold on
        
        xlabel('Spike width');
        ylabel('AC mean');
        xlim([0 25])
        ylim([0 40])
        zlim([-30 60])
        set(gca,'xdir','normal')
        set(gca,'ydir','normal')
    end        
end



                      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize output
if calculate
    
    numunits = size(adtc,1);
    unitdata = nan(numunits,5);     % [ <ep_max>  <fr>  <spikewidth>  <autocorr_mean>  <hpc> ]
    
    % Identify each unit's highest mean fr run-linear epoch -- from it, collect fr and spikewidth
    animnums = unique(adtc(:,1))';
    for an = animnums
        inds = find(adtc(:,1)==an)';
        animalinfo = animaldef(animals_order{an});
        cellinfo = loaddatastruct(animalinfo{2},animalinfo{3},'cellinfo');
        tetinfo = loaddatastruct(animalinfo{2},animalinfo{3},'tetinfo');
        task = loaddatastruct(animalinfo{2},animalinfo{3},'task');
        for ii = inds
            fr_max = 0;
            ep_max = 0;
            spikewidth_max = 0;
            day = adtc(ii,2);
            tet = adtc(ii,3);
            cellnum = adtc(ii,4);
            %if all([an day tet cellnum] == [1     6    19     1])
            %    keyboard
            %end
            if any(strcmp(tetinfo{day}{end}{tet}.area,{'CA1','CA2','CA3','DG','septal'}))
                unitdata(ii,[5]) = [1];
            else
                unitdata(ii,[5]) = [0];
            end
            for ep = 1:length(cellinfo{day})
                % disregard non-W track epochs
                if ~isempty(task{day}{ep})
                    if isfield(task{day}{ep},'type')
                        if strcmp(task{day}{ep}.type,'run')
                            if ~any(strcmp(task{day}{ep}.environment,{'WTrackA' 'WTrackB' 'TrackA' 'TrackB'}))
                                continue
                            end
                        else
                            continue
                        end
                    else
                        continue
                    end
                else
                    continue
                end
                if ~isempty(cellinfo{day}{ep})
                    if tet <= length(cellinfo{day}{ep}) && ~isempty(cellinfo{day}{ep}{tet})
                        if cellnum <= length(cellinfo{day}{ep}{tet}) && ~isempty(cellinfo{day}{ep}{tet}{cellnum})
                            fr_ep = cellinfo{day}{ep}{tet}{cellnum}.meanrate;
                            if fr_ep > fr_max
                                ep_max = ep;
                                fr_max = fr_ep;
                                spikewidth_max = cellinfo{day}{ep}{tet}{cellnum}.spikewidth;
                            end
                        end
                    end
                end
            end
            % install values
            unitdata(ii,[1 2 3]) = [ep_max  fr_max  spikewidth_max];
        end
    end
    
    % From xcorr raw data file, get its mean autocorrelogram value
    
    for an = animnums
        
        cd('/opt/data13/kkay/Superxcorr_data')
        load(sprintf('Spikecorr_state10_%s.mat',animals_order{an}(1:3)),'f')
        animalinfo = animaldef(animals_order{an});
        cellinfo = loaddatastruct(animalinfo{2},animalinfo{3},'cellinfo');
        task = loaddatastruct(animalinfo{2},animalinfo{3},'task');
        
        inds = find(adtc(:,1)==an)';
        
        for ii = inds
            
            mean_autocorr = nan;
            
            day = adtc(ii,2);
            tet = adtc(ii,3);
            cellnum = adtc(ii,4);
            
            ep_max = unitdata(ii,1);
            
            for ee = 1:length(f{1}{10}.output{1})
                dataentry = f{1}{10}.output{1}(ee);
                if isempty(dataentry.dayep)
                    continue
                end
                if all(dataentry.dayep == [day  ep_max])
                    unitind = rowfind([day ep_max tet cellnum],dataentry.index);
                    if isempty(dataentry.pairmatrix)
                        continue
                    end
                    if unitind > 0
                        % search for pair index for the unit's autocorr
                        for pp = 1:size(dataentry.pairmatrix,3)
                            if all([day ep_max tet cellnum]==dataentry.pairmatrix(1,:,pp)) && ...  % autocorr entry is the one that matches both rows
                               all([day ep_max tet cellnum]==dataentry.pairmatrix(2,:,pp))
                                
                                binsize = diff(dataentry.edges(1:2));
                                bincenters = (dataentry.edges(1:(end-2)) + binsize/2 );  % convert to milliseconds
                                
                                a = length(bincenters)/2 + 2;          % index right after center bin (probably +1 ms)
                                b = lookup(autocorrtime,dataentry.edges);   % index of max autocorr measuring time
                                
                                acorr = dataentry.CCG{pp}(a:b);  
                                bincenters_acorr = bincenters(a:b);
                                
                                numspikes_ac = sum(acorr);
                                if numspikes_ac < minspikes_ac
                                    disp(sprintf('%s %d %d %d %d has < %d spikes in autocorr',...
                                        animalinfo{3}(1:3),day,ep_max,tet,cellnum,minspikes_ac))
                                    continue
                                end
                                acorr_norm = acorr/numspikes_ac;
                                
                                % Calculate first moment of autocorr function
                                mean_autocorr = 0;
                                for vv = 1:length(acorr_norm)
                                    mean_autocorr =  mean_autocorr + acorr_norm(vv) * bincenters_acorr(vv);
                                end
                                 % convert to milliseconds
                                mean_autocorr = mean_autocorr * 1000;
                                
                                if 0 %all([an day tet cellnum] == [2     9    13     1])
                                    keyboard
                                    figure
                                    plot(bincenters,dataentry.CCG{pp}(1:end-1))
                                end
                                
                                %(old code) mean_autocorr = 1000 * (dataentry.edges(a + lookup(0.5,cumsum(acorr_norm)) ) - binsize);  % first moment
                                if mean_autocorr > autocorrtime*1000  % should be impossible
                                    keyboard
                                    [an day tet cellnum]
                                    figure
                                    plot(acorr_norm)
                                end
                                if ~isnumeric(mean_autocorr)
                                    keyboard
                                end
                                break
                            end
                        end
                    end
                end
            end
            if isnan(mean_autocorr)
                disp(sprintf('%s %d %d %d %d has no autocorr data (**waiting on spikecorr to return)',...
                    animalinfo{3}(1:3),day,ep_max,tet,cellnum))
            else
                unitdata(ii,[4]) = mean_autocorr;
            end
        end
    end
end

% plots all unitdata
if plot_all
    if 1
        figure
        scatter3(unitdata(:,4),unitdata(:,3),unitdata(:,2),50,'k','o'); hold on
        xlabel('mean of ac');
        ylabel('spikewidth');
        zlabel('Fr');
        xlim([0 40])
        ylim([0 25])
        zlim([-30 60])
        set(gca,'xdir','normal')
        set(gca,'ydir','reverse')
        set(gca,'zdir','normal')
    end
    if 0
        figure
        scatter(unitdata(:,2),unitdata(:,3),30,'k','.'); hold on
        xlabel('Fr');
        ylabel('spikewidth');
        set(gca,'xdir','normal')
        set(gca,'ydir','normal')
    end    
    if 0
        figure
        scatter(unitdata(:,2),unitdata(:,4),30,'k','.'); hold on
        xlabel('Fr');
        ylabel('mean of ac');
        set(gca,'xdir','normal')
        set(gca,'ydir','normal')
    end        
    if 0
        figure
        scatter(unitdata(:,3),unitdata(:,4),30,'k','.'); hold on
        xlabel('spikewidth');
        ylabel('mean of ac');
        set(gca,'xdir','normal')
        set(gca,'ydir','normal')
    end        
end

% plot adtc_inter
if plot_adtc_inter
    
    npinds = ismember(adtc,adtc_inter,'rows');
    pinds = ~ismember(adtc,adtc_inter,'rows');
    
    if ~isempty(anim_toplot)
        aninds = adtc(:,1) == anim_toplot ;
        % filter units
        interinds = interinds & aninds;
        pinds = pinds & aninds;
        
        disp(sprintf('** plotting only animal #%d',anim_toplot))
    end
    
    if 1
        figure
        scatter3(unitdata(interinds,4),unitdata(interinds,3),unitdata(interinds,2),450,'k','.'); hold on
        scatter3(unitdata(pinds,4),unitdata(pinds,3),unitdata(pinds,2),200,[.1 1 .1],'+');
        xlabel('mean of ac');
        ylabel('spikewidth');
        zlabel('Fr');
        xlim([0 40])
        ylim([0 25])
        zlim([-30 60])
        set(gca,'xdir','normal')
        set(gca,'ydir','reverse')
        set(gca,'zdir','normal')
    end
    if 1
        figure
        scatter(unitdata(interinds,2),unitdata(interinds,3),40,'k','.'); hold on
        scatter(unitdata(pinds,2),unitdata(pinds,3),40,[.1 1 .1],'+');
        xlabel('Fr');
        ylabel('spikewidth');
        set(gca,'xdir','normal')
        set(gca,'ydir','normal')
    end    
    if 1
        figure
        scatter(unitdata(interinds,2),unitdata(interinds,4),40,'k','.'); hold on
        scatter(unitdata(pinds,2),unitdata(pinds,4),40,[.1 1 .1],'+');
        xlabel('Fr');
        ylabel('mean of ac');
        set(gca,'xdir','normal')
        set(gca,'ydir','normal')
    end        
    if 1
        figure
        scatter(unitdata(interinds,3),unitdata(interinds,4),40,'k','.'); hold on
        scatter(unitdata(pinds,3),unitdata(pinds,4),40,[.5 1 .5],'+');
        xlabel('spikewidth');
        ylabel('mean of ac');
        set(gca,'xdir','normal')
        set(gca,'ydir','normal')
    end     
    
end


