function out = dfakk_getspikecorr(index,excludeperiods,spikes, varargin)

day = index(1,1);
ep = index(1,2);

% use with kk_multicellanal iterator
%function out = dfakk_getspikecorr(index,excludetimes, spikes, varargin)
% Options:
%   'bin', n 	binsize in sec. Default 0.002 (2 ms)
%   'tmax', n	maximum time for cross correlation. Default 1 sec.
%   'edgespikes', 0 or 1  
%		  0 will enforce the exclude periods and will thus
%   	    	    exclude all spikes outside of the exclude windows. (default)
%		  1 will include all spikes where at least one of the spikes is
%		    not excluded 

% shift default
shiftsize = 0.1 ;  % in sec  , default is 100 ms
% jitter default
maxtimejit = [];
jittersize = [];  % in ms, default is nan to toggle off
numperms = [];

for option = 1:2:length(varargin)-1   
    if isstr(varargin{option})       
        switch(varargin{option})
            case 'edges'
                edges = varargin{option+1};
            case 'shiftsize'
                shiftsize = varargin{option+1};
            case 'jitter_params'
                maxtimejit = varargin{option+1}(1);
                jittersize = varargin{option+1}(2);
                numperms = varargin{option+1}(3);
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end        
    else
        error('Options must be strings, followed by the variable');
    end
end

% Initialize output
out.dayep = [day ep];
out.index = index;
out.edges = edges;
out.pairmatrix = [];
out.numrefspikes = [];
out.CCG = {};
out.shiftsize = shiftsize;
out.CCG_shift = {};
out.jitter_params = [maxtimejit jittersize numperms];
out.CCG_jitter = {};


% Determine pairs
numunits = size(index,1);
pairlist = [];
pairmatrix = [];
if numunits > 0  % 1
    pairlist = combnk([1:numunits],2);                    % combinations
    pairlist = [pairlist ; (1:numunits)' (1:numunits)'];  % and self-pairs, for autocorr
    pairmatrix = nan(2,4,size(pairlist,1));
    for jj = 1:size(pairlist,1)
        cell1 = pairlist(jj,1);
        cell2 = pairlist(jj,2);
        pairmatrix(:,:,jj) = [index(cell1,:) ; index(cell2,:)];
    end
else
    return
end
out.pairmatrix = pairmatrix;

% Convert excludeperiods to includeperiods
epstart = spikes{day}{ep}{index(1,3)}{index(1,4)}.timerange(1)/10000 ;
epend = spikes{day}{ep}{index(1,3)}{index(1,4)}.timerange(2)/10000 ;
timevec = epstart:0.001:epend ;
excludevec = list2vec(excludeperiods,timevec);
includeperiods = vec2list(~excludevec,timevec);

% Iterate through each pair
numpairs = size(pairmatrix,3);
        % initialize
    out.numrefspikes = nan(1,numpairs); 
    out.CCG = cell(1,numpairs); 
    out.CCG_shift = cell(1,numpairs); 
    out.CCG_jitter = cell(1,numpairs); 

disp(sprintf('Calculating spikecorr: %d %d, %d pairs',day,ep,numpairs))

for pp = 1:numpairs
    
    adtc1 = pairmatrix(1,:,pp);   % 1st row: reference cell
    adtc2 = pairmatrix(2,:,pp);   % 2nd row: 'signal' cell
    data1 = spikes{adtc1(1)}{adtc1(2)}{adtc1(3)}{adtc1(4)}.data;
    data2 = spikes{adtc2(1)}{adtc2(2)}{adtc2(3)}{adtc2(4)}.data;
    
    % Apply the exclude rule
    s1inc = [];
    s2inc = [];
    if ~isempty(data1)
        s1inc = data1( ~isExcluded(data1(:,1), excludeperiods) , 1 ) ;
        out.numrefspikes(pp) = length(s1inc) ;   % store # of included spikes from the first cell (can normalize ccg later)
    else
        s1inc = [];
    end
    if ~isempty(data2)
        s2inc = data2( ~isExcluded(data2(:,1), excludeperiods) , 1 ) ;
    else
        s2inc = [];
    end
    
    % Iterate through each include period and calculate the cross-correlogram
    CCG = [];
    CCG_shift = [];
    for rr = 1:size(includeperiods,1)

            s1per = s1inc(logical(isExcluded(s1inc,includeperiods(rr,:))));
            s2per = s2inc(logical(isExcluded(s2inc,includeperiods(rr,:))));
            
            % shift cell2 spikes
            s2per_shift = s2per + shiftsize;  % shifting
                removeind = (s2per_shift > includeperiods(rr,2));  % if extend past include period, remove
                s2per_shift(removeind) = [];
            
            ccg = kkxcorr(s1per, s2per, edges);
            ccg_shift = kkxcorr(s1per, s2per_shift, edges);
            
            if isempty(CCG)
                CCG = ccg;
            elseif ~isempty(ccg)
                CCG = CCG + ccg;
            end

            if isempty(CCG_shift)
                CCG_shift = ccg_shift;
            elseif ~isempty(ccg_shift)
                CCG_shift = CCG_shift + ccg_shift;
            end

    end
    
    % Install in output
    out.CCG{pp} = CCG;
    out.CCG_shift{pp} = CCG_shift;    
   
end

% jitter resampling procedure %%%%%%%%%%%

if ~isempty(jittersize)

disp(sprintf('jitter resample: %s',datestr(now)))

binsize = edges(2)-edges(1);   % use same bin size as of the above xcorr
edges_jit = (-maxtimejit-binsize/2):binsize:(maxtimejit+binsize/2);

for pp = 1:numpairs
    
    adtc1 = pairmatrix(1,:,pp);   % 1st row: reference cell
    adtc2 = pairmatrix(2,:,pp);   % 2nd row: 'signal' cell
    data1 = spikes{adtc1(1)}{adtc1(2)}{adtc1(3)}{adtc1(4)}.data;
    data2 = spikes{adtc2(1)}{adtc2(2)}{adtc2(3)}{adtc2(4)}.data;
    
    % Apply the exclude rule
    s1inc = [];
    s2inc = [];
    if ~isempty(data1)
        s1inc = data1( ~isExcluded(data1(:,1), excludeperiods) , 1 ) ;
        out.numrefspikes(pp) = length(s1inc) ;   % store # of included spikes from the first cell (can normalize ccg later)
    else
        s1inc = [];
    end
    if ~isempty(data2)
        s2inc = data2( ~isExcluded(data2(:,1), excludeperiods) , 1 ) ;
    else
        s2inc = [];
    end
    
    % Iterate through each include period and calculate the cross-correlogram
    CCG_jit = zeros(numperms,length(edges_jit));
    for rr = 1:size(includeperiods,1)

            s1per = s1inc(logical(isExcluded(s1inc,includeperiods(rr,:))));
            s2per = s2inc(logical(isExcluded(s2inc,includeperiods(rr,:))));

            % jitter cell1 (ref)'s spikes  -- notice that if it falls outside of includeperiod, we're still keeping it
            s1per_jit = s1per + (jittersize/1000) * rand(size(s1per));
            % calc xcorr
            ccg_jit = kkxcorr(s1per_jit, s2per, edges_jit);
            % add to permutation's CCG
            if ~isempty(ccg_jit)
                CCG_jit(pp,:) = CCG_jit(pp,:) + ccg_jit;
            end

    end
   
    % Install in pair's output
    out.CCG_jitter{pp} = CCG_jit;
   
end


end

    
end
