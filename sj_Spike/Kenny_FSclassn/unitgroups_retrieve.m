% Returns adtc of cells of interest

datadir = '/opt/data13/kkay/Unitgroups/';

animals_torun = {'Government','Egypt','Chapati','Dave','Higgs','Frank','Bond','Corriander'}; 

if 1
    cellfilter = '';
    customname = 'All_W_active';
    wtrack_filter = 1;   % set == 1 to filter for units clustered on W-track
        minspikes_w = 100;   % minimum # of spikes in a W-track epoch
end
if 0
    cellfilter = 'isequal($type,''inter'')';
    customname = 'interneurons';
    wtrack_filter = 1;   % set == 1 to filter for units clustered on W-track
        minspikes_w = 100;   % minimum # of spikes in a W-track epoch  
end
if 0
    cellfilter = '(isequal($area,''DG'')) && (isequal($type,''principal''))';
    customname = 'DGprincipal';
    wtrack_filter = 1;   % set == 1 to filter for units clustered on W-track
        minspikes_w = 20;   % minimum # of spikes in a W-track epoch  
end   




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
adtc = [];
for aa = 1:length(animals_torun)
    
    animals_order = {'Government','Egypt','Chapati','Dave','Higgs','Frank','Bond','Corriander'};
    animalname = animals_torun{aa};
        animalinfo = animaldef(animalname);
    animnum = find(strcmp(animalname,animals_order));
    
    cellinfo = loaddatastruct(animalinfo{2},animalinfo{3},'cellinfo');
    task = loaddatastruct(animalinfo{2},animalinfo{3},'task');
    
    detc_list = evaluatefilter(cellinfo,cellfilter);
    dtc_list = unique( detc_list(:,[1 3 4]),'rows' );
    
    
    % (optional)
    if wtrack_filter
        
        for cc = size(dtc_list,1):-1:1
            
            day = dtc_list(cc,1);
            tet = dtc_list(cc,2);
            cellnum = dtc_list(cc,3);
            
            passflag = 0;
            
            %if all([1     2     2     1] == [aa day tet cellnum])
            %    keyboard
            %end
            
            for ep = 1:length(cellinfo{day})
                % disregard non-W track epochs
                if ~isempty(task{day}{ep})
                    if isfield(task{day}{ep},'environment')
                        if ~any(strcmp(task{day}{ep}.environment,{'WTrackA' 'WTrackB' 'TrackA' 'TrackB'}))
                            continue
                        end
                    else
                        continue
                    end
                else
                    continue
                end
                if ~isempty(cellinfo{day}{ep})
                    if tet <= length(cellinfo{day}{ep}) && ~isempty(cellinfo{day}{ep}{tet})
                        if cellnum <= length(cellinfo{day}{ep}{tet}) && ~isempty(cellinfo{day}{ep}{tet}{cellnum})
                            numspikes_ep = cellinfo{day}{ep}{tet}{cellnum}.numspikes;
                            if numspikes_ep >= minspikes_w
                                passflag = 1;
                            end
                        end
                    end
                end
                if passflag
                    break
                end
            end
            % remove cell if does not meet criteria
            if ~passflag
                dtc_list(cc,:) = [];
                disp(sprintf('%s %d %d %d removed for not being W-track active',animalinfo{3},day,tet,cellnum))
            end
        end
        
    end
    
    
    numunits = size(dtc_list,1);
    adtc = [adtc   ;   animnum * ones(numunits,1)    dtc_list];  % append
    
    
end

eval([customname ' = adtc']);

save(sprintf('%sUnitgroup_%s',datadir,customname),customname,'-v7.3')