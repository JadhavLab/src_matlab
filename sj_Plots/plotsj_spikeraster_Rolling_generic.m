
% SJ - 12/29/2012. From plotsj_rip_spikeraster_LongEEG
% Instead of aligning to an event and plotting spike raster + EEG +
% Position, do it in a long rolling window for he given epoch

%function [pret] = plotsj_rip_spikeraster(prefix, day, epoch, saveg1)
% Shantanu- Dec 2012
% Win - Adding position or velocity to graph to see trajectory clearly

% Adapted from sj_ripegs1 and sj_plotrajdatafind3
% Plot raster of spikes during ripple using given cells. Not calling in function command right now.
% Can also plot LFP and posn. For these, esp. posn, check sj_plottrajdatafind3
% Shantanu 08Jun2012

% if nargin<1,
%     keyboard
%     error('Please enter Expt Prefix and Day No!');
% end
% if nargin<2,
%     keyboard
%     error('Please enter Day No!');
% end
% if nargin<3,
%     epoch=2; %% Epoch - 2 or 4 for runs
% end
% if nargin<4
%     saveg1=0; % Save summary figure
% end

clear;
prefix='HPa';
day=1; epoch=4;

%prefix='HPb';
%day=1; epoch=2;

doposn = 1;
if mod(epoch,2)==1, % If sleep session, dont do Position
    doposn=0;
end

win = 60; % 60 sec window
overlap = 10;

lowsp_thrs = 4; %cm/sec
highsp_thrs = lowsp_thrs;
dospeed = 1;

%sd=3; %% SD threshold for ripples
% Pret and Post from start of ripple? Also plot start-middle and end of ripple
%pret=30000;
%postt=30000;


switch prefix
    case 'HPa'
        directoryname = '/data25/sjadhav/HPExpt/HPa_direct';
        dire = '/data25/sjadhav/HPExpt/HPa';
        animdirect = directoryname;
        dire = '/data25/sjadhav/HPExpt/HPa';
        riptetlist = [1,4,5,6,7,8,9,11,12,14];
        maineegtet = 1; maineegidx=1; % CA1 tet
        peegtet = 16; % PFCtet
        
        %riptetlist=[1,4,5,6];
        %eegtets = riptetlist;
        %maineegtet = 1;
        %maineegidx=1;
        % %Also get a PFC eeg
        %peegtet = 15;
        %eegtets = [eegtets, peegtet];
        %peegidx=length(eegtets);
        
    case 'HPb'
        directoryname = '/data25/sjadhav/HPExpt/HPb_direct';
        riptetlist=[1,4,5,6];
        animdirect = directoryname;
        dire = '/data25/sjadhav/HPExpt/HPb';
        riptetlist = [1,3,4,5,6,16,17,18,20];
        maineegtet = 1; maineegidx=1; % CA1 tet
        peegtet = 9; % PFCtet
end

currdir = pwd;
if (directoryname(end) == '/')
    directoryname = directoryname(1:end-1);
end
if (dire(end) == '/')
    dire = dire(1:end-1);
end

if (day < 10)
    daystring = ['0',num2str(day)];
else
    daystring = num2str(day);
end

animdirect = directoryname;

% Get time ranges from times file - No need
%----------------------------------
cd(dire);
dirlist = dir('*');
for i=2:length(dirlist)
    currname = dirlist(i).name;
    if strcmp(currname(1:2),daystring)
        daydir = currname;
        break;
    end
end
timesfile = sprintf('%s/%s/times.mat',dire,daydir);
load(timesfile);
nranges=1;
range1=ranges(epoch+1,:);

% --------------- Parameters ---------------


Fspos = 30; %Hz
respos = 1/30; % sec
Fseeg = 1500; %Hz
reseeg = 1/1500; % sec
Fsspikes = 10000; %Hz
resspikes = 1/10000; %sec


%% -----------------------------------------
% SET DATA
% -------------------------------------------
eegtets = riptetlist;
% Also get a PFC eeg
eegtets = [eegtets, peegtet];
peegidx= find(eegtets==peegtet);

% Get data files
% Spike data
%-----------
spikefile = sprintf('%s/%sspikes%02d.mat', animdirect, prefix, day);
load(spikefile);
tetinfofile = sprintf('%s/%stetinfo.mat', directoryname, prefix);
load(tetinfofile);
cellinfofile = sprintf('%s/%scellinfo.mat', directoryname, prefix);
load(cellinfofile);
ripfile = sprintf('%s/%sripples%02d.mat', directoryname, prefix, day);
load(ripfile);
posfile = sprintf('%s/%spos%02d.mat', animdirect, prefix, day);
load(posfile);
linposfile = sprintf('%s/%slinpos%02d.mat', animdirect, prefix, day);
load(linposfile);



% Pos - Linpos
% -------------
absvel = abs(pos{day}{epoch}.data(:,5)); % Can also use field 9
posn = pos{day}{epoch}.data(:,2:3); % Can also use fields 6:7
postime = pos{day}{epoch}.data(:,1); % same as linpostime. in secs
statematrix = linpos{day}{epoch}.statematrix;
linpostime = statematrix.time;
% Get track, wells and traj info
trajwells = linpos{day}{epoch}.trajwells;
wellCoord = linpos{day}{epoch}.wellSegmentInfo.wellCoord;


% Get cells
% ---------
% CA1 cells (black)
%filterString = '( ($meanrate > 0.1) && ~strcmp($tag, ''iCA1Int'') && ~strcmp($tag, ''CA1Int'') && (strcmp($tag2, ''CA1Pyr'') || strcmp($tag2, ''iCA1Pyr'')) ) ';
%filterString = '( (strcmp($tag2, ''CA1Pyr'') || strcmp($tag2, ''iCA1Pyr'')) && ($meanrate > 0.1) && ~strcmp($tag, ''iCA1Int'') && ~strcmp($tag, ''CA1Int'') ) ';
filterString = '( (strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7) ) ';

cellindices = evaluatefilter(cellinfo{day}{epoch}, filterString);
cellsi = [repmat([day epoch], size(cellindices,1),1 ), cellindices]; % day-epoch-tet-cell for CA1 cells
usecellsi = 1:size(cellsi,1);

% PFC cells - ripunmod (blue)
%filterString = 'strcmp($area, ''PFC'') && strcmp($ripmodtag, ''y'')';
filterString = 'strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag, ''n'')';
pcellindices = evaluatefilter(cellinfo{day}{epoch}, filterString);
cellsp = [repmat([day epoch], size(pcellindices,1),1 ), pcellindices] % day-epoch-tet-cell for CA1 cells
usecellsp = 1:size(cellsp,1);

% PFC cells - ripmod (red)
cellspr=[];
filterString = 'strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag, ''y'')';
prcellindices = evaluatefilter(cellinfo{day}{epoch}, filterString);
cellspr = [repmat([day epoch], size(prcellindices,1),1 ), prcellindices] % day-epoch-tet-cell for CA1 cells
usecellspr = 1:size(cellspr,1);


% SORT CA1 CELLS BY LINFIELDS As in plotsj_linearpositions... - HAVE TO CHOOSE TRAJ
% -------------------------------------------------

traj1=1; traj2=2; % Out Left and In Left

linfieldfile = sprintf('%s/%slinfields%02d.mat', animdirect, prefix, day);
load(linfieldfile);


for i=1:size(cellsi,1)
    try
        currlf = linfields{day}{epoch}{cellsi(i,3)}{cellsi(i,4)}; %day, epoch, tet, cell
    catch
        cellsi(i,3), cellsi(i,4),
        keyboard;
    end
    lf1 = currlf{traj1}(:,5); % Norm Occ Rate for traj 1.
    lf2 = currlf{traj2}(:,5);
    lfcomb = mean([lf1,lf2],2); % Combine In and Out. Dominant Trajectory will define position in order
    % Save trajs
    lin_traj1{i}=lf1;  lin_traj2{i}=lf2;  lin_trajcomb{i}=lfcomb;
    
    % Get peak positions
    [peak_traj1(i), pos_traj1(i)] = max(lf1);
    [peak_traj2(i), pos_traj2(i)] = max(lf2);
    [peak_trajcomb(i), pos_trajcomb(i)] = max(lfcomb);
end

% Get Sort order
% --------------
[~,sort1] = sort(pos_traj1); % By traj1 - eg. 3=Out Right or 1=Out Left
[~,sort2] = sort(pos_traj2); % By traj2 - eg. 4=In Right or 2=In Left
[~,sortcomb] = sort(pos_trajcomb); % By trajcomb - eg. Combine Out and In Right/ Left

% Return ordered list
cellsi_sorttraj1 = cellsi(sort1',:);
cellsi_sorttraj2 = cellsi(sort2',:);
cellsi_sorttrajcomb = cellsi(sortcomb',:);

% Done ordering CA1
% -----------------
% Replace cellsi by sorted order
cellsi = cellsi_sorttrajcomb




% Spike data
%-----------

for i=1:size(cellsi,1)
    eval(['spiketimei{',num2str(i),'}= spikes{cellsi(',num2str(i),',1)}{cellsi(',num2str(i),',2)}'...
        '{cellsi(',num2str(i),',3)}{cellsi(',num2str(i),',4)}.data(:,1);']);
    eval(['spikeposi{',num2str(i),'}= spikes{cellsi(',num2str(i),',1)}{cellsi(',num2str(i),',2)}'...
        '{cellsi(',num2str(i),',3)}{cellsi(',num2str(i),',4)}.data(:,2:3);']);
    eval(['spikeposidxi{',num2str(i),'}= spikes{cellsi(',num2str(i),',1)}{cellsi(',num2str(i),',2)}'...
        '{cellsi(',num2str(i),',3)}{cellsi(',num2str(i),',4)}.data(:,7);']);
end

for i=1:size(cellsp,1)
    eval(['spiketimep{',num2str(i),'}= spikes{cellsp(',num2str(i),',1)}{cellsp(',num2str(i),',2)}'...
        '{cellsp(',num2str(i),',3)}{cellsp(',num2str(i),',4)}.data(:,1);']);
    eval(['spikeposp{',num2str(i),'}= spikes{cellsp(',num2str(i),',1)}{cellsp(',num2str(i),',2)}'...
        '{cellsp(',num2str(i),',3)}{cellsp(',num2str(i),',4)}.data(:,2:3);']);
    eval(['spikeposidxp{',num2str(i),'}= spikes{cellsp(',num2str(i),',1)}{cellsp(',num2str(i),',2)}'...
        '{cellsp(',num2str(i),',3)}{cellsp(',num2str(i),',4)}.data(:,7);']);
end

if ~isempty(cellspr)
    for i=1:size(cellspr,1)
        i;
        eval(['spiketimepr{',num2str(i),'}= spikes{cellspr(',num2str(i),',1)}{cellspr(',num2str(i),',2)}'...
            '{cellspr(',num2str(i),',3)}{cellspr(',num2str(i),',4)}.data(:,1);']);
        eval(['spikepospr{',num2str(i),'}= spikes{cellspr(',num2str(i),',1)}{cellspr(',num2str(i),',2)}'...
            '{cellspr(',num2str(i),',3)}{cellspr(',num2str(i),',4)}.data(:,2:3);']);
        eval(['spikeposidxpr{',num2str(i),'}= spikes{cellspr(',num2str(i),',1)}{cellspr(',num2str(i),',2)}'...
            '{cellspr(',num2str(i),',3)}{cellspr(',num2str(i),',4)}.data(:,7);']);
    end
end


% Ripple times across tetrodes
% ---------------------------------
cellcountthresh = 3;

riptets = riptetlist;
riptimes = []; rip_starttime = []; triggers = [];
[riptimes] = getripples_direct([day, epoch], ripples, riptets,'minstd',3);
currriptet=riptets(1);
% SPIKE COUNT THRESHOLD - MOVE TO ANOTHER FILE?
%filterString = '( strcmp($tag, ''CA1Pyr'') || strcmp($tag, ''iCA1Pyr'') || strcmp($tag, ''CA1Run'') || strcmp($tag, ''CA1Pyrpr'') || strcmp($tag, ''CA1Pyrp'') || strcmp($tag, ''iCA1Run'') || strcmp($tag, ''iCA1Pyrpr'') || strcmp($tag, ''iCA1Pyrp'') )';
filterString = ' (   strcmp($tag2, ''CA1Pyr'') || strcmp($tag2, ''iCA1Pyr'')    )';
cellindices = evaluatefilter(cellinfo{day}{epoch}, filterString);
indices = [repmat([day epoch], size(cellindices,1),1 ), cellindices];
spikecounts = [];   celldata = [];
%go through each cell and calculate the binned spike counts
for cellcount = 1:size(indices,1)
    index = indices(cellcount,:);
    if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)}.data)
        spiketimes = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);
    else
        spiketimes = [];
    end
    spikebins = periodAssign(spiketimes, riptimes(:,[1 2]));
    if ~isempty(spiketimes)
        validspikes = find(spikebins);
        spiketimes = spiketimes(validspikes);
        spikebins = spikebins(validspikes);
    end
    
    if ~isempty(spiketimes)
        tmpcelldata = [spiketimes spikebins];
        tmpcelldata(:,3) = cellcount;
    else
        tmpcelldata = [0 0 cellcount];
    end
    celldata = [celldata; tmpcelldata];
    spikecount = zeros(1,size(riptimes,1));
    for i = 1:length(spikebins)
        spikecount(spikebins(i)) = spikecount(spikebins(i))+1;
    end
    
    spikecounts = [spikecounts; spikecount];
end
celldata = sortrows(celldata,1); %sort all spikes by time
cellcounts = sum((spikecounts > 0));
%Find all events with enough cells
eventindex = find(cellcounts >= cellcountthresh);
rip_ncells = cellcounts(eventindex);
riptimes_keep = riptimes(eventindex,:);
rip_starttime = 1000*riptimes_keep(:,1);  % in ms
rip_endtime = 1000*riptimes_keep(:,2);

% Find ripples separated by atleast a second. Already looking across tetrodes and have cellcountthresh
% ------------------------------------------------------------------------------------------------------------------
iri = diff(rip_starttime);
keepidx = [1;find(iri>=1000)+1];
rip_starttime = rip_starttime(keepidx);
rip_endtime = rip_endtime(keepidx);

% Implement speed criterion
% -------------------------
pidx = lookup(rip_starttime,postime*1000);
speed_atrip = absvel(pidx);
lowsp_idx = find(speed_atrip <= lowsp_thrs);
highsp_idx = find(speed_atrip > highsp_thrs);
rip_starttime = rip_starttime(lowsp_idx);
rip_endtime = rip_endtime(lowsp_idx);

% Sort riptimes - should already be in order
% ------------------------
[rip_starttime,sortidx] = sort(rip_starttime);
triggers = rip_starttime; triggers_end = rip_endtime(sortidx);
% SKIPPING Inter-Rip-Interval
%     iri = diff(triggers);
%     keepidx = [1;find(iri>=1000)+1];
%     triggers = triggers(keepidx); triggers_end = triggers_end(keepidx);
pt = triggers; pt_sec = pt./1000;
pt_end = triggers_end; pt_end = pt./1000;

nrip = length(pt),
maxcells = max(rip_ncells)






% EEg and Ripple cont data
% ------------------------
eegpt = pt./1000; % in secs
tets=[riptetlist,peegtet];
for te=1:length(tets)
    
    currtet=tets(te);
    cnteeg=0; cntrip=0;
    
    % Load EEG and ripple LFP file
    %-------------------------
    EEGfile = sprintf('%s/EEG/%seeggnd%02d-%01d-%02d.mat', directoryname, prefix, day,epoch,currtet);
    load(EEGfile);
    eeg=eeggnd;
    e = eeg{day}{epoch}{currtet};
    if te==1
        teeg = geteegtimes(e);
        eind = lookup(pt, teeg);
        e.samprate=round(e.samprate);
        eegstart = eeg{day}{epoch}{te}.starttime; % in secs - Epoch start
        eegend = teeg(end); % in secs - Epoch end
    end
    ripfile = sprintf('%s/EEG/%sripple%02d-%01d-%02d.mat', directoryname, prefix, day,epoch,currtet);
    load(ripfile);
    ripamp = ripple{day}{epoch}{currtet}.data(:,1);
    ripenv = ripple{day}{epoch}{currtet}.data(:,3);
    
    eegalltet{te} = e.data;
    ripampalltet{te} = double(ripamp);
    
    %
    %     % Align EEG and Ripple Band to ripple time - NO Aligning In This Code
    %     %--------------------------------------------------------------------
    %
    %     nelements = length(1000-round((pret/1000)*e.samprate):1000+round((postt/1000)*e.samprate));
    %     for i=1:length(pt) % Need to Skip initial and final indices?
    %         i;
    %         cnteeg=cnteeg+1;
    %         currriptime = pt(i); currripsize = ripsize(i);
    %         currind = eind(i);
    %         if ( (currind-round((pret/1000)*e.samprate) <=0) || (currind+round((postt/1000)*e.samprate)>length(e.data)) )
    %             e_stim{te}(cnteeg,:)=0*(1:nelements);
    %             ripamp_stim{te}(cnteeg,:)=0*(1:nelements);
    %             ripenv_stim{te}(cnteeg,:)=0*(1:nelements);
    %         else
    %             e_stim{te}(cnteeg,:)=e.data(currind-round((pret/1000)*e.samprate):currind+round((postt/1000)*e.samprate));
    %             ripamp_stim{te}(cnteeg,:)=double(ripamp(currind-round((pret/1000)*e.samprate):currind+round((postt/1000)*e.samprate)));
    %             ripenv_stim{te}(cnteeg,:)=double(ripenv(currind-round((pret/1000)*e.samprate):currind+round((postt/1000)*e.samprate)));
    %             ripsize_stim(cnteeg,te) = currripsize;
    %         end
    %     end
    
end % end tets


% Position Data
% --------------
if doposn==1
    statematrix = linpos{day}{epoch}.statematrix;
    linpostime = statematrix.time;
    % Get track, wells and traj info
    trajwells = linpos{day}{epoch}.trajwells;
    wellCoord = linpos{day}{epoch}.wellSegmentInfo.wellCoord;
end


absvel = abs(pos{day}{epoch}.data(:,5)); % Can also use field 9
posn = pos{day}{epoch}.data(:,2:3); % Can also use fields 6:7
postime = pos{day}{epoch}.data(:,1); % same as linpostime




% --------------------------------------------------------------------------------------------------
% Plot example single-trial LFPs aligned to ripples. This is from sj_ripegs1. Plots LFP on all tets
% Here, use only main eggtet and plot EEG and Ripple. THen plot spike raster of all cells below
% -------------------------------------------------------------------------------------------------


% ------------------------------
% Figure and Font Sizes
forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end
clr = {'b','m','g','y','c','k','r','b','g','y','b','m','g','y','c','k','r','b','g','y','b','m','g','y','c','k','r','b','g','y'};

clr1='k';
clr2='r';


datadir = '/data25/sjadhav/HPExpt/HPa_direct/ProcessedData/';
figdir = '/data25/sjadhav/HPExpt/HPa_direct/Figures/';
% ---------------------------------------

%nplots=length(tets); Use only main eegtet

%taxis = [1:size(ripenv_stim{1},2)]*1000/e.samprate;
%taxis = taxis-pret;
%lineat=200;
%for i=1:size(e_stim{1},1)
winst = eegstart, winst = 6265,
winend = winst + win; % secs
while winend <= eegend
    
    
    % For Manual
    %winst =7660; winend = 7720;
    
    % Only do if atleast one ripple is bigger than threshold sd
    %if max(ripsize_stim(i,:)) >= sd
    figure(i); hold on;
    %redimscreen_halfvert;
    redimscreen;
    
    % Get eeg times axis
    eind1 = lookup(winst, teeg);
    eind2 = lookup(winend, teeg);
    taxis = teeg(eind1:eind2);
    taxis = taxis - winst;
    
    winst_ms = winst*1000;
    winend_ms = winend*1000;
    
    %currriptime = pt(i)*1000;  % In ms Or use eeg time - currind = eind(i);
    %sttime = currriptime - pret; sttime_sec = sttime./1000;
    %endtime = currriptime + postt; endtime_sec = endtime./1000;
    
    % Look up position index at this time in stateatrix
    % posidx_rip = lookup(pt(i),statematrix.time); % All in secs
    if doposn==1
        posidx_st = lookup(winst,statematrix.time);
        posidx_end = lookup(winend,statematrix.time);
        currpostime = statematrix.time(posidx_st:posidx_end);
        currpostime = currpostime - currpostime(1); % Make time axis start at 0
    end
    
    % Find ripples within this window
    ripidx = find(pt_sec>=winst & pt_sec<=winend);
    riptimes = pt_sec(ripidx); % In secs
    riptimes_win = riptimes - winst;
    
    baseline = 0;
    % First Speed on bottom
    % ----------------------
    
    % Redo Time axis for speed
    currspeedtime = statematrix.time(posidx_st:posidx_end);
    currspeedtime = currspeedtime - currspeedtime(1);
    currspeed = absvel(posidx_st:posidx_end);
    
    speedscale = max(currspeed)-min(currspeed);
    plotscale = 5; baseline = baseline+plotscale+1;
    plotcurrspeed = currspeed.*(plotscale/speedscale); thrs = 4.*(plotscale/speedscale);
    
    plot(currspeedtime, plotcurrspeed,'k-','Linewidth',2);
    
    %Divider line, and also plot threshold of lowspthrs
    xpts = 0:1:win; ypts = thrs*ones(size(xpts));
    plot(xpts , ypts, 'r-','Linewidth',2);
    ypts = baseline*ones(size(xpts));
    plot(xpts , ypts, 'k--','Linewidth',2);
    
    % Update baseline to give a little vertical gap
    baseline = baseline+1;
    
    
    
    % First PFC Spikes on bottom. Each tick has space of height2, and using 1.8 of it for line
    % -----------------------
    % First PFC spikes, ripunmod, then ripmod
    % ----------
    cnt = 0;
    for c=usecellsp
        eval(['currspkt = spiketimep{',num2str(c),'};']);
        currspkt = currspkt;
        currspkt = currspkt(find(currspkt>=winst & currspkt<=winend ));
        
        % If spikes, subtract from subtract from start time and bin
        if ~isempty(currspkt)
            currspkt = currspkt - winst;
            %raster = starttime:0.001:endtime;
        end
        
        cnt=cnt+1;
        figure(i); hold on; %subplot(nplots,1,cnt+5); hold on;
        if ~isempty(currspkt)
            if size(currspkt,2)~=1, currspkt=currspkt'; end
            % Use plotraster or spikeTrain
            plotraster(currspkt,(baseline+2*(cnt-1))*ones(size(currspkt)),1.8,[],'Color','b','LineWidth',2);
            %plotraster(currspkt,(baseline+2*(cnt-1))*ones(size(currspkt)),1.8,[],'Color',[clr1],'LineWidth',2);
            %spikeTrain(currspkt,(baseline+(c-1))*ones(size(currspkt)),0.8,[],'Color',[clr{cnt}]);
        else
            plot(0,0,'k.');
        end
        %set(gca,'XLim',[0 endtime-starttime]);
    end
    
    if ~isempty(cellspr)
        for c=usecellspr
            eval(['currspkt = spiketimepr{',num2str(c),'};']);
            currspkt = currspkt;
            currspkt = currspkt(find(currspkt>=winst & currspkt<=winend ));
            
            % If spikes, subtract from subtract from start time and bin
            if ~isempty(currspkt)
                currspkt = currspkt - winst;
                %raster = starttime:0.001:endtime;
            end
            
            cnt=cnt+1;
            figure(i); hold on; %subplot(nplots,1,cnt+5); hold on;
            if ~isempty(currspkt)
                if size(currspkt,2)~=1, currspkt=currspkt'; end
                % Use plotraster or spikeTrain
                plotraster(currspkt,(baseline+2*(cnt-1))*ones(size(currspkt)),1.8,[],'Color','r','LineWidth',2);
                %plotraster(currspkt,(baseline+2*(cnt-1))*ones(size(currspkt)),1.8,[],'Color',[clr1],'LineWidth',2);
                %spikeTrain(currspkt,(baseline+(c-1))*ones(size(currspkt)),0.8,[],'Color',[clr{cnt}]);
            else
                plot(0,0,'k.');
            end
            %set(gca,'XLim',[0 endtime-starttime]);
        end
    end
    
    baseline = baseline + (size(cellsp,1)+size(cellspr,1))*2;
    % Update baseline to give a little vertical gap
    baseline = baseline+1;
    
    % Then PFC EEG
    % ------------
    n = peegidx;
    eegtet = eegalltet{n};
    curreeg = eegtet(eind1:eind2);
    % Plot
    % ----
    eegscale = max(curreeg)-min(curreeg);
    downeeg = baseline; upeeg = downeeg+8;
    plotscale = 8;
    curreeg = downeeg + (plotscale/2) + curreeg.*(plotscale/eegscale);
    plot(taxis,curreeg,'r-','LineWidth',2);
    
    % Update baseline
    baseline = baseline + 9;
    %Divider line
    xpts = 0:1:win;
    ypts = baseline*ones(size(xpts));
    plot(xpts , ypts, 'k--','Linewidth',2);
    
    % Update baseline to give a little vertical gap
    baseline = baseline+0.5;
    
    
    % Now, CA1 spikes
    % ---------------
    
    cnt = 0;
    for c=usecellsi
        eval(['currspkt = spiketimei{',num2str(c),'};']);
        currspkt = currspkt;
        currspkt = currspkt(find(currspkt>=winst & currspkt<=winend ));
        
        % If spikes, subtract from subtract from start time and bin
        if ~isempty(currspkt)
            currspkt = currspkt - winst;
            %raster = starttime:0.001:endtime;
        end
        
        cnt=cnt+1;
        figure(i); hold on; %subplot(nplots,1,cnt+5); hold on;
        if ~isempty(currspkt)
            if size(currspkt,2)~=1, currspkt=currspkt'; end
            % Use plotraster or spikeTrain
            plotraster(currspkt,(baseline+2*(cnt-1))*ones(size(currspkt)),1.8,[],'Color','k','LineWidth',2);
            %plotraster(currspkt,(baseline+2*(cnt-1))*ones(size(currspkt)),1.8,[],'Color',[clr{cnt}],'LineWidth',2);
            %plotraster(currspkt,(baseline+2*(cnt-1))*ones(size(currspkt)),1.8,[],'Color',[clr1],'LineWidth',2);
            %spikeTrain(currspkt,(baseline+(c-1))*ones(size(currspkt)),0.8,[],'Color',[clr{cnt}]);
        else
            plot(0,0,'k.');
        end
        %set(gca,'XLim',[0 endtime-starttime]);
    end
    
    Hp_baseline = baseline; % Save for linearized position
    % Update baseline
    baseline = baseline + size(cellsi,1)*2;
    up = baseline;
    
    if doposn==1
        % Linearized position (from well 1 and well 3) on top of Hp spikes
        plotscale = up-Hp_baseline;
        lindist1 = statematrix.linearDistanceToWells(posidx_st:posidx_end,1);
        lindist1 = lindist1-min(lindist1);
        distscale = max(lindist1)-min(lindist1);
        lindist1 = Hp_baseline + lindist1.*(plotscale/distscale);
        plot(currpostime,lindist1,'k-','LineWidth',2);
        lindist2 = statematrix.linearDistanceToWells(posidx_st:posidx_end,2);
        lindist2 = lindist2-min(lindist2);
        distscale = max(lindist2)-min(lindist2);
        lindist2 = Hp_baseline + lindist2.*(plotscale/distscale);
        plot(currpostime,lindist2,'c-','LineWidth',2);
        lindist3 = statematrix.linearDistanceToWells(posidx_st:posidx_end,3);
        lindist3 = lindist3-min(lindist3);
        distscale = max(lindist3)-min(lindist3);
        lindist3 = Hp_baseline + lindist3.*(plotscale/distscale);
        %plot(currpostime,lindist3,'b-','LineWidth',2);
    end
    
    % Update baseline to give a little vertical gap
    baseline = baseline+0.5;
    
    
    % EEG On main tet
    % --------------
    n = maineegidx;
    eegtet = eegalltet{n};
    curreeg = eegtet(eind1:eind2);
    % Plot
    % ----
    eegscale = max(curreeg)-min(curreeg);
    downeeg = baseline; upeeg = downeeg+8;
    plotscale = 8;
    curreeg = downeeg + (plotscale/2) + curreeg.*(plotscale/eegscale);
    plot(taxis,curreeg,'k-','LineWidth',1);
    
    % Update baseline
    baseline = baseline + 8.5;
    
    % EEG in ripple band
    % --------------------------------------
    n = maineegidx;
    riptet = ripampalltet{n};
    curreeg = riptet(eind1:eind2);
    % Plot
    % ----
    eegscale = max(curreeg)-min(curreeg);
    downeeg = baseline; upeeg = downeeg+8;
    plotscale = 8;
    curreeg = downeeg + (plotscale/2) + curreeg.*(plotscale/eegscale);
    plot(taxis,curreeg,'k-','LineWidth',1);
    
    %Update baseline
    baseline = baseline + 8;
    
    
    
    set(gca, 'YTick',[]);
    
    winsecs = [0:10:winend-winst]; %eg. 0:10:60
    secs = [winst:10:winend];
    secs = round(secs); %secs = roundn(secs,-1);
    msecs = [winst_ms:10000:winend_ms];
    set(gca,'XTick',winsecs,'XTickLabel',num2str(secs'));
    xlabel('Time (secs)','FontSize',18,'Fontweight','normal');
    title(['Win St Ttime: ' num2str(roundn(winst,-1))],'FontSize',18,'Fontweight','normal');
    %title(['SWR Time: ',num2str(roundn(pt(i),-1)), 'secs']);
    
    % Draw Lines
    ylim = get(gca,'YLim');
    ypts = ylim(1):ylim(2);
    for i=1:length(winsecs)
        xpts = winsecs(i)*ones(size(ypts));
        plot(xpts , ypts, 'k--','Linewidth',0.5);
    end
    
    % Mark Ripple Times in Current Window
    for s=1:length(riptimes_win)
        DIOt = riptimes_win(s);
        xaxis = DIOt:0.05:DIOt+0.3; % 300ms
        jbfill(xaxis,ylim(2)*ones(size(xaxis)),ylim(1)*ones(size(xaxis)),'m','m',1,0.2);
    end
    
    
   
    set(gca,'YLim',[0 upeeg]);
    set(gca,'XLim',[0 winend-winst]);
    
    keyboard; % Pause after each plot
    
    saveg=0;
    if saveg==1
        figfile = [figdir,prefix,'Day2Ep4RasterEEGegNo',num2str(i)];
        print('-dpdf', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
    
    close all
    
    % Update winst and winend
    winst = winst + win - overlap;
    winend = winst + win;
    
    
    %end % if ripsize > sd
end





if saveg1==1
    figfile = [figdir,'n',prefix,'_Day',num2str(day),'Ep',num2str(epoch),'Stim',num2str(i),'_RipEg1'];
    print('-dpdf', figfile);
    print('-djpeg', figfile);
    saveas(gcf,figfile,'fig');
end

% Manually plot
tet = 1; stim=75;
figure; hold on;
eegtet=e_stim{tet};
x=eegtet(stim,:); % eg 75 tet4
plot(taxis,x,'k-','LineWidth',2);
set(gca,'XLim',[-50 50]);
figfile = [figdir,prefix,'_Day',num2str(day),'Ep',num2str(epoch),'Tet',num2str(tet),'Stim',num2str(stim),'_lfpeg1'];
print('-dpdf', figfile);
print('-djpeg', figfile);
saveas(gcf,figfile,'fig');

figure; hold on;
riptet=ripamp_stim{tet};
y=riptet(stim,:); % eg 75 tet4
plot(taxis,y,'k-','LineWidth',2);
set(gca,'XLim',[-50 50]);

figfile = [figdir,prefix,'_Day',num2str(day),'Ep',num2str(epoch),'Tet',num2str(tet),'Stim',num2str(stim),'_ripeg1'];
print('-dpdf', figfile);
print('-djpeg', figfile);
saveas(gcf,figfile,'fig');

% *************************************************************************
cd(datadir);
keyboard;

