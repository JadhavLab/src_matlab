function fout = plotBehavior(ind,excperiods,animprefix,pos,linpos,varargin)

%% Constants
CONST = struct( ...
	'trajlinewidth',  1.60 ...
	); 

TRAJ_PLOT_OPT = {'-','linewidth',1.5};
LEGEND_OPT = {'fontsize',8,'location','eastoutside'};
% ZONE_PLOT_OPT = {'-','linewidth',2};
SKELETON_PLOT_OPT = {'-','linewidth',0.5,'color',[0 0 0 0.25]};

%% Defaults and flags

% Defaults
if exist('arrowh.m','file')
    arrowdensity= 20; % percent increments along trajecectory start-to-end
end

po.randomsubsample = 0;
po.maxtraj_perplot = 0;
po.arrow = true;
po.trackbackground = 1;
filter = '';
titlePrepend = '';
mode = zeros(4,1);

savedir = '~/Data/Local/Figures/Behavior';
waitplots = false;

%% Parse inputs
for v = 1:2:numel(varargin)
    
    switch varargin{v}
        % -----------------------------------------------------------------
        % GENERAL MODE CONTROL, user can select any combination of behavior
        % plots. This list will expand overtime when I feel the need to
        % check more aspects of behavior.
        case 'mode'
            
            type = varargin{v+1};
            if ischar(type)
            switch type
                % See below this section for description of what these plot
                % modes are ...
                case 'filetrajmode',    mode(1)=true;
                case 'singletrajmode',  mode(2)=true;
                case 'multitrajmode',   mode(3)=true;
                case 'trajectorychecks',mode(4)=true;
            end
            elseif isnumeric(type)
                % You can also provide a binary vector specifying which
                % plot modes to use
                mode = logical(type);
            end            
        % -----------------------------------------------------------------   
        % TRAJFILE MODE CONTROLS
        % Trajectory mode pulls trajectories from a trajectory mat file
        % placed by the zone control time filter code. This allows one to
        % quickly examine time-filtered individual trajectories, and to
        % even try different filters fed into your original time filter.
        case 'filter'
            filter = varargin{v+1};
        % -----------------------------------------------------------------
        % ONETRAJ MODE CONTROLS
        % Regular mode pulls the positions directly from excluded times
        % -----------------------------------------------------------------
        % MUTLITRAJ MODE CONTROLS
        % -----------------------------------------------------------------
        % PLOT PROPERTY CONTROLS
        case 'arrow'
            % Arrow modifies your plots to include directional flow in
            % trajectories, so that we can see direction of movement
            po.arrow = varargin{v+1};
        case 'randomsubsample'
            % For instance, if there are 43 trajectories, and it is too
            % busy to see them all, this method will select a percentage of
            % them randomly to plot
            po.randomsubsample = varargin{v+1};
		case 'maxtraj_perplot' 
            % when randomsubsample is invoked, this determines the maximum
            % number of trajectories
			po.maxtraj_perplot = varargin{v+1};
        % -----------------------------------------------------------------
        % TRACK BACKGROUNDS
        case 'trackskeleton'
            if varargin{v+1}
                po.trackbackground = 2;
            end
        % -----------------------------------------------------------------    
        % ZONE INPUT
        case 'zoneinfo' % manual input of a zonefile, if you have one, else
                        % if you attempt to try filemode, it will look
                        % automatically for it.
		% -----------------------------------------------------------------	
		% OTHER
        case 'titlePrepend', titlePrepend = varargin{v+1};
		case 'waitplots'
            % Whether to wait after each plot is created ... for real-time
            % examination
			waitplots = varargin{v+1};
		case 'savedir'
            % Where to save plots
			savedir = varargin{v+1};  
        otherwise
            warning('plotBehavior2: unrecognized varargin input');
    end
end

if sum(mode) == 0 % if no plot modes selected
    if exist(fullfile(savedir, [animprefix 'zonetrack.mat']),'file')
        mode(1) = true; % default to filetrajectory if special mat file exists
    else
        mode(3) = true; % default to multitraj if it does not exist
    end
end


%% MAIN

% Obtain only pos,linpos for this epoch
pos = pos{ind(1)}{ind(2)}; linpos=linpos{ind(1)}{ind(2)};

% Take (t,x,y) tuple from pos structure
x_sm = colof('x-sm',pos); y_sm=colof('y-sm',pos);
if isempty(x_sm) || x_sm==0
    warning('Smoothed data not available');
    t = pos.data(:,1); x = pos.data(:,2); y = pos.data(:,3);
else
    t = pos.data(:,1); x = pos.data(:,x_sm); y = pos.data(:,y_sm);
end

modes = find(mode);

% Initialize figure and plot base data
for m = modes
    
    f=figure(100);
    set(f,'Units','normalized', ...
        'Position',[0 0 1 1]);
    switch po.trackbackground
        case 0
            clf;
            hold on;
        case 1
            hold off;
            plot(pos.data(:,2),pos.data(:,3),SKELETON_PLOT_OPT{:}); 
            hold on;
        case 2
            clf; hold off;
            segCoords = linpos.segmentInfo.segmentCoords;
            for c = 1:size(segCoords,1)
                plot(segCoords(c,[1 3]), segCoords(c,[2 4]),SKELETON_PLOT_OPT{:});
                hold on;
            end
        otherwise
            warning('Not a background type');
    end

    switch m
    % --------------------------------------------------------------------------
    % Main single-plot/trajectory sample plots
    % --------------------------------------------------------------------------	
    case 1 % File Trajectory Mode - Dependent on Zonetrack.mat
            % file mode is by far the most detailed mode, and plots trajectory
            % samples from the zonetrack files; hence, it's also the most
            % stringent, in that it requires a file exist with the additional
            % information it uses
            fileTrajMode(ind,t,x,y,po,filter);
            conditionalWait();
    % --------------------------------------------------------------------------
    % Non-file-dependent plots, these work for filter call, regardless of
    % whether a time filter created a zonetrack
    % --------------------------------------------------------------------------
    case 2 % Unitary Trajectrory Mode - Independent of Zonetrack.mat

            % plots all of the sample as a single trajectory, even if there are
            % discontinuities
            singleTrajMode(ind,excperiods,t,x,y,po);
            conditionalWait();
    case 3 % Multiple Trajectory Mode - Independent of Zonetrack.mat
            % assumes each discontinuity in time in a different trajectory, and
            % plots it as a separate entity
            multiTrajMode(ind,excperiods,t,x,y,po);
            conditionalWait();
    % --------------------------------------------------------------------------	
    % Sequences of plots
    % --------------------------------------------------------------------------
    case 4 % Trajectory Check Mode -- Dependent on Zonetrack.mat
        % This mode is used to visualize each trajectory snippet sample(s)
        % one by one, and cross-checked against the full corresponding
        % trajectory in trajinfo
        trajectoryCheck(ind,t,x,y,po,filter);
    case 5 % Time filter check mode... checks Zonetrack time filter output
        % This mode checks that the zonetrack filter is sensically labeling
        % the different trajectory sites.
        filterCheck(ind,t,x,y,po,filter);    
    end
    
end

fout.savedir    = savedir;
fout.modes      = modes;

% END OF FUNCTION
return;

%%
%-------------------------------------------------------------------------
%               HELPER FUNCTIONS -- The Different Plot Modes
%-------------------------------------------------------------------------

%% FilterTermCheck
%  Purpose: To check that that filter terms in a behavioral time filter are
%  working
    function filterCheck(ind,t,x,y,po,filter)
        
        cla;
        
        % load up trajinfo
		animdat		= animaldef(animprefix);
		trajinfo	= loaddatastruct(animdat{2},animdat{3},'trajinfo',ind(1));
		trajinfo	= trajinfo{ind(1)}{ind(2)};
		
		% If need to, acquire zonetrack information from timefilter output
        persistent filestring   % file string allows one to track whether 
                                % the current animal's zonetrack matches legend_text
                                % the previously held peristent zonetrack
        persistent zonetrack    % saves time keeping data as a persistent 
                                % variable, so it doesn't have to be 
                                % reloaded everytime the function is called
                                
        if isempty(filestring) || ~isequal(filestring,[animprefix 'zonetrack'])
        % load the timefilter data, if it exists or is if the zonetrack is
        % data for a different animal
            filestring = fullfile(savedir, [animprefix 'zonetrack']);
            zonetrack = load(filestring);
            zonetrack = zonetrack.out;
		end
		
		% Set this epochs zone track
        zonetrack_this = zonetrack{ind(1)}{ind(2)};
        
        
        % Plot all of the filter terms
        [variables, ~] = parsefilterstring(filter);
        
        for j = 1:numel(variables)
            plot(zonetrack_this.time, zonetrack_this.(variables{j}));
        end
        
        set(legend(variables),LEGEND_OPT{:});
        
        a=gca;
        a.YLim(2) = a.YLim(2)*1.4;
        highest= a.YLim(2);
        lowest = a.YLim(1);
        separatorY = linspace(lowest,highest,100);
        
        % Separate and annotate times via the official trajinfo
        for i = 1:size(trajinfo.trajtime,1)
            
            % Plot the separator
            separatorX = trajinfo.trajtime(i,2)*ones(1,100);
            plot(separatorX,separatorY,'k');
            
            % Compute raw locations for annotation
            x_loc(2) = 1/2*(trajinfo.trajtime(i,2) - trajinfo.trajtime(i,1)) ...
                + trajinfo.trajtime(i,1);
            x_loc(1) = 1/16*(trajinfo.trajtime(i,2) - trajinfo.trajtime(i,1)) ...
                + trajinfo.trajtime(i,1);
            y_loc(1) = 1.1; y_loc(2) = 1;
            % Normalize raw locations to the axis limits
            x_loc = (x_loc-a.XLim(1))/range(a.XLim);
            y_loc = (y_loc-a.YLim(1))/range(a.YLim);
            % Normalize axis-normalized to figure normalized
            x_loc = x_loc*a.Position(3) + a.Position(1);
            y_loc = y_loc*a.Position(4) + a.Position(2);
            % Lay annotation down
            message_handle= trajinfoMessage(a,trajinfo,i,x_loc,y_loc,0,1);
            message_handle.FontSize=4;
            
        end
        
        
        title_string =sprintf('Trajinfo-Zonetrack Cross-Check, day %d, ep %d',...
            ind(1),ind(2));
        
        conditionalWait();
        conditionalSave('plotBehavior:ZontrackValidation',title_string);
    end


%% trajectoryCheck(ind,t,x,y,po,filter)
%   Purpose: Checks each trajectory snippet against the full trajectory
%-------------------------------------------------------------------------
	function trajectoryCheck(ind,t,x,y,po,filter)
        
		dat_ax = gca;
		dat_ax.Position = [.3 .1 .6 .8];
        text_ax = axes('Position',[0 0 1 1],'Visible','off'); axes(dat_ax);
		% load up trajinfo
		animdat		= animaldef(animprefix);
		trajinfo	= loaddatastruct(animdat{2},animdat{3},'trajinfo',ind(1));
		trajinfo	= trajinfo{ind(1)}{ind(2)};
		
		% If need to, acquire zonetrack information from timefilter output
        persistent filestring   % file string allows one to track whether 
                                % the current animal's zonetrack matches legend_text
                                % the previously held peristent zonetrack
        persistent zonetrack    % saves time keeping data as a persistent 
                                % variable, so it doesn't have to be 
                                % reloaded everytime the function is called
                                
        if isempty(filestring) || ~isequal(filestring,[animprefix 'zonetrack'])
        % load the timefilter data, if it exists or is if the zonetrack is
        % data for a different animal
            filestring = fullfile(savedir, [animprefix 'zonetrack']);
            zonetrack = load(filestring);
            zonetrack = zonetrack.out;
		end
		
		% Set this epochs zone track
        zonetrack_this = zonetrack{ind(1)}{ind(2)};
		
		
		trajs = getTrajSet(po,1:max(zonetrack_this.trajnumber));
        for i = trajs
            
            % select the times of interest
            if isempty(filter)
                t_ofinterest = zonetrack_this.trajnumber == i;
            else
                % if the filter is not empty, then additionally contrain times
                t_ofinterest = filterTimes(filter,zonetrack_this,i);
            end
            
            % if the set of points is empty for this trajectory, because
            % this trajectory's points were completely filtered out, skip
            % it, and count ahead
			if sum(t_ofinterest) == 0; continue; end
			
			% plot the entire trajectory
			t_trajectory = t >= trajinfo.trajtime(i,1) & ...
				t <= trajinfo.trajtime(i,2);
			total = plot( dat_ax, x(t_trajectory), y(t_trajectory) , 'r' );
			total.LineWidth = CONST.trajlinewidth;
			% plot the sample
            samp = plot(dat_ax, x(t_ofinterest), y(t_ofinterest), 'b' );
			samp.LineWidth = CONST.trajlinewidth;
            
            % If directional flow plotting on, then provide arrows along the
            % trajectory to give a better send of how the animal is moving
            % through that space
            if po.arrow && ~isempty(samp)
                arrow_handles_samp=...
					plotAndCorrectArrows(samp,x,y,t_ofinterest);
				arrow_handles_total=...
					plotAndCorrectArrows(total,x,y,t_trajectory);
            end
            
            % If zone control existed for this file, then plot out the zone!
            if isfield(zonetrack_this,'zc')
                zc = zonetrack_this.zc;
                [zx, zy] = circ(zc.spatial_proximity,[zc.region_centroid(1), zc.region_centroid(2)],true);
                circle=fill(zx,zy,'k');
                circle.EdgeAlpha=0.50; circle.FaceAlpha=0.20;
                circle.EdgeColor='r'; circle.FaceColor='k';
                circle.LineWidth=2; circle.LineStyle=':';
                drawnow;
            end
            
            % For rendering as text into the figure, so we can look at what's
            % in trajinfo versus the actual trajectory
            message_handle = trajinfoMessage(text_ax,trajinfo,i,...
                0.025,0.6);
            
			% Label and save the plot
			title_string = sprintf(['TrajectoryCheck, day %d, ep %d, ' ...
				'traj %d'], ind(1),ind(2),i);
			title(title_string,'FontSize',24);
			xlabel('Distance-X (cm)');
			ylabel('Distance-Y (cm)');
            
			% If user requested pause after each plot
			conditionalWait();
			
			% Save, if requested
            conditionalSave('plotBeh-trajectoryCheck',title_string);
			
			% Clear the plot object for the next go
            delete(circle);
			delete(samp);
			delete(arrow_handles_samp);
			delete(total);
			delete(arrow_handles_total);
            delete(message_handle);
			
        end
		
	end

%% fileInTrajMode(ind,t,x,y,po,filter)
%   Purpose: Pulls trajectories from a call to trackZoneTrigger and plots
%   using the output structure
%-------------------------------------------------------------------------
    function fileTrajMode(ind,t,x,y,po,filter)
        %
        % Purpose: This contitutes a plotting mode where, instead of just
        % looking at the information given through the filter inputs, it
        % looks mainly at the information provided during the time filter
        % step ... it looks a file created at that time, and allows a much
        % fuller plotting of the behavior selected, including information
        % about the selection zone.
	
        
        % If need to, acquire zonetrack information from timefilter output
        persistent filestring   % file string allows one to track whether 
                                % the current animal's zonetrack matches legend_text
                                % the previously held peristent zonetrack
        persistent zonetrack    % saves time keeping data as a persistent 
                                % variable, so it doesn't have to be 
                                % reloaded everytime the function is called
                                
        if isempty(filestring) || ~isequal(filestring,[animprefix 'zonetrack'])
        % load the timefilter data, if it exists or is if the zonetrack is
        % data for a different animal
            filestring = fullfile(savedir, [animprefix 'zonetrack']);
            zonetrack = load(filestring);
            zonetrack = zonetrack.out;
        end

        % Set this epochs zone track
        zonetrack_this = zonetrack{ind(1)}{ind(2)};

        % Plot each trajectory pulled in this epoch
        p = get(gca,'Children');
        legend_text{1} = 'All Behavior';
        p = p(1);
		
		trajs = getTrajSet(po,1:max(zonetrack_this.trajnumber));
		
		plotcount = 1;
        for i = trajs
			
			plotcount = plotcount+1;
            
            % select the times of interest
            if isempty(filter)
                t_ofinterest = zonetrack_this.trajnumber == i;
            else
                % if the filter is not empty, then additionally contrain times
                t_ofinterest = filterTimes(filter,zonetrack_this,i);
            end
            
            
			if sum(t_ofinterest) == 0
                % if no times of interest, then put down an empty plot
                % object and empty string in the legend/plot arrays
                legend_text{plotcount} = '()';
                p(plotcount) = plot(NaN,NaN);
				continue;
			end
            p(plotcount)=plot( x(t_ofinterest), y(t_ofinterest) , TRAJ_PLOT_OPT{:});
			p(plotcount).LineWidth = CONST.trajlinewidth;
            
            % If directional flow plotting on, then provide arrows along the
            % trajectory to give a better send of how the animal is moving
            % through that space
            if po.arrow && ~isempty(p(plotcount))
                plotAndCorrectArrows(p(plotcount),x,y,t_ofinterest);
            end
            legend_text{plotcount} = num2str(i);

        end
        
        % If zone control existed for this file, then plot out the zone!
        if ~isempty(zonetrack_this.zc.spatial_proximity)
            zc = zonetrack_this.zc;
            [zx, zy] = circ(zc.spatial_proximity,[zc.region_centroid(1), zc.region_centroid(2)],true);
            circle=fill(zx,zy,'k');
			circle.EdgeAlpha=0.50; circle.FaceAlpha=0.20;
			circle.EdgeColor='r'; circle.FaceColor='k';
			circle.LineWidth=2; circle.LineStyle=':';
            p(end+1) = circle;
            legend_text{end+1} = 'Zone';
			drawnow;
        end

        % Label things
        title_string = sprintf('ZonePerTrajectory,day%d,ep%d',ind(1),ind(2));
        try 
            set(legend(p,legend_text),LEGEND_OPT{:});
        catch ME; 
            save(savedir,...
                ['legend' num2str(ceil(rand(1,3)*10)) 'error.mat'],'ME');...
        end;
        title(title_string);
        
        % Save, if requested
        conditionalSave('plotBeh-fileMode',title_string);
        
    end
%% singeTrajMode(ind,t,x,y)
%   Purpose: Plots only using EXCLUDETIMES input .. contiguous time regions
%   NOT ASSUMED to be different trajectories
%-------------------------------------------------------------------------
    function singleTrajMode(ind, excperiods, t, x, y)
        %
        % Purpose: 
        
        % Check that aren't more than two excperiods, and if so, warn the
        % user that he/she may be using the wrong plot modelegend_text
        if size(excperiods,1) < 2 && ...
                (lookup(excperiods(1,end),t) ~= numel(t) || ...
                 lookup(excperiods(1,1),t) ~= 1)
           warning(['plotBehavior2: Plotting potentially a multitraj data'... 
               ' set in single traj mode']);
        end
        
        % Get the included times
        t_ofinterest = ~isExcluded(t, excperiods);
        
        % Now proceed to plot them
        legend_text{1} = 'All Behavior';
        legend_text{2} = 'This Trajectory';
        p(1)    =   get(gca,'Children');
        p(2)    =   plot(x(t_ofinterest),y(t_ofinterest));
        
        % If directional flow plotting on, then provide arrows along the
        % trajectory to give a better send of how the animal is moving
        % through that space
        if arrow && ~isempty(p(2))
                plotAndCorrectArrows(p(2),x,y,t_ofinterest);
        end
        
        % Title and legend
        title_string = sprintf('FilteredBehavior,day%d,ep%d',ind(1),ind(2));
        set(legend(p,legend_text),LEGEND_OPT{:});
        title(title_string);
        
        % Save, if requested
        conditionalSave('plotBeh-singleTrajMode',title_string);
        
    end
%%  multiTrajMode(ind,excperiods,t,x,y)
%   Purpose: Plots only using EXCLUDETIMES input .. contiguous time regions
%   assumed to be different trajectories
    function multiTrajMode(ind,excperiods,t,x,y,po)
        %
        % Purpose: 
        
        % Check that we're potentially dealing with something that has
        % multiple separate continuous periods
        if size(excperiods,1) < 2 && ...
                (lookup(excperiods(1,end),t) == numel(t) || ...
                 lookup(excperiods(1,1),t) == 1)
             warning(['plotBehavior2: Plotting potentially singletraj data'...
                 ' set in mult traj mode.']);
        end
        
        % First, we need to take the multiple discontinous trajectories
        incperiods = getincludedtimes(excperiods);
        
        trajs = getTrajSet(po,1:size(incperiods,1));
        
        legend_text{1} = 'All Behavior';
        p(1) = get(gca,'Children');
        trajcount = 0;
        for i = trajs
            
            t_ofinterest = t >= incperiods(i,1) & t <= incperiods(i,2);
            if sum(t_ofinterest)<=0, continue, end;
            
            trajcount=trajcount+1;
            p(trajcount)=plot( x(t_ofinterest), y(t_ofinterest) );
            p(trajcount).LineWidth = CONST.trajlinewidth;
           
            
            % If directional flow plotting on, then provide arrows along the
            % trajectory to give a better send of how the animal is moving
            % through that space
            if po.arrow && ~isempty(p(trajcount))
                plotAndCorrectArrows(p(trajcount),x,y,t_ofinterest);
            end
            
            legend_text{trajcount} = num2str(trajcount);
            
        end
        
         % Label things
        title_string = sprintf('ZonePerTrajectory,day%d,ep%d',ind(1),ind(2));
        set(legend(p,legend_text),LEGEND_OPT{:});
        title(title_string);
        
        % Save, if requested
        conditionalSave('plotBeh-MultipleTrajMode',title_string);
        
    end
%-------------------------------------------------------------------------
%               HELPER FUNCTIONS -- Universal plot tools
%-------------------------------------------------------------------------
%%   plotTrajinfoMessage(text_ax,trajinfo,i)
%   Purpose: Unifies the arrow plotting prpocess for all of the modes. A
%   change here changes the way all three plot them.
	function message_handle = trajinfoMessage(text_ax,trajinfo,i,x,y,type,shift)
        
        if nargin == 7 && shift
            persistent initialized;
            persistent height_state;
            if isempty(initialized)
                initialized=true;
                height_state=0;
            else
                height_state = height_state+1;
                height_state = mod(height_state,2);
            end
            height_level = height_state - 0.5; % makes value oscillate between calls, 0, 1, -1, ...
        end
        
        if nargin < 6
            type = 1;
        end
        
        message = sprintf(['Trajbound: %d \n' ...
            'Rewarded: %d \n' ...
            'Trajtime: (%6.4f,\n %6.4f) \n' ...
            'WellStEnd: %d -> %d'], ...
            trajinfo.trajbound(i), trajinfo.rewarded(i), ...
            trajinfo.trajtime(i,1), trajinfo.trajtime(i,2),...
            trajinfo.wellstend(i,1), trajinfo.wellstend(i,2));
        
        if type
            message_handle = text(text_ax,x,y, message);
            message_handle.FontSize=24;
        else
            
            if exist('height_level','var')
                if height_level < 0
                    y(1) = 0.05;
                else
                    y(1) = 0.95;
                end
            end
            
            a=gca;
            axis(text_ax);
            message_handle=annotation('textarrow',x,y,'String',message);
            axis(a);
        end
    end
%%   plotAndCorrectArrows(p,t,x,y)
%   Purpose: Unifies the arrow plotting prpocess for all of the modes. A
%   change here changes the way all three plot them.
	function arrow_handles = plotAndCorrectArrows(p,x,y,t_ofinterest)
        %
        % Purpose: 
        
        X = x(t_ofinterest); Y = y(t_ofinterest);
        
        % test if we can throw arrows on the xy data
        if numel(X) < 2 || (numel(X) == 2 && X(1) == X(2) && ...
                Y(1) == Y(2))
            return;
        end 
        
        traj_color = p.Color; 
        if norm(traj_color,2)~=0 
            traj_color = traj_color/norm(traj_color,2);
        end
        
        arrow_handles=arrowh( X, Y, ...
                    traj_color, [80,60], 0:arrowdensity:100);
        
        for a = 1:numel(arrow_handles)
            obj = handle(arrow_handles(a));
            obj.FaceAlpha = 0.75;
            obj.EdgeColor = 'k';
            obj.EdgeAlpha = 0.2;
        end
	end
%-------------------------------------------------------------------------
	function set = getTrajSet(po,superset)
		if po.randomsubsample > 0 || po.maxtraj_perplot > 0
			
			randind = randperm(max(superset));
			
			if po.randomsubsample
				superset=randind(1:po.randomsubsample);
			elseif po.maxtraj_perplot > 0 && max(superset) > po.maxtraj_perplot
				superset=randind(1:po.maxtraj_perplot);
			end
			
			set = sort(superset);
		else
			set = superset;
		end
    end
%-------------------------------------------------------------------------
    function inFilter = filterTimes(filter,zonetrack_this,i)
        
        if isempty(filter)
            % If no filter provided, its just a logical of true's the size
            % of the time vector, because we're going to allow all times
            % already selected
            inFilter = ones(size(zonetrack_this.time));
            
        else
            % If there is a filter string, then create the eval string.
            % This string evaluates a logical that prunes the existing
            % inZone times by rewards, trajbounds, or any other variable in
            % zonetrack
            evalstr = strrep(filter,'$','zonetrack_this.');
            evalstr = [evalstr ';'];
            inFilter = eval(evalstr);
        end
    end

%-------------------------------------------------------------------------
%               HELPER FUNCTIONS -- Plotting and waiting
%-------------------------------------------------------------------------
    function conditionalSave(mode,title_string)
        
        children=get(gcf,'children');
        
        % Prepened title, if user provided any
        title(children(end),...
            sprintf('%s\n%s',titlePrepend,children(end).Title.String));
        drawnow;
        
        % Save
        if exist('savedir','var')
            warning('off'); mkdir(fullfile(savedir,mode,animprefix)); warning('on');
            savestr = fullfile(savedir,mode,animprefix,[title_string '.png']);
            saveas(gcf, savestr,'png');
            warning('off'); mkdir(fullfile(savedir,mode,animprefix,'fig')); warning('on');
            savestr = fullfile(savedir,mode,animprefix,'fig',[title_string '.fig']);
            saveas(gcf, savestr,'fig');
        end
    end
%-------------------------------------------------------------------------
    function conditionalWait()
        % Controls how user waits when waitplots flag is active
         if waitplots
            [~]= input('Press enter to continue ...','s');
        end
    end
%-------------------------------------------------------------------------
    function desiredindex =  colof(req, structdat)
    % This looks at the 'fields' field of a struct and decides which column
    % to sample in .data ... this exists because I've seen some of the
    % columns shift around occassionally in the data
		data = structdat.data;
		fieldparams = structdat.fields;

		% remove any parentheticals
		[a,b] = regexp(fieldparams,'\(.*\)');

		d=[];
		for i = 1:numel(a)
			d = [d a(i):b(i)];
		end

		fieldparams(d) = [];

		fieldparams = strsplit(fieldparams);

		desiredindex = 0;

		for i = 1:length(fieldparams)
			if strcmp(fieldparams{i},req)
				desiredindex = i;
			end
		end
	end

end