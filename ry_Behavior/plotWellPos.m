function plotWellPos(animal,index)

ainfo = animaldef('HPa');

load( sprintf( '%s/%slinpos%02d', ainfo{2:3}, index(1) ) );
% load( sprintf( '%spos%02d', ainfo{3}, index(1) ) ); 
% pos = pos{index(1)}{index(2)};
linpos = linpos{index(1)}{index(2)};

wellpos = linpos.wellSegmentInfo.wellCoord';
segpos = linpos.segmentInfo.segmentCoords;
segpos = mat2cell(segpos,ones(size(segpos,1),1));
segpos = cellfun(@(x) reshape(x,[2 2])', segpos,'uniformoutput',false);

for s = segpos', s=s{1};
    hold on; line(s(:,1),s(:,2));
end

% plot( [segpos(1:end,1); segpos(1:end,3)], [segpos(1:end,2); segpos(1:end,4)], '-k' );
hold on;
for well = wellpos,
    plot(well(1),well(2),'o');
end

end