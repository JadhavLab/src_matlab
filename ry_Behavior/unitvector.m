function [u,v] = unitvector(dir)
% Get unit vector of angle

    if isscalar(dir)
        dir = quad2normal(dir);
    else
        dir = ( (dir-min(dir(:)))/abs(range(dir(:))) * 2*pi );
    end
    u=cos(dir);
    v=sin(dir);

    function quad = quad2normal(quad)
        quad(quad<0) = quad(quad<0)+2*pi;
    end
    
end