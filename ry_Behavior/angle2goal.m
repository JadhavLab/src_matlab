function [angle_heading_to_goal,dir_heading,head_dir,angle_well,w] = ...
    angle2goal(x,y,dir,linpos)
        % Calculates the angle between the animal and all of the wells
        
        smoothing = false;    

        % Get well coordinates
        well=linpos.wellSegmentInfo.wellCoord;
        % Acquire HEADING direction
        x_heading = [0; diff(x)];
        y_heading = [0; diff(y)];
        % Heading dir
        dir_heading = atan2(y_heading,x_heading);

        % Get x and y of each well
        x_well = well(:,1);
        y_well = well(:,2);

        % Calculate angle
        angle_well = atan2( ...
            bsxfun(@minus,y_well',y),...
            bsxfun(@minus,x_well',x)...
            );

        % Calculate angle to well
        angle_heading_to_goal = quad2normal(bsxfun(@minus,dir_heading,angle_well));
        if ~isempty(dir); head_dir = quad2normal(dir); else; head_dir=[]; end
        
        % Local history of velocity, which can be thought of as magnitude
        % of heading_dir that could be used to select only times in which
        % the measure smoothly varies.
        u = conv(x_heading,ones(1,5)); v = conv(y_heading,ones(1,5));
        w = sqrt(u.^2 + v.^2);
        
        % If smoothing on, smooth measuring that involve heading direction
        if smoothing
            angle_heading_to_goal = smooth_boxcar(angle_heading_to_goal,3);
            dir_heading = smooth_boxcar(dir_heading,3);
        end

    end

    function quad = quad2normal(quad)
        quad(quad<0) = quad(quad<0)+2*pi;
    end
    
    function x = smooth_boxcar(x,n)
        if isrow(x),x=x';end;
        x = conv( x , ones(n,1) );
    end
    
    function [u,v] = unitvector(dir)
    % Get unit vector of angle for tan bi-quadrant direction

        if isscalar(dir)
            dir = quad2normal(dir);
        else
            dir = ( (dir-min(dir(:)))/abs(range(dir(:))) * 2*pi );
        end
        u=cos(dir);
        v=sin(dir);

    end