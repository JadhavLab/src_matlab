function addCumPerformance(animal)
% This function adds cumultative performance measures to the task struct of
% an animal
%
% TODO: Add bayesian measures from getestprobcorrect.m

animal = animaldef(animal);
traj = loaddatastruct(animal{2},animal{3},'trajinfo');

% ------
% Initialized trackers
% ------
epoch=[];day=[];tot=[];
% -----
for d = 1:numel(traj)
    nextd=true;
    for e = 1:numel(traj{d})
        if isempty(traj{d}{e}), continue; end;
        
        [epoch,day,tot] = computeMeasures(traj{d}{e});
        nextd=false;
        
        % Now add computed measures to the struct
        traj{d}{e}.C.description = ['C contains cumulative statisics over epoch, day, and overall for number of rewards, percent correct, and derivative of reward proportion.'];
        traj{d}{e}.C.epoch = epoch;
        traj{d}{e}.C.day = epoch;
        traj{d}{e}.C.overall = epoch;
    end
end

% save the trajinfo data struct
savedatastruct(traj,animal{2},animal{3},'trajinfo');

function [e,d,t] = computeMeasures(traj,type)
    
    if exist('type','var')
        type='';
    end
    
    % Accumulate epoch measures
    e = cumulate(epoch,traj,{'flush'});
    
    % Accumulate day measures
    if nextd
        d = cumulate(day,traj,{'flush'});
    else
        d =cumulate(day,traj);
    end
    
    % Accumulate total measures
    t = cumulate(tot,traj);
    
    
    function x= cumulate(x,traj,command)
        
        if (exist('command','var') && ismember('flush',command)) || ...
                isempty(x)
            
            all.init_cr = 0;
            all.init_N = 0;
            all.init_dr = 0;
            all.init_ddr = 0;
            
            in.init_cr = 0;
            in.init_N = 0;
            in.init_dr = 0;
%             in.init_ddr = 0;
            
            out.init_cr = 0;
            out.init_N = 0;
            out.init_dr = 0;
%             out.init_ddr = 0;
            
        else
            
            all.init_cr = x.all.correct(end);
            all.init_N = x.all.N(end);
            all.init_dr = x.all.derivative(end);
            all.init_ddr = x.all.dderivative(end);
            
            out.init_cr = x.out.correct(end);
            out.init_N = x.out.N(end);
            out.init_dr = x.out.derivative(end);
%             out.init_ddr = x.out.dderivative(end);
            
            in.init_cr = x.in.correct(end);
            in.init_N = x.in.N(end);
            in.init_dr = x.in.derivative(end);
%             in.init_ddr = x.in.dderivative(end);
            
        end
        
        x.all=struct(...
            'correct'     ,zeros(size(traj.rewarded)),...
            'N'             ,zeros(size(traj.rewarded)),...
            'percent'     ,zeros(size(traj.rewarded)),...
            'derivative'    ,zeros(size(traj.rewarded)),...
            'dderivative'   ,zeros(size(traj.rewarded))...
            );
        x.out=struct(...
            'correct'     ,nan(size(traj.rewarded)),...
            'N'             ,zeros(size(traj.rewarded)),...
            'percent'     ,zeros(size(traj.rewarded)),...
            'derivative'    ,zeros(size(traj.rewarded))...
...             'dderivative'   ,zeros(size(traj.rewarded))...
            );
        x.in=struct(...
            'correct'     ,nan(size(traj.rewarded)),...
            'N'             ,zeros(size(traj.rewarded)),...
            'percent'     ,zeros(size(traj.rewarded)),...
            'derivative'    ,zeros(size(traj.rewarded))...
...             'dderivative'   ,zeros(size(traj.rewarded))...
            );
        
        % stats for all trials
        x.all.correct = all.init_cr + cumsum(traj.rewarded);
        x.all.N = all.init_N + [1:numel(traj.rewarded)]';
        x.all.percent = x.all.correct./x.all.N;
        x.all.derivative = all.init_dr + [NaN; diff(x.all.percent)];
        x.all.dderivative = all.init_ddr + [NaN; NaN; diff(diff(x.all.percent))];
        
        % find all outbound trials
        ind = traj.trajbound==0;
        x.out.N = out.init_N + cumsum(ind);
        temp = traj.rewarded; temp(~ind)=0;
        x.out.correct = out.init_cr + cumsum(temp);
        x.out.percent = x.out.correct./x.out.N;
        x.out.derivative = out.init_dr + [NaN; diff(x.out.percent)];
%         x.out.dderivative = out.init_ddr + [NaN; NaN; diff(diff(x.out.Prewarded))];
        
        % find all inbound trials
        ind = traj.trajbound==1;
        x.in.N = in.init_N + cumsum(ind);
        temp = traj.rewarded; temp(~ind)=0;
        x.out.correct = in.init_cr + cumsum(temp);
        x.in.percent = x.in.correct./x.in.N;
        x.in.derivative = in.init_dr + [NaN; diff(x.in.percent)];
%         x.in.dderivative = in.init_ddr + [NaN; NaN; diff(diff(x.in.Prewarded))];
        
    end
    
end
end