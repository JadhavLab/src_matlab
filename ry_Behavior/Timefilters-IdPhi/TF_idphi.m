function out = TF_idphi(animaldir,animalprefix,epochs,varargin)
% This function timefilters on idphi collected within some radius around
% an animal, for some amount of time in the the past

%% Variable argument inputs
p=inputParser;
p.addParameter('space_boundary',2,@isreal);
p.addParameter('time_boundary',0.25,@isreal);
p.addParameter('ploton',2.5,@isreal);
p.addParameter('saveFilter',false,@islogical);
p.addParameter('skipProcessed',false,@islogical);
p.parse(varargin{:});
space_boundary=p.Results.space_boundary;
time_boundary=p.Results.time_boundary;
skipProcessed=p.Results.skipProcessed;
ploton=p.Results.ploton;
saveFilter=p.Results.saveFilter;

%% Process

% Acquire pos
pos = loaddatastruct(animaldir,animalprefix,'pos');

if skipProcessed
    turnaccel = loaddatastruct(fullfile(animaldir,'idphi/'),animalprefix,'turnaccel');
end

% Process each epoch's accel, dphi, and Idphi with space and time
% boundaries inputted above
for row = 1:size(epochs,1)
    
    d=epochs(row,1);
    e=epochs(row,2);
    fprintf('\nTF_idphi an %s day %d ep %d',animalprefix,d,e);
    
    if skipProcessed && ~isempty(turnaccel{d}{e})
        if time_boundary==turnaccel{d}{e}.time_boundary &&...
                space_boundary==turnaccel{d}{e}.space_boundary
            out{d}{e}=turnaccel{d}{e};
            continue;
        end
    end
    
    p = pos{d}{e};
    [dphi,phi]=getPhiParameters(p);
    [accel,jerk]=getHigherMoments(p);
    t = p.data(:,1);
    x = p.data(:,colof('x-sm',p));
    y = p.data(:,colof('y-sm',p));
    
    Idphi=zeros(size(dphi));
    for i = 1:length(dphi)
        
        space_indices = radius(i,x,y);
        time_indices = (t(i) - time_boundary)>=t ...
            & t<=t(i);
        
        selector = space_indices & time_indices;
        
        Idphi(i) = sum(abs(dphi(selector)));
    end
    
    out{d}{e}.time = p.data(:,1);
    out{d}{e}.Idphi=Idphi;
    out{d}{e}.logIdphi = log10(Idphi);
    out{d}{e}.dphi=dphi;
    out{d}{e}.phi=phi;
    out{d}{e}.accel=accel;
    out{d}{e}.absaccel=abs(accel);
    out{d}{e}.jerk=jerk;
    out{d}{e}.absjerk=abs(jerk);
    out{d}{e}.space_boundary=space_boundary;
    out{d}{e}.time_boundary=time_boundary;
  
    % Plot?
    ploton=false;
    if ploton
        gcf;clf;
        subplot(1,3,1);
        plot(x,y,'k:');
        hold on;
        hs=scatter(x,y,[],Idphi,'filled');
        alpha(hs,0.25);colorbar;axis tight;
        title('Idphi');
        subplot(1,3,2);
        plot(x,y,'k:');
        hold on;
        hs=scatter(x,y,[],dphi,'filled');
        alpha(hs,0.25);colorbar;axis tight;
        title('dphi');
        subplot(1,3,3);
        plot(x,y,'k:');
        hold on;
        hs=scatter(x,y,[],phi,'filled');
        alpha(hs,0.25);colorbar;axis tight;
        title('phi');
        saveThis(gcf,pwd,sprintf('%sphi-%d-%d',animalprefix,d,e),'png');
    end
    
end

% Save filter?
if saveFilter
    warning off; mkdir(fullfile(animaldir,'idphi/')); warning on;
    savedatastruct(out,fullfile(animaldir,'idphi/'),animalprefix,'turnaccel');
end

%% Helper functions
    function [indices] = radius(i,x,y)
        dist=sqrt((x(i)-x).^2 + (y(i)-y).^2);
        indices=dist <= space_boundary;
    end

    function [dphi,phi] = getPhiParameters(pos)

        pos.fields = [pos.fields ' dphi'];
        dT=mode(diff(pos.data(:,1)));

        currx = pos.data(:,colof('x-sm',pos)); 
        curry = pos.data(:,colof('y-sm',pos));
        
        dx =[nan; diff(currx)];
        dy =[nan; diff(curry)];

        %Get phi: orientation of motion in angular coordinates
        phi = atan2(dy,dx);

        dphi = [nan; diff( phi )];
    end

    function [acceleration,jerk] = getHigherMoments(pos)
		
		velocity		= pos.data(:,9);
		time			= pos.data(:,1);
		
		diff_vel		= diff(velocity);
		diff_time		= diff(time);
		
		acceleration	= [diff_vel./diff_time];
        
        diff_accel          = diff(acceleration);
		diff_diff_time		= 1;

        jerk    = [diff_accel./diff_diff_time];
        
        acceleration=[nan;acceleration;];
        jerk=[nan(2,1);jerk];

        
    end

end