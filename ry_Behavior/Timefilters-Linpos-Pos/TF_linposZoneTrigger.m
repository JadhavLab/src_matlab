function out = TF_linposZoneTrigger(animaldir,animalprefix,epochs,varargin)
% out = TF_trackZoneTrigger(animaldir,animalprefix,epochs,varargin)
% ------------------------------------------------------------------------------
% Name:		 
%
% Author:	Ryan
%
% Purpose:	Controls selection of times to sample the animal on the track.
%			It is built around the notion of selecting a zone, where, when
%			the animal enters or exits this zone, it triggers a sample.
%			Another ability, instead of triggering on an entrace or exit,
%			it can simply take the entire time in that zone. One can also
%			select the nth zone trigger per trajectory. This can filter out
%			the first crossing in a trajectory (or nth crossing! ... an
%			interesting analysis might look at how conditions change from
%			1st to nth crossing.) Additionally it can filter times when
%			animal is nearest a point per trajectory. Last, if you leave
%			the controlling inputs blank, it creates a time filter for just
%			raw trajinfo information.
%
%           To just achieve a timefilter of the raw trajectory
%           information from trajinfo, leave the zonecontrols and trigger
%           controls empty.
%
%
% Parents:	DFSry_ChoiceRegionAnalysis
% 
% Inputs:	ANIMALDIR, ANIMALPREFIX, EPOCHS, VARARAGIN
%
% Outputs:	out     ...		a struct containing the time filtered regions per
%							epoch from which to, down-stream in the filter
%							analysis, do either eeg or spike analysis on.
%
%           out.inZone      A binary of 1's indicating times in the
%                           specified zone or trigger around entrance/exit
%                           of a zone, or trigger at the point an animal is
%                           nearest a single point.
%           out.trajbound   A vector containing inbound (1) or outbound (0)
%                           ONLY FOR THE TIMES inside the zone above
%           out.rewarded    A vector containg a record of whether the
%                           trajectory was rewarded (1) or not (0), only
%                           for the times inside the zone
%           out.trajnumber  A vector containing the actual number of the
%                           of the trajectory the time point is apart of,
%                           for times in the zone
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

    %% SET AND READ OPTIONAL ARGUMENTS
 	% Set optional arguments. User can elect spatial proximity to well and
    % temporal proximity to end of trial

    p=inputParser;
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    % TIME/TRAJ CONTROLS
    % Controls seelction of a point based on time. This is not for
    % triggering (see trigger controls below), but rather picking a time
    % where something happens on the track.
    %----------------------------------------------------------------------
    p.addParameter('normtimeselect',[],@isreal); % example [0.5 1] would select latter trial halves
    p.addParameter('wellswitch',[],@isreal);     % example true, this only selects trajectories where the selects an end well different from the start well
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    % LINDIST CONTROLS
    % Controls which a segment or segment portion to obtain times
    % from, and the portion of the given segments to obtain.
    % ---------------------------------------------------------------------
    p.addParameter('lindist',[],@isreal);
    p.addParameter('exclude_lindist',[],@isreal);
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    % SEGMENT CONTROLS
    % Controls which a segment or segment portion to obtain times
    % from, and the portion of the given segments to obtain.
    % ---------------------------------------------------------------------
    p.addParameter('segment_select',[],@isreal);
    p.addParameter('segment_select_portion',[],@isreal);
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    % ZONE CONTROLS
    % Controls which zones or just a point to sample behavior from
    % (or trigger on if provide trigger controls below)
    %----------------------------------------------------------------------
    p.addParameter('spatial_proximity',[],@isreal);
        % If spatial proximity provided, then instead of the zone
        % being a point, it becomes a circle with radius
        % spatial_proximity.
    p.addParameter('zone_segment',[],@isreal);
         % Which segment to draw the zone in
    p.addParameter('zone_segment_dist',[],@isreal);
         % Where to to draw the zone in the segment. Accepted values
        % are 'initial' 'final' and number between 0.0-1.0
        % indicating percent distance from initial to final point
        % of the segment in linpos
    p.addParameter('coordinate_positions',[],@isreal);
        % Alternatively, you can directly provide a coordinate to
        % draw a zone
    p.addParameter('force_traj_split',[],@islogical);
        % This ensures continguous segments of selected time never
        % bleed over the edge of a trajectory time
    p.addParameter('inverse',[],@islogical);
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------   
    % TRIGGER CONTROLS
    % If trigger control options not given this function defaults
    % to just taking times inside the zone. If TCs are given, then
    % you get times triggered on the entrance or exits from a zone.
    % ---------------------------------------------------------------------
    p.addParameter('trigger_mode','',@ischar);
        % Currently, can trigger either at zone
        % entrances/exits/both OR on the time nearest a single
        % point. -- specify 'zonecross' for a zone cross trigger or
        % 'pointpass' for minimized point distance trigger
    p.addParameter('temporal_trigger_window',[],@(x) isreal(x) && ~any(x<0));
        % How many seconds before and after to sample around the
        % trigger time.
    p.addParameter('nth_trigger',[],@isreal);
        % Controls which nth trigger crossing event(s) to use;
        % specify 1 if you only want the first crossing
    p.addParameter('entrance_or_exit','',@ischar);
        % Whether to trigger on entrances or exits. if not
        % specified, it triggers on both.
    
    %OTHER
    p.addParameter('savedir','',@ischar)
    
    % PARSE
    p.CaseSensitive=false; p.parse(varargin{:});
    % "zone control" "trigger control" "lindist control" "segment control"
    zc = struct; tc = struct; lc = struct; sc = struct;
    
    % Distribute results to selection control types
    trc.wellswitch = p.Results.wellswitch;
    trc.normtimeselect = p.Results.normtimeselect;
    lc.lindist = p.Results.lindist;
    lc.exclude_lindist=p.Results.exclude_lindist;
    sc.segment_select=p.Results.segment_select;
    sc.segment_select_portion=p.Results.segment_select_portion;
    zc.spatial_proximity=p.Results.spatial_proximity;
    zc.zone_segment=p.Results.zone_segment;
    zc.zone_segment_dist=p.Results.zone_segment_dist;
    zc.coordinate_positions=p.Results.coordinate_positions;
    zc.force_traj_split=p.Results.force_traj_split;
    zc.inverse = p.Results.inverse;
    tc.trigger_mode=p.Results.trigger_mode;
    tc.temporal_trigger_window=p.Results.temporal_trigger_window;
    tc.nth_trigger=p.Results.nth_trigger;
    tc.entrance_or_exit=p.Results.entrance_or_exit;
    
    savedir=p.Results.savedir;
    
    %% Obtain trajinfo and linpos for all filter epochs
	
	% Agglomerate master cell of trajinfo, pos, linpos
	trajinfo = loaddatastruct(animaldir,animalprefix,'trajinfo',...
		unique(epochs(:,1)));
	%pos = loaddatastruct(animaldir,animalprefix,'pos', ...
	%	unique(epochs(:,1)));
	linpos = loaddatastruct(animaldir,animalprefix,'linpos',...
		unique(epochs(:,1)));

	%% Compute Trajectory-based Time Information
	% ---------------------------------------------------------------------
    
	for i = 1:size(epochs,1)
		
		d = epochs(i,1);
		e = epochs(i,2);
		
		fprintf('Timefilter: Processing day %d, ep %d\n',d,e);
		
		% Create shortcut structures for the trajectory, pos, linpos of the
		% current epoch
		this_trajinfo           = trajinfo{d}{e};
		this_linpos             = linpos{d}{e};
        this_linpos.statematrix.lindist=this_linpos.statematrix.lindist/max(this_linpos.statematrix.lindist);
		
		%% Compute cartesion choice point centroid, if zone controls provided
        % (this only has to be doen once per epoch)
		if ~isempty(zc.coordinate_positions)
			zc.region_centroid = zc.coordinate_positions;
		elseif ~isempty(zc.zone_segment) && ~isempty(zc.zone_segment_dist)
			zc.region_centroid = setCoordinate(this_linpos,...
				zc.zone_segment,zc.zone_segment_dist);
		else
			zc.region_centroid=[];
        end
		
        %% PROCESS EACH TRAJECTORY
        
        % Initialize
		zone_InOrOut     = zeros(size(this_linpos.statematrix.time));
		trajnumber       = zeros(size(this_linpos.statematrix.time)); 
        trialtime        = zeros(size(this_linpos.statematrix.time)); 
        normtrialtime    = zeros(size(this_linpos.statematrix.time)); 
        trajbound        = zeros(size(this_linpos.statematrix.time));
        rewarded         = zeros(size(this_linpos.statematrix.time));
        wellst           = zeros(size(this_linpos.statematrix.time));
        wellend          = zeros(size(this_linpos.statematrix.time));
        zone_InOrOut_trajnumber     = zeros(size(this_linpos.statematrix.time));

        
		for traj = 1:numel(this_trajinfo.trajbound)
            
            % Find all times matching user-defined characteristics by 1's
            % and times not by 0's
            [constraint_times, traj_times]= ...
                trajectoryTimes(traj, ...
                this_trajinfo, [], this_linpos, ...
                zc,sc,lc,tc,trc);
            
            % Flip into proper orientation
			if isrow(constraint_times); constraint_times = constraint_times'; end;
            
            % Add information to our collection matrix for inZone
			zone_InOrOut = zone_InOrOut | constraint_times;
            
            minTime = min(this_linpos.statematrix.time(traj_times));
            maxTime = max(this_linpos.statematrix.time(traj_times));
            
            % Set remaining higher order collection matrices
            zone_InOrOut_trajnumber = zone_InOrOut_trajnumber + double(constraint_times) .* traj;
			trajnumber  = trajnumber    + traj                              * traj_times;
            trialtime   = trialtime     + (this_linpos.statematrix.time-minTime) .* traj_times;
            trajbound   = trajbound     + this_trajinfo.trajbound(traj)     * traj_times;
            rewarded    = rewarded      + this_trajinfo.rewarded(traj)      * traj_times;
            wellst      = wellst        + this_trajinfo.wellstend(traj,1)   * traj_times;
            wellend     = wellend       + this_trajinfo.wellstend(traj,1)   * traj_times;
            normtrialtime   = trialtime/(maxTime-minTime);
        end
		
        %% POST-PROCESS OUTPUTS
        
		% Trajtime doesn't tile, i.e. carve into trajectories, the whole
		% time vector, sometimes. Last trajectory can end premature by a
		% point to the end of the time vector.Here that issue is corrected.
		points_outside_trajtime = numel(this_linpos.statematrix.time) ...
			- numel(zone_InOrOut);
		zone_InOrOut = postProcess(zone_InOrOut,points_outside_trajtime);
        trajbound   = postProcess(trajbound,points_outside_trajtime);
        rewarded    = postProcess(rewarded, points_outside_trajtime);
        trajnumber  = postProcess(trajnumber, points_outside_trajtime);
        trialtime   = postProcess(trialtime, points_outside_trajtime);
        normtrialtime   = postProcess(normtrialtime, points_outside_trajtime);
        wellst      = postProcess(wellst, points_outside_trajtime);
        wellend     = postProcess(wellend, points_outside_trajtime);
        zone_InOrOut_trajnumber = postProcess(zone_InOrOut_trajnumber, points_outside_trajtime);
		
        %% SET OFFICIAL OUTPUS OF THIS TIME FILTER
        
        % Assign zonecontrol outputs, so downstream plot methods can use
        out{d}{e}.zc            = zc;
        
		% Assign trajectory related outputs
        out{d}{e}.inZone		= zone_InOrOut;
        out{d}{e}.inZone_traj   = zone_InOrOut_trajnumber;
        out{d}{e}.trajnumber	= trajnumber;
        
        out{d}{e}.trajbound     = trajbound;
        out{d}{e}.rewarded      = rewarded;
        
        out{d}{e}.wellst            = wellst;
        out{d}{e}.wellend           = wellend;
        
        out{d}{e}.trialtime         = trialtime;
        out{d}{e}.normtrialtime     = normtrialtime;
        
        % Lastly, provide the all important time vector
		out{d}{e}.time			= this_linpos.statematrix.time;
		
		% Assert outputs of equal length
		assert(isequal(size(out{d}{e}.inZone),size(out{d}{e}.time)));
		
	end
	
	% Save timefilter structure for special plotting functionalities of
	% plotBehavior -- this is not by any means required by downstream
	% functions
	savefile = sprintf('%szonetrack',animalprefix);
    warning off; mkdir(savedir); warning on;
	save(fullfile(savedir,savefile),'out');
	
    % END OF FUNCTION
	return;
    
    
% -------------------------------------------------------------------------  
%% HELPER FUNCTIONS 	
% -------------------------------------------------------------------------

%%              trajectoryTimes
% Purpose:		computes trajectory in zone or nearest a point, and can
%               either return this information as is, or pass it to a
%               function that controls triggering on a time or times
%               extracted herein.
% ChangeLog
%			
%				May 2016: In efforts to make the algorithm analyze larger
%				tracts of time properly, instead of returning an answer
%				about the characteristics of just this trajectorie's times,
%				switching to returning information about entire session,
%				per trajectory.
% -------------------------------------------------------------------------
function [constraint_times, times_in_traj] = ...
		trajectoryTimes(tr,trajinfo,~,lp,zc,sc,lc,tc,trc)
	
	%% Pre-process
    
    % Animal t,x,y for epoch
    t = lp.statematrix.time;
    if isfield(lp.statematrix,'XY_proj')
        x = lp.statematrix.XY_proj(:,1);
        y = lp.statematrix.XY_proj(:,2);
    else
        x = [];
        y = [];
        warning('XY_proj not made for this epoch');
    end
    constraint_times = ones(size(t));
    % Subset t,x,y based on times in traj
    times_in_traj = (t >= trajinfo.trajtime(tr,1)) & ...
        (t < trajinfo.trajtime(tr,2));
	
    % Find a region of times based on one of three modes -- currently, the
    % function 
    if ~isempty(zc.region_centroid) % MODE 1: Circular Zones
        
         % Setup distance function
        dist_to_point = @(x,y) ...
            sqrt((x-zc.region_centroid(1)).^2 +(y-zc.region_centroid(2)).^2);
        
        %% Process zone or point of interest
        switch tc.trigger_mode % EXTENSION OF MODE 1

            case {'zonetrigger','off',''}

            % Find indices in region
            constraint_times = (dist_to_point(x,y) <= zc.spatial_proximity) & ...
                times_in_traj & constraint_times;

            case 'pointpass'

            % Get nearest point to centroid provided
            x(~times_in_traj)=inf;y(~times_in_traj)=inf;
            point_distances = dist_to_point(x,y);
            % Find the minimum
            [~,minInd] = min(point_distances);
            % Set into binary vector
            constraint_times = false(size(t));
            constraint_times(minInd)=true;

            otherwise
                error('TF_trackZoneTrigger: Not a valid trigger_mode');
        end

        % If no zone or trigger controls, then we just return vector of 1's
        % length of traj
        if isempty(tc) && isempty(zc)
            return
        end
    end
    if ~isempty(sc.segment_select) % MODE 2: SEGMENTS
        
        if isrow(sc.segment_select); sc.segment_select=sc.segment_select';end;
        for s = sc.segment_select
            times_in_seg = s==lp.statematrix.segmentIndex;
            constraint_times = constraint_times & times_in_seg;
        end
    end 
    if ~isempty(lc.lindist)  % MODE 3: LINDIST
        
        if (size(lc.lindist,1) > size(lc.lindist,2)) || ...
                isrow(lc.lindist)
            lc.lindist=lc.lindist'; 
        end
        for l = lc.lindist
            time_within_lindist = lp.statematrix.lindist >= l(1) & ...
                lp.statematrix.lindist <= l(2);
            constraint_times = constraint_times & time_within_lindist;
        end
        
        if ~isempty(lc.exclude_lindist) % Exclusion sub-mode
            
            if (size(lc.exclude_lindist,1) > size(lc.exclude_lindist,2)) || ...
                    isrow(lc.lindist)
                lc.exclude_lindist=lc.exclude_lindist'; 
            end
            for l = lc.exclude_lindist
                time_without_lindist = ~(lp.statematrix.lindist >= l(1) & ...
                    lp.statematrix.lindist <= l(2));
                constraint_times = constraint_times & time_without_lindist;
            
            end
        end
        
    end
    if ~isempty(trc.normtimeselect) % MODE 4: TIME-SELECT
        normtime = (  t-min(t(times_in_traj)) )...
            /(max(t(times_in_traj))-min(t(times_in_traj)));
        constraint_times = constraint_times & ...
            (normtime >= trc.normtimeselect(1) & normtime <= trc.normtimeselect(2));
    end
    if ~isempty(trc.wellswitch)  % MODE 5: WELL CRITERIA
        trajinfo.wellstend(tr,:)
        if trc.wellswitch
            if trajinfo.wellstend(tr,1) == trajinfo.wellstend(tr,2)
                constraint_times(constraint_times==true)=false;
            end
        else
            if trajinfo.wellstend(tr,1) ~= trajinfo.wellstend(tr,2)
                constraint_times(constraint_times==true)=false;
            end
        end
    end
	
	%% Process trigger, if applicable
	% If we need to use the trigger control structure to calculate times
	% given the trigger parameters, e.g., times around entrance or exit of
	% a region
	if ~isempty(tc) && ~isempty(tc.trigger_mode) ...
            && ~isequal(tc.trigger_mode,'off') && sum(constraint_times) > 0
		constraint_times = convertToTrigger(constraint_times,t,tc);
	end
	
	%% Post-processing
	% Ensure no times bleeding over the trajectory
    if zc.inverse == true
        constraint_times = ~constraint_times & times_in_traj;
    end
	if zc.force_traj_split == true
		separation=[find(times_in_traj,1,'first') find(times_in_traj,1,'last')];
		separator=times_in_traj; separator(separation)=false;
		constraint_times = constraint_times .* separator;
    end
end

%%  			convertToTrigger
% Purpose:		convert indices of time in zone to indices of times in a
%				zone defined by a trigger into or out of that region
% -------------------------------------------------------------------------
function region_in_trigger = convertToTrigger(binary_in_zone,times,tc)
	
	debug = true;
	
	% Find the entries into ROI
	enters	= find(diff(binary_in_zone) == 1);
	% Find exits
	exits	= find(diff(binary_in_zone) == -1)+1;
	
	if debug
		fprintf('NumberEnter: %d, NumberExit: %d\n', numel(enters),numel(exits));
	end
	
	% Check if its an entrance or exit --
	% if it is neither, no option, take all as triggers
	if isfield(tc,'entrance_or_exit')
		switch lower(tc.entrance_or_exit)
			case 'entrance'
				triggers = enters;
			case 'exit'
				triggers = exits;
            otherwise
                error('entrance_or_exit: invalid option %s',tc.entrance_or_exit);
		end
	else
		triggers = sort([enters; exits]);
	end
	
	% Compute trigger subset based on the nth_trigger trigger control
	if ~isempty(tc.nth_trigger)
		triggers = triggers(tc.nth_trigger);
	end
	
	% Compute times around the triggers
	rit_total = [];
	if isempty(tc.temporal_trigger_window)
        region_in_trigger				= zeros(size(times));
		region_in_trigger(triggers)		= 1 ;
    else
		
        % TODO fix for edge conditions when vectorized segments > 1; for
        % right now, this is a very fast but RAM heavy computation
		vectorized_segments = 1; % controls how the vectorized computation is chunked, for computing speed .. 1 = fastest, n > 1 = lower RAM
		assert(vectorized_segments==1);
        
		trigger_logical = zeros(size(times));
		trigger_logical(triggers) = 1;
		
        warning off; % warns about function mat2cell calls that will phase out soon
		seg_time = mat2cell(times,vectorized_segments,1);
		seg_triggers = mat2cell(trigger_logical,vectorized_segments,1);
        warning on;
		for k = 1:numel(seg_time);
			
			timefrac=seg_time{k};
			trigfrac = logical(seg_triggers{k});
			
			tw = tc.temporal_trigger_window;
			if isscalar(tw); tw = [tw tw]; end;
            if find(tw<0); warning('non-positive window value .. are you sure? time-window(1),time+window(2)');end;
			
			if ~isempty(timefrac(trigfrac))
				trigwinstart	= repmat(timefrac(trigfrac)-tw(1),[1,numel(timefrac)]);
				trigwinstop		= repmat(timefrac(trigfrac)+tw(2),[1,numel(timefrac)]);

				T = repmat(timefrac',[size(trigwinstart,1),1]);

				region_in_trigger = T >= trigwinstart & T <= trigwinstop;
				region_in_trigger = sum(region_in_trigger,1) > 0;
			else
				region_in_trigger = zeros(size(timefrac))';
			end

			rit_total = [rit_total region_in_trigger];
		end
		
		region_in_trigger = rit_total;		
	end
	
end


%%   			setCoordinate
% Purpose:		given a segment, and information that identifies where on
%				the segment, it spits out the proper (x,y) tuple
% ------------------------------------------------------------------------------
function xy = setCoordinate(linpos, segmentnumber, segmentaddress)

	segmentcoord = linpos.segmentInfo.segmentCoords;
    warning off;
	sc_grouped = ...
		mat2cell(segmentcoord,size(segmentcoord,1),2);
    warning off;

	if ischar(segmentaddress)
		switch lower(segmentaddress)
			case 'initial'
				xy = sc_grouped{segmentnumber,1};
			case 'final'
				xy = sc_grouped{segmentnumber,2};
			otherwise
				error('findCoordinate: invalid segmentaddress string');
		end
	elseif isreal(segmentaddress) && segmentaddress > 0 && segmentaddress < 1
		final		= sc_grouped{segmentnumber,2};
		initial		= sc_grouped{segmentnumber,1};
		% Define the spatial extent between initial and the final segment
		spat_extent = final - inintial;
		% Calculate the the fraction along that line to assign the point.
		% This is done by taking initial + frac*(final-initial)
		xy = initial + segmentaddress*(final-initial);
	else
		error('findCoordinate: segmentaddresss unrecognized type');
	end
end

%%              postProcess
% Purpose:		adds any missing points, if/when it happens. (very rare,
%               at most a single point).
% -------------------------------------------------------------------------
    function data = postProcess(data,points_in_excess)
        
        if numel(points_in_excess)>1
            warning('Points in excess %d',points_in_excess);
        end
        
        data = ...
            [data; zeros(points_in_excess,1)];
    end

end

%% GRAVEYARD
% 
%  THE FOLLOWING ARE FUNCTIONS THAT EVENTUALLY COULD BE USED TO CREATE A
%  MORE GENERALIZED TF_linposZoneTrigger
% %%              getTrajStruct
% % Purpose:      Handles conversion of any of the traj-by-traj trajinfo
% % fields into timefilter format. This is done once per traj, and it outputs
% % time length values.
% function this_trajinfo = getTrajStruct(this_trajinfo,traj)
%     
%     for f = fields(this_trajinfo)'
%         f=f{1};
%         
%         if isstruct(this_trajinfo.(f))
%             this_trajinfo.(f) = getTrajStruct(this_trajinfo.(f),traj);
%         else
%             if ~(isrow(this_trajinfo.(f)) || iscolumn(this_trajinfo.(f)))
%                 continue;
%             else
%                 this_trajinfo.(f) = this_trajinfo.(f)(traj);
%             end
%         end
%     end
%     
% end
% 
% %%              updateTrajStruct
% % Purpose:      Handles conversion of any of the traj-by-traj trajinfo
% % fields into timefilter format. This is done once per traj, and it outputs
% % time length values.
% function current = updateTrajStruct(current,new,traj_times)
%     
%     if isempty(current)
%         
%         for f = fields(new)'
%             f=f{1};
%             
%             if isstruct(new.(f))
%                 new.(f) = updateTrajStruct([],new.(f),traj_times);
%             else
%                 current.(f) = new.(f) * traj_times;
%             end
%             
%         end
%         
%     else
%         
%         for f = fields(new)'
%             f=f{1};
%             
%             if isstruct(new.(f))
%                 new.(f) = updateTrajStruct(current.(f),new.(f),traj_times);
%             else
%                 current.(f) = current.(f) + new.(f) * traj_times;
%             end
%             
%         end
%         
%     end
%         
% end
