% ------------------------------------------------------------------------------
% Name:		TF_trackZoneTrigger
%
% Author:	Ryan
%
% Purpose:	Controls selection of times to sample the animal on the track.
%			It is built around the notion of selecting a zone, where, when
%			the animal enters or exits this zone, it triggers a sample.
%			Another ability, instead of triggering on an entrace or exit,
%			it can simply take the entire time in that zone. One can also
%			select the nth zone trigger per trajectory. This can filter out
%			the first crossing in a trajectory (or nth crossing! ... an
%			interesting analysis might look at how conditions change from
%			1st to nth crossing.) Additionally it can filter times when
%			animal is nearest a point per trajectory. Last, if you leave
%			the controlling inputs blank, it creates a time filter for just
%			raw trajinfo information.
%
% Parents:	DFSry_ChoiceRegionAnalysis
% 
% Inputs:	ANIMALDIR, ANIMALPREFIX, EPOCHS, VARARAGIN
%
% Outputs:	out     ...		a struct containing the time filtered regions per
%							epoch from which to, down-stream in the filter
%							analysis, do either eeg or spike analysis on.
%
%           out.inZone      A binary of 1's indicating times in the
%                           specified zone or trigger around entrance/exit
%                           of a zone, or trigger at the point an animal is
%                           nearest a single point.
%           out.trajbound   A vector containing inbound (1) or outbound
%           (0)M
%                           ONLY FOR THE TIMES inside the zone above
%           out.rewarded    A vector containg a record of whether the
%                           trajectory was rewarded (1) or not (0), only
%                           for the times inside the zone
%           out.trajInZone  A vector containing the actual number of the
%                           of the trajectory the time point is apart of,
%                           for times in the zone
%
%
% IN-PROGRESS	Attempting to add a true traj split mode, that ensures times
%				different trajectories are split by excluded times no
%				matter how large the selected times
%
%
% TODO --		Fix for boundary condition when time trigger bleeds over trajtime
%				boundaries ... FIXED!! Now need to come up with a solution for
%				when different selected times merge
%
% TODO --		Add null condition, when no extra input provided! Should just
%				return standard filterable trajinfo time vectors, without
%				special filtered binary_in_zone
%
% TODO --		Add segment transition mode
%
% TODO --		Add different types of zones .. box and ellipsis
%
% TODO --       need to remodel such that timefilter can work on our linpos
%               linpos structures
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = TF_new_trackZoneTrigger(animaldir,animalprefix,epochs,varargin)
	
% 	% Set optional arguments. User can elect spatial proximity to well and
% 	% temporal proximity to end of trial
% 	velpos = DFTFsj_getvelpos(animaldir,animalprefix,epochs);
	savedir='';
	zc = struct; tc = struct;
	for i = 1:2:numel(varargin)
		switch varargin{i}
            
            % To just achieve a timefilter of the raw trajectory
            % information from trajinfo, leave the zonecontrols and trigger
            % controls empty.
			
			% ZONE CONTROLS
            % Controls which zones or just a point to sample behavior from
            % (or trigger on if provide trigger controls below)
			% ------------------------------------------------------------
			case 'spatial_proximity'
                % If spatial proximity provided, then instead of the zone
                % being a point, it becomes a circle with radius
                % spatial_proximity.
				zc.spatial_proximity		= varargin{i+1};
				
			case 'segment_id'
                % Which segment to draw the zone in
				zc.segment_id				= varargin{i+1};
			case 'segment_address'
                % Where to to draw the zone in the segment. Accepted values
                % are 'initial' 'final' and number between 0.0-1.0
                % indicating percent distance from initial to final point
                % of the segment in linpos
				zc.segment_address			= varargin{i+1};
			case 'coordinate_positions'
                % Alternatively, you can directly provide a coordinate to
                % draw a zone
				zc.coordinate_positions		= varargin{i+1};
				
			case 'force_traj_split'
				% This ensures continguous segments of selected time never
				% bleed over the edge of a trajectory time
				zc.force_traj_split			= varargin{i+1};
				
			% TRIGGER CONTROLS
            % If trigger control options not given this function defaults
            % to just taking times inside the zone. If TCs are given, then
            % you get times triggered on the entrance or exits from a zone.
			% ------------------------------------------------------------
            case 'trigger_mode', tc.trigger_mode = varargin{i+1};
                % Currently, can trigger either at zone
                % entrances/exits/both OR on the time nearest a single
                % point. -- specify 'zonecross' for a zone cross trigger or
                % 'pointpass' for minimized point distance trigger
			case 'temporal_trigger_window'
                % How many seconds before and after to sample around the
                % trigger time.
				tc.temporal_trigger_window	= varargin{i+1};
			case 'nth_trigger'
                % Controls which nth trigger crossing event(s) to use;
                % specify 1 if you only want the first crossing
				tc.nth_trigger				= varargin{i+1};
			case 'entrance_or_exit'
                % Whether to trigger on entrances or exits. if not
                % specified, it triggers on both.
				tc.entrance_or_exit			= varargin{i+1};
                
            % TRAJECTORY TYPE CONTORLS
            % This section has not been implemented yet, but will be
            % extremely easy to add. Already is a way to filter on these
            % variables from within the function.
%             case 'inbound'
%                 tj.inbound = varargin{i+1};
%                 tj.outbound = ~varargin{i+1};
%             case 'outbound'
%                 tj.inbound = ~varargin{i+1};
%                 tj.outbound = varargin{i+1};
            
            % OTHER
            case 'savedir', savedir         = varargin{i+1};
			
		end
    end
    
    % Apply default options
    [zc,tc]=setdefaults(zc,tc);
	
	% Agglomerate master cell of trajinfo, pos, linpos
	trajinfo = loaddatastruct(animaldir,animalprefix,'trajinfo',...
		unique(epochs(:,1)));
	pos = loaddatastruct(animaldir,animalprefix,'pos', ...
		unique(epochs(:,1)));
	linpos = loaddatastruct(animaldir,animalprefix,'linpos',...
		unique(epochs(:,1)));

	%% Compute Trajectory-based Time Information
	% --------------------------------------------------------------------------
	for i = 1:size(epochs,1)
		
		d = epochs(i,1);
		e = epochs(i,2);
		
		fprintf('Timefilter: Processing day %d, ep %d\n',d,e);
		
		% Create shortcut structures for the trajectory, pos, linpos of the
		% current epoch
		this_trajinfo		= trajinfo{d}{e}; 
		this_pos			= pos{d}{e};
		this_linpos			= linpos{d}{e};
		
		%% Compute Region
		if isfield(zc,'coordinate_positions')
			region_centroid = zc.coordinate_positions;
		elseif isfield(zc,'segment_id') && isfield(zc,'segment_address')
			region_centroid = setCoordinate(this_linpos,...
				zc.segment_id,zc.segment_address);
		else
			error('Must provide information to address sampling zone');
        end
		
        %% PROCESS EACH TRAJECTORY
		zone_InOrOut     = zeros(size(this_pos.data(:,1)));
		time_trajinfo = [];
		for traj = 1:numel(this_trajinfo.trajbound)
            
            % Find all times matching user-defined characteristics by 1's
            % and times not by 0's
            [compatible_times traj_times]= ...
                trajectoryTimes(traj, ...
                this_trajinfo, this_pos, this_linpos, ...
                region_centroid,zc,tc);
            
            % Flip into proper orientation
			if isrow(compatible_times); compatible_times = compatible_times'; end;
            
            % Pull out additional information about this traj
            trajbound_this   = this_trajinfo.trajbound(traj);
            rewarded_this    = this_trajinfo.rewarded(traj);
            
            % Add information to our collection matrix for inZone
			zone_InOrOut = zone_InOrOut | compatible_times;
            % Set remaining higher order collection matrices
			trajnumber = trajnumber + traj*traj_times;
            
            trajbound = trajbound + trajbound_this*traj_times;
            rewarded = rewarded + rewarded_this*traj_times;
        end
		
        %% POST-PROCESS OUTPUTS
		% Trajtime doesn't tile, i.e. carve into trajectories, the whole
		% time vector, sometimes. Last trajectory can end premature by a
		% point to the end of the time vector.Here that issue is corrected.
		points_outside_trajtime = numel(this_pos.data(:,1)) ...
			- numel(zone_InOrOut);
		zone_InOrOut = ...
            [zone_InOrOut; zeros(points_outside_trajtime,1)];
        trajbound = ...
            [trajbound; zeros(points_outside_trajtime,1)];
        rewarded = ...
            [rewarded; zeros(points_outside_trajtime,1)];
        trajnumber = ...
            [trajnumber; zeros(points_outside_trajtime,1)];
		
        %% SET OFFICIAL OUTPUS OF THIS TIME FILTER
        % Assign zonecontrol outputs, so downstream plot methods can use
        zc.region_centroid      = region_centroid;
        out{d}{e}.zc            = zc;
        
		% Assign trajectory related outputs
        out{d}{e}.inZone		= zone_InOrOut;
        out{d}{e}.trajInZone	= trajnumber;
        
        out{d}{e}.trajbound     = trajbound;
        out{d}{e}.rewarded      = rewarded;
        
        % Lastly, provide the all important time vector
		out{d}{e}.time			= this_pos.data(:,1);
		
		% Assert outputs of equal length
		assert(isequal(size(out{d}{e}.inZone),size(out{d}{e}.time)));
		
	end
	
	% Save timefilter structure for special plotting functionalities of
	% plotBehavior -- this is not by any means required by downstream
	% functions
	savefile = sprintf('%szonetrack',animalprefix);
    warning off; mkdir(savedir); warning on;
	save(fullfile(savedir,savefile),'out');
	
    % END OF FUNCTION
	return;
    
    
% ------------------------------------------------------------------------------  
%% HELPER FUNCTIONS 	
% ------------------------------------------------------------------------------
%%   getTrajStruct
%       Purpose:      Handles conversion of any of the traj-by-traj
%       trajinfo fields into timefilter format. This is done once
%       per traj, and it outputs time length values.
function oneTraj = getTrajStruct(this_trajinfo,traj)
    
    for f = fields(this_trajinfo)'
        f=f{1};
        
        if isstruct(this_trajinfo.(f))
            this_trajinfo.(f) = getTrajStruct(this_trajinfo.(f),traj);
        else
            if ~(isrow(this_trajinfo.(f)) || iscolumn(this_trajinfo.(f)))
                continue;
            else
                this_trajinfo.(f) = this_trajinfo.(f)(traj,:);
            end
        end
    end
    
    oneTraj=this_trajinfo;
    
end

%%              updateTrajStruct
%       Purpose:      Handles conversion of any of the traj-by-traj
%       trajinfo fields into timefilter format. This is done once per traj,
%       and it outputs time length values.
function current = updateTrajStruct(current,new,traj_times)
    
    if isempty(current)
        
        for f = fields(new)'
            f=f{1};
            
            if isstruct(new.(f))
                new.(f) = updateTrajStruct([],new.(f),traj_times);
            else
                current.(f) = new.(f) * traj_times;
            end
            
        end
        
    else
        
        for f = fields(new)'
            f=f{1};
            
            if isstruct(new.(f))
                new.(f) = updateTrajStruct(current.(f),new.(f),traj_times);
            else
                current.(f) = current.(f) + new.(f) * traj_times;
            end
            
        end
        
    end
        
end
% ------------------------------------------------------------------------------
%%              setdefaults
% Purpose:      sets the defaults of our two option constructs: tc (trigger
%               control) and zc (zone control).
function [zc,tc] = setdefaults(zc,tc)
%     if ~isfield(zc,'spatial_proximity') || isempty(zc.spatial_proximity)
%         zc.spatial_proximity=1;
%     end
%     if ~isfield(zc,'segment_id') || isempty(zc.segment_id)
%         zc.segment_id=1;
%     end
%     if ~isfield(zc,'segment_address') || isempty(zc.segment_address)
%         zc.segment_address='final';
%     end
%     if ~isfield(zc,'coordinate_positions') || isempty(zc.segment_address)
%     end
    if isempty(fields(tc)) && isempty(fields(zc))
        return
    end

    if ~isfield(tc,'trigger_mode') || isempty(tc.trigger_mode)
        tc.trigger_mode = '';
    end
end

%%              trajectoryTimes
% Purpose:		computes trajectory in zone or nearest a point, and can
%               either return this information as is, or pass it to a
%               function that controls triggering on a time or times
%               extracted herein.
% ChangeLog
%			
%				May 2016: In efforts to make the algorithm analyze larger
%				tracts of time properly, instead of returning an answer
%				about the characteristics of just this trajectorie's times,
%				switching to returning information about entire session,
%				per trajectory.
% ------------------------------------------------------------------------------
function [compatible_times times_in_traj] = ...
		trajectoryTimes(tr,trajinfo,p,lp,region_centroid,zc,tc)
	
	%% Pre-process
	
	% Animal t,x,y for epoch
	t = p.data(:,1);
	x = p.data(:,2);
	y = p.data(:,3);
	
	% Subset t,x,y based on times in traj
	times_in_traj = (t >= trajinfo.trajtime(tr,1)) & ...
		(t < trajinfo.trajtime(tr,2));
    
    % Setup distance function
        dist_to_point = @(x,y) ...
            sqrt((x-region_centroid(1)).^2 +(y-region_centroid(2)).^2);
        
    % If no zone or trigger controls, then we just return vector of 1's
    % length of traj
    if isempty(tc) && isempty(zc)
        compatible_times = ones(size(t));
        return
    end

    %% Process zone or point of interest
    switch tc.trigger_mode
        
        case {'zonetrigger','off',''}

        % Find indices in region
        inside_region = (dist_to_point(x,y) <= zc.spatial_proximity) & ...
			times_in_traj;
        
        case 'pointpass'
        
        % Get nearest point to centroid provided
		x(~times_in_traj)=inf;y(~times_in_traj)=inf;
        point_distances = dist_to_point(x,y);
        % Find the minimum
        [~,minInd] = min(point_distances);
        % Set into binary vector
        inside_region = false(size(t));
        inside_region(minInd)=true;
        
        otherwise
            error('TF_trackZoneTrigger: Not a valid trigger_mode');
    end
	
	%% Process trigger, if applicable
	% If we need to use the trigger control structure to calculate times
	% given the trigger parameters, e.g., times around entrance or exit of
	% a region
	if ~isempty(tc) && ~isempty(tc.trigger_mode) ...
            && ~isequal(tc.trigger_mode,'off') && sum(inside_region) > 0
		compatible_times = convertToTrigger(inside_region,t,tc);
    else
        compatible_times = inside_region;
	end
	
	%% Post-processing
	% Ensure no times bleeding over the trajectory
	if isfield(zc,'force_traj_split') && zc.force_traj_split
		separation=[find(times_in_traj,1,'first') find(times_in_traj,1,'last')];
		separator=times_in_traj; separator(separation)=false;
		compatible_times = compatible_times .* separator';
	end
end

%%  			convertToTrigger
% Purpose:		convert indices of time in zone to indices of times in a
%				zone defined by a trigger into or out of that region
% ------------------------------------------------------------------------------
function region_in_trigger = convertToTrigger(binary_in_zone,times,tc)
	
	debug = false;
	
	% Find the entries into ROI
	enters	= find(diff(binary_in_zone) == 1);
	% Find exits
	exits	= find(diff(binary_in_zone) == -1)+1;
	
	if debug
		fprintf('NumberEnter: %d, NumberExit: %d\n', numel(enters),numel(exits));
	end
	
	% Check if its an entrance or exit --
	% if it is neither, no option, take all as triggers
	if isfield(tc,'entrance_or_exit')
		switch lower(tc.entrance_or_exit)
			case 'entrance'
				triggers = enters;
			case 'exit'
				triggers = exits;
            otherwise
                error('entrance_or_exit: invalid option %s',tc.entrance_or_exit);
		end
	else
		triggers = sort([enters; exits]);
	end
	
	% Compute trigger subset based on the nth_trigger trigger control
	if isfield(tc,'nth_trigger')
		triggers = triggers(tc.nth_trigger);
	end
	
	% Compute times around the triggers
	rit_total = [];
	if isfield(tc,'temporal_trigger_window')
		
        % TODO fix for edge conditions when vectorized segments > 1; for
        % right now, this is a very fast but RAM heavy computation
		vectorized_segments = 1; % controls how the vectorized computation is chunked, for computing speed .. 1 = fastest, n > 1 = lower RAM
		assert(vectorized_segments==1);
        
		trigger_logical = zeros(size(times));
		trigger_logical(triggers) = 1;
		
        warning off; % warns about function mat2cell calls that will phase out soon
		seg_time = mat2cell(times,vectorized_segments,1);
		seg_triggers = mat2cell(trigger_logical,vectorized_segments,1);
        warning on;
		for k = 1:numel(seg_time);
			
			timefrac=seg_time{k};
			trigfrac = logical(seg_triggers{k});
			
			tw = tc.temporal_trigger_window;
			if isscalar(tw); tw = [tw tw]; end;
			
			if ~isempty(timefrac(trigfrac))
				trigwinstart	= repmat(timefrac(trigfrac)-tw(1),[1,numel(timefrac)]);
				trigwinstop		= repmat(timefrac(trigfrac)+tw(2),[1,numel(timefrac)]);

				T = repmat(timefrac',[size(trigwinstart,1),1]);

				region_in_trigger = T >= trigwinstart & T <= trigwinstop;
				region_in_trigger = sum(region_in_trigger,1) > 0;
			else
				region_in_trigger = zeros(size(timefrac))';
			end

			rit_total = [rit_total region_in_trigger];
		end
		
		region_in_trigger = rit_total;
		
	else
		region_in_trigger				= zeros(size(times));
		region_in_trigger(triggers)		= 1 ;
	end
	
end


%%   			setCoordinate
% Purpose:		given a segment, and information that identifies where on
%				the segment, it spits out the proper (x,y) tuple
% ------------------------------------------------------------------------------
function xy = setCoordinate(linpos, segmentnumber, segmentaddress)

	segmentcoord = linpos.segmentInfo.segmentCoords;
    warning off;
	sc_grouped = ...
		mat2cell(segmentcoord,size(segmentcoord,1),2);
    warning off;

	if ischar(segmentaddress)
		switch lower(segmentaddress)
			case 'initial'
				xy = sc_grouped{segmentnumber,1};
			case 'final'
				xy = sc_grouped{segmentnumber,2};
			otherwise
				error('findCoordinate: invalid segmentaddress string');
		end
	elseif isreal(segmentaddress) && segmentaddress > 0 && segmentaddress < 1
		final		= sc_grouped{segmentnumber,2};
		initial		= sc_grouped{segmentnumber,1};
		% Define the spatial extent between initial and the final segment
		spat_extent = final - inintial;
		% Calculate the the fraction along that line to assign the point.
		% This is done by taking initial + frac*(final-initial)
		xy = initial + segmentaddress*(final-initial);
	else
		error('findCoordinate: segmentaddresss unrecognized type');
	end
end

end