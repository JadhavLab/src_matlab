
% In version 2, get rid of csi and propbursts, since Ndl doesn't seem to have these.
% Also, you want to make an addition, where you go back to the params and matclust file, and load other parameters
% such as amplitude, ahp size, etc
% Just using the cellinfo structure to get and plot cell properties


clear;
runscript = 0;
savedata = 0; % save data option - only works if runscript is also on
figopt1 = 1; % Figure Options

savedir = '/data25/sjadhav/HPExpt/ProcessedData/';
%val=1; savefile = [savedir 'HP_cellinfo_CA1']; %
val=2; savefile = [savedir 'HP_cellinfo_PFC']; %


% If runscript, run Datafilter and save data
if runscript == 1
    
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Ndl'};
    
    %Filter creation
    %--------------------------------------------------------
    
    % Epoch filter
    % -------------
    %dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    %epochfilter = 'isequal($type, ''run'')'; % All run environments
    epochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''ytr'')'; % Start with just W-tracks run
    
    % Cell filter
    % -----------
    
    % Any cell defined in at least one run in environment
    
    switch val
        case 2
           cells = '(strcmp($area, ''PFC'')) &&  ($numspikes > 100)';
        case 1
           cells = '( strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) &&  ($numspikes > 100)';
    end
    
    
    % Time filter
    % -----------
    % None
    
    
    % Iterator
    % --------
    iterator = 'singlecellanal';
    
    % Filter creation
    % ----------------
    cellsf = createfilter('animal',animals,'epochs',epochfilter,'cells',cells,'iterator', iterator);
    
    % Set analysis function
    % ----------------------
    cellsf = setfilterfunction(cellsf, 'DFAsj_getcellinfo2', {'cellinfo'}); % You can also use "cellprop" which uses spikes as the variable and calls getcellprop
    
    
    % Run analysis
    % ------------
    cellsf = runfilter(cellsf);  %
    
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end  % end runscript

if ~exist('savedata')
    return
end


% --------------------------
% Post-filter run analysis
% --------------------------

% ---------------------------------------------------------
% Gather data and append animal index to it as well
% Unlike codes where you want to keep data for epochs separate for plotting (eg. place field code), here, combine everything for population analysis.
% ---------------------------------------------------------

alldata = []; alltags = []; allanimindex = []; % includes animal index
cnt = 0; % For cnting across animals for tags
for an = 1:length(animals)
    for i=1:length(cellsf(an).output{1}),
        anim_index{an}(i,:)=cellsf(an).output{1}(i).index;
        % Only indexes
        animindex=[an cellsf(an).output{1}(i).index]; % Put animal index in front
        allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
        
        % Indexes + Data [anim day epoch tet cell rate numspikes spikewidth
        append = [an cellsf(an).output{1}(i).index cellsf(an).output{1}(i).meanrate ...
            cellsf(an).output{1}(i).numspikes cellsf(an).output{1}(i).spikewidth];
        alldata = [alldata; append];
        
        % Make a tag field with the same length
        %cnt=cnt+1;
        %allrawtags(cnt).tag = cellsf(an).output{1}(i).tag;
        
    end
end

% -----------------------------------------------------------------
% Consolidate single cells' across epochs in a day .celloutput field
% Can do for each animal and then store separately by using the index from output field - Do in loop for each animal,
% or can use the above appended animal+index field to parse data
% -----------------------------------------------------------------

% Save the consolidated data back in a structure - TO put back in filter
% structure, has to be animal-wise. Sell alternate method below
celloutput = struct;
dummyindex=allanimindex;  % all anim-day-epoch indices


cntcells=0;
for i=1:size(alldata,1)
    animdaytetcell=alldata(i,[1 2 4 5]);
    ind=[];
    while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
        ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
        dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
        % so you could find the next one
    end
    
    % Gather everything for the current cell across epochs
    currrate=[]; currcsi=[]; currpropbursts=[]; currnumspikes=[]; currspikewidth=[]; currtag = [];
    for r=ind
        currrate=[currrate ; alldata(r,6)];
        currnumspikes=[currnumspikes;alldata(r,7)];
        currspikewidth=[currspikewidth;alldata(r,8)];
    end
    
    if ~isempty(currrate)
        %currtag = allrawtags(ind(1)).tag; % Tag is same for all epochs
        cntcells = cntcells + 1;
        celloutput(cntcells).index=animdaytetcell;
        celloutput(cntcells).meanrate=currrate; % Remember, this is seperated across epochs. Can take mean
        celloutput(cntcells).numspikes=currnumspikes;
        celloutput(cntcells).spikewidth=currspikewidth;
    end
end

% Alternate method - Keep Animal data separate. For each animal, look within dayeptetcell
% ----------------------------------------------------------------------------------------
% for an = 1:length(animals)
%     dummyindex = anim_index{an};     % collect all day-epoch-tet-cell indices
%     for i=1:length(cellsf(an).output{1})
%         daytetcell=cellsf.output{1}(i).index([1 3 4]);
%         ind=[];
%         while rowfind(daytetcell,dummyindex(:,[1 3 4]))~=0          % collect all rows (epochs)
%             ind = [ind rowfind(daytetcell,dummyindex(:,[1 3 4]))];
%             dummyindex(rowfind(daytetcell,dummyindex(:,[1 3 4])),:)=[0 0 0 0];
%         end
%         % Gather everything for the current cell across epochs
%         currrate=[]; currcsi=[]; currpropbursts=[]; currnumspikes=[]; currspikewidth=[]; currtag = [];
%         for r=ind
%             currrate=[currrate ; cellsf.output{1}(r).meanrate];
%             currcsi=[currcsi; cellsf.output{1}(r).csi];
%             currpropbursts=[currpropbursts; cellsf.output{1}(r).propbursts];
%             currnumspikes=[currnumspikes;cellsf.output{1}(r).numspikes];
%             currspikewidth=[currspikewidth;cellsf.output{1}(r).spikewidth];
%         end
%         if ~isempty(currrate)
%             currtag = cellsf.output{1}(ind(1)).tag; % Tag is same for all epochs
%             cellsf(an).celloutput.index=daytetcell;
%             cellsf(an).celloutput.meanrate=currrate;
%             cellsf(an).celloutput.csi=csi;
%             cellsf(an).celloutput.propbursts=propbursts;
%             cellsf(an).celloutput.numspikes=numspikes;
%             cellsf(an).celloutput.spikewidth=spikewidth;
%             cellsf(an).celloutput.tag=ncurrtag;
%         end
%     end
% end


% Values for cells - take means across epochs
allmeanrate = []; allcsi = []; allpropbursts = []; allnumspikes = []; allspikewidth = [];
for i=1:length(celloutput)
    allcellidx(i,:) = celloutput(i).index;
    allmeanrate = [allmeanrate; mean(celloutput(i).meanrate)];
    allspikewidth = [allspikewidth, mean(celloutput(i).spikewidth)];
end




% ---------
% Plotting
% ---------

length(allmeanrate)
length(find(allmeanrate<1))
length(find(allmeanrate<0.1))





%1) All Cells - Fir rates vs spikewidths
figure; hold on;
redimscreen_figforppt1;

%scatter(allspikewidth, allmeanrate, [],'r');
plot(allspikewidth, allmeanrate,'ko','MarkerSize',8,'LineWidth',1.5);
%plot(allspikewidth(Intidx), allmeanrate(Intidx),'kx','MarkerSize',8,'LineWidth',1.5);

xaxis = min(allspikewidth):0.1:max(allspikewidth);
plot(xaxis,0.1*ones(size(xaxis)),'k-','Linewidth',1);
plot(xaxis,10*ones(size(xaxis)),'k-','Linewidth',1);

set(gca,'YLim',[0 1.05*max(allmeanrate)]);
set(gca,'XLim',[0.95*min(xaxis) 1.05*max(xaxis)]);

xlabel('Spike Widths','FontSize',24,'Fontweight','normal');
ylabel('Firing Rates (Hz)','FontSize',24,'Fontweight','normal');
title('Cell Properties','FontSize',24,'Fontweight','normal')


clr = ['r','b','c','m','g','y'];

for an=1:4
    curridxs = find(allcellidx(:,1)==an);
    plot(allspikewidth(curridxs), allmeanrate(curridxs),[clr(an) 'o'],'MarkerSize',8,'LineWidth',1.5);
    i=1;
end
    
    
    







