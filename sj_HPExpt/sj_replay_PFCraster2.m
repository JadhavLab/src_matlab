
% PFC rip responses based in CA1 replay. Try for HPa day1 first


savefig=0;


savedir = '/data25/sjadhav/HPExpt/ProcessedData/';
savefile = [savedir 'HPa_day1_replay5_PFCresp'];
%savefile = [savedir 'HPa_day1_replay_PFCresp'];
%load(savefile,'PFCf');
load(savefile);
pretu=550; posttu=550; rwin = [0 200]; binsize=10;
pret=500; postt=500; trim=50;
nstd = round(binsize*2/binsize); g1 = gaussian(nstd, 3*nstd+1);

prefix='HPa'; 

cellsp  = PFCf(1).output{1}(1).PFCindices;
nPFCcells = size(cellsp,1);

n=3;  % Cell number - Pick cell.
PFCidx = cellsp(n,:)
spkalign_cand_ep1 = PFCf(1).output{1}(1).spkalign_cand{n};
spkalign_cand_ep2 = PFCf(1).output{1}(2).spkalign_cand{n};
spkalign_cand = [spkalign_cand_ep1, spkalign_cand_ep2];

spkalign_all_ep1 = PFCf(1).output{1}(1).spkalign_all{n};
spkalign_all_ep2 = PFCf(1).output{1}(2).spkalign_all{n};
spkalign_all = [spkalign_all_ep1, spkalign_all_ep2];

spkalign_nonevent_ep1 = PFCf(1).output{1}(1).spkalign_nonevent{n};
spkalign_nonevent_ep2 = PFCf(1).output{1}(2).spkalign_nonevent{n};
spkalign_nonevent = [spkalign_nonevent_ep1, spkalign_nonevent_ep2];


% -------------------
% Cand events and non-cand events figure
% -------------------

spkscell = spkalign_cand;
ncand = length(spkscell);

figure; hold on; redimscreen_figforppt1; xfont=24; yfont=24; tfont=24;  set(0,'defaultaxesfontsize',24);
set(gcf,'Position',[100 130 1000 950]);
%set(gcf, 'Position',[205 658 723 446]);
subplot(2,1,1); hold on;
spkcount = []; histo = [];
for c=1:length(spkscell)
    tmps = spkscell{c};
    
    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
    plot(tmps,(length(spkscell)-c+1)*ones(size(tmps)),'r.','MarkerSize',16);
    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
    
    % Get count of spikes in response window
    if ~isempty(tmps)
        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
        spkcount = [spkcount; length(subset_tmps)];
    end    
    % Also get a histogram
    histspks = histc(tmps,[-pretu:binsize:posttu]);
    if isempty(histspks), % Only happens if spikeu is empty
        histspks = zeros(size([-pretu:binsize:posttu]));
    else
        histspks = smoothvect(histspks, g1);
    end
    histo(c,:) = histspks*1000/binsize;   
end

% ----------------
spkscell = spkalign_nonevent;
nnoev = length(spkscell);

spkcount = []; histo2 = [];
for c=1:length(spkscell)
    tmps = spkscell{c};
    
    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
    plot(tmps,(length(spkscell)-c+1+ncand)*ones(size(tmps)),'k.','MarkerSize',16);
    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
    
    % Get count of spikes in response window
    if ~isempty(tmps)
        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
        spkcount = [spkcount; length(subset_tmps)];
    end    
    % Also get a histogram
    histspks = histc(tmps,[-pretu:binsize:posttu]);
    if isempty(histspks), % Only happens if spikeu is empty
        histspks = zeros(size([-pretu:binsize:posttu]));
    else
        histspks = smoothvect(histspks, g1);
    end
    histo2(c,:) = histspks*1000/binsize;   
end

% ------------------
spkscell = spkalign_all; % Plot this only in histogram
ntot = length(spkscell);

spkcount = []; histo3 = [];
for c=1:length(spkscell)
    tmps = spkscell{c};
    
    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
    %plot(tmps,(length(spkscell)-c+1)*ones(size(tmps)),'k.','MarkerSize',16);
    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
    
    % Get count of spikes in response window
    if ~isempty(tmps)
        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
        spkcount = [spkcount; length(subset_tmps)];
    end    
    % Also get a histogram
    histspks = histc(tmps,[-pretu:binsize:posttu]);
    if isempty(histspks), % Only happens if spikeu is empty
        histspks = zeros(size([-pretu:binsize:posttu]));
    else
        histspks = smoothvect(histspks, g1);
    end
    histo3(c,:) = histspks*1000/binsize;   
end
% TRIM ENDS of Histogram 
% -----------------------------------------------
histo = histo(:,trim/binsize:(pretu+posttu-trim)/binsize);
histo2 = histo2(:,trim/binsize:(pretu+posttu-trim)/binsize);
histo3 = histo3(:,trim/binsize:(pretu+posttu-trim)/binsize);

% Rster plot
set(gca,'XLim',[-pret postt]);
set(gca,'XTick',[])
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Cand replay event','FontSize',yfont,'Fontweight','normal');
set(gca,'YLim',[0 length(spkscell)]);
% Plot Line at 0 ms and rwin
ypts = 0:1:length(spkscell);
xpts = 0*ones(size(ypts));
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
title(sprintf('%s Day %d Tet %d Cell %d', prefix, PFCidx(1), PFCidx(3), PFCidx(4)),...
    'FontSize',tfont,'Fontweight','normal');

subplot(2,1,2); hold on;
xaxis = -pret:binsize:postt;
plot(xaxis,mean(histo),'r-','Linewidth',4);
plot(xaxis,mean(histo2),'k-','Linewidth',3);
plot(xaxis,mean(histo3),'c-','Linewidth',3);
legend('Cand Evs','Nonevs','All');
set(gca,'XLim',[-pret postt]);
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Firing rate (Hz)','FontSize',yfont,'Fontweight','normal');
%set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'));
ylow = min(mean( histo)-sem( histo));
yhigh = max(mean( histo)+sem( histo));
set(gca,'YLim',[ylow-0.1 yhigh+0.1]);
ypts = ylow-0.1:0.1:yhigh+0.1;
xpts = 0*ones(size(ypts));
% Plot Line at 0 ms - Onset of stimulation
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);


% % Save figure
[y, m, d] = datevec(date);
savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/%s_D%d_T%d_C%d_ripalign_all_candevents', prefix, PFCidx(1), PFCidx(3), PFCidx(4));
figfile = savestring;
if savefig==1
    print('-dpdf', savestring)
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end







% --------------------------------------------------------
% Get replay decoding
% -------------------------------------------------------

savedir = '/data25/sjadhav/HPExpt/ProcessedData/';
savefile = [savedir 'HPa_day1_replaydecode5_gather.mat'];
%savefile = [savedir 'HPa_day1_replaydecode_gather.mat'];
%load(savefile,'PFCf');
load(savefile);

pvalue_ep1 = decodefilter(1).output{1}(1).pvalue;
pvalue_ep2 = decodefilter(1).output{1}(1).pvalue;
pvalue = [pvalue_ep1; pvalue_ep2];

sig = find(pvalue<0.05); nsig = length(sig);
nosig = find(pvalue>0.05 & ~isnan(pvalue)); nnosig = length(nosig);
ntot = nsig + nnosig;

% -------------------
% Sig and non-sig events figure
% -------------------

spkscell=[];
for ii=1:length(sig)
    spkscell{ii} = spkalign_cand{sig(ii)};
end

figure; hold on; redimscreen_figforppt1; xfont=24; yfont=24; tfont=24;
set(gcf,'Position',[100 130 1000 950]);
%set(gcf, 'Position',[205 658 723 446]);
subplot(2,1,1); hold on;
spkcount = []; histo = [];
for c=1:length(spkscell)
    tmps = spkscell{c};
    
    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
    plot(tmps,(length(spkscell)-c+1)*ones(size(tmps)),'r.','MarkerSize',16);
    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
    
    % Get count of spikes in response window
    if ~isempty(tmps)
        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
        spkcount = [spkcount; length(subset_tmps)];
    end    
    % Also get a histogram
    histspks = histc(tmps,[-pretu:binsize:posttu]);
    if isempty(histspks), % Only happens if spikeu is empty
        histspks = zeros(size([-pretu:binsize:posttu]));
    else
        histspks = smoothvect(histspks, g1);
    end
    histo(c,:) = histspks*1000/binsize;   
end

% Non-sig

spkscell=[];
for ii=1:length(nosig)
    spkscell{ii} = spkalign_cand{nosig(ii)};
end

spkcount = []; histo2 = [];
for c=1:length(spkscell)
    tmps = spkscell{c};
    
    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
    plot(tmps,(length(spkscell)-c+1+nsig)*ones(size(tmps)),'k.','MarkerSize',16);
    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
    
    % Get count of spikes in response window
    if ~isempty(tmps)
        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
        spkcount = [spkcount; length(subset_tmps)];
    end    
    % Also get a histogram
    histspks = histc(tmps,[-pretu:binsize:posttu]);
    if isempty(histspks), % Only happens if spikeu is empty
        histspks = zeros(size([-pretu:binsize:posttu]));
    else
        histspks = smoothvect(histspks, g1);
    end
    histo2(c,:) = histspks*1000/binsize;   
end

% TRIM ENDS of Histogram 
% -----------------------------------------------
histo = histo(:,trim/binsize:(pretu+posttu-trim)/binsize);
histo2 = histo2(:,trim/binsize:(pretu+posttu-trim)/binsize);



set(gca,'XLim',[-pret postt]);
set(gca,'XTick',[])
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Sign replay event','FontSize',yfont,'Fontweight','normal');
set(gca,'YLim',[0 length(spkscell)]);
% Plot Line at 0 ms and rwin
ypts = 0:1:length(spkscell);
xpts = 0*ones(size(ypts));
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
title(sprintf('%s Day %d Tet %d Cell %d Nspkwin %d', prefix, PFCidx(1), PFCidx(3), PFCidx(4), sum(spkcount)),...
    'FontSize',tfont,'Fontweight','normal');

subplot(2,1,2); hold on;
xaxis = -pret:binsize:postt;
plot(xaxis,mean(histo),'r-','Linewidth',4);
plot(xaxis,mean(histo2),'k-','Linewidth',3);
legend('Sig Replay','Nonsig');
set(gca,'XLim',[-pret postt]);
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Firing rate (Hz)','FontSize',yfont,'Fontweight','normal');
%set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'));
ylow = min(mean( histo)-sem( histo));
yhigh = max(mean( histo)+sem( histo));
set(gca,'YLim',[ylow-0.1 yhigh+0.1]);
ypts = ylow-0.1:0.1:yhigh+0.1;
xpts = 0*ones(size(ypts));
% Plot Line at 0 ms - Onset of stimulation
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);



savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/%s_D%d_T%d_C%d_ripalign_candevents_sig', prefix, PFCidx(1), PFCidx(3), PFCidx(4));
figfile = savestring;
if savefig==1
    print('-dpdf', savestring)
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end








% --------------------------------------------------
% Get decode data for each significant replay event
% -------------------------------------------------
decode_ep1 = decodefilter(1).output{1}(1).decodedata;
decode_ep2 = decodefilter(1).output{1}(2).decodedata;
decode = [decode_ep1, decode_ep2];

% Ep1
sig = find(pvalue_ep1<0.05); nsig = length(sig);
% Keep significant only
replay_ep1=[];
for ii=1:length(sig)
    replay_ep1{ii} = decode_ep1{sig(ii)};
end

% Ep1
sig2 = find(pvalue_ep2<0.05); nsig2 = length(sig);
% Keep significant only
replay_ep2=[];
for ii=1:length(sig2)
    replay_ep2{ii} = decode_ep2{sig2(ii)};
end

% Get prob distr
trajmapping = [1 1 2 2]; xdata = {[],[]}; ydata = {[],[]};
probdata = {}; probdata = cell(2,1);
animal=1; d=1;
%combine the outbound and inbound trajectories - separately for epochs
% This has to be done for each event withn decodedata/ replay_ep1
cntevs=0; allprobdata=[]; allprobdata_left=[]; allprobdata_right=[];

e=1;
for evidx = 1:length(replay_ep1)
    if ~isempty(replay_ep1{evidx})
        probdata = {}; probdata = cell(2,1);
        for traj = 1:4;
            trajindex = find(trainingfilter(animal).output{d}(e).traj == traj);
            xdata{trajmapping(traj)} = trainingfilter(animal).output{d}(e).dist(trajindex);
            ydata{trajmapping(traj)} = stack(ydata{trajmapping(traj)}, replay_ep1{evidx}(trajindex,1)');
            if isempty(probdata{trajmapping(traj)})
                probdata{trajmapping(traj)} = replay_ep1{evidx}(trajindex,:);
            else
                probdata{trajmapping(traj)} = probdata{trajmapping(traj)} + replay_ep1{evidx}(trajindex,:);
            end
        end
        
        % probdata has 2 cells, for Left and Right (outbound and inbound collapsed) respectively
        cntevs=cntevs+1;
        allprobdata{cntevs} = probdata;
        allprobdata_left{cntevs} = probdata{1};
        allprobdata_right{cntevs} = probdata{2};
    end    
end

e=2;
for evidx = 1:length(replay_ep2)
    if ~isempty(replay_ep2{evidx})
        probdata = {}; probdata = cell(2,1);
        for traj = 1:4;
            trajindex = find(trainingfilter(animal).output{d}(e).traj == traj);
            xdata{trajmapping(traj)} = trainingfilter(animal).output{d}(e).dist(trajindex);
            ydata{trajmapping(traj)} = stack(ydata{trajmapping(traj)}, replay_ep2{evidx}(trajindex,1)');
            if isempty(probdata{trajmapping(traj)})
                probdata{trajmapping(traj)} = replay_ep2{evidx}(trajindex,:);
            else
                probdata{trajmapping(traj)} = probdata{trajmapping(traj)} + replay_ep2{evidx}(trajindex,:);
            end
        end
        
        % probdata has 2 cells, for Left and Right (outbound and inbound collapsed) respectively
        cntevs=cntevs+1;
        allprobdata{cntevs} = probdata;
        allprobdata_left{cntevs} = probdata{1};
        allprobdata_right{cntevs} = probdata{2};
    end    
end

stackleft=[]; stackright=[];
for ii=1:length(allprobdata_left)  
    currrep = allprobdata_left{ii};
    if size(currrep,2)>=6   % Atleast 6 timebins
        stackleft = [stackleft;currrep(1:85,1:6)];
    end
end
%figure; hold on; plot(mean(stackleft));


keyboard;


















