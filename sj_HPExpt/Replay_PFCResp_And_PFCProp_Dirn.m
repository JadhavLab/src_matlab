
% Get output from DFSsj_HPexot_getreplaydata_withPFCHist.m, and compare
% replay dirn (out vs in) with PFCprop: 
% Similar to Replay_PFCResp_And_PFCProp which was for armindex
% and Replay_PFCResp_And_PFCPE, for Path equivalence of deocded traj

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
cd /data25/sjadhav/HPExpt/HP_ProcessedData/;

respidx = [50:70]; % 0:200
bckidx = [1:40]; % -500:-100
binsize = 10; % ms
pret=500; postt=500; %% Times to plot
nstd = round(binsize*2/binsize); g1 = gaussian(nstd, 3*nstd+1); % nstd=1: gaussian of length 4. nstd = 2: gaussian of length 7, nstd=3: gaussian of length 10.

figcell=0;
set(0,'defaultaxesfontsize',20);
 
val=1;
gatherdata = 1; savegatherdata = 1;
switch val
    case 1
        gatherdatafile = [savedir 'HP_replaydata_PFCrespHist_Dirn_gather_X6']; % for saving current stuff
end


if gatherdata
    
    % In HP_replaydata_PFCallHist_proc_X6, you have condition of at least
    % one CA1 cell beyong choice point. In this proc file, the condition
    % has been removed, which is appropriate for direction of replay
    load('HP_replaydata_PFCallHist_proc_DecodeTraj_X6.mat','decodefilter');
    
    % Parameters if any
    % ------------------------------------------------------------------------------
    
    cnt=0; % How many PFC cells, separately in epochs
    cntalleps=0;
    %CA1
    CA1replay_arm_index1=[]; CA1replay_arm_index2=[];  curr_PFChist_Valid_epoch=[]; curr_PFCspks_Valid_epoch=[];
    curr_replay_arm_index1_Valid=[]; curr_replay_arm_index2_Valid=[];
    curr_DecodeTraj = []; CA1DecodeTraj = [];
    curr_dirTraj = []; CA1dirTraj=[];
    %PFC
    PFCidxs=[]; PFCtype=[]; PFChist_Valid=[]; PFCspks_Valid=[];
    
    % Go over epochs, and extract replay data and PFC resp
    for an = 1:length(decodefilter)
        for i=1:length(decodefilter(an).output{1})
            if ~isempty(decodefilter(an).output{1}(i).GlobalEpochIndex)
                cntalleps=cntalleps+1;
                % Variables for entire epoch
                GlobalEpochIndex(cntalleps,:) = decodefilter(an).output{1}(i).GlobalEpochIndex;
                curr_replay_arm_index1_Valid = decodefilter(an).output{1}(i).replay_arm_index1_Valid;
                curr_replay_arm_index2_Valid = decodefilter(an).output{1}(i).replay_arm_index2_Valid;
                curr_PFChist_Valid_epoch = decodefilter(an).output{1}(i).PFChist_Valid;
                curr_PFCspks_Valid_epoch = decodefilter(an).output{1}(i).PFCspks_Valid;
                curr_DecodeTraj = decodefilter(an).output{1}(i).GlobalDecodeTrajNorm;
                curr_dirTraj = decodefilter(an).output{1}(i).direction_Valid; % 0 = inbound, 1 = outbound
                
                curr_GlobalPFCindices = decodefilter(an).output{1}(i).GlobalPFCIndices;
                curr_PFCtype = decodefilter(an).output{1}(i).PFCtype;
                % Separate out by PFC cells - indices for cells in epoch
                currnPFCells = size(curr_GlobalPFCindices ,1);
                for c = 1:currnPFCells
                    cnt = cnt+1;
                    PFCidxs(cnt,:) = curr_GlobalPFCindices(c,:);
                    PFCtype(cnt) = curr_PFCtype(c);
                    if ~isempty(curr_PFChist_Valid_epoch)
                        PFChist_Valid{cnt} = curr_PFChist_Valid_epoch{c}; % For each cell, this is a matrix
                    else % No PFC spikes in epoch
                        PFChist_Valid{cnt} = zeros(length(curr_replay_arm_index1_Valid),101);
                    end
                    if ~isempty(curr_PFChist_Valid_epoch)
                        PFCspks_Valid{cnt} =  curr_PFCspks_Valid_epoch{c}; % For each cell, this is a cell array
                    else
                        PFCspks_Valid{cnt}=[];
                    end
                    % The following is same for entire epoch, but save separately for each cell anyway
                    CA1replay_arm_index1{cnt} =  curr_replay_arm_index1_Valid; % Same for entire epoch
                    CA1replay_arm_index2{cnt} =  curr_replay_arm_index2_Valid;
                    % DecodeTraj is also same for entire epoch, but save separately for each PFC cells
                    CA1DecodeTraj{cnt} = curr_DecodeTraj;
                    % Similar for direction of replay
                    CA1dirTraj{cnt} = curr_dirTraj; % vector of dirns for each replay event in epoch
                end
            end
        end % end i
    end %end animal
    
    
    
    
    
    % Copied from DFSsj_HPexpt_ripCA1popresp2
    
    % ----------
    % Combine across epochs - based on PFC idxs
    % ---------
    cntcells = 0;
    uniqueIndices = unique(PFCidxs(:,[1 2 4 5]),'rows'); % Collapse across epochs
    % iterating only over the unique indices and finding matches in allindexes
    
    for i=1:length(uniqueIndices)
        cntcells = cntcells+1;
        curridx = uniqueIndices(i,:);
        ind=find(ismember(PFCidxs(:,[1 2 4 5]),curridx,'rows'))';
        
        curr_replay_dirn=[]; %curr_replay_armidx2=[];
        curr_PFChist = [];curr_PFCspks=[]; curr_PFCtype=[];
        cntep=0;
        % PFC
        for r=ind
            try
                curr_replay_dirn = [curr_replay_dirn; CA1dirTraj{r}'];
            catch
                keyboard;
            end
            %curr_replay_armidx2 = [curr_replay_armidx2; CA1replay_arm_index2{r}'];
            curr_PFChist = [curr_PFChist; PFChist_Valid{r}];
            cntep=cntep+1;
            curr_PFCspks{cntep} = PFCspks_Valid{r};
            if isempty(curr_PFCtype)
                curr_PFCtype = PFCtype(r);
            end
        end
        
        % Save both in struct, and variable format
        allPFCidxs(cntcells,:) = curridx;
        allPFCtype(cntcells) = curr_PFCtype;
        allPFChist{cntcells} = curr_PFChist;
        allreplay_dirn{cntcells} = curr_replay_dirn;
        %allreplay_armidx2{cntcells} = curr_replay_armidx2;
        
        all_replayresp_PFC(cntcells).PFCidx = curridx;
        all_replayresp_PFC(cntcells).PFCtype = curr_PFCtype;
        all_replayresp_PFC(cntcells).PFChist = curr_PFChist;
        all_replayresp_PFC(cntcells).allreplay_dirn = curr_replay_dirn;
        %all_replayresp_PFC(cntcells).allreplay_armidx2 = curr_replay_armidx2;
    end
    
   
    % Divide PFC Resps based on Replay Direction and get RespPrefIndex:
    % Left = Inbound/ Right - Outbound
    for i=1:cntcells
        
        curr_PFCidx = allPFCidxs(i,:);
        curr_PFCtype = allPFCtype(i);
        curr_dirn = allreplay_dirn{i};
        %curr_armidx2 = allreplay_armidx2{i};
        curr_PFChist = allPFChist{i}; PFChistavg = mean(curr_PFChist,1);
        
        % Get a vector of responses and bckground to - to align with armindex
        % ---------------------------------------------------------------
        curr_PFCresp = mean((curr_PFChist(:,respidx)),2);
        curr_PFCbck = mean((curr_PFChist(:,bckidx)),2);
        curr_PFCmodln =  curr_PFCresp-curr_PFCbck;
        
        if ~isempty(curr_PFCresp) && ~isempty(curr_dirn)
            
            % Get Corrln between armindex and PFC resp
            % ------------------------------------------
            [r_dirnvsmodln,p_dirnvsmodln] = corr(curr_PFCmodln,curr_dirn);
            [r_dirnvsresp,p_dirnvsresp] = corr(curr_PFCresp,curr_dirn);
            
            % Divide Between Left/INBOUND and Right/OUTBOUND Arm Replays, and corresponding PFCresp
            % ---------------------
            leftpref = find(curr_dirn==0); curr_Nlefts=length(leftpref); % Inbound
            rightpref = find(curr_dirn==1); curr_Nrights=length(rightpref); % Outbound
            lefthist = curr_PFChist(leftpref,:); lefthistavg = mean(lefthist,1);
            righthist = curr_PFChist(rightpref,:); righthistavg = mean(righthist,1);
            % Left Resp
            curr_leftresp = mean((lefthist(:,respidx)),2); curr_leftresp_mean=mean(curr_leftresp);
            curr_leftbck = mean((lefthist(:,bckidx)),2); curr_leftbck_mean=mean(curr_leftbck);
            
            % 1) Raw Subtractive Modulation
            %curr_leftmodln =  curr_leftresp-curr_leftbck; curr_leftmodln_mean = mean(curr_leftmodln);
            % 2) Norm Subtractive Modln
            curr_leftmodln_mean =  (curr_leftresp_mean-curr_leftbck_mean)./curr_leftbck_mean;
            % 3) Divisive Modulation
            %curr_leftmodln_mean = curr_leftresp_mean./curr_leftbck_mean;
            
            % Right Resp
            curr_rightresp = mean((righthist(:,respidx)),2); curr_rightresp_mean=mean(curr_rightresp);
            curr_rightbck = mean((righthist(:,bckidx)),2); curr_rightbck_mean=mean(curr_rightbck);
            
            % 1) Raw Subtractive Modulation
            %curr_rightmodln = curr_rightresp_mean-curr_rightbck_mean; curr_rightmodln_mean = mean(curr_rightmodln);
            % 2) Norm Subtractive Modln
            curr_rightmodln_mean =  (curr_rightresp_mean-curr_rightbck_mean)./curr_rightbck_mean;
            % 3) Divisive Modulation
            %curr_rightmodln_mean = curr_rightresp_mean./curr_rightbck_mean;
            
            % Get an Index for PFCresp corresponding to dirnindex
            % ----------------------------------------------------
            if curr_Nlefts>=5 && curr_Nrights>=5
                curr_RespPrefIndex = (curr_leftresp_mean-curr_rightresp_mean)./ (curr_leftresp_mean+curr_rightresp_mean);
                curr_ModlnPrefIndex = (curr_leftmodln_mean-curr_rightmodln_mean)./ (curr_leftmodln_mean+curr_rightmodln_mean);
            else
                curr_RespPrefIndex = NaN;
                curr_ModlnPrefIndex = NaN;
            end
%             
%             curr_Nlefts, curr_Nrights
%             curr_RespPrefIndex, curr_ModlnPrefIndex
            
            % Store for current cell
            % -----------------------
            all_r_dirnvsmodln(i) = r_dirnvsmodln; all_p_dirnvsmodln(i) = p_dirnvsmodln;
            all_r_dirnvsresp(i) = r_dirnvsresp; all_p_dirnvsresp(i) = p_dirnvsresp;
            all_Nlefts(i) = curr_Nlefts; all_Nrights(i) = curr_Nrights;
            all_RespPrefIndex(i) = curr_RespPrefIndex; all_ModlnPrefIndex(i) = curr_ModlnPrefIndex;
        else     
            %keyboard;
            all_r_dirnvsmodln(i) = NaN; all_p_dirnvsmodln(i) = NaN;
            all_r_dirnvsresp(i) = NaN; all_p_dirnvsresp(i) = NaN;
            all_Nlefts(i) = curr_Nlefts; all_Nrights(i) = curr_Nrights;
            all_RespPrefIndex(i) = NaN; all_ModlnPrefIndex(i) = NaN;
        end
        
        % Plot for current cell
        % ---------------------
        if figcell
            
            % To control plotting
            if (curr_PFCidx(1)==1 && curr_PFCidx(2)==8 && curr_PFCidx(3)==17 && curr_PFCidx(4)==3)
            
                figure; hold on;
                
                PFChistavg = smoothvect(PFChistavg, g1); lefthistavg = smoothvect(lefthistavg, g1); righthistavg = smoothvect(righthistavg, g1);
                xaxis = -pret:binsize:postt;
                %plot(xaxis,mean(rip_spkshist_cellsort_PFC),'k-','Linewidth',4);
                
                title(sprintf('PFC Response to Replays'),'FontSize',24,'Fontweight','normal');
                ylabel('Firing Rate','FontSize',24,'Fontweight','normal');
                xlabel('Time','FontSize',24,'Fontweight','normal');
                plot(xaxis,PFChistavg,'k-','LineWidth',4);
                plot(xaxis,lefthistavg,'b-','LineWidth',2);
                plot(xaxis,righthistavg,'r-','LineWidth',2);
                legend('All','Inbound','Outbound')
                ypts = [0:0.01:max(PFChistavg)];
                xpts = 0*ones(size(ypts));
                plot(xpts , ypts, 'k--','Linewidth',1);
                %line([50 50],[0 max(PFChistavg)]);
                set(gca,'XLim',[-400 400]);
                
                keyboard;
            end
            
            %figure; hold on;
            %subplot(2,1,1); hold on; plot(curr_PFCresp,curr_dirn,'ro'); title('Resp'); xlabel('Resp'); ylabel('ArmIdx');
            %subplot(2,1,2); hold on; plot(curr_PFCmodln,curr_dirn,'ro'); title('Modln'); xlabel('Modln'); ylabel('ArmIdx');
            
        end
        
        %[r,p] = corrcoef(allPFCtrialResps{i}, allCA1PoplnResps{i});
        %allr(i) = r(1,2);
        %allp(i) = p(1,2);
        
    end % end cntcells
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data



% Get PFC classification based on allPFCidxs
excited = find(allPFCtype == 1);
inhibited = find(allPFCtype == -1);
neutral = find(allPFCtype == 0);

% Get PFC Properties File
% -----------------------
load('HP_PFC_DirnIdx_gather_X6.mat','all_DI_PFCidxs','all_DI_PFCDirnIdx');

cntcells=0;
for i=1:size(allPFCidxs,1),
    %ind=find(ismember(PFCidxs(:,[1 2 4 5]),curridx,'rows'))';
    curridx = allPFCidxs(i,:);
    match = rowfind(curridx, all_DI_PFCidxs);
    if match~=0
        if ~isnan(all_DI_PFCDirnIdx(match)) && (~isnan(all_RespPrefIndex(i)) || ~isnan(all_ModlnPrefIndex(i)))
            cntcells=cntcells+1;
            comb_idxs(cntcells,:) = curridx;
            comb_type(cntcells) = allPFCtype(i);
            comb_RespPrefIndex(cntcells) = all_RespPrefIndex(i);
            comb_ModlnPrefIndex(cntcells) = all_ModlnPrefIndex(i);
            comb_PFCDirnIdx(cntcells) = all_DI_PFCDirnIdx(match);
        end
    end
end

cntcells

% get rid of NaNs in RespIdx and DirnIdx, and edge cases - most likely due to insufficient data
rem1=find(isnan(comb_PFCDirnIdx)); rem2=find(isnan(comb_RespPrefIndex)); rem3 = find(abs(comb_RespPrefIndex)==1);
rem4=[];
%rem4 = find(abs(comb_ModlnPrefIndex)>5); % For Raw Subtractive Modulation

rem = [rem1,rem2,rem3,rem4];
comb_RespPrefIndex(rem)=[]; comb_PFCDirnIdx(rem)=[]; comb_ModlnPrefIndex(rem)=[];
comb_idxs(rem,:)=[]; comb_type(rem)=[];

exc = find(comb_type == 1); exc_idxs = comb_idxs(exc,:);
inh = find(comb_type == -1); inh_idxs = comb_idxs(inh,:);
neu = find(comb_type == 0); neu_idxs = comb_idxs(neu,:);

exc_RespPrefIdx = comb_RespPrefIndex(exc); exc_ModlnPrefIdx = comb_ModlnPrefIndex(exc); exc_DirnIdx = comb_PFCDirnIdx(exc); 
inh_RespPrefIdx = comb_RespPrefIndex(inh); inh_ModlnPrefIdx = comb_ModlnPrefIndex(inh); inh_DirnIdx = comb_PFCDirnIdx(inh); 
neu_RespPrefIdx = comb_RespPrefIndex(neu); neu_ModlnPrefIdx = comb_ModlnPrefIndex(neu); neu_DirnIdx = comb_PFCDirnIdx(neu); 


% Remove edge cases - most likely due to insufficient data
% rem = find(abs(exc_RespPrefIdx)==1);
% exc_RespPrefIdx(rem)=[]; exc_ModlnPrefIdx(rem)=[]; exc_DirnIdx(rem)=[];
% rem = find(abs(inh_RespPrefIdx)==1);
% inh_RespPrefIdx(rem)=[]; inh_ModlnPrefIdx(rem)=[]; inh_DirnIdx(rem)=[];
% rem = find(abs(neu_RespPrefIdx)==1);
% neu_RespPrefIdx(rem)=[]; neu_ModlnPrefIdx(rem)=[]; neu_DirnIdx(rem)=[];


[exc_r1,exc_p1] = corr(exc_RespPrefIdx',exc_DirnIdx'); [exc_r2,exc_p2] = corr(exc_ModlnPrefIdx',exc_DirnIdx');
[inh_r1,inh_p1] = corr(inh_RespPrefIdx',inh_DirnIdx'); [inh_r2,inh_p2] = corr(inh_ModlnPrefIdx',inh_DirnIdx');
[neu_r1,neu_p1] = corr(neu_RespPrefIdx',neu_DirnIdx'); [neu_r2,neu_p2] = corr(neu_ModlnPrefIdx',neu_DirnIdx');


% ------------
figure; hold on;redimscreen_2versubplots;
subplot(3,1,1); hold on;
title(sprintf('Excited Neurons: RespPrefIdx vs DirnIdx'),'FontSize',24,'Fontweight','normal');
ylabel('Dirn Pref Index','FontSize',24,'Fontweight','normal');
xlabel('Replay Resp Index','FontSize',24,'Fontweight','normal');
plot(exc_RespPrefIdx,exc_DirnIdx,'ro','MarkerSize',20,'LineWidth',2);
xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:0.4; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% Regression
[b00,bint00,r00,rint00,stats00] = regress(exc_DirnIdx', [ones(size(exc_RespPrefIdx')) exc_RespPrefIdx']);
xpts = min(exc_RespPrefIdx):0.01:max(exc_RespPrefIdx);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'r-','LineWidth',4); 
rsquare_exc = stats00(1), pregr_exc = stats00(3)

subplot(3,1,2); hold on;
title(sprintf('Inhibited Neurons: RespPrefIdx vs DirnIdx'),'FontSize',24,'Fontweight','normal');
ylabel('Dirn Pref Index','FontSize',24,'Fontweight','normal');
xlabel('Replay Resp Index','FontSize',24,'Fontweight','normal');
plot(inh_RespPrefIdx,inh_DirnIdx,'bo','MarkerSize',20,'LineWidth',2);
xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:0.4; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% Regression
[b00,bint00,r00,rint00,stats00] = regress(inh_DirnIdx', [ones(size(inh_RespPrefIdx')) inh_RespPrefIdx']);
xpts = min(inh_RespPrefIdx):0.01:max(inh_RespPrefIdx);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'b-','LineWidth',4); 
rsquare_inh = stats00(1), pregr_inh = stats00(3)

subplot(3,1,3); hold on;
title(sprintf('Neutral Neurons: RespPrefIdx vs DirnIdx'),'FontSize',24,'Fontweight','normal');
ylabel('Dirn Pref Index','FontSize',24,'Fontweight','normal');
xlabel('Replay Resp Index','FontSize',24,'Fontweight','normal');
plot(neu_RespPrefIdx,neu_DirnIdx,'go','MarkerSize',20,'LineWidth',2);
xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:1; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% Regression
[b00,bint00,r00,rint00,stats00] = regress(neu_DirnIdx', [ones(size(neu_RespPrefIdx')) neu_RespPrefIdx']);
xpts = min(neu_RespPrefIdx):0.01:max(neu_RespPrefIdx);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'g-','LineWidth',4); 
rsquare_neu = stats00(1), pregr_neu = stats00(3)


% ------------
figure; hold on;redimscreen_2versubplots;
subplot(2,1,1); hold on;
title(sprintf('Excited Neurons: RespPrefIdx vs DirnIdx'),'FontSize',24,'Fontweight','normal');
plot(exc_RespPrefIdx,exc_DirnIdx,'ro','MarkerSize',20,'LineWidth',2);
xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:0.4; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% Regression
[b00,bint00,r00,rint00,stats00] = regress(exc_DirnIdx', [ones(size(exc_RespPrefIdx')) exc_RespPrefIdx']);
xpts = min(exc_RespPrefIdx):0.01:max(exc_RespPrefIdx);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'r-','LineWidth',4); 
rsquare = stats00(1)

subplot(2,1,2); hold on;
title(sprintf('Excited Neurons: ModlnPrefIdx vs DirnIdx'),'FontSize',24,'Fontweight','normal');
plot(exc_ModlnPrefIdx,exc_DirnIdx,'ro','MarkerSize',20,'LineWidth',2);
xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:0.4; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% Regression
[b00,bint00,r00,rint00,stats00] = regress(exc_DirnIdx', [ones(size(exc_ModlnPrefIdx')) exc_ModlnPrefIdx']);
xpts = min(exc_RespPrefIdx):0.01:max(exc_ModlnPrefIdx);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'r-','LineWidth',4); 
rsquare = stats00(1)

% ------------
figure; hold on;redimscreen_2versubplots;
subplot(2,1,1); hold on;
title(sprintf('Inhibited Neurons: RespPrefIdx vs DirnIdx'),'FontSize',24,'Fontweight','normal');
plot(inh_RespPrefIdx,inh_DirnIdx,'bo','MarkerSize',20,'LineWidth',2);
xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:0.4; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% Regression
[b00,bint00,r00,rint00,stats00] = regress(inh_DirnIdx', [ones(size(inh_RespPrefIdx')) inh_RespPrefIdx']);
xpts = min(inh_RespPrefIdx):0.01:max(inh_RespPrefIdx);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'b-','LineWidth',4); 
rsquare = stats00(1)

subplot(2,1,2); hold on;
title(sprintf('Inhibited Neurons: ModlnPrefIdx vs DirnIdx'),'FontSize',24,'Fontweight','normal');
plot(inh_ModlnPrefIdx,inh_DirnIdx,'bo','MarkerSize',20,'LineWidth',2);
xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:0.4; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% Regression
[b00,bint00,r00,rint00,stats00] = regress(inh_DirnIdx', [ones(size(inh_ModlnPrefIdx')) inh_ModlnPrefIdx']);
xpts = min(inh_ModlnPrefIdx):0.01:max(inh_ModlnPrefIdx);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'b-','LineWidth',4); 
rsquare = stats00(1)

% ------------
figure; hold on;redimscreen_2versubplots;
subplot(2,1,1); hold on;
title(sprintf('Neutral Neurons: RespPrefIdx vs DirnIdx'),'FontSize',24,'Fontweight','normal');
plot(neu_RespPrefIdx,neu_DirnIdx,'go','MarkerSize',20,'LineWidth',2);
xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:1; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% Regression
[b00,bint00,r00,rint00,stats00] = regress(neu_DirnIdx', [ones(size(neu_RespPrefIdx')) neu_RespPrefIdx']);
xpts = min(neu_RespPrefIdx):0.01:max(neu_RespPrefIdx);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'g-','LineWidth',4); 
rsquare = stats00(1)

subplot(2,1,2); hold on;
title(sprintf('Neutral Neurons: ModlnPrefIdx vs DirnIdx'),'FontSize',24,'Fontweight','normal');
plot(neu_ModlnPrefIdx,neu_DirnIdx,'go','MarkerSize',20,'LineWidth',2);
xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:1; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% Regression
[b00,bint00,r00,rint00,stats00] = regress(neu_DirnIdx', [ones(size(neu_ModlnPrefIdx')) neu_ModlnPrefIdx']);
xpts = min(neu_ModlnPrefIdx):0.01:max(neu_ModlnPrefIdx);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'g-','LineWidth',4); 
rsquare = stats00(1)






% ----------------------------------------------------------------------------------




% % Plot Number Sig
% figure; hold on;redimscreen_figforppt1;
% subplot(2,1,1); hold on;
% title(sprintf('No. Of Neu With Sig Corrln: DirnIdx vs Modln'),'FontSize',24,'Fontweight','normal');
% bar(1,exc_Nsig_r1);
% bar(2,inh_Nsig_r1,'r');
% bar(3,neu_Nsig_r1,'k');
% subplot(2,1,2); hold on;
% title(sprintf('No. Of Neu With Sig Corrln: DirnIdx vs Resp'),'FontSize',24,'Fontweight','normal');
% bar(1,exc_Nsig_r2,'b');
% bar(2,inh_Nsig_r2,'r');
% bar(3,neu_Nsig_r2,'k');
% 
% 
% % Plot Popln "r"s
% figure; hold on;redimscreen_figforppt1;
% subplot(2,1,1); hold on;
% title(sprintf('Mean Corrln: DirnIdx vs Modln'),'FontSize',24,'Fontweight','normal');
% bar(1,mean(exc_r1),'b'); errorbar(1,mean(exc_r1),sem(exc_r1));
% bar(2,mean(inh_r1),'r'); errorbar(2,mean(inh_r1),sem(inh_r1));
% bar(3,mean(neu_r1),'k'); errorbar(3,mean(neu_r1),sem(neu_r1));
% set(gca,'XTickLabel',{'Exc','Inh','Neu'});
% subplot(2,1,2); hold on;
% title(sprintf('Abs Corrln: DirnIdx vs Modln'),'FontSize',24,'Fontweight','normal');
% bar(1,mean(abs(exc_r1)),'b'); errorbar(1,mean(abs(exc_r1)),sem(abs(exc_r1)));
% bar(2,mean(abs(inh_r1)),'r'); errorbar(2,mean(abs(inh_r1)),sem(abs(inh_r1)));
% bar(3,mean(abs(neu_r1)),'k'); errorbar(3,mean(abs(neu_r1)),sem(abs(neu_r1)));
% set(gca,'XTickLabel',{'Exc','Inh','Neu'});
% 
% 
% % Plot Popln "r"s
% figure; hold on;redimscreen_figforppt1;
% subplot(2,1,1); hold on;
% title(sprintf('Mean Corrln: DirnIdx vs Resp'),'FontSize',24,'Fontweight','normal');
% bar(1,mean(exc_r2),'b'); errorbar(1,mean(exc_r2),sem(exc_r2));
% bar(2,mean(inh_r2),'r'); errorbar(2,mean(inh_r2),sem(inh_r2));
% bar(3,mean(neu_r2),'k'); errorbar(3,mean(neu_r2),sem(neu_r2));
% set(gca,'XTickLabel',{'Exc','Inh','Neu'});
% subplot(2,1,2); hold on;
% title(sprintf('Abs Corrln: DirnIdx vs Resp'),'FontSize',24,'Fontweight','normal');
% bar(1,mean(abs(exc_r2)),'b'); errorbar(1,mean(abs(exc_r2)),sem(abs(exc_r2)));
% bar(2,mean(abs(inh_r2)),'r'); errorbar(2,mean(abs(inh_r2)),sem(abs(inh_r2)));
% bar(3,mean(abs(neu_r2)),'k'); errorbar(3,mean(abs(neu_r2)),sem(abs(neu_r2)));
% set(gca,'XTickLabel',{'Exc','Inh','Neu'});
% 






% ---------------------------------------------------------------------------







% bar(allNcandevents);
% title(sprintf(['No. of cand events for thrs ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
% xlabel(['Epoch No'],'FontSize',24,'Fontweight','normal');
% ylabel(['N cand events'],'FontSize',24,'Fontweight','normal');
% line([16 16], [0 150], 'Color','r'); line([32 32], [0 150], 'Color','r'); line([48 48], [0 150], 'Color','r');
% line([11 11], [0 150], 'Color','c'); line([27 27], [0 150], 'Color','c'); line([43 43], [0 150], 'Color','c');
% %text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');
%


% % Popln Resp
% % ---------
% rem = find(isnan(allr_days));
% allr_days(rem) = []; allp_days(rem) = [];
% sig= find(allp_days < 0.05);
%
% figure; hold on;redimscreen_figforppt1;
% plot(allr_days, 'k.','MarkerSize',24);
% plot(allr_days(sig), 'r.','MarkerSize',24);
% title(sprintf('CC - PFcPoplnResp and CA1PoplnResp'),'FontSize',24,'Fontweight','normal');
% ylabel(['Corr Coeff'],'FontSize',24,'Fontweight','normal');
% xlabel(['Day/Session no'],'FontSize',24,'Fontweight','normal');
% legend('All Cells','Sig Corr');
%
%
%
% % Skip NaNs
% rem = find(isnan(allr));
% allr(rem) = []; allp(rem) = [];
% sig= find(allp < 0.05);
%
% figure; hold on;redimscreen_figforppt1;
% plot(allr, 'k.','MarkerSize',24);
% plot(allr(sig), 'r.','MarkerSize',24);
% title(sprintf('CC - PFc tr and CA1 pr'),'FontSize',24,'Fontweight','normal');
% ylabel(['Corr Coeff'],'FontSize',24,'Fontweight','normal');
% xlabel(['PFC cell no'],'FontSize',24,'Fontweight','normal');
% legend('All Cells','Sig Corr');
% %text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');
%
%
%
%

