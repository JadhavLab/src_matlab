
% DFSsj_HPexpt_savecandripples - Get cand replay events similar to getpopulationevents, and save in structure
% eg. HPacandripples01

% Replay decoding, simlar to mcarr/runrippledecoding or mkarlsso/populationdecoding_forloren

clear; %close all;
runscript = 0;
savedata = 0; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells

savedir = '/data25/sjadhav/HPExpt/ProcessedData/';
%val=1; cellcountthresh=5; savefile = [savedir 'HP_cand5ripples'];
%val=2; cellcountthresh=4; savefile = [savedir 'HP_cand4ripples'];
%%%val=2; cellcountthresh=4; savefile = [savedir 'HP_replaydecode4'];
val=3; cellcountthresh=4; savefile = [savedir 'HP_candfripples'];

savefig1=0;


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Ndl'};
    %animals = {'HPa'};
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    % dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    epochfilter = runepochfilter;
    %sleepepochfilter = 'isequal($type, ''sleep'')'; % Only pre and post sleep marked as sleep
    
    % %Cell filter
    % %-----------
    % %PFC
    % %----
    % && strcmp($thetamodtag, ''y'')
    
    % All CA1 cells. Function will parse them out.
    cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7)) ';
    
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % Iterator
    % --------
    iterator = 'multicellanal';
    
    
    % %RUN DECODING FILTER FOR RIPPLES DURING RUNS
    % -------------------------
    
    % Timefilter for ripple in function. Can use a speed filter as well.
    
    % Filter creation
    % ----------------
    decodefilter = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter, 'iterator', iterator);
    
    disp('Done Decode Filter Creation');
    
    % Set analysis function
    % ---------------------
     switch val
        case {1,2}
            decodefilter = setfilterfunction(decodefilter,'sj_getpopulationevents2_forsave',{'spikes', 'linpos', 'pos', 'ripples','tetinfo'},'cellcountthresh',cellcountthresh,'dospeed',1,'lowsp_thrs',4,'minrip',2,'minstd',3);
        case 3
            val
            decodefilter = setfilterfunction(decodefilter,'sj_getpopulationevents2_forsave',{'spikes', 'linpos', 'pos', 'ripples','tetinfo'},'cellcountthresh',cellcountthresh,'dospeed',1,'lowsp_thrs',4,'minrip',1,'minstd',3);
     end
            % Default stdev for ripples is 3. You will use tetinfo to get ripples
    
    % Run analysis
    % ------------
    decodefilter = runfilter(decodefilter);
    disp('Finished running decode filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript savedata
        save(savefile, '-v7.3');
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end



% -------------------------  Filter Format Done -------------------------


if 0  % Save data structure
    
    for an = 1:length(decodefilter)
        curranim = an;
        %prefix='HPc'; currsavedir = '/data25/sjadhav/HPExpt/HPc_direct/';
        switch curranim
            case 1
                prefix='HPa', currsavedir = '/data25/sjadhav/HPExpt/HPa_direct/';
                
            case 2
                prefix='HPb', currsavedir = '/data25/sjadhav/HPExpt/HPb_direct/';
                
            case 3
                prefix='HPc', currsavedir = '/data25/sjadhav/HPExpt/HPc_direct/';
                
            case 4
                prefix='Ndl', currsavedir = '/data25/sjadhav/HPExpt/Ndl_direct/';
        end
        
        curr_dayeps = decodefilter(an).epochs{1};
        dayvector = curr_dayeps(:,1);
        uniquedays = unique(dayvector);
        
        for d=1:length(uniquedays)
            cand5ripples=[];
            currday = uniquedays(d)
            
            epind = find(dayvector==currday);
            for ep = 1:length(epind)
                currepoch = curr_dayeps(epind(ep),2);
                cmd=sprintf('candfripples{%d}{%d} = decodefilter(an).output{1}(epind(ep));',currday, currepoch);
                eval(cmd);
            end
            
            % Save for current day
            currsavefile = sprintf('%scandfripples%02d',prefix,currday);
            currsavefile = [currsavedir currsavefile]
            save(currsavefile,'candfripples');
        end
        
    end
end

    
    



