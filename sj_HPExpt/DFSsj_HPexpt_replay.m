

% Replay decoding, simlar to mcarr/runrippledecoding or mkarlsso/populationdecoding_forloren

clear; %close all;
runscript = 1;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells

savedir = '/data25/sjadhav/HPExpt/ProcessedData/';


% With Speed criterion - Version 4 onwards
%--------------------------------------
%val=1; cellcountthresh=5; savefile = [savedir 'HP_replay5decode'];
%val=2; cellcountthresh=4; savefile = [savedir 'HP_replay4decode'];

% No speed criterion - Original
% -------------------------------
val=3; cellcountthresh=4; savefile = [savedir 'HP_replaydecode'];


savefig1=0;


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Ndl'};
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    % dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    epochfilter = runepochfilter;
    %sleepepochfilter = 'isequal($type, ''sleep'')'; % Only pre and post sleep marked as sleep
    
    % %Cell filter
    % %-----------
    % %PFC
    % %----
    % && strcmp($thetamodtag, ''y'')
    
    % All CA1 cells. Function will parse them out.
    cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7)) ';
    
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % Iterator
    % --------
    iterator = 'multicellanal';
    
    
    %% RUN TRAINING FILTER
    % -------------------
    timefilter_place = { {'DFTFsj_getvelpos', '(($absvel >= 5))'},...
        {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',3} };
    
    %create training data by calulating the linearized rates of all cells
    trainingfilter = createfilter('animal',animals,'epochs',epochfilter,'cells',cellfilter,'excludetime', timefilter_place,'iterator',iterator);
    
    disp('Done Training Filter Creation');
    
    %Options set: bin size = 2 cm, peakthresh = 3Hz / std above the mean
    trainingfilter = setfilterfunction(trainingfilter, 'sj_calcpopulationlinfields', {'spikes','linpos'},2,3); 
    % Could also just pass linfields instead of calculating again, but need statematrix info from linpos as well
    trainingfilter = runfilter(trainingfilter);
    
    disp('Finished running training filter script');
    
    %% RUN DECODING FILTER FOR RIPPLES DURING RUNS
    % -------------------------
    
    % Timefilter for ripple in function. Can use a speed filter as well.
    
    % Filter creation
    % ----------------
    decodefilter = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter, 'iterator', iterator);
    
    disp('Done Decode Filter Creation');
    
    % Set analysis function
    % ---------------------
    
    switch val
        case {1,2}
            decodefilter = setfilterfunction(decodefilter,'sj_getpopulationevents2',{'spikes', 'linpos', 'pos', 'ripples','tetinfo'},'cellcountthresh',cellcountthresh,'dospeed',1,'lowsp_thrs',4,'minrip',2,'minstd',3);
        case 3
             decodefilter = setfilterfunction(decodefilter,'sj_getpopulationevents2',{'spikes', 'linpos', 'pos', 'ripples','tetinfo'},'cellcountthresh',cellcountthresh,'dospeed',0,'minrip',1,'minstd',3);
    end           
            % Default stdev for ripples is 3. You will use tetinfo to get ripples
    
    % Run analysis
    % ------------
    decodefilter = runfilter(decodefilter);
    disp('Finished running decode filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript savedata
        save(savefile, '-v7.3');
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end



% -------------------------  Filter Format Done -------------------------

%% DETERMINE WHICH EVENTS ARE REPLAY EVENTS


% SJ - training filter epochs and decodefilter epochs are the same for me


% %Run only for some epochs with this option. eg. HPa day 1 both epochs. an=1; e=1:2
% %-------------------------------------
% an=1; usee=1:2; d=1;
% for ee=1:length(usee)
%     e=usee(ee)
%     decode_dayep = decodefilter(an).epochs{d}(e,:)
%     training_e = e; % SJ - training filter epochs and decodefilter epochs are the same for me
%      [out, out_decodedata]= sj_calcepochreplaystats([an d e], [1 training_e], trainingfilter, decodefilter);
%      if ~isempty(out)
%          ind = zeros(length(decodefilter(an).output{d}(e).eventtraj),1);
%          eventind = out(:,7);
%          decodefilter(an).output{d}(e).pvalue = NaN(size(ind));
%          decodefilter(an).output{d}(e).pvalue(eventind) = out(:,4); % pvalues of replays in cand events
%          decodefilter(an).output{d}(e).rvalue = NaN(size(ind));
%          decodefilter(an).output{d}(e).rvalue(eventind) = out(:,2); % rvalues of replays in cand events
%          decodefilter(an).output{d}(e).entropy = NaN(size(ind));
%          decodefilter(an).output{d}(e).entropy(eventind) = out(:,3); % entropy of replays in cand events
%          decodefilter(an).output{d}(e).ncells = NaN(size(ind));
%          decodefilter(an).output{d}(e).ncells(eventind) = out(:,5); % candidate event index
%          decodefilter(an).output{d}(e).replaylength = NaN(size(ind));
%          decodefilter(an).output{d}(e).replaylength(eventind) = out(:,6); % in timebins - lth = timebins*15ms
%          decodefilter(an).output{d}(e).decodedata = out_decodedata; % For each eventindex, there is a decodedata vecto       
%      end
% end
% 
% savefile = [savedir 'HPa_day1_replay5decode_gather'];
% clear figopt1 runscript savedata
% save(savefile, '-v7.3');
% keyboard;




%Run for entire data
%-------------------
% for an = 1:length(decodefilter)
%     for d = 1:length(decodefilter(an).output)
%         for e = 1:length(decodefilter(an).output{d})
%             
%             disp(sprintf('Anim %d Day %d Epoch %d', an, d, e));
%             decode_dayep = decodefilter(an).epochs{d}(e,:)
% 
%             %Determine which training index to use
%             if ~isempty(decodefilter(an).output{d}(e).index)               
%                 decode_dayep = decodefilter(an).epochs{d}(e,:);
%                 training_e = e; % SJ - training filter epochs and decodefilter epochs are the same for me
%                 %decode_dayep = decodefilter(an).output{d}(e).index(1,1:2); % Get dayep for current iteration
%                 %training_e = find(decode_dayep==trainingfilter(an).epochs{1});
%                 if ~isempty(training_e)
%                     [out, out_decodedata]= sj_calcepochreplaystats([an d e], [1 training_e], trainingfilter, decodefilter);
% ERROR  ERROR  ERROR  ERROR  ERROR  ERROR - IN OLD CODE. SHOULD BE [D TRAINING_E ABOVE]
%                     if ~isempty(out)
%                         ind = zeros(length(decodefilter(an).output{d}(e).eventtraj),1);
%                         eventind = out(:,7);
%                         decodefilter(an).output{d}(e).pvalue = NaN(size(ind));
%                         decodefilter(an).output{d}(e).pvalue(eventind) = out(:,4); % pvalues of replays in cand events
%                         decodefilter(an).output{d}(e).rvalue = NaN(size(ind));
%                         decodefilter(an).output{d}(e).rvalue(eventind) = out(:,2); % rvalues of replays in cand events
%                         decodefilter(an).output{d}(e).entropy = NaN(size(ind));
%                         decodefilter(an).output{d}(e).entropy(eventind) = out(:,3); % entropy of replays in cand events
%                         decodefilter(an).output{d}(e).ncells = NaN(size(ind));
%                         decodefilter(an).output{d}(e).ncells(eventind) = out(:,5); % candidate event index
%                         decodefilter(an).output{d}(e).replaylength = NaN(size(ind));
%                         decodefilter(an).output{d}(e).replaylength(eventind) = out(:,6); % in timebins - lth = timebins*15ms
%                         decodefilter(an).output{d}(e).decodedata = out_decodedata; % For each eventindex, there is a decodedata vector                      
%                         %In "HP_replaydecodetmp", 
%                         %pvalue was out:3, rvalue was out:2, ncells was out:4, eventindex was out:5                        
%                         %didn't have entropy, and didn't save decodedata?
%                         
%                     end
%                 end
%             end
%         end
%     end
% end
% savefile = [savedir 'HP_replay5decode_gather'];
% clear figopt1 runscript savedata
% save(savefile, '-v7.3');




keyboard;












% % ----------------------------------
% % Whether to gather data or to load previously gathered data
% % --------------------------------------------------------------------
% gatherdata = 1; savegatherdata = 1;
% switch val
%     case 1
%         gatherdatafile = [savedir 'HP_replaydecode_gather'];
%     case 2
%         gatherdatafile = [savedir 'HP_candevents4_gather'];
% end
% 
% 
% 
% 
% 
% 
% if gatherdata
%     % Parameters if any
%     % -----------------
%     % -------------------------------------------------------------
%     
%     cnt=0; % counting epochs
%     anim_index=[]; allNcandevents=[]; allNcounts=[]; allNcountsvec = [];
%     
%     
%     
%     for an = 1:length(modf)
%         for i=1:length(modf(an).output{1})
%             
%             cnt=cnt+1;
%             
%             % Variables for entire epoch
%             anim_index{an}{i} = modf(an).output{1}(i).index;
%             allNcandevents(cnt) = modf(an).output{1}(i).Ncandevents; % How many cand events in epoch
%             allNcounts{cnt} = modf(an).output{1}(i).Ncounts; % No of active cells in each epoch
%             allNcountsvec = [allNcountsvec, modf(an).output{1}(i).Ncounts];
%         end
%     end
%     
%     
%     % Save
%     % -----
%     if savegatherdata == 1
%         save(gatherdatafile);
%     end
%     
% else % gatherdata=0
%     
%     load(gatherdatafile);
%     
% end % end gather data
% 
% 
% % ------------
% % PLOTTING, ETC
% % ------------
% 
% % ------------------
% % Population Figures
% % ------------------
% 
% forppr = 0;
% % If yes, everything set to redimscreen_figforppr1
% % If not, everything set to redimscreen_figforppt1
% figdir = '/data25/sjadhav/HPExpt/Figures/ThetaMod/';
% summdir = figdir;
% set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
% 
% if forppr==1
%     set(0,'defaultaxesfontsize',16);
%     tfont = 18; % title font
%     xfont = 16;
%     yfont = 16;
% else
%     set(0,'defaultaxesfontsize',24);
%     tfont = 28;
%     xfont = 20;
%     yfont = 20;
% end
% 
% 
% figure; hold on;redimscreen_figforppt1;
% bar(allNcandevents);
% title(sprintf(['No. of cand events for thrs ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
% xlabel(['Epoch No'],'FontSize',24,'Fontweight','normal');
% ylabel(['N cand events'],'FontSize',24,'Fontweight','normal');
% line([16 16], [0 150], 'Color','r'); line([32 32], [0 150], 'Color','r'); line([48 48], [0 150], 'Color','r');
% line([11 11], [0 150], 'Color','c'); line([27 27], [0 150], 'Color','c'); line([43 43], [0 150], 'Color','c');
% %text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');
% 
% figure; hold on;redimscreen_figforppt1;
% vec = 0:1:20;
% distr = histc(allNcountsvec,vec);
% bar(vec, distr, 'r');
% title(sprintf(['Distr No. of cells in events ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
% xlabel(['No. of cells'],'FontSize',24,'Fontweight','normal');
% ylabel(['Hist'],'FontSize',24,'Fontweight','normal');
% 
% 
% keyboard;
% 
% 
% 
