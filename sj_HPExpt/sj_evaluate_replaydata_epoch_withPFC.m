function [decodefilter]= sj_evaluate_replaydata_epoch_withPFC(index, trainingindex, trainingfilter, PFCtrainingfilter, decodefilter,varargin)

warning('OFF','stats:regress:RankDefDesignMat');

dostats = 0;
cellthresh = 2;
sigthresh = 4;


animal = index(1); an = animal;
dd = index(2);
epoch = index(3);
trajmapping = [1 1 2 2];
binsize = .015; %default temporal bin
out = [];
out_decodedata = [];
for option = 1:2:length(varargin)-1
    if isstr(varargin{option})
        switch(varargin{option})
            case 'binsize'
                binsize = varargin{option+1};
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end
    else
        error('Options must be strings, followed by the variable');
    end
end


% Need to initialize decodefilter output, for events that do not fit criteria
ind = zeros(length(decodefilter(an).output{dd}(epoch).eventtraj),1);
replay_arm_index1 = NaN(size(ind));
replay_arm_index2 = NaN(size(ind));
decodefilter(an).output{dd}(epoch).pvalue = NaN(size(ind));
decodefilter(an).output{dd}(epoch).rvalue = NaN(size(ind));
decodefilter(an).output{dd}(epoch).slope = NaN(size(ind));
decodefilter(an).output{dd}(epoch).entropy = NaN(size(ind));
decodefilter(an).output{dd}(epoch).direction = NaN(size(ind));
decodefilter(an).output{dd}(epoch).ncells = NaN(size(ind));
decodefilter(an).output{dd}(epoch).replaylength = NaN(size(ind));
decodefilter(an).output{dd}(epoch).decodedata = [];
decodefilter(an).output{dd}(epoch).replay_arm_index1 = NaN(size(ind));
decodefilter(an).output{dd}(epoch).replay_arm_index2 = NaN(size(ind));
decodefilter(an).output{dd}(epoch).probdata = [];
decodefilter(an).output{dd}(epoch).xdata = [];
decodefilter(an).output{dd}(epoch).activepeakpos = [];

%PFC values
decodefilter(an).output{dd}(epoch).PFCpvalue = NaN(size(ind));
decodefilter(an).output{dd}(epoch).PFCrvalue = NaN(size(ind));
decodefilter(an).output{dd}(epoch).PFCslope = NaN(size(ind));
decodefilter(an).output{dd}(epoch).PFCentropy = NaN(size(ind));
decodefilter(an).output{dd}(epoch).PFCdirection = NaN(size(ind));
decodefilter(an).output{dd}(epoch).PFCncells = NaN(size(ind));
decodefilter(an).output{dd}(epoch).PFCreplaylength = NaN(size(ind));
decodefilter(an).output{dd}(epoch).PFCdecodedata = [];
decodefilter(an).output{dd}(epoch).PFCreplay_arm_index1 = NaN(size(ind));
decodefilter(an).output{dd}(epoch).PFCreplay_arm_index2 = NaN(size(ind));
decodefilter(an).output{dd}(epoch).PFCprobdata = [];
decodefilter(an).output{dd}(epoch).PFCxdata = [];

cp = trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).homesegmentlength; % Choice Point Location
cp_idx = round(cp/2);
decodefilter(animal).output{dd}(epoch).cp_idx = cp_idx;
decodefilter(animal).output{dd}(epoch).cp = cp;

for eventindex = 1:length(decodefilter(animal).output{dd}(epoch).eventdata)
    
    %disp(eventindex);
    
    % ------
    % Do CA1
    % ------
    trainingdata = [];
    spikedata = [];
    decodedata = [];
    indexlist = [];
    activespiketimes = [];
    activerates = [];
    
    %pick out all the matching cells from the training data and the
    %decoding data
    %traindata contains linear rates, and is n by x, where n is the
    %number of cells and x is the number of spatial bins
    %spikedata contains spikecounts, and is n by t, where t is the
    %number of temporal bins in the data to be decoded.
    matches = rowfind(trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).index(:,[1 3 4]),decodefilter(animal).output{dd}(epoch).index(:,[1 3 4])); %find the matching cell indices
    startevent = decodefilter(animal).output{dd}(epoch).eventtime(eventindex,1);
    endevent = decodefilter(animal).output{dd}(epoch).eventtime(eventindex,2);
    
    
    cellcount = decodefilter(animal).output{dd}(epoch).eventdata(eventindex).cellcount;
    traj_cellSeq = trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).cellSequence; % cell seq for the 4 trajectories
    cellpeakpos = trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).cellpeakpos;
    
    % This needs to be cross-checked, since some cells might be skipped in training filter.
    cellSeq = decodefilter(animal).output{dd}(epoch).eventdata(eventindex).cellSeq; % cell sequence in the SWR
    
    
    %if ((endevent-startevent) < 2) % length of reply < 2 sec - Dont need this
    timebins = startevent:binsize:endevent;
    eventcellsactive = [];
    activecount = 0;
    activepeakpos = [];
    for trainingcell = 1:length(matches)
        if (matches(trainingcell) > 0) %we have a match
            indexlist = [indexlist; trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).index(trainingcell,:)];
            trainingdata = [trainingdata; trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).rates(trainingcell,:)];
            currpeakpos = cellpeakpos(trainingcell,:);
            
            tmpspiketimes = decodefilter(animal).output{dd}(epoch).eventdata(eventindex).spiketimes(find(decodefilter(animal).output{dd}(epoch).eventdata(eventindex).cellindex == matches(trainingcell)));
            %save all the info for the active cells
            if ~isempty(tmpspiketimes)
                activecount = activecount+1;
                activespiketimes{activecount} = tmpspiketimes;
                activerates = [activerates; trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).rates(trainingcell,:)];
                activepeakpos = [activepeakpos; currpeakpos];
            end
            
        end
    end
    trainingdata = trainingdata*binsize; %transform rates to expected number of spikes
    activerates = activerates*binsize;
    
    % ---------------------------------------------------------
    % Collapse Decoding Part of  sj_calcReplayStats in here
    % ----------------------------------------------------------
    distvector = trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).dist;
    expSpikeCounts = activerates;
    exponentmatrix = exp(-expSpikeCounts);
    cellsactive = [];
    totalsamples = 10000;
    if length(activespiketimes)>=cellthresh  % >=2
        for i = 1:length(activespiketimes)
            spikebins = lookup(activespiketimes{i},timebins);
            spikecount = zeros(1,length(timebins));
            for j = 1:length(spikebins)
                spikecount(spikebins(j)) = spikecount(spikebins(j))+1;
            end
            spikedata = [spikedata; spikecount];
            cellsactive = [cellsactive; (spikecount > 0)];
        end
        
        %the decoded data contains spatial probabilities, and is x by t
        decodedata = zeros(size(expSpikeCounts,2),size(spikedata,2));
        naninds = find(isnan(expSpikeCounts(1,:)));
        for t = 1:size(spikedata,2) %time
            Tspikecount = repmat(spikedata(:,t),1,size(expSpikeCounts,2));
            %calculate P(spikecount|x) for this timebin across all cells and all x
            spatialprob = prod(((expSpikeCounts.^Tspikecount)./factorial(Tspikecount)).*exp(-expSpikeCounts),1)';
            spatialprob(naninds) = 0;
            spatialprob = spatialprob/sum(spatialprob);  %normalize across space
            %spatialprob(find(spatialprob < max(spatialprob/2))) = 0;
            decodedata(:,t) = spatialprob;
        end
        
        totalspikecounts = sum(spikedata,1);
        totalcellsactive = sum(cellsactive,1);
        nonzerobins = find(totalspikecounts > 0);
        
        rvalues = [];
        slopes = [];
        entropy = [];
        for rloop = 1:10
            tBinPicks = distsample(totalsamples,totalspikecounts);
            regressdata = [];
            for i = 1:length(nonzerobins)
                if (totalspikecounts(nonzerobins(i)) > 0)
                    tmpnumsamples = sum(tBinPicks == nonzerobins(i));
                    distpicks = distvector(distsample(tmpnumsamples,decodedata(:,nonzerobins(i))))';
                    entropy_loop(i) = -nansum((hist(distpicks,0:5:200)./length(distpicks)).*log(hist(distpicks,0:5:200)./length(distpicks)));
                    
                    %distpicks(:,2) = timebins(nonzerobins(i));
                    distpicks(:,2) = i;
                    regressdata = [regressdata; distpicks];
                end
            end
            regressdata(:,3) = 1;
            [b,bint,r,rint,stats] = regress(regressdata(:,1),regressdata(:,2:3));
            rvalues = [rvalues; stats(1)];
            slopes = [slopes; b(1)];
            entropy = [entropy; mean(entropy_loop)];
        end
        
        % Save rvalue, slope and entropy for current event
        decodefilter(animal).output{dd}(epoch).rvalue(eventindex) = mean(rvalues);
        decodefilter(animal).output{dd}(epoch).slope(eventindex) = mean(slopes);
        decodefilter(animal).output{dd}(epoch).entropy(eventindex) = mean(entropy);
        % Mark inbound = 0, outbound = 1
        if mean(slopes) < 0,
            direction = 0;  % inbound
        else
            direction = 1;  % outbound
        end
        decodefilter(animal).output{dd}(epoch).direction(eventindex) = direction;
        % Save other things for current event
        decodefilter(animal).output{dd}(epoch).ncells(eventindex) = length(activespiketimes);
        decodefilter(animal).output{dd}(epoch).replaylength(eventindex) = length(timebins);
        decodefilter(animal).output{dd}(epoch).decodedata{eventindex} = decodedata;
        
        
        % Now Evaluate the Decoded Data to See which Trajectory it Most Resembles
        % -------------------------------------------------------------------------
        activepeakdist = 2*max(activepeakpos,[],2); % bizsize for posn is 2cm. Get max of all 4 traj for cells
        if any(activepeakdist>cp) % Need at least 1 cell past choicepoint, then continue
            % Parse decode data
            trajmapping = [1 1 2 2]; xdata = {[],[]}; ydata = {[],[]};
            probdata = {}; probdata = cell(2,1);
            
            %combine the outbound and inbound trajectories
            for traj = 1:4;
                trajindex = find(trainingfilter(animal).output{dd}(epoch).traj == traj);
                xdata{trajmapping(traj)} = trainingfilter(animal).output{dd}(epoch).dist(trajindex);
                ydata{trajmapping(traj)} = stack(ydata{trajmapping(traj)}, decodedata(trajindex,1)');
                if isempty(probdata{trajmapping(traj)})
                    probdata{trajmapping(traj)} = decodedata(trajindex,:);
                else
                    probdata{trajmapping(traj)} = probdata{trajmapping(traj)} + decodedata(trajindex,:);
                end
            end
            
            % Which trajectory is replayed.
            
            
            % One way: get area under curve for all probabilities>Choice Point
            area1 = sum(sum(probdata{1}(cp_idx+1:end,:))); % Traj1 is Left arm, our view
            area2 = sum(sum(probdata{2}(cp_idx+1:end,:))); % Traj2 is Right arm, our view
            replay_arm_index1 = (area1-area2)./(area1+area2);
            decodefilter(animal).output{dd}(epoch).replay_arm_index1(eventindex) = replay_arm_index1;
            decodefilter(animal).output{dd}(epoch).probdata{eventindex} = probdata;
            decodefilter(animal).output{dd}(epoch).xdata{eventindex} = xdata;
            decodefilter(animal).output{dd}(epoch).activepeakpos{eventindex} = activepeakpos;
            
            % Second way - Get the one with higher mean probability - entire trajectory.
            % But this includes central arm data. If these cells fire on both trajectories, as they usually do, they should be skipped
            % This will be valis if cells are "splitter" cells on central arm
            %traj1 = mean(mean(probdata{1}))>=mean(mean(probdata{2}));
            %if traj1; traj = 1; else traj = 2; end
            area1 = sum(sum(probdata{1})); % Traj1 is Left arm, our view
            area2 = sum(sum(probdata{2})); % Traj2 is Right arm, our view
            replay_arm_index2 = (area1-area2)./(area1+area2);
            decodefilter(animal).output{dd}(epoch).replay_arm_index2(eventindex) = replay_arm_index2;
        end
        
        % Calc significance for CA1: pvalue for events with at least sigthresh=4/5 events
        % Collapse from sj_calcReplayStats
        % -----------------------------------------------
        if (length(activespiketimes) >= sigthresh) && dostats == 1
            %tmpout= sj_calcReplayStats(activespiketimes,activerates,timebins,trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).dist);
            %tmpoutstats = tmpout.stats;
            %out = [out; [tmpoutstats length(activespiketimes) length(timebins) eventindex]];
            %out_decodedata{eventindex} = tmpout.decodedata;
            
            disp('Doing Stats');
            
            scrambleddata = [];
            permbins = nonzerobins;
            for iteration = 1:200
                %the decoded data contains spatial probabilities, and is x by t
                
                %permindex = randperm(size(expSpikeCounts,1));
                permindex = 1:size(expSpikeCounts,1);
                
                permbins = permbins(randperm(length(permbins)));
                
                tmpexpSpikeCounts = expSpikeCounts(permindex,:);
                tmpexponentmatrix = exponentmatrix(permindex,:);
                decodedata2 = zeros(size(expSpikeCounts,2),length(permbins));
                
                
                for t = 1:length(permbins) %time
                    Tspikecount = repmat(spikedata(:,permbins(t)),1,size(expSpikeCounts,2));
                    factorialmatrix = repmat(factorial(spikedata(:,permbins(t))),1,size(expSpikeCounts,2));
                    
                    %calculate P(spikecount|x) for this timebin across all cells and all x
                    spatialprob = prod(((tmpexpSpikeCounts.^Tspikecount)./factorialmatrix).*tmpexponentmatrix,1)';
                    spatialprob(naninds) = 0;
                    spatialprob = spatialprob/sum(spatialprob);  %normalize across space
                    %spatialprob(find(spatialprob < max(spatialprob/2))) = 0;
                    decodedata2(:,t) = spatialprob;
                end
                
                
                regressdata = [];
                tBinPicks = distsample(totalsamples,totalspikecounts);
                for i = 1:length(permbins)
                    if (totalspikecounts(nonzerobins(i)) > 0)
                        tmpnumsamples = sum(tBinPicks == nonzerobins(i));
                        distpicks = distvector(distsample(tmpnumsamples,decodedata2(:,i)))';
                        %distpicks(:,2) = timebins(nonzerobins(i));
                        distpicks(:,2) = i;
                        regressdata = [regressdata; distpicks];
                    end
                end
                regressdata(:,3) = 1;
                [b,bint,r,rint,tmpstats] = regress(regressdata(:,1),regressdata(:,2:3));
                scrambleddata = [scrambleddata; tmpstats(1)];
                
                if (iteration == 100) %early break for cells that are clearly not significant
                    tmpresult = [];
                    for i = 1:length(rvalues)
                        tmpresult(i) = sum(rvalues(i) < scrambleddata)/length(scrambleddata);
                    end
                    if (mean(tmpresult) > .2)
                        break
                    else %do more of the initial regressions
                        for rloop = 1:490
                            tBinPicks = distsample(totalsamples,totalspikecounts);
                            regressdata = [];
                            for i = 1:length(nonzerobins)
                                if (totalspikecounts(nonzerobins(i)) > 0)
                                    tmpnumsamples = sum(tBinPicks == nonzerobins(i));
                                    distpicks = distvector(distsample(tmpnumsamples,decodedata(:,nonzerobins(i))))';
                                    %distpicks(:,2) = timebins(nonzerobins(i));
                                    distpicks(:,2) = i;
                                    regressdata = [regressdata; distpicks];
                                end
                            end
                            regressdata(:,3) = 1;
                            [b,bint,r,rint,stats] = regress(regressdata(:,1),regressdata(:,2:3));
                            rvalues = [rvalues; stats(1)];
                            slopes = [slopes; b(2)];
                        end
                    end
                end
            end
            
            outvector = [];
            for i = 1:length(rvalues)
                outvector(i) = sum(rvalues(i) < scrambleddata)/length(scrambleddata);
            end
            
            decodefilter(animal).output{dd}(epoch).pvalue(eventindex) = mean(outvector);
            
            %out = [out; calcReplayStats(activespiketimes,activerates,timebins,trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).dist)];
            %out = [out; [calcReplayStats(activespiketimes,activerates,timebins,trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).dist) decodefilter(animal).output{dd}(epoch).eventimmobiletime(eventindex) length(activespiketimes) ]];  %decodefilter(animal).output{1}(epochnum).std(eventindex)
            %out = [out; [decodefilter(animal).output{dd}(epoch).eventimmobiletime(eventindex) length(activespiketimes) ]];  %decodefilter(animal).output{dd}(epoch).std(eventindex)
            %out = [out;index(1)];
            %out = [out; [calcReplayStats(activespiketimes,activerates,timebins,trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).dist) length(activespiketimes) eventindex]];
            
        end % if ncells>sigthresh
        %end
        
        
        % -----------------------------------------
        % Do PFC within this condition of CA1cellthresh>2. Otherwise, dont bother
        % -------------------------------------------
        ptrainingdata = [];
        pspikedata = [];
        pdecodedata = [];
        pindexlist = [];
        pactivespiketimes = [];
        pactiverates = [];
        
        
        % if no valid PFC cell in last epoch(s), then the filter wont have any entries
        PFC_Nepochs = length(PFCtrainingfilter(1).output{1});
        if trainingindex(2)<=PFC_Nepochs
            
            % any valid PFC cell in epoch at all?
            if (~isempty(PFCtrainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).index) && ~isempty(decodefilter(animal).output{dd}(epoch).pindex))
                
                matches = rowfind(PFCtrainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).index(:,[1 3 4]),decodefilter(animal).output{dd}(epoch).pindex(:,[1 3 4])); %find the matching cell indices for PFC
                cellcount = decodefilter(animal).output{dd}(epoch).eventdata(eventindex).pcellcount;
                %traj_cellSeq = trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).cellSequence; % cell seq for the 4 trajectories
                %cellpeakpos = trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).cellpeakpos;
                %cellSeq = decodefilter(animal).output{dd}(epoch).eventdata(eventindex).cellSeq; % cell sequence in the SWR
                
                timebins = startevent:binsize:endevent;
                peventcellsactive = [];
                pactivecount = 0;
                %activepeakpos = [];
                for trainingcell = 1:length(matches)
                    if (matches(trainingcell) > 0) %we have a match
                        pindexlist = [pindexlist; PFCtrainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).index(trainingcell,:)];
                        ptrainingdata = [ptrainingdata; PFCtrainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).rates(trainingcell,:)];
                        %currpeakpos = cellpeakpos(trainingcell,:);
                        
                        if ~isempty(decodefilter(animal).output{dd}(epoch).eventdata(eventindex).pspiketimes) % PFC can be empty - no spike
                            tmpspiketimes = decodefilter(animal).output{dd}(epoch).eventdata(eventindex).pspiketimes(find(decodefilter(animal).output{dd}(epoch).eventdata(eventindex).pcellindex == matches(trainingcell)));
                            %save all the info for the active cells
                            if ~isempty(tmpspiketimes)
                                pactivecount = pactivecount+1;
                                pactivespiketimes{pactivecount} = tmpspiketimes;
                                pactiverates = [pactiverates; PFCtrainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).rates(trainingcell,:)];
                                %activepeakpos = [activepeakpos; currpeakpos];
                            end
                        end
                        
                    end
                end
                ptrainingdata = ptrainingdata*binsize; %transform rates to expected number of spikes
                pactiverates = pactiverates*binsize;
                
                % ---------------------------------------------------------
                % Decoding Part of PFC
                % ----------------------------------------------------------
                expSpikeCounts = pactiverates;
                exponentmatrix = exp(-expSpikeCounts);
                pcellsactive = [];
                if length(pactivespiketimes)>0  % This acts similar to >cellthresh for CA1
                    for i = 1:length(pactivespiketimes)
                        spikebins = lookup(pactivespiketimes{i},timebins);
                        spikecount = zeros(1,length(timebins));
                        for j = 1:length(spikebins)
                            spikecount(spikebins(j)) = spikecount(spikebins(j))+1;
                        end
                        pspikedata = [pspikedata; spikecount];
                        pcellsactive = [pcellsactive; (spikecount > 0)];
                    end
                    
                    %the decoded data contains spatial probabilities, and is x by t
                    pdecodedata = zeros(size(expSpikeCounts,2),size(pspikedata,2));
                    naninds = find(isnan(expSpikeCounts(1,:)));
                    for t = 1:size(pspikedata,2) %time
                        Tspikecount = repmat(pspikedata(:,t),1,size(expSpikeCounts,2));
                        %calculate P(spikecount|x) for this timebin across all cells and all x
                        spatialprob = prod(((expSpikeCounts.^Tspikecount)./factorial(Tspikecount)).*exp(-expSpikeCounts),1)';
                        spatialprob(naninds) = 0;
                        spatialprob = spatialprob/sum(spatialprob);  %normalize across space
                        %spatialprob(find(spatialprob < max(spatialprob/2))) = 0;
                        pdecodedata(:,t) = spatialprob;
                    end
                    
                    ptotalspikecounts = sum(pspikedata,1);
                    ptotalcellsactive = sum(pcellsactive,1);
                    nonzerobins = find(ptotalspikecounts > 0);
                    
                    rvalues = [];
                    slopes = [];
                    entropy = []; entropy_loop=[];
                    for rloop = 1:10
                        tBinPicks = distsample(totalsamples,ptotalspikecounts);
                        regressdata = [];
                        for i = 1:length(nonzerobins)
                            if (ptotalspikecounts(nonzerobins(i)) > 0)
                                tmpnumsamples = sum(tBinPicks == nonzerobins(i));
                                distpicks = distvector(distsample(tmpnumsamples,pdecodedata(:,nonzerobins(i))))';
                                entropy_loop(i) = -nansum((hist(distpicks,0:5:200)./length(distpicks)).*log(hist(distpicks,0:5:200)./length(distpicks)));
                                
                                %distpicks(:,2) = timebins(nonzerobins(i));
                                distpicks(:,2) = i;
                                regressdata = [regressdata; distpicks];
                            end
                        end
                        regressdata(:,3) = 1;
                        [b,bint,r,rint,stats] = regress(regressdata(:,1),regressdata(:,2:3));
                        rvalues = [rvalues; stats(1)];
                        slopes = [slopes; b(1)];
                        entropy = [entropy; mean(entropy_loop)];
                    end
                    
                    % Save rvalue, slope and entropy for current event
                    decodefilter(animal).output{dd}(epoch).PFCrvalue(eventindex) = mean(rvalues);
                    decodefilter(animal).output{dd}(epoch).PFCslope(eventindex) = mean(slopes);
                    decodefilter(animal).output{dd}(epoch).PFCentropy(eventindex) = mean(entropy);
                    % Mark inbound = 0, outbound = 1
                    if mean(slopes) < 0,
                        direction = 0;  % inbound
                    else
                        direction = 1;  % outbound
                    end
                    decodefilter(animal).output{dd}(epoch).PFCdirection(eventindex) = direction;
                    % Save other things for current event
                    decodefilter(animal).output{dd}(epoch).PFCncells(eventindex) = length(pactivespiketimes);
                    decodefilter(animal).output{dd}(epoch).PFCreplaylength(eventindex) = length(timebins);
                    decodefilter(animal).output{dd}(epoch).PFCdecodedata{eventindex} = pdecodedata;
                    
                    % Now Evaluate the Decoded Data to See which Trajectory it Most Resembles
                    % -------------------------------------------------------------------------
                    % Do this regardless of following condition for CA1
                    %activepeakdist = 2*max(activepeakpos,[],2); % bizsize for posn is 2cm. Get max of all 4 traj for cells
                    %if any(activepeakdist>cp) % Need at least 1 cell past choicepoint, then continue
                    % Parse decode data
                    trajmapping = [1 1 2 2]; xdata = {[],[]}; ydata = {[],[]};
                    probdata = {}; probdata = cell(2,1);
                    
                    %combine the outbound and inbound trajectories
                    for traj = 1:4;
                        trajindex = find(PFCtrainingfilter(animal).output{dd}(epoch).traj == traj);
                        xdata{trajmapping(traj)} = PFCtrainingfilter(animal).output{dd}(epoch).dist(trajindex);
                        ydata{trajmapping(traj)} = stack(ydata{trajmapping(traj)}, pdecodedata(trajindex,1)');
                        if isempty(probdata{trajmapping(traj)})
                            probdata{trajmapping(traj)} = pdecodedata(trajindex,:);
                        else
                            probdata{trajmapping(traj)} = probdata{trajmapping(traj)} + pdecodedata(trajindex,:);
                        end
                    end
                    
                    % Which trajectory is replayed.
                    
                    
                    % One way: get area under curve for all probabilities>Choice Point
                    area1 = sum(sum(probdata{1}(cp_idx+1:end,:))); % Traj1 is Left arm, our view
                    area2 = sum(sum(probdata{2}(cp_idx+1:end,:))); % Traj2 is Right arm, our view
                    replay_arm_index1 = (area1-area2)./(area1+area2);
                    decodefilter(animal).output{dd}(epoch).PFCreplay_arm_index1(eventindex) = replay_arm_index1;
                    decodefilter(animal).output{dd}(epoch).PFCprobdata{eventindex} = probdata;
                    decodefilter(animal).output{dd}(epoch).PFCxdata{eventindex} = xdata;
                    %decodefilter(animal).output{dd}(epoch).activepeakpos{eventindex} = activepeakpos;
                    
                    % Second way - Get the one with higher mean probability - entire trajectory.
                    % But this includes central arm data. If these cells fire on both trajectories, as they usually do, they should be skipped
                    % This will be valis if cells are "splitter" cells on central arm
                    %traj1 = mean(mean(probdata{1}))>=mean(mean(probdata{2}));
                    %if traj1; traj = 1; else traj = 2; end
                    area1 = sum(sum(probdata{1})); % Traj1 is Left arm, our view
                    area2 = sum(sum(probdata{2})); % Traj2 is Right arm, our view
                    replay_arm_index2 = (area1-area2)./(area1+area2);
                    decodefilter(animal).output{dd}(epoch).PFCreplay_arm_index2(eventindex) = replay_arm_index2;
                    %end
                end % any active PFC cell in event?
                
            end % any valid PFC cell in epoch at all?
            
        end % any valis PFC epoch at all?
        
    end % if cellthresh = 2 is crossed
end % end eventindex