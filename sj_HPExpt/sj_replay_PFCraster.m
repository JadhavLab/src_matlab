
% PFC rip responses based on CA1 replay. Try for HPa day1 first

savedir = '/data25/sjadhav/HPExpt/ProcessedData/Replay2014/';
savefile = [savedir 'HPa_day1_replay_PFCresp'];
%load(savefile,'PFCf');
load(savefile);
pretu=550; posttu=550; rwin = [0 200]; binsize=10;
pret=500; postt=500; trim=50;
nstd = round(binsize*2/binsize); g1 = gaussian(nstd, 3*nstd+1);

prefix='HPa'; 

cellsp  = PFCf(1).output{1}(1).PFCindices;
nPFCcells = size(cellsp,1);

n=3;  % Cell number - Pick cell.
PFCidx = cellsp(n,:)
spkalign_cand_ep1 = PFCf(1).output{1}(1).spkalign_cand{n};
spkalign_cand_ep2 = PFCf(1).output{1}(2).spkalign_cand{n};
spkalign_cand = [spkalign_cand_ep1, spkalign_cand_ep2];

spkalign_all_ep1 = PFCf(1).output{1}(1).spkalign_all{n};
spkalign_all_ep2 = PFCf(1).output{1}(2).spkalign_all{n};
spkalign_all = [spkalign_all_ep1, spkalign_all_ep2];

spkalign_nonevent_ep1 = PFCf(1).output{1}(1).spkalign_nonevent{n};
spkalign_nonevent_ep2 = PFCf(1).output{1}(2).spkalign_nonevent{n};
spkalign_nonevent = [spkalign_nonevent_ep1, spkalign_nonevent_ep2];


% -------------------
% All ripple figure
% -------------------

spkscell = spkalign_all;

figure; hold on; redimscreen_figforppt1; xfont=24; yfont=24; tfont=24;
set(gcf,'Position',[100 130 1000 950]);
%set(gcf, 'Position',[205 658 723 446]);
subplot(2,1,1); hold on;
spkcount = []; histo = [];
for c=1:length(spkscell)
    tmps = spkscell{c};
    
    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
    plot(tmps,(length(spkscell)-c+1)*ones(size(tmps)),'k.','MarkerSize',16);
    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
    
    % Get count of spikes in response window
    if ~isempty(tmps)
        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
        spkcount = [spkcount; length(subset_tmps)];
    end    
    % Also get a histogram
    histspks = histc(tmps,[-pretu:binsize:posttu]);
    if isempty(histspks), % Only happens if spikeu is empty
        histspks = zeros(size([-pretu:binsize:posttu]));
    else
        histspks = smoothvect(histspks, g1);
    end
    histo(c,:) = histspks*1000/binsize;   
end
% TRIM ENDS of Histogram 
% -----------------------------------------------
histo = histo(:,trim/binsize:(pretu+posttu-trim)/binsize);

set(gca,'XLim',[-pret postt]);
%set(gca,'XTick',[])
%set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'));

xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('All ripples','FontSize',yfont,'Fontweight','normal');
set(gca,'YLim',[0 length(spkscell)]);
% Plot Line at 0 ms and rwin
ypts = 0:1:length(spkscell);
xpts = 0*ones(size(ypts));
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
title(sprintf('%s Day %d Tet %d Cell %d Nspkwin %d', prefix, PFCidx(1), PFCidx(3), PFCidx(4), sum(spkcount)),...
    'FontSize',tfont,'Fontweight','normal');

subplot(2,1,2); hold on;
xaxis = -pret:binsize:postt;
plot(xaxis,mean(histo),'k-','Linewidth',4);
set(gca,'XLim',[-pret postt]);
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Firing rate (Hz)','FontSize',yfont,'Fontweight','normal');
%set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'));
ylow = min(mean( histo)-sem( histo));
yhigh = max(mean( histo)+sem( histo));
set(gca,'YLim',[ylow-0.1 yhigh+0.1]);
ypts = ylow-0.1:0.1:yhigh+0.1;
xpts = 0*ones(size(ypts));
% Plot Line at 0 ms - Onset of stimulation
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);



% -------------------
% Cand events figure
% -------------------

spkscell = spkalign_cand;

figure; hold on; redimscreen_figforppt1; xfont=24; yfont=24; tfont=24;
set(gcf,'Position',[100 130 1000 950]);
%set(gcf, 'Position',[205 658 723 446]);
subplot(2,1,1); hold on;
spkcount = []; histo = [];
for c=1:length(spkscell)
    tmps = spkscell{c};
    
    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
    plot(tmps,(length(spkscell)-c+1)*ones(size(tmps)),'k.','MarkerSize',16);
    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
    
    % Get count of spikes in response window
    if ~isempty(tmps)
        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
        spkcount = [spkcount; length(subset_tmps)];
    end    
    % Also get a histogram
    histspks = histc(tmps,[-pretu:binsize:posttu]);
    if isempty(histspks), % Only happens if spikeu is empty
        histspks = zeros(size([-pretu:binsize:posttu]));
    else
        histspks = smoothvect(histspks, g1);
    end
    histo(c,:) = histspks*1000/binsize;   
end
% TRIM ENDS of Histogram 
% -----------------------------------------------
histo = histo(:,trim/binsize:(pretu+posttu-trim)/binsize);

set(gca,'XLim',[-pret postt]);
set(gca,'XTick',[])
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Cand replay event','FontSize',yfont,'Fontweight','normal');
set(gca,'YLim',[0 length(spkscell)]);
% Plot Line at 0 ms and rwin
ypts = 0:1:length(spkscell);
xpts = 0*ones(size(ypts));
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
title(sprintf('%s Day %d Tet %d Cell %d Nspkwin %d', prefix, PFCidx(1), PFCidx(3), PFCidx(4), sum(spkcount)),...
    'FontSize',tfont,'Fontweight','normal');

subplot(2,1,2); hold on;
xaxis = -pret:binsize:postt;
plot(xaxis,mean(histo),'k-','Linewidth',4);
set(gca,'XLim',[-pret postt]);
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Firing rate (Hz)','FontSize',yfont,'Fontweight','normal');
%set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'));
ylow = min(mean( histo)-sem( histo));
yhigh = max(mean( histo)+sem( histo));
set(gca,'YLim',[ylow-0.1 yhigh+0.1]);
ypts = ylow-0.1:0.1:yhigh+0.1;
xpts = 0*ones(size(ypts));
% Plot Line at 0 ms - Onset of stimulation
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);


% -------------------
% Non events figure
% -------------------

spkscell = spkalign_nonevent;

figure; hold on; redimscreen_figforppt1; xfont=24; yfont=24; tfont=24;
set(gcf,'Position',[100 130 1000 950]);
%set(gcf, 'Position',[205 658 723 446]);
subplot(2,1,1); hold on;
spkcount = []; histo = [];
for c=1:length(spkscell)
    tmps = spkscell{c};
    
    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
    plot(tmps,(length(spkscell)-c+1)*ones(size(tmps)),'k.','MarkerSize',16);
    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
    
    % Get count of spikes in response window
    if ~isempty(tmps)
        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
        spkcount = [spkcount; length(subset_tmps)];
    end    
    % Also get a histogram
    histspks = histc(tmps,[-pretu:binsize:posttu]);
    if isempty(histspks), % Only happens if spikeu is empty
        histspks = zeros(size([-pretu:binsize:posttu]));
    else
        histspks = smoothvect(histspks, g1);
    end
    histo(c,:) = histspks*1000/binsize;   
end
% TRIM ENDS of Histogram 
% -----------------------------------------------
histo = histo(:,trim/binsize:(pretu+posttu-trim)/binsize);

set(gca,'XLim',[-pret postt]);
set(gca,'XTick',[])
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Non event','FontSize',yfont,'Fontweight','normal');
set(gca,'YLim',[0 length(spkscell)]);
% Plot Line at 0 ms and rwin
ypts = 0:1:length(spkscell);
xpts = 0*ones(size(ypts));
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
title(sprintf('%s Day %d Tet %d Cell %d Nspkwin %d', prefix, PFCidx(1), PFCidx(3), PFCidx(4), sum(spkcount)),...
    'FontSize',tfont,'Fontweight','normal');

subplot(2,1,2); hold on;
xaxis = -pret:binsize:postt;
plot(xaxis,mean(histo),'k-','Linewidth',4);
set(gca,'XLim',[-pret postt]);
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Firing rate (Hz)','FontSize',yfont,'Fontweight','normal');
%set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'));
ylow = min(mean( histo)-sem( histo));
yhigh = max(mean( histo)+sem( histo));
set(gca,'YLim',[ylow-0.1 yhigh+0.1]);
ypts = ylow-0.1:0.1:yhigh+0.1;
xpts = 0*ones(size(ypts));
% Plot Line at 0 ms - Onset of stimulation
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);





% --------------------------------------------------------
% Get replay decoding
% -------------------------------------------------------

savedir = '/data25/sjadhav/HPExpt/ProcessedData/';
savefile = [savedir 'HPa_day1_replaydecode_gather.mat'];
%load(savefile,'PFCf');
load(savefile);

pvalue_ep1 = decodefilter(1).output{1}(1).pvalue;
pvalue_ep2 = decodefilter(1).output{1}(1).pvalue;
pvalue = [pvalue_ep1; pvalue_ep2];

sig = find(pvalue<0.05);
nosig = find(pvalue>0.05 & ~isnan(pvalue));

% -------------------
% Sig and non-sig events figure
% -------------------

spkscell=[];
for ii=1:length(sig)
    spkscell{ii} = spkalign_cand{sig(ii)};
end

figure; hold on; redimscreen_figforppt1; xfont=24; yfont=24; tfont=24;
set(gcf,'Position',[100 130 1000 950]);
%set(gcf, 'Position',[205 658 723 446]);
subplot(2,1,1); hold on;
spkcount = []; histo = [];
for c=1:length(spkscell)
    tmps = spkscell{c};
    
    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
    plot(tmps,(length(spkscell)-c+1)*ones(size(tmps)),'r.','MarkerSize',16);
    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
    
    % Get count of spikes in response window
    if ~isempty(tmps)
        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
        spkcount = [spkcount; length(subset_tmps)];
    end    
    % Also get a histogram
    histspks = histc(tmps,[-pretu:binsize:posttu]);
    if isempty(histspks), % Only happens if spikeu is empty
        histspks = zeros(size([-pretu:binsize:posttu]));
    else
        histspks = smoothvect(histspks, g1);
    end
    histo(c,:) = histspks*1000/binsize;   
end

% Non-sig

spkscell=[];
for ii=1:length(nosig)
    spkscell{ii} = spkalign_cand{nosig(ii)};
end

spkcount = []; histo2 = [];
for c=1:length(spkscell)
    tmps = spkscell{c};
    
    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
    plot(tmps,(length(spkscell)-c+1)*ones(size(tmps)),'r.','MarkerSize',16);
    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
    
    % Get count of spikes in response window
    if ~isempty(tmps)
        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
        spkcount = [spkcount; length(subset_tmps)];
    end    
    % Also get a histogram
    histspks = histc(tmps,[-pretu:binsize:posttu]);
    if isempty(histspks), % Only happens if spikeu is empty
        histspks = zeros(size([-pretu:binsize:posttu]));
    else
        histspks = smoothvect(histspks, g1);
    end
    histo2(c,:) = histspks*1000/binsize;   
end

% TRIM ENDS of Histogram 
% -----------------------------------------------
histo = histo(:,trim/binsize:(pretu+posttu-trim)/binsize);
histo2 = histo2(:,trim/binsize:(pretu+posttu-trim)/binsize);



set(gca,'XLim',[-pret postt]);
set(gca,'XTick',[])
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Sign replay event','FontSize',yfont,'Fontweight','normal');
set(gca,'YLim',[0 length(spkscell)]);
% Plot Line at 0 ms and rwin
ypts = 0:1:length(spkscell);
xpts = 0*ones(size(ypts));
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
title(sprintf('%s Day %d Tet %d Cell %d Nspkwin %d', prefix, PFCidx(1), PFCidx(3), PFCidx(4), sum(spkcount)),...
    'FontSize',tfont,'Fontweight','normal');

subplot(2,1,2); hold on;
xaxis = -pret:binsize:postt;
plot(xaxis,mean(histo),'r-','Linewidth',4);
plot(xaxis,mean(histo2),'k-','Linewidth',4);
legend('Sig Replay','Nonsig');
set(gca,'XLim',[-pret postt]);
xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
ylabel('Firing rate (Hz)','FontSize',yfont,'Fontweight','normal');
%set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'));
ylow = min(mean( histo)-sem( histo));
yhigh = max(mean( histo)+sem( histo));
set(gca,'YLim',[ylow-0.1 yhigh+0.1]);
ypts = ylow-0.1:0.1:yhigh+0.1;
xpts = 0*ones(size(ypts));
% Plot Line at 0 ms - Onset of stimulation
plot(xpts , ypts, 'k--','Linewidth',2);
xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
























