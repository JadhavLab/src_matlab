function out = DFAsj_getreplay_PFCresp(indices, excludeperiods, spikes, candripples, varargin)

% GET PFC resp for ripples/ cand replay events gotten simlar to "sj_getpopulationevents2"
% FOr response, do similar to ripalign code

% None of this is used - you get saved candripples directly
tetfilter = '';
excludeperiods = [];
maxcell = 0;
minstd = 3;
lowsp_thrs = 4; %cm/sec
highsp_thrs = lowsp_thrs;
dospeed = 0;
cellcountthresh = 5;
minrip=1;

rwin = [0 200];
%bwin = [-500 -300];
pret=500; postt=500;
binsize = 10;
smwin=10; %Smoothing Window

for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludeperiods'
            excludeperiods = varargin{option+1};
        case 'minstd'
            minstd = varargin{option+1};
        case 'minrip'
            minrip = varargin{option+1};
        case 'maxcell'
            maxcell = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        case 'lowsp_thrs'
            lowsp_thrs = varargin{option+1};
        case 'cellcountthresh'
            cellcountthresh = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
    
end


index = [indices(1,1) indices(1,2)];
day = index(1);
epoch = index(2);


riptimes = candripples{day}{epoch}.riptimes;
eventtime = candripples{day}{epoch}.eventtime;
noneventtime = candripples{day}{epoch}.noneventtime;
CA1replayindices = candripples{day}{epoch}.index;

out.PFCindices = indices;
out.CA1replayindices = CA1replayindices;
out.eventtime = eventtime; % Cand replay event times
out.noneventtime = noneventtime; % Non-event times
out.riptimes = riptimes; % All riptimes



% There can be multiple PFC cells in epoch. Get all of them.
% -------------------------------------
cellsp=[]; usecellsp=0; % PFC cells
totalcells = size(indices,1);
for i=1:totalcells
    cellsp = [cellsp; day epoch indices(i,3) indices(i,4)];
    usecellsp = usecellsp+1;
end
nPFCcells = size(cellsp,1);

% Get spikedata
for i=1:size(cellsp,1)
    i;
    eval(['spikep{',num2str(i),'}= spikes{cellsp(',num2str(i),',1)}{cellsp(',num2str(i),',2)}'...
        '{cellsp(',num2str(i),',3)}{cellsp(',num2str(i),',4)}.data(:,1);']); 
    
    eval(['spikeu = 1000*spikep{',num2str(i),'};']); % in ms
    
    
    trialResps=[]; rip_spks_cell=[]; spkalign_all=[];
    trialResps_nonevent=[]; rip_spks_cell_nonevent=[];
    
    % --------------------
    % Align to cand events
    % --------------------
    cntrip=0; nspk=0; cntall=0; spkalign_all=[];
    %nstd = round(binsize*2/binsize); g1 = gaussian(nstd, 3*nstd+1);
    for ii=1:size(eventtime,1)
        cntrip=cntrip+1;
        currrip = out.eventtime(ii,1)*1000; %in ms
        currspks =  spikeu(find( (spikeu>=(currrip-pret)) & (spikeu<=(currrip+postt)) ));
        nspk = nspk + length(currspks);
        currspks = currspks-(currrip);
        %     histspks = histc(currspks,[-pret:binsize:postt]);
        rip_spks_cell{cntrip}=currspks;
        % Get no of spikes in response window, and back window
        trialResps(cntrip) =  length(find(currspks>=rwin(1) & currspks<=rwin(2)));
        
        cntall = cntall+1;
        spkalign_all{cntall}=currspks;
        
        %     % Histogram
        %     % ---------
        %     %rip_spkshist_cell(cntrip,:) = histspks;
        %     if isempty(histspks), % Only happens if spikeu is empty
        %         histspks = zeros(size([-pret:binsize:postt]));
        %     end
        %     try
        %         histspks = smoothvect(histspks, g1);
        %     catch
        %         disp('Stopped in DFAsj_getripalignspiking');
        %         keyboard;
        %     end
        %     try
        %         rip_spkshist_cell(cntrip,:) = histspks;
        %     catch
        %         disp('Stopped in DFAsj_getripalignspiking');
        %         keyboard;
        %     end
    end
    
    eval(['out.trialResps_cand{',num2str(i),'} = trialResps;']);
    eval(['out.spkalign_cand{',num2str(i),'} = rip_spks_cell;']);
    %out.trialResps_cand = trialResps;
    %out.spkalign_cand = rip_spks_cell;
    
    
    % --------------------
    % Align to noncand events
    % --------------------
    cntrip=0; nspk=0; trialResps_nonevent=[]; rip_spks_cell_nonevent=[];
    %nstd = round(binsize*2/binsize); g1 = gaussian(nstd, 3*nstd+1);
    for ii=1:size(noneventtime,1)
        cntrip=cntrip+1;
        currrip = out.noneventtime(ii,1)*1000;
        currspks =  spikeu(find( (spikeu>=(currrip-pret)) & (spikeu<=(currrip+postt)) ));
        nspk = nspk + length(currspks);
        currspks = currspks-(currrip);
        %     histspks = histc(currspks,[-pret:binsize:postt]);
        rip_spks_cell_nonevent{cntrip}=currspks;
        % Get no of spikes in response window, and back window
        trialResps_nonevent(cntrip) =  length(find(currspks>=rwin(1) & currspks<=rwin(2)));
        
        cntall = cntall+1;
        spkalign_all{cntall}=currspks;
        
        %     % Histogram
        %     % ---------
        %     %rip_spkshist_cell(cntrip,:) = histspks;
        %     if isempty(histspks), % Only happens if spikeu is empty
        %         histspks = zeros(size([-pret:binsize:postt]));
        %     end
        %     try
        %         histspks = smoothvect(histspks, g1);
        %     catch
        %         disp('Stopped in DFAsj_getripalignspiking');
        %         keyboard;
        %     end
        %     try
        %         rip_spkshist_cell(cntrip,:) = histspks;
        %     catch
        %         disp('Stopped in DFAsj_getripalignspiking');
        %         keyboard;
        %     end
    end
    
    eval(['out.trialResps_nonevent{',num2str(i),'} = trialResps_nonevent;']);
    eval(['out.spkalign_nonevent{',num2str(i),'} = rip_spks_cell_nonevent;']);
    %out.trialResps_nonevent = trialResps_nonevent;
    %out.spkalign_nonevent = rip_spks_cell_nonevent;
    
    % Also, for all ripples in epoch = candidate + nonevent
    % --------------------------
    eval(['out.trialResps_all{',num2str(i),'} = [trialResps, trialResps_nonevent];']);
    eval(['out.spkalign_all{',num2str(i),'} = spkalign_all;']);
    %out.trialResps_all = [trialResps; trialResps_nonevent];
    %out.spkalign_nonevent = spkalign_all;
    
end





