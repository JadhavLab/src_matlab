pref='C:\Users\adagid\Dropbox\maximumEntropy\data\tracesToConvert';
files1=ls(pref);
files1=files1(3:end,:);
numFiles=size(files1,2);

targetPath='C:\Users\adagid\Dropbox\maximumEntropy\data\semiManConverted';
for i=1:numFiles
   load([pref '\' files1(i,1:findstr(files1(i,:),'mat')+2)]);
   [allEventsBinnedBinary allEventsBinnedAnalog traces allEvents eventThreshs]=getBinnedData([pref '\' files1(i,1:findstr(files1(i,:),'mat')+2)],fr);
   save([targetPath '\' files1(i,1:findstr(files1(i,:),'.')-1) 'All2']) 
    
end