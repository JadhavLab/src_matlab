
prefixStr='/home/gideon/Code/maximumEntropy/data/semiManConvertedGoodTracesOnly/';
datafileName=dir(prefixStr);
numFiles=size(datafileName,1);
allCorVals=[];
allRelCorVals=[];

for kk=4:numFiles
ratioPatternProbsP2Shuff=[];
 ratioPatternProbsP2=[];
    %    numFiringCells=sum(P2data);
    figure
    tmp1=['/home/gideon/Code/maximumEntropy/data/newShuffData/' datafileName(kk).name]
    shufPrefixStr=[tmp1(1:end-4) 'Shuff']
    for kkk=1:10
       load([shufPrefixStr num2str(kkk) '.mat'])
        
       %P2=P2+eps;
       %patternProbs=patternProbs+eps;
            loglog(P2(2:end),patternProbs(2:end),'kx');
            hold on;   
            drawnow;
            patternProbs=patternProbs(2:end);
P2=P2(2:end);
nonZeroInds=find(patternProbs>0)
pp=patternProbs(nonZeroInds);
p2=P2(nonZeroInds);
           ratioPatternProbsP2Shuff=[ratioPatternProbsP2Shuff pp./p2];
    end
    
    load([prefixStr datafileName(kk).name])
       %P2=P2+eps;
       %patternProbs=patternProbs+eps;
   
        loglog(P2(2:end),patternProbs(2:end),'rx');
        
        
        patternProbs=patternProbs(2:end);
P2=P2(2:end);
nonZeroInds=find(patternProbs>0)
pp=patternProbs(nonZeroInds);
p2=P2(nonZeroInds);
        
        
        ratioPatternProbsP2=pp./p2;
    plot(10^-10:0.0001:1,10^-10:0.0001:1,'r')
    title(['num cells= ' num2str(size(P2data,1)) ' file =' datafileName(kk).name])
    
    % ratioPatternProbsP2 is ratio of real data probabilities divided by
    % probabilities in a pairwise model
    % ratioPatternProbsP2Shuff is the same for shuffled data (no
    % correlations). 
    %here I test if the ratio for the real data differs from 1 (log is
    %higher than 0)
    [h1 p1]=ttest(log(ratioPatternProbsP2))
    
    % here I test if these ratios differ in their means. I.e., that for the
    % real data there are larger deviations from a pairwise model than from shuffled data 
    % NOTICE, I TAKE LOG TO MAKE THE DATA MORE NORMAL, BUT IT IS STILL NOT
    % VERY NORMAL, so next is a non-parametric test
    [h p]=ttest2(log(ratioPatternProbsP2),log(ratioPatternProbsP2Shuff))
    
    [pp hh]=ranksum(ratioPatternProbsP2,ratioPatternProbsP2Shuff)
    
  
     
     
end