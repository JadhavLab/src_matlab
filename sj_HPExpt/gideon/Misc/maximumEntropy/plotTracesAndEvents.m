path1='C:\Users\adagid\Dropbox\maximumEntropy\data\tracesEventsAndBinned';
files11=ls(path1);
files11=files11(3:end,:);
for ii=1:size(files11,1)
    ii
    load([path1 '\' files11(ii,1:findstr(files11(ii,:),'mat')+2)]);
    figure;
    dd=max(max(traces));
    for k=1:size(traces,1)
        plot(traces(k,:)+k*dd);
        hold on;
        plot(allEvents(k,:)+k*dd,'r');
    end;
    title(files11(ii,1:findstr(files11(ii,:),'mat')+2))
end
