
deleteTr=find(eventThreshs==1000);
t=traces;
t(deleteTr,:)=NaN;
goodTraces=find(sum(isnan(t'))==0);
traces=t(goodTraces,:);
allEventsBinnedBinary=allEventsBinnedBinary(goodTraces,:);
allEventsBinnedAnalog=allEventsBinnedAnalog(goodTraces,:);
allEvents=allEvents(goodTraces,:);


eventThreshs=eventThreshs(~(eventThreshs==1000));