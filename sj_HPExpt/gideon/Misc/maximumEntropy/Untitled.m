patternProbs=zeros(1,size(P2data,2));
data1=data;
data1(data1==0)=-1;
P2data1=P2data';
P2data1(P2data1==0)=-1;
for i=1:samplePoints
curPat=data1(:,i);
res1=P2data1*curPat;
whichPat=find(res1==numCells)
patternProbs(whichPat)=patternProbs(whichPat)+1;
end
patternProbs=patternProbs/sum(patternProbs);
%plotting probabilities of patterns other than the all-zeros pattern
figure;
plot(patternProbs(2:end))

%exp probs as a function of model probs
figure;
loglog(P2(2:end),patternProbs(2:end),'x')

%simpler view but clearer
figure;
plot(patternProbs./P2)
%in this data, it highlights pattern 714, which occurs 1.6 million times more
%often than expected (although it happens only once)- like rolling a dice 8
%times and getting in all of them 6 
patternProbs(714)/P2(714)
%but several patterns occur 100-200 more often than expected. Notice some
%of them share neurons, but some don't!