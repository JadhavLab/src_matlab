pref='C:\Users\adagid\Dropbox\maximumEntropy\data\semiManConverted';
files1=ls(pref);
files1=files1(3:end,:);
numFiles=size(files1,1);


for iii=1:numFiles
    
    pref='C:\Users\adagid\Dropbox\maximumEntropy\data\semiManConverted';
    load([pref '\' files1(iii,1:findstr(files1(iii,:),'mat')+2)]);
    pref='C:\Users\adagid\Dropbox\maximumEntropy\data\semiManConverted';
    files1=ls(pref);
    files1=files1(3:end,:);
    numFiles=size(files1,2);
    
    deleteBadTraces
    clear pref;
    targetPath='C:\Users\adagid\Dropbox\maximumEntropy\data\semiManConvertedGoodTracesOnly';
    save([targetPath '\' files1(iii,1:findstr(files1(iii,:),'.')-1)],'traces','allEvents','allEventsBinnedBinary','allEventsBinnedAnalog','eventThreshs')
    
end