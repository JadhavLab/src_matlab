function [xlim ylim]=limitByMass(xy,thresh)

%thresh=700;

% a=750.8;
% b=-2.951e-7;
% c=-1.447e+4;
% d=-0.007952;
% thresh = a*exp(b*length(xy)) + c*exp(d*length(xy));
thresh

try
x=xy(:,1);
y=xy(:,2);
catch
    dde=1
    
end
[n c]=hist3(xy,[35 35]);
% figure;
% imagesc(n)
c1=c{:,1};
c2=c{:,2};
xjump=diff(c1);
yjump=diff(c2);

z=zeros(35,35);
z(find(n>(length(x)/thresh)))=1;

deleteLonelyPoints;

a=find(z>0);
rows=mod(a,35);
rows(rows==0)=35;
cols=ceil(a/35);

posx=c1(rows);
posy=c2(cols);

sortedPosy=sort(unique((posy)));
highest=[find(posy==sortedPosy(end)) find(posy==sortedPosy(end-1)) find(posy==sortedPosy(end-2))];
initialJump=6;


topRow=max(posy);
tops=find(posy==topRow);
xlim=mean(posx(highest));
mean(posy(highest));
yjump(1)=min(yjump(1),0.01);
ylim=mean(posy(highest))+initialJump*yjump(1);


