
prefixStr='/home/gideon/Code/maximumEntropy/data/semiManConvertedGoodTracesOnly/';
datafileName=dir(prefixStr);
numFiles=size(datafileName,1);
allCorVals=[];
allRelCorVals=[];
for kk=4:numFiles
    load([prefixStr datafileName(kk).name])
    untitled;
    myColors={'cyan','blue','red','green','black','magenta'}
    numFiringCells=sum(P2data);
    figure
    subplot(6,1,1)
    for i=2:size(P2data,2)
        loglog(P2(i),patternProbs(i),'x','color',myColors{mod(numFiringCells(i),6)+1});
        i
        hold on;
    end
    
    plot(10^-10:0.0001:1,10^-10:0.0001:1,'r')
    title(['num cells= ' num2str(size(P2data,1)) ' file =' datafileName(kk).name])
    
    %    title('observed probabilities as a function of pairwise model probabilities');
    hold on
    subplot(6,1,2)
    for i=2:size(P2data,2)
        loglog(independentModelProbs(i),patternProbs(i),'x','color',myColors{mod(numFiringCells(i),6)+1});
        i
        hold on;
    end
    
    plot(10^-10:0.0001:1,10^-10:0.0001:1,'r')
    title('observed probabilities as a function of independent model probabilities');
    subplot(6,1,3)
    plot(sort(patternProbs(2:end)./P2(2:end)))
    
    
    
    %% pairwise corrs
    corVals=[];
    corMat=corrcoef(allEventsBinnedBinary');
    subplot(6,1,4)
    imagesc(corMat);caxis([-1 1]); colorbar;
    for i=1:size(corMat,1)-1
        corVals=[corVals; diag(corMat,i)];
    end
    allCorVals=[allCorVals; corVals];
    allRelCorVals=[allRelCorVals; corVals/max(corVals)];
    subplot(6,1,5)
    hist(corVals);
    positions=[cos(0:2*pi/(numCells):2*pi)' sin(0:2*pi/(numCells):2*pi)'];
    subplot(6,1,6)
    for i=1:numCells,for j=1:i,plot([positions(i,1) positions(j,1)],[positions(i,2) positions(j,2)],'linewidth',abs(corMat(i,j))*15,'color','k');hold on;end;end
end