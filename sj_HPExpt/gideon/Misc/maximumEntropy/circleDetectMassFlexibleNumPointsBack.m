function [events sumFarDists eventAmplitudes initialThresh]=circleDetectMassFlexibleNumPointsBack(flu,lineFreq)
transientIncreaseInMS=100;
numPointsBack=round(transientIncreaseInMS*lineFreq/1000);
scrsz = get(0,'ScreenSize');

%Finding all local maxima
peakIndices=find(flu(2:end-1)>flu(1:end-2) & flu(2:end-1)>flu(3:end))+1;
peakIndices=peakIndices(find(peakIndices>numPointsBack,1):end);

%fluSmoothed=smooth(flu,20)';
%Finding amplitude for all local maxima
transientAmplitudes=flu(peakIndices)-flu(peakIndices-numPointsBack);
% transientAmplitudes=flu(peakIndices)'-fluSmoothed(peakIndices-numPointsBack);
 transientAmplitudes=transientAmplitudes';

%Finding integrals for all local maxima
transientIntegrals=[];
%intLength=round(500*lineFreq/1000);
intLength=round(0.3*lineFreq);
for i=1:length(peakIndices)
    if length(flu)>(peakIndices(i)+intLength)
        transientIntegral=flu(peakIndices(i)-numPointsBack:peakIndices(i)+intLength)-flu(peakIndices(i)-numPointsBack);
        transientIntegrals=[transientIntegrals sum(transientIntegral)];
    end
end

transientAmplitudes=transientAmplitudes(1:length(transientIntegrals))';
transientIntegrals=transientIntegrals';
meanTransientAmplitudes=nanmean(transientAmplitudes);
meanTransientIntegrals=nanmean(transientIntegrals);

transientIntegralsAndAmplitudes=[transientIntegrals transientAmplitudes];

initialThresh=200;
while true
    
    [transientIntegralslim transientAmplitudeslim]=limitByMass(transientIntegralsAndAmplitudes,initialThresh);
   

goodOnes=find((transientIntegrals)>transientIntegralslim &(transientAmplitudes)>transientAmplitudeslim) ;
addedCandidates=find(transientAmplitudes>transientAmplitudeslim/2);
addedCandidates=peakIndices(addedCandidates);
addedCandidates=addedCandidates(addedCandidates>intLength);
difArr=[];
difInd=[];
difArr2=[];
difInd2=[];
from1=round(360*lineFreq/1000);
to1=round(70*lineFreq/1000);
from2=round(500*lineFreq/1000);
to2=round(140*lineFreq/1000);
addedCandidates=addedCandidates(addedCandidates>from2);
for j=1:length(addedCandidates)
    try
    curDif=flu(addedCandidates(j))-nanmean(flu(addedCandidates(j)-from1:addedCandidates(j)-to1));
    curDif2=flu(addedCandidates(j))-nanmean(flu(addedCandidates(j)-from2:addedCandidates(j)-to2));
    difArr=[difArr curDif];
    difInd=[difInd addedCandidates(j)];
    difArr2=[difArr2 curDif2];
    difInd2=[difInd2 addedCandidates(j)];
    catch
ddd=1;
    end
end

 alsoGood=difInd(find(difArr>transientAmplitudeslim *1.2));
 alsoGood2=difInd2(find(difArr2>transientAmplitudeslim *1.3));

dists=sqrt(power(transientAmplitudes(goodOnes)-meanTransientAmplitudes,2)+power(transientIntegrals(goodOnes)-meanTransientIntegrals,2));
sumFarDists=1000*nansum(dists)/length(transientIntegrals);


figure(201)

hold off;
plot(transientIntegrals,transientAmplitudes,'x')
hold on;
plot(transientIntegrals(goodOnes),transientAmplitudes(goodOnes),'ro')
title(['score ' num2str(sumFarDists)]);
axis([-40 40 -0.6 0.6])



events=sort(unique([peakIndices(goodOnes)' alsoGood alsoGood2]));
eventAmplitudes=flu(events)-flu(events-numPointsBack);
ww=figure(77);plot(flu);hold on;plot(events,flu(events),'rx');ylim([-0.5 0.5])
set(ww,'Position',[1 scrsz(4)/4 scrsz(3) scrsz(4)/2])



 ans1=input('ok??','s');
    if ~strcmp(ans1(1),'y')
        close all;
        initialThresh=input('set new threshold');
    else
        close all;
        break;
    end
close all;
end