%ISSUES:
%IT SEEMS THE MODEL DOESN'T PREDICT WELL THE SINGLE-CELL PROBABILITIES
%CHECK WHETHER THE NEGATIVE SIGN OF THE BETA'S IS EXPECTED




%% data

%sload('C:\Users\adagid\Dropbox\maximumEntropy\data\semiManConvertedGoodTracesOnly\cells04112011-1103-114All2.mat')
function maxEntropyCalculation2(fileToLoad)
load(fileToLoad);

binaryD=allEventsBinnedBinary;
binaryD(binaryD==2)=1;
samplePoints=size(binaryD,2);
numCells=size(binaryD,1);
data=binaryD;
% samplePoints=10000;
% numCells=15;
% data=round(rand(numCells,samplePoints)-0.4);
% %bias
% data(1:5,1:3:end)=1;

%% Building matrix with all possible binary combinations
%numCells=15;
bigArr=zeros(2^numCells,numCells);
runner=2;
for k=1:numCells
    cur=nchoosek(1:numCells,k);
    for i=1:size(cur,1)
        bigArr(runner,cur(i,:))=1;
        runner=runner+1;
    end;
end;
P2data=unique(bigArr,'rows')';
allCombinations=size(P2data,2);
%%
randomStartValue=0.01;
alpha=randomStartValue*ones(1,numCells);
beta=randomStartValue*ones(numCells,numCells);
%single cell probabilities in data
spd=mean(data');

% pair probabilities in data
ppd=NaN(numCells,numCells);
for i=1:numCells
    for j=1:i-1
        ppd(i,j)=sum(sum(data([i,j],:))==2)/samplePoints;
    end
end


fol=[];
relChangeAlpha=1;
relChangeBeta=1;
numRuns=0;
while relChangeAlpha>0.00001 || relChangeBeta>0.00001
    numRuns=numRuns+1
    fol=[fol beta(2,1)];
    %Quantifying P2
    P2=NaN(1,allCombinations);
    for i=1:allCombinations
        alphaVal=alpha*P2data(:,i);
        betaVal=sum(sum(tril((P2data(:,i)*P2data(:,i)').*beta,-1)));
        P2(i)=exp(alphaVal+betaVal);
    end
    P2=P2/sum(P2);
    
    %Single cell probabilities according to P2
    P2cells=[];
    for i=1:numCells
        P2cells(i)=sum(P2(find(P2data(i,:))));
    end
    
    dGdAlpha=spd-P2cells;
    
    
    ppp2=NaN(numCells,numCells);
    for i=1:numCells
        for j=1:i-1
            ppp2(i,j)=sum(P2(find(sum(P2data([i,j],:))==2)));
        end
    end
    
    dGdBeta=ppd-ppp2;
    
    relChangeAlpha=mean(abs(dGdAlpha))
    relChangeBeta=mean(abs(dGdBeta(~isnan(dGdBeta))))
    alpha=alpha+0.1*dGdAlpha;
    beta=beta+0.1*dGdBeta;
end

figure;
subplot(2,1,1)
imagesc(corrcoef(data'));
caxis([-1 1])
subplot(2,1,2)
imagesc(beta);
caxis([-1 1]);
colorbar
%%
%assessing probability based on an independent model
repSpd=repmat(spd',1,size(P2data,2));
tmpRes1=P2data.*repSpd;
tmpRes1(tmpRes1==0)=1;
independentModelProbs=prod(tmpRes1);
%the pattern of all zeros
independentModelProbs(1)=NaN;



%%
patternProbs=zeros(1,size(P2data,2));
data1=data;
data1(data1==0)=-1;
P2data1=P2data';
P2data1(P2data1==0)=-1;
for i=1:samplePoints
    curPat=data1(:,i);
    res1=P2data1*curPat;
    whichPat=find(res1==numCells);
    patternProbs(whichPat)=patternProbs(whichPat)+1;
end
patternProbs=patternProbs/sum(patternProbs);
%plotting probabilities of patterns other than the all-zeros pattern
figure;
plot(patternProbs(2:end))

%exp probs as a function of pairwise model probs
figure;
subplot(2,1,1)
loglog(P2(2:end),patternProbs(2:end),'x')
hold on;
plot(10^-10:0.0001:1,10^-10:0.0001:1,'r')
title('observed probabilities as a function of pairwise model probabilities');

%exp probs as a function of independent model probs
subplot(2,1,2);
loglog(independentModelProbs(2:end),patternProbs(2:end),'x')
hold on;
plot(10^-10:0.0001:1,10^-10:0.0001:1,'r')
title('observed probabilities as a function of independent model probabilities');


%simpler view but clearer
figure;
plot(patternProbs./P2)
hold on
plot(patternProbs./independentModelProbs,'r')

%in this data, it highlights pattern 714, which occurs 1.6 million times more
%often than expected (although it happens only once)- like rolling a dice 8
%times and getting in all of them 6
%patternProbs(714)/P2(714)
%but several patterns occur 100-200 more often than expected. Notice some
%of them share neurons, but some don't!


%which pattern is it?
%P2data(:,714)

%to go back and watch where in the fluorescence trace a rare event
%occurred:
%vv=find(P2data(:,3830))
%rareEventInd=length(traces)*find(sum(allEventsBinnedBinary(vv,:))==length(vv))/length(allEventsBinnedBinary)

% how many times the outlying patterns occurred:
howManyTimesOccurred=patternProbs(find(patternProbs./P2>100))/(1/size(allEventsBinnedBinary,2))

% showing which patterns
figure;imagesc(P2data(:,find(patternProbs./P2>100))');xlabel('cells');ylabel('patterns')


% this is an example for file 'allEventsBinnedBinary1103-114'
% here I look at the probability of not a specific pattern, but of all
% patterns in which a specific subset of neurons is firing. The motivation
% is that in this dataset there are 20 different patterns that occur with
% high percentage compared to the pairwise model, but each occurs only
% once. However, when you look at it you see many of them share active
% neurons. And so patterns containing this subset occur 300 times more
% often than expected!
c1=[6 11 16 17];
%sum(patternProbs(find(sum(P2data(c1,:))==4)))/sum(P2(find(sum(P2data(c1,:))==4)))


%how close are the estimated single-cell and pairwise probabilities to the
%data
figure;
subplot(2,1,1)
plot(spd);hold on;plot(P2cells,'r');
xlabel('cell num');ylabel('prob');title('blue-data,red-model')
valsPPD=ppd(~isnan(ppd))
valsPPP2=ppp2(~isnan(ppp2))
subplot(2,1,2)
plot(valsPPD);hold on;plot(valsPPP2,'r')
xlabel('pair num');ylabel('prob');




%%
% This is an example for calculating the prob for an interesting assembly to fire (don't
% care about the rest) in data, indepedent model and pairwise model.
% REMEMBER NOT ONLY TO CALCULATE PROBABILITIES OF SPECIFIC PATTERNS SUCH AS
% [1 0 1 0 1 1], BUT ALSO THE PROBABILITY THAT, FOR EXAMPLE, NEURONS
% 1,3,5,6 FIRED TOGETHER (AND DON'T CARE ABOUT THE REST)
% interestingAssembly=[2 3 7 8 11 13];
% ia=interestingAssembly;
% dataProb=sum((sum(data(ia,:))==length(ia)))/samplePoints;
% independentModelProb=1;
% for kk=1:length(ia),independentModelProb=independentModelProb*spd(ia(kk));end;
% pairwiseModelProb=sum(P2(find((sum(P2data(ia,:))==length(ia)))));

%%
save([fileToLoad(1:end-4) 'Data3.mat'])
