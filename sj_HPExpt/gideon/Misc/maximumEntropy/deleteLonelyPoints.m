    z(35,35)=0;
deleted=1;
while deleted
    [zx zy]=find(z==1);

    %     figure
    %     imagesc(z);
    deleted=0;
    for curPoint=1:length(zx)
        sumn=0;
        inds=neighbsInds(zx(curPoint),zy(curPoint));
        for k=1:8
                        if inds(k,1)>0 && inds(k,2)>0 && inds(k,1)<=35 && inds(k,2)<=35
%            if inds(k,1)>0 && inds(k,2)>0 
                sumn=sumn+z(inds(k,1),inds(k,2));
            end
        end
        if sumn<2
            z(inds(1,1)+1,inds(1,2)+1)=0;
            deleted=1;

            %if the following case exists:
            %----------------------------+++
            %---------------------------+------
            %-----------------------+++------
            %Then delete the point in the second row, and the point  to the
            %upper right. The above example would result in:
            %-----------------------------++
            %----------------------------------
            %-----------------------+++------
        elseif  inds(1,1)>0 && inds(1,2)>0 && inds(8,1)>0 && inds(8,2)>0 && sumn==2 && z(inds(1,1),inds(1,2))==1 && z(inds(8,1),inds(8,2))==1
            z(inds(1,1)+1,inds(1,2)+1)=0;
            z(inds(1,1)+2,inds(1,2)+2)=0;
            deleted=1;

        elseif  inds(1,1)>0 && inds(1,2)>0 && inds(5,1)>0 && inds(5,2)>0 && sumn==2 && z(inds(1,1),inds(1,2))==1 && z(inds(5,1),inds(5,2))==1
            z(inds(1,1)+1,inds(1,2)+1)=0;
            z(inds(1,1)+2,inds(1,2)+2)=0;
            deleted=1;

        elseif  inds(2,1)>0 && inds(2,2)>0 &&  inds(7,1)>0 && inds(7,2)>0  && sumn<5 && z(inds(2,1),inds(2,2))==0 && z(inds(7,1),inds(7,2))==0
            z(inds(1,1)+1,inds(1,2)+1)=0;
            deleted=1;

        elseif  inds(4,1)>0 && inds(4,2)>0 && inds(5,1)>0 && inds(5,2)>0 && sumn<5 && z(inds(4,1),inds(4,2))==0 && z(inds(5,1),inds(5,2))==0
            z(inds(1,1)+1,inds(1,2)+1)=0;
            deleted=1;



        end
    end
end

%if there is an empty row- cut off everything above it
sumz=sum(z);
for t=2:length(sumz)-1
    if sumz(t)==0 && sum(sumz(1:t-1))>0 && sum(sumz((t+1):length(sumz)))>0
        z(:,t+1:length(sumz))=0;
    end
end




% Cutting off the top in case there is a narrow throat
%figure
%imagesc(z);
sumz=sum(z);
for t=2:length(sumz)-1
    maxRowBelow=1;
    for tt=max(1,t-8):t-1
        maxRowBelow=max(maxRowBelow,max([find(z(:,tt)==1); 1]) );
    end
if t==10
    re=1;
end
    if sumz(t)<6 &&  max([find(z(:,t)==1); 1])<max([find(z(:,t+1)==1); 1]) && max([find(z(:,t)==1); 1])<maxRowBelow
        z(:,t+1:length(sumz))=0;
    end
end
d=1;






