function [allEventsBinnedBinary allEventsBinnedAnalog traces allEvents eventThreshs]=getBinnedData(path,lineFreq)
%H:\Gideon\spont\data\cells100111-1441-131.mat
%addpath('C:\Users\lorenlab\Dropbox\Project with Elad\NetworksACproject\events');
load(path);
%load('H:\Gideon\new linescan experiments\control\191210\data\cells191210-1136-100');
% lineFreq=getLineFreq('H:\Gideon\new linescan experiments\control\191210\LineScan-12192010-1420-100\LineScan-12192010-1420-100_Ch2.lsd')
%lineFreq=130;

%traces=cells;
numCells=size(traces,1);
traceLength=size(traces,2);
%tracesUF=cells;
eventThreshs=[];
b=fir1(51,0.1);
for i=1:numCells
    traces(i,:)=filtfilt(b,1,traces(i,:));
end


binSizeInSec=round(0.1*lineFreq);
allEvents=zeros(numCells,traceLength);
allEventsBinary1=zeros(numCells,traceLength);
sfds=[];
for i=1:numCells
    [eventsG sumFarDistsG eventsGAmplitudes eventThresh]=circleDetectMassFlexibleNumPointsBack(traces(i,:)',lineFreq);
    eventThreshs=[eventThreshs eventThresh];
    sfds=[sfds sumFarDistsG];
    allEvents(i,eventsG)=eventsGAmplitudes;
    allEventsBinary1(i,eventsG)=1;
end
figure; for i=1:size(traces,1),plot(traces(i,:)+i/2);hold on;end
for i=1:size(traces,1),plot(allEventsBinary1(i,:)/3+i/2,'r');hold on;end

% traces=traces(sfds>65,:);
% tracesUF=tracesUF(sfds>65,:);
% allEvents=allEvents(sfds>65,:);
% allEventsBinary1=allEventsBinary1(sfds>65,:);

numCells=size(allEvents,1);
figure;

allEventsBinnedBinary=[];
allEventsBinnedAnalog=[];
for i=1:binSizeInSec:traceLength-binSizeInSec
    sumBinaryEvents=sum(allEventsBinary1(:,i:i+binSizeInSec-1),2);
    sumAnalogEvents=sum(allEvents(:,i:i+binSizeInSec-1),2);
    allEventsBinnedBinary=[allEventsBinnedBinary sumBinaryEvents];
    allEventsBinnedAnalog=[allEventsBinnedAnalog sumAnalogEvents];
end

% figure
% subplot(2,1,1)
% imagesc(allEventsBinnedBinary)
% subplot(2,1,2)
% imagesc(allEventsBinnedAnalog)
