%plot all EEGs of day 14, epoch 4 (SB)
allEEGs=plotEEGs3('/data15/gideon/Ndl/EEG/Ndleeg14-4-',14,4,8);
load('/data15/gideon/Ndl/Ndllinpos14.mat')
%on top, plot velocity
tt=linpos{14}{4}.statematrix.time;
vel=linpos{14}{4}.statematrix.linearVelocity;
tt=tt-tt(1);
hold on
plot(tt*1500,abs(vel(:,3))*1000,'k','linewidth',2)
%zooming in on a single movement section
axis([102000 118000 0 60000])

% what we see above is:
% 1. Theta in Hc occurs during movement (mostly), no big surprise
% 2. Theta is not evident in cortex (blue-AC, black-PFC)

%% This is an example from a sleep session where the artifact occurred
allEEGs=plotEEGs3('/data15/gideon/Ndl/EEG/Ndleeg04-1-',4,1,0);

% zooming in on a single scratching section
axis([955000 968000 0 35000])

% what you see here is that the artifact occurs on all tetrodes, strongest
% in AC. Also, there is no speed data here but he was probably not moving
% (SB).

%%
load('/data15/gideon/Ndl/EEG/Ndleeg04-1-04.mat')
load('/data15/gideon/Ndl/EEG/ndltheta04-1-04.mat')
d3=eeg{4}{1}{4}.data;
t3=theta{4}{1}{4}.data;
figure;hist(double(t3(:,3)),1000)
figure;plot(d3);
hold on
plot(1:10:length(t3)*10,t3(:,3),'r')

%%
%define a section with some artifacts
dd=d3(900000:1000000);
figure;plot(dd)

[S,F,T,P] =spectrogram(dd,128,120,[1:0.2:50],1500);
figure
surf(T,F,10*log10(P),'edgecolor','none'); axis tight;
view(0,90);
%% design filter
%750=nyquist
filter8to50=fir1(551,[8/750 50/750]);
freqz(filter8to50,1,1500)
ddfiltered=filtfilt(filter8to50,1,dd);
figure;plot(dd);hold on;plot(ddfiltered,'r')
%% compare the result
[S,F,T,P] =spectrogram(ddfiltered,128,120,[1:0.2:50],1500);
figure
surf(T,F,10*log10(P),'edgecolor','none'); axis tight;
view(0,90);
%% How do ripples and the chewing noise coexist?
allEEGs=plotEEGs3('/data15/gideon/Ndl/EEG/Ndleeg16-5-',16,5,8);
load('/data15/gideon/Ndl/Ndlripples16.mat')
plot(ripples{16}{5}{13}.startind,1,'mo','markerfacecolor','m')

% ripples tend to occur when he is not "chewing":
 axis(1.0e+05 *[ 3.8738    6.9838   -0.0178    0.3322])

% but even when they do co-occur, it seems to ride fine on top of the
% noise:
% zoom1:
axis(1.0e+05 *[ 6.3536    6.7424   -0.0133    0.3249])
% further zoomed:
axis( 1.0e+05 *[6.5650    6.5871   -0.0130    0.2177])

%% spectrogram w/wo noise, w ripples 

% THEN, RUN THE FILTER AND IDENTIFY ALL TIMES OF THE ARTIFACT.
% IT SEEMS THAT:
% NEITHER SPIKES NOR RIPPLES ARE AFFECTED BY THE ARTIFACT
% SO, THE ONLY THING THAT WOULD BE AFFECTED IS THE LFP DURING THAT TIME.
% FOR THOSE ANALYSES, THE TIMES OF THE ARTIFACT SHOULD BE EXCLUDED

startx=1.0e+05*5.1950;
endx=1.0e+05*6.9371;


tet13duringRips=allEEGs(startx:endx,13);
% just for plotting (for the linkaxes), I divide the amplitude by 3.3
tet13duringRips=tet13duringRips/3.3;
figure;
sp1=subplot(3,1,1)
plot(1/1500:1/1500:length(tet13duringRips)/1500,tet13duringRips);axis tight
%plot(tet13duringRips);axis tight
curRips=ripples{16}{5}{13}.startind;
curRipsSection=curRips(find(curRips>startx&curRips<endx));
curRipsSection=curRipsSection-startx;
hold on;plot(curRipsSection/1500,1,'mo','markerfacecolor','m')
[S,F,T,P] =spectrogram(tet13duringRips,178,170,[1:2:300],1500);
sp2=subplot(3,1,2)
surf(T,F,10*log10(P),'edgecolor','none'); axis tight;
view(0,90);
% z-scored per freq
sp3=subplot(3,1,3)
meanPerFreq=mean(P')
meanPerFreqMat=repmat(meanPerFreq',1,size(P,2));
a=((P-meanPerFreqMat)./meanPerFreqMat);
imagesc(T,F,flipud(a))
 set(gca,'YTickLabel',300:-50:0)
 linkaxes([sp1 sp2 sp3],'xy')