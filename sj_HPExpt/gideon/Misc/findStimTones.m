function [stimEEGtimes stimInds]=findStimTones(baseString, dayString,epochString,tetrodeString,freqsAttensString,offset1)
% Find stimuli times
%load('/data15/gideon/Gth/EEG/Gtheeg03-1-12');

load([baseString dayString '-' epochString '-' tetrodeString])
day=str2num(dayString);
epoch=str2num(epochString);
tetrode=str2num(tetrodeString);

stimTrace=eeg{day}{epoch}{tetrode}.data;
stimTraceThreshd=stimTrace;
stimTraceThreshd(stimTraceThreshd>-5000)=0;
minInds=find(diff(stimTraceThreshd)<-4000);
goodOnes=[1; 1-(diff(minInds)<100)];
minInds=minInds(logical(goodOnes));

if offset1==2
    BBNInds=minInds([1:6, 381:387,763:769,1145:1151]);
    BBNIndsToRemove=([1:6, 381:387,764:769,1146:1151]);
    
elseif offset1==1
    BBNInds=minInds([1:6, 382:388,764:770,1146:1152]);
    BBNIndsToRemove=([1:6, 382:388,764:770,1146:1152]);
else
    BBNInds=minInds([1:7, 383:389,765:771,1147:1153 1202]);
    BBNIndsToRemove=([1:7, 383:389,765:771,1147:1153 1202]);
    
end
noBBNs=ones(1,size(minInds,1));
noBBNs(BBNIndsToRemove)=0;




stimIndsWBBN=minInds;
stimInds=stimIndsWBBN(logical(noBBNs));



figure;plot(stimTrace);hold on; plot(stimInds,-25000,'rx');plot(BBNInds,-25000,'kx');

%load('/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat');
load(freqsAttensString)
freqsAttensAll=[freqsAttens freqsAttens freqsAttens freqsAttens];
numTones=size(freqsAttensAll,2);

if numTones==length(stimInds)
    'Number of stimuli match'
else
    'NUMBER OF STIMULI DO NOT MATCH!!!'
end

% Finished finding stimuli times

% Arrange stimuli by type

[orderedStim orderedStimInd]=sortrows(freqsAttensAll',[1 2]);
uniqueStims=unique(orderedStim,'rows');
numUniqueStims=size(uniqueStims,1);
numTrialsPerStim=numTones/numUniqueStims;

%Order of stim
%Columns are stim type, rows are trials
%Columns are ordered from low to high frequencies, and within frequency,
%from low to high intensity
allTrialsPerStim=reshape(orderedStimInd,numTrialsPerStim,size(uniqueStims,1));

%Data indices of stimuli, in the same structure
allTrialsPerStimInd=stimInds(allTrialsPerStim);

stimEEGtimes=geteegtimes(eeg{day}{epoch}{tetrode});


