function [stimEEGtimes stimInds]=findStimTrack(baseString, dayString,epochString,tetrodeString)
% Find stimuli times
%load('/data15/gideon/Gth/EEG/Gtheeg03-1-12');

load([baseString dayString '-' epochString '-' tetrodeString])
day=str2num(dayString);
epoch=str2num(epochString);
tetrode=str2num(tetrodeString);

stimTrace=eeg{day}{epoch}{tetrode}.data;
stimTraceThreshd=stimTrace;
stimTraceThreshd(stimTraceThreshd>-5000)=0;
minInds=find(diff(stimTraceThreshd)<-4000);
goodOnes=[1; 1-(diff(minInds)<100)];
minInds=minInds(logical(goodOnes));

% if offset1==2
%     BBNInds=minInds([1:6, 381:387,763:769,1145:1151]);
%     BBNIndsToRemove=([1:6, 381:387,764:769,1146:1151]);
%     
% elseif offset1==1
%     BBNInds=minInds([1:6, 382:388,764:770,1146:1152]);
%     BBNIndsToRemove=([1:6, 382:388,764:770,1146:1152]);
% else
%     BBNInds=minInds([1:7, 383:389,765:771,1147:1153]);
%     BBNIndsToRemove=([1:7, 383:389,765:771,1147:1153]);
%     
% end
% noBBNs=ones(1,size(minInds,1));
% noBBNs(BBNIndsToRemove)=0;
% 
% stimIndsWBBN=minInds;
% stimInds=stimIndsWBBN(logical(noBBNs));

stimInds=minInds;

figure;plot(stimTrace);hold on; plot(stimInds,-25000,'rx');



% Finished finding stimuli times


stimEEGtimes=geteegtimes(eeg{day}{epoch}{tetrode});


