function compareBefDurAftrResponsesSpikesSB(dayString1,SBEpochs,trackEpochs,tetrodes,baseStringSpikes,stimEEGTimesSB,stimIndsSBMat,whichStimSBMat,stimEEGTimesTrack,stimIndsTrack)
% SOUND STIMULI:
% DURATION: 200MS
% SHORT ISI: 50MS
% LONG ISI (BETWEEN PAIRS): 500 MS

% SB data, first and last epochs
stimEEGTimes1=stimEEGTimesSB{1};
stimEEGTimes2=stimEEGTimesSB{2};
stimInds1=stimIndsSBMat(:,1);
stimInds2=stimIndsSBMat(:,2);
whichStim1=whichStimSBMat(1,:);
whichStim2=whichStimSBMat(2,:);

%Track data
numTrackEpochs=size(stimEEGTimesTrack,2);
totalNumEpochs=numTrackEpochs+2;

day=str2num(dayString1);
%SB epochs
epoch1=SBEpochs(1);
epoch2=SBEpochs(2);

allEpochs=[SBEpochs,trackEpochs];

%----parameters
timeWindowInS=1;
numStim=length(stimInds1);
integralLengthInS=0.3;


%----spikes

%close all;
timeWindowInS=0.5;
bigVecSampRate=10000;
timeWindowLength=timeWindowInS*bigVecSampRate;
IntegralWindowInS=0.2;
IntegralWindowLength=IntegralWindowInS*bigVecSampRate;
allSpikes1={};
allSpikes2={};

%load('/data15/gideon/Gth/EEG/Gtheeg03-1-12')
%load('/data15/gideon/Gth/Gthspikes02')
load([baseStringSpikes dayString1])
spikesAllCells={};
fras=[];
cellCount=0;
allSpikeResponses=[];
allFluctVec=[];
spikes=multi;


for i=1:length(spikes{day}{epoch1})
   
            
            ['tet=' num2str(i)]
            
            cellCount=cellCount+1;
            
            % First epoch
            
            spikeResponses1=NaN(numStim,2*timeWindowLength+1);
            % creating vector where each cell corresponds to 0.1ms
            stimEEGTimesBin1=zeros(1,ceil(stimEEGTimes1(end)*10000));
            % putting ones where stimuli occurred
            stimEEGTimesBin1(round(stimEEGTimes1(stimInds1)*10000))=1;
            %indices of stim in big vector
            stimIndsBin1=find(stimEEGTimesBin1==1);
            % spike times in 0.1 ms units
%            spikes1=spikes{day}{epoch1}{i}{j}.data;
            spikes1=spikes{day}{epoch1}{i}/10000;
if length(spikes1)==0,spikes1=1;end

            % creating vector where each cell corresponds to 0.1ms
            % the length is from 0 to  seconds after the last stim
            spikeTimesBin1=zeros(1,ceil((stimIndsBin1(end))+10*10000));
            % putting spikes in the right places
            spikeTimesBin1(round(spikes1(:,1)*10000))=1;
            for stim1=1:length(stimInds1)
                spikeResponses1(stim1,:)=spikeTimesBin1(stimIndsBin1(stim1)-timeWindowLength:stimIndsBin1(stim1)+timeWindowLength);
            end
            allSpikes1{end+1}=spikes1(:,1);
            
            
            % Last epoch
            
            spikeResponses2=NaN(numStim,2*timeWindowLength+1);
            stimEEGTimesBin2=zeros(1,ceil(stimEEGTimes2(end)*10000));
            stimEEGTimesBin2(round(stimEEGTimes2(stimInds2)*10000))=1;
            stimIndsBin2=find(stimEEGTimesBin2==1);
%            spikes2=spikes{day}{epoch2}{i}{j}.data;
            spikes2=spikes{day}{epoch2}{i}/10000;
if length(spikes2)==0,spikes2=1;end

            spikeTimesBin2=zeros(1,ceil((stimIndsBin2(end))+10*10000));
            spikeTimesBin2(round(spikes2(:,1)*10000))=1;
            for stim2=1:length(stimInds2)
                spikeResponses2(stim2,:)=spikeTimesBin2(stimIndsBin2(stim2)-timeWindowLength:stimIndsBin2(stim2)+timeWindowLength);
            end
            allSpikes2{end+1}=spikes2(:,1);
            
            %---------- Track epochs
            spikeResponsesTrack=[];%NaN(1,2*timeWindowLength+1);
            
            for mm=1:numTrackEpochs
                stimEEGTimes=stimEEGTimesTrack{mm};
                stimInds=stimIndsTrack{mm};
                
                stimEEGTimesBin=zeros(1,ceil(stimEEGTimes(end)*10000));
                stimEEGTimesBin(round(stimEEGTimes(stimInds)*10000))=1;
                stimIndsBin=find(stimEEGTimesBin==1);
                
            spikes1=spikes{day}{trackEpochs(mm)}{i}/10000;
            if length(spikes1)==0,spikes1=1;end

                spikeTimesBin=zeros(1,ceil((stimIndsBin(end))+10*10000));
                spikeTimesBin(round(spikes1(:,1)*10000))=1;

                firstSoundInPairs=[1; find(diff(stimInds)>600)+1];
                stimInds=stimInds(firstSoundInPairs);
                for stim1=1:length(stimInds)
                    spikeResponsesTrack(end+1,:)=spikeTimesBin(stimIndsBin(stim1)-timeWindowLength:stimIndsBin(stim1)+timeWindowLength);
                    
                end
            end
            %----------------------
            
            %[left, bottom, width, height]:
            scrsz = get(0,'ScreenSize');
            figure('Position',[1 1 scrsz(3) scrsz(4)])
            tmp1=smooth(nanmean(spikeResponses1(find(whichStim1==10),:)'),200);
            maxmax2=2*max(tmp1(2000:8000));
            inds2=-0.5:1/10000:0.5;
            
            
            % Aligned to the first sound in every pair of UP CHIRP
            subplot(4,4,1)
            curStimType1=find(whichStim1==10);
            plot(inds2,smooth(nanmean(spikeResponses1(curStimType1,:)'),200));
            hold on;
            plot(inds2,smooth(nanmean(spikeResponsesTrack'),200),'k');
            curStimType2=find(whichStim2==10);
            plot(inds2,smooth(nanmean(spikeResponses2(curStimType2,:)'),200),'r');
            maxmax1=max([max(smooth(nanmean(spikeResponses1(curStimType1,:)'),200))+1e-5 max(smooth(nanmean(spikeResponses2(curStimType2,:)'),200))+1e-5 max(smooth(nanmean(spikeResponsesTrack'),200))]);
            maxmax1=maxmax2;
            xlim([-0.5 0.5])
            title(['Tet ' num2str(i) ' UP CHIRPS'])
            
            subplot(4,4,2)
            s1=spikeResponses1(curStimType1,:);
            for ii=1:length(curStimType1),if sum(s1(ii,:))>0,plot(find(s1(ii,:)),ii,'ok','markerfacecolor','k','markersize',1);hold on;end;end;xlim([0 10000])
            title('First SB epoch')
            axis off
            
            subplot(4,4,3)
            for ii=1:size(spikeResponsesTrack,1),if sum(spikeResponsesTrack(ii,:))>0,plot(find(spikeResponsesTrack(ii,:)),ii,'ok','markerfacecolor','k','markersize',1);hold on;end;end;xlim([0 10000])
            title('Track epochs')
            axis off
            
            subplot(4,4,4)
            s2=spikeResponses2(curStimType2,:);
            for ii=1:length(curStimType2),if sum(s2(ii,:))>0,plot(find(s2(ii,:)),ii,'ok','markerfacecolor','k','markersize',1);hold on;end;end;xlim([0 10000])
            title('Last SB epoch')
            axis off
            
            % Aligned to the first sound in every pair of DOWN CHIRP
            subplot(4,4,5)
            curStimType1=find(whichStim1==20);
            plot(inds2,smooth(nanmean(spikeResponses1(curStimType1,:)'),200));
            hold on;
            curStimType2=find(whichStim2==20);
            plot(inds2,smooth(nanmean(spikeResponses2(curStimType2,:)'),200),'r');
            maxmax1=max(max(smooth(nanmean(spikeResponses1(curStimType1,:)'),200))+1e-5,max(smooth(nanmean(spikeResponses2(curStimType2,:)'),200))+1e-5);
                        maxmax1=maxmax2;

            xlim([-0.5 0.5])
            title('DOWN CHIRPS')

            subplot(4,4,6)
            s1=spikeResponses1(curStimType1,:);
            for ii=1:length(curStimType1),if sum(s1(ii,:))>0,plot(find(s1(ii,:)),ii,'ok','markerfacecolor','k','markersize',1);hold on;end;end;xlim([0 10000])
            axis off
            
            subplot(4,4,8)
            s2=spikeResponses2(curStimType2,:);
            for ii=1:length(curStimType2),if sum(s2(ii,:))>0,plot(find(s2(ii,:)),ii,'ok','markerfacecolor','k','markersize',1);hold on;end;end;xlim([0 10000])
            axis off
            
            
            % Aligned to the first sound in every pair of PURE TONES
            subplot(4,4,9)
            curStimType1=find(whichStim1==30);
            plot(inds2,smooth(nanmean(spikeResponses1(curStimType1,:)'),200));
            hold on;
            curStimType2=find(whichStim2==30);
            plot(inds2,smooth(nanmean(spikeResponses2(curStimType2,:)'),200),'r');
            maxmax1=max(max(smooth(nanmean(spikeResponses1(curStimType1,:)'),200))+1e-5,max(smooth(nanmean(spikeResponses2(curStimType2,:)'),200))+1e-5);
                        maxmax1=maxmax2;
            xlim([-0.5 0.5])

            title('PURE TONES')
            subplot(4,4,10)
            s1=spikeResponses1(curStimType1,:);
            for ii=1:length(curStimType1),if sum(s1(ii,:))>0,plot(find(s1(ii,:)),ii,'ok','markerfacecolor','k','markersize',1);hold on;end;end;xlim([0 10000])
           axis off
           
            subplot(4,4,12)
            s2=spikeResponses2(curStimType2,:);
            for ii=1:length(curStimType2),if sum(s2(ii,:))>0,plot(find(s2(ii,:)),ii,'ok','markerfacecolor','k','markersize',1);hold on;end;end;xlim([0 10000])
            axis off
            
            % Aligned to the first sound in every pair of BBN
            subplot(4,4,13)
            curStimType1=find(whichStim1==40);
            plot(inds2,smooth(nanmean(spikeResponses1(curStimType1,:)'),200));
            hold on;
            curStimType2=find(whichStim2==40);
            plot(inds2,smooth(nanmean(spikeResponses2(curStimType2,:)'),200),'r');

            maxmax1=max(max(smooth(nanmean(spikeResponses1(curStimType1,:)'),200))+1e-5,max(smooth(nanmean(spikeResponses2(curStimType2,:)'),200))+1e-5);
            maxmax1=maxmax2;
            xlim([-0.5 0.5])
            xlabel('Time (S)')
            
            title('BBN')
            subplot(4,4,14)
            s1=spikeResponses1(curStimType1,:);
            for ii=1:length(curStimType1),if sum(s1(ii,:))>0,plot(find(s1(ii,:)),ii,'ok','markerfacecolor','k','markersize',1);hold on;end;end;xlim([0 10000])
            axis off
            
            subplot(4,4,16)
            s2=spikeResponses2(curStimType2,:);
            for ii=1:length(curStimType2),if sum(s2(ii,:))>0,plot(find(s2(ii,:)),ii,'ok','markerfacecolor','k','markersize',1);hold on;end;end;xlim([0 10000])
            axis off
            
            saveas(gcf,['/data15/gideon/Rtl/ProcessedData/audResponses/day' dayString1 ' Tet ' num2str(i) '.jpg'])

            
            close all;
            
end