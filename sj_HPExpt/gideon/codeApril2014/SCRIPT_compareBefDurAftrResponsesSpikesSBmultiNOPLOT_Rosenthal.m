%% adapted from the parallel script for Nadal, which was adapted from the file with
% no animal extension in the name, which was for Rosenthal and included "during".
% This is for Rosenthal, but I currently only look at before vs after


 RTLaudtets=[15:17,19:21];
 RTLdays=[1,2,4:12]; %NOTICE DAYS 10-12 WITH NEW SOUND
 RTLepochs=repmat([1 10],11,1);

 

allRespAmpsBef={};
allRespAmpsAft={};
epochCounter=1;

for dayx=[RTLdays]
        
if dayx<10
    daystr=['0' num2str(dayx)];
else
    daystr=num2str(dayx);
end

 [stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/opt/data15/gideon/Rtl/EEG/Rtleeg', daystr,'1','22');
 [stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/opt/data15/gideon/Rtl/EEG/Rtleeg', daystr,num2str(RTLepochs(epochCounter,2)),'22');
 if strcmp(daystr,'06'), stimIndsAft=stimIndsAft(2:end);end
 [respAmpsBef respAmpsAft]=compareBefAftrResponsesSpikesSBmulti('/opt/data15/gideon/Rtl/EEG/Rtleeg',daystr,'1',num2str(RTLepochs(epochCounter,2)),RTLaudtets, '/opt/data15/gideon/Rtl/Rtlmulti',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft)
 epochCounter=epochCounter+1;
 

allRespAmpsBef{dayx}=respAmpsBef;
allRespAmpsAft{dayx}=respAmpsAft;
close all;
clear stimIndsTrack stimEEGTimesTrack stimEEGtimesSB stimEEGtimesBef stimEEGtimesAft
cc=1;
end
save /opt/data15/gideon/allaudrespsbefaftdataROSENTHAL
%%
load /opt/data15/gideon/allaudrespsbefaftdataROSENTHAL

%% this looks at response to target sound (pre, dur post), and BBN (pre, post) (AC tets)
% over days. It doesn't show anything striking, tetrodes are very different
% more systematic analysis in next section

tetXtargetBef=[];
tetXtargetAft=[];
tetXtargetDur=[];
% choose tet here
tetX=6;
for i=RTLdays, 
    tetXtargetBef=[tetXtargetBef allRespAmpsBef{i}(tetX,1)];
 tetXtargetAft=[tetXtargetAft allRespAmpsAft{i}(tetX,1)];
end

figure;
subplot(2,1,1)
plot(RTLdays,tetXtargetBef)
hold on
plot(RTLdays,tetXtargetAft,'r')

subplot(2,1,2)
tetXBBNBefRTL=[];
tetXBBNAftRTL=[];
tetXBBNDurRTL=[];


for i=RTLdays, tetXBBNBefRTL=[tetXBBNBefRTL allRespAmpsBef{i}(tetX,4)];end
for i=RTLdays, tetXBBNAftRTL=[tetXBBNAftRTL allRespAmpsAft{i}(tetX,4)];end
%for i=[1,2,4:12], tetXBBNDur=[tetXBBNDur allRespAmpsDur{i}(tetX)];end

plot(RTLdays,tetXBBNBefRTL)
hold on
plot(RTLdays,tetXBBNAftRTL,'r')
%plot([1,2,4:12],tetXBBNDur,'k')
%%
tetXtargetBefRTL=[];
tetXtargetAftRTL=[];
tetXtargetDurRTL=[];
tetXDist1BefRTL=[];
tetXDist1AftRTL=[];
tetXDist2BefRTL=[];
tetXDist2AftRTL=[];
tetXBBNBefRTL=[];
tetXBBNAftRTL=[];
 alldaysXRTL=[];

for tetX=1:6
for days1=RTLdays, 
    tetXtargetBefRTL=[tetXtargetBefRTL allRespAmpsBef{days1}(tetX,1)];
 tetXtargetAftRTL=[tetXtargetAftRTL allRespAmpsAft{days1}(tetX,1)];
 tetXDist1BefRTL=[tetXDist1BefRTL allRespAmpsBef{days1}(tetX,2)];
 tetXDist1AftRTL=[tetXDist1AftRTL allRespAmpsAft{days1}(tetX,2)];
  tetXDist2BefRTL=[tetXDist2BefRTL allRespAmpsBef{days1}(tetX,3)];
 tetXDist2AftRTL=[tetXDist2AftRTL allRespAmpsAft{days1}(tetX,3)];
 tetXBBNBefRTL=[tetXBBNBefRTL allRespAmpsBef{days1}(tetX,4)];
 tetXBBNAftRTL=[tetXBBNAftRTL allRespAmpsAft{days1}(tetX,4)];
  alldaysXRTL=[alldaysXRTL days1];

end
end


%% tet preferences
% 
% 
% for tet1=1:6
% curtet=[(RTLresps1bef(tet1,:));(RTLresps2bef(tet1,:));(RTLresps3bef(tet1,:));(RTLresps4bef(tet1,:))]
% [rr tt]=sort(curtet);
% figure;imagesc(tt);
% end

%%
% are responses larger after? (AC tets, days <10)
figure;
subplot(1,4,1)
plot(tetXtargetBefRTL(alldaysXRTL<10),tetXtargetAftRTL(alldaysXRTL<10),'ko','markerFaceColor','k')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXtargetBefRTL(alldaysXRTL<10),tetXtargetAftRTL(alldaysXRTL<10),0.05,'left')
title(['target, P=' num2str(p)])

subplot(1,4,2)
plot(tetXDist1BefRTL(alldaysXRTL<10),tetXDist1AftRTL(alldaysXRTL<10),'ko','markerFaceColor','k')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXDist1BefRTL(alldaysXRTL<10),tetXDist1AftRTL(alldaysXRTL<10),0.05,'left')
title(['down chirp, P=' num2str(p)])

subplot(1,4,3)
plot(tetXDist2BefRTL(alldaysXRTL<10),tetXDist2AftRTL(alldaysXRTL<10),'ko','markerFaceColor','k')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXDist2BefRTL(alldaysXRTL<10),tetXDist2AftRTL(alldaysXRTL<10),0.05,'left')
title(['pure tones, P=' num2str(p)])

subplot(1,4,4)
plot(tetXBBNBefRTL(alldaysXRTL<10),tetXBBNAftRTL(alldaysXRTL<10),'ko','markerFaceColor','k')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXBBNBefRTL(alldaysXRTL<10),tetXBBNAftRTL(alldaysXRTL<10),0.05,'left')
title(['BBN, P=' num2str(p)])
%%
% are responses larger after? (AC tets, days >=10)
scrsz = get(0,'ScreenSize');
figure('Position',[1 1 scrsz(3)/5 scrsz(4)/8]);
       set(gcf,'PaperUnits','inches','PaperPosition',[0 0 12 10])
subplot(1,4,1)
plot(tetXtargetBefRTL(alldaysXRTL>=10),tetXtargetAftRTL(alldaysXRTL>=10),'ko','markerFaceColor','k')
hold on
xx=[0:10];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXtargetBefRTL(alldaysXRTL>=10),tetXtargetAftRTL(alldaysXRTL>=10),0.05,'left')
title(['target, P=' num2str(p)])

subplot(1,4,2)
plot(tetXDist1BefRTL(alldaysXRTL>=10),tetXDist1AftRTL(alldaysXRTL>=10),'ko','markerFaceColor','k')
hold on
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXDist1BefRTL(alldaysXRTL>=10),tetXDist1AftRTL(alldaysXRTL>=10),0.05,'left')
title(['down chirp, P=' num2str(p)])

subplot(1,4,3)
plot(tetXDist2BefRTL(alldaysXRTL>=10),tetXDist2AftRTL(alldaysXRTL>=10),'ko','markerFaceColor','k')
hold on
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXDist2BefRTL(alldaysXRTL>=10),tetXDist2AftRTL(alldaysXRTL>=10),0.05,'left')
title(['pure tones, P=' num2str(p)])

subplot(1,4,4)
plot(tetXBBNBefRTL(alldaysXRTL>=10),tetXBBNAftRTL(alldaysXRTL>=10),'ko','markerFaceColor','k')
hold on
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXBBNBefRTL(alldaysXRTL>=10),tetXBBNAftRTL(alldaysXRTL>=10),0.05,'left')
title(['BBN, P=' num2str(p)])
%% change in response amplitude over the week
RTLresps1bef=[];
RTLresps2bef=[];
RTLresps3bef=[];
RTLresps4bef=[];

for tetX=1:6
for days1=RTLdays
    RTLresps1bef(tetX,days1)= allRespAmpsBef{days1}(tetX,1);
    RTLresps2bef(tetX,days1)= allRespAmpsBef{days1}(tetX,2);
    RTLresps3bef(tetX,days1)= allRespAmpsBef{days1}(tetX,3);
    RTLresps4bef(tetX,days1)= allRespAmpsBef{days1}(tetX,4);

    
end
end

RTLresps1bef=RTLresps1bef(:,RTLdays);
RTLresps2bef=RTLresps2bef(:,RTLdays);
RTLresps3bef=RTLresps3bef(:,RTLdays);
RTLresps4bef=RTLresps4bef(:,RTLdays);

RTLresps1aft=[];
RTLresps2aft=[];
RTLresps3aft=[];
RTLresps4aft=[];

for tetX=1:6
for days1=RTLdays
    RTLresps1aft(tetX,days1)= allRespAmpsAft{days1}(tetX,1);
    RTLresps2aft(tetX,days1)= allRespAmpsAft{days1}(tetX,2);
    RTLresps3aft(tetX,days1)= allRespAmpsAft{days1}(tetX,3);
    RTLresps4aft(tetX,days1)= allRespAmpsAft{days1}(tetX,4);

    
end
end
RTLresps1aft=RTLresps1aft(:,RTLdays);
RTLresps2aft=RTLresps2aft(:,RTLdays);
RTLresps3aft=RTLresps3aft(:,RTLdays);
RTLresps4aft=RTLresps4aft(:,RTLdays);

% to target sound
figure;
subplot(4,1,1)
plot(RTLdays,mean(RTLresps1bef),'b')
hold on;
plot(RTLdays,mean(RTLresps1aft),'r')

% to other sounds
subplot(4,1,2)
plot(RTLdays,mean(RTLresps2bef),'b')
hold on;
plot(RTLdays,mean(RTLresps2aft),'r')


subplot(4,1,3);
plot(RTLdays,mean(RTLresps3bef),'b')
hold on;
plot(RTLdays,mean(RTLresps3aft),'r')


subplot(4,1,4)
plot(RTLdays,mean(RTLresps3bef),'b')
hold on;
plot(RTLdays,mean(RTLresps3aft),'r')

%%
% different sounds, bef
figure;hold on

plot(RTLdays,mean(RTLresps1bef),'k')
plot(RTLdays,mean(RTLresps2bef),'b')
plot(RTLdays,mean(RTLresps3bef),'r')
plot(RTLdays,mean(RTLresps4bef),'g')

% different sounds, aft
figure;hold on

plot(RTLdays,mean(RTLresps1aft),'k')
plot(RTLdays,mean(RTLresps2aft),'b')
plot(RTLdays,mean(RTLresps3aft),'r')
plot(RTLdays,mean(RTLresps4aft),'g')

%save /opt/data15/gideon/allaudrespsbefaftdataROSENTHAL
%% Rosenthal
means1=[mean(RTLresps1bef);mean(RTLresps2bef);mean(RTLresps3bef);mean(RTLresps4bef)];
stds1=[std(RTLresps1bef);std(RTLresps2bef);std(RTLresps3bef);std(RTLresps4bef)]./sqrt(size(RTLresps1bef,1));
figure;barwitherr(stds1',means1')
figure;imagesc(means1(:,1:9)./repmat(max(means1(:,1:9)),4,1))

means1=[mean(RTLresps1aft);mean(RTLresps2aft);mean(RTLresps3aft);mean(RTLresps4aft)];
stds1=[std(RTLresps1aft);std(RTLresps2aft);std(RTLresps3aft);std(RTLresps4aft)]./sqrt(size(RTLresps1aft,1));
figure;barwitherr(stds1',means1')

%% Nadal
load /opt/data15/gideon/allaudrespsbefaftdataNADAL
means1=[mean(resps1bef);mean(resps2bef);mean(resps3bef);mean(resps4bef)];
stds1=[std(resps1bef);std(resps2bef);std(resps3bef);std(resps4bef)]./sqrt(size(resps1bef,1));
figure;barwitherr(stds1',means1')
 figure;imagesc(means1./repmat(max(means1),4,1))
 
means1=[mean(resps1aft);mean(resps2aft);mean(resps3aft);mean(resps4aft)];
stds1=[std(resps1aft);std(resps2aft);std(resps3aft);std(resps4aft)]./sqrt(size(resps1aft,1));
figure;barwitherr(stds1',means1')

%%
% merging non-target sounds, Nadal bef
nontarget=[resps2bef;resps3bef;resps4bef]
means2=[mean(resps1bef);mean(nontarget)];
stds2=[std(resps1bef)./sqrt(size(resps1bef,1));std(nontarget)./sqrt(size(nontarget,1))];
figure;barwitherr(stds2',means2')

% merging non-target sounds, Nadal aft
nontarget=[resps2aft;resps3aft;resps4aft]
means2=[mean(resps1aft);mean(nontarget)];
stds2=[std(resps1aft)./sqrt(size(resps1aft,1));std(nontarget)./sqrt(size(nontarget,1))];
figure;barwitherr(stds2',means2')


%%
% merging non-target sounds, Rtl bef
RTLnontarget=[RTLresps2bef;RTLresps3bef;RTLresps4bef]
RTLmeans2=[mean(RTLresps1bef);mean(RTLnontarget)];
RTLstds2=[std(RTLresps1bef)./sqrt(size(RTLresps1bef,1));std(RTLnontarget)./sqrt(size(RTLnontarget,1))];
figure;barwitherr(RTLstds2',RTLmeans2')

% merging non-target sounds, Rtl aft
RTLnontarget=[RTLresps2aft;RTLresps3aft;RTLresps4aft]
RTLmeans2=[mean(RTLresps1aft);mean(RTLnontarget)];
RTLstds2=[std(RTLresps1aft)./sqrt(size(RTLresps1aft,1));std(RTLnontarget)./sqrt(size(RTLnontarget,1))];
figure;barwitherr(RTLstds2',RTLmeans2')
%% ratio between target to non-targets across days
 figure;plot(mean(RTLresps1bef(:,1:8))./mean(RTLnontarget(:,1:8)))
hold on
plot(mean(resps1bef)./mean(nontarget),'r')
ylim([0.65 1.35])
%% combining with Nadal's data, do AC resps increase at end of day
load /opt/data15/gideon/allaudrespsbefaftdataNADAL
tetXtargetBefAll=[tetXtargetBefRTL(alldaysXRTL<10) tetXtargetBef];
tetXtargetAftAll=[tetXtargetAftRTL(alldaysXRTL<10) tetXtargetAft];

tetXDist1BefAll=[tetXDist1BefRTL(alldaysXRTL<10) tetXDist1Bef];
tetXDist1AftAll=[tetXDist1AftRTL(alldaysXRTL<10) tetXDist1Aft];

tetXDist2BefAll=[tetXDist2BefRTL(alldaysXRTL<10) tetXDist2Bef];
tetXDist2AftAll=[tetXDist2AftRTL(alldaysXRTL<10) tetXDist2Aft];

tetXBBNBefAll=[tetXBBNBefRTL(alldaysXRTL<10) tetXBBNBef];
tetXBBNAftAll=[tetXBBNAftRTL(alldaysXRTL<10) tetXBBNAft];


%figure('Position',[1 1 scrsz(3)/5 scrsz(4)/6]);
 %       set(gcf,'PaperUnits','inches','PaperPosition',[0 0 12 10])
figure('Position',[1 1 scrsz(3)/5 scrsz(4)/8]);
       set(gcf,'PaperUnits','inches','PaperPosition',[0 0 12 10])

subplot(1,4,1)
plot(tetXtargetBefAll,tetXtargetAftAll,'ko','markerFaceColor','k')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXtargetBefAll,tetXtargetAftAll,0.05,'left')
title(['target, P=' num2str(p)])

subplot(1,4,2)
plot(tetXDist1BefAll,tetXDist1AftAll,'ko','markerFaceColor','k')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXDist1BefAll,tetXDist1AftAll,0.05,'left')
title(['down chirp, P=' num2str(p)])

subplot(1,4,3)
plot(tetXDist2BefAll,tetXDist2AftAll,'ko','markerFaceColor','k')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXDist2BefAll,tetXDist2AftAll,0.05,'left')
title(['pure tones, P=' num2str(p)])

subplot(1,4,4)
plot(tetXBBNBefAll,tetXBBNAftAll,'ko','markerFaceColor','k')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXBBNBefAll,tetXBBNAftAll,0.05,'left')
title(['BBN, P=' num2str(p)])
