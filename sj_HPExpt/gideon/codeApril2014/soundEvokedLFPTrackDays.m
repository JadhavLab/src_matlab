epochTypes=[-1*ones(7,10);0 1 0 1 0 1 0 -1 -1 -1;0 1 0 0 1 0 -1 -1 -1 -1;0 1 0 1 0 1 0 -1 -1 -1;repmat([0 1 0 1 0 1 0 1 0 -1],4,1);0 0 1 0 1 0 1 0 1 0;repmat([0 1 0 1 0 1 0 1 0 -1],2,1)];
allAmps=[];
% in day 10 no stim, day 8+9 maybe noise
allSpeedAmps=[];
daySpeedAmps=[];
for day=[11:17]
    for epoch=find(epochTypes(day,:)==1)
        if day<10
            daystr=['0' num2str(day)];
        else
            daystr=num2str(day);
        end
        epochstr=num2str(epoch)
        
        [stimEEGtimesTrack1 stimIndsTrack1]=findStimTrackFirstInPair('/data15/gideon/Ndl/EEG/Ndleeg', daystr,epochstr,'22');
        
        [aleeg2responses2]=getResponsesLFPTrack('/data15/gideon/Ndl/EEG/Ndleeg',daystr,epochstr,1:21, stimEEGtimesTrack1,stimIndsTrack1,8);
        
        amps=[];for i=1:21,a=aleeg2responses2{i};amps=[amps mean(abs(a(:,1480:1560)),2)];end
        allAmps=[allAmps;amps];
        load(['/data15/gideon/Ndl/Ndlpos' daystr '.mat']);
        
        indsx1=lookup(stimEEGtimesTrack1(stimIndsTrack1),pos{day}{epoch}.data(:,1));
        speed1=pos{day}{epoch}.data(indsx1,5);
        
        daySpeedAmps=[daySpeedAmps; speed1 amps(:,1)];
        
    end

    [r p]=corrcoef(daySpeedAmps(:,1),daySpeedAmps(:,2));
    figure;
    subplot(2,1,1)
    plot(daySpeedAmps(:,1),daySpeedAmps(:,2),'kx');
    xlabel('Speed')
    ylabel('Response amplitude (integral of absolute LFP)')
    title(['day ' daystr ', R = ' num2str(r(1,2)) ' P= ' num2str(p(1,2))])
    subplot(2,1,2)
    slowind=find(daySpeedAmps(:,1)<6);
    fastind=find(daySpeedAmps(:,1)>5);
    [afast bfast]=hist(daySpeedAmps(fastind,2),0:10:max(daySpeedAmps(fastind,2)));
    [aslow bslow]=hist(daySpeedAmps(slowind,2),0:10:max(daySpeedAmps(fastind,2)));
    plot(bslow,aslow./sum(aslow),'linewidth',2)
    hold on
    plot(bfast,afast./sum(afast),'r','linewidth',2)
    xlabel('Response amplitude (integral of absolute LFP)')
    ylabel('Probability')
    legend('slow speed','fast speeds')
    allSpeedAmps=[allSpeedAmps; daySpeedAmps];
    keyboard
end

 [r p]=corrcoef(allSpeedAmps(:,1),allSpeedAmps(:,2));
    figure;
    subplot(2,1,1)
    plot(allSpeedAmps(:,1),allSpeedAmps(:,2),'kx');
    xlabel('Speed')
    ylabel('Response amplitude (integral of absolute LFP)')
    title(['All days, R = ' num2str(r(1,2)) ' P= ' num2str(p(1,2))])
    subplot(2,1,2)
    slowind=find(allSpeedAmps(:,1)<6);
    fastind=find(allSpeedAmps(:,1)>5);
    [afast bfast]=hist(allSpeedAmps(fastind,2),0:10:max(allSpeedAmps(fastind,2)));
    [aslow bslow]=hist(allSpeedAmps(slowind,2),0:10:max(allSpeedAmps(fastind,2)));
    plot(bslow,aslow./sum(aslow),'linewidth',2)
    hold on
    plot(bfast,afast./sum(afast),'r','linewidth',2)
    xlabel('Response amplitude (integral of absolute LFP)')
    ylabel('Probability')
    legend('slow speed','fast speeds')
    keyboard
%%

figure;
for i=[1:7]%,8:21]
    hold on;
    plot(smooth(allAmps(:,i),200),'b')
end
plot(smooth(mean(allAmps(:,1:7),2),200),'k','linewidth',2)
axis([50 2350 50 130])
title('AC tetrodes')
xlabel('# sound presentation')
ylabel('response amplitude')
%%

figure;
% 8 is ref, and 9 deteriorated 
for i=[10:14]
    hold on;
    plot(smooth(allAmps(:,i),200),'b')
end
plot(smooth(mean(allAmps(:,9:14),2),200),'k','linewidth',2)
axis([50 2350 50 130])
title('Hc tetrodes')
xlabel('# sound presentation')
ylabel('response amplitude')
%%
figure;
for i=[15:21]
    hold on;
    plot(smooth(allAmps(:,i),200),'b')
end
plot(smooth(mean(allAmps(:,15:21),2),200),'k','linewidth',2)
axis([50 2350 50 130])
title('PFC tetrodes')
xlabel('# sound presentation')
ylabel('response amplitude')