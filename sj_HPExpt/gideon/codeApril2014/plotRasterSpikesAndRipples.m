% this shows that on day 16, I think:
% one auditory cell fires at stim offset
% aud neurons fire trains before ripples

load('/data15/gideon/Ndl/Ndlripples16')
[riptimes] = getripples_direct([16, 4], ripples, [11:14],'minstd',3);
[stimEEGtimes stimInds]=findStimTrack('/data15/gideon/Ndl/EEG/Ndleeg', '16','4','22');
[allSpikes]=getResponsesSpikesSBCraig('/data15/gideon/Ndl/EEG/Ndleeg','16','4',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes,stimInds);
stimtimes=stimEEGtimes(stimInds);

numcells=length(allSpikes);
figure;
line([riptimes(:,1)';riptimes(:,1)'],[zeros(1,length(riptimes));(numcells+1)*ones(1,length(riptimes))],'color','m')
line([stimtimes;stimtimes],[zeros(1,length(stimtimes));(numcells+1)*ones(1,length(stimtimes))],'color','g')

hold on;
for i=1:length(allSpikes),
    % AC cells
    if allSpikes{i}.cellInd(3)<8
    plot(allSpikes{i}.spikeTimes,i,'ko','markerfacecolor','k'),hold on;
    % Hc cells
    elseif allSpikes{i}.cellInd(3)<15
    plot(allSpikes{i}.spikeTimes,i,'ro','markerfacecolor','r'),hold on;
    % PFC cells
    else
            plot(allSpikes{i}.spikeTimes,i,'bo','markerfacecolor','b'),hold on;
    end
end
%%
