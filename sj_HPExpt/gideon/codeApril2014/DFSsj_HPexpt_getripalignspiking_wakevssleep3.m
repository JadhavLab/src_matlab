
% Ver2, Dec 2013 - Implement Min. NSpike condition for PFC cells. See Line 47 and Line 240

% Ripple modulation of cells, especilally PFC cells. Time filter version of sj_HPexpt_ripalign_singlecell_getrip4.
% Will call DFAsj_getripalign.m
% Also see DFSsj_plotthetamod.m and DFSsj_HPexpt_xcorrmeasures2. Will gather data like these

clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 1; % Figure Options - Individual cells
savedirX = '/opt/data15/gideon/ProcessedData/';

% PFC, 4 animals
%val=1;savefile = [savedirX 'PFC_ripplemod_speed_minrip2_sleep']; area = 'PFC'; clr = 'b'; % PFC - low speed criterion

% AC, Nadal for now
val=2;savefile = [savedirX 'AC_ripplemod_speed_minrip2_sleep']; area = 'AC'; clr = 'b'; % PFC - low speed criterion

%val=7;savefile = [savedirX 'HP_ripplemod_CA1_alldata_speed_minrip2_feb14']; area = 'CA1'; clr = 'b'; % PFC - low speed criterion


savefig1=0;


% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


%If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    switch val
        case 1
    animals = {'HPa','HPb','HPc','Nadal'};
        case 2
    animals = {'Nadal'};
            
    end
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    % dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
        runepochfilter = 'isequal($type, ''run'')';

    %runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    
    sleepepochfilter = 'isequal($type, ''sleep'')'; %   
    
    
    % ====  NOTE: THERE IS NO AUDPROT=0 CONDITION HERE!!! ADD IT!!! 
    
    
    
    % Only pre and post sleep marked as sleep
    
    % Cell filter
    % -----------
    switch val
        case 1
            cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; % PFC cells with spiking criterion
        case 2
            cellfilter = 'strcmp($area, ''AC'') && ($numspikes > 100)'; % AC cells with spiking criterion
    
    end
    
    % cellfilter = 'strcmp($tag2, ''PFC'')';
    %cellfilter = 'strcmp($tag2, ''CA1Pyr'') && ($numspikes > 100)'; % This includes all.
    % For more exclusive choosing, use $tag. Take care of number of spikes while gathering data
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % The following are ripple time filter options. Instead of using time filters, within the function,
    % call getripples using the riptetfilter option to get the riptimes. Since you want a vector of riptimes.
    % You can also use inter-ripple-interval of 1 sec within the function, etc.
    
    % a) Using getriptimes: generates 1 vector with 1's for ripple times for all tetrodes.
    % Should be similar to getripples/ getripplees_direct
    
    %     timefilterrun_rip = {{'DFTFsj_getvelpos', '(($absvel <= 5))'},...
    %         {'DFTFsj_getriptimes','($nripples > 0)','tetfilter',riptetfilter,'minthresh',3}};
    
    % b) getripples. This is similar to getripltimes, but gives start and end of each ripple time
    % instead of a time filter. Also has an additional condition of at least 50 ms ripple.
    
    % Iterator
    % --------
    iterator = 'singlecellanal';
    
    % Filter creation
    % ----------------
    %     modf = createfilter('animal',animals,'days',dayfilter,'epochs',runepochfilter, 'cells',...
    %         cellfilter, 'iterator', iterator);
 
    modf = createfilter('animal',animals,'epochs',sleepepochfilter, 'cells',...
        cellfilter, 'iterator', iterator);
    modg = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter, 'iterator', iterator);
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    
%   switch val
 %      case 1
        modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'dospeed',1,'lowsp_thrs',4,'minrip',2); % Default stdev is 3
        modg = setfilterfunction(modg,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'dospeed',1,'lowsp_thrs',4,'minrip',2); % Default stdev is 3

    
   
  % end
    % Going to call getripples_tetinfo within function
    
    % Run analysis
    % ------------
    modf = runfilter(modf);
    modg = runfilter(modg);
    
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------



% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 0; savegatherdata = 0;
switch val
    case 1
        gatherdatafile = [savedirX 'PFC_ripplemod_speed_minrip2_sleep_gather']; % PFC cells to Hipp ripples
case 2
        gatherdatafile = [savedirX 'AC_ripplemod_speed_minrip2_sleep_gather']; % AC cells to Hipp ripples

end




if gatherdata
    
   
    % -------------- START WITH SLEEP-MODF
    
    cnt=0; % Count how many cells will be kept based on nspikes in output: >0
    allanimindex=[]; alldataraster=[]; alldatahist = []; all_Nspk=[];
    
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
            if (modf(an).output{1}(i).Nspikes > 0)
                cnt=cnt+1;
                anim_index{an}(cnt,:) = modf(an).output{1}(i).index;
                % Only indexes
                animindex=[an modf(an).output{1}(i).index]; % Put animal index in front
                allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
                % Data
                alldataraster{cnt} = modf(an).output{1}(i).rip_spks_cell; % Only get raster and histogram response
                alldatahist{cnt} = modf(an).output{1}(i).rip_spkshist_cell;
                all_Nspk(cnt) = modf(an).output{1}(i).Nspikes;
                alldataraster_rdm{cnt} = modf(an).output{1}(i).rdm_spks_cell; % Only get raster and histogram response
                alldatahist_rdm{cnt} = modf(an).output{1}(i).rdm_spkshist_cell;
                % trialResps: Summed Nspks/trial in respective window
                alldatatrialResps{cnt} = modf(an).output{1}(i).trialResps;
                alldatatrialResps_bck{cnt} = modf(an).output{1}(i).trialResps_bck;
                alldatatrialResps_rdm{cnt} = modf(an).output{1}(i).trialResps_rdm;
                % Nspikes summed across trials in response and bckgnd window
                all_Nspk_resp(cnt) = sum(modf(an).output{1}(i).trialResps);
                all_Nspk_bck(cnt) = sum(modf(an).output{1}(i).trialResps_bck);
                % Properties
                allcellfr(cnt) = modf(an).output{1}(i).cellfr;
                
                %end
                if cnt==1
                    pret =  modf(an).output{1}(i).pret;
                    postt = modf(an).output{1}(i).postt;
                    binsize = modf(an).output{1}(i).binsize;
                    rwin = modf(an).output{1}(i).rwin;
                    bckwin = modf(an).output{1}(i).bckwin;
                    bins_resp  = modf(an).output{1}(i).bins_resp;
                    bins_bck = modf(an).output{1}(i).bins_bck;
                    timeaxis = modf(an).output{1}(i).timeaxis;
                end
            end
        end
        
    end
    
    % Consolidate single cells across epochs. Multiple methods: see also DFSsj_getcellinfo and DFSsj_xcorrmeasures2
    % ----------------------------------------------------------------------------
    
    allripplemod = struct;
    
   
    
    % Method 2
    % ---------
    dummyindex=allanimindex;  % all anim-day-epoch-tet-cell indices
    cntcells=0;
    for i=1:length(alldatahist)
        animdaytetcell=allanimindex(i,[1 2 4 5]);
        ind=[];
        while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
            dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one
        end
        
        % Gather everything for the current cell across epochs
        currhist=[]; currraster=[]; currNspk=0; currNspk_resp=0; currNspk_bck=0; curr_cellfr=[];
        currhist_rdm=[]; currraster_rdm=[];
        currtrialResps=[]; currtrialResps_rdm=[]; currtrialResps_bck=[];
        for r=ind
            currNspk = currNspk + all_Nspk(r);
            currNspk_resp = currNspk_resp + all_Nspk_resp(r);
            currNspk_bck = currNspk_bck + all_Nspk_bck(r);
            currhist = [currhist; alldatahist{r}];
            currraster = [currraster, alldataraster{r}];
            currhist_rdm = [currhist_rdm; alldatahist_rdm{r}];
            currraster_rdm = [currraster_rdm, alldataraster_rdm{r}];
            currtrialResps = [currtrialResps, alldatatrialResps{r}];
            currtrialResps_rdm = [currtrialResps_rdm, alldatatrialResps_rdm{r}];
            currtrialResps_bck = [currtrialResps_bck, alldatatrialResps_bck{r}];
            curr_cellfr = [curr_cellfr; allcellfr(r)];
        end
        
        % Condition for Nspk. Version 1 had a min of 50 for entire window. Increase it to 100,
        % and can also add a condition for spikes in (resp+bck) window. Need to try a few values
        if (currNspk >= 50)
            %if ((currNspk_resp+currNspk_bck) >= 40)
            %if (currNspk >= 100) && ((currNspk_resp+currNspk_bck) >= 40)
            cntcells = cntcells + 1;
            % For Ndl-GIdeon;s animal, shift days
%             if animdaytetcell(1)==4
%                 animdaytetcell(2)=animdaytetcell(2)-7; % Day starts from no. 8
%             end
            allripplemod_idx(cntcells,:)=animdaytetcell;
            allripplemod(cntcells).index=animdaytetcell;
            allripplemod(cntcells).hist=currhist*(1000/binsize); % Convert to firing rate in Hz
            allripplemod(cntcells).raster=currraster;
            allripplemod(cntcells).Nspk=currNspk;
            allripplemod(cntcells).hist_rdm=currhist_rdm*(1000/binsize); % Convert to firing rate in Hz
            allripplemod(cntcells).raster_rdm=currraster_rdm;
            % Trial Resps
            allripplemod(cntcells).trialResps = currtrialResps';
            allripplemod(cntcells).trialResps_rdm = currtrialResps_rdm';
            allripplemod(cntcells).trialResps_bck = currtrialResps_bck';
            % Properties
            allripplemod(cntcells).cellfr = nanmean(curr_cellfr);
        end
    end
    
   allripplemodsleep=allripplemod;
   
   % -------------- NOW WAKE-MODG
    
    cnt=0; % Count how many cells will be kept based on nspikes in output: >0
    allanimindex=[]; alldataraster=[]; alldatahist = []; all_Nspk=[];
    
    for an = 1:length(modg)
        for i=1:length(modg(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
            if (modg(an).output{1}(i).Nspikes > 0)
                cnt=cnt+1;
                anim_index{an}(cnt,:) = modg(an).output{1}(i).index;
                % Only indexes
                animindex=[an modg(an).output{1}(i).index]; % Put animal index in front
                allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
                % Data
                alldataraster{cnt} = modg(an).output{1}(i).rip_spks_cell; % Only get raster and histogram response
                alldatahist{cnt} = modg(an).output{1}(i).rip_spkshist_cell;
                all_Nspk(cnt) = modg(an).output{1}(i).Nspikes;
                alldataraster_rdm{cnt} = modg(an).output{1}(i).rdm_spks_cell; % Only get raster and histogram response
                alldatahist_rdm{cnt} = modg(an).output{1}(i).rdm_spkshist_cell;
                % trialResps: Summed Nspks/trial in respective window
                alldatatrialResps{cnt} = modg(an).output{1}(i).trialResps;
                alldatatrialResps_bck{cnt} = modg(an).output{1}(i).trialResps_bck;
                alldatatrialResps_rdm{cnt} = modg(an).output{1}(i).trialResps_rdm;
                % Nspikes summed across trials in response and bckgnd window
                all_Nspk_resp(cnt) = sum(modg(an).output{1}(i).trialResps);
                all_Nspk_bck(cnt) = sum(modg(an).output{1}(i).trialResps_bck);
                % Properties
                allcellfr(cnt) = modg(an).output{1}(i).cellfr;
                
                %end
                if cnt==1
                    pret =  modg(an).output{1}(i).pret;
                    postt = modg(an).output{1}(i).postt;
                    binsize = modg(an).output{1}(i).binsize;
                    rwin = modg(an).output{1}(i).rwin;
                    bckwin = modg(an).output{1}(i).bckwin;
                    bins_resp  = modg(an).output{1}(i).bins_resp;
                    bins_bck = modg(an).output{1}(i).bins_bck;
                    timeaxis = modg(an).output{1}(i).timeaxis;
                end
            end
        end
        
    end
    
    % Consolidate single cells across epochs. Multiple methods: see also DFSsj_getcellinfo and DFSsj_xcorrmeasures2
    % ----------------------------------------------------------------------------
    
    allripplemod = struct;
    
   
    
    % Method 2
    % ---------
    dummyindex=allanimindex;  % all anim-day-epoch-tet-cell indices
    cntcells=0;
    for i=1:length(alldatahist)
        animdaytetcell=allanimindex(i,[1 2 4 5]);
        ind=[];
        while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
            dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one
        end
        
        % Gather everything for the current cell across epochs
        currhist=[]; currraster=[]; currNspk=0; currNspk_resp=0; currNspk_bck=0; curr_cellfr=[];
        currhist_rdm=[]; currraster_rdm=[];
        currtrialResps=[]; currtrialResps_rdm=[]; currtrialResps_bck=[];
        for r=ind
            currNspk = currNspk + all_Nspk(r);
            currNspk_resp = currNspk_resp + all_Nspk_resp(r);
            currNspk_bck = currNspk_bck + all_Nspk_bck(r);
            currhist = [currhist; alldatahist{r}];
            currraster = [currraster, alldataraster{r}];
            currhist_rdm = [currhist_rdm; alldatahist_rdm{r}];
            currraster_rdm = [currraster_rdm, alldataraster_rdm{r}];
            currtrialResps = [currtrialResps, alldatatrialResps{r}];
            currtrialResps_rdm = [currtrialResps_rdm, alldatatrialResps_rdm{r}];
            currtrialResps_bck = [currtrialResps_bck, alldatatrialResps_bck{r}];
            curr_cellfr = [curr_cellfr; allcellfr(r)];
        end
        
        % Condition for Nspk. Version 1 had a min of 50 for entire window. Increase it to 100,
        % and can also add a condition for spikes in (resp+bck) window. Need to try a few values
        if (currNspk >= 50)
            %if ((currNspk_resp+currNspk_bck) >= 40)
            %if (currNspk >= 100) && ((currNspk_resp+currNspk_bck) >= 40)
            cntcells = cntcells + 1;
            % For Ndl-GIdeon;s animal, shift days
%             if animdaytetcell(1)==4
%                 animdaytetcell(2)=animdaytetcell(2)-7; % Day starts from no. 8
%             end
            allripplemod_idx(cntcells,:)=animdaytetcell;
            allripplemod(cntcells).index=animdaytetcell;
            allripplemod(cntcells).hist=currhist*(1000/binsize); % Convert to firing rate in Hz
            allripplemod(cntcells).raster=currraster;
            allripplemod(cntcells).Nspk=currNspk;
            allripplemod(cntcells).hist_rdm=currhist_rdm*(1000/binsize); % Convert to firing rate in Hz
            allripplemod(cntcells).raster_rdm=currraster_rdm;
            % Trial Resps
            allripplemod(cntcells).trialResps = currtrialResps';
            allripplemod(cntcells).trialResps_rdm = currtrialResps_rdm';
            allripplemod(cntcells).trialResps_bck = currtrialResps_bck';
            % Properties
            allripplemod(cntcells).cellfr = nanmean(curr_cellfr);
        end
    end
    
   allripplemodwake=allripplemod;
    
   
 
    
    
    
    % QUANTIFYING RIPPLE RESPONSIVENESS, SLEEP
    %varRange=[400:700];
    varRange=[300:700];
    for ii=1:length(allripplemodsleep)
        ii
        curRast=allripplemodsleep(ii).raster;
        numRips=length(curRast);
        % creating a raster matrix of 0/1 with 1ms resolution 
        % it goes from -550 t0 +550ms relative to ripples
        curRastMat=zeros(numRips,1100);
        for i=1:numRips,curRastMat(i,round(curRast{i}+551))=1;end    
        
       
        bckVarShufs=[]; respbckratioShufs=[];
        % --- Old, no longer nec ---
        respVarShufs=[];
        numRuns=500;
        allShufMeans=[];
        % shuffling each trial in the raster separately in time,
        % cyclically. Doing that numRuns times (currently 1000).
        for runs=1:numRuns
            
            curRastMatShuf=zeros(numRips,1100);            
            for i=1:numRips
                shiftVal=round(rand(1)*1000);
                curRastMatShuf(i,1+mod(round(curRast{i}+551+shiftVal),1100))=1;
            end
            
            % shuffled "psth"
            meanRespShuf=smooth(mean(curRastMatShuf(:,50:end-50)),50);
            allShufMeans=[allShufMeans; meanRespShuf'];
            %             plot(smooth(mean(curRastMatShuf(:,50:end-50)),50),'r')
            %             hold on;
            
            % for each shuffled psth, calculate the variance (mean squared
            % distance from mean)
            respVarShufs=[respVarShufs var(meanRespShuf(varRange))];
            
        end
        
        meanResp=smooth(mean(curRastMat(:,50:1050)),50);
        meanRespRange=meanResp(varRange);
        meanRespShuf=(mean(allShufMeans));
        meanRespShufRange= meanRespShuf(varRange);
        
        %old measure for response: the variance of the "real" psth
        respVar=var(meanResp(varRange));
        %new measure: instead of mean squared distance from its own mean,
        %mean squared distance from the mean shuffled psth's
        respVar2=mean((meanRespRange-meanRespShufRange').^2);
        % this is used to get a positive deviation from mean
      
        varPSTH=(meanResp-meanRespShuf').^2;
      
        rasterShufP=1-sum(respVar>respVarShufs)/numRuns;
        rasterShufP2=1-sum(respVar2>respVarShufs)/numRuns;      
              
        allripplemodsleep(ii).rasterShufP2=rasterShufP2;
        
        varRespAmp2=respVar2/mean(respVarShufs);
        
        allripplemodsleep(ii).varRespAmp2=varRespAmp2;
        allripplemodsleep(ii).varPSTH=varPSTH;
       
    end
    
    
    % QUANTIFYING RIPPLE RESPONSIVENESS, WAKE
    %varRange=[400:700];
    varRange=[300:700];
    for ii=1:length(allripplemodwake)
        ii
        curRast=allripplemodwake(ii).raster;
        numRips=length(curRast);
        % creating a raster matrix of 0/1 with 1ms resolution 
        % it goes from -550 t0 +550ms relative to ripples
        curRastMat=zeros(numRips,1100);
        for i=1:numRips,curRastMat(i,round(curRast{i}+551))=1;end    
        
       
        respVarShufs=[];
        numRuns=500;
        allShufMeans=[];
        % shuffling each trial in the raster separately in time,
        % cyclically. Doing that numRuns times (currently 1000).
        for runs=1:numRuns
            
            curRastMatShuf=zeros(numRips,1100);            
            for i=1:numRips
                shiftVal=round(rand(1)*1000);
                curRastMatShuf(i,1+mod(round(curRast{i}+551+shiftVal),1100))=1;
            end
            
            % shuffled "psth"
            meanRespShuf=smooth(mean(curRastMatShuf(:,50:end-50)),50);
            allShufMeans=[allShufMeans; meanRespShuf'];
            %             plot(smooth(mean(curRastMatShuf(:,50:end-50)),50),'r')
            %             hold on;
            
            % for each shuffled psth, calculate the variance (mean squared
            % distance from mean)
            respVarShufs=[respVarShufs var(meanRespShuf(varRange))];
        
        end
        
        meanResp=smooth(mean(curRastMat(:,50:1050)),50);
        meanRespRange=meanResp(varRange);
        meanRespShuf=(mean(allShufMeans));
        meanRespShufRange= meanRespShuf(varRange);
        
        %old measure for response: the variance of the "real" psth
        respVar=var(meanResp(varRange));
        %new measure: instead of mean squared distance from its own mean,
        %mean squared distance from the mean shuffled psth's
        respVar2=mean((meanRespRange-meanRespShufRange').^2);
        % this is used to get a positive deviation from mean
      
        varPSTH=(meanResp-meanRespShuf').^2;
      
        rasterShufP=1-sum(respVar>respVarShufs)/numRuns;
        rasterShufP2=1-sum(respVar2>respVarShufs)/numRuns;      
   
        allripplemodwake(ii).rasterShufP2=rasterShufP2;
        
       % varRespAmp=respVar/mean(respVarShufs);
        varRespAmp2=respVar2/mean(respVarShufs);
        
       % allripplemod(ii).varRespAmp=varRespAmp;
        allripplemodwake(ii).varRespAmp2=varRespAmp2;
        allripplemodwake(ii).varPSTH=varPSTH;
       
    end
    
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data


allripplemod_idxW=[];
for i=1:length(allripplemodwake),allripplemod_idxW=[allripplemod_idxW;allripplemodwake(i).index];end
allripplemod_idxS=[];
for i=1:length(allripplemodsleep),allripplemod_idxS=[allripplemod_idxS;allripplemodsleep(i).index];end

allinds=unique([allripplemod_idxW;allripplemod_idxS],'rows');

allhistswake=[];
allhistssleep=[];
 scrsz = get(0,'ScreenSize');
 % ripple excited/inhibited 1/0
 excinhwake=[];
 excinhsleep=[];
 % gathering wake and sleep rip-trig psth's  
 for v=1:size(allinds,1)
     curidx=allinds(v,:);%animal day
     ripidxW=find(ismember(allripplemod_idxW,curidx,'rows'));
     ripidxS=find(ismember(allripplemod_idxS,curidx,'rows'));
     if ~isempty(ripidxW) & ~isempty(ripidxS)
         curTraceAwake=zscore(mean(allripplemodwake(1,ripidxW).hist));
         allhistswake=[allhistswake;curTraceAwake];
         if mean(curTraceAwake(50:60))<mean(curTraceAwake(10:40)) % rip-inh
             excinhwake=[excinhwake 0];
         else
         excinhwake=[excinhwake 1];
             
         end
         
         curTraceSleep=zscore(mean(allripplemodsleep(1,ripidxS).hist));
         allhistssleep=[allhistssleep;curTraceSleep];
     if mean(curTraceSleep(50:60))<mean(curTraceSleep(10:40)) % rip-inh
             excinhsleep=[excinhsleep 0];
         else
         excinhsleep=[excinhsleep 1];
             
         end
     
     end
 end
 
 % determining rip-trig significance
 
sigsleep=[];
sigwake=[];
days1=[];
for v=1:size(allinds,1)
    curidx=allinds(v,:);%animal day
    ripidxW=find(ismember(allripplemod_idxW,curidx,'rows'));
    ripidxS=find(ismember(allripplemod_idxS,curidx,'rows'));
    if ~isempty(ripidxW) & ~isempty(ripidxS)
        curind=allripplemodwake(1,ripidxW).index;
        % Adjust for Nadal
        if (val==1&curind(1)==4)  | val==2&curind(1)==1
            days1=[days1 curind(2)-7];
        
            
        else
        days1=[days1 curind(2)];
            
        end
        if allripplemodwake(1,ripidxW).rasterShufP2<.05
            sigwake=[sigwake 1];
        else
            sigwake=[sigwake 0];
        end
        if allripplemodsleep(1,ripidxS).rasterShufP2<.05
            sigsleep=[sigsleep 1];
        else
            sigsleep=[sigsleep 0];
        end
    end
end
x1=linspace(-500,500,101)

% all cells
figure('Position',[900 200 scrsz(3)/3 scrsz(4)/2])
ww1=subplot(1,2,1);imagesc(allhistswake);title('Awake');ww2=subplot(1,2,2);imagesc(allhistssleep);title('Sleep')
linkaxes([ww1 ww2],'xy');
figure;errorbar(1:101,mean(abs(zscore(allhistssleep')')),std(abs(zscore(allhistssleep')'))/sqrt(size(allhistssleep,1)));
hold on
errorbar(1:101,mean(abs(zscore(allhistswake')')),std(abs(zscore(allhistswake')'))/sqrt(size(allhistswake,1)),'r');
title('all, zscored abs')
% all cells, blanking out non-significant ones
awakesigim=NaN(size(allhistswake));
awakesigim(find(sigwake),:)=allhistswake(find(sigwake),:);
sleepsigim=NaN(size(allhistssleep));
sleepsigim(find(sigsleep),:)=allhistssleep(find(sigsleep),:);
figure('Position',[900 200 scrsz(3)/3 scrsz(4)/2])
ww3=subplot(1,2,1);imagesc(x1,1:size(awakesigim,1),awakesigim);title('Awake');
xlabel('Time (ms)')
ylabel('cells')
ww4=subplot(1,2,2);imagesc(x1,1:size(sleepsigim,1),sleepsigim);title('Sleep')
linkaxes([ww3 ww4],'xy');
xlabel('Time (ms)')
ylabel('cells')

% sig cells
figure('Position',[900 200 scrsz(3)/3 scrsz(4)/2])
subplot(1,2,1);
imagesc(allhistswake(sigwake==1,:))
title('Awake (sig)')
subplot(1,2,2);
imagesc(allhistssleep(sigsleep==1,:))
title('Sleep (sig)')
allhistssleepSig=allhistssleep(sigsleep==1,:);
allhistswakeSig=allhistswake(sigwake==1,:);
figure;errorbar(x1,mean(abs(zscore(allhistssleepSig')')),std(abs(zscore(allhistssleepSig')'))/sqrt(size(allhistssleepSig,1)));
hold on
errorbar(x1,mean(abs(zscore(allhistswakeSig')')),std(abs(zscore(allhistswakeSig')'))/sqrt(size(allhistswakeSig,1)),'r');
legend('Sleep','Awake')
title('only sig, zscored abs')

figure;errorbar(x1,mean(allhistssleepSig),std(allhistssleepSig)/sqrt(size(allhistssleepSig,1)));
hold on
errorbar(x1,mean(allhistswakeSig),std(allhistswakeSig)/sqrt(size(allhistswakeSig,1)),'r');
legend('Sleep','Awake')
title('only sig')

rateSleepRipSig=mean(sigsleep);
rateWakeRipSig=mean(sigwake);

% Rip-exc/inh SLEEP
figure
subplot(3,1,1)
imagesc(allhistssleep(sigsleep==1&excinhsleep==1,:))
subplot(3,1,2)
imagesc(allhistssleep(sigsleep==1&excinhsleep==0,:))
subplot(3,1,3)
plot(mean(allhistssleep(sigsleep==1&excinhsleep==1,:)))
hold on
plot(mean(allhistssleep(sigsleep==1&excinhsleep==0,:)))
legend('exc','inh')
title('sleep')

% Rip-exc/inh WAKE
figure
subplot(3,1,1)
imagesc(allhistswake(sigwake==1&excinhwake==1,:))
subplot(3,1,2)
imagesc(allhistswake(sigwake==1&excinhwake==0,:))
subplot(3,1,3)
plot(mean(allhistswake(sigwake==1&excinhwake==1,:)))
hold on
plot(mean(allhistswake(sigwake==1&excinhwake==0,:)))
legend('exc','inh')
title('wake')

% rip-exc/inh, sleep vs. wake
figure;plot(mean(allhistswake(sigwake==1&excinhwake==1,:)))
hold on
plot(mean(allhistssleep(sigsleep==1&excinhsleep==1,:)),'r')
figure;plot(mean(allhistswake(sigwake==1&excinhwake==0,:)))
hold on
plot(mean(allhistssleep(sigsleep==1&excinhsleep==0,:)),'r')


% rip exc, wake, by days
 figure;plot(mean(allhistswake(sigwake==1&excinhwake==1&days1<=2,:))','k')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==1&days1>=3&days1<=4,:))','b')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==1&days1>=5&days1<=6,:))','m')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==1&days1>=7&days1<=8,:))','r')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==1&days1>=9,:))','g')
legend('1-2','3-4','5-6','7-8','9-10')
title('rip exc, awake')

% rip inh, wake, by days
 figure;plot(mean(allhistswake(sigwake==1&excinhwake==0&days1<=2,:))','k')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==0&days1>=3&days1<=4,:))','b')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==0&days1>=5&days1<=6,:))','m')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==0&days1>=7&days1<=8,:))','r')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==0&days1>=9,:))','g')
legend('1-2','3-4','5-6','7-8','9-10')
title('rip inh, awake')

% rip exc, sleep, by days
 figure;plot(mean(allhistssleep(sigsleep==1&excinhsleep==1&days1<=2,:))','k')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==1&days1>=3&days1<=4,:))','b')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==1&days1>=5&days1<=6,:))','m')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==1&days1>=7&days1<=8,:))','r')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==1&days1>=9,:))','g')
legend('1-2','3-4','5-6','7-8','9-10')
title('rip exc, sleep')

% rip inh, sleep, by days
 figure;plot(mean(allhistssleep(sigsleep==1&excinhsleep==0&days1<=2,:))','k')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==0&days1>=3&days1<=4,:))','b')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==0&days1>=5&days1<=6,:))','m')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==0&days1>=7&days1<=8,:))','r')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==0&days1>=9,:))','g')
legend('1-2','3-4','5-6','7-8','9-10')
title('rip inh, sleep')
%---------
% rip exc, wake, by days
 figure;plot(mean(allhistswake(sigwake==1&excinhwake==1&days1<=3,:))','k')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==1&days1>=4&days1<=6,:))','b')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==1&days1>=7,:))','r')
legend('1-3','4-6','>=7')
title('rip exc, awake')

% rip inh, wake, by days
 figure;plot(mean(allhistswake(sigwake==1&excinhwake==0&days1<=3,:))','k')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==0&days1>=4&days1<=6,:))','b')
hold on;plot(mean(allhistswake(sigwake==1&excinhwake==0&days1>=7,:))','r')
legend('1-3','4-6','>=7')
title('rip inh, awake')

% rip exc, sleep, by days
 figure;plot(mean(allhistssleep(sigsleep==1&excinhsleep==1&days1<=3,:))','k')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==1&days1>=4&days1<=6,:))','b')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==1&days1>=7,:))','r')
legend('1-3','4-6','>=7')
title('rip exc, sleep')

% rip inh, sleep, by days
 figure;plot(mean(allhistssleep(sigsleep==1&excinhsleep==0&days1<=3,:))','k')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==0&days1>=4&days1<=6,:))','b')
hold on;plot(mean(allhistssleep(sigsleep==1&excinhsleep==0&days1>=7,:))','r')
legend('1-3','4-6','>=7')
title('rip inh, sleep')
%--------
[s ss]=sort(days1,'ascend');
% imagesc by days
allhistswakedays=(allhistswake(ss,:));
allhistssleepdays=(allhistssleep(ss,:));
sigsleepdays=sigsleep(ss);
excinhsleepdays=excinhsleep(ss);
sigwakedays=sigwake(ss);
excinhwakedays=excinhwake(ss);

% Rip-exc/inh SLEEP
figure
subplot(2,1,1)
imagesc(x1,s(find(sigsleepdays==1&excinhsleepdays==1)),allhistssleepdays(sigsleepdays==1&excinhsleepdays==1,:))
subplot(2,1,2)
imagesc(x1,s(find(sigsleepdays==1&excinhsleepdays==0)),allhistssleepdays(sigsleepdays==1&excinhsleepdays==0,:))
title('sleep')
% Rip-exc/inh AWAKE
figure
subplot(2,1,1)
imagesc(x1,s(find(sigwakedays==1&excinhwakedays==1)),allhistswakedays(sigwakedays==1&excinhwakedays==1,:))
subplot(2,1,2)
imagesc(x1,s(find(sigwakedays==1&excinhwakedays==0)),allhistswakedays(sigwakedays==1&excinhwakedays==0,:))
title('wake')
keyboard
%---
% plotting for individual cells
 scrsz = get(0,'ScreenSize');

for v=1:size(allinds,1)
    curidx=allinds(v,:);%animal day
    ripidxW=find(ismember(allripplemod_idxW,curidx,'rows'));
    ripidxS=find(ismember(allripplemod_idxS,curidx,'rows'));
    if ~isempty(ripidxW) & ~isempty(ripidxS)
        rstW=allripplemodwake(1,ripidxW).raster;
        rstS=allripplemodsleep(1,ripidxS).raster;
figure('Position',[900 200 scrsz(3)/10 scrsz(4)/1.5])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 12 20])

% TO PLOT THE RASTERS, UNCOMMENT:
subplot(3,1,1)
plotRaster1(rstW,rwin,pret,postt)
title(['cell ' num2str(curidx) ' Wake'])
subplot(3,1,2)
plotRaster1(rstS,rwin,pret,postt)
title(['cell ' num2str(curidx) ' Sleep'])
subplot(3,1,3)
x1=linspace(-500,500,101);
plot(x1,mean(allripplemodwake(1,ripidxW).hist),'r','linewidth',3);
hold on
plot(x1,mean(allripplemodwake(1,ripidxW).hist)+std(allripplemodwake(1,ripidxW).hist)/sqrt(size(allripplemodwake(1,ripidxW).hist,1)),'r--','linewidth',1);
plot(x1,mean(allripplemodwake(1,ripidxW).hist)-std(allripplemodwake(1,ripidxW).hist)/sqrt(size(allripplemodwake(1,ripidxW).hist,1)),'r--','linewidth',1);
 
plot(x1,mean(allripplemodsleep(1,ripidxS).hist),'linewidth',3)
plot(x1,mean(allripplemodsleep(1,ripidxS).hist)+std(allripplemodsleep(1,ripidxS).hist)/sqrt(size(allripplemodsleep(1,ripidxS).hist,1)),'b--','linewidth',1);
plot(x1,mean(allripplemodsleep(1,ripidxS).hist)-std(allripplemodsleep(1,ripidxS).hist)/sqrt(size(allripplemodsleep(1,ripidxS).hist,1)),'b--','linewidth',1);

 legend('wake','+sem','-sem','sleep','+sem','-sem')
 xlabel('Time (ms)')
%saveas(gcf,['/opt/data15/gideon/ripmodsleepwakeAC/ripmodsleepwakeWNORASTERS' num2str(v) '.jpg'])
% only sleep
figure('Position',[900 200 scrsz(3)/10 scrsz(4)/1.5])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 10])
subplot(2,1,1)
plotRaster1(rstS,rwin,pret,postt)
subplot(2,1,2)

plot(x1,mean(allripplemodsleep(1,ripidxS).hist),'linewidth',5)
hold on
plot(x1,mean(allripplemodsleep(1,ripidxS).hist)+std(allripplemodsleep(1,ripidxS).hist)/sqrt(size(allripplemodsleep(1,ripidxS).hist,1)),'b--','linewidth',2);
plot(x1,mean(allripplemodsleep(1,ripidxS).hist)-std(allripplemodsleep(1,ripidxS).hist)/sqrt(size(allripplemodsleep(1,ripidxS).hist,1)),'b--','linewidth',2);
cury=ylim;
ylim([0 cury(2)]);
 ypts=0:0.1:cury(2);
xpts = 0*ones(size(ypts));
plot(xpts , ypts, 'r--','Linewidth',2);
saveas(gcf,['/opt/data15/gideon/Figs/RipTrigRasters3/ripmodsleep' num2str(v) '.jpg'])
 saveas(gcf,['/opt/data15/gideon/Figs/RipTrigRasters3/ripmodsleep' num2str(v) '.pdf'])
  saveas(gcf,['/opt/data15/gideon/Figs/RipTrigRasters3/ripmodsleep' num2str(v) '.fig'])

%saveas(gcf,['/opt/data15/gideon/ripmodsleepwakeAC/ripmodsleepwakeWNORASTERSONLYSLEEPB' num2str(v) '.tif'])

% subplot(2,1,1)
% ylim([500 700])
% saveas(gcf,['/opt/data15/gideon/Figs/RipTrigRasters3/ripmodsleepsubgroup' num2str(v) '.jpg'])
%  saveas(gcf,['/opt/data15/gideon/Figs/RipTrigRasters3/ripmodsleepsubgroup' num2str(v) '.pdf'])
 
close all
end
%keyboard

end
keyboard
% ------------------------------
% Plotting for individual cells
% ------------------------------
%%

% NOTICE HERE
allripplemod=allripplemodsleep;

figdir = '/opt/data15/gideon/Figs/RipTrigRasters2/'; saveg1=1;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
forppr=1;
if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',40);
    tfont = 40;
    xfont = 40;
    yfont = 40;
end

figopt1=1;
if (figopt1)
    for i=1:cntcells
        curridx = allripplemod(i).index;
        currhist = allripplemod(i).hist;
        currraster = allripplemod(i).raster;
      % sig_shuf = allripplemod(i).sig_shuf; % Sig or not
      % sig_ttest = allripplemod(i).sig_ttest; % Sig or not
      % modln_shuf = allripplemod(i).modln_shuf; % %tile value of shuffle
      % modln_peak = allripplemod(i).modln_peak; % %tage peak change over baseline
      % modln = allripplemod(i).modln; % %tage mean change over baseline
        
        currNspk = allripplemod(i).Nspk;
        
        % New
    %   modln_var = allripplemod(i).varRespAmp;
     %  p_var = allripplemod(i).rasterShufP;
       modln_var2 = allripplemod(i).varRespAmp2;
        p_var2 = allripplemod(i).rasterShufP2;
  %      sigvar=0; if p_var<0.05, sigvar=1; end
        sigvar2=0; if p_var2<0.05, sigvar2=1; end
        cellfr = allripplemod(i).cellfr;
        
        rip_spkshist_cellsort_PFC = currhist; rip_spks_cellsort_PFC = currraster;
        day = curridx(2); tet = curridx(3); cell = curridx(4);
        switch curridx(1)
            case 1
                prefix = 'HPa';
            case 2
                prefix = 'HPb';
            case 3
                prefix = 'HPc';
            case 4
                prefix = 'Ndl';
        end
        
        % To control plotting
        %if sigvar==0 && sig_shuf==1
        %if curridx(1)==4 && curridx(2)==10 && curridx(3)==17 && curridx(4)==4
        if sigvar2==1 
        
            % 1) Raster
            % ----------
            figure; hold on; redimscreen_figforppt1;
            set(gcf,'Position',[100 130 1000 950]);
            %set(gcf, 'Position',[205 658 723 446]);
            subplot(2,1,1); hold on;
            spkcount = [];
            for c=1:length(rip_spks_cellsort_PFC)
                tmps = rip_spks_cellsort_PFC{c};
                
                %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',2,'Color','k');
                plot(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),'k.','MarkerSize',16);
                %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
                
                % Get count of spikes in response window
                if ~isempty(tmps)
                    subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
                    spkcount = [spkcount; length(subset_tmps)];
                end
            end
            set(gca,'XLim',[-pret postt]);
            set(gca,'XTick',[])
            %set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
            xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
            ylabel('SWR number','FontSize',yfont,'Fontweight','normal');
            set(gca,'YLim',[0 size(rip_spkshist_cellsort_PFC,1)]);
            % Plot Line at 0 ms and rwin
            ypts = 0:1:size(rip_spkshist_cellsort_PFC,1);
            xpts = 0*ones(size(ypts));
            plot(xpts , ypts, 'k--','Linewidth',2);
            % Plot lines at rwin
            %             xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
            %             xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
            %             xpts = bckwin(1)*ones(size(ypts)); plot(xpts , ypts, 'g--','Linewidth',1);
            %             xpts = bckwin(2)*ones(size(ypts)); plot(xpts , ypts, 'g--','Linewidth',1);
            %
            title(sprintf('%s Day %d Tet %d Cell %d Nspkwin %d FR %g', prefix, day, tet, cell, sum(spkcount), roundn(cellfr,-1)),...
                'FontSize',tfont,'Fontweight','normal');
%             if saveg1==1,
%                 figfile = [figdir,'RippleAlignRaster_',prefix,'_Day',num2str(day),'_Tet',num2str(tet),'_Cell',num2str(cell)];
%                 print('-dpdf', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
%             end
            %close(1);
            
            % Hist
            % ----
            %figure; hold on; redimscreen_figforppt1;
            %set(gcf, 'Position',[205 136 723 446]);
            subplot(2,1,2); hold on;
            xaxis = -pret:binsize:postt;
            plot(xaxis,mean(rip_spkshist_cellsort_PFC),'k-','Linewidth',4);
            %plot(xaxis,mean(rip_spkshist_cellsort_PFC)+sem(rip_spkshist_cellsort_PFC),'b--','Linewidth',1);
            %plot(xaxis,mean(rip_spkshist_cellsort_PFC)-sem(rip_spkshist_cellsort_PFC),'b--','Linewidth',1);
            
            set(gca,'XLim',[-pret postt]);
            xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
            ylabel('Firing rate (Hz)','FontSize',yfont,'Fontweight','normal');
            %set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
            set(gca,'XTick',[-500,-250,0,250,500],'XTickLabel',num2str([-500,-250,0,250,500]'));
            ylow = min(mean(rip_spkshist_cellsort_PFC)-sem(rip_spkshist_cellsort_PFC));
            yhigh = max(mean(rip_spkshist_cellsort_PFC)+sem(rip_spkshist_cellsort_PFC));
            set(gca,'YLim',[ylow-0.1 yhigh+0.1]);
            ypts = ylow-0.1:0.1:yhigh+0.1;
            xpts = 0*ones(size(ypts));
            % Plot Line at 0 ms - Onset of stimulation
            plot(xpts , ypts, 'k--','Linewidth',2);
            % Plot lines at rwin and bckwi
            %             xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
            %             xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
            %             xpts = bckwin(1)*ones(size(ypts)); plot(xpts , ypts, 'g--','Linewidth',1);
            %             xpts = bckwin(2)*ones(size(ypts)); plot(xpts , ypts, 'g--','Linewidth',1);
            
       %     if sig_ttest ==1, str = '*'; else, str = ''; end
        %    if sig_shuf ==1, str_shuf = '*'; else, str_shuf = ''; end
         %   if sigvar ==1, str_var = '*'; else, str_var = ''; end
          %  if sigvar2 ==1, str_var2 = '*'; else, str_var2 = ''; end
            %         title(sprintf('%s Day%d Tet%d Cell%d: M %g%s Prc %g%s', prefix, day, tet, cell, roundn(modln_peak,-1),...
            %             str, roundn(modln_shuf,-2), str_shuf),'FontSize',tfont,'Fontweight','normal');
            title(sprintf('M %g%s Mvar %g%s Mvar2 %g%s', roundn(modln_peak,-1),...
                str_shuf, roundn(modln_var,-1), str_var, roundn(modln_var2,-1), str_var2),'FontSize',tfont,'Fontweight','normal');
            if saveg1==1,
                figfile = [figdir,'RippleAlignHist_',prefix,'_Day',num2str(day),'_Tet',num2str(tet),'_Cell',num2str(cell)];
                print('-dpdf', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
            end
            
            keyboard;
            
            close all
            
        end % if curridx and sig_shuf
        
        
        
    end % end cntcells
end % end if figopt









% ------------------
% Population Figures
% ------------------

% Population Data
% ----------------------------
cntsig = 0; cntnosig = 0;
allsigmodln_peak = []; allsigmodln_shuf = [];  allsighist = [];
allnosigmodln_peak = []; allnosigmodln_shuf = [];  allnosighist = [];
allSigVarPSTHs=[];
allNoSigVarPSTHs=[];
% for nadal
alldays(allanim==4)=alldays(allanim==4)-7;
days = unique(alldays);
%anim = unique(allanim);
ncells_days = zeros(length(days),1);
ncells_days_sig = zeros(length(days),1);
allsig_var2=[];for i=1:length(allripplemod),allsig_var2=[allsig_var2 allripplemod(i).rasterShufP2<0.05];end

for i = 1:length(allripplemod)
    currday = allripplemod(i).days;
    %FOR NADAL
    if allripplemod(i).anim==4, 
        currday=currday-7;
    end
    ncells_days(currday) = ncells_days(currday)+1;
    
    if ( allsig_var2(i) == 1)
        cntsig = cntsig+1;
        ncells_days_sig(currday) = ncells_days_sig(currday)+1;
        allsigmodln_peak(cntsig) = allripplemod(i).modln_peak;
        allsigmodln_shuf(cntsig) = allripplemod(i).modln_shuf;
        allsighist(cntsig,:) = mean(allripplemod(i).hist,1);
        allSigVarPSTHs(cntsig,:)=allripplemod(i).varPSTH;
    else
        cntnosig = cntnosig+1;
        allnosigmodln_peak(cntnosig) = allripplemod(i).modln_peak;
        allnosigmodln_shuf(cntnosig) = allripplemod(i).modln_shuf;
        allnosighist(cntnosig,:) = mean(allripplemod(i).hist,1);
       allNoSigVarPSTHs(cntnosig,:)=allripplemod(i).varPSTH;

    end
end
keyboard

% these were saved after running the current code for PFC and CA1, and then
% allSigVarPSTHsCA1=allSigVarPSTHs;
% allNoSigVarPSTHsCA1=allNoSigVarPSTHs;
% and similarly for PFC
load pfcPSTHs
load ca1PSTHs
figure;plot(mean(allSigVarPSTHsPFC));hold on;plot(mean(allSigVarPSTHsCA1),'r')
figure;plot(xcorr(mean(allSigVarPSTHsPFC(:,50:950)),mean(allSigVarPSTHsCA1(:,50:950))))

% Normalize histogram by mean fr per row
allsignormhist = allsighist./repmat(max(allsighist,[],2),1,size(allsighist,2));
allnosignormhist = allnosighist./repmat(max(allnosighist,[],2),1,size(allnosighist,2));

forppr = 1;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

figdir = '/data25/sjadhav/HPExpt/Figures/31Oct/';
summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',40);
    tfont = 40;
    xfont = 40;
    yfont = 40;
end


if 1
    % 1) Norm histogram of all Sig and Non-Sig
    % ----------------------------------------
    
    figure; hold on; redimscreen_figforppt1;
    set(gcf, 'Position',[205 136 723 446]);
    xaxis = -pret:binsize:postt;
    plot(xaxis,mean(allsignormhist),'r','Linewidth',5);
    plot(xaxis,mean(allnosignormhist),'b','Linewidth',5);
    legend('Ripple Mod Units','Non-Mod Units');
    plot(xaxis,mean(allsignormhist)+sem(allsignormhist),'r--','Linewidth',1);
    plot(xaxis,mean(allsignormhist)-sem(allsignormhist),'r--','Linewidth',1);
    plot(xaxis,mean(allnosignormhist)+sem(allnosignormhist),'b--','Linewidth',1);
    plot(xaxis,mean(allnosignormhist)-sem(allnosignormhist),'b--','Linewidth',1);
    
    set(gca,'XLim',[-pret postt]);
    title(sprintf('%s Run: Norm histogram for ripple aligned response',area),'FontSize',tfont);
    xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
    ylabel('Norm. Fir rate','FontSize',yfont,'Fontweight','normal');
    set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
    ylow = min([mean(allsignormhist), mean(allnosignormhist)]);
    yhigh = max([mean(allsignormhist),mean(allnosignormhist)]);
    set(gca,'YLim',[ylow-0.05 yhigh+0.05]);
    ypts = ylow-0.2:0.1:yhigh+0.2;
    xpts = 0*ones(size(ypts));
    % Plot Line at 0 ms - Onset of stimulation
    plot(xpts , ypts, 'k--','Linewidth',2);
    % Plot lines at rwin and bckwi
    xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
    xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
    xpts = bckwin(1)*ones(size(ypts)); plot(xpts , ypts, 'g--','Linewidth',1);%
    xpts = bckwin(2)*ones(size(ypts)); plot(xpts , ypts, 'g--','Linewidth',1);
    
    Nsig = length(find(allsig_var2==1));
    Nnosig = length(find(allsig_var2==0));
    text(-pret+50, 0.85, sprintf('Mod: %d',Nsig),'FontSize',xfont,'Fontweight','normal');
    text(-pret+50, 0.8, sprintf('UnMod: %d',Nnosig),'FontSize',xfont,'Fontweight','normal');
    
    figfile = [figdir,area,'_Run_RippleAlign_PoplnHist']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
    % plotting the population variance:
    
     figure; hold on; redimscreen_figforppt1;
    set(gcf, 'Position',[205 136 723 446]);
    xaxis = -pret:binsize:postt;
    plot(xaxis,var(allsignormhist),'r','Linewidth',5);
 %   plot(xaxis,var(allnosignormhist),'b','Linewidth',5);
    legend('Ripple Mod Units','Non-Mod Units');
    
    set(gca,'XLim',[-pret postt]);
    title(sprintf('%s Run: Variance of population histogram for ripple aligned response',area),'FontSize',tfont);
    xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
    ylabel('Variance of population Fir rate','FontSize',yfont,'Fontweight','normal');
    set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
    ylow = min([var(allsignormhist), var(allnosignormhist)]);
    yhigh = max([var(allsignormhist),var(allnosignormhist)]);
    set(gca,'YLim',[ylow-0.05 yhigh+0.05]);
    ypts = ylow-0.2:0.1:yhigh+0.2;
    xpts = 0*ones(size(ypts));
    % Plot Line at 0 ms - Onset of stimulation
    plot(xpts , ypts, 'k--','Linewidth',2);
    % Plot lines at rwin and bckwi
    xpts = rwin(1)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
    xpts = rwin(2)*ones(size(ypts)); plot(xpts , ypts, 'k--','Linewidth',1);
    xpts = bckwin(1)*ones(size(ypts)); plot(xpts , ypts, 'g--','Linewidth',1);%
    xpts = bckwin(2)*ones(size(ypts)); plot(xpts , ypts, 'g--','Linewidth',1);
    
 
    text(-pret+50, 0.85, sprintf('Mod: %d',Nsig),'FontSize',xfont,'Fontweight','normal');
    text(-pret+50, 0.8, sprintf('UnMod: %d',Nnosig),'FontSize',xfont,'Fontweight','normal');
    
    
end


if 1
    % 2) No of sig modulated cells over days: %tage and number
    % ------------------------------------
    figure; hold on;
    if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
    
    persig_days = 100*ncells_days_sig./ncells_days;
    plot(persig_days,['ro'],'MarkerSize',30,'LineWidth',2);
    title(sprintf('%s Run: Fraction Ripple mod cells',area),'FontSize',tfont);
    xlabel(['Day'],'FontSize',xfont,'Fontweight','normal');
    ylabel(['Percentage of cells'],'FontSize',yfont,'Fontweight','normal');
    set(gca,'XLim',[0.5 length(persig_days)+0.5])
    set(gca,'YLim',[0 100]);
    
    figfile = [figdir,area,'_Run_RippleAlign_PertageSigDays']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
    
    figure; hold on;
    if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end
    
    Nsig_days = ncells_days_sig;
    plot(Nsig_days,[clr 'o'],'MarkerSize',18,'LineWidth',2);
    title(sprintf('No. of sig modulated units'));
    xlabel(['Day'],'FontSize',xfont,'Fontweight','normal');
    ylabel(['Number of cells'],'FontSize',yfont,'Fontweight','normal');
    set(gca,'YLim',[0 max(Nsig_days)+2]);
    
    if savefig1==1,
        figfile = [figdir,area,'_Ripplemodmod_NSigDays'];
        print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
    end
    
    
end























%
% if 0
%     % plot individual phase histogram of all units
%
%     norm = 1;
%
%     figure
%     titlestring=sprintf('%s %s phase hist of individual units // %s',animals{1},region,referencestring);
%     title(titlestring,'FontSize',14,'FontWeight','bold')
%     counter=1;
%     for k=1:length(caf.celloutput)
%         if counter==81
%             counter=1;
%             figure
%             titlestring=sprintf('%s %s phase hist of individual units // %s',animals{1},region,referencestring);
%             title(titlestring,'FontSize',14,'FontWeight','bold')
%         end
%         subplot(8,10,counter)
%         bins_plot = caf.celloutput(k).bins(1:(end-1));
%         bins_plot = bins_plot + (bins(2)-bins(1))/2;
%         phasehist = caf.celloutput(k).phasehist(1:(end-1));
%         phasehist_norm = phasehist/sum(phasehist);
%         if norm == 1
%             if size(phasehist_norm,1) < size(phasehist_norm,2)
%                 phasehist_norm = phasehist_norm';
%             end
%             %plot
%             h = bar([bins_plot bins_plot+2*pi],[phasehist_norm ; phasehist_norm],'histc');
%             title(num2str(caf.celloutput(k).index))
%             axis tight
%             ylim([0 .2])
%         else
%             if size(phasehist,1) < size(phasehist,2)
%                 phasehist = phasehist';
%             end
%             %plot
%             h = bar([bins_plot bins_plot+2*pi],[phasehist ; phasehist],'histc');
%             title(num2str(caf.celloutput(k).index),'FontSize',12,'FontWeight','bold')
%             axis tight
%             ylim([0 250])
%         end
%
%         set(h(1),'facecolor',clr)
%         set(h(1),'edgecolor',clr)
%
%         % plot guide lines
%         hold on
%         plot([pi,pi],[0 9999],'k--','LineWidth',1.5)
%         plot([-pi,-pi],[0 9999],'k--','LineWidth',1.5)
%         plot([3*pi,3*pi],[0 9999],'k--','LineWidth',1.5)
%         plot([0,0],[0 9999],'k:','LineWidth',1.5)
%         plot([2*pi,2*pi],[0 9999],'k:','LineWidth',1.5)
%
%         counter=counter+1;
%     end
% end
%



% % bar
% count = histc(allspikephases, bins);
% out = bar(bins, count, 'hist');
% set(out,'facecolor','k')
% title('aggregate theta modulation');
%
% % lineplot
% dischargeprob=count./sum(count);
% plot(bins(1:(end-1)),dischargeprob(1:(end-1)),'k','LineWidth',2);
%
% [m ph] = modulation(allspikephases);






