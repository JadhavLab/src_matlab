% works on the output of DFSsj_HPexpt_getripandsoundalignspikingAC3
savedirX = '/opt/data15/gideon/ProcessedData/';
plotIndividualCells=0;
areas={'CA1','AC','PFC'}
%areas={'AC'}

histOns=[];
histOnsW=[];
for areaInd=1:3
    area=areas{areaInd};
switch area
    case 'AC'
% compare sleep and awake ripple-evoked activity
load([savedirX 'NdlBrg_ripplemodsoundmod2_ACawake_gather']);
allripplemodW=allripplemod;
allripplemod_idxW=allripplemod_idx;
load([savedirX 'NdlBrg_ripplemodsoundmod2_AC_gather']);

    case 'CA1'
        % compare sleep and awake ripple-evoked activity
load([savedirX 'NdlBrg_ripplemodsoundmod2_CA1awake_gather']);
allripplemodW=allripplemod;
allripplemod_idxW=allripplemod_idx;
load([savedirX 'NdlBrg_ripplemodsoundmod2_CA1_gather']);

    case 'PFC'
% compare sleep and awake ripple-evoked activity
load([savedirX 'NdlBrg_ripplemodsoundmod2_PFCawake_gather']);
allripplemodW=allripplemod;
allripplemod_idxW=allripplemod_idx;
load([savedirX 'NdlBrg_ripplemodsoundmod2_PFC_gather']);
        
end
allinds=unique([allripplemod_idxW;allripplemod_idx],'rows');

allripmodonset3=[];
for i=1:length(allripplemod)
    if ~isempty(allripplemod(i).ripmodonset3)
        allripmodonset3=[allripmodonset3 allripplemod(i).ripmodonset3]; 
    else
        allripmodonset3=[allripmodonset3 nan];
    end;
end


allripmodonset3W=[];
for i=1:length(allripplemodW)
    if ~isempty(allripplemodW(i).ripmodonset3)
        allripmodonset3W=[allripmodonset3W allripplemodW(i).ripmodonset3]; 
    else
        allripmodonset3W=[allripmodonset3W nan];
    end;
end


% centering around ripple time
allripmodonset3W=allripmodonset3W-500;
allripmodonset3=allripmodonset3-500;

% figure;
% plot(allripmodonset3,'ko');ylim([0 800]);
% hold on;
% plot(allripmodonset3W,'ro');ylim([0 800]);
% legend({'sleep','awake'})
% title(area)


cntcellsRipW=length(allripplemodW);
cntcellsRip=length(allripplemod);

animdayvecRip=[];
for ii=1:cntcellsRip,animdayvecRip=[animdayvecRip; allripplemod(ii).index(1:2)];end
animdayvecRip(animdayvecRip(:,1)==1,2)=animdayvecRip(animdayvecRip(:,1)==1,2)-7;

animdayvecRipW=[];
for ii=1:cntcellsRipW,animdayvecRipW=[animdayvecRipW; allripplemodW(ii).index(1:2)];end
animdayvecRipW(animdayvecRipW(:,1)==1,2)=animdayvecRipW(animdayvecRipW(:,1)==1,2)-7;


keep1=find(~isnan(allripmodonset3));
keep1W=find(~isnan(allripmodonset3W));
allripmodonset3=allripmodonset3(keep1);
allripmodonset3W=allripmodonset3W(keep1W);
animdayvecRip=animdayvecRip(keep1,:);
animdayvecRipW=animdayvecRipW(keep1W,:);

% didn't have CA1 cells for Borg
if strcmp(area,'CA1')
keep2=find(animdayvecRip(:,1)==1);
keep2W=find(animdayvecRipW(:,1)==1);
allripmodonset3=allripmodonset3(keep2);
allripmodonset3W=allripmodonset3W(keep2W);
animdayvecRip=animdayvecRip(keep2,:);
animdayvecRipW=animdayvecRipW(keep2W,:);
end

histOns=[ histOns;hist(allripmodonset3,-500:50:500)/sum(hist(allripmodonset3,-500:50:500))];
histOnsW= [histOnsW;hist(allripmodonset3W,-500:50:500)/sum(hist(allripmodonset3W,-500:50:500))];


scrsz = get(0,'ScreenSize');
figure('Position',[300 scrsz(4)/2 scrsz(3)/4 scrsz(4)/1.5])
subplot(2,1,1)
hist(allripmodonset3,-500:50:500)
title([area ' sleep'])
subplot(2,1,2)
hist(allripmodonset3W,-500:50:500)
title([area ' awake'])


figure;
plot(animdayvecRip(animdayvecRip(:,1)==1,2),allripmodonset3(animdayvecRip(:,1)==1),'ko')
hold on;plot(animdayvecRip(animdayvecRip(:,1)==2,2),allripmodonset3(animdayvecRip(:,1)==2),'ro')
legend({'anim1','anim2'})
ylim([-500 500]);
xlabel('days')
ylabel('onset')
title([area ' sleep'])

figure;
plot(animdayvecRipW(animdayvecRipW(:,1)==1,2),allripmodonset3W(animdayvecRipW(:,1)==1),'ko')
hold on;plot(animdayvecRipW(animdayvecRipW(:,1)==2,2),allripmodonset3W(animdayvecRipW(:,1)==2),'ro')
legend({'anim1','anim2'})
ylim([-500 500]);
xlabel('days')
ylabel('onset')
title([area ' awake'])
cm=colormap('Jet')
figure;
c1=1;
threshs=[-90:-10:-140]
for thresh=threshs
fracPre=[];
    
for k=1:11, fracPre=[fracPre nanmean(allripmodonset3(animdayvecRip(:,2)==k)<thresh)];end
plot(fracPre,'color',cm(c1,:));
c1=c1+10;
hold on;
end
ylim([0 0.7])
xlabel('days')
ylabel('Fraction of pre-ripple onset')
legend({'-90','-100','-110','-120','-130','-140'},'location','northwest')
title([area ', fraction of pre-ripple onset, different pre-ripple thresholds'])



%% compare wake vs sleep swr responsiveness
allhistswake=[];
allhistssleep=[];
allindsNadal=allinds(allinds(:,1)==1,:);
fillind=1;
for v=1:size(allinds,1)
    curidx=allinds(v,:);%animal day
    ripidxW=find(ismember(allripplemod_idxW,curidx,'rows'));
    ripidx=find(ismember(allripplemod_idx,curidx,'rows'));
    if ~isempty(ripidxW) & ~isempty(ripidx)
        
       if allripplemodW(1,ripidxW).rasterShufP2<.05
        allhistswake=[allhistswake;zscore(mean(allripplemodW(1,ripidxW).hist))];
       else
        allhistswake=[allhistswake;NaN(1,101)];
           
       end
     
       if allripplemod(1,ripidx).rasterShufP2<.05
      
           allhistssleep=[allhistssleep;zscore(mean(allripplemod(1,ripidx).hist))];
        else
        allhistssleep=[allhistssleep;NaN(1,101)];
      
       end
       fillind=fillind+1;
    end
end
figure('Position',[500 scrsz(4)/4 scrsz(3)/3 scrsz(4)/2])
subplot(1,2,1);imagesc(allhistswake);title('Awake');subplot(1,2,2);imagesc(allhistssleep);title('Sleep')

%%
keyboard
if plotIndividualCells
scrsz = get(0,'ScreenSize');
for v=1:size(allinds,1)
    curidx=allinds(v,:);%animal day
    ripidxW=find(ismember(allripplemod_idxW,curidx,'rows'));
    ripidx=find(ismember(allripplemod_idx,curidx,'rows'));
figure('Position',[300 scrsz(4)/2 scrsz(3)/1.3 scrsz(4)/2])   
subplot(2,2,1)
try    
if ~isempty(ripidxW)
        
       imagesc(allripplemodW(1,ripidxW).hist)
title('awake ripples')
       %        if allsoundmod(1,sndidx).rasterShufP2<0.05
%        title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index) ' score=' num2str(allsoundmod(1,sndidx).varRespAmp2) '*']) 
%        else
%       title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index) ' score=' num2str(allsoundmod(1,sndidx).varRespAmp2)]) 
%            
%        end
    end
    if ~isempty(ripidx)
subplot(2,2,2)
imagesc(allripplemod(1,ripidx).hist)
       if allripplemod(1,ripidx).rasterShufP2<0.05
       title(['sleep ripples, ind= ' num2str(allripplemod(1,ripidx).index) ' score=' num2str(allripplemod(1,ripidx).varRespAmp2) '*']) 
       else
      title(['sleep ripples, ind= ' num2str(allripplemod(1,ripidx).index) ' score=' num2str(allripplemod(1,ripidx).varRespAmp2)]) 
           
       end
    end
    if ~isempty(ripidx)
       subplot(2,2,4)
 meanriptrace=mean(allripplemod(1,ripidx).hist);
 meanriptraceZ=(meanriptrace-mean(meanriptrace))/std(meanriptrace);
 plot(meanriptraceZ);
 hold on;
 try
 plot([allripplemod(ripidx).ripmodonset1/10 allripplemod(ripidx).ripmodonset1/10],[0 max(meanriptraceZ)],'b');
 plot([allripplemod(ripidx).ripmodonset2/10 allripplemod(ripidx).ripmodonset2/10],[0 max(meanriptraceZ)],'r');
 plot([allripplemod(ripidx).ripmodonset3/10 allripplemod(ripidx).ripmodonset3/10],[0 max(meanriptraceZ)],'k');
 plot([allripplemod(ripidx).ripmodonset4/10 allripplemod(ripidx).ripmodonset4/10],[0 max(meanriptraceZ)],'g');
 plot([allripplemod(ripidx).ripmodonset5/10 allripplemod(ripidx).ripmodonset5/10],[0 max(meanriptraceZ)],'m');
 catch
 end
 hold on;
    end
 if ~isempty(ripidxW)
            subplot(2,2,3)

 meansndtrace=mean(allripplemodW(1,ripidxW).hist);
 meansndtraceZ=(meansndtrace-mean(meansndtrace))/std(meansndtrace);

     plot(meansndtraceZ,'r');
     hold on;
     try
     plot([allripplemodW(ripidxW).ripmodonset1/10 allripplemodW(ripidxW).ripmodonset1/10],[0 max(meansndtraceZ)],'b');
     plot([allripplemodW(ripidxW).ripmodonset2/10 allripplemodW(ripidxW).ripmodonset2/10],[0 max(meansndtraceZ)],'r');
     plot([allripplemodW(ripidxW).ripmodonset3/10 allripplemodW(ripidxW).ripmodonset3/10],[0 max(meansndtraceZ)],'k');
     plot([allripplemodW(ripidxW).ripmodonset4/10 allripplemodW(ripidxW).ripmodonset4/10],[0 max(meansndtraceZ)],'g');
     plot([allripplemodW(ripidxW).ripmodonset5/10 allripplemodW(ripidxW).ripmodonset5/10],[0 max(meansndtraceZ)],'m');
     catch
     end
 %legend('sleep','sleep','awake','awake')
        
%  cursoundind=allsoundmod(1,sndidx).soundind;
% [sortedSounds sortedSoundind]=sort(cursoundind);
% figure('Position',[900 scrsz(4)/2 scrsz(3)/3 scrsz(4)/1.2])
% subplot(2,1,1);imagesc(1:100,sortedSounds,allsoundmod(1,sndidx).hist(sortedSoundind,:));title('sorted by sound ind')
% [sortedSpeeds sortedSpeedsInd]=sort(allsoundmod(sndidx).speeds);
% subplot(2,1,2);imagesc(1:100,sortedSpeeds,allsoundmod(1,sndidx).hist(sortedSpeedsInd,:));title('sorted by speed')

 end
    try
    saveas(gcf,['/data15/gideon/ripmodsleepwake2/ripmodsleepwake' num2str(allripplemod(1,ripidx).index) '.jpg'])
    close all
    catch
    end
catch
end
   % keyboard
end
end
end

