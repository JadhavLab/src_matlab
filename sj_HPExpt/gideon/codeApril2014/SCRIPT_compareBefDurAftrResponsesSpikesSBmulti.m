%% THIS SCRIPT EXTRACTS AUDITORY MULTIUNIT RESPONSES BEFORE, DURING AND AFTER 
% TRAINING FROM ANIMAL ROSENTHAL

% can extract for all tets, but now only for AC
ACtets=15:21;

for dayx=6:12
        
if dayx<10
    daystr=['0' num2str(dayx)];
else
    daystr=num2str(dayx);
end

[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Rtl/EEG/Rtleeg', daystr,'1','22');
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Rtl/EEG/Rtleeg', daystr,'10','22');
if strcmp(daystr,'06'), stimIndsAft=stimIndsAft(2:end);end

stimEEGtimesSB{1}=stimEEGtimesBef;
stimEEGtimesSB{2}=stimEEGtimesAft;
stimIndsSBMat=[stimIndsBef stimIndsAft];
whichStimSBMat=[whichStimBef;whichStimAft];
[stimEEGtimesTrack1 stimIndsTrack1]=findStimTrackFirstInPair('/data15/gideon/Rtl/EEG/Rtleeg', daystr,'3','22');
[stimEEGtimesTrack2 stimIndsTrack2]=findStimTrackFirstInPair('/data15/gideon/Rtl/EEG/Rtleeg', daystr,'5','22');
[stimEEGtimesTrack3 stimIndsTrack3]=findStimTrackFirstInPair('/data15/gideon/Rtl/EEG/Rtleeg', daystr,'7','22');

if dayx~=5
[stimEEGtimesTrack4 stimIndsTrack4]=findStimTrackFirstInPair('/data15/gideon/Rtl/EEG/Rtleeg', daystr,'9','22');
end

stimEEGTimesTrack{1}=stimEEGtimesTrack1;
stimEEGTimesTrack{2}=stimEEGtimesTrack2;
stimEEGTimesTrack{3}=stimEEGtimesTrack3;
if dayx~=5

stimEEGTimesTrack{4}=stimEEGtimesTrack4;
end

stimIndsTrack{1}=stimIndsTrack1;
stimIndsTrack{2}=stimIndsTrack2;
stimIndsTrack{3}=stimIndsTrack3;
if dayx~=5
stimIndsTrack{4}=stimIndsTrack4;
end
%compareBefAftrResponsesSpikesSBmulti('/data15/gideon/Rtl/EEG/Rtleeg',daystr,'1','10',1:21, '/data15/gideon/Rtl/Rtlmulti',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft)
if dayx~=5
compareBefDurAftrResponsesSpikesSBmulti(daystr,[1 10],[3 5 7 9],ACtets, '/data15/gideon/Rtl/Rtlmulti',stimEEGtimesSB,stimIndsSBMat,whichStimSBMat,stimEEGTimesTrack,stimIndsTrack);
else
compareBefDurAftrResponsesSpikesSBmulti(daystr,[1 10],[3 5 7],ACtets, '/data15/gideon/Rtl/Rtlmulti',stimEEGtimesSB,stimIndsSBMat,whichStimSBMat,stimEEGTimesTrack,stimIndsTrack);
    
end
close all
end