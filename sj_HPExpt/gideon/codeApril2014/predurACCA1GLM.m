%% --------------- START: Predicting CA1 SWR activity from preceding AC
savedirX = '/opt/data15/gideon/ProcessedData/';

load([savedirX 'Ndl_ripples_AC'])
allripplemodAC=allripplemod;
allripplemod_idxAC=allripplemod_idx;

load([savedirX 'Ndl_ripples_CA1'])
allripplemodCA1=allripplemod;
allripplemod_idxCA1=allripplemod_idx;
combined_idx=unique([allripplemod_idxAC(:,1:2)],'rows');
converged1=[];
n = [];
nsig = [];
fracsig=[];
allErrReal=[];
allErrShuf=[];
allPs=[];
allPsK=[];
allinds=[];
allbetas={};
allsigs={};
allsigbetasvec=[];
for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    ACidx=find(ismember(allripplemod_idxAC(:,1:2),curidx,'rows'));
    CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
    % creating a matrix of AC ensemble firing before ripples
    ACmat=[];
    for j=1:size(ACidx,1)
        ACind1=ACidx(j);
        AChist=allripplemodAC(1,ACind1).hist;
        % can change this range to 51:70 to compare
        ACampPre=mean(AChist(:,31:50),2);
        % add this AC cell is it is the first or equal in length to prev
        if size(ACmat,2)==0 | (size(ACmat,2)>0&size(ACmat,1)==size(ACampPre,1))
            ACmat=[ACmat ACampPre];
            % if the first cell had fewer datapoints, switch it out with the
            % current
        elseif size(ACmat,1)==1&(size(ACmat,2)<size(ACampPre,2))
            ACmat=ACampPre;
        end
    end
    allErrReal1=[];
    allErrShuf1=[];
    
    for k=1:size(CA1idx,1)
        
        CA1ind1=CA1idx(k);
        CA1hist=allripplemodCA1(1,CA1ind1).hist;
        % can change this range to 31:50 to compare (prediction  lower)
        CA1amp=mean(CA1hist(:,51:70),2);   
        lastwarn('');
        if size(ACmat,1)==size(CA1amp,1)
            [btrall, ~, statsall] = glmfit(ACmat,CA1amp,'poisson');
            allbetas{end+1}=btrall(2:end);
            allsigs{end+1}=statsall.p(2:end);
            btmp=btrall(2:end);stmp=statsall.p(2:end);
            allsigbetasvec=[allsigbetasvec; btmp(stmp<0.05)];
            %--- BEGIN cross-validation
            
            if isempty(lastwarn)
                converged1=[converged1 1];
            else
                converged1=[converged1 0];      
            end
            % continue only if converged
            if isempty(lastwarn)
                currsig = find(statsall.p(2:end) < 0.05);
                nsig = [nsig length(currsig)];
                n = [n size(ACmat,2)];

                fracsig = [fracsig length(currsig)/size(ACmat,2)];
                numRips=size(ACmat,1);
                allErrReal1=[];
                allErrShuf1=[];
                numTrain=1000;
                
                for ii=1:numTrain
                    ripidxs=randperm(numRips);
                    dataPercentForTrain=0.5;
                    Ntrain=ripidxs(1:round(numRips*dataPercentForTrain));
                    Ntest=ripidxs(round(numRips*dataPercentForTrain)+1:numRips);
                    lastwarn('');
                    [btr, ~, statstr] = glmfit(ACmat(Ntrain,:),CA1amp(Ntrain),'poisson');
                    % continue only if converged
                    if isempty(lastwarn)
                        yfit = glmval(btr, ACmat(Ntest,:),'log',statstr,0.95);
                        errReal=nanmean(abs(yfit-CA1amp(Ntest)));
                        Ntestshufd=Ntest(randperm(length(Ntest)));
                        errShuf=nanmean(abs(yfit-CA1amp(Ntestshufd)));
                        allErrReal1=[allErrReal1 errReal];
                        allErrShuf1=[allErrShuf1 errShuf];
                    end
                end
                [r1,p1]=ttest2(allErrReal1,allErrShuf1,0.05,'left');
                [kp1]=kruskalwallis([allErrReal1' allErrShuf1'],[],'off');
                allErrReal=[allErrReal nanmean(allErrReal1)];
                allErrShuf=[allErrShuf nanmean(allErrShuf1)];
                allPs=[allPs p1];
                allPsK=[allPsK kp1];
                allinds=[allinds; allripplemod_idxCA1(CA1idx(k),:)];

            end
        end
    end
end

% plotting results
figure;plot(allPsK)
xlabel('#AC ensemble+CA1 cell')
ylabel('Cross-validation p-value')
title(['Rate of significantly predicted CA1 cells from preceding AC activity: ' num2str( mean(allPsK<0.05))])

figure;plot(n,allPsK,'ko')
xlabel('number of cells in ensemble')
ylabel('p-val')
[r p]=corrcoef(n,allPsK)

%--------------- END: Predicting CA1 SWR activity from preceding AC
%%
% --------------- START: Predicting preceding AC activity from CA1 SWR activity
savedirX = '/opt/data15/gideon/ProcessedData/';

load([savedirX 'Ndl_ripples_AC'])
allripplemodAC=allripplemod;
allripplemod_idxAC=allripplemod_idx;

load([savedirX 'Ndl_ripples_CA1'])
allripplemodCA1=allripplemod;
allripplemod_idxCA1=allripplemod_idx;
combined_idx=unique([allripplemod_idxAC(:,1:2)],'rows');
converged2=[];
n2 = [];
nsig2 = [];
fracsig2=[];
allErrReal2=[];
allErrShuf2=[];
allPs2=[];
allPsK2=[];
allinds2=[];
allbetas2={};
allsigs2={};
for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    ACidx=find(ismember(allripplemod_idxAC(:,1:2),curidx,'rows'));
    CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
    % creating a matrix of AC ensemble firing before ripples
    CA1mat=[];
    for j=1:size(CA1idx,1)
        CA1ind1=CA1idx(j);
        CA1hist=allripplemodCA1(1,CA1ind1).hist;
            % NOTE!!! CHANGE BACK!!
%        CA1amp1=mean(CA1hist(:,51:70),2); 
        CA1amp1=mean(CA1hist(:,31:50),2);

        % add this cell is it is the first or equal in length to prev
        if size(CA1mat,2)==0 | (size(CA1mat,2)>0&size(CA1mat,1)==size(CA1amp1,1))
            CA1mat=[CA1mat CA1amp1];
            % if the first cell had fewer datapoints, switch it out with the
            % current
        elseif size(CA1mat,1)==1&(size(CA1mat,2)<size(CA1amp1,2))
            CA1mat=CA1amp1;
        end
    end

    
    for k=1:size(ACidx,1)
        
        ACind1=ACidx(k);
        AChist=allripplemodAC(1,ACind1).hist;
        % NOTE!! CHANGE BACK!!
        %ACamp=mean(AChist(:,31:50),2);   
        ACamp=mean(AChist(:,51:70),2);   

        lastwarn('');
        if size(CA1mat,1)==size(ACamp,1)
            [btrall, ~, statsall] = glmfit(CA1mat,ACamp,'poisson');
            allbetas2{end+1}=btrall(2:end);
            allsigs2{end+1}=statsall.p(2:end);
            
            %--- BEGIN cross-validation
            
            if isempty(lastwarn)
                converged2=[converged2 1];
            else
                converged2=[converged2 0];      
            end
            % continue only if converged
            if isempty(lastwarn)
                currsig2 = find(statsall.p(2:end) < 0.05);
                nsig2 = [nsig2 length(currsig2)];
                n2 = [n2 size(CA1mat,2)];
                fracsig2 = [fracsig2 length(currsig2)/size(CA1mat,2)];
                numRips=size(CA1mat,1);
                allErrReal=[];
                allErrShuf=[];
                numTrain=1000;
                
                for ii=1:numTrain
                    ripidxs=randperm(numRips);
                    dataPercentForTrain=0.5;
                    Ntrain=ripidxs(1:round(numRips*dataPercentForTrain));
                    Ntest=ripidxs(round(numRips*dataPercentForTrain)+1:numRips);
                    lastwarn('');
                    [btr, ~, statstr] = glmfit(CA1mat(Ntrain,:),ACamp(Ntrain),'poisson');
                    % continue only if converged
                    if isempty(lastwarn)
                        yfit = glmval(btr, CA1mat(Ntest,:),'log',statstr,0.95);
                        errReal=nanmean(abs(yfit-ACamp(Ntest)));
                        Ntestshufd=Ntest(randperm(length(Ntest)));
                        errShuf=nanmean(abs(yfit-ACamp(Ntestshufd)));
                        allErrReal=[allErrReal2 errReal];
                        allErrShuf=[allErrShuf2 errShuf];
                    end
                end
                [r1,p1]=ttest2(allErrReal2,allErrShuf2,0.05,'left');
                [kp1]=kruskalwallis([allErrReal2' allErrShuf2'],[],'off');
                allErrReal2=[allErrReal2 nanmean(allErrReal)];
                allErrShuf2=[allErrShuf2 nanmean(allErrShuf)];
                allPs2=[allPs2 p1];
                allPsK2=[allPsK2 kp1];
                allinds2=[allinds2; allripplemod_idxAC(ACidx(k),:)];

            end
        end
    end
end

% plotting results
figure;plot(allPsK2)
xlabel('#AC ensemble+CA1 cell')
ylabel('Cross-validation p-value')
title(['Rate of significantly predictedpre-SWR AC cells from SWR ensemble activity: ' num2str( mean(allPsK<0.05))])
 

figure;plot(n2,allPsK2,'ko')
xlabel('number of cells in ensemble')
ylabel('p-val')
[r p]=corrcoef(n2,allPsK2)
%--------------- END: Predicting CA1 SWR activity from preceding AC