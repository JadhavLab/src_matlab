
% to show data around single rips and the correponding spectrogram:
%   for x=1:51,figure;subplot(2,1,1);plot(data(:,x));subplot(2,1,2);[S,F,T,P] =spectrogram(data(:,x)',228,220,[0:300],1500);surf(T,F,10*log10(P),'edgecolor','none'); axis tight;view(0,90);end


day=4;
epoch=4;
tet=9;
riptets=[9 12];
animal='Brg';

[params] = sj_HPexpt_baselinespecgram(animal, day, epoch, tet, 0,'movingwin',[100 10]/1000,'fpass',[0 400])
[params] = sj_HPexpt_baselinespecgram_forgnd(animal, day, epoch, tet,'movingwin',[100 10]/1000,'fpass',[0 400])
[params] = sj_HPexpt_eventtrigspecgrams_getrip(animal, day, epoch, tet, 'rip',riptets,1,0,1,'movingwin',[100 10]/1000,'fpass',[0 400])
