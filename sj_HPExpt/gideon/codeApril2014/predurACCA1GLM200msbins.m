%% --------------- START: Predicting CA1 SWR activity from preceding AC
% savedirX = '/opt/data15/gideon/ProcessedData/';
% 
% load([savedirX 'Ndl_ripples_AC'])
% allripplemodAC=allripplemod;
% allripplemod_idxAC=allripplemod_idx;
% 
% load([savedirX 'Ndl_ripples_CA1'])
% allripplemodCA1=allripplemod;
% allripplemod_idxCA1=allripplemod_idx;
% 
% % load([savedirX 'Ndl_ripples_PFC'])
% % allripplemodAC=allripplemod;
% % allripplemod_idxAC=allripplemod_idx;
% 
% 
% 
% 
% combined_idx=unique([allripplemod_idxAC(:,1:2)],'rows');
% timewindows=[1:200;101:300;201:400;301:500;401:600;501:700;601:800;701:900;801:1000];
% 
% %timewindows=[1:100;101:200;201:300;301:400;401:500;501:600;601:700;701:800;801:900;901:1000];
% numtimewindows=size(timewindows,1);
% ALLSIGRATIOS=NaN(numtimewindows,numtimewindows);
% ALLERRORRATIOS=NaN(numtimewindows,numtimewindows);
% 
%                         numTrain=1000;
% 
% for ACtimewindow=1:numtimewindows
%     for CA1timewindow=1:numtimewindows
%         
%         converged1=[];
%         n = [];
%         nsig = [];
%         fracsig=[];
%         allErrReal=[];
%         allErrShuf=[];
%         allPs=[];
%         allPsK=[];
%         allinds=[];
%         allbetas={};
%         allsigs={};
%         allsigbetasvec=[];
%         for v=1:size(combined_idx,1)
%             curidx=combined_idx(v,[1:2]);%animal day
%             ACidx=find(ismember(allripplemod_idxAC(:,1:2),curidx,'rows'));
%             CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
%             % creating a matrix of AC ensemble firing before ripples
%             ACmat=[];
%             for j=1:size(ACidx,1)
%                 ACind1=ACidx(j);
%              %   AChist=allripplemodAC(1,ACind1).hist;
%                 AChist=rast2mat(allripplemodAC(1,ACind1).raster);
%                 ACampPre=mean(AChist(:,timewindows(ACtimewindow,:)),2);
%                 % add this AC cell is it is the first or equal in length to prev
%                 if size(ACmat,2)==0 | (size(ACmat,2)>0&size(ACmat,1)==size(ACampPre,1))
%                     ACmat=[ACmat ACampPre];
%                     % if the first cell had fewer datapoints, switch it out with the
%                     % current
%                 elseif size(ACmat,1)==1&(size(ACmat,2)<size(ACampPre,2))
%                     ACmat=ACampPre;
%                 end
%             end
%             allErrReal1=[];
%             allErrShuf1=[];
%             
%             for k=1:size(CA1idx,1)
%                 
%                 CA1ind1=CA1idx(k);
%              %   CA1hist=allripplemodCA1(1,CA1ind1).hist;
%                 CA1hist=rast2mat(allripplemodCA1(1,CA1ind1).raster);
% 
%                 % can change this range to 31:50 to compare (prediction  lower)
%                 CA1amp=mean(CA1hist(:,timewindows(CA1timewindow,:)),2);
%                 lastwarn('');
%                 if size(ACmat,1)==size(CA1amp,1)
%                     [btrall, ~, statsall] = glmfit(ACmat,CA1amp,'poisson');
%                     allbetas{end+1}=btrall(2:end);
%                     allsigs{end+1}=statsall.p(2:end);
%                     btmp=btrall(2:end);stmp=statsall.p(2:end);
%                     allsigbetasvec=[allsigbetasvec; btmp(stmp<0.05)];
%                     %--- BEGIN cross-validation
%                     
%                     if isempty(lastwarn)
%                         converged1=[converged1 1];
%                     else
%                         converged1=[converged1 0];
%                     end
%                     % continue only if converged
%                     if isempty(lastwarn)
%                         currsig = find(statsall.p(2:end) < 0.05);
%                         nsig = [nsig length(currsig)];
%                         n = [n size(ACmat,2)];
%                         
%                         fracsig = [fracsig length(currsig)/size(ACmat,2)];
%                         numRips=size(ACmat,1);
%                         allErrReal1=[];
%                         allErrShuf1=[];
%                         
%                         for ii=1:numTrain
%                             ripidxs=randperm(numRips);
%                             dataPercentForTrain=0.8;
%                             Ntrain=ripidxs(1:round(numRips*dataPercentForTrain));
%                             Ntest=ripidxs(round(numRips*dataPercentForTrain)+1:numRips);
%                             lastwarn('');
%                             [btr, ~, statstr] = glmfit(ACmat(Ntrain,:),CA1amp(Ntrain),'poisson');
%                             % continue only if converged
%                             if isempty(lastwarn)
%                                 yfit = glmval(btr, ACmat(Ntest,:),'log',statstr,0.95);
%                                 errReal=nanmean(abs(yfit-CA1amp(Ntest)));
%                                 Ntestshufd=Ntest(randperm(length(Ntest)));
%                                 errShuf=nanmean(abs(yfit-CA1amp(Ntestshufd)));
%                                 allErrReal1=[allErrReal1 errReal];
%                                 allErrShuf1=[allErrShuf1 errShuf];
%                             end
%                         end
%                         [r1,p1]=ttest2(allErrReal1,allErrShuf1,0.05,'left');
%                         [kp1]=kruskalwallis([allErrReal1' allErrShuf1'],[],'off');
%                         
%                         allErrReal=[allErrReal nanmean(allErrReal1)];
%                         allErrShuf=[allErrShuf nanmean(allErrShuf1)];
%                         allPs=[allPs p1];
%                         allPsK=[allPsK kp1];
%                         allinds=[allinds; allripplemod_idxCA1(CA1idx(k),:)];
%                         
%                     end
%                 end
%             end
%         end
%         
%         % plotting results
% %         figure;plot(allPsK)
% %         xlabel('#AC ensemble+CA1 cell')
% %         ylabel('Cross-validation p-value')
% %         title(['Rate of significantly predicted CA1 cells from preceding AC activity: ' num2str( mean(allPsK<0.05))])
% %         
% %         figure;plot(n,allPsK,'ko')
% %         xlabel('number of cells in ensemble')
% %         ylabel('p-val')
% %         [r p]=corrcoef(n,allPsK);
%         
%         ALLSIGRATIOS(ACtimewindow,CA1timewindow)= mean(allPsK<0.01 & allErrReal<allErrShuf);
%         ALLERRORRATIOS(ACtimewindow,CA1timewindow)= mean(allErrReal./allErrShuf);
%     figure(1);
%     subplot(2,1,1);
%     drawnow;
%     imagesc(ALLSIGRATIOS);
%     subplot(2,1,2);
%         imagesc(ALLERRORRATIOS);
% drawnow;
%     end
% end
% 
% % subplot(2,1,1)
% % set(gca,'XTickLabel',{'-500:-300','-400:-200','-300:-100','-200:0','-100:100','0:200','100:300','200:400','300:500'})
% % set(gca,'YTickLabel',{'-500:-300','-400:-200','-300:-100','-200:0','-100:100','0:200','100:300','200:400','300:500'})
% % title('Rate of sig predicted CA1 cell firing from AC ensembles using GLM')
% % xlabel('CA1 cell time window')
% % ylabel('AC ensemble time window')
% % 
% % subplot(2,1,2)
% % set(gca,'XTickLabel',{'-500:-300','-400:-200','-300:-100','-200:0','-100:100','0:200','100:300','200:400','300:500'})
% % set(gca,'YTickLabel',{'-500:-300','-400:-200','-300:-100','-200:0','-100:100','0:200','100:300','200:400','300:500'})
% % 
% % title('Error rate of predicted CA1 cell firing from AC ensembles using GLM')
% % ylabel('AC ensemble time window')
% % xlabel('CA1 cell time window')
% 
% 
% save('/opt/data15/gideon/ProcessedData/GLMresACCA1200msbins1.mat')
% 

%--------------- END: Predicting CA1 SWR activity from preceding AC
%%
%--------------- START: Predicting preceding AC activity from CA1 SWR activity
savedirX = '/opt/data15/gideon/ProcessedData/';

load([savedirX 'Ndl_ripples_AC'])
allripplemodAC=allripplemod;
allripplemod_idxAC=allripplemod_idx;

load([savedirX 'Ndl_ripples_CA1'])
allripplemodCA1=allripplemod;
allripplemod_idxCA1=allripplemod_idx;

% load([savedirX 'Ndl_ripples_PFC'])
% allripplemodAC=allripplemod;
% allripplemod_idxAC=allripplemod_idx;

combined_idx=unique([allripplemod_idxAC(:,1:2)],'rows');
timewindows=[1:200;101:300;201:400;301:500;401:600;501:700;601:800;701:900;801:1000];

%timewindows=[1:100;101:200;201:300;301:400;401:500;501:600;601:700;701:800;801:900;901:1000];
numtimewindows=size(timewindows,1);
ALLSIGRATIOS2=NaN(numtimewindows,numtimewindows);
ALLERRORRATIOS2=NaN(numtimewindows,numtimewindows);

                        numTrain=1000;

for ACtimewindow=1:numtimewindows
    for CA1timewindow=1:numtimewindows

converged2=[];
n2 = [];
nsig2 = [];
fracsig2=[];
allErrReal2=[];
allErrShuf2=[];
allPs2=[];
allPsK2=[];
allinds2=[];
allbetas2={};
allsigs2={};
for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    ACidx=find(ismember(allripplemod_idxAC(:,1:2),curidx,'rows'));
    CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
    % creating a matrix of AC ensemble firing before ripples
    CA1mat=[];
    for j=1:size(CA1idx,1)
        CA1ind1=CA1idx(j);
%        CA1hist=allripplemodCA1(1,CA1ind1).hist;
        CA1hist=rast2mat(allripplemodCA1(1,CA1ind1).raster);
 
        CA1amp1=mean(CA1hist(:,timewindows(CA1timewindow,:)),2);

        % add this cell is it is the first or equal in length to prev
        if size(CA1mat,2)==0 | (size(CA1mat,2)>0&size(CA1mat,1)==size(CA1amp1,1))
            CA1mat=[CA1mat CA1amp1];
            % if the first cell had fewer datapoints, switch it out with the
            % current
        elseif size(CA1mat,1)==1&(size(CA1mat,2)<size(CA1amp1,2))
            CA1mat=CA1amp1;
        end
    end

    
    for k=1:size(ACidx,1)
        
        ACind1=ACidx(k);
      %  AChist=allripplemodAC(1,ACind1).hist;
        AChist=rast2mat(allripplemodAC(1,ACind1).raster);

        ACamp=mean(AChist(:,timewindows(ACtimewindow,:)),2);   

        lastwarn('');
        if size(CA1mat,1)==size(ACamp,1)
            [btrall, ~, statsall] = glmfit(CA1mat,ACamp,'poisson');
            allbetas2{end+1}=btrall(2:end);
            allsigs2{end+1}=statsall.p(2:end);
            
            %--- BEGIN cross-validation
            
            if isempty(lastwarn)
                converged2=[converged2 1];
            else
                converged2=[converged2 0];      
            end
            % continue only if converged
            if isempty(lastwarn)
                currsig2 = find(statsall.p(2:end) < 0.05);
                nsig2 = [nsig2 length(currsig2)];
                n2 = [n2 size(CA1mat,2)];
                fracsig2 = [fracsig2 length(currsig2)/size(CA1mat,2)];
                numRips=size(CA1mat,1);
                allErrReal=[];
                allErrShuf=[];
                
                for ii=1:numTrain
                    ripidxs=randperm(numRips);
                    dataPercentForTrain=0.8;
                    Ntrain=ripidxs(1:round(numRips*dataPercentForTrain));
                    Ntest=ripidxs(round(numRips*dataPercentForTrain)+1:numRips);
                    lastwarn('');
                    [btr, ~, statstr] = glmfit(CA1mat(Ntrain,:),ACamp(Ntrain),'poisson');
                    % continue only if converged
                    if isempty(lastwarn)
                        yfit = glmval(btr, CA1mat(Ntest,:),'log',statstr,0.95);
                        errReal=nanmean(abs(yfit-ACamp(Ntest)));
                        Ntestshufd=Ntest(randperm(length(Ntest)));
                        errShuf=nanmean(abs(yfit-ACamp(Ntestshufd)));
                        allErrReal=[allErrReal errReal];
                        allErrShuf=[allErrShuf errShuf];
                    end
                end
                [r1,p1]=ttest2(allErrReal,allErrShuf,0.05,'left');
                [kp1]=kruskalwallis([allErrReal' allErrShuf'],[],'off');
                allErrReal2=[allErrReal2 nanmean(allErrReal)];
                allErrShuf2=[allErrShuf2 nanmean(allErrShuf)];
                allPs2=[allPs2 p1];
                allPsK2=[allPsK2 kp1];
                allinds2=[allinds2; allripplemod_idxAC(ACidx(k),:)];

            end
        end
    end
end

   ALLSIGRATIOS2(ACtimewindow,CA1timewindow)=mean(allPsK2<0.01 & allErrReal2<allErrShuf2);
        ALLERRORRATIOS2(ACtimewindow,CA1timewindow)= mean(allErrReal2./allErrShuf2);
        figure(11);
    subplot(2,1,1);
    drawnow;
    imagesc(ALLSIGRATIOS2);
    subplot(2,1,2);
        imagesc(ALLERRORRATIOS2);
drawnow;
    end
end

% subplot(2,1,1)
% set(gca,'XTickLabel',{'-500:-300','-400:-200','-300:-100','-200:0','-100:100','0:200','100:300','200:400','300:500'})
% set(gca,'YTickLabel',{'-500:-300','-400:-200','-300:-100','-200:0','-100:100','0:200','100:300','200:400','300:500'})
% title('Rate of sig predicted AC cell firing from CA1 ensembles using GLM')
% xlabel('AC cell time window')
% ylabel('CA1 ensemble time window')
% 
% subplot(2,1,2)
% set(gca,'XTickLabel',{'-500:-300','-400:-200','-300:-100','-200:0','-100:100','0:200','100:300','200:400','300:500'})
% set(gca,'YTickLabel',{'-500:-300','-400:-200','-300:-100','-200:0','-100:100','0:200','100:300','200:400','300:500'})
% 
% title('Error rate of predicted AC cell firing from CA1 ensembles using GLM')
% ylabel('CA1 ensemble time window')
% xlabel('AC cell time window')
% 
%  figure;imagesc(ALLERRORRATIOS-ALLERRORRATIOS2);colorbar
%  title('AC->CA1 minus CA1->AC (blue=former better)')
%  set(gca,'XTickLabel',{'-500:-300','-400:-200','-300:-100','-200:0','-100:100','0:200','100:300','200:400','300:500'})
% set(gca,'YTickLabel',{'-500:-300','-400:-200','-300:-100','-200:0','-100:100','0:200','100:300','200:400','300:500'})
% caxis([-0.004 0.004])
%--------------- END: Predicting CA1 SWR activity from preceding AC
save('/opt/data15/gideon/ProcessedData/GLMresACCA1200msbins2.mat')

