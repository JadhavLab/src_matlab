
% THE BASIS OF THIS SCRIPT IS TAKEN FROM  DFSsj_HPexpt_getripandsoundalignspikingAC3
format bank
scrsz = get(0,'ScreenSize');
x1=linspace(-500,500,101);
plotPairs=1;
savedir = '/data15/gideon/ProcessedData/';

% look at awake or sleep ripple correlations. Can also change the code to
% show both.
sleepwake='';


load([savedir 'NdlBrg_ripplemodsoundmod2_CA1_gather']);
allripplemodCA1=allripplemod;
allripplemod_idxCA1=allripplemod_idx;
allsoundmodCA1=allsoundmod;
allsoundmod_idxCA1=allsoundmod_idx;


load([savedir 'NdlBrg_ripplemodsoundmod2_AC_gather']);
allripplemodAC=allripplemod;
allripplemod_idxAC=allripplemod_idx;
allsoundmodAC=allsoundmod;
allsoundmod_idxAC=allsoundmod_idx;


load([savedir 'NdlBrg_ripplemodsoundmod2_CA1awake_gather']);
allripplemodCA1W=allripplemod;
allripplemod_idxCA1W=allripplemod_idx;
allsoundmodCA1W=allsoundmod;
allsoundmod_idxCA1W=allsoundmod_idx;


load([savedir 'NdlBrg_ripplemodsoundmod2_ACawake_gather']);
allripplemodACW=allripplemod;
allripplemod_idxACW=allripplemod_idx;
allsoundmodACW=allsoundmod;
allsoundmod_idxACW=allsoundmod_idx;



allpsCA1AC=[];
allrsCA1AC=[];
allpsCA1ACBef=[];
allrsCA1ACBef=[];
allpsCA1ACW=[];
allrsCA1ACW=[];
allpsCA1ACBefW=[];
allrsCA1ACBefW=[];

allanimsCA1AC=[];
alldaysCA1AC=[];
allCA1ACindscorrs=[];

allRspatialCorr=[];
allPspatialCorr=[];

combined_idx=unique([allripplemod_idxAC(:,1:2)],'rows');

for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    ACidx=find(ismember(allripplemod_idxAC(:,1:2),curidx,'rows'));
    CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
    for j=1:size(ACidx,1)
        for k=1:size(CA1idx,1)
            ACind1=ACidx(j);
            CA1ind1=CA1idx(k);
            
            AChist=allripplemodAC(1,ACind1).hist;
            CA1hist=allripplemodCA1(1,CA1ind1).hist;
            
            ACind1full=allripplemodAC(1,ACind1).index;
            CA1ind1full=allripplemodCA1(1,CA1ind1).index;
            
            CA1ind1W=find(ismember(allripplemod_idxCA1W,CA1ind1full,'rows'))
            ACind1W=find(ismember(allripplemod_idxACW,ACind1full,'rows'))
            if ~isempty(CA1ind1W)&~isempty(ACind1W)
                hasAwake=1;
                AChistW=allripplemodACW(1,ACind1W).hist;
                CA1histW=allripplemodCA1W(1,CA1ind1W).hist;
                ACampW=mean(AChistW(:,40:70),2);
                CA1ampW=mean(CA1histW(:,40:70),2);
                ACampBefW=mean(AChistW(:,1:21),2);
                CA1ampBefW=mean(CA1histW(:,1:21),2);
            else
                hasAwake=0;
                AChistW=nan;
                CA1histW=nan;
                ACampW=nan;
                CA1ampW=nan;
                ACampBefW=nan;
                CA1ampBefW=nan;
                
            end
            ACsndidx=find(ismember(allsoundmod_idxAC,ACind1full,'rows'));
            CA1sndidx=find(ismember(allsoundmod_idxCA1,CA1ind1full,'rows'));
            
            
            
            day=ACind1full(2);
            CA1tet=CA1ind1full(3);
            CA1cell=CA1ind1full(4);
            ACtet=ACind1full(3);
            ACcell=ACind1full(4);
            
            
            %sleep
            ACamp=mean(AChist(:,40:70),2);
            CA1amp=mean(CA1hist(:,40:70),2);
            ACampBef=mean(AChist(:,1:21),2);
            CA1ampBef=mean(CA1hist(:,1:21),2);
            
            rW=nan;pW=nan;rBefW=nan;pBefW=nan;
            if length(ACamp)==length(CA1amp)
                
                [r p]=corrcoef(ACamp,CA1amp);
                r=r(2,1);p=p(2,1);
                
                [rBef pBef]=corrcoef(ACampBef,CA1ampBef);
                rBef=rBef(2,1);pBef=pBef(2,1);
                
                allpsCA1AC=[allpsCA1AC p];
                allrsCA1AC=[allrsCA1AC r];
                allpsCA1ACBef=[allpsCA1ACBef pBef];
                allrsCA1ACBef=[allrsCA1ACBef rBef];
                
                
                if hasAwake & length(ACampW)==length(CA1ampW)
                    [rW pW]=corrcoef(ACampW,CA1ampW);
                    rW=rW(2,1);pW=pW(2,1);
                    
                    [rBefW pBefW]=corrcoef(ACampBef,CA1ampBef);
                    rBefW=rBefW(2,1);pBefW=pBefW(2,1);
                    
                    allpsCA1ACW=[allpsCA1ACW pW];
                    allrsCA1ACW=[allrsCA1ACW rW];
                    allpsCA1ACBefW=[allpsCA1ACBefW pBefW];
                    allrsCA1ACBefW=[allrsCA1ACBefW rBefW];
                else
                    hasAwake=0;
                end
                
                
                
                
                
                
                allanimsCA1AC=[allanimsCA1AC allripplemod_idxCA1(CA1ind1,1)];
                alldaysCA1AC=[alldaysCA1AC allripplemod_idxCA1(CA1ind1,2)];
                % creating a large matrix with the form:
                % anim-day-tet-cell-tet-cell-r-p-rBef-pBef
                allCA1ACindscorrs=[allCA1ACindscorrs; allripplemodAC(1,ACind1).index allripplemodCA1(1,CA1ind1).index(3:4) r p rBef pBef];
                
                % loading the appropriate mapfield file. Eventually do with
                % linfields instead of mapfields
                if day<10,daystr=['0' num2str(day)],else daystr=[num2str(day)];end
                load(['/data15/gideon/Ndl/Nadalmapfields' daystr '.mat'])
                load(['/data15/gideon/Ndl/Nadallinfields' daystr '.mat'])
                maxEpochs=size(mapfields{1,day},2);
                realEpochs=[];for k=1:maxEpochs,realEpochs=[realEpochs ~isempty(mapfields{1,day}{1,k})];end
                realEpochs=find(realEpochs);
                
            %    if plotPairs & (p<0.05 | pW<0.05)
                    clr = {'b','r','g','m','c','y','k','r'};
                    hh=figure('Position',[100 400 scrsz(3)/1.1 scrsz(4)/2]);
                    concatLinFieldsCA1Epochs=[];
                    concatLinFieldsACEpochs=[];
                    
                    % plotting lin fields for CA1 cell
                    for k=1:length(realEpochs)
                        subplot(4,7,k);
                        concatLinFieldsCA1=[];
                        % 4 trajectories
                        for b=1:4
                            if CA1cell<=size(linfields{1,day}{1,realEpochs(k)}{1,CA1tet},2)& ~isempty(linfields{1,day}{1,realEpochs(k)})&~isempty(linfields{1,day}{1,realEpochs(k)}{1,CA1tet})&~isempty(linfields{1,day}{1,realEpochs(k)}{1,CA1tet}{1,CA1cell})
                                plot(linfields{1,day}{1,realEpochs(k)}{1,CA1tet}{1,CA1cell}{b}(:,5),'color',clr{b});
                                concatLinFieldsCA1=[concatLinFieldsCA1 linfields{1,day}{1,realEpochs(k)}{1,CA1tet}{1,CA1cell}{b}(:,5)'];
                                hold on
                            end
                        end
                        
                        % calculating mean linear field across epochs by
                        % concatenating trajectory-fields for each epoch,
                        % and then averaging across epochs
                        
                        % sometimes there are slight differences in the
                        % length of linfields, this shortens to their
                        % minimum
                        if ~isempty(concatLinFieldsCA1Epochs)&length(concatLinFieldsCA1)>size(concatLinFieldsCA1Epochs,2)
                            concatLinFieldsCA1=concatLinFieldsCA1(1:size(concatLinFieldsCA1Epochs,2));
                        elseif ~isempty(concatLinFieldsCA1Epochs)&length(concatLinFieldsCA1)<size(concatLinFieldsCA1Epochs,2)
                            
                        concatLinFieldsCA1Epochs=concatLinFieldsCA1Epochs(:,1:size(concatLinFieldsCA1,2));
                            
                            
                        end
                        concatLinFieldsCA1Epochs=[concatLinFieldsCA1Epochs;concatLinFieldsCA1];


                        axis off
                        if k==1
                            h=legend('OutRealLeft','InRealLeft','OutRealRight','InRealRight','location','westoutside');
                            set(h,'fontsize',12)
                        end
                        
                    end
                    
                    % plotting map fields for CA1 cell
                    for k=1:length(realEpochs)
                        subplot(4,7,k+7);
                        if CA1cell<=size(mapfields{1,day}{1,realEpochs(k)}{1,CA1tet},2)& ~isempty(mapfields{1,day}{1,realEpochs(k)})&~isempty(mapfields{1,day}{1,realEpochs(k)}{1,CA1tet})&~isempty(mapfields{1,day}{1,realEpochs(k)}{1,CA1tet}{1,CA1cell})
                            imagesc(mapfields{1,day}{1,realEpochs(k)}{1,CA1tet}{1,CA1cell}.smoothedspikerate);
                            
                        end
                        
                        axis off
                        if k==1
                            title(['CA1 cell ' num2str(CA1ind1full) ' place field'])
                            
                        end
                        
                    end
                    % plotting lin fields for AC cell
                    for k=1:length(realEpochs)
                        subplot(4,7,k+21);
                        concatLinFieldsAC=[];

                        % 4 trajectories
                        for b=1:4
                            if ACcell<=size(linfields{1,day}{1,realEpochs(k)}{1,ACtet},2)& ~isempty(linfields{1,day}{1,realEpochs(k)})&~isempty(linfields{1,day}{1,realEpochs(k)}{1,ACtet})&~isempty(linfields{1,day}{1,realEpochs(k)}{1,ACtet}{1,ACcell})
                                plot(linfields{1,day}{1,realEpochs(k)}{1,ACtet}{1,ACcell}{b}(:,5),'color',clr{b});
                                concatLinFieldsAC=[concatLinFieldsAC linfields{1,day}{1,realEpochs(k)}{1,ACtet}{1,ACcell}{b}(:,5)'];

                                hold on
                            end
                        end
                        % calculating mean linear field across epochs by
                        % concatenating trajectory-fields for each epoch,
                        % and then averaging across epochs
                        
                        % sometimes there are slight differences in the
                        % length of linfields, this shortens to their
                        % minimum
                    if ~isempty(concatLinFieldsACEpochs)&length(concatLinFieldsAC)>size(concatLinFieldsACEpochs,2)
                            concatLinFieldsAC=concatLinFieldsAC(1:size(concatLinFieldsACEpochs,2));
                        elseif ~isempty(concatLinFieldsACEpochs)&length(concatLinFieldsAC)<size(concatLinFieldsACEpochs,2)
                            
                        concatLinFieldsACEpochs=concatLinFieldsACEpochs(:,1:size(concatLinFieldsAC,2));
                            
                            
                        end
                        concatLinFieldsACEpochs=[concatLinFieldsACEpochs;concatLinFieldsAC];

                    
                        axis off
                        if k==1
                            h=legend('OutRealLeft','InRealLeft','OutRealRight','InRealRight','location','westoutside');
                            set(h,'fontsize',12)
                        end
                        
                    end
                    
                    
                    % plotting map fields for AC cell
                    for k=1:length(realEpochs)
                        subplot(4,7,k+14);
                        if ACcell<=size(mapfields{1,day}{1,realEpochs(k)}{1,ACtet},2)& ~isempty(mapfields{1,day}{1,realEpochs(k)})&~isempty(mapfields{1,day}{1,realEpochs(k)}{1,ACtet})&~isempty(mapfields{1,day}{1,realEpochs(k)}{1,ACtet}{1,ACcell})
                            imagesc(mapfields{1,day}{1,realEpochs(k)}{1,ACtet}{1,ACcell}.smoothedspikerate);
                            
                        end
                        
                        axis off
                        if k==1
                            title(['AC cell ' num2str(ACind1full) ' place field'])
                            
                        end
                        
                    end
                    
                    % plotting sound responses of CA1 cell
                    subplot(4,7,12)
                    if ~isempty(CA1sndidx)
                        imagesc(x1,1:size(allsoundmodCA1(1,CA1sndidx).hist,1),allsoundmodCA1(1,CA1sndidx).hist)
                    end
                    title(['sound'])
                    
                    % plotting sound responses of AC cell
                    subplot(4,7,19)
                    
                    if ~isempty(ACsndidx)
                        imagesc(x1,1:size(allsoundmodAC(1,ACsndidx).hist,1),allsoundmodAC(1,ACsndidx).hist)
                    end
                    xlabel('Time(ms)')
                    ylabel('auditory stim')
                    
                    % plotting SLEEP ripple responses of CA1 cell
                    subplot(4,7,13)
                    imagesc(x1,1:size(CA1hist,1),CA1hist)
                    title(['Sleep Ripples'])
                    
                    % plotting SLEEP ripple responses of AC cell
                    
                    subplot(4,7,20)
                    imagesc(x1,1:size(AChist,1),AChist)
                    if p<0.05
                        title(['R='  sprintf('%0.2f',r) '*'])
                    else
                        title(['R='  sprintf('%0.2f',r) 'NS'])
                        
                    end
                    %-
                    % plotting AWAKE ripple responses of CA1 cell
                    subplot(4,7,14)
                    imagesc(x1,1:size(CA1histW,1),CA1histW)
                    title(['Awake Ripples'])
                    
                    % plotting AWAKE ripple responses of AC cell
                    
                    subplot(4,7,21)
                    imagesc(x1,1:size(AChistW,1),AChistW)
                    if pW<0.05
                        title(['R='  sprintf('%0.2f',rW) '*'])
                    else
                        title(['R='  sprintf('%0.2f',rW) 'NS'])
                        
                    end
                    
                    
                    % calculating spatial correlation
                    ACmeanlinfield=nanmean(concatLinFieldsACEpochs,1);
                    CA1meanlinfield=nanmean(concatLinFieldsCA1Epochs,1);
                    ACmeanlinfield=ACmeanlinfield(1:min(length(ACmeanlinfield),length(CA1meanlinfield)));
                    CA1meanlinfield=CA1meanlinfield(1:min(length(ACmeanlinfield),length(CA1meanlinfield)));
                    keepinds=find(~isnan(CA1meanlinfield)&~isnan(ACmeanlinfield));
                    CA1meanlinfield=CA1meanlinfield(keepinds);
                    ACmeanlinfield=ACmeanlinfield(keepinds);
                    [RspatialCorr PspatialCorr]=corrcoef(ACmeanlinfield,CA1meanlinfield);
                    allRspatialCorr=[allRspatialCorr RspatialCorr(1,2)];
                    allPspatialCorr=[allPspatialCorr PspatialCorr(1,2)];
                    
                    
                    subplot(4,7,18)
                    if PspatialCorr(2,1)<0.05
                    title(['Spatial corr= ' num2str(RspatialCorr(1,2)) ' *'])
                    else
                    title(['Spatial corr= ' num2str(RspatialCorr(1,2)) ' NS'])
                        
                    end
                    if p<0.05 | pW<0.05
%                           figure(100)
%                     plot(RspatialCorr(1,2),r,'ko');
%                     hold on
                    
                    keyboard
                    end
                  
                     close(hh)
                %end
            end
            %keyboard
        end
    end
    
    
    
end

% testing for correlation between spatial correlation and ripple
% correlation
keepinds2=find(~isnan(allRspatialCorr));
allRspatialCorrGood=allRspatialCorr(keepinds2);
allrsCA1ACGood=allrsCA1AC(keepinds2);
allPspatialCorrGood=allPspatialCorr(keepinds2);
allpsCA1ACGood=allpsCA1AC(keepinds2);
% plotting and testing for only spatially significantly correlated pairs
% AND significantly ripple correlated pairs
figure;plot(allRspatialCorrGood(allPspatialCorrGood<0.05&allpsCA1ACGood<0.05),allrsCA1ACGood(allPspatialCorrGood<0.05&allpsCA1ACGood<0.05),'ko')
[rr pp]=corrcoef(allRspatialCorrGood(allPspatialCorrGood<0.05&allpsCA1ACGood<0.05),allrsCA1ACGood(allPspatialCorrGood<0.05&allpsCA1ACGood<0.05))
xlabel('spatial correlation')
ylabel('Ripple correlation')

% NOTE, NOW IN allCA1ACindscorrs THERE ARE ALL INDICES OF PAIRS AND THEIR
% RIP-CORR VALUES. RELATE THIS TO THEIR PLACE FIELDS AND SOUND
% RESPONSES!!!

% adjusting days for Nadal
alldaysCA1AC(allanimsCA1AC==1)=alldaysCA1AC(allanimsCA1AC==1)-7;

rateSigPairs=[];numPairs=[];
for day1=1:11,rateSigPairs=[rateSigPairs mean(allpsCA1AC(alldaysCA1AC==day1&allanimsCA1AC==1)<0.05)];numPairs=[numPairs length(allpsCA1AC(alldaysCA1AC==day1&allanimsCA1AC==1))];end
figure;plot(rateSigPairs)
xlabel('day');ylabel('fraction of sig rip-mod pairs');title('fraction of significantly rip-mod CA1-AC pairs, Nadal')
% significantly more pairs during ripples compared to before
ztestprop2([sum(allpsCA1AC<0.05) length(allpsCA1AC)],[sum(allpsCA1ACBef<0.05) length(allpsCA1ACBef)])

% Correlations are stronger during ripples, both positive and negative-
% nice!
figure;
subplot(1,2,1)
plot(allrsCA1AC(allpsCA1AC<0.05),allrsCA1ACBef(allpsCA1AC<0.05),'ko')
hold on
plot(-0.5:0.01:1,-0.5:0.01:1,'r')
[h p]=ttest(abs(allrsCA1AC(allpsCA1AC<0.05)),abs(allrsCA1ACBef(allpsCA1AC<0.05)))
xlabel('rip-corr during rips')
ylabel('rip-corr before rips')
subplot(1,2,2)
ratioDurPreCor=(abs(allrsCA1AC(allpsCA1AC<0.05))./abs(allrsCA1ACBef(allpsCA1AC<0.05)));
ratioDurPreCor=ratioDurPreCor(ratioDurPreCor<100);
hist(log(ratioDurPreCor))
xlabel('log(ratio Dur/Pre Cor)')
ylabel('count')
keyboard