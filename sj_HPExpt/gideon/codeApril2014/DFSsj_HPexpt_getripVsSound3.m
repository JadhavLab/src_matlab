
% GIDEON: CONTINUE AFTER THE SECTION OF PLOTSOUNDRIPPLEMOD, TO SAVE THESE
% DATA AND PROCESS THEM

% Ver2, Dec 2013 - Implement Min. NSpike condition for PFC cells. See Line 47 and Line 240

% Ripple modulation of cells, especilally PFC cells. Time filter version of sj_HPexpt_ripalign_singlecell_getrip4.
% Will call DFAsj_getripalign.m
% Also see DFSsj_plotthetamod.m and DFSsj_HPexpt_xcorrmeasures2. Will gather data like these

clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells
savedirX = '/opt/data15/gideon/ProcessedData/';
plotsoundripplemod=1;
%val=1;savefile = [savedirX 'NdlBrg_ripVsSound2_AC']; area = 'AC';
%val=2;savefile = [savedirX 'NdlBrg_ripVsSound2_CA1']; area = 'CA1';
%val=3;savefile = [savedirX 'NdlBrg_ripVsSound2_PFC']; area = 'PFC';
val=4;savefile = [savedirX 'NdlBrg_ripVsSound2_ACawake']; area = 'AC';
%val=5;savefile = [savedirX 'NdlBrg_ripVsSound2_CA1awake']; area = 'CA1';
%val=6;savefile = [savedirX 'NdlBrg_ripVsSound2_PFCawake']; area = 'PFC';

% for val=1:6
%         if val==1, savefile = [savedirX 'NdlBrg_ripVsSound2_AC']; area = 'AC'; end
%     if val==2, savefile = [savedirX 'NdlBrg_ripVsSound2_CA1']; area = 'AC'; end
%     if val==3, savefile = [savedirX 'NdlBrg_ripVsSound2_PFC']; area = 'AC'; end
%
%     if val==4, savefile = [savedirX 'NdlBrg_ripVsSound2_ACawake']; area = 'AC'; end
%     if val==5, savefile = [savedirX 'NdlBrg_ripVsSound2_CA1awake']; area = 'CA1'; end
%     if val==6, savefile = [savedirX 'NdlBrg_ripVsSound2_PFCawake']; area = 'PFC'; end
%

%save([savedirX 'area'],'area');
figdir='/home/gideon/ripsoundmod/newslow/';


%val=1; savefile = [savedirX 'HP_ripplemod_PFC_alldata']; area = 'PFC'; clr = 'b'; % PFC
%val=2; savefile = [savedirX 'HP_ripplemod_CA1_alldata']; area = 'CA1';  clr = 'r';% CA1
%val=3;savefile = [savedirX 'HP_ripplemod_PFC_alldata_speed']; area = 'PFC'; clr = 'b'; % PFC - low speed criterion
%val=4;savefile = [savedirX 'HP_ripplemod_PFC_alldata_stdev5']; area = 'PFC'; clr = 'b'; % PFC - low speed criterion
%val=5;savefile = [savedirX 'HP_ripplemod_PFC_alldata_singletrack']; area = 'PFC'; clr = 'b'; % PFC - low speed criterion

%val=7;savefile = [savedirX 'HP_ripplemod_CA1_alldata_speed_minrip2_feb14']; area = 'CA1'; clr = 'b'; % PFC - low speed criterion


savefig1=0;


% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


%If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
       animals = {'Nadal','Borg'};
    %animals = {'Nadal'};
    %   animals = {'Borg'};
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    % dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    
    %  runepochfilter = 'isequal($environment, ''ytr'')';
    runepochfilter = 'isequal($type, ''run'')';
    
    sleepepochfilter ='isequal($type, ''sleep'') && isequal($audprot, ''0'')';
    
    % Cell filter
    % -----------
    %     switch val
    %         case 1
    %             cellfilter = 'strcmp($area, ''AC'') && ($numspikes > 100)'; %
    %         case 2
    %             cellfilter = 'strcmp($area, ''CA1'') && ($numspikes > 100)'; %
    %         case 3
    %             cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; %
    %         case 4
    %             cellfilter = 'strcmp($area, ''AC'') && ($numspikes > 100)'; %
    %             case 5
    %             cellfilter = 'strcmp($area, ''CA1'') && ($numspikes > 100)'; %
    %             case 6
    %             cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; %
    %
    %
    %             %cellfilter = 'strcmp($area, ''PFC'')'; % This includes all, including silent cells
    %             %         case 2
    %             %             %cellfilter = ' (strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ~strcmp($tag2, ''CA1Int'') && ~strcmp($tag2, ''iCA1Int'')';
    %             %             cellfilter = '(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7)';
    %             %         case 3
    %             %             cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; % PFC cells with spiking criterion
    %             %             %cellfilter = 'strcmp($area, ''PFC'')'; % This includes all, including silent cell
    %             %         case 4
    %             %             cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; % PFC cells with spiking criterion
    %             %             %cellfilter = 'strcmp($area, ''PFC'')'; % This includes all, including silent cells
    %             %         case 5
    %             %             cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; % PFC cells with spiking criterion
    %             %             %cellfilter = 'strcmp($area, ''PFC'')'; % This includes all, including silent cells
    %             %
    %             %         case 6
    %             %             cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; % PFC cells with spiking criterion
    %             %             %cellfilter = 'strcmp($area, ''PFC'')'; % This includes all, including silent cells
    %             %         case 7
    %             %             cellfilter = '(strcmp($area, ''CA1'')|| strcmp($area, ''iCA1'')) && ($numspikes > 100)';
    %     end
    
    % cellfilter = 'strcmp($tag2, ''PFC'')';
    %cellfilter = 'strcmp($tag2, ''CA1Pyr'') && ($numspikes > 100)'; % This includes all.
    % For more exclusive choosing, use $tag. Take care of number of spikes while gathering data
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % The following are ripple time filter options. Instead of using time filters, within the function,
    % call getripples using the riptetfilter option to get the riptimes. Since you want a vector of riptimes.
    % You can also use inter-ripple-interval of 1 sec within the function, etc.
    
    % a) Using getriptimes: generates 1 vector with 1's for ripple times for all tetrodes.
    % Should be similar to getripples/ getripplees_direct
    
    %     timefilterrun_rip = {{'DFTFsj_getvelpos', '(($absvel <= 5))'},...
    %         {'DFTFsj_getriptimes','($nripples > 0)','tetfilter',riptetfilter,'minthresh',3}};
    
    % b) getripples. This is similar to getripltimes, but gives start and end of each ripple time
    % instead of a time filter. Also has an additional condition of at least 50 ms ripple.
    
    % Iterator
    % --------
    iterator = 'stimtimeanal';
    
    % Filter creation
    % ----------------
    %     modf = createfilter('animal',animals,'days',dayfilter,'epochs',runepochfilter, 'cells',...
    %         cellfilter, 'iterator', iterator);
    
    
    if val<4 % awake sound, sleep ripples
        modf = createfilter('animal',animals,'epochs',sleepepochfilter,...
            'iterator', iterator);
        
        modg = createfilter('animal',animals,'epochs',runepochfilter,...
            'iterator', iterator);
    else % awake sound, awake ripples
        modf = createfilter('animal',animals,'epochs',runepochfilter,'iterator', iterator);
        
        modg = createfilter('animal',animals,'epochs',runepochfilter,'iterator', iterator);
        
    end
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    
    % switch val
    %  case 1
    
    modg = setfilterfunction(modg,'DFAgr_getsoundtimesGR',{'sound', 'tetinfo', 'pos'},'dospeed',0);%1,'lowsp_thrs',3); % Default stdev is 3
    
    modf = setfilterfunction(modf,'DFAsj_getriptimesGR',{'ripples', 'tetinfo', 'pos'},'dospeed',0);%1,'lowsp_thrs',6,'minrip',2); % Default stdev is 3
    
    %        case 2
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'}); %
    %        case 3
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'dospeed',1,'lowsp_thrs',5); %
    %        case 4
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'minstd',5); %
    %        case 5
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'}); % Default stdev is 3
    %
    %        case 6
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'dospeed',1,'lowsp_thrs',4,'minrip',2); % Default stdev is 3
    %        case 7
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'dospeed',1,'lowsp_thrs',4,'minrip',2); % Default stdev is 3
    %
    
    %   end
    % Going to call getripples_tetinfo within function
    
    % Run analysis
    % ------------
    modg = runfilter(modg);
    modf = runfilter(modf);
    
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------



% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
switch val
    case 1
        gatherdatafile = [savedirX 'Ndl_ripVsSound_gather_AC']; % PFC cells to Hipp ripples
    case 2
        gatherdatafile = [savedirX 'Ndl_ripVsSound_gather_CA1']; % PFC cells to Hipp ripples
    case 3
        gatherdatafile = [savedirX 'Ndl_ripVsSound_gather_PFC']; % PFC cells to Hipp ripples
    case 4
        gatherdatafile = [savedirX 'Ndl_ripVsSound_gather_ACawake']; % PFC cells to Hipp ripples
    case 5
        gatherdatafile = [savedirX 'Ndl_ripVsSound_gather_CA1awake']; % PFC cells to Hipp ripples
    case 6
        gatherdatafile = [savedirX 'Ndl_ripVsSound_gather_PFCawake']; % PFC cells to Hipp ripples
        
end




if gatherdata
    
    
    
    
    %  ========= RIPPLES DATA ==========
    cnt=0; % Count how many cells will be kept based on nspikes in output: >0
    allanimindex_rip=[]; alldataraster_rip=[]; alldatahist_rip = []; all_Nspk_rip=[];
    
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
            %  if (modf(an).output{1}(i).Nspikes > 0)
            cnt=cnt+1;
            anim_index_rip{an}(cnt,:) = modf(an).output{1}(i).index;
            % Only indexes
            animindex_rip=[an modf(an).output{1}(i).index]; % Put animal index in front
            allanimindex_rip = [allanimindex_rip; animindex_rip]; % Collect all Anim Day Epoch Tet Cell Index
            % Data
            allriptimes{cnt}=modf(an).output{1}(i).rip_starttime;
            
            
        end
        
    end
    
    %  ========= SOUND DATA ==========
    
    cnt=0; % Count how many cells will be kept based on nspikes in output: >0
    allanimindex_snd=[]; alldataraster_snd=[]; alldatahist_snd = []; all_Nspk_snd=[];
    
    for an = 1:length(modg)
        for i=1:length(modg(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
            % if (modg(an).output{1}(i).Nspikes > 0)
            cnt=cnt+1;
            anim_index_snd{an}(cnt,:) = modg(an).output{1}(i).index;
            % Only indexes
            animindex_snd=[an modg(an).output{1}(i).index]; % Put animal index in front
            allanimindex_snd = [allanimindex_snd; animindex_snd]; % Collect all Anim Day Epoch Tet Cell Index
            % Data
            allsoundtimes{cnt}=modg(an).output{1,1}(i).sound_starttime;
            allsoundind{cnt}=modg(an).output{1,1}(i).soundind;
            
            
        end
        
    end
    
    
    % Consolidate single cells across epochs RIPPLES
    % ----------------------------------------------------------------------------
    allSoundInds=[];
    allRipInds=[];
    allRip = struct;
    allanimindex_rip;  % all anim-day-epoch-tet-cell indices
    uniqueAnimDay=unique(allanimindex_rip(:,1:2),'rows');
    
    cntcellsRip=0;
    for i=1:size(uniqueAnimDay,1)
        animday=uniqueAnimDay(i,:);
        ind=find(ismember(allanimindex_rip(:,1:2),animday,'rows'))';
        
        allriptimesDay=[];
        for r=ind
            a=allriptimes{r};
            allriptimesDay=[allriptimesDay; a];
        end
        
        lastInd=length(allRip);
        allRip(lastInd+1).index=animday;
        allRip(lastInd+1).riptimes=allriptimesDay;
        allRipInds=[allRipInds;animday];
        
        
    end
    allRip=allRip(2:end);
    
    %     Consolidate single cells across epochs SOUNDS
    %     ----------------------------------------------------------------------------
    
    
    allSound = struct;
    allanimindex_rip;  % all anim-day-epoch-tet-cell indices
    uniqueAnimDay=unique(allanimindex_snd(:,1:2),'rows');
    
    cntcellsRip=0;
    for i=1:size(uniqueAnimDay,1)
        animday=uniqueAnimDay(i,:);
        ind=find(ismember(allanimindex_snd(:,1:2),animday,'rows'))';
        
        allSoundtimesDay=[];
        allSoundIndDay=[];
        
        for r=ind
            a=allsoundtimes{r};
            b=allsoundind{r};
            
            allSoundtimesDay=[allSoundtimesDay; a'];
            allSoundIndDay=[allSoundIndDay; b'];
            
        end
        
        lastInd=length(allSound);
        allSound(lastInd+1).index=animday;
        allSound(lastInd+1).soundtimes=allSoundtimesDay;
        allSound(lastInd+1).soundinds=allSoundIndDay;
        
        allSoundInds=[allSoundInds;animday];
        
        
        
    end
    allSound=allSound(2:end);
    
    
    xaxis=-20:0.001:20-0.001;
    xaxis2=-1:0.001:1-0.001;
allCurMat2=[];
    for i=1:size(allSoundInds,1)
        curIndS=i;
        curIndR=find(ismember(allRipInds,allSoundInds(curIndS,:),'rows'));
        curSoundTimes=allSound(curIndS).soundtimes;
        curSoundInds=allSound(curIndS).soundinds;
        
        curRipTimes=allRip(curIndR).riptimes;
        
%         figure;plot([curRipTimes curRipTimes],[0 2],'k');hold on;plot(curSoundTimes,1,'ro');
%         title(num2str(allSound(curIndS).index))
        
        % Long time window around beginning of sound series
        % PSTH is from -20S to +20S relative to sound series start
        % at a 1ms resolution
        firstSounds=find(curSoundInds==1);
        curMat=zeros(length(firstSounds),40000);
        for j=1:length(firstSounds)
           
            curSeriesStart=curSoundTimes(firstSounds(j));
            curSeriesPre=curSoundTimes(firstSounds(j))-20000;
            curSeriesEnd=curSoundTimes(firstSounds(j))+20000;
            ripsInWindow=curRipTimes(find(curRipTimes>curSeriesPre&curRipTimes<curSeriesEnd))-curSeriesPre;
            curMat(j,round(ripsInWindow))=1;
            
        end
        figure;
        plot(xaxis,smooth(mean(curMat),200))
        title(num2str(allSound(curIndS).index))
       % keyboard
        
        % Short time window around beginning of every sound 
        % PSTH is from -1S to +1S relative to sound onset
        % at a 1ms resolution
        allSoundResps=[];
        curMat2=zeros(length(curSoundInds),2000);
        for j=1:length(curSoundInds)
            curSoundStart=curSoundTimes(j);
            curSoundPre=curSoundTimes(j)-1000;
            curSoundEnd=curSoundTimes(j)+1000;
            ripsInWindow=curRipTimes(find(curRipTimes>curSoundPre&curRipTimes<curSoundEnd))-curSoundPre;
            curMat2(j,round(ripsInWindow))=1;
            
        end
        figure;plot(xaxis2,smooth(mean(curMat2),40))
        title(num2str(allSound(curIndS).index))
      %  figure;for k=1:15,subplot(3,5,k);plot(xaxis2,mean(curMat2(curSoundInds==k,:)));title(k);end
allCurMat2=[allCurMat2;curMat2];
        keyboard
        %close all;
    end
end
