% works on the output of DFSsj_HPexpt_getripandsoundalignspikingAC3
savedirX = '/opt/data15/gideon/ProcessedData/';
plotIndividualCells=0;
areas={'CA1','AC','PFC'}
%areas={'AC'}

histOns=[];
histOnsW=[];
%for areaInd=1:3
 %   area=areas{areaInd};
%switch area
 %   case 'AC'

% AC 
load([savedirX 'NdlBrg_ripplemodsoundmod4_ACawake_gather']);
ACallripplemodW=allripplemod;
ACallripplemod_idxW=allripplemod_idx;
load([savedirX 'NdlBrg_ripplemodsoundmod4_AC_gather']);
ACallripplemod=allripplemod;
ACallripplemod_idx=allripplemod_idx;

% CA1
load([savedirX 'NdlBrg_ripplemodsoundmod2_CA1awake_gather']);
CA1allripplemodW=allripplemod;
CA1allripplemod_idxW=allripplemod_idx;
load([savedirX 'NdlBrg_ripplemodsoundmod2_CA1_gather']);
CA1allripplemod=allripplemod;
CA1allripplemod_idx=allripplemod_idx;

% PFC
load([savedirX 'NdlBrg_ripplemodsoundmod4_PFCawake_gather']);
PFCallripplemodW=allripplemod;
PFCallripplemod_idxW=allripplemod_idx;
load([savedirX 'NdlBrg_ripplemodsoundmod4_PFC_gather']);
PFCallripplemod=allripplemod;
PFCallripplemod_idx=allripplemod_idx;



% AC sleep
ACsleepSWRsig=[];
for i=1:size(ACallripplemod_idx,1)
    if ACallripplemod(1,i).rasterShufP2<.05
    ACsleepSWRsig=[ACsleepSWRsig 1];
    else
        ACsleepSWRsig=[ACsleepSWRsig 0];
    end
end
% AC awake
ACwakeSWRsig=[];
for i=1:size(ACallripplemod_idxW,1)
    if ACallripplemodW(1,i).rasterShufP2<.05
    ACwakeSWRsig=[ACwakeSWRsig 1];
    else
        ACwakeSWRsig=[ACwakeSWRsig 0];
    end
end


% CA1 sleep
CA1sleepSWRsig=[];
for i=1:size(CA1allripplemod_idx,1)
    if CA1allripplemod(1,i).rasterShufP2<.05
    CA1sleepSWRsig=[CA1sleepSWRsig 1];
    else
        CA1sleepSWRsig=[CA1sleepSWRsig 0];
    end
end
% CA1 awake
CA1wakeSWRsig=[];
for i=1:size(CA1allripplemod_idxW,1)
    if CA1allripplemodW(1,i).rasterShufP2<.05
    CA1wakeSWRsig=[CA1wakeSWRsig 1];
    else
        CA1wakeSWRsig=[CA1wakeSWRsig 0];
    end
end



% PFC sleep
PFCsleepSWRsig=[];
for i=1:size(PFCallripplemod_idx,1)
    if PFCallripplemod(1,i).rasterShufP2<.05
    PFCsleepSWRsig=[PFCsleepSWRsig 1];
    else
        PFCsleepSWRsig=[PFCsleepSWRsig 0];
    end
end
% PFC awake
PFCwakeSWRsig=[];
for i=1:size(PFCallripplemod_idxW,1)
    if PFCallripplemodW(1,i).rasterShufP2<.05
    PFCwakeSWRsig=[PFCwakeSWRsig 1];
    else
        PFCwakeSWRsig=[PFCwakeSWRsig 0];
    end
end

% both animals
rateACwake=mean(ACwakeSWRsig);
rateACsleep=mean(ACsleepSWRsig);
rateCA1wake=mean(CA1wakeSWRsig);
rateCA1sleep=mean(CA1sleepSWRsig);
ratePFCwake=mean(PFCwakeSWRsig);
ratePFCsleep=mean(PFCsleepSWRsig);

rateACwake1=mean(ACwakeSWRsig(ACallripplemod_idxW(:,1)==1));
rateACwake2=mean(ACwakeSWRsig(ACallripplemod_idxW(:,1)==2));

rateCA1wake1=mean(CA1wakeSWRsig(CA1allripplemod_idxW(:,1)==1));
rateCA1wake2=mean(CA1wakeSWRsig(CA1allripplemod_idxW(:,1)==2));

ratePFCwake1=mean(PFCwakeSWRsig(PFCallripplemod_idxW(:,1)==1));
ratePFCwake2=mean(PFCwakeSWRsig(PFCallripplemod_idxW(:,1)==2));

rateACsleep1=mean(ACsleepSWRsig(ACallripplemod_idx(:,1)==1));
rateACsleep2=mean(ACsleepSWRsig(ACallripplemod_idx(:,1)==2));

rateCA1sleep1=mean(CA1sleepSWRsig(CA1allripplemod_idx(:,1)==1));
rateCA1sleep2=mean(CA1sleepSWRsig(CA1allripplemod_idx(:,1)==2));

ratePFCsleep1=mean(PFCsleepSWRsig(PFCallripplemod_idx(:,1)==1));
ratePFCsleep2=mean(PFCsleepSWRsig(PFCallripplemod_idx(:,1)==2));

