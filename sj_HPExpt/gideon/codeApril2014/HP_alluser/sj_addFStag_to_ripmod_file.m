
% Load the "ripplemod" file and the "cellinfo" file for PFC cells, and add
% a "FStag" field to the allripplemod structure identifying the FS cells

clear;
%savedir = 'mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';

% Ripplemod file
% --------------
ripplefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014']; 
%load(ripplefile, 'allripplemod','allripplemod_idx'); % load allripplemod and allripplemod_idx. 
load(ripplefile); % Lod the entire file since you have to eventually save it

% Cellinfo file
% --------------
cellfile = [savedir, 'HP_cellinfo_PFC_gather_3-7-2014']; 
load(cellfile,'FSidx');

% First add a FS tag field for allripplemod
% ------------------------------------------
for i = 1:size(allripplemod_idx,1)
    allripplemod(i).FStag = 'n';
end
   
for i = 1:size(FSidx,1)
    
    curridx = FSidx(i,:);
    
    match = rowfind(curridx, allripplemod_idx)
    
    if match~=0
        allripplemod(match).FStag = 'y';
    end
end


savedata = 1;
% Update filename by putting a "FStagged" in front of it
% ------------------------------------------------------
savefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014_FStagged']; 
save(savefile);

    
    
    





