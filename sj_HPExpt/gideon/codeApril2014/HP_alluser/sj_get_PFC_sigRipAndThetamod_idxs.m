
% Get the list of cells with significant ripple modulation

% demetris' directory
cd '/mnt/data25/sjadhav/HPExpt/HP_ProcessedData'

% cd '/data25/sjadhav/HPExpt/HP_ProcessedData'

% Ripplemod
% ----------
load('HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014','allripplemod','allripplemod_idx');
PFC_sigripmod_idxs=[]; PFC_ripUnmod_idxs=[];

for i=1:length(allripplemod)
    
    if allripplemod(i).rasterShufP2 < 0.05
        PFC_sigripmod_idxs=[PFC_sigripmod_idxs; allripplemod(i).index];
    else
        PFC_ripUnmod_idxs=[PFC_ripUnmod_idxs; allripplemod(i).index];
    end
    
end

% Thetamod 
% -------
load('HP_thetamod_PFC_alldata_Nspk50_gather_3-6-2014','allthetamod','allthetamod_idx');
PFC_sigthetamod_idxs=[]; PFC_thetaUnmod_idxs=[];

for i=1:length(allthetamod)
    
    if allthetamod(i).prayl < 0.05
        PFC_sigthetamod_idxs=[PFC_sigthetamod_idxs; allthetamod(i).index];
    else
        PFC_thetaUnmod_idxs=[PFC_thetaUnmod_idxs; allthetamod(i).index];
    end
    
end

% Ripplemod and Thetamod both
% ---------------------------
cntcells=0; allidxs=[]; allprayl=[]; allpvar2=[]; match_gather=[];
for i=1:length(allthetamod)
    i;
    curridx = allthetamod(i).index;
    match = rowfind(curridx, allripplemod_idx);
    
    if match~=0,    
        cntcells = cntcells+1;
        allidxs(cntcells,:) = curridx;
        % Theta
        allprayl(cntcells) = allthetamod(i).prayl;
        % Ripple
        allpvar2(cntcells) = allripplemod(match).rasterShufP2;
    end
end

sigboth = find(allprayl<0.05 & allpvar2<0.05); sigtheta = find(allprayl<0.05); sigrip = find(allpvar2<0.05);
PFC_sigRipAndTheta_Idxs = allidxs(sigboth,:);


keyboard;
% Save data
% ---------
save('PFC_sigripmod_idxs.mat','PFC_sigripmod_idxs');
save('PFC_sigthetamod_idxs.mat','PFC_sigthetamod_idxs');
save('PFC_sigRipAndTheta_Idxs.mat','PFC_sigRipAndTheta_Idxs');


