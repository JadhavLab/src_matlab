% based on predurACCA1GLM4

%% --------------- START: Predicting CA1 SWR activity from preceding AC
savedirX = '/opt/data15/gideon/HP_ProcessedData/';
% the following files were created by
% DFSsj_HPexpt_getripalignspiking_ver6.m (I think..)



load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6'])

allripplemodPFC=allripplemod;
allripplemod_idxPFC=allripplemod_idx;

firingratesexc=[];
firingratesinh=[];
firingratesunmod=[];

indexc=[];
indinh=[];

for v=1:length(allripplemodPFC)
    if allripplemodPFC(1,v).rasterShufP2<0.05 & strcmp(allripplemodPFC(1,v).type,'exc')
        firingratesexc=[firingratesexc allripplemodPFC(1,v).cellfr];
        indexc=[indexc; allripplemodPFC(1,v).index];
    elseif allripplemodPFC(1,v).rasterShufP2<0.05 & strcmp(allripplemodPFC(1,v).type,'inh')
        firingratesinh=[firingratesinh allripplemodPFC(1,v).cellfr];
        indinh=[indinh; allripplemodPFC(1,v).index];

    elseif allripplemodPFC(1,v).rasterShufP2>0.05
        firingratesunmod=[firingratesunmod allripplemodPFC(1,v).cellfr];
    end
end
%% firing rates are not different between rip-exc/rip-inh

X=nan(250,3);
X(1:60,1)=firingratesexc';
X(1:55,2)=firingratesinh';
X(1:209,3)=firingratesunmod';
[p table stats]=anova1(X)
multcompare(stats)
figure;multcompare(stats)

%% days
% correcting for nadal
indinh(indinh(:,1)==4,2)=indinh(indinh(:,1)==4,2)-7;
indexc(indexc(:,1)==4,2)=indexc(indexc(:,1)==4,2)-7;
onsametet=intersect(indinh(:,1:3),indexc(:,1:3),'rows')
