% Adapted to Borg from Nadal on April 1, 2014

% Based on GRdioanalysis

% From GRdioanalysis:
% This script quantifies behavior from dio data. First, diodayprocess was
% run on all days (see preprocessing file).
% The way I quantify is as follows:
% For each reward given, I look at the time window starting from the
% previous reward to just before the current reward was given (1000 samples
% back from the current reward, so as not to include the poke invoking the new reward).
% If during that interval there were only pokings corresponding to the previous
% reward well, this is a correct trial (there are always multiple pokings to
% get a single reward). If during that interval other wells were triggered,
% this is an error trial. Errors and correct trials are stored per pair of
% wells in the following form: an error trial is stored in errorTypes(x,y),
% where x is the previous reward location, and y is the first well he
% erroneously poked in after that (notice, NOT where he was supposed to
% go). A correct trial is counted in correctTypes(x,y) where x is the previous
% rewarded well and y is the current rewarded well.


% 11,13,16 are inputs (rat triggering wells)
% 30,31,32 are outputs (milk coming out)
% On day 10 no reward was given in sound well, so no bit 30

savedirimgs='/data15/gideon/Brg/Figures/rewardPSTH/';



toSilentCorrectRatios=[];
toSoundCorrectRatios=[];
toHomeCorrectRatios=[];
allRewards=[];
sequentialTrials=[];
figure(100);
for day1=1:11
    if day1<10
        dayStr=['0' num2str(day1)];
    else
        dayStr=[num2str(day1)];
    end
    
    load(['/data15/gideon/Brg/brgDIO' dayStr '.mat'])
    d11=diopulses{11}.pulsetimes(:,1);
    d16=diopulses{16}.pulsetimes(:,1);
    d13=diopulses{13}.pulsetimes(:,1);
    try
        d30=diopulses{30}.pulsetimes(:,1);
    catch
        'No sound rewards, should be only day 10'
        d30=0;
    end
    d31=diopulses{31}.pulsetimes(:,1);
    d32=diopulses{32}.pulsetimes(:,1);
    
    
        silentWell=d11;
        silentReward=d31;
        soundWell=d13;
        soundReward=d30;
        homeWell=d16;
        homeReward=d32;
        corresp1=[11 31;13 30;16 32];
        
   
    
     psthTimeWindowInS=20;
     timeAxis=-psthTimeWindowInS/2:0.001:psthTimeWindowInS/2;
    
    allWells={homeWell,silentWell,soundWell};
    
    % Deleting spurious rewards that sometimes appear as doubles
    soundReward=[soundReward(diff(soundReward)>5000); soundReward(end)];
    silentReward=[silentReward(diff(silentReward)>5000); silentReward(end)];
    homeReward=[homeReward(diff(homeReward)>5000); homeReward(end)];
    
    
     
    
    
    soundRewardAll=soundReward/10000;
    homeRewardAll=homeReward/10000;
    silentRewardAll=silentReward/10000;
    
    pos = loaddatastruct('/data15/gideon/Brg/','Brg','pos',day1);
    numEpochs1=size(pos{day1},2);
    allDayPos=[];
    for mn=1:numEpochs1, allDayPos=[allDayPos; pos{day1}{mn}.data];end
    postimes=allDayPos(:,1);
    vel=allDayPos(:,5);
    
    
    load(['/data15/gideon/Brg/Brgripples' dayStr]);
    allDayRips=[];
    for mn=1:numEpochs1
    [riptimes] = getripples_direct([day1, mn], ripples, 11:14,'minstd',6);
    allDayRips=[allDayRips; riptimes(:,1)];
    end
    
    % psth in 0.1ms resolution
                curPsthSilent=zeros(1,psthTimeWindowInS*1000+1);
                % video is ~30Hz
%                allVelsSilent=zeros(1,psthTimeWindowInS*30);
                for b=1:length(silentRewardAll)
                    psthRipTimes=allDayRips(find(allDayRips>=silentRewardAll(b)-psthTimeWindowInS/2&allDayRips<=silentRewardAll(b)+psthTimeWindowInS/2))-(silentRewardAll(b)-psthTimeWindowInS/2);
                    psthRipInds=round(psthRipTimes*1000)+1;
                    curPsthSilent(b,psthRipInds)=1;
%                     curvel=vel(find(postimes>=silentRewardAll(b)-psthTimeWindowInS/2&postimes<=silentRewardAll(b)+psthTimeWindowInS/2));
%                     allVelsSilent(b,1:length(curvel))=curvel;
                end
    
                
                  curPsthHome=zeros(1,psthTimeWindowInS*1000+1);
                % video is ~30Hz
%                allVelsSilent=zeros(1,psthTimeWindowInS*30);
                for b=1:length(homeRewardAll)
                    psthRipTimes=allDayRips(find(allDayRips>=homeRewardAll(b)-psthTimeWindowInS/2&allDayRips<=homeRewardAll(b)+psthTimeWindowInS/2))-(homeRewardAll(b)-psthTimeWindowInS/2);
                    psthRipInds=round(psthRipTimes*1000)+1;
                    curPsthHome(b,psthRipInds)=1;
%                    
                end
                
                
                
                  curPsthSound=zeros(1,psthTimeWindowInS*1000+1);
                % video is ~30Hz
%                allVelsSilent=zeros(1,psthTimeWindowInS*30);
                for b=1:length(soundRewardAll)
                    psthRipTimes=allDayRips(find(allDayRips>=soundRewardAll(b)-psthTimeWindowInS/2&allDayRips<=soundRewardAll(b)+psthTimeWindowInS/2))-(soundRewardAll(b)-psthTimeWindowInS/2);
                    psthRipInds=round(psthRipTimes*1000)+1;
                    curPsthSound(b,psthRipInds)=1;
%                     curvel=vel(find(postimes>=silentRewardAll(b)-psthTimeWindowInS/2&postimes<=silentRewardAll(b)+psthTimeWindowInS/2));
%                     allVelsSilent(b,1:length(curvel))=curvel;
                end
                
    figure(100)
                subplot(11,3,3*(day1)+1)
                plot(timeAxis,smooth(mean(curPsthHome),100))
                if day1==1
                title('Home')
                end
                subplot(11,3,3*(day1)+2)
                plot(timeAxis,smooth(mean(curPsthSilent),100))
                if day1==1
                title('Silent')
                end
                subplot(11,3,3*(day1)+3)
                plot(timeAxis,smooth(mean(curPsthSound),100))
                if day1==1
                title('Sound')
                end
                if day1==11
                xlabel('Time (S)')
                end
                
               figure
               subplot(1,3,1)
                plot(timeAxis,smooth(mean(curPsthHome),100))
                title(['Day ' num2str(day1) ', Home'])
                subplot(1,3,2)
                plot(timeAxis,smooth(mean(curPsthSilent),100))
                title('Silent')
                subplot(1,3,3)
                plot(timeAxis,smooth(mean(curPsthSound),100))
                title('Sound')
                xlabel('Time (S)')

              %  keyboard
            
        
        
end
% figure;
% subplot(2,1,1)
% imagesc(sequentialTrials)
% title('correct/incorrect trials throughout experiment')
% subplot(2,1,2)
% plot(smooth(sequentialTrials,150),'k','linewidth',2)
% xlabel('# trial')
% ylabel('% correct, smoothed')
% xlim([0 length(sequentialTrials)])
%
%
% figure;
% plot(allRewards,'r','linewidth',2)
% xlabel('Day')
% ylabel('# Trials')
% axis([0 12 0 300])
%
% figure;
% plot(toSoundCorrectRatios,'linewidth',2)
% hold on
% plot(toSilentCorrectRatios,'r','linewidth',2)
% plot((toSilentCorrectRatios+toSoundCorrectRatios)./2,'k','linewidth',2)
% plot(toHomeCorrectRatios,'m','linewidth',2)
% title('Behavior performance over days')
% ylabel('Percent correct')
% xlabel('Day')
% legend('To Sound','To Silent','Sound/Silent average','To home')
% xlim([0 12])
