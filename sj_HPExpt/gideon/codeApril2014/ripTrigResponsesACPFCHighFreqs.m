% this script compared ripple-triggered activity in AC and PFC, in sleep
% and track sessions for high frequencies (0-400Hz). 
% For last days, it seems that in run sessions only, there is
% high-frequency activity before and after ripples
day=8
resps15={};
resps1={};
allPostRipEEG15={};
allPostRipEEG1={};

for sleep=[0]% 1]
    if sleep
        epochs=[3,5,7];
        typeStr=' sleep';
    else
        epochs=[2,4,6];
        typeStr=' run';
    end
    noNoiseTimes = getNOchewnoisetimes('/data15/gideon/Ndl','ndl', [day*ones(7,1) (1:7)'], [1 4 7],'mindur',2,'minthresh',300,'mingapdur',1);
    [postRipEEG15] = sj_HPexpt_eventtrigspecgrams_getripExcludeTimes('Ndl', day, epochs, [15], 'rip',[11:14],1,0,1,'movingwin',[100 10]/1000,'fpass',[0 400],'noNoiseTimes',noNoiseTimes);
    title(['PFC, day ' num2str(day) typeStr])
    [postRipEEG1] = sj_HPexpt_eventtrigspecgrams_getripExcludeTimes('Ndl', day, epochs, [1], 'rip',[11:14],1,0,1,'movingwin',[100 10]/1000,'fpass',[0 400],'noNoiseTimes',noNoiseTimes);
    title(['AC 1 day ' num2str(day) typeStr])
    
    allPostRipEEG15{sleep+1}=postRipEEG15;
    allPostRipEEG1{sleep+1}=postRipEEG1;
    
    %2250 is the middle (ripple onset), because the matrix is from -1.5 to +1.5
    %seconds relative to ripple onset
end