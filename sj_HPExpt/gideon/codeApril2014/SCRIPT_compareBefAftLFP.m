% ROSENTHAL
% Rtl aud tets: 16,17,19,20,21
RTLaudtets=[16,17,19,20,21];
RTLdays=[1,2,4:9]
for RTLday=RTLdays
[stimEEGtimes1 stimInds1 whichStim1]=findStimComplexSounds('/opt/data15/gideon/Rtl/EEG/Rtleeg', ['0' num2str(RTLday)],'1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimes2 stimInds2 whichStim2]=findStimComplexSounds('/opt/data15/gideon/Rtl/EEG/Rtleeg', ['0' num2str(RTLday)],'10','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
if RTLday==6, stimInds2=stimInds2(2:end);end
[whichStim1 aleeg2responses1]=getResponsesLFPComplexSoundsNoImgs('/opt/data15/gideon/Rtl/EEG/Rtleeg',['0' num2str(RTLday)],'1',RTLaudtets, stimEEGtimes1,stimInds1,whichStim1);
[whichStim2 aleeg2responses2]=getResponsesLFPComplexSoundsNoImgs('/opt/data15/gideon/Rtl/EEG/Rtleeg',['0' num2str(RTLday)],'10',RTLaudtets, stimEEGtimes2,stimInds2,whichStim2);
plotBefAft2(RTLaudtets,whichStim1,whichStim2,aleeg2responses1,aleeg2responses2)
[befAmps deltaAmps]=calcBefAft2(RTLaudtets,whichStim1,whichStim2,aleeg2responses1,aleeg2responses2)
keyboard
end
%% NADAL


NDLaudtets=[1:7];
NDLdays=[8:14]
NDLepochs=[1 7;1 6;1 7;1 9;1 9;1 9;1 9]
for NDLday=NDLdays
    if NDLday<10
[stimEEGtimes1 stimInds1 whichStim1]=findStimComplexSounds('/opt/data15/gideon/Ndl/EEG/Ndleeg', ['0' num2str(NDLday)],num2str(NDLepochs(NDLday-7,1)),'22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimes2 stimInds2 whichStim2]=findStimComplexSounds('/opt/data15/gideon/Ndl/EEG/Ndleeg', ['0' num2str(NDLday)],num2str(NDLepochs(NDLday-7,2)),'22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[whichStim1 aleeg2responses1]=getResponsesLFPComplexSoundsNoImgs('/opt/data15/gideon/Ndl/EEG/Ndleeg',['0' num2str(NDLday)],num2str(NDLepochs(NDLday-7,1)),NDLaudtets, stimEEGtimes1,stimInds1,whichStim1);
[whichStim2 aleeg2responses2]=getResponsesLFPComplexSoundsNoImgs('/opt/data15/gideon/Ndl/EEG/Ndleeg',['0' num2str(NDLday)],num2str(NDLepochs(NDLday-7,2)),NDLaudtets, stimEEGtimes2,stimInds2,whichStim2);
    else
        [stimEEGtimes1 stimInds1 whichStim1]=findStimComplexSounds('/opt/data15/gideon/Ndl/EEG/Ndleeg', [num2str(NDLday)],num2str(NDLepochs(NDLday-7,1)),'22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimes2 stimInds2 whichStim2]=findStimComplexSounds('/opt/data15/gideon/Ndl/EEG/Ndleeg', [num2str(NDLday)],num2str(NDLepochs(NDLday-7,2)),'22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[whichStim1 aleeg2responses1]=getResponsesLFPComplexSoundsNoImgs('/opt/data15/gideon/Ndl/EEG/Ndleeg',[num2str(NDLday)],num2str(NDLepochs(NDLday-7,1)),NDLaudtets, stimEEGtimes1,stimInds1,whichStim1);
[whichStim2 aleeg2responses2]=getResponsesLFPComplexSoundsNoImgs('/opt/data15/gideon/Ndl/EEG/Ndleeg',[num2str(NDLday)],num2str(NDLepochs(NDLday-7,2)),NDLaudtets, stimEEGtimes2,stimInds2,whichStim2);

    end
plotBefAft2(NDLaudtets,whichStim1,whichStim2,aleeg2responses1,aleeg2responses2)
[befAmps deltaAmps]=calcBefAft2(NDLaudtets,whichStim1,whichStim2,aleeg2responses1,aleeg2responses2)
keyboard
end

%% BORG
BRGaudtets=[2:5];
BRGdays=[1:5 7 9:10]
BRGepochs=[1 7;1 7;1 7;1 7;1 7;nan nan;1 7;nan nan;  1 7;  1 7];
for BRGday=BRGdays
    if BRGday<10
        try
[stimEEGtimes1 stimInds1 whichStim1]=findStimComplexSounds('/opt/data15/gideon/Brg/EEG/Brgeeg', ['0' num2str(BRGday)],num2str(BRGepochs(BRGday,1)),'22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
        catch
            keyboard
        end
[stimEEGtimes2 stimInds2 whichStim2]=findStimComplexSounds('/opt/data15/gideon/Brg/EEG/Brgeeg', ['0' num2str(BRGday)],num2str(BRGepochs(BRGday,2)),'22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[whichStim1 aleeg2responses1]=getResponsesLFPComplexSoundsNoImgs('/opt/data15/gideon/Brg/EEG/Brgeeg',['0' num2str(BRGday)],num2str(BRGepochs(BRGday,1)),BRGaudtets, stimEEGtimes1,stimInds1,whichStim1);
[whichStim2 aleeg2responses2]=getResponsesLFPComplexSoundsNoImgs('/opt/data15/gideon/Brg/EEG/Brgeeg',['0' num2str(BRGday)],num2str(BRGepochs(BRGday,2)),BRGaudtets, stimEEGtimes2,stimInds2,whichStim2);
    else
        [stimEEGtimes1 stimInds1 whichStim1]=findStimComplexSounds('/opt/data15/gideon/Brg/EEG/Brgeeg', [num2str(BRGday)],num2str(BRGepochs(BRGday,1)),'22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimes2 stimInds2 whichStim2]=findStimComplexSounds('/opt/data15/gideon/Brg/EEG/Brgeeg', [num2str(BRGday)],num2str(BRGepochs(BRGday,2)),'22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[whichStim1 aleeg2responses1]=getResponsesLFPComplexSoundsNoImgs('/opt/data15/gideon/Brg/EEG/Brgeeg',[num2str(BRGday)],num2str(BRGepochs(BRGday,1)),BRGaudtets, stimEEGtimes1,stimInds1,whichStim1);
[whichStim2 aleeg2responses2]=getResponsesLFPComplexSoundsNoImgs('/opt/data15/gideon/Brg/EEG/Brgeeg',[num2str(BRGday)],num2str(BRGepochs(BRGday,2)),BRGaudtets, stimEEGtimes2,stimInds2,whichStim2);

    end
plotBefAft2(BRGaudtets,whichStim1,whichStim2,aleeg2responses1,aleeg2responses2)
[befAmps deltaAmps]=calcBefAft2(BRGaudtets,whichStim1,whichStim2,aleeg2responses1,aleeg2responses2)
end
