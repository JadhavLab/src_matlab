% this script compared ripple-triggered activity in AC and PFC, in sleep
% and track sessions. Change days at the top, and epochs accordingly.

for day=15%[11:14,16:17]
resps15={};
resps1={};
for sleep=[0 1]
    if sleep
        epochs=[3,5,7]+1;
        typeStr='sleep';
else
    epochs=[2,4,6,8]+1;
    typeStr='run';
end
noNoiseTimes = getNOchewnoisetimes('/data15/gideon/Ndl','ndl', [day*ones(10,1) (1:10)'], [1 4 7],'mindur',2,'minthresh',300,'mingapdur',1);
[allPostRipEEG15] = sj_HPexpt_eventtrigspecgrams_getripExcludeTimes('Ndl', day, epochs, [15], 'rip',[11:14],1,0,1,'movingwin',[400 40]/1000,'fpass',[0 100],'noNoiseTimes',noNoiseTimes);
figure(gcf)
title(['PFC, ' typeStr ' day ' num2str(day)])
caxis([-1 1])

saveas(gcf,['/data15/gideon/Ndl/ProcessedData/riptrigspcgrms/PFC' typeStr num2str(day) 'LowFreqs.jpg'])

[allPostRipEEG1] = sj_HPexpt_eventtrigspecgrams_getripExcludeTimes('Ndl', day, epochs, [1], 'rip',[11:14],1,0,1,'movingwin',[400 40]/1000,'fpass',[0 100],'noNoiseTimes',noNoiseTimes);
figure(gcf)
caxis([-1 1])
title(['AC, ' typeStr ' day ' num2str(day)])

saveas(gcf,['/data15/gideon/Ndl/ProcessedData/riptrigspcgrms/AC' typeStr num2str(day) 'LowFreqs.jpg'])

%2250 is the middle (ripple onset), because the matrix is from -1.5 to +1.5
%seconds relative to ripple onset

resps15{sleep+1}=mean((abs(allPostRipEEG15(:,2250:3000)))');
resps1{sleep+1}=mean((abs(allPostRipEEG1(:,2250:3000)))');

%use this to subtract lfp at time of ripple onset
%resps15{sleep+1}=mean((abs(allPostRipEEG15(:,2250:3000)-repmat(allPostRipEEG15(:,2250),1,751)))');
%resps1{sleep+1}=mean((abs(allPostRipEEG1(:,2250:3000)-repmat(allPostRipEEG1(:,2250),1,751)))');




%  pre15=abs(allPostRipEEG15(:,1:2000));
%  pre15=pre15(:);
%  pre15Mean=mean(pre15);
%  pre15STD=std(pre15);
% % 
%  pre1=abs(allPostRipEEG1(:,1:2000));
%  pre1=pre1(:);
%  pre1Mean=mean(pre1);
%  pre1STD=std(pre1);
% % 
%  resps15=(resps15)/pre15STD;
%  resps1=(resps1)/pre1STD;

figure;
subplot(2,1,1)
%size of rip-triggeres activity in PFC against AC
plot(resps1{sleep+1},resps15{sleep+1},'x')
hold on;
plot(0:400,0:400,'r')
xlabel('Rip-trig LFP amp in AC')
ylabel('Rip-trig LFP amp in PFC')

if sleep, title('sleep'),else, title('run');end
subplot(2,1,2)
hist(resps15{sleep+1}./resps1{sleep+1})
xlabel('PFC/AC rip-trig LFP amp ratio')
ylabel('Proportion')
title(['mean ratio= ' num2str(mean(resps15{sleep+1}./resps1{sleep+1}))])

end
saveas(gcf,['/data15/gideon/Ndl/ProcessedData/riptrigspcgrms/ComparingRipSize' typeStr num2str(day) 'LowFreqs.jpg'])
close all
end
% RESULTS SUMMARY:
% 
% Day 8: sleep pfc: smallish peak following ripples
% 	sleep ac: smallish peak following ripples
% 	track pfc: peak then dip, starts before ripples
% 	track ac: peak then dip, starts before ripples
% 
% Day 9: 	sleep pfc: peaks following ripples
% 	sleep ac: weak peak following ripples
% 	track pfc: peak then dip following ripples, seems to start before
% 	track ac: stronger, peak then dip following ripples, peaks before too
% 
% Day 10: sleep pfc: peak following ripples
% 	  sleep ac: peak following ripples
% 	track pfc: big peak following ripples, in first 2/3 epochs it starts before ripples
% 	track ac: big peak following ripples, in first 2/3 epochs it starts before ripples
% 
% Day 11: sleep pfc: small peak before, big after then dip
% 	sleep ac: similar but weaker
% 	track pfc: peak then dip, maybe small shoulder before
% 	track ac: peak then dip, maybe small shoulder before
% 
% Day 12: sleep pfc: big peak, always dip following ripples,  epoch 2 shoulder before
% 	sleep ac: small peak, always dip following ripples,  epoch 2 shoulder before
% 	track pfc: sometimes peak, always dip following ripples
% 	track ac: sometimes peak, always dip following ripples
% 
% day 13: sleep pfc: shoulder before ripple time and then peak
% 	 sleep ac: peak then dip following ripples (nothing before rips)
% 	track pfc: similar as below but smaller dip
% 	track ac: dip following ripples (nothing before rips)
% 
% Day 14: sleep pfc: in 2/3 epochs small peak before, in all big peak after
% 	sleep ac: similar, not identical
% 	track pfc: not consistent
% 	track ac: clearer: sharp peak and then dip after
% 
% Day 15: sleep pfc: big peak after
% 	sleep ac: smaller peak after
% 	track pfc: not clear
% 	track ac: not clear
% 	
% Day 16: sleep pfc: big peak, always dip following ripples
% 	   sleep ac: smaller peak,  dip following ripples
% 	  track pfc: inverted U from -0.5 S to ripple onset, nothing clear after
% 	track ac: weaker, hints of something similar
% 
% Day 17: sleep pfc: big peak, then dip following ripples
% 	sleep ac: small peak, then dip following ripples
% 	track pfc: barely anything
% 	track ac: bigger, though not very big
% 
% Seems like:
% 1.Largest results is that ripples during sleep evoke a stronger response in cortex compared with ripples on track. 
% 2.It seems that for cortex, the mean trace is more informative than the spectrogram in lower frequencies
% 3.Return to the spectorgram for higher frequencies 
% 4.I had a feeling that ripple-triggered activity is larger in AC on track as compared to sleep, whereas in PFC it is the opposite: stronger in sleep as compared to track. Hasn't really seemed so when quantified. Right now it seems that ripples generally induce a larger response in PFC
% 5.Maybe largest responses in middle days?
% 6.Look at precise timing of PFC/AC responses. It seems PFC is earlier.
% 7.