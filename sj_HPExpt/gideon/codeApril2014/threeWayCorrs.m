% CONSOLIDATING PFC AND AC RIPPLE MODULATION, loading file created by  DFSsj_HPexpt_getripalignspikingAC3
savedir = '/data15/gideon/ProcessedData/';

load([savedir 'Ndl_ripples_AC'])
allripplemodAC=allripplemod;
allripplemod_idxAC=allripplemod_idx;

load([savedir 'Ndl_ripples_PFC'])
allripplemodPFC=allripplemod;
allripplemod_idxPFC=allripplemod_idx;


load([savedir 'Ndl_ripples_CA1'])
allripplemodCA1=allripplemod;
allripplemod_idxCA1=allripplemod_idx;

allpsPFCAC=[];
allrsPFCAC=[];
allpsPFCACBef=[];
allrsPFCACBef=[];

allanimsPFCAC=[];
alldaysPFCAC=[];


combined_idx=unique([allripplemod_idxAC(:,1:2)],'rows');
% only Nadal for now
combined_idx=combined_idx(combined_idx(:,1)==1,:)
for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    ACidx=find(ismember(allripplemod_idxAC(:,1:2),curidx,'rows'));
    PFCidx=find(ismember(allripplemod_idxPFC(:,1:2),curidx,'rows'));
    CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
    nCA1cells=length(CA1idx);
    nACcells=length(ACidx);
    nPFCcells=length(PFCidx);
    allCA1amps=[];
    for m=1:size(CA1idx,1)
        CA1ind1=CA1idx(m);
        CA1hist=allripplemodCA1(1,CA1ind1).hist;
        CA1amp=mean(CA1hist(:,50:70),2);
        CA1ampBef=mean(CA1hist(:,1:21),2);
        
        % regular case
        if size(allCA1amps,1)==0 || size(allCA1amps,1)>0 & size(allCA1amps,2)==length(CA1amp)
            allCA1amps=[allCA1amps; CA1amp'];
            % if first cell had too little rips, replace it
        elseif  size(allCA1amps,1)>0 & size(allCA1amps,2)<length(CA1amp)
            allCA1amps=CA1amp';
            % in third case where the new cell has fewer rips, don't add him
        end
    end
    allACamps=[];
    for m=1:size(ACidx,1)
        ACind1=ACidx(m);
        AChist=allripplemodAC(1,ACind1).hist;
            ACamp=mean(AChist(:,50:70),2);
            ACampBef=mean(AChist(:,1:21),2);
          % regular case
        if size(allACamps,1)==0 || size(allACamps,1)>0 & size(allACamps,2)==length(ACamp)
            allACamps=[allACamps; ACamp'];
            % if first cell had too little rips, replace it
        elseif  size(allACamps,1)>0 & size(allACamps,2)<length(ACamp)
            allACamps=ACamp';
            % in third case where the new cell has fewer rips, don't add him
        end
     end
     allPFCamps=[];
     for m=1:size(PFCidx,1)
            PFCind1=PFCidx(m);
            PFChist=allripplemodPFC(1,PFCind1).hist;
            PFCamp=mean(PFChist(:,50:70),2);
            PFCampBef=mean(PFChist(:,1:21),2);
  % regular case
        if size(allPFCamps,1)==0 || size(allPFCamps,1)>0 & size(allPFCamps,2)==length(PFCamp)
            allPFCamps=[allPFCamps; PFCamp'];
            % if first cell had too little rips, replPFCe it
        elseif  size(allPFCamps,1)>0 & size(allPFCamps,2)<length(PFCamp)
            allPFCamps=PFCamp';
            % in third case where the new cell has fewer rips, don't add him
        end
     end
     
%try
allAmps=[allCA1amps;allACamps;allPFCamps];
[r1 p1]=corrcoef(allAmps');

nCells=size(allAmps,1);
% removing within-region corrs
for i=1:nCA1cells,for j=1:nCA1cells,p1(i,j)=10;end;end
for i=nCA1cells+1:nCA1cells+nACcells,for j=nCA1cells+1:nCA1cells+nACcells, p1(i,j)=10;end;end
for i=nCA1cells+nACcells+1:nCells,for j=nCA1cells+nACcells+1:nCells, p1(i,j)=10;end;end


regionsLabels=[repmat('CA1',nCA1cells,1);repmat('AC ',nACcells,1);repmat('PFC',nPFCcells,1)];
plotNiceCorrGraph(r1,p1,regionsLabels)
title(['Animal ' num2str(combined_idx(v,1)) ' Day ' num2str(combined_idx(v,2)) ' numRips= ' num2str(size(allAmps,2))]) 
a=readGraph('adjMatrix',double(p1<0.05));
% comparing number of triangles to random   
b = triu(ones(size(a,1)),1);
vals=a(b==1);
allnumcyclesShuf=[];
for k=1:1000
% shuffling vals within triangular matrix
valsJ=vals(randperm(length(vals)));
aa=zeros(size(a));
aa(b==1)=valsJ;
aShuffled=aa+aa';
numcyclesShuf=cycleCountBacktrack('adjMatrix',aShuffled,3);
allnumcyclesShuf=[allnumcyclesShuf numcyclesShuf];
end
numcyclesReal=cycleCountBacktrack('adjMatrix',a,3)
pval=1-mean(allnumcyclesShuf<=numcyclesReal)
keyboard
% catch
%     'diff lengths'
%     keyboard
% end
end