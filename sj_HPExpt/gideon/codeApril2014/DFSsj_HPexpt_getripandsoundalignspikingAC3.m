

clear; %close all;
runscript = 1;
savedata = 1; % save data option - only works if runscript is also on
savedirX = '/opt/data15/gideon/ProcessedData/';
plotsoundripplemod=0;
%val=1;savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_AC']; area = 'AC';
%val=2;savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_CA1']; area = 'CA1';
%val=3;savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_PFC']; area = 'PFC';
%val=4;savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_ACawake']; area = 'AC';
%val=5;savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_CA1awake']; area = 'CA1';
val=6;savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_PFCawake']; area = 'PFC';

% for val=1:6
%         if val==1, savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_AC']; area = 'AC'; end
%     if val==2, savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_CA1']; area = 'AC'; end
%     if val==3, savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_PFC']; area = 'AC'; end
%
%     if val==4, savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_ACawake']; area = 'AC'; end
%     if val==5, savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_CA1awake']; area = 'CA1'; end
%     if val==6, savefile = [savedirX 'NdlBrg_ripplemodsoundmod4_PFCawake']; area = 'PFC'; end
%   runscript = 1;
% savedata = 1;
% savedirX = '/data15/gideon/ProcessedData/';


%save([savedirX 'area'],'area');
figdir='/home/gideon/ripsoundmod/newslow/';


%val=1; savefile = [savedirX 'HP_ripplemod_PFC_alldata']; area = 'PFC'; clr = 'b'; % PFC
%val=2; savefile = [savedirX 'HP_ripplemod_CA1_alldata']; area = 'CA1';  clr = 'r';% CA1
%val=3;savefile = [savedirX 'HP_ripplemod_PFC_alldata_speed']; area = 'PFC'; clr = 'b'; % PFC - low speed criterion
%val=4;savefile = [savedirX 'HP_ripplemod_PFC_alldata_stdev5']; area = 'PFC'; clr = 'b'; % PFC - low speed criterion
%val=5;savefile = [savedirX 'HP_ripplemod_PFC_alldata_singletrack']; area = 'PFC'; clr = 'b'; % PFC - low speed criterion

%val=7;savefile = [savedirX 'HP_ripplemod_CA1_alldata_speed_minrip2_feb14']; area = 'CA1'; clr = 'b'; % PFC - low speed criterion


savefig1=0;


% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


%If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    %    animals = {'Nadal','Borg'};
    animals = {'Nadal','Borg'};
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    % dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    
    %  runepochfilter = 'isequal($environment, ''ytr'')';
    runepochfilter = 'isequal($type, ''run'')';
    
    sleepepochfilter ='isequal($type, ''sleep'') && isequal($audprot, ''0'')';
    
    % Cell filter
    % -----------
    switch val
        case 1
            cellfilter = 'strcmp($area, ''AC'') && ($numspikes > 100)'; %
        case 2
            cellfilter = 'strcmp($area, ''CA1'') && ($numspikes > 100)'; %
        case 3
            cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; %
        case 4
            cellfilter = 'strcmp($area, ''AC'') && ($numspikes > 100)'; %
        case 5
            cellfilter = 'strcmp($area, ''CA1'') && ($numspikes > 100)'; %
        case 6
            cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; %
            
            
            
    end
    
    % cellfilter = 'strcmp($tag2, ''PFC'')';
    %cellfilter = 'strcmp($tag2, ''CA1Pyr'') && ($numspikes > 100)'; % This includes all.
    % For more exclusive choosing, use $tag. Take care of number of spikes while gathering data
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % The following are ripple time filter options. Instead of using time filters, within the function,
    % call getripples using the riptetfilter option to get the riptimes. Since you want a vector of riptimes.
    % You can also use inter-ripple-interval of 1 sec within the function, etc.
    
    % a) Using getriptimes: generates 1 vector with 1's for ripple times for all tetrodes.
    % Should be similar to getripples/ getripplees_direct
    
    %     timefilterrun_rip = {{'DFTFsj_getvelpos', '(($absvel <= 5))'},...
    %         {'DFTFsj_getriptimes','($nripples > 0)','tetfilter',riptetfilter,'minthresh',3}};
    
    % b) getripples. This is similar to getripltimes, but gives start and end of each ripple time
    % instead of a time filter. Also has an additional condition of at least 50 ms ripple.
    
    % Iterator
    % --------
    iterator = 'singlecellanal';
    
    % Filter creation
    % ----------------
    %     modf = createfilter('animal',animals,'days',dayfilter,'epochs',runepochfilter, 'cells',...
    %         cellfilter, 'iterator', iterator);
    
    
    if val<4 % awake sound, sleep ripples
        modf = createfilter('animal',animals,'epochs',sleepepochfilter, 'cells',...
            cellfilter, 'iterator', iterator);
        
        modg = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
            cellfilter, 'iterator', iterator);
    else % awake sound, awake ripples
        modf = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
            cellfilter, 'iterator', iterator);
        
        modg = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
            cellfilter, 'iterator', iterator);
        
    end
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    
    % switch val
    %  case 1
    
    modg = setfilterfunction(modg,'DFAgr_getsoundalignspiking',{'spikes', 'sound', 'tetinfo', 'pos'},'dospeed',0);%1,'lowsp_thrs',3); % Default stdev is 3
    
    modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'dospeed',1,'lowsp_thrs',4,'minrip',2); % Default stdev is 3
    
    %        case 2
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'}); %
    %        case 3
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'dospeed',1,'lowsp_thrs',5); %
    %        case 4
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'minstd',5); %
    %        case 5
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'}); % Default stdev is 3
    %
    %        case 6
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'dospeed',1,'lowsp_thrs',4,'minrip',2); % Default stdev is 3
    %        case 7
    %         modf = setfilterfunction(modf,'DFAsj_getripalignspiking',{'spikes', 'ripples', 'tetinfo', 'pos'},'dospeed',1,'lowsp_thrs',4,'minrip',2); % Default stdev is 3
    %
    
    %   end
    % Going to call getripples_tetinfo within function
    
    % Run analysis
    % ------------
    modg = runfilter(modg);
    modf = runfilter(modf);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear runscript savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------



% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;

switch val
    case 1
        gatherdatafile = [savedirX 'NdlBrg_ripplemodsoundmod4_AC_gather']; % PFC cells to Hipp ripples
    case 2
        gatherdatafile = [savedirX 'NdlBrg_ripplemodsoundmod4_CA1_gather']; % PFC cells to Hipp ripples
    case 3
        gatherdatafile = [savedirX 'NdlBrg_ripplemodsoundmod4_PFC_gather']; % PFC cells to Hipp ripples
    case 4
        gatherdatafile = [savedirX 'NdlBrg_ripplemodsoundmod4_ACawake_gather']; % PFC cells to Hipp ripples
    case 5
        gatherdatafile = [savedirX 'NdlBrg_ripplemodsoundmod4_CA1awake_gather']; % PFC cells to Hipp ripples
    case 6
        gatherdatafile = [savedirX 'NdlBrg_ripplemodsoundmod4_PFCawake_gather']; % PFC cells to Hipp ripples
        
end




if gatherdata
    
    % Parameters if any
    % -----------------
    
    % -------------------------------------------------------------
    
    
    %  ========= RIPPLES DATA ==========
    cnt=0; % Count how many cells will be kept based on nspikes in output: >0
    allanimindex_rip=[]; alldataraster_rip=[]; alldatahist_rip = []; all_Nspk_rip=[];
    
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
            if (modf(an).output{1}(i).Nspikes > 0)
                cnt=cnt+1;
                anim_index_rip{an}(cnt,:) = modf(an).output{1}(i).index;
                % Only indexes
                animindex_rip=[an modf(an).output{1}(i).index]; % Put animal index in front
                allanimindex_rip = [allanimindex_rip; animindex_rip]; % Collect all Anim Day Epoch Tet Cell Index
                % Data
                alldataraster_rip{cnt} = modf(an).output{1}(i).rip_spks_cell; % Only get raster and histogram response
                alldatahist_rip{cnt} = modf(an).output{1}(i).rip_spkshist_cell;
                all_Nspk_rip(cnt) = modf(an).output{1}(i).Nspikes;
                alldataraster_rdm_rip{cnt} = modf(an).output{1}(i).rdm_spks_cell; % Only get raster and histogram response
                alldatahist_rdm_rip{cnt} = modf(an).output{1}(i).rdm_spkshist_cell;
                % trialResps: Summed Nspks/trial in respective window
                alldatatrialResps_rip{cnt} = modf(an).output{1}(i).trialResps;
                alldatatrialResps_bck_rip{cnt} = modf(an).output{1}(i).trialResps_bck;
                alldatatrialResps_rdm_rip{cnt} = modf(an).output{1}(i).trialResps_rdm;
                % Nspikes summed across trials in response and bckgnd window
                all_Nspk_resp_rip(cnt) = sum(modf(an).output{1}(i).trialResps);
                all_Nspk_bck_rip(cnt) = sum(modf(an).output{1}(i).trialResps_bck);
                % Properties
                allcellfr_rip(cnt) = modf(an).output{1}(i).cellfr;
                
                %end
                if cnt==1
                    pret_rip =  modf(an).output{1}(i).pret;
                    postt_rip = modf(an).output{1}(i).postt;
                    binsize_rip = modf(an).output{1}(i).binsize;
                    rwin_rip = modf(an).output{1}(i).rwin;
                    bckwin_rip = modf(an).output{1}(i).bckwin;
                    bins_resp_rip  = modf(an).output{1}(i).bins_resp;
                    bins_bck_rip = modf(an).output{1}(i).bins_bck;
                    timeaxis_rip = modf(an).output{1}(i).timeaxis;
                end
            end
        end
        
    end
    
    %  ========= SOUND DATA ==========
    
    cnt=0; % Count how many cells will be kept based on nspikes in output: >0
    allanimindex_snd=[]; alldataraster_snd=[]; alldatahist_snd = []; all_Nspk_snd=[];
    
    for an = 1:length(modg)
        for i=1:length(modg(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
            if (modg(an).output{1}(i).Nspikes > 0)
                cnt=cnt+1;
                anim_index_snd{an}(cnt,:) = modg(an).output{1}(i).index;
                % Only indexes
                animindex_snd=[an modg(an).output{1}(i).index]; % Put animal index in front
                allanimindex_snd = [allanimindex_snd; animindex_snd]; % Collect all Anim Day Epoch Tet Cell Index
                % Data
                alldataraster_snd{cnt} = modg(an).output{1}(i).sound_spks_cell; % Only get raster and histogram response
                alldatahist_snd{cnt} = modg(an).output{1}(i).sound_spkshist_cell;
                allspeeds_snd{cnt} = modg(an).output{1}(i).speedAtSound;
%                 allY_snd{cnt} = modg(an).output{1}(i).yPosAtSound;
%                 allX_snd{cnt} = modg(an).output{1}(i).xPosAtSound;
%                 
                all_Nspk_snd(cnt) = modg(an).output{1}(i).Nspikes;
                alldataraster_rdm_snd{cnt} = modg(an).output{1}(i).rdm_spks_cell; % Only get raster and histogram response
                alldatahist_rdm_snd{cnt} = modg(an).output{1}(i).rdm_spkshist_cell;
                % trialResps: Summed Nspks/trial in respective window
                alldatatrialResps_snd{cnt} = modg(an).output{1}(i).trialResps;
                alldatatrialResps_bck_snd{cnt} = modg(an).output{1}(i).trialResps_bck;
                alldatatrialResps_rdm_snd{cnt} = modg(an).output{1}(i).trialResps_rdm;
                % Nspikes summed across trials in response and bckgnd window
                all_Nspk_resp_snd(cnt) = sum(modg(an).output{1}(i).trialResps);
                all_Nspk_bck_snd(cnt) = sum(modg(an).output{1}(i).trialResps_bck);
                % Properties
                allcellfr_snd(cnt) = modg(an).output{1}(i).cellfr;
                allsoundind{cnt}=modg(an).output{1}(i).soundind;
                %end
                if cnt==1
                    pret_snd =  modg(an).output{1}(i).pret;
                    postt_snd = modg(an).output{1}(i).postt;
                    binsize_snd = modg(an).output{1}(i).binsize;
                    rwin_snd = modg(an).output{1}(i).rwin;
                    bckwin_snd = modg(an).output{1}(i).bckwin;
                    bins_resp_snd  = modg(an).output{1}(i).bins_resp;
                    bins_bck_snd = modg(an).output{1}(i).bins_bck;
                    timeaxis_snd = modg(an).output{1}(i).timeaxis;
                end
            end
        end
        
    end
    
    
    % Consolidate single cells across epochs RIPPLES
    % ----------------------------------------------------------------------------
    
    allripplemod = struct;
    dummyindex=allanimindex_rip;  % all anim-day-epoch-tet-cell indices
    cntcellsRip=0;
    for i=1:length(alldatahist_rip)
        animdaytetcell=allanimindex_rip(i,[1 2 4 5]);
        ind=[];
        while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
            dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one
        end
        
        % Gather everything for the current cell across epochs
        currhist=[]; currraster=[]; currNspk=0; currNspk_resp=0; currNspk_bck=0; curr_cellfr=[];
        currhist_rdm=[]; currraster_rdm=[];
        currtrialResps=[]; currtrialResps_rdm=[]; currtrialResps_bck=[];
        for r=ind
            currNspk = currNspk + all_Nspk_rip(r);
            currNspk_resp = currNspk_resp + all_Nspk_resp_rip(r);
            currNspk_bck = currNspk_bck + all_Nspk_bck_rip(r);
            currhist = [currhist; alldatahist_rip{r}];
            currraster = [currraster, alldataraster_rip{r}];
            currhist_rdm = [currhist_rdm; alldatahist_rdm_rip{r}];
            currraster_rdm = [currraster_rdm, alldataraster_rdm_rip{r}];
            currtrialResps = [currtrialResps, alldatatrialResps_rip{r}];
            currtrialResps_rdm = [currtrialResps_rdm, alldatatrialResps_rdm_rip{r}];
            currtrialResps_bck = [currtrialResps_bck, alldatatrialResps_bck_rip{r}];
            curr_cellfr = [curr_cellfr; allcellfr_rip(r)];
        end
        
        % Condition for Nspk. Version 1 had a min of 50 for entire window. Increase it to 100,
        % and can also add a condition for spikes in (resp+bck) window. Need to try a few values
        if (currNspk >= 10)
            %if ((currNspk_resp+currNspk_bck) >= 40)
            %if (currNspk >= 100) && ((currNspk_resp+currNspk_bck) >= 40)
            cntcellsRip = cntcellsRip + 1;
            % For Ndl-GIdeon;s animal, shift days
            %             if animdaytetcell(1)==4
            %                 animdaytetcell(2)=animdaytetcell(2)-7; % Day starts from no. 8
            %             end
            allripplemod_idx(cntcellsRip,:)=animdaytetcell;
            allripplemod(cntcellsRip).index=animdaytetcell;
            allripplemod(cntcellsRip).hist=currhist*(1000/binsize_rip); % Convert to firing rate in Hz
            allripplemod(cntcellsRip).raster=currraster;
            allripplemod(cntcellsRip).Nspk=currNspk;
            allripplemod(cntcellsRip).hist_rdm=currhist_rdm*(1000/binsize_rip); % Convert to firing rate in Hz
            allripplemod(cntcellsRip).raster_rdm=currraster_rdm;
            % Trial Resps
            allripplemod(cntcellsRip).trialResps = currtrialResps';
            allripplemod(cntcellsRip).trialResps_rdm = currtrialResps_rdm';
            allripplemod(cntcellsRip).trialResps_bck = currtrialResps_bck';
            % Properties
            allripplemod(cntcellsRip).cellfr = nanmean(curr_cellfr);
        end
    end
    
    %     Consolidate single cells across epochs SOUNDS
    %     ----------------------------------------------------------------------------
    
    allsoundmod = struct;
    dummyindex=allanimindex_snd;  % all anim-day-epoch-tet-cell indices
    cntcellsSnd=0;
    for i=1:length(alldatahist_snd)
        animdaytetcell=allanimindex_snd(i,[1 2 4 5]);
        ind=[];
        while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
            dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one
        end
        
        % Gather everything for the current cell across epochs
        currhist=[]; currraster=[]; currNspk=0; currNspk_resp=0; currNspk_bck=0; curr_cellfr=[];
        currhist_rdm=[]; currraster_rdm=[];
        currtrialResps=[]; currtrialResps_rdm=[]; currtrialResps_bck=[];currspeeds=[];currsoundind=[];currXs=[];currYs=[];
        for r=ind
            currNspk = currNspk + all_Nspk_snd(r);
            currNspk_resp = currNspk_resp + all_Nspk_resp_snd(r);
            currNspk_bck = currNspk_bck + all_Nspk_bck_snd(r);
            currhist = [currhist; alldatahist_snd{r}];
            currraster = [currraster, alldataraster_snd{r}];
            currhist_rdm = [currhist_rdm; alldatahist_rdm_snd{r}];
            currraster_rdm = [currraster_rdm, alldataraster_rdm_snd{r}];
            currtrialResps = [currtrialResps, alldatatrialResps_snd{r}];
            currtrialResps_rdm = [currtrialResps_rdm, alldatatrialResps_rdm_snd{r}];
            currtrialResps_bck = [currtrialResps_bck, alldatatrialResps_bck_snd{r}];
            curr_cellfr = [curr_cellfr; allcellfr_snd(r)];
            currspeeds = [currspeeds; allspeeds_snd{r}];
%             currYs=[currYs;allY_snd{r}];
%             currXs=[currXs;allX_snd{r}];
            
            currsoundind = [currsoundind allsoundind{r}];
        end
        
        % Condition for Nspk. Version 1 had a min of 50 for entire window. Increase it to 100,
        % and can also add a condition for spikes in (resp+bck) window. Need to try a few values
        if (currNspk >= 10)
            %if ((currNspk_resp+currNspk_bck) >= 40)
            %if (currNspk >= 100) && ((currNspk_resp+currNspk_bck) >= 40)
            cntcellsSnd = cntcellsSnd + 1;
            % For Ndl-GIdeon;s animal, shift days
            %             if animdaytetcell(1)==4
            %                 animdaytetcell(2)=animdaytetcell(2)-7; % Day starts from no. 8
            %             end
            allsoundmod_idx(cntcellsSnd,:)=animdaytetcell;
            allsoundmod(cntcellsSnd).index=animdaytetcell;
            allsoundmod(cntcellsSnd).hist=currhist*(1000/binsize_snd); % Convert to firing rate in Hz
            allsoundmod(cntcellsSnd).raster=currraster;
            allsoundmod(cntcellsSnd).Nspk=currNspk;
            allsoundmod(cntcellsSnd).hist_rdm=currhist_rdm*(1000/binsize_snd); % Convert to firing rate in Hz
            allsoundmod(cntcellsSnd).raster_rdm=currraster_rdm;
            % Trial Resps
            allsoundmod(cntcellsSnd).trialResps = currtrialResps';
            allsoundmod(cntcellsSnd).trialResps_rdm = currtrialResps_rdm';
            allsoundmod(cntcellsSnd).trialResps_bck = currtrialResps_bck';
            % Properties
            allsoundmod(cntcellsSnd).cellfr = nanmean(curr_cellfr);
            allsoundmod(cntcellsSnd).speeds = currspeeds;
            allsoundmod(cntcellsSnd).posY = currYs;
            allsoundmod(cntcellsSnd).posX = currXs;
            
            allsoundmod(cntcellsSnd).soundind = currsoundind;
            
        end
    end
    b=fir1(32,0.1);
    % determining ripple-modulation
    varRange=[250:750];
    preRange=50:250;
    for ii=1:cntcellsRip
        ii
        curRast=allripplemod(ii).raster;
        numRips=length(curRast);
        % creating a raster matrix of 0/1 with 1ms resolution
        % it goes from -550 t0 +550ms relative to ripples
        curRastMat=zeros(numRips,1100);
        for i=1:numRips,curRastMat(i,round(curRast{i}+551))=1;end
        
        respVarShufs=[];
        numRuns=1000;
        allShufMeans=[];
        % shuffling each trial in the raster separately in time,
        % cyclically. Doing that numRuns times (currently 1000).
        for runs=1:numRuns
            asv=[];
            curRastMatShuf=zeros(numRips,1100);
            for i=1:numRips
                shiftVal=round(rand(1)*1100);
                asv=[asv shiftVal];
                curRastMatShuf(i,1+mod(round(curRast{i}+551+shiftVal),1100))=1;
            end
            
            % shuffled "psth"
            meanRespShuf=smooth(mean(curRastMatShuf(:,50:end-50)),50);
            allShufMeans=[allShufMeans; meanRespShuf'];
            
            % for each shuffled psth, calculate the variance (mean squared
            % distance from mean)
            respVarShufs=[respVarShufs var(meanRespShuf(varRange))];
            
        end
        
        meanResp=smooth(mean(curRastMat(:,50:1050)),50);
        meanRespRange=meanResp(varRange);
        meanRespShuf=(mean(allShufMeans));
        meanRespShufRange= meanRespShuf(varRange);
        
        %new measure: instead of mean squared distance from its own mean,
        %mean squared distance from the mean shuffled psth's
        respVar2=mean((meanRespRange-meanRespShufRange').^2);
        % this is used to get a positive deviation from mean
        varPSTH=(meanResp-meanRespShuf').^2;
        absPSTH=abs(meanResp-meanRespShuf');
        regPSTH=(meanResp-meanRespShuf');
        regPSTHfilt=filtfilt(b,1,regPSTH);
        regPSTH=regPSTHfilt;
        
        rasterShufP2=1-sum(respVar2>respVarShufs)/numRuns;
        varRespAmp2=respVar2/mean(respVarShufs);
        allripplemod(ii).rasterShufP2=rasterShufP2;
        allripplemod(ii).varRespAmp2=varRespAmp2;
        allripplemod(ii).varPSTH=varPSTH;
        % identifying first deviation from random psth
        % tested different ways:
        % crossedThresh=find(varPSTH(101:end)>0.000002,1)+100;
        % better ? way:
        %       maxPSTHind=find(meanResp(varRange)==max(meanResp(varRange)),1)+varRange(1);
        %crossedThresh=100+find(varPSTH(101:maxPSTHind)<0.00001,1,'last');
        %       crossedThresh1=varRange(1)+find(regPSTH(varRange)>(mean(regPSTH(varRange))+0.5*std(regPSTH(varRange))),1,'first')
        %       crossedThresh2=varRange(1)+find(regPSTH(varRange)>(mean(regPSTH(preRange))+2*std(regPSTH(preRange))),1,'first')
        %
        %        crossedThresh3=varRange(1)+find(regPSTH(varRange(1):maxPSTHind)<(mean(regPSTH(varRange))+0.5*std(regPSTH(varRange))),1,'last')
        
        meanRespFluct=meanResp-mean(meanResp(preRange));
        % requiring the first peak that is larger than 80% of the absolute
        % peak. This is to deal with double-peaked cases where the second is
        % slightly taller, so now it should take the first
        maxPSTHind=find(meanResp(varRange)>=0.9*max(meanResp(varRange)),1)+varRange(1);
        minPSTHind=find(meanResp(varRange)==min(meanResp(varRange)),1)+varRange(1);
        % if the biggest extrema is a maximum
        if abs(meanRespFluct(maxPSTHind))>abs(meanRespFluct(minPSTHind))
            %             crossedThresh1=varRange(1)+find(regPSTH(varRange)>(mean(regPSTH(preRange))+1*std(regPSTH(preRange))),1,'first');
            %             crossedThresh2=varRange(1)+find(regPSTH(varRange)>(mean(regPSTH(preRange))+2*std(regPSTH(preRange))),1,'first');
            crossedThresh3=varRange(1)+find(regPSTH(varRange(1):maxPSTHind)<(mean(regPSTH(preRange))+3*std(regPSTH(preRange))),1,'last');
            
            % crossedThresh4=varRange(1)+find(regPSTH(varRange(1):maxPSTHind)<(mean(regPSTH(preRange))+2*std(regPSTH(preRange))),1,'last')
            % crossedThresh5=varRange(1)+find(regPSTH(varRange(1):maxPSTHind)<(mean(regPSTH(preRange))+1*std(regPSTH(preRange))),1,'last')
            
            exinh='RIP-EXC';
            exinh1=1;
        else
            %                     crossedThresh1=varRange(1)+find(regPSTH(varRange)<(mean(regPSTH(preRange))-1*std(regPSTH(preRange))),1,'first');
            %                     crossedThresh2=varRange(1)+find(regPSTH(varRange)<(mean(regPSTH(preRange))-2*std(regPSTH(preRange))),1,'first');
            crossedThresh3=varRange(1)+find(regPSTH(varRange(1):maxPSTHind)>(mean(regPSTH(preRange))-3*std(regPSTH(preRange))),1,'last');
            
            % crossedThresh4=varRange(1)+find(regPSTH(varRange(1):minPSTHind)>(mean(regPSTH(preRange))-2*std(regPSTH(preRange))),1,'last')
            % crossedThresh5=varRange(1)+find(regPSTH(varRange(1):maxPSTHind)>(mean(regPSTH(preRange))-1*std(regPSTH(preRange))),1,'last')
            
            
            exinh='RIP-INH';
            exinh1=0;
        end
        % crossedThresh5=varRange(1)+find(regPSTH(varRange(1):maxPSTHind)<(mean(regPSTH(preRange))+4*std(regPSTH(preRange))),1,'last')
        
        if rasterShufP2<0.05
            %         allripplemod(ii).ripmodonset1=crossedThresh1;
            %         allripplemod(ii).ripmodonset2=crossedThresh2;
            allripplemod(ii).ripmodonset3=crossedThresh3;
            %         allripplemod(ii).ripmodonset4=crossedThresh4;
            %         allripplemod(ii).ripmodonset5=crossedThresh5;
            allripplemod(ii).ripmodtype=exinh1;
            
            
            %         figure;plot(regPSTH);
            %
            %         if ~isempty(crossedThresh1)
            %             hold on;plot([crossedThresh1 crossedThresh1],[0 max(regPSTH)],'k')
            %         end
            %         if ~isempty(crossedThresh2)
            %             hold on;plot([crossedThresh2 crossedThresh2],[0 max(regPSTH)],'m')
            %         end
            %         if ~isempty(crossedThresh3)
            %             hold on;plot([crossedThresh3 crossedThresh3],[0 max(regPSTH)],'b')
            %         end
            %         if ~isempty(crossedThresh4)
            %             hold on;plot([crossedThresh4 crossedThresh4],[0 max(regPSTH)],'g')
            %         end
            %         if ~isempty(crossedThresh5)
            %             hold on;plot([crossedThresh5 crossedThresh5],[0 max(regPSTH)],'r')
            %         end
            %
            %
            %
            %         title([exinh ', ripmodP=' num2str(rasterShufP2) ' thresh1 ' num2str(crossedThresh1) ' thresh2 ' num2str(crossedThresh2) ' thresh3 ' num2str(crossedThresh3) ' thresh4 ' num2str(crossedThresh4) ' thresh5 ' num2str(crossedThresh5)])
            % %         saveas(gcf,['/data15/gideon/ripmodthFigs/PFCripmodthresh' num2str(ii) '.jpg'])
            %         keyboard
        else
            %         allripplemod(ii).ripmodonset1=nan;
            %         allripplemod(ii).ripmodonset2=nan;
            allripplemod(ii).ripmodonset3=nan;
            %         allripplemod(ii).ripmodonset4=nan;
            %         allripplemod(ii).ripmodonset5=nan;
            
            allripplemod(ii).ripmodtype=nan;
            
        end
        
        
        
    end
    
    
    % determining sound-modulation
    varRange=[500:950];
    for ii=1:cntcellsSnd
        ii
        curRast=allsoundmod(ii).raster;
        numRips=length(curRast);
        % creating a raster matrix of 0/1 with 1ms resolution
        % it goes from -550 t0 +550ms relative to ripples
        curRastMat=zeros(numRips,1100);
        for i=1:numRips,curRastMat(i,round(curRast{i}+551))=1;end
        
        respVarShufs=[];
        numRuns=1000;
        allShufMeans=[];
        % shuffling each trial in the raster separately in time,
        % cyclically. Doing that numRuns times (currently 1000).
        for runs=1:numRuns
            
            curRastMatShuf=zeros(numRips,1100);
            for i=1:numRips
                shiftVal=round(rand(1)*1000);
                curRastMatShuf(i,1+mod(round(curRast{i}+551+shiftVal),1100))=1;
            end
            
            % shuffled "psth"
            meanRespShuf=smooth(mean(curRastMatShuf(:,50:end-50)),50);
            allShufMeans=[allShufMeans; meanRespShuf'];
            
            % for each shuffled psth, calculate the variance (mean squared
            % distance from mean)
            respVarShufs=[respVarShufs var(meanRespShuf(varRange))];
            
        end
        
        meanResp=smooth(mean(curRastMat(:,50:1050)),50);
        meanRespRange=meanResp(varRange);
        meanRespShuf=(mean(allShufMeans));
        meanRespShufRange= meanRespShuf(varRange);
        
        %new measure: instead of mean squared distance from its own mean,
        %mean squared distance from the mean shuffled psth's
        respVar2=mean((meanRespRange-meanRespShufRange').^2);
        % this is used to get a positive deviation from mean
        varPSTH=(meanResp-meanRespShuf').^2;
        rasterShufP2=1-sum(respVar2>respVarShufs)/numRuns;
        varRespAmp2=respVar2/mean(respVarShufs);
        allsoundmod(ii).rasterShufP2=rasterShufP2;
        allsoundmod(ii).varRespAmp2=varRespAmp2;
        allsoundmod(ii).varPSTH=varPSTH;
        
    end
    
    % combining ripple and sound modulation for visualization, continue more
    % analysis in the section below
    
    %     scrsz = get(0,'ScreenSize');
    %     plotsoundripplemod=1;
    %             if plotsoundripplemod
    %
    %     shufpvals=[];
    %     combined_idx=unique([allripplemod_idx;allsoundmod_idx],'rows');
    %     for v=1:size(combined_idx,1)
    %         curidx=combined_idx(v,:);
    %         ridx=find(ismember(allripplemod_idx,curidx,'rows'));
    %         sidx=find(ismember(allsoundmod_idx,curidx,'rows'));
    %
    %
    %         %       if ~isempty(sidx) & ~isempty(ridx)
    %         % sort sound responses by speed
    %
    %
    %         figure('Position',[scrsz(3)/2 scrsz(4)/4 scrsz(3)/3 scrsz(4)/1.5])
    %         if ~isempty(ridx)
    %
    %             subplot(2,2,1)
    %             imagesc(allripplemod(1,ridx).hist);
    %             title(['RIPPLEMOD, IDX= ' num2str(allripplemod(1,ridx).index)])
    %             subplot(2,2,3:4)
    %             hold on;
    %             plot((mean(allripplemod(1,ridx).hist)-mean(mean(allripplemod(1,ridx).hist)))/std(mean(allripplemod(1,ridx).hist)))
    %
    %         end
    %         if ~isempty(sidx)
    %             [sortedSpeeds sortedSpeedsInd]=sort(allsoundmod(sidx).speeds);
    %             subplot(2,2,2)
    %             imagesc(allsoundmod(1,sidx).hist(sortedSpeedsInd,:));
    %             % imagesc(allsoundmod(1,sidx).hist);
    %             title(['SOUNDMOD, IDX= ' num2str(allsoundmod(1,sidx).index)])
    %
    %
    %
    %             subplot(2,2,3:4)
    %
    %             hold on;plot((mean(allsoundmod(1,sidx).hist)-mean(mean(allsoundmod(1,sidx).hist)))/std(mean(allsoundmod(1,sidx).hist)),'r')
    %             legend('ripple','sound')
    %
    %
    %         end
    %
    %                 m=1;
    %     end
    %     end
    
    
    
    
    
    
    
    
    
    
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data
close all
%end

% analysis of ripple-triggered firing onset in CA1,AC and PFC
if 0
    ripTrigFiringOnsetAnalysis3
end

%---------------------------------------------------------
if 0
% plotting for individual cells
scrsz = get(0,'ScreenSize');
allripplemodsleep=allripplemod;
for v=1:length(allripplemodsleep)
    %plotting only sig
%    if allripplemodsleep(v).rasterShufP2<0.05
    curind=allripplemodsleep(v).index;
    curanim=curind(1);
    curday=curind(2);
    curtet=curind(3);
    curcell=curind(4);
    

         rstS=allripplemodsleep(v).raster;
         x1=linspace(-500,500,101);

        figure('Position',[900 200 scrsz(3)/10 scrsz(4)/1.5])
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 10])
        subplot(2,1,1)
        plotRaster1(rstS,[0 200],500,500)
        
        title(['Anim:' num2str(curanim) ' Day:' num2str(curday) ' Tet:' num2str(curtet) ' Cell:' num2str(curcell)])
        subplot(2,1,2)
        
        plot(x1,mean(allripplemodsleep(1,v).hist),'linewidth',5)
        hold on
        plot(x1,mean(allripplemodsleep(1,v).hist)+std(allripplemodsleep(1,v).hist)/sqrt(size(allripplemodsleep(1,v).hist,1)),'b--','linewidth',2);
        plot(x1,mean(allripplemodsleep(1,v).hist)-std(allripplemodsleep(1,v).hist)/sqrt(size(allripplemodsleep(1,v).hist,1)),'b--','linewidth',2);
        cury=ylim;
        ylim([0 cury(2)]);
        ypts=0:0.1:cury(2);
        xpts = 0*ones(size(ypts));
        plot(xpts , ypts, 'r--','Linewidth',2);
         saveas(gcf,['/opt/data15/gideon/Figs/RipTrigRasters2/' area 'ripmodsleep' num2str(curanim) '-' num2str(curday) '-' num2str(curtet) '-' num2str(curcell) '.jpg'])
         saveas(gcf,['/opt/data15/gideon/Figs/RipTrigRasters2/' area 'ripmodsleep' num2str(curanim) '-' num2str(curday) '-' num2str(curtet) '-' num2str(curcell)  '.pdf'])
        saveas(gcf,['/opt/data15/gideon/Figs/RipTrigRasters2/' area 'ripmodsleep' num2str(curanim) '-' num2str(curday) '-' num2str(curtet) '-' num2str(curcell)  '.fig'])
        %subplot(2,1,1)
      %  ylim([200 400])
       % saveas(gcf,['/opt/data15/gideon/Figs/RipTrigRasters2/' area 'ripmodsleep' num2str(curanim) '-' num2str(curday) '-' num2str(curtet) '-' num2str(curcell) 'SG.jpg'])
        %saveas(gcf,['/opt/data15/gideon/Figs/RipTrigRasters2/' area 'ripmodsleep' num2str(curanim) '-' num2str(curday) '-' num2str(curtet) '-' num2str(curcell)  'SG.pdf'])
        
        close all
    end
end
    %keyboard
    

%---------------------------------------------------------


% MAKE ALL THE FOLLOWING CODE IN A SEPARATE SCRIPT, WHICH LOADS ALL THE 6
% DATAFILES AND DOES IT ON ALL, LIKE THE SCRIPT ABOVE

soundModVec=[];
for ii=1:cntcellsSnd,soundModVec=[soundModVec allsoundmod(ii).rasterShufP2];end
mean(soundModVec<0.05)
ripModVec=[];
for ii=1:cntcellsRip,ripModVec=[ripModVec allripplemod(ii).rasterShufP2];end
mean(ripModVec<0.05)

ripAmpVec=[];
for ii=1:cntcellsRip,ripAmpVec=[ripAmpVec allripplemod(ii).varRespAmp2];end

animdayvecRip=[];
for ii=1:cntcellsRip,animdayvecRip=[animdayvecRip; allripplemod(ii).index(1:2)];end
animdayvecRip(animdayvecRip(:,1)==1,2)=animdayvecRip(animdayvecRip(:,1)==1,2)-7;
animdayvecSnd=[];
for ii=1:cntcellsSnd,animdayvecSnd=[animdayvecSnd; allsoundmod(ii).index(1:2)];end
animdayvecSnd(animdayvecSnd(:,1)==1,2)=animdayvecSnd(animdayvecSnd(:,1)==1,2)-7;



% rip rate across days, combining both animals
ripratedaysanim=[];
for days1=1:11, ripratedaysanim=[ripratedaysanim mean(ripModVec(animdayvecRip(:,2)==days1)<0.05)];end
figure;plot(ripratedaysanim,'k','linewidth',3)
ylim([0 1])
xlabel('day');ylabel('fraction of rip-modulated cells');title('fraction of rip-modulated cells')
% separating animals:
ripratedaysanim1=[];
for days1=1:10, ripratedaysanim1=[ripratedaysanim1 mean(ripModVec(animdayvecRip(:,1)==1&animdayvecRip(:,2)==days1)<0.05)];end
hold on;
plot(ripratedaysanim1)
ripratedaysanim2=[];
for days1=1:11, ripratedaysanim2=[ripratedaysanim2 mean(ripModVec(animdayvecRip(:,1)==2&animdayvecRip(:,2)==days1)<0.05)];end
plot(ripratedaysanim2,'r')
legend('both animals','anim1','anim2')

%mean rip amp across days, combining both animals
ripampdaysanim=[];
for days1=1:10, ripampdaysanim=[ripampdaysanim mean(ripAmpVec(animdayvecRip(:,2)==days1))];end
figure;
plot(ripampdaysanim,'k','linewidth',3)
xlabel('day');ylabel('rip-modulation amplitude');
ripampdaysanim1=[];
for days1=1:10, ripampdaysanim1=[ripampdaysanim1 mean(ripAmpVec(animdayvecRip(:,1)==1&animdayvecRip(:,2)==days1))];end
hold on;plot(ripampdaysanim1)
ripampdaysanim2=[];
for days1=1:11, ripampdaysanim2=[ripampdaysanim2 mean(ripAmpVec(animdayvecRip(:,1)==2&animdayvecRip(:,2)==days1))];end
plot(ripampdaysanim2,'r')
legend('both animals','anim1','anim2')

% Snd rate across days, combining both animals
sndratedaysanim=[];
for days1=1:11, sndratedaysanim=[sndratedaysanim mean(soundModVec(animdayvecSnd(:,2)==days1)<0.05)];end
figure;plot(sndratedaysanim,'k','linewidth',3)
xlabel('day');ylabel('fraction of sound-modulated cells');
ylim([0 1])
% separating animals:
sndratedaysanim1=[];
for days1=1:11, sndratedaysanim1=[sndratedaysanim1 mean(soundModVec(animdayvecSnd(:,1)==1&animdayvecSnd(:,2)==days1)<0.05)];end
hold on;plot(sndratedaysanim1)
ylim([0 1])
sndratedaysanim2=[];
for days1=1:11, sndratedaysanim2=[sndratedaysanim2 mean(soundModVec(animdayvecSnd(:,1)==2&animdayvecSnd(:,2)==days1)<0.05)];end
plot(sndratedaysanim2,'r')
ylim([0 1])

legend('both animals','anim1','anim2')


load([savedirX 'area']);
switch area
    
    case 'AC'
        % compare sleep and awake ripple-evoked activity
        load([savedirX 'NdlBrg_ripplemodsoundmod4_ACawake_gather']);
        allripplemodW=allripplemod;
        allripplemod_idxW=allripplemod_idx;
        load([savedirX 'NdlBrg_ripplemodsoundmod4_AC_gather']);
        
    case 'CA1'
        % compare sleep and awake ripple-evoked activity
        load([savedirX 'NdlBrg_ripplemodsoundmod4_CA1awake_gather']);
        allripplemodW=allripplemod;
        allripplemod_idxW=allripplemod_idx;
        load([savedirX 'NdlBrg_ripplemodsoundmod4_CA1_gather']);
        
    case 'PFC'
        % compare sleep and awake ripple-evoked activity
        load([savedirX 'NdlBrg_ripplemodsoundmod4_PFCawake_gather']);
        allripplemodW=allripplemod;
        allripplemod_idxW=allripplemod_idx;
        load([savedirX 'NdlBrg_ripplemodsoundmod4_PFC_gather']);
        
end
allinds=unique([allripplemod_idxW;allripplemod_idx],'rows');


%  compare awake vs. sleep rip-trig firing. Done more population-style in
%  compareAwakeVsSleep3
if 0
    scrsz = get(0,'ScreenSize');
    for v=1:size(allinds,1)
        curidx=allinds(v,:);%animal day
        ripidxW=find(ismember(allripplemod_idxW,curidx,'rows'));
        ripidx=find(ismember(allripplemod_idx,curidx,'rows'));
        figure('Position',[300 scrsz(4)/2 scrsz(3)/3 scrsz(4)/2])
        subplot(2,2,1)
        if ~isempty(ripidxW)
            imagesc(allripplemodW(1,ripidxW).hist)
            title('awake ripples')
            %        if allsoundmod(1,sndidx).rasterShufP2<0.05
            %        title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index) ' score=' num2str(allsoundmod(1,sndidx).varRespAmp2) '*'])
            %        else
            %       title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index) ' score=' num2str(allsoundmod(1,sndidx).varRespAmp2)])
            %
            %        end
        end
        if ~isempty(ripidx)
            subplot(2,2,2)
            imagesc(allripplemod(1,ripidx).hist)
            if allripplemod(1,ripidx).rasterShufP2<0.05
                title(['sleep ripples, ind= ' num2str(allripplemod(1,ripidx).index) ' score=' num2str(allripplemod(1,ripidx).varRespAmp2) '*'])
            else
                title(['sleep ripples, ind= ' num2str(allripplemod(1,ripidx).index) ' score=' num2str(allripplemod(1,ripidx).varRespAmp2)])
                
            end
        end
        if ~isempty(ripidx)
            subplot(2,2,3:4)
            meanriptrace=mean(allripplemod(1,ripidx).hist);
            meanriptraceZ=(meanriptrace-mean(meanriptrace))/std(meanriptrace);
            plot(meanriptraceZ);
            hold on;
        end
        if ~isempty(ripidxW)
            subplot(2,2,3:4)
            
            meansndtrace=mean(allripplemodW(1,ripidxW).hist);
            meansndtraceZ=(meansndtrace-mean(meansndtrace))/std(meansndtrace);
            
            plot(meansndtraceZ,'r');
            legend('sleep','awake')
            
            %  cursoundind=allsoundmod(1,sndidx).soundind;
            % [sortedSounds sortedSoundind]=sort(cursoundind);
            % figure('Position',[900 scrsz(4)/2 scrsz(3)/3 scrsz(4)/1.2])
            % subplot(2,1,1);imagesc(1:100,sortedSounds,allsoundmod(1,sndidx).hist(sortedSoundind,:));title('sorted by sound ind')
            % [sortedSpeeds sortedSpeedsInd]=sort(allsoundmod(sndidx).speeds);
            % subplot(2,1,2);imagesc(1:100,sortedSpeeds,allsoundmod(1,sndidx).hist(sortedSpeedsInd,:));title('sorted by speed')
            
        end
        
        keyboard
    end
    
end
% compare awake-sound and sleep-ripple evoked activity
% now done in rippleVsSoundResponseAnalysis3

if 0
    sndresp=[];
    ripresp=[];
    sndrespamp=[];
    riprespamp=[];
    
    allinds=unique([allsoundmod_idx;allripplemod_idx],'rows');
    
    for v=1:size(allinds,1)
        curidx=allinds(v,:);%animal day
        sndidx=find(ismember(allsoundmod_idx,curidx,'rows'));
        ripidx=find(ismember(allripplemod_idx,curidx,'rows'));
        if ~isempty(sndidx)
            try
                sndresp=[sndresp  allsoundmod(1,sndidx).rasterShufP2<0.05];
                sndrespamp=[sndrespamp  allsoundmod(1,sndidx).varRespAmp2];
            catch
                keyboard
            end
        else
            sndresp=[sndresp  NaN];
            sndrespamp=[sndrespamp NaN];
            
        end
        if ~isempty(ripidx)
            ripresp=[ripresp  allripplemod(1,ripidx).rasterShufP2<0.05];
            riprespamp=[riprespamp  allripplemod(1,ripidx).varRespAmp2];
            
        else
            ripresp=[ripresp  NaN];
            riprespamp=[riprespamp NaN];
            
        end
        
    end
    X=[riprespamp;sndrespamp];
    goodvals=find(sum(isnan(X))==0);
    [r p]=corrcoef(riprespamp(goodvals),sndrespamp(goodvals))
    
    scrsz = get(0,'ScreenSize');
    for v=1:size(allinds,1)
        curidx=allinds(v,:);%animal day
        sndidx=find(ismember(allsoundmod_idx,curidx,'rows'));
        ripidx=find(ismember(allripplemod_idx,curidx,'rows'));
        figure('Position',[300 scrsz(4)/2 scrsz(3)/3 scrsz(4)/1.2])
        subplot(2,2,1)
        if ~isempty(sndidx)
            imagesc(allsoundmod(1,sndidx).hist)
            if allsoundmod(1,sndidx).rasterShufP2<0.05
                title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index) ' score=' num2str(allsoundmod(1,sndidx).varRespAmp2) '*'])
            else
                title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index) ' score=' num2str(allsoundmod(1,sndidx).varRespAmp2)])
                
            end
        end
        if ~isempty(ripidx)
            subplot(2,2,2)
            imagesc(allripplemod(1,ripidx).hist)
            if allripplemod(1,ripidx).rasterShufP2<0.05
                title(['Ripmod, ind= ' num2str(allripplemod(1,ripidx).index) ' score=' num2str(allripplemod(1,ripidx).varRespAmp2) '*'])
            else
                title(['Ripmod, ind= ' num2str(allripplemod(1,ripidx).index) ' score=' num2str(allripplemod(1,ripidx).varRespAmp2)])
                
            end
        end
        if ~isempty(ripidx)
            subplot(2,2,3:4)
            meanriptrace=mean(allripplemod(1,ripidx).hist);
            meanriptraceZ=(meanriptrace-mean(meanriptrace))/std(meanriptrace);
            plot(meanriptraceZ);
            hold on;
        end
        if ~isempty(sndidx)
            subplot(2,2,3:4)
            
            meansndtrace=mean(allsoundmod(1,sndidx).hist);
            meansndtraceZ=(meansndtrace-mean(meansndtrace))/std(meansndtrace);
            
            plot(meansndtraceZ,'r');
            legend('rip','snd')
            
            cursoundind=allsoundmod(1,sndidx).soundind;
            [sortedSounds sortedSoundind]=sort(cursoundind);
            figure('Position',[900 scrsz(4)/2 scrsz(3)/3 scrsz(4)/1.2])
            subplot(2,1,1);imagesc(1:100,sortedSounds,allsoundmod(1,sndidx).hist(sortedSoundind,:));title('sorted by sound ind')
            [sortedSpeeds sortedSpeedsInd]=sort(allsoundmod(sndidx).speeds);
            subplot(2,1,2);imagesc(1:100,sortedSpeeds,allsoundmod(1,sndidx).hist(sortedSpeedsInd,:));title('sorted by speed')
            
        end
        
        keyboard
    end
    
    nanmean(sndresp(ripresp==1))
    nanmean(sndresp(ripresp==0))
    nanmean(ripresp(sndresp==1))
    nanmean(ripresp(sndresp==0))
end
% CONSOLIDATING CA1 AND AC RIPPLE MODULATION, loading file created by  DFSsj_HPexpt_getripalignspikingAC3
load([savedirX 'Ndl_ripples_AC'])
allripplemodAC=allripplemod;
allripplemod_idxAC=allripplemod_idx;

load([savedirX 'Ndl_ripples_CA1'])
allripplemodCA1=allripplemod;
allripplemod_idxCA1=allripplemod_idx;
allpsCA1AC=[];
allrsCA1AC=[];
allpsCA1ACFarBef=[];
allrsCA1ACFarBef=[];
allpsCA1ACBef2=[];
allrsCA1ACBef2=[];
allrsCA1ACAll=[];
allpsCA1ACAll=[];
allrsACBef=[];
allpsACBef=[];

allpsShufCA1AC=[];
allrsShufCA1AC=[];
allrsCA1ACFarBefShuf=[];
allpsCA1ACFarBefShuf=[];

allanimsCA1AC=[];
alldaysCA1AC=[];
allCA1ACindscorrs=[];
combined_idx=unique([allripplemod_idxAC(:,1:2)],'rows');
for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    ACidx=find(ismember(allripplemod_idxAC(:,1:2),curidx,'rows'));
    CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
    for j=1:size(ACidx,1)
        for k=1:size(CA1idx,1)
            ACind1=ACidx(j);
            CA1ind1=CA1idx(k);
            %AChist=allripplemodAC(1,ACind1).hist;
            AChist=rast2mat(allripplemodAC(1,ACind1).raster);
            %CA1hist=allripplemodCA1(1,CA1ind1).hist;
            CA1hist=rast2mat(allripplemodCA1(1,CA1ind1).raster);
            
            ACamp=mean(AChist(:,501:700),2);
            CA1amp=mean(CA1hist(:,501:700),2);
            ACampBef=mean(AChist(:,1:200),2);
            CA1ampBef=mean(CA1hist(:,1:200),2);
            % this is immediately before ripple
            ACampBef2=mean(AChist(:,301:500),2);
            CA1ampBef2=mean(AChist(:,301:500),2);
            
            if length(ACamp)==length(CA1amp)
                [r p]=corrcoef(ACamp,CA1amp);
                r=r(2,1);p=p(2,1);
                
                if  r<-0.18 % r>0.3 |
                    % example: finding a high-corr AC-CA1 pair, sorting by
                    % AC activity during SWRs
                    h2 = ones(5,8);
                    AChistint=mean(AChist(:,500:700),2);
                    [amp1 ampind1]=sort((AChistint));
                    figure;subplot(1,2,1)
                    imagesc(1-(filter2(h2,AChist(ampind1,:)))>0);colormap(gray)
                    subplot(1,2,2)
                    imagesc(1-(filter2(h2,CA1hist(ampind1,:)))>0);colormap(gray)
                    keyboard
                    
                end
                
                %  title([ num2str(allripplemod_idxCA1(CA1ind1,:)) 'r= ' num2str(r) ' p= ' num2str(p)]);
                [rshuf pshuf]=corrcoef(ACamp(randperm(length(ACamp))),CA1amp(randperm(length(ACamp))));
                rshuf=rshuf(2,1);pshuf=pshuf(2,1);

                
                [rFarBef pFarBef]=corrcoef(ACampBef,CA1ampBef);
                rFarBef=rFarBef(2,1);pFarBef=pFarBef(2,1);
                
                [rFarBefShuf pFarBefShuf]=corrcoef(ACampBef(randperm(length(ACampBef))),CA1ampBef(randperm(length(ACampBef))));
                rFarBefShuf=rFarBefShuf(2,1);pFarBefShuf=pFarBefShuf(2,1);

                
                [rACBef pACBef]=corrcoef(ACampBef2,CA1amp);
                rACBef=rACBef(2,1);pACBef=pACBef(2,1);
                
                [rCA1Bef pCA1Bef]=corrcoef(ACamp,CA1ampBef2);
                rCA1Bef=rCA1Bef(2,1);pCA1Bef=pCA1Bef(2,1);
                
                [rAll pAll]=corrcoef(ACamp+ACampBef2,CA1amp);
                rAll=rAll(2,1);pAll=pAll(2,1);
                
                allpsCA1AC=[allpsCA1AC p];
                allrsCA1AC=[allrsCA1AC r];
                allpsShufCA1AC=[allpsShufCA1AC pshuf];
                allrsShufCA1AC=[allrsShufCA1AC rshuf];
                
                allpsCA1ACFarBef=[allpsCA1ACFarBef pFarBef];
                allrsCA1ACFarBef=[allrsCA1ACFarBef rFarBef];
                allpsCA1ACFarBefShuf=[allpsCA1ACFarBefShuf pFarBefShuf];
                allrsCA1ACFarBefShuf=[allrsCA1ACFarBefShuf rFarBefShuf];
                
                
                
                allpsACBef=[allpsACBef pACBef];
                allrsACBef=[allrsACBef rACBef];
                allpsCA1ACAll=[allpsCA1ACAll pAll];
                allrsCA1ACAll=[allrsCA1ACAll rAll];
                
                allanimsCA1AC=[allanimsCA1AC allripplemod_idxCA1(CA1ind1,1)];
                alldaysCA1AC=[alldaysCA1AC allripplemod_idxCA1(CA1ind1,2)];
                % creating a large matrix with the form:
                % anim-day-tet-cell-tet-cell-r-p-rBef-pBef
                allCA1ACindscorrs=[allCA1ACindscorrs; allripplemodAC(1,ACind1).index allripplemodCA1(1,CA1ind1).index(3:4) r p rFarBef pFarBef];
                
            else
                
            end
            %keyboard
        end
    end
    
    
    
end

% NOTE, NOW IN allCA1ACindscorrs THERE ARE ALL INDICES OF PAIRS AND THEIR
% RIP-CORR VALUES. RELATE THIS TO THEIR PLACE FIELDS AND SOUND
% RESPONSES!!!

% adjusting days for Nadal
alldaysCA1AC(allanimsCA1AC==1)=alldaysCA1AC(allanimsCA1AC==1)-7;

rateSigPairs=[];numPairs=[];
for day1=1:11,rateSigPairs=[rateSigPairs mean(allpsCA1AC(alldaysCA1AC==day1&allanimsCA1AC==1)<0.05)];numPairs=[numPairs length(allpsCA1AC(alldaysCA1AC==day1&allanimsCA1AC==1))];end
figure;plot(rateSigPairs)
xlabel('day');ylabel('fraction of sig rip-mod pairs');title('fraction of significantly rip-mod CA1-AC pairs, Nadal')
% significantly more pairs during ripples compared to before
ztestprop2([sum(allpsCA1AC<0.05) length(allpsCA1AC)],[sum(allpsCA1ACFarBef<0.05) length(allpsCA1ACFarBef)])
figure;errorbar(1:2,[mean(allpsCA1AC<0.05),mean(allpsCA1ACFarBef<0.05)],[std(allpsCA1AC<0.05)/sqrt(length(allpsCA1AC)),std(allpsCA1ACFarBef<0.05)/sqrt(length(allpsCA1ACFarBef))])
ylim([0 0.25])
title('Fraction of significant ripple-correlated CA1-AC pairs')
% Correlations are stronger during ripples, both positive and negative-
% nice!
figure;
subplot(1,2,1)
plot(allrsCA1AC(allpsCA1AC<0.05),allrsCA1ACFarBef(allpsCA1AC<0.05),'ko')
hold on
plot(-0.5:0.01:1,-0.5:0.01:1,'r')
[h p]=ttest(abs(allrsCA1AC(allpsCA1AC<0.05)),abs(allrsCA1ACFarBef(allpsCA1AC<0.05)))
xlabel('rip-corr during rips')
ylabel('rip-corr before rips')
subplot(1,2,2)
ratioDurPreCor=(abs(allrsCA1AC(allpsCA1AC<0.05))./abs(allrsCA1ACFarBef(allpsCA1AC<0.05)));
ratioDurPreCor=ratioDurPreCor(ratioDurPreCor<100);
hist(log(ratioDurPreCor))
xlabel('log(ratio Dur/Pre Cor)')
ylabel('count')

figure;
stdCA1AC=[std(allpsCA1AC<0.05)/sqrt(length(allpsCA1AC)),std(allpsShufCA1AC<0.05)/sqrt(length(allpsShufCA1AC)),std(allpsCA1ACFarBef<0.05)/sqrt(length(allpsCA1ACFarBef)),std(allpsCA1ACFarBefShuf<0.05)/sqrt(length(allpsCA1ACFarBefShuf))];
meanCA1AC=[mean(allpsCA1AC<0.05),mean(allpsShufCA1AC<0.05),mean(allpsCA1ACFarBef<0.05),mean(allpsCA1ACFarBefShuf<0.05)];
barwitherr(stdCA1AC,meanCA1AC)
keyboard

% =====================================================================================
% Predicting CA1 SWR activity from preceding AC and other way now in code predurACCA1GLM
% ======================================================================================

%%



% CONSOLIDATING AC AND AC RIPPLE MODULATION, loading file created by  DFSsj_HPexpt_getripalignspikingAC3
load([savedirX 'Ndl_ripples_AC'])
allripplemodAC=allripplemod;
allripplemod_idxAC=allripplemod_idx;
allripplemodAC2=allripplemod;
allripplemod_idxAC2=allripplemod_idx;


allpsAC2AC=[];
allrsAC2AC=[];
allpsAC2ACBef=[];
allrsAC2ACBef=[];

allanimsAC2AC=[];
alldaysAC2AC=[];
allAC2ACindscorrs=[];
combined_idx=unique([allripplemod_idxAC(:,1:2)],'rows');
for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    ACidx=find(ismember(allripplemod_idxAC(:,1:2),curidx,'rows'));
    AC2idx=find(ismember(allripplemod_idxAC2(:,1:2),curidx,'rows'));
    for j=1:size(ACidx,1)
        for k=(j+1):size(AC2idx,1)
            ACind1=ACidx(j);
            AC2ind1=AC2idx(k);
            AChist=allripplemodAC(1,ACind1).hist;
            AC2hist=allripplemodAC2(1,AC2ind1).hist;
            
            ACamp=mean(AChist(:,50:70),2);
            AC2amp=mean(AC2hist(:,50:70),2);
            ACampBef=mean(AChist(:,1:21),2);
            AC2ampBef=mean(AC2hist(:,1:21),2);
            if length(ACamp)==length(AC2amp)
                [r p]=corrcoef(ACamp,AC2amp)
                r=r(2,1);p=p(2,1);
                %  title([ num2str(allripplemod_idxAC2(AC2ind1,:)) 'r= ' num2str(r) ' p= ' num2str(p)]);
                
                [rBef pBef]=corrcoef(ACampBef,AC2ampBef);
                rBef=rBef(2,1);pBef=pBef(2,1);
                
                allpsAC2AC=[allpsAC2AC p];
                allrsAC2AC=[allrsAC2AC r];
                allpsAC2ACBef=[allpsAC2ACBef pBef];
                allrsAC2ACBef=[allrsAC2ACBef rBef];
                
                allanimsAC2AC=[allanimsAC2AC allripplemod_idxAC2(AC2ind1,1)];
                alldaysAC2AC=[alldaysAC2AC allripplemod_idxAC2(AC2ind1,2)];
                allAC2ACindscorrs=[allAC2ACindscorrs; allripplemodAC(1,ACind1).index allripplemodAC2(1,AC2ind1).index(3:4) r p rBef pBef];
                
            else
                %     'different lengths'
                %     keyboard
            end
            %keyboard
        end
    end
    
    
    
end

% adjusting days for Nadal
alldaysAC2AC(allanimsAC2AC==1)=alldaysAC2AC(allanimsAC2AC==1)-7;

rateSigPairs=[];numPairs=[];
for day1=1:11,rateSigPairs=[rateSigPairs mean(allpsAC2AC(alldaysAC2AC==day1&allanimsAC2AC==1)<0.05)];numPairs=[numPairs length(allpsAC2AC(alldaysAC2AC==day1&allanimsAC2AC==1))];end
figure;plot(rateSigPairs)

% significantly more pairs during/BEFORE ripples  ????
% LOOK CAREFULLY AT THIS
ztestprop2([sum(allpsAC2AC<0.05) length(allpsAC2AC)],[sum(allpsAC2ACBef<0.05) length(allpsCA1ACFarBef)])

% Correlations are stronger during ripples, both positive and negative-
% nice!
figure;
subplot(1,2,1)
plot(allrsAC2AC(allpsAC2AC<0.05),allrsAC2ACBef(allpsAC2AC<0.05),'ko')
hold on
plot(-0.5:0.01:1,-0.5:0.01:1,'r')
[h p]=ttest(abs(allrsAC2AC(allpsAC2AC<0.05)),abs(allrsAC2ACBef(allpsAC2AC<0.05)))
xlabel('rip-corr during rips')
ylabel('rip-corr before rips')
subplot(1,2,2)

ratioDurPreCor=(abs(allrsAC2AC(allpsAC2AC<0.05))./abs(allrsAC2ACBef(allpsAC2AC<0.05)));
ratioDurPreCor=ratioDurPreCor(ratioDurPreCor<100);
hist(log(ratioDurPreCor))
xlabel('log(ratio Dur/Pre Cor)')
ylabel('count')

% CONSOLIDATING PFC AND AC RIPPLE MODULATION, loading file created by  DFSsj_HPexpt_getripalignspikingAC3
load([savedirX 'Ndl_ripples_AC'])
allripplemodAC=allripplemod;
allripplemod_idxAC=allripplemod_idx;

load([savedirX 'Ndl_ripples_PFC'])
allripplemodPFC=allripplemod;
allripplemod_idxPFC=allripplemod_idx;
allpsPFCAC=[];
allrsPFCAC=[];
allpsPFCACBef=[];
allrsPFCACBef=[];

allanimsPFCAC=[];
alldaysPFCAC=[];


combined_idx=unique([allripplemod_idxAC(:,1:2)],'rows');
for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    ACidx=find(ismember(allripplemod_idxAC(:,1:2),curidx,'rows'));
    PFCidx=find(ismember(allripplemod_idxPFC(:,1:2),curidx,'rows'));
    for j=1:size(ACidx,1)
        for k=1:size(PFCidx,1)
            ACind1=ACidx(j);
            PFCind1=PFCidx(k);
            AChist=allripplemodAC(1,ACind1).hist;
            PFChist=allripplemodPFC(1,PFCind1).hist;
            
            ACamp=mean(AChist(:,50:70),2);
            PFCamp=mean(PFChist(:,50:70),2);
            ACampBef=mean(AChist(:,1:21),2);
            PFCampBef=mean(PFChist(:,1:21),2);
            if length(ACamp)==length(PFCamp)
                [r p]=corrcoef(ACamp,PFCamp);
                r=r(2,1);p=p(2,1);
                %    title([ num2str(allripplemod_idxPFC(PFCind1,:)) 'r= ' num2str(r) ' p= ' num2str(p)]);
                
                [rBef pBef]=corrcoef(ACampBef,PFCampBef);
                rBef=rBef(2,1);pBef=pBef(2,1);
                
                allpsPFCAC=[allpsPFCAC p];
                allrsPFCAC=[allrsPFCAC r];
                allpsPFCACBef=[allpsPFCACBef pBef];
                allrsPFCACBef=[allrsPFCACBef rBef];
                
                allanimsPFCAC=[allanimsPFCAC allripplemod_idxPFC(PFCind1,1)];
                alldaysPFCAC=[alldaysPFCAC allripplemod_idxPFC(PFCind1,2)];
                
                
            else
                %     'different lengths'
                %     keyboard
            end
            %        keyboard
        end
    end
    
    
end


% adjusting days for Nadal
alldaysPFCAC(allanimsPFCAC==1)=alldaysPFCAC(allanimsPFCAC==1)-7;

% Correlations are stronger during ripples, both positive and negative-
% nice!
figure;
subplot(1,2,1)
plot(allrsPFCAC(allpsPFCAC<0.05),allrsPFCACBef(allpsPFCAC<0.05),'ko')
hold on
plot(-0.5:0.01:1,-0.5:0.01:1,'r')
[h p]=ttest(abs(allrsPFCAC(allpsPFCAC<0.05)),abs(allrsPFCACBef(allpsPFCAC<0.05)))
xlabel('rip-corr during rips')
ylabel('rip-corr before rips')
subplot(1,2,2)
ratioDurPreCor=(abs(allrsPFCAC(allpsPFCAC<0.05))./abs(allrsPFCACBef(allpsPFCAC<0.05)));
ratioDurPreCor=ratioDurPreCor(ratioDurPreCor<100);
hist(log(ratioDurPreCor))
xlabel('log(ratio Dur/Pre Cor)')
ylabel('count')

figure;
plot(alldaysPFCAC(allpsPFCAC<0.05&allanimsPFCAC==1),allrsPFCAC(allpsPFCAC<0.05&allanimsPFCAC==1),'ko')

% CONSOLIDATING CA1 and PFC RIPPLE MODULATION, loading file created by  DFSsj_HPexpt_getripalignspikingAC3
load([savedirX 'Ndl_ripples_CA1'])
allripplemodCA1=allripplemod;
allripplemod_idxCA1=allripplemod_idx;

load([savedirX 'Ndl_ripples_PFC'])
allripplemodPFC=allripplemod;
allripplemod_idxPFC=allripplemod_idx;
allpsPFCCA1=[];
allrsPFCCA1=[];
allpsPFCCA1Bef=[];
allrsPFCCA1Bef=[];

allanimsPFCCA1=[];
alldaysPFCCA1=[];


combined_idx=unique([allripplemod_idxCA1(:,1:2)],'rows');
for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
    PFCidx=find(ismember(allripplemod_idxPFC(:,1:2),curidx,'rows'));
    for j=1:size(CA1idx,1)
        for k=1:size(PFCidx,1)
            CA1ind1=CA1idx(j);
            PFCind1=PFCidx(k);
            CA1hist=allripplemodCA1(1,CA1ind1).hist;
            PFChist=allripplemodPFC(1,PFCind1).hist;
            
            CA1amp=mean(CA1hist(:,50:70),2);
            PFCamp=mean(PFChist(:,50:70),2);
            CA1ampBef=mean(CA1hist(:,1:21),2);
            PFCampBef=mean(PFChist(:,1:21),2);
            if length(CA1amp)==length(PFCamp)
                [r p]=corrcoef(CA1amp,PFCamp);
                r=r(2,1);p=p(2,1);
                %    title([ num2str(allripplemod_idxPFC(PFCind1,:)) 'r= ' num2str(r) ' p= ' num2str(p)]);
                
                [rBef pBef]=corrcoef(CA1ampBef,PFCampBef);
                rBef=rBef(2,1);pBef=pBef(2,1);
                
                allpsPFCCA1=[allpsPFCCA1 p];
                allrsPFCCA1=[allrsPFCCA1 r];
                allpsPFCCA1Bef=[allpsPFCCA1Bef pBef];
                allrsPFCCA1Bef=[allrsPFCCA1Bef rBef];
                
                allanimsPFCCA1=[allanimsPFCCA1 allripplemod_idxPFC(PFCind1,1)];
                alldaysPFCCA1=[alldaysPFCCA1 allripplemod_idxPFC(PFCind1,2)];
                
                
            else
                %     'different lengths'
                %     keyboard
            end
            %        keyboard
        end
    end
    
    
end


% adjusting days for Nadal
alldaysPFCCA1(allanimsPFCCA1==1)=alldaysPFCCA1(allanimsPFCCA1==1)-7;

% Correlations are stronger during ripples, both positive and negative-
% nice!
figure;plot(allrsPFCCA1(allpsPFCCA1<0.05),allrsPFCCA1Bef(allpsPFCCA1<0.05),'ko')
hold on
plot(-0.5:0.01:1,-0.5:0.01:1,'r')
[h p]=ttest(abs(allrsPFCCA1(allpsPFCCA1<0.05)),abs(allrsPFCCA1Bef(allpsPFCCA1<0.05)))
xlabel('rip-corr during rips')
ylabel('rip-corr before rips')


%=========== NEW
%threeWayCorrs



