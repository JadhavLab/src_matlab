% is run from DFSsj_HPexpt_getripandsoundalignspikingAC3

savedirX = '/opt/data15/gideon/ProcessedData/';
plotIndividualCells=0;
areas1={'CA1','AC','PFC'}
histOns=[];
histOnsW=[];
allxcorrLags={};
 allallripmodhists={};
    allallripmodhistsW={};
    allallripmodhistssig={};
    allallripmodhistsWsig={};
    allanimdayvecRip={};
 allanimdayvecRipW={};
    for areaInd=1:3
    area1=areas1{areaInd};
    switch area1
        case 'AC'
            % compare sleep and awake ripple-evoked activity
            load([savedirX 'NdlBrg_ripplemodsoundmod2_ACawake_gather']);
            allripplemodW=allripplemod;
            allripplemod_idxW=[];
            for w=1:length(allripplemodW),allripplemod_idxW=[allripplemod_idxW;allripplemodW(w).index];end
            load([savedirX 'NdlBrg_ripplemodsoundmod2_AC_gather']);
            allripplemod_idx=[];
            for w=1:length(allripplemod),allripplemod_idx=[allripplemod_idx;allripplemod(w).index];end

        case 'CA1'
            % compare sleep and awake ripple-evoked activity
            load([savedirX 'NdlBrg_ripplemodsoundmod2_CA1awake_gather']);
            allripplemodW=allripplemod;
            allripplemod_idxW=[];
            for w=1:length(allripplemodW),allripplemod_idxW=[allripplemod_idxW;allripplemodW(w).index];end
            load([savedirX 'NdlBrg_ripplemodsoundmod2_CA1_gather']);
            allripplemod_idx=[];
            for w=1:length(allripplemod),allripplemod_idx=[allripplemod_idx;allripplemod(w).index];end

        case 'PFC'
            % compare sleep and awake ripple-evoked activity
            load([savedirX 'NdlBrg_ripplemodsoundmod2_PFCawake_gather']);
            allripplemodW=allripplemod;
            allripplemod_idxW=[];
            for w=1:length(allripplemodW),allripplemod_idxW=[allripplemod_idxW;allripplemodW(w).index];end
            load([savedirX 'NdlBrg_ripplemodsoundmod2_PFC_gather']);
            allripplemod_idx=[];
            for w=1:length(allripplemod),allripplemod_idx=[allripplemod_idx;allripplemod(w).index];end

    end
    allinds=unique([allripplemod_idxW;allripplemod_idx],'rows');
    allripmodhists=[];
    allripmodhistssig=[];
    allripmodonset3=[];
    for i=1:length(allripplemod)
        if ~isempty(allripplemod(i).ripmodonset3)
            allripmodonset3=[allripmodonset3 allripplemod(i).ripmodonset3];
            allripmodhists=[allripmodhists; zscore(mean(allripplemod(i).hist))];
            if allripplemod(i).rasterShufP2<0.05
           allripmodhistssig=[allripmodhistssig; zscore(mean(allripplemod(i).hist))];
                
            end
        else
            allripmodonset3=[allripmodonset3 nan];
      allripmodhists=[allripmodhists; nan(1,size(allripmodhists,2))];
       
        end;
    end
    
    allripmodhistsW=[];
    allripmodhistsWsig=[];
    allripmodonset3W=[];
    for i=1:length(allripplemodW)
        if ~isempty(allripplemodW(i).ripmodonset3)
            allripmodonset3W=[allripmodonset3W allripplemodW(i).ripmodonset3];
            allripmodhistsW=[allripmodhistsW; zscore(mean(allripplemodW(i).hist))];
if allripplemodW(i).rasterShufP2<0.05
           allripmodhistsWsig=[allripmodhistsWsig; zscore(mean(allripplemodW(i).hist))];
                
            end
        else
            allripmodonset3W=[allripmodonset3W nan];
           allripmodhistsW=[allripmodhistsW; nan(1,size(allripmodhistsW,2))];

        end;
    end
    
    allallripmodhists{end+1}=allripmodhists;
    allallripmodhistsW{end+1}=allripmodhistsW;
    
    allallripmodhistssig{end+1}=allripmodhistssig;
    allallripmodhistsWsig{end+1}=allripmodhistsWsig;
    
    % centering around ripple time
    allripmodonset3W=allripmodonset3W-500;
    allripmodonset3=allripmodonset3-500;
    
    % figure;
    % plot(allripmodonset3,'ko');ylim([0 800]);
    % hold on;
    % plot(allripmodonset3W,'ro');ylim([0 800]);
    % legend({'sleep','awake'})
    % title(area)
    
    
    cntcellsRipW=length(allripplemodW);
    cntcellsRip=length(allripplemod);
    
    animdayvecRip=[];
    for ii=1:cntcellsRip,animdayvecRip=[animdayvecRip; allripplemod(ii).index(1:2)];end
    animdayvecRip(animdayvecRip(:,1)==1,2)=animdayvecRip(animdayvecRip(:,1)==1,2)-7;
   
    animdayvecRipW=[];
    for ii=1:cntcellsRipW,animdayvecRipW=[animdayvecRipW; allripplemodW(ii).index(1:2)];end
    animdayvecRipW(animdayvecRipW(:,1)==1,2)=animdayvecRipW(animdayvecRipW(:,1)==1,2)-7;
    
    
    keep1=find(~isnan(allripmodonset3));
    keep1W=find(~isnan(allripmodonset3W));
    allripmodonset3=allripmodonset3(keep1);
    allripmodonset3W=allripmodonset3W(keep1W);
    animdayvecRip=animdayvecRip(keep1,:);
    animdayvecRipW=animdayvecRipW(keep1W,:);
    
    % didn't have CA1 cells for Borg
    if strcmp(area1,'CA1')
        keep2=find(animdayvecRip(:,1)==1);
        keep2W=find(animdayvecRipW(:,1)==1);
        allripmodonset3=allripmodonset3(keep2);
        allripmodonset3W=allripmodonset3W(keep2W);
        animdayvecRip=animdayvecRip(keep2,:);
        animdayvecRipW=animdayvecRipW(keep2W,:);
    end
    
    
    allanimdayvecRip{end+1}=animdayvecRip;
    allanimdayvecRipW{end+1}=animdayvecRipW;
    
    histOns=[ histOns;hist(allripmodonset3,-500:50:500)/sum(hist(allripmodonset3,-500:50:500))];
    histOnsW= [histOnsW;hist(allripmodonset3W,-500:50:500)/sum(hist(allripmodonset3W,-500:50:500))];
    
    
    scrsz = get(0,'ScreenSize');
    figure('Position',[300 200 scrsz(3)/4 scrsz(4)/1.5])
    subplot(2,1,1)
    hist(allripmodonset3,-500:50:500)
    title([area1 ' sleep'])
    subplot(2,1,2)
    hist(allripmodonset3W,-500:50:500)
    title([area1 ' awake'])
    
    
    figure;
    plot(animdayvecRip(animdayvecRip(:,1)==1,2),allripmodonset3(animdayvecRip(:,1)==1),'ko')
    hold on;plot(animdayvecRip(animdayvecRip(:,1)==2,2),allripmodonset3(animdayvecRip(:,1)==2),'ro')
    legend({'anim1','anim2'})
    ylim([-500 500]);
    xlabel('days')
    ylabel('onset')
    title([area1 ' sleep'])
    
    figure;
    plot(animdayvecRipW(animdayvecRipW(:,1)==1,2),allripmodonset3W(animdayvecRipW(:,1)==1),'ko')
    hold on;plot(animdayvecRipW(animdayvecRipW(:,1)==2,2),allripmodonset3W(animdayvecRipW(:,1)==2),'ro')
    legend({'anim1','anim2'})
    ylim([-500 500]);
    xlabel('days')
    ylabel('onset')
    title([area1 ' awake'])
    cm=colormap('Jet')
    figure;
    c1=1;
    threshs=[-90:-10:-140]
    for thresh=threshs
        fracPre=[];
        
        for k=1:11, fracPre=[fracPre nanmean(allripmodonset3(animdayvecRip(:,2)==k)<thresh)];end
        plot(fracPre,'color',cm(c1,:));
        c1=c1+10;
        hold on;
    end
    ylim([0 0.7])
    xlabel('days')
    ylabel('Fraction of pre-ripple onset')
    legend({'-90','-100','-110','-120','-130','-140'},'location','northwest')
    title([area1 ', fraction of pre-ripple onset, different pre-ripple thresholds'])
    
    
    
    % compare awake and sleep- result not plotted but is in allxcorrLags,
    % and shows no difference
    
    xcorrLags=[];
    
        scrsz = get(0,'ScreenSize');
        for v=1:size(allinds,1)
            curidx=allinds(v,:);%animal day
            ripidxW=find(ismember(allripplemod_idxW,curidx,'rows'));
            ripidx=find(ismember(allripplemod_idx,curidx,'rows'));
             if ~isempty(ripidx)
                meansleeptrace=mean(allripplemod(1,ripidx).hist);
                meansleeptraceZ=(meansleeptrace-mean(meansleeptrace))/std(meansleeptrace);
             end
             if ~isempty(ripidxW)
                meanwaketrace=mean(allripplemodW(1,ripidxW).hist);
                meanwaketraceZ=(meanwaketrace-mean(meanwaketrace))/std(meanwaketrace);
             end
             % currently only taking the xcorr of psths when cell is 
             % significantly modulated in both sleep and awake
            if ~isempty(ripidx)&~isempty(ripidxW)&allripplemod(1,ripidx).rasterShufP2<0.05&allripplemodW(1,ripidxW).rasterShufP2<0.05

             xcorr1=(xcorr(meansleeptrace,meanwaketrace));
             lagInMs=find(xcorr1==max(xcorr1))-length(meansleeptrace)
            xcorrLags=[xcorrLags lagInMs];
            end


            if plotIndividualCells

            figure('Position',[300 200 scrsz(3)/1.3 200])
            subplot(2,2,1)
            if ~isempty(ripidxW)
                imagesc(allripplemodW(1,ripidxW).hist)
                title('awake ripples')
                %        if allsoundmod(1,sndidx).rasterShufP2<0.05
                %        title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index) ' score=' num2str(allsoundmod(1,sndidx).varRespAmp2) '*'])
                %        else
                %       title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index) ' score=' num2str(allsoundmod(1,sndidx).varRespAmp2)])
                %
                %        end
            end
            if ~isempty(ripidx)
                subplot(2,2,2)
                imagesc(allripplemod(1,ripidx).hist)
                if allripplemod(1,ripidx).rasterShufP2<0.05
                    title(['sleep ripples, ind= ' num2str(allripplemod(1,ripidx).index) ' score=' num2str(allripplemod(1,ripidx).varRespAmp2) '*'])
                else
                    title(['sleep ripples, ind= ' num2str(allripplemod(1,ripidx).index) ' score=' num2str(allripplemod(1,ripidx).varRespAmp2)])
                    
                end
            end
            if ~isempty(ripidx)
                subplot(2,2,4)
                plot(meansleeptraceZ);
                hold on;
                plot([allripplemod(ripidx).ripmodonset3/10 allripplemod(ripidx).ripmodonset3/10],[0 max(meansleeptraceZ)],'k');
                
            end
            if ~isempty(ripidxW)
                subplot(2,2,3)
                
                meanwaketrace=mean(allripplemodW(1,ripidxW).hist);
                meanwaketraceZ=(meanwaketrace-mean(meanwaketrace))/std(meanwaketrace);
                
                plot(meanwaketraceZ,'r');
                hold on;
                
               plot([allripplemodW(ripidxW).ripmodonset3/10 allripplemodW(ripidxW).ripmodonset3/10],[0 max(meanwaketraceZ)],'k');
               
                legend('sleep','sleep','awake','awake')
                
                %  cursoundind=allsoundmod(1,sndidx).soundind;
                % [sortedSounds sortedSoundind]=sort(cursoundind);
                % figure('Position',[900 200 scrsz(3)/3 scrsz(4)/1.2])
                % subplot(2,1,1);imagesc(1:100,sortedSounds,allsoundmod(1,sndidx).hist(sortedSoundind,:));title('sorted by sound ind')
                % [sortedSpeeds sortedSpeedsInd]=sort(allsoundmod(sndidx).speeds);
                % subplot(2,1,2);imagesc(1:100,sortedSpeeds,allsoundmod(1,sndidx).hist(sortedSpeedsInd,:));title('sorted by speed')
                
            end
            
            keyboard
        end
        end
    allxcorrLags{end+1}=xcorrLags;
   % keyboard
    end
    
    %% RATE OF SWR-MODULATED CELLS
CA1sleepSWRmodrate=size(allallripmodhistssig{1},1)./size(allallripmodhists{1},1)
ACsleepSWRmodrate=size(allallripmodhistssig{2},1)./size(allallripmodhists{2},1)
PFCsleepSWRmodrate=size(allallripmodhistssig{3},1)./size(allallripmodhists{3},1)
    
CA1wakeSWRmodrate=size(allallripmodhistsWsig{1},1)./size(allallripmodhistsW{1},1)
ACwakeSWRmodrate=size(allallripmodhistsWsig{2},1)./size(allallripmodhistsW{2},1)
PFCwakeSWRmodrate=size(allallripmodhistsWsig{3},1)./size(allallripmodhistsW{3},1)

%% sorting by day
xaxis=-500:10:500;

[sortedDays1 sortedDaysS1]=sort(allanimdayvecRip{1}(:,2),'descend');
figure;
imagesc(xaxis,sortedDays1(end:-1:1),allallripmodhistssig{1}(sortedDaysS1,:));
ylabel('days')
xlabel('Time (ms)')
title('CA1')

[sortedDays2 sortedDaysS2]=sort(allanimdayvecRip{2}(:,2),'descend');
figure;
imagesc(xaxis,sortedDays2(end:-1:1),allallripmodhistssig{2}(sortedDaysS2,:));
ylabel('days')
xlabel('Time (ms)')
title('AC')

[sortedDays3 sortedDaysS3]=sort(allanimdayvecRip{3}(:,2),'descend');
figure;
imagesc(xaxis,sortedDays3(end:-1:1),allallripmodhistssig{3}(sortedDaysS3,:));
ylabel('days')
xlabel('Time (ms)')
title('PFC')
%% as above, but only for ripple-excited cells (because the rip-inh tend to have excitation before)

xaxis=-500:10:500;

[sortedDays1 sortedDaysS1]=sort(allanimdayvecRip{1}(:,2),'descend');
figure;
imagesc(xaxis,sortedDays1(end:-1:1),allallripmodhistssig{1}(sortedDaysS1,:));
ylabel('days')
xlabel('Time (ms)')
title('CA1')

[sortedDays2 sortedDaysS2]=sort(allanimdayvecRip{2}(:,2),'descend');
ACpsths=allallripmodhistssig{2}(sortedDaysS2,:);
ACdays=sortedDays2(end:-1:1);
excinh=mean(ACpsths(:,50:70),2)-mean(ACpsths(:,30:50),2);
figure;imagesc(xaxis,ACdays(excinh>0),ACpsths(excinh>0,:))
ylabel('days')
xlabel('Time (ms)')
title('AC')

[sortedDays3 sortedDaysS3]=sort(allanimdayvecRip{3}(:,2),'descend');
PFCpsths=allallripmodhistssig{3}(sortedDaysS3,:);
PFCdays=sortedDays3(end:-1:1);
excinh=mean(PFCpsths(:,50:70),2)-mean(PFCpsths(:,30:50),2);
figure;imagesc(xaxis,PFCdays(excinh>0),PFCpsths(excinh>0,:))
ylabel('days')
xlabel('Time (ms)')
title('PFC')


%% ripple-triggered spiking in all regions. Currently sorted by pre-response amplitude, can 
% also sort by days using:
% [sortedDays2 sortedDaysS2]=sort(allanimdayvecRip{2}(:,2),'descend');
zoomWindow=[-250 250];
% sleep
figure('Position',[900 100 scrsz(3)/3 scrsz(4)/2])
xaxis=-500:10:500;
subplot(3,1,1);
[sorted1 sortedS1]=sort(mean(allallripmodhistssig{1}(:,30:50),2),'descend');
imagesc(xaxis,1:size(allallripmodhistssig{1},1),allallripmodhistssig{1}(sortedS1,:));
xlim(zoomWindow)
title('Ripple-triggered PSTHs, all rip-mod cells, CA1');xlabel('Time (ms)');ylabel('Cells')
subplot(3,1,2);
[sorted2 sortedS2]=sort(mean(allallripmodhistssig{2}(:,30:50),2),'descend');
imagesc(xaxis,1:size(allallripmodhistssig{2},1),allallripmodhistssig{2}(sortedS2,:));
xlim(zoomWindow)
title('Ripple-triggered PSTHs, all rip-mod cells, AC');xlabel('Time (ms)');ylabel('Cells')
subplot(3,1,3);
[sorted3 sortedS3]=sort(mean(allallripmodhistssig{3}(:,30:50),2),'descend');
imagesc(xaxis,1:size(allallripmodhistssig{3},1),allallripmodhistssig{3}(sortedS3,:));
xlim(zoomWindow)
title('Ripple-triggered PSTHs, all rip-mod cells, PFC');xlabel('Time (ms)');ylabel('Cells')
annotation('textbox', [0.5, 1, 0, 0], 'string', 'Sleep','fontsize',20)

%%
% awake
figure('Position',[900 200 scrsz(3)/3 scrsz(4)/2])
xaxis=-500:10:500;
subplot(3,1,1);
[sorted1W sortedS1W]=sort(mean(allallripmodhistsWsig{1}(:,30:50),2),'descend');
imagesc(xaxis,1:size(allallripmodhistsWsig{1},1),allallripmodhistsWsig{1}(sortedS1W,:));
xlim(zoomWindow)
title('Ripple-triggered PSTHs, all rip-mod cells, CA1');xlabel('Time (ms)');ylabel('Cells')
subplot(3,1,2);
[sorted2W sortedS2W]=sort(mean(allallripmodhistsWsig{2}(:,30:50),2),'descend');
imagesc(xaxis,1:size(allallripmodhistsWsig{2},1),allallripmodhistsWsig{2}(sortedS2W,:));
xlim(zoomWindow)
title('Ripple-triggered PSTHs, all rip-mod cells, AC');xlabel('Time (ms)');ylabel('Cells')
subplot(3,1,3);
[sorted3W sortedS3W]=sort(mean(allallripmodhistsWsig{3}(:,30:50),2),'descend');
imagesc(xaxis,1:size(allallripmodhistsWsig{3},1),allallripmodhistsWsig{3}(sortedS3W,:));
xlim(zoomWindow)
title('Ripple-triggered PSTHs, all rip-mod cells, PFC');xlabel('Time (ms)');ylabel('Cells')
annotation('textbox', [0.5, 1, 0, 0], 'string', 'Awake','fontsize',20)

%% sleep vs awake, per region
figure('Position',[900 200 scrsz(3)/3 scrsz(4)/2])
xaxis=-500:10:500;
subplot(2,1,1);
imagesc(xaxis,1:size(allallripmodhistssig{1},1),allallripmodhistssig{1}(sortedS1,:));
xlim(zoomWindow)
xlabel('Time (ms)');ylabel('Cells')
title('Sleep')
subplot(2,1,2);
imagesc(xaxis,1:size(allallripmodhistsWsig{1},1),allallripmodhistsWsig{1}(sortedS1W,:));
xlim(zoomWindow)
annotation('textbox', [0.5, 1, 0, 0], 'string', 'CA1','fontsize',20)
xlabel('Time (ms)');ylabel('Cells')
title('Awake')

figure('Position',[900 200 scrsz(3)/3 scrsz(4)/2])
xaxis=-500:10:500;
subplot(2,1,1);
imagesc(xaxis,1:size(allallripmodhistssig{2},1),allallripmodhistssig{2}(sortedS2,:));
xlim(zoomWindow)
xlabel('Time (ms)');ylabel('Cells')
title('Sleep')
subplot(2,1,2);
imagesc(xaxis,1:size(allallripmodhistsWsig{2},1),allallripmodhistsWsig{2}(sortedS2W,:));
xlim(zoomWindow)
annotation('textbox', [0.5, 1, 0, 0], 'string', 'AC','fontsize',20)
xlabel('Time (ms)');ylabel('Cells')
title('Awake')

figure('Position',[900 200 scrsz(3)/3 scrsz(4)/2])
xaxis=-500:10:500;
subplot(2,1,1);
imagesc(xaxis,1:size(allallripmodhistssig{3},1),allallripmodhistssig{3}(sortedS3,:));
xlim(zoomWindow)
xlabel('Time (ms)');ylabel('Cells')
title('Sleep')
subplot(2,1,2);
imagesc(xaxis,1:size(allallripmodhistsWsig{3},1),allallripmodhistsWsig{3}(sortedS3W,:));
xlim(zoomWindow)
annotation('textbox', [0.5, 1, 0, 0], 'string', 'PFC','fontsize',20)
xlabel('Time (ms)');ylabel('Cells')
title('Awake')

%% difference in mean PSTH, sleep vs. awake
% CA1
CA1awake=allallripmodhistsWsig{1}(sortedS1W,:);
CA1sleep=allallripmodhistssig{1}(sortedS1,:);
% shifting each PSTH according to the pre window
CA1awake2=CA1awake-repmat(mean(CA1awake(:,1:25),2),1,size(CA1awake,2));
CA1sleep2=CA1sleep-repmat(mean(CA1sleep(:,1:25),2),1,size(CA1sleep,2));
figure('Position',[900 200 scrsz(3)/2 scrsz(4)/2])
subplot(3,1,1)
plot(xaxis,mean(abs(CA1awake2)),'linewidth',3);hold on;plot(xaxis,mean(abs(CA1sleep2)),'r','linewidth',3);
legend('awake','sleep');title('CA1');grid minor


% AC
ACawake=allallripmodhistsWsig{2}(sortedS2W,:);
ACsleep=allallripmodhistssig{2}(sortedS2,:);
% shifting each PSTH according to the pre window
ACawake2=ACawake-repmat(mean(ACawake(:,1:25),2),1,size(ACawake,2));
ACsleep2=ACsleep-repmat(mean(ACsleep(:,1:25),2),1,size(ACsleep,2));
subplot(3,1,2)
plot(xaxis,mean(abs(ACawake2)),'linewidth',3);hold on;plot(xaxis,mean(abs(ACsleep2)),'r','linewidth',3);
legend('awake','sleep');title('AC');grid minor


% PFC
PFCawake=allallripmodhistsWsig{3}(sortedS3W,:);
PFCsleep=allallripmodhistssig{3}(sortedS3,:);
% shifting each PSTH according to the pre window
PFCawake2=PFCawake-repmat(mean(PFCawake(:,1:25),2),1,size(PFCawake,2));
PFCsleep2=PFCsleep-repmat(mean(PFCsleep(:,1:25),2),1,size(PFCsleep,2));
subplot(3,1,3)
plot(xaxis,mean(abs(PFCawake2)),'linewidth',3);hold on;plot(xaxis,mean(abs(PFCsleep2)),'r','linewidth',3);
legend('awake','sleep');title('PFC');grid minor


%%
%% difference in mean PSTH, sleep vs. awake
% CA1
%CA1awake=allallripmodhistsWsig{1}(sortedS1W,:);
CA1sleep=allallripmodhistssig{1}(sortedS1,:);
% shifting each PSTH according to the pre window
%CA1awake2=CA1awake-repmat(mean(CA1awake(:,1:25),2),1,size(CA1awake,2));
CA1sleep2=CA1sleep-repmat(mean(CA1sleep(:,1:25),2),1,size(CA1sleep,2));
figure('Position',[900 200 scrsz(3)/4 scrsz(4)/2])
%subplot(2,1,1)
%plot(xaxis,mean(abs(CA1awake2)),'linewidth',3);hold on;
plot(xaxis,mean((CA1sleep2)),'b','linewidth',3);
%legend('awake','sleep');
title('CA1');grid minor


% AC
%ACawake=allallripmodhistsWsig{2}(sortedS2W,:);
%ACsleep=allallripmodhistssig{2}(sortedS2,:);
% shifting each PSTH according to the pre window
ACawake2=ACawake-repmat(mean(ACawake(:,1:25),2),1,size(ACawake,2));
ACsleep2=ACsleep-repmat(mean(ACsleep(:,1:25),2),1,size(ACsleep,2));
%subplot(2,1,2)
%plot(xaxis,mean(abs(ACawake2)),'linewidth',3);
hold on;
plot(xaxis,mean((ACsleep2)),'g','linewidth',3);
legend('CA1','AC');
grid minor


% % PFC
% PFCawake=allallripmodhistsWsig{3}(sortedS3W,:);
% PFCsleep=allallripmodhistssig{3}(sortedS3,:);
% % shifting each PSTH according to the pre window
% PFCawake2=PFCawake-repmat(mean(PFCawake(:,1:25),2),1,size(PFCawake,2));
% PFCsleep2=PFCsleep-repmat(mean(PFCsleep(:,1:25),2),1,size(PFCsleep,2));
% subplot(3,1,3)
% plot(xaxis,mean(abs(PFCawake2)),'linewidth',3);hold on;plot(xaxis,mean(abs(PFCsleep2)),'r','linewidth',3);
% legend('awake','sleep');title('PFC');grid minor
figure
hold on
shadedErrorBar(xaxis,mean(CA1sleep2,1),std(CA1sleep2)./sqrt(size(CA1sleep2,1)),'-b',1);
shadedErrorBar(xaxis,mean(ACsleep2,1),std(ACsleep2)./sqrt(size(ACsleep2,1)),'-g',1);
hold on
plot(xaxis,mean(CA1sleep2,1),'b','linewidth',3)
plot(xaxis,mean(ACsleep2,1),'g','linewidth',3)