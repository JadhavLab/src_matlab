%This code draws position occupancy, ripple position occupancy, and
%normalized ripple occupancy (the ratio of ripple/position).
%Each row is an epoch
%Colors in each of position/ripple/ratio are normalized across epochs

regRunEpochs=[2 4 6];
day8RunEpochs=[2 4 6 8];

for day=1:11
day
 epoch=regRunEpochs;
 if day==8 || day==11
     epoch=day8RunEpochs;

 end
     
    
nepochs=length(epoch);
ripTet=12;
epsilon=0.0001;
windowSize=10;
maxmaxall=0;
maxmaxrip=0;
maxmaxrate=0;


%[left, bottom, width, height]:
scrsz = get(0,'ScreenSize');
hh=figure('Position',[scrsz(3)/4 scrsz(4)/10 scrsz(3)/2 scrsz(4)*0.6])
for i=1:nepochs

if day<10
load(['/data15/gideon/Brg/Brgpos0' num2str(day) '.mat'])
load(['/data15/gideon/Brg/Brgripples0' num2str(day) '.mat'])
else
load(['/data15/gideon/Brg/Brgpos' num2str(day) '.mat'])
load(['/data15/gideon/Brg/Brgripples' num2str(day) '.mat'])
  
end


%all positions
pAll = pos{day}{epoch(i)}.data(:,2:3);
nAll=hist3(pAll,[{1:windowSize:250},{1:windowSize:250}])'+epsilon;
subplot(nepochs,4,(i-1)*4+2)
pcolor(nAll)
title('occupancy')
maxmaxall=max(maxmaxall,max(max(nAll)));

%ripple positions
riptimes = ripples{day}{epoch(i)}{ripTet}.midtime;
ripposindex = lookup(riptimes, pos{day}{epoch(i)}.data(:,1));
pRip = pos{day}{epoch(i)}.data(ripposindex,2:3);
nRip=hist3(pRip,[{1:windowSize:250},{1:windowSize:250}])';
subplot(nepochs,4,(i-1)*4+3)
pcolor(nRip)
title('ripples')
maxmaxrip=max(maxmaxrip,max(max(nRip)));

% subplot(2,1,2)
% plot(p(:,1), p(:,2), 'b.')
% title('ripple locations')


subplot(nepochs,4,(i-1)*4+4)
pcolor(nRip./nAll);
title('occupancy-normalized ripples')
maxmaxrate=max(maxmaxrate,max(max(nRip./nAll)));


%plotting the raw data on left panels
subplot(nepochs,4,(i-1)*4+1)
plot(pAll(:,1), pAll(:,2), 'b.')
hold on
plot(pRip(:,1), pRip(:,2), 'r.')
axis([0 250 0 250])

end
figure(hh)

for i=2:4:4*nepochs
    subplot(nepochs,4,i);
    caxis([0 maxmaxall]);
end
for i=3:4:4*nepochs
        subplot(nepochs,4,i);

    caxis([0 maxmaxrip]);
end
for i=4:4:4*nepochs
        subplot(nepochs,4,i);

    caxis([0 maxmaxrate]);
end

subtitle(['day ' num2str(day)])
figure
plot(pAll(:,1), pAll(:,2), 'b.')
hold on
plot(pRip(:,1), pRip(:,2), 'ro','markerfacecolor','r')
axis([50 230 50 230])
title(['Ripple location, day ' num2str(day)],'fontsize',20)
            saveas(gcf,['/data15/gideon/Brg/ProcessedData/riplocations/day' num2str(day) '.jpg'])

end