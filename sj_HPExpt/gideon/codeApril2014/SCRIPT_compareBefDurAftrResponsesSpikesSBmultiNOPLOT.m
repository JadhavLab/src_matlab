%% THIS SCRIPT EXTRACTS AUDITORY MULTIUNIT RESPONSES BEFORE, DURING AND AFTER 
% TRAINING FROM ANIMAL ROSENTHAL
% INSTEAD OF PLOTTING (LIKE THE PARALLEL SCRIPT), IT EXTRACTS VALUES

% can extract for all tets, but now only for AC
ACtets=15:21;
CA1tets=8:14;

allRespAmpsBef={};
allRespAmpsDur={};
allRespAmpsAft={};

for dayx=[1:2,4:12]
        
if dayx<10
    daystr=['0' num2str(dayx)];
else
    daystr=num2str(dayx);
end

[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/opt/data15/gideon/Rtl/EEG/Rtleeg', daystr,'1','22');
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/opt/data15/gideon/Rtl/EEG/Rtleeg', daystr,'10','22');
if strcmp(daystr,'06'), stimIndsAft=stimIndsAft(2:end);end

stimEEGtimesSB{1}=stimEEGtimesBef;
stimEEGtimesSB{2}=stimEEGtimesAft;
stimIndsSBMat=[stimIndsBef stimIndsAft];
whichStimSBMat=[whichStimBef;whichStimAft];
[stimEEGtimesTrack1 stimIndsTrack1]=findStimTrackFirstInPair('/opt/data15/gideon/Rtl/EEG/Rtleeg', daystr,'3','22');
[stimEEGtimesTrack2 stimIndsTrack2]=findStimTrackFirstInPair('/opt/data15/gideon/Rtl/EEG/Rtleeg', daystr,'5','22');
[stimEEGtimesTrack3 stimIndsTrack3]=findStimTrackFirstInPair('/opt/data15/gideon/Rtl/EEG/Rtleeg', daystr,'7','22');

if dayx~=5
[stimEEGtimesTrack4 stimIndsTrack4]=findStimTrackFirstInPair('/opt/data15/gideon/Rtl/EEG/Rtleeg', daystr,'9','22');
end

stimEEGTimesTrack{1}=stimEEGtimesTrack1;
stimEEGTimesTrack{2}=stimEEGtimesTrack2;
stimEEGTimesTrack{3}=stimEEGtimesTrack3;
if dayx~=5

stimEEGTimesTrack{4}=stimEEGtimesTrack4;
end

stimIndsTrack{1}=stimIndsTrack1;
stimIndsTrack{2}=stimIndsTrack2;
stimIndsTrack{3}=stimIndsTrack3;
if dayx~=5
stimIndsTrack{4}=stimIndsTrack4;
end
%compareBefAftrResponsesSpikesSBmulti('/opt/data15/gideon/Rtl/EEG/Rtleeg',daystr,'1','10',1:21, '/opt/data15/gideon/Rtl/Rtlmulti',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft)
if dayx~=5
[respAmpsBef respAmpsDur respAmpsAft]=compareBefDurAftrResponsesSpikesSBmultiNOPLOT(daystr,[1 10],[3 5 7 9],1:21, '/opt/data15/gideon/Rtl/Rtlmulti',stimEEGtimesSB,stimIndsSBMat,whichStimSBMat,stimEEGTimesTrack,stimIndsTrack);

else
[respAmpsBef respAmpsDur respAmpsAft]=compareBefDurAftrResponsesSpikesSBmultiNOPLOT(daystr,[1 10],[3 5 7],1:21, '/opt/data15/gideon/Rtl/Rtlmulti',stimEEGtimesSB,stimIndsSBMat,whichStimSBMat,stimEEGTimesTrack,stimIndsTrack);
    
end
allRespAmpsBef{dayx}=respAmpsBef;
allRespAmpsDur{dayx}=respAmpsDur;
allRespAmpsAft{dayx}=respAmpsAft;
close all;
clear stimIndsTrack stimEEGTimesTrack stimEEGtimesSB stimEEGtimesBef stimEEGtimesAft
cc=1;
end
%% this looks at response to target sound (pre, dur post), and BBN (pre, post) (AC tets)
% over days. It doesn't show anything striking, tetrodes are very different
% more systematic analysis in next section

tetXtargetBef=[];
tetXtargetAft=[];
tetXtargetDur=[];
% choose tet here
tetX=9;
for i=[1,2,4:12], 
    tetXtargetBef=[tetXtargetBef allRespAmpsBef{i}(tetX,1)];
 tetXtargetAft=[tetXtargetAft allRespAmpsAft{i}(tetX,1)];
 tetXtargetDur=[tetXtargetDur allRespAmpsDur{i}(tetX)];
end

figure;
subplot(2,1,1)
plot([1,2,4:12],tetXtargetBef)
hold on
plot([1,2,4:12],tetXtargetAft,'r')
plot([1,2,4:12],tetXtargetDur,'k')

subplot(2,1,2)
tetXBBNBef=[];
tetXBBNAft=[];
tetXBBNDur=[];


for i=[1,2,4:12], tetXBBNBef=[tetXBBNBef allRespAmpsBef{i}(tetX,4)];end
for i=[1,2,4:12], tetXBBNAft=[tetXBBNAft allRespAmpsAft{i}(tetX,4)];end
%for i=[1,2,4:12], tetXBBNDur=[tetXBBNDur allRespAmpsDur{i}(tetX)];end

plot([1,2,4:12],tetXBBNBef)
hold on
plot([1,2,4:12],tetXBBNAft,'r')
%plot([1,2,4:12],tetXBBNDur,'k')
%% does activity on the track predict enhancement of post-pre? (AC tets)
% currently activity on track predicts the difference post-pre for the
% target sound. It doesn't predict change in pre-post for BBN and pure
% tones (as expected). It does for down chirps.
% looking only at days where the target was up-chirps (1-9)
tetXtargetBef=[];
tetXtargetAft=[];
tetXtargetDur=[];
tetXDist1Bef=[];
tetXDist1Aft=[];
tetXDist2Bef=[];
tetXDist2Aft=[];
tetXBBNBef=[];
tetXBBNAft=[];

for tetX=[15:17,19:21]
for days1=[1,2,4:9], 
    tetXtargetBef=[tetXtargetBef allRespAmpsBef{days1}(tetX,1)];
 tetXtargetAft=[tetXtargetAft allRespAmpsAft{days1}(tetX,1)];
 tetXtargetDur=[tetXtargetDur allRespAmpsDur{days1}(tetX)];
 tetXDist1Bef=[tetXDist1Bef allRespAmpsBef{days1}(tetX,2)];
 tetXDist1Aft=[tetXDist1Aft allRespAmpsAft{days1}(tetX,2)];
  tetXDist2Bef=[tetXDist2Bef allRespAmpsBef{days1}(tetX,3)];
 tetXDist2Aft=[tetXDist2Aft allRespAmpsAft{days1}(tetX,3)];
 tetXBBNBef=[tetXBBNBef allRespAmpsBef{days1}(tetX,4)];
 tetXBBNAft=[tetXBBNAft allRespAmpsAft{days1}(tetX,4)];
 
end
end


figure;
subplot(1,4,1)
plot(tetXtargetDur,tetXtargetAft-tetXtargetBef,'ko')
axis([-1 5 -5 10])
[r p]=corrcoef(tetXtargetDur,tetXtargetAft-tetXtargetBef)
title(['target sound, R=' num2str(r(2,1)) ' P=' num2str(p(2,1))]);
xlabel('response on track')
ylabel('response post-pre')
p=polyfit(tetXtargetDur,tetXtargetAft-tetXtargetBef,1);
f=polyval(p,0:5);
hold on
plot(0:5,f,'r')

subplot(1,4,2)
plot(tetXtargetDur,tetXDist1Aft-tetXDist1Bef,'ko')
axis([-1 5 -5 10])
[r p]=corrcoef(tetXtargetDur,tetXDist1Aft-tetXDist1Bef)
title(['down chirps, R=' num2str(r(2,1)) ' P=' num2str(p(2,1))]);
p=polyfit(tetXtargetDur,tetXDist1Aft-tetXDist1Bef,1);
f=polyval(p,0:5);
hold on
plot(0:5,f,'r')

subplot(1,4,3)
plot(tetXtargetDur,tetXDist2Aft-tetXDist2Bef,'ko')
axis([-1 5 -5 10])
[r p]=corrcoef(tetXtargetDur,tetXDist2Aft-tetXDist2Bef)
title(['pure tones, R=' num2str(r(2,1)) ' P=' num2str(p(2,1))]);


subplot(1,4,4)
plot(tetXtargetDur,tetXBBNAft-tetXBBNBef,'ko')
axis([-1 5 -5 10])
[r p]=corrcoef(tetXtargetDur,tetXBBNAft-tetXBBNBef)
title(['BBN, R=' num2str(r(2,1)) ' P=' num2str(p(2,1))]);
%%
% are responses larger after? (AC tets)
figure;
subplot(1,4,1)
plot(tetXtargetBef,tetXtargetAft,'ko')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXtargetBef,tetXtargetAft,0.05,'left')
title(['target, P=' num2str(p)])

subplot(1,4,2)
plot(tetXDist1Bef,tetXDist1Aft,'ko')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXDist1Bef,tetXDist1Aft,0.05,'left')
title(['down chirp, P=' num2str(p)])

subplot(1,4,3)
plot(tetXDist2Bef,tetXDist2Aft,'ko')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXDist2Bef,tetXDist2Aft,0.05,'left')
title(['pure tones, P=' num2str(p)])

subplot(1,4,4)
plot(tetXBBNBef,tetXBBNAft,'ko')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXBBNBef,tetXBBNAft,0.05,'left')
title(['BBN, P=' num2str(p)])


%% now for the last dats where BBN was the target, expect the activity of the target to 
% predict difference between pre and post for BBN. Doesn't currently reach
% significance
tetXtargetBef=[];
tetXtargetAft=[];
tetXtargetDur=[];
tetXDist1Bef=[];
tetXDist1Aft=[];
tetXDist2Bef=[];
tetXDist2Aft=[];
tetXBBNBef=[];
tetXBBNAft=[];

for tetX=[15:17,19:21]
for days1=[10:12], 
    tetXtargetBef=[tetXtargetBef allRespAmpsBef{days1}(tetX,1)];
 tetXtargetAft=[tetXtargetAft allRespAmpsAft{days1}(tetX,1)];
 tetXtargetDur=[tetXtargetDur allRespAmpsDur{days1}(tetX)];
 tetXDist1Bef=[tetXDist1Bef allRespAmpsBef{days1}(tetX,2)];
 tetXDist1Aft=[tetXDist1Aft allRespAmpsAft{days1}(tetX,2)];
  tetXDist2Bef=[tetXDist2Bef allRespAmpsBef{days1}(tetX,3)];
 tetXDist2Aft=[tetXDist2Aft allRespAmpsAft{days1}(tetX,3)];
 tetXBBNBef=[tetXBBNBef allRespAmpsBef{days1}(tetX,4)];
 tetXBBNAft=[tetXBBNAft allRespAmpsAft{days1}(tetX,4)];
 
end
end


figure;
subplot(1,4,1)
plot(tetXtargetDur,tetXtargetAft-tetXtargetBef,'ko')
axis([-1 5 -5 10])
[r p]=corrcoef(tetXtargetDur,tetXtargetAft-tetXtargetBef)
title(['target sound, R=' num2str(r(2,1)) ' P=' num2str(p(2,1))]);
xlabel('response on track')
ylabel('response post-pre')


subplot(1,4,2)
plot(tetXtargetDur,tetXDist1Aft-tetXDist1Bef,'ko')
axis([-1 5 -5 10])
[r p]=corrcoef(tetXtargetDur,tetXDist1Aft-tetXDist1Bef)
title(['down chirps, R=' num2str(r(2,1)) ' P=' num2str(p(2,1))]);


subplot(1,4,3)
plot(tetXtargetDur,tetXDist2Aft-tetXDist2Bef,'ko')
axis([-1 5 -5 10])
[r p]=corrcoef(tetXtargetDur,tetXDist2Aft-tetXDist2Bef)
title(['pure tones, R=' num2str(r(2,1)) ' P=' num2str(p(2,1))]);


subplot(1,4,4)
plot(tetXtargetDur,tetXBBNAft-tetXBBNBef,'ko')
axis([-1 5 -5 10])
[r p]=corrcoef(tetXtargetDur,tetXBBNAft-tetXBBNBef)
title(['BBN, R=' num2str(r(2,1)) ' P=' num2str(p(2,1))]);