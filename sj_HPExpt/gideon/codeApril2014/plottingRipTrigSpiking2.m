


%interesting

  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 8,4, 13, 13, 5, 1)
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 9,1, 13, 13, 1, 1)

  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 9,3, 13, 13, 3, 1) %inhibited?
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 9,5, 13, 13, 3, 1) % ||

  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 9,1, 13, 13, 17, 1)
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 9,1, 13, 13, 19, 1)

  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 9,1, 13, 13, 19, 2)
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 9,2, 13, 13, 21, 1)% inhibited, in other epochs as well
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 9,2, 13, 13, 21, 2)% inhibited, in other epochs as well

  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 10,1, 13, 13, 1, 1)%active before rips? maybe responses to sounds, and ripples on offset?
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 10,5, 13, 13, 1, 1)%|| (though no stim here)

  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 10,1, 13, 13, 2, 1)%aud neuron directly activated?! in other epochs too, though run epochs different
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 10,3, 13, 13, 3, 1) % NICEST SO FAR, SAME AS ABOVE, occurs in other SB sessions too
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 10,1, 13, 13, 3, 2) % ON SAME TET AS ABOVE, THIS AUD CELL IS INHIBITED BY RIPS IN SB! LOOK ALSO AT EPOCH 7
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 10,6, 13, 13, 3, 2) % ON TRACK, IT SEEMS TO FIRE SIMULTANEOUSLY WITH RIPS, AND THEN AROUND 300MS LATER, are these times of aud stim?
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 10,7, 13, 13, 5, 1) % either inhibited by rips, or rips tend to come after sound,,,

  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 10,1, 13, 13, 15, 1)% activated by rips, but also a band at -100. In run epochs tends to respond later
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 10,6, 13, 13, 15, 2)% on track, fires 100ms after rips?

  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 11,1, 13, 13, 1, 1)% 200 msc after rips
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 11,3, 13, 13, 3, 2)% **after rips, also in epoch 5,7,9
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 11,6, 13, 13, 3, 2)% locked?
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 11,4, 13, 13, 5, 1)% very interesting. Rips act as an "on switch"? also in other epochs. Find out where sounds are
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 11,1, 13, 13, 19, 2)%
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 11,1, 13, 13, 21, 1)% **WOW, PFC cell prefectly locked to ripples in virtually all epochs. Interneuron?
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 11,1, 13, 13, 21, 2)% **And this one, on the same tet, seems inhibited. This may be a pyramidal connected to the above interneuron? look also in epoch 8
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 11,1, 13, 13, 21, 3)% This neuron is activated by ripples in SB, and inhibited by ripples on track? or longer delay?
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 12,9, 13, 13, 1, 1)% tends to fire 100-150ms after ripple in SB
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 12,5, 13, 13, 3, 2)% **activated. again, "switch"-like. Look also in epoch 9
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 12,1, 13, 13, 5, 1)% activity 300-400 ms before rips, ripple firing on offset of sound? SEE NEXT LINE
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 12,5, 13, 13, 15, 1)
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 12,2, 13, 13, 17, 1)
%CELLS ON TETS 17 AND 19 SEEM MOSTLY NOT MODULATED BY RIPS
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 12,1, 13, 13, 21, 1)% activated in most SB epochs
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 12,6, 13, 13, 21, 2)% weird, silent before and after rips on track
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 12,2, 13, 13, 21, 3)% activated by rips on track

sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 13,3, 14, 14, 1,1);%activated by rips
sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 13,3, 14, 14, 15,1); %**start to ramp up before rips?
sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 13,3, 14, 14, 15,3);%preceding rips? in most SB sessions
%%
%DAY 14,16,17
for i=9:-1:1,
    try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,i, 14, 14, 1,1);
%         saveas(gcf-1,['NewFolder.2/anticipatory/day17ep' num2str(i) 'tet1cell1'],'jpg')
%         saveas(gcf,['NewFolder.2/anticipatory/day17ep' num2str(i) 'tet1cell1raster'],'jpg')
    
    catch
    end
end
%%
%DAY 15
for i=10:-1:1,
    try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 15,i, 14, 14, 21,1);
        saveas(gcf-1,['NewFolder.2/nicepfc/day15ep' num2str(i) 'tet21cell1'],'jpg')
        saveas(gcf,['NewFolder.2/nicepfc/day15ep' num2str(i) 'tet21cell1raster'],'jpg')
    
    catch
    end
end

%%
        
        %appearance of the pre-rip neuron!

        % since it appears over multiple days on the same tet, with similar spike
% width, maybe same cell?
% check if it is auditory responsive, as well as others cells on same
% tetrode, to rule out weird tetrode location
for i=9:-1:1,try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 14,i, 14, 14, 15,1);%maybe activated, then inhibited, then activated by rips in epoch1 , look at other epochs
for i=9:-1:1,try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 14,i, 14, 14, 17,3);%locked to ripples during run epochs
for i=9:-1:1,try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 14,i, 14, 14, 21,1);%inhibited by ripples on track
        
%Day 15
for i=9:-1:1,try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 15,i, 14, 14, 21,1);%** The superstar cell...
for i=9:-1:1,try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 15,i, 14, 14, 21,4);%** strongly activated by rips!! most epochs
for i=9:-1:1,try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 15,i, 14, 14, 21,1);%** strongly activated by ripples, maybe before? in most epochs
for i=9:-1:1,try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 15,i, 14, 14, 21,4)%strongly activated by rips!! most epochs. similar to cell 1 on same tet
        

%Day 16
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,1, 13, 14, 1, 1)% very strongly activated auditory neuron, starts before ripple?
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,3, 13, 14, 1, 1)% cont from above- happens without stim too
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,5, 13, 14, 1, 1)% cont- WOW!, similar for all SB sessions.
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,8, 13, 14, 1, 2)% activated by rips on track?
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,3, 13, 14, 7,1)% seems inhibited following rips, switch style
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,7, 13, 14, 17,2)% inhibited? some other SB spochs
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,1, 13, 14, 17,3)% same?
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,1, 13, 14, 17,6)% activated
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,9, 13, 14, 17,6)% peaks before and after rips, in some other epochs too
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,1, 13, 14, 21,1)%activated in SB
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,8, 13, 14, 21,1)%cont from above- on track fires before ripples?! see next line for prev epoch:
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,6, 13, 14, 21,1) % again, firing on track before rips. In first 2 run epochs fires before and during rips

%Day 17
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,1, 13, 14, 1,1)%strongly activated aud neuron, starts firing before ripple!
% cont from above: I see this in day 16 too (not looked at 15 yet), maybe
% this is a result of learning!?!?! Look if this is the same unit as day
% 16, if not it's nicer.. Could this be that as a result of the learning,
% ripples become triggered by auditory cortex?
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,8, 13, 14, 1,1)% this is a run session from above, showing nice locking
  sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,7, 13, 14, 1,1)%another epoch from above, WOW! this is really nice! 
% CONTINUE HERE
sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,7, 13, 14, 5,2);% another example of auditory before ripples, with no stim. Somewhat in other epochs too
 sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,1, 13, 14, 6,1);% increase following ripples?
 
sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,4, 13, 14, 15,1);% inhibition during ripples, weak but happens in all SB epochs and maybe runs
sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,2, 13, 14, 15,3);% locked to ripples? not many spikes but see in other epochs too
sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,1, 13, 14, 17,1);% inhibition during, fires 200 ms after rips. also in epochs 1-2
sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,1, 13, 14, 17,3);% inhibition during, fires 200 ms after rips. also in other epochs 
for i=9:-1:1,try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,i, 13, 14, 17,4); catch ;end;end% very clear ramping up before and following rips in some epochs
for i=9:-1:1,try sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 17,i, 13, 14, 21,1); catch ;end;end% very clear ramping up before and following rips in some epochs
