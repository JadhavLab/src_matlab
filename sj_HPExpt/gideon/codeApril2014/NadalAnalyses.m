% Analyses

%% Count cells across days- fig
countCells
 %%  Behavior- figs
 GRdioanalysis

%% Dealing with chew artifact - so far not included
findChewArtifact
% also stages in NadalPreprocessing



%% place fields
% excellent place fields for CA1
% AC: some neurons tend not to fire in junction, maybe because no sound
% movement (e.g., tet 3 cell days 10-11). But other do fire there (d11t5c1)
% d11t21c1: becomes reward-prone?
% d12t17c3: maybe too?
% d12t11c3: place cell moving to reward?
% d12t15c1: becomes junction-prone,also d15t15c1
% d12t17c1: maybe too?
% d13t21c1: too
% auditory cells selective for home well: d13t1c1,d13t3c1,d13t3c2(also
% junction), 
% place cell remapping: d13t13c1, d14t14c1,
% d15t10c2,d15t11c1?,d15t14c1,d17t10c3,d17t10c8,d17t14c1
% d13t15c2: PFC cell becoming a "route" cell?
% Example PFC cells with small preference (there are others
% too):d16t17c4,d16t17c5,d16t21c1,d17t21c1
% PFC "place cell"? d13t21c6 
% Interesting! AC cells with "place field" in home and reward wells
% d16t5c1, d16t6c1, d16t7c1, weaker-d17t3c1, d17t5c1, strong!: d17t5c2,
% d17t5c4,
% 15-3-11-1: close to home well, but only upon return. Looks like a
% pattern: place fields near a reward well are selective for direction
% towards the well? another example: 15-3-14-1 (except last epoch weird)
% counter example: 15-3-14-3, 17-2-10-1
% 15-3-11-3: place field highly selective to returning from R 
% 17-2-10-8: place field at home, highly selective to going out to R
% 15-3-1-2: AC cell fires more on way out to R, probably because sounds.check
% 15-3-2-1, 17-2-5-1: AC cell prefers returning from R (so probably not sound..)
% 16-2-5-1, 17-2-3-1. 17-2-3-2, 17-4-5-4: AC cell prefers home arm on the way out, and reward wells on
% the way in
% 13-2-11-3: place field before junction, very different if going left or right (bigger if right)
% 13-2-14-1, 14-2-14-1, 16-2-10-1, 16-2-14-1, 17-2-10-5, 17-2-14-1 (developing): much stronger firing on the way to R compared to returning
% from it
% 16-1-21-1: PFC cell decays with distance from home
% 13-4-17-3,14-2-17-2: PFC prefers returning from L
% 17-2-15-2: PFC cell prefers wells
DFSsj_HPexpt_placefield2

%% LFP responses to sounds on track throughout days, and as a fucntion of speed

% didn't follow through this. Look into the integration window, and if
% higher responses in first day in AC is due to artifact (like ref tet)
% for now, can take some example plots from different days.
soundEvokedLFPTrackDays



%% responses to sounds on the track
% run this for all run epochs and watch sound-triggered spiking
% many interesting things here, go over all
% neurons becoming responsive to only offset of second sound- object
% responsive?
% neurons responding only to beginning of series
% Hc and PFC responding
% compare to responses in SB
% much more...

[stimEEGtimes stimInds]=findStimTrack('/data15/gideon/Ndl/EEG/Ndleeg', '15','3','22');
[allSpikes]=getResponsesSpikesTrack('/data15/gideon/Ndl/EEG/Ndleeg','15','3',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes,[],[],stimInds);
%% auditory responses in first no-track days with pure tone stimuli
% notice this is an example from day 3, where there were a couple of nice
% units and ok LFP. In other days nice LFP but units not very nice
[allTrialsPerStim allTrialsPerStimInd stimEEGtimes stimInds]=findStimNdlDay3('/data15/gideon/Ndl/EEG/Ndleeg', '03','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);

% LFP:
[sac spikeResponses]=getResponsesLFP('/data15/gideon/Ndl/EEG/Ndleeg','03','1',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes,allTrialsPerStim,allTrialsPerStimInd,stimInds);

% spikes
[allSpikes]=getResponsesSpikesSBTones('/data15/gideon/Ndl/EEG/Ndleeg','03','1',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes,stimInds);
%% other examples (but can do for all days 2-7)
[allTrialsPerStim allTrialsPerStimInd stimEEGtimes stimInds]=findStimBrg('/data15/gideon/Ndl/EEG/Ndleeg', '04','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[sac spikeResponses]=getResponsesLFP('/data15/gideon/Ndl/EEG/Ndleeg','04','1',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes,allTrialsPerStim,allTrialsPerStimInd,stimInds);
[allSpikes]=getResponsesSpikesSBTones('/data15/gideon/Ndl/EEG/Ndleeg','04','1',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes,stimInds);

[allTrialsPerStim allTrialsPerStimInd stimEEGtimes stimInds]=findStimBrg('/data15/gideon/Ndl/EEG/Ndleeg', '05','3','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[sac spikeResponses]=getResponsesLFP('/data15/gideon/Ndl/EEG/Ndleeg','05','3',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes,allTrialsPerStim,allTrialsPerStimInd,stimInds);
[allSpikes]=getResponsesSpikesSBTones('/data15/gideon/Ndl/EEG/Ndleeg','05','3',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes,stimInds);

[allTrialsPerStim allTrialsPerStimInd stimEEGtimes stimInds]=findStimBrg('/data15/gideon/Ndl/EEG/Ndleeg', '06','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[sac spikeResponses]=getResponsesLFP('/data15/gideon/Ndl/EEG/Ndleeg','06','1',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes,allTrialsPerStim,allTrialsPerStimInd,stimInds);
[allSpikes]=getResponsesSpikesSBTones('/data15/gideon/Ndl/EEG/Ndleeg','06','1',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes,stimInds);
%% for craig
% but there is a jitter in the stim finding, consult with Craig
craigAnalysis;
%% compare responses to sounds across both SB and track sessions
%for day=[11,13,14,16,17]
    daystr='15';%num2str(day);
%--- SB sessions
[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', daystr,'2','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', daystr,'10','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
stimEEGtimesSB{1}=stimEEGtimesBef;
stimEEGtimesSB{2}=stimEEGtimesAft;
stimIndsSBMat=[stimIndsBef stimIndsAft];
whichStimSBMat=[whichStimBef;whichStimAft];
%--- Track sessions

[stimEEGtimesTrack1 stimIndsTrack1]=findStimTrackFirstInPair('/data15/gideon/Ndl/EEG/Ndleeg', daystr,'3','22');
[stimEEGtimesTrack2 stimIndsTrack2]=findStimTrackFirstInPair('/data15/gideon/Ndl/EEG/Ndleeg', daystr,'5','22');
[stimEEGtimesTrack3 stimIndsTrack3]=findStimTrackFirstInPair('/data15/gideon/Ndl/EEG/Ndleeg', daystr,'7','22');
[stimEEGtimesTrack4 stimIndsTrack4]=findStimTrackFirstInPair('/data15/gideon/Ndl/EEG/Ndleeg', daystr,'9','22');

stimEEGTimesTrack{1}=stimEEGtimesTrack1;
stimEEGTimesTrack{2}=stimEEGtimesTrack2;
stimEEGTimesTrack{3}=stimEEGtimesTrack3;
stimEEGTimesTrack{4}=stimEEGtimesTrack4;

stimIndsTrack{1}=stimIndsTrack1;
stimIndsTrack{2}=stimIndsTrack2;
stimIndsTrack{3}=stimIndsTrack3;
stimIndsTrack{4}=stimIndsTrack4;

%--- Comparing
compareBefDurAftrResponsesSpikesSB(daystr,[2 10],[3 5 7 9],1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimesSB,stimIndsSBMat,whichStimSBMat,stimEEGTimesTrack,stimIndsTrack);
%end
%% auditory responses in SB
[stimEEGtimes1 stimInds1 whichStim1]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '12','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[allSpikes]=getResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','12','1',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes1,stimInds1,whichStim1);
%% auditory responses in SB, compare first and last epochs
% Go over this more carefully. For now, the points that come out are:
% 1. Different response profiles within all regions
% 2. Maybe tendency towards more responses to target sound, but not huge
% 3. Within day neurons definitely show different response profiles
% 4. Hc neurons seem to be sound responsive mostly in middle days?
% 5. Some exotic neurons to look into
% 6. 

[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '08','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '08','7','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','08','1','7',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft);
[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '09','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '09','6','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','09','1','6',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft);
[stimEEGtimes121 stimInds121 whichStim121]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '12','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimes129 stimInds129 whichStim129]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '12','9','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','12','1','9',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes121,stimInds121,whichStim121,stimEEGtimes129,stimInds129,whichStim129);
[stimEEGtimes101 stimInds101 whichStim101]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '10','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimes107 stimInds107 whichStim107]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '10','7','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','10','1','7',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimes101,stimInds101,whichStim101,stimEEGtimes107,stimInds107,whichStim107);
[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '11','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '11','9','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','11','1','9',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft);
[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '12','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '12','9','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','12','1','9',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft);
[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '13','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '13','9','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','13','1','9',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft);
[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '14','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '14','9','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','14','1','9',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft);
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '15','10','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '15','2','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','15','2','10',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft);
[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '16','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '16','9','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','16','1','9',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft);
[stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '17','1','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
[stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/data15/gideon/Ndl/EEG/Ndleeg', '17','9','22','/home/gideon/Code/Matlab/makingSounds/soundSeries/toneSweep1/freqsAttensToneSweep1.mat',0);
compareBefAftrResponsesSpikesSB('/data15/gideon/Ndl/EEG/Ndleeg','17','1','9',1:21, '/data15/gideon/Ndl/Ndlspikes',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft);




%% ripple location
plotRipLocationScript

%%
 countRipplesPerEpoch
%% ripple-triggered spiking
plottingRipTrigSpiking2.m

%% ripple-triggered spectrograms
% these are examples, do for all 
% idea: in run, ripples trigger cortex, in sleep, cortex triggers ripples ?!?!?
[params] = sj_HPexpt_eventtrigspecgrams_getrip('Ndl', 9, 6, 15, 'rip',13,1,0,1,'movingwin',[1000 100]/1000,'fpass',[0 40])
[params] = sj_HPexpt_eventtrigspecgrams_getrip('Ndl', 10, 5, 1, 'rip',13,1,0,1,'movingwin',[400 40]/1000,'fpass',[0 100])
[params] = sj_HPexpt_eventtrigspecgrams_getrip('Ndl', 10, 6, 1, 'rip',13,1,0,1,'movingwin',[100 10]/1000,'fpass',[0 400])

%%
ripTrigResponsesACPFC
%% This script allows looking at ripple-triggered spectrograms between 0-400Hz.
% The novel finding that seems to arise here is that on later days, and
% only on the track, there is a high-frequency component BEFORE ripples, in
% both PFC and AC. When looking at the raw data it looks like it's not an
% artifact, but rather spikes! 
ripTrigResponsesACPFCHighFreqs2
%% So the next thing is to go back to
% ripple-triggered spiking in these days. It actually seems real!!
% look in plottingRipTrigSpiking2, but here is an example:
sj_HPexpt_ripalign_singlecell_getrip_tmp('Ndl', 16,8, 13, 14, 21,1)
%% how do stimuli and ripples interact on the track?
% seems that ripples occur just before or in onset of stimuli series in the
% first days, and in later days it happens 5 seconds before stimuli onset,
% supposedly when they are getting the reward (there is a 5 seconds
% delay)
% These preceding ripples can either mean expectancy for sound, just
% ripples during rest, or reward-induced. To tell apart, need to look at
% pokes in the middle port when no sound was given.
% Look like learning-induced sharpening of ripple timing around reward times
str=getSoundRippleInteraction2('/data15/gideon/Ndl/EEG/Ndleeg','08',[2 4 6],11:14,'/data15/gideon/Ndl/Ndlripples');
str=getSoundRippleInteraction2('/data15/gideon/Ndl/EEG/Ndleeg','09',[2 5],11:14,'/data15/gideon/Ndl/Ndlripples');
str=getSoundRippleInteraction2('/data15/gideon/Ndl/EEG/Ndleeg','11',[2 4 6 8],11:14,'/data15/gideon/Ndl/Ndlripples');
str=getSoundRippleInteraction2('/data15/gideon/Ndl/EEG/Ndleeg','12',[2 4 6 8],11:14,'/data15/gideon/Ndl/Ndlripples');
str=getSoundRippleInteraction2('/data15/gideon/Ndl/EEG/Ndleeg','13',[2 4 6 8],11:14,'/data15/gideon/Ndl/Ndlripples');
str=getSoundRippleInteraction2('/data15/gideon/Ndl/EEG/Ndleeg','14',[2 4 6 8],11:14,'/data15/gideon/Ndl/Ndlripples');
str=getSoundRippleInteraction2('/data15/gideon/Ndl/EEG/Ndleeg','15',[3 5 7 9],11:14,'/data15/gideon/Ndl/Ndlripples');
str=getSoundRippleInteraction2('/data15/gideon/Ndl/EEG/Ndleeg','16',[2 4 6 8],11:14,'/data15/gideon/Ndl/Ndlripples');
str=getSoundRippleInteraction2('/data15/gideon/Ndl/EEG/Ndleeg','17',[2 4 6 8],11:14,'/data15/gideon/Ndl/Ndlripples');

%% how do stimuli and ripples interact in the SB?
% no clear result here. sometimes it seems ripple rate decreases with
% sounds, sometimes stays or increases. Ripple rate following the target
% sound may have slightly stronger effect, but weak. My guess is the sounds
% have a combined effect of waking up (-decrease ripples) and replaying
% (=increase), and the ratio changes. 
% Here are a few examples, but can run for all:
getSoundRippleInteractionSB('/data15/gideon/Ndl/EEG/Ndleeg','15',[2],11:14,'/data15/gideon/Ndl/Ndlripples');
getSoundRippleInteractionSB('/data15/gideon/Ndl/EEG/Ndleeg','15',[10],11:14,'/data15/gideon/Ndl/Ndlripples');
getSoundRippleInteractionSB('/data15/gideon/Ndl/EEG/Ndleeg','16',[1],11:14,'/data15/gideon/Ndl/Ndlripples');
getSoundRippleInteractionSB('/data15/gideon/Ndl/EEG/Ndleeg','16',[9],11:14,'/data15/gideon/Ndl/Ndlripples');
getSoundRippleInteractionSB('/data15/gideon/Ndl/EEG/Ndleeg','17',[9],11:14,'/data15/gideon/Ndl/Ndlripples');
getSoundRippleInteractionSB('/data15/gideon/Ndl/EEG/Ndleeg','17',[1],11:14,'/data15/gideon/Ndl/Ndlripples');

%% giving it another shot- here looking around single sound pairs rather than full series. There might
% be a tendency that the target sounds evoke more ripples between days
% 13-15? here are a few examples:
getSoundRippleInteractionSBsingleSounds('/data15/gideon/Ndl/EEG/Ndleeg','17',[9],11:14,'/data15/gideon/Ndl/Ndlripples');
getSoundRippleInteractionSBsingleSounds('/data15/gideon/Ndl/EEG/Ndleeg','09',[1],11:14,'/data15/gideon/Ndl/Ndlripples');
getSoundRippleInteractionSBsingleSounds('/data15/gideon/Ndl/EEG/Ndleeg','13',[9],11:14,'/data15/gideon/Ndl/Ndlripples');

%% correlations. Lots to look into here, both within and between regions
 DFSsj_HPexpt_xcorrmeasures3
%% theta modulation
DFSsj_plotthetamod
 %% can add rewards later too...
 plotRasterSpikesAndRipples