% run on the output of DFSsj_HPexpt_getripandsoundalignspikingAC3
savedirX = '/opt/data15/gideon/ProcessedData/';
areas={'AC','CA1','PFC'}
%areas={'AC'}
savedirimgsX='/opt/data15/gideon/ProcessedData/RipVsSound2/Rtl/';
saveImgs=1;
            smoothwin = ones(1,15)/15;

histOns=[];
histOnsW=[];
allxcorrLags={};
for areaInd=1:3
    area=areas{areaInd};
    
            % compare sleep and awake ripple-evoked activity
            load([savedirX 'Rtl_ripplemodsoundmod4_' area '_gather']);
            allripplemod_idx=[];
            for w=1:length(allripplemod),allripplemod_idx=[allripplemod_idx;allripplemod(w).index];end
            allsoundmod_idx=[];
            for w=1:length(allsoundmod),allsoundmod_idx=[allsoundmod_idx;allsoundmod(w).index];end
              
    speedmodsQ=[];
    allinds=unique([allsoundmod_idx;allripplemod_idx],'rows');
plotSoundRip=1;
    soundRespType=[];
    soundRipCorr=[];
    soundResp1=[];
    ripResp1=[];
    ripresps=[];
    soundresps=[];
    ripmodamp=[];
    soundmodamp=[];
    riprespsall=[];
             sndrespsall=[];
             riprespsallP=[];
             sndrespsallP=[];
    x1=linspace(-500,500,101)
    scrsz = get(0,'ScreenSize');
    for v=1:size(allinds,1)
        curidx=allinds(v,:);%animal day
        sndidx=find(ismember(allsoundmod_idx,curidx,'rows'));
        ripidx=find(ismember(allripplemod_idx,curidx,'rows'));
         if ~isempty(ripidx)
            meanriptrace=mean(allripplemod(1,ripidx).hist);
            meanriptraceZ=(meanriptrace-mean(meanriptrace))/std(meanriptrace);
            
        end
        if ~isempty(sndidx)
            meansndtrace=mean(allsoundmod(1,sndidx).hist);
            meansndtraceZ=(meansndtrace-mean(meansndtrace))/std(meansndtrace);
        end
         if ~isempty(ripidx)&~isempty(sndidx)
             riprespsall=[riprespsall allripplemod(1,ripidx).varRespAmp2];
             sndrespsall=[sndrespsall allsoundmod(1,sndidx).varRespAmp2];
             riprespsallP=[riprespsallP allripplemod(1,ripidx).rasterShufP2];
             sndrespsallP=[sndrespsallP allsoundmod(1,sndidx).rasterShufP2];
             
             if allripplemod(1,ripidx).rasterShufP2<0.05&allsoundmod(1,sndidx).rasterShufP2<0.05   
             ripresps=[ripresps;meanriptraceZ];
             soundresps=[soundresps;meansndtraceZ];
             ripmodamp=[ripmodamp  mean(meanriptraceZ(40:80))];
    soundmodamp=[soundmodamp mean(meansndtraceZ(50:90))];
%              soundResp1=[soundResp1 allsoundmod(1,sndidx).rasterShufP2<0.05];
%              ripResp1=[ripResp1 allripplemod(1,ripidx).rasterShufP2<0.05];
%             % 1 for sound-excited, 0 otherwise
%              soundRespType=mean(meansndtrace(51:101))>mean(meansndtrace(1:50));
%              [r1 p1]=corrcoef(meansndtraceZ,meanriptraceZ);
%              if p1(1,2)<0.05
%              soundRipCorr=[soundRipCorr r1(1,2)];
%              else
%                soundRipCorr=[soundRipCorr nan];
% 
%              end
             end
         end
        
        h = fspecial('gaussian',20,2);

            
            
        if plotSoundRip
        figure('Position',[300 scrsz(4)/2 scrsz(3)/3 scrsz(4)/2])
        subplot(2,2,1)
        if ~isempty(sndidx)
            imagesc(x1,1:size(allsoundmod(1,sndidx).hist,1),allsoundmod(1,sndidx).hist)
            if allsoundmod(1,sndidx).rasterShufP2<0.05
                title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index) ' score=' num2str(allsoundmod(1,sndidx).varRespAmp2) '*'])
            else
                title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index) ' score=' num2str(allsoundmod(1,sndidx).varRespAmp2)])
                
            end
        end
        if ~isempty(ripidx)
            subplot(2,2,2)
 imagesc(x1,1:size(allripplemod(1,ripidx).hist,1),filter2(h,allripplemod(1,ripidx).hist))
 if allripplemod(1,ripidx).rasterShufP2<0.05
                title(['Ripmod, ind= ' num2str(allripplemod(1,ripidx).index) ' score=' num2str(allripplemod(1,ripidx).varRespAmp2) '*'])
            else
                title(['Ripmod, ind= ' num2str(allripplemod(1,ripidx).index) ' score=' num2str(allripplemod(1,ripidx).varRespAmp2)])
                
            end
        end
        if ~isempty(ripidx)
            subplot(2,2,3:4)
            plot(x1,meanriptraceZ,'linewidth',3);
            hold on;
        end
        if ~isempty(sndidx)
            subplot(2,2,3:4)
                       
            plot(x1,meansndtraceZ,'r','linewidth',3);
            legend('rip','snd')
            if saveImgs
            saveas(gcf,[savedirimgsX 'RipSoundCell' num2str(allsoundmod(1,sndidx).index),'.jpg'])
            end
            
            cursoundind=allsoundmod(1,sndidx).soundind;
            [sortedSounds sortedSoundind]=sort(cursoundind);
         figure('Position',[900 200 scrsz(3)/8 scrsz(4)/5])
        set(gcf,'PaperUnits','inches','PaperPosition',[0 0 9 9])
        
            subplot(1,2,1);
            %imagesc(x1,1:length(sortedSounds),allsoundmod(1,sndidx).hist);
            
            % similar to above, just not smoothed and 1ms res
             m=filter2(smoothwin,rast2mat(allsoundmod(1,sndidx).raster));

            imagesc(floor(1-m));colormap(gray)
            title(['Soundmod, ind= ' num2str(allsoundmod(1,sndidx).index)])
            
           % subplot(1,3,2);
            %imagesc(1-m(sortedSoundind,:));colormap(gray)
            %title(['sorted by sound ind'])

            [sortedSpeeds sortedSpeedsInd]=sort(allsoundmod(sndidx).speeds);
            subplot(1,2,2);
            %imagesc(x1,sortedSpeeds,allsoundmod(1,sndidx).hist(sortedSpeedsInd,:));
             % similar to above, just not smoothed and 1ms res
         %   m3=rast2mat(allsoundmod(1,sndidx).raster);
          %  m3=filter2(smoothwin,m3(sortedSpeedsInd,:));
            imagesc(floor(1-m(sortedSpeedsInd,:)));colormap(gray)
            title('sorted by speed')
            xlabel('Time (ms)')
            
            resp=allsoundmod(1,sndidx).hist;
            respAmpX=sum(resp(:,50:70),2);
            respAmpX=respAmpX/max(respAmpX);
            
                  saveas(gcf,[savedirimgsX 'SpeedSoundCellR' num2str(allsoundmod(1,sndidx).index),'.jpg'])
               %   saveas(gcf,[savedirimgsX 'SpeedSoundCellR' num2str(allsoundmod(1,sndidx).index),'.pdf'])
                  
                figure;plot(allsoundmod(sndidx).speeds,respAmpX,'ko');xlabel('speed');ylabel('tone response')
                  [qr qp]=corrcoef(allsoundmod(sndidx).speeds,respAmpX);
                  speedmodsQ=[speedmodsQ;qr(2,1) qp(2,1) allsoundmod(1,sndidx).rasterShufP2  mean(mean(resp(:,51:70)))/mean(mean(resp(:,1:40)))]
            % below is response vs. location
%               figure('Position',[900 200 scrsz(3)/8 scrsz(4)/5])
%         set(gcf,'PaperUnits','inches','PaperPosition',[0 0 9 9])
%         
%             colormap(hot);
%             c=colormap;
%             scatter(allsoundmod(sndidx).posX,allsoundmod(sndidx).posY,250,c(ceil(respAmpX*63+1),:),'filled')
%             %plotting the colored (>0) ones on top 
%             hold on;
%             posResp=find(respAmpX>0)
%             scatter(allsoundmod(sndidx).posX(posResp),allsoundmod(sndidx).posY(posResp),250,c(ceil(respAmpX(posResp)*63+1),:),'filled')
% colorbar
%          %   if saveImgs
%             saveas(gcf,[savedirimgsX 'SpeedSoundCell' num2str(allsoundmod(1,sndidx).index),'.jpg'])
%             saveas(gcf,[savedirimgsX 'SpeedSoundCell' num2str(allsoundmod(1,sndidx).index),'.pdf'])
%             saveas(gcf,[savedirimgsX 'SpeedSoundCell' num2str(allsoundmod(1,sndidx).index),'.fig'])
          %  end
        end
        
       % keyboard
       close all
        end
    end
   % keyboard
possndmod=speedmodsQ(speedmodsQ(:,4)>1&speedmodsQ(:,3)<0.05,:);
negsndmod=speedmodsQ(speedmodsQ(:,4)<=1&speedmodsQ(:,3)<0.05,:);
[h p]=ttest2(possndmod(:,1),negsndmod(:,1))
figure;barwitherr([std(possndmod(:,1))/sqrt(size(possndmod,1)) std(negsndmod(:,1))/sqrt(size(negsndmod,1))],[mean(possndmod(:,1)) mean(negsndmod(:,1))])
set(gca,'XTickLabel',{'Positively sound modulated','Negatively sound modulated'})
ylabel('movement modulation')

posmovmod=speedmodsQ(speedmodsQ(:,1)>0&speedmodsQ(:,3)<0.05,:);
negmovmod=speedmodsQ(speedmodsQ(:,1)<=0&speedmodsQ(:,3)<0.05,:);
[h p]=ttest2(posmovmod(:,4),negmovmod(:,4))
figure;barwitherr([std(posmovmod(:,4))/sqrt(size(posmovmod,1)) std(negmovmod(:,4))/sqrt(size(negmovmod,1))],[mean(posmovmod(:,4)) mean(negmovmod(:,4))])
set(gca,'XTickLabel',{'Positively movement modulated','Negatively movement modulated'})
ylabel('sound modulation')

figure;
subplot(2,2,1);
imagesc(soundresps);
ylabel('cells');
title('sound resps');
subplot(2,2,2);
imagesc(ripresps);
ylabel('cells');
xlabel('time')
title('rip resps');
% ripmodamp=mean(meanriptraceZ(40:80));
% soundmodamp=mean(meansndtraceZ(50:90));
% subplot(2,2,3);
% plot(ripmodamp,soundmodamp,'ko')
% ylabel('sound response')
% xlabel('ripple response')

subplot(2,2,3:4);
plot(ripmodamp,abs(soundmodamp),'ko')
[r p]=corrcoef(ripmodamp,abs(soundmodamp));
ylabel('absolute sound response')
xlabel('ripple response')
title(['r= ' num2str(r(2,1)) ' p= ' num2str(p(2,1))])
p=polyfit(ripmodamp,abs(soundmodamp),1);
f=polyval(p,-0.2:0.01:1.2);
hold on;plot(-0.2:0.01:1.2,f,'r','linewidth',2)

 set(0,'defaultaxesfontsize',28);
figure;plot(ripmodamp,abs(soundmodamp),'ko','markerfacecolor','k','markersize',8)
[r p]=corrcoef(ripmodamp,abs(soundmodamp));
ylabel('absolute sound response')
xlabel('ripple response')
title(['r= ' num2str(r(2,1)) ' p= ' num2str(p(2,1))])
p=polyfit(ripmodamp,abs(soundmodamp),1);
f=polyval(p,-0.2:0.01:1.2);
hold on;plot(-0.2:0.01:1.2,f,'r','linewidth',2)
set(gca,'box','off')
keyboard;
end