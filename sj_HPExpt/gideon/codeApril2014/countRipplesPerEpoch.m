numRips=NaN(17,10);
for day=8:17
    if day<10
load(['/data15/gideon/Ndl/Ndlripples0' num2str(day)])
        
    else
load(['/data15/gideon/Ndl/Ndlripples' num2str(day)])
    end
    for ep=1:10
        try
[riptimes] = getripples_direct([day, ep], ripples, [10:14],'minstd',6);
    numRips(day,ep)=size(riptimes,1);
        catch
            
        end
        
    end
end
%%
numRips2=numRips';
epochTypes2=epochTypes';
numRipsRun=numRips2;
numRipsRun(epochTypes2==0)=NaN;
numRipsSleep=numRips2;
numRipsSleep(epochTypes2==1)=NaN;

epochTypes=[-1*ones(7,10);0 1 0 1 0 1 0 -1 -1 -1;0 1 0 0 1 0 -1 -1 -1 -1;0 1 0 1 0 1 0 -1 -1 -1;repmat([0 1 0 1 0 1 0 1 0 -1],4,1);0 0 1 0 1 0 1 0 1 0;repmat([0 1 0 1 0 1 0 1 0 -1],2,1)];
overallMeanNumRipsRun=mean(numRips(epochTypes==1));
overallMeanNumRipsRun=mean(numRips(epochTypes==0));
figure;plot(numRips2(epochTypes2==0),'linewidth',2);
title('Sleep epochs','fontsize',20);xlabel('# epoch','fontsize',20);ylabel('#ripples','fontsize',20)
figure;plot(numRips2(epochTypes2==1),'linewidth',2);
title('Run epochs','fontsize',20);xlabel('# epoch','fontsize',20);ylabel('#ripples','fontsize',20)
figure;errorbar(1:17,nanmean(numRipsRun')',nanstd(numRipsRun),'linewidth',2)
title('Run epochs','fontsize',20);xlabel('Day','fontsize',20);ylabel('mean num ripples','fontsize',20)
xlim([6 18])
figure;errorbar(1:17,nanmean(numRipsSleep')',nanstd(numRipsSleep),'linewidth',2)
title('Sleep epochs','fontsize',20);xlabel('Day','fontsize',20);ylabel('mean num ripples','fontsize',20)
xlim([6 18])
figure;
for day=8:17,plot(numRips2(epochTypes(day,:)==0,day),'linewidth',2);hold on;end
title('Sleep epochs','fontsize',20);xlabel('Epoch in day','fontsize',20);ylabel('num ripples','fontsize',20)
figure;for day=8:17,plot(numRips2(epochTypes(day,:)==1,day),'linewidth',2);hold on;end
title('Run epochs','fontsize',20);xlabel('Epoch in day','fontsize',20);ylabel('num ripples','fontsize',20)
