%% adapted from the parallel script for Rosenthal.
% This is for Nadal, and here I currently only look at before vs after


 NDLaudtets=[2:7];
 NDLdays=[8:14]
 NDLepochs=[1 7;1 6;1 7;1 9;1 9;1 9;1 9]

 

allRespAmpsBef={};
allRespAmpsAft={};
epochCounter=1;

for dayx=[NDLdays]
        
if dayx<10
    daystr=['0' num2str(dayx)];
else
    daystr=num2str(dayx);
end

 [stimEEGtimesBef stimIndsBef whichStimBef]=findStimComplexSounds('/opt/data15/gideon/Ndl/EEG/Ndleeg', daystr,'1','22');
 [stimEEGtimesAft stimIndsAft whichStimAft]=findStimComplexSounds('/opt/data15/gideon/Ndl/EEG/Ndleeg', daystr,num2str(NDLepochs(epochCounter,2)),'22');
 
 [respAmpsBef respAmpsAft]=compareBefAftrResponsesSpikesSBmulti('/opt/data15/gideon/Ndl/EEG/Ndleeg',daystr,'1',num2str(NDLepochs(epochCounter,2)),NDLaudtets, '/opt/data15/gideon/Ndl/Ndlmulti',stimEEGtimesBef,stimIndsBef,whichStimBef,stimEEGtimesAft,stimIndsAft,whichStimAft)
 epochCounter=epochCounter+1;
 

allRespAmpsBef{dayx}=respAmpsBef;
allRespAmpsAft{dayx}=respAmpsAft;
close all;
clear stimIndsTrack stimEEGTimesTrack stimEEGtimesSB stimEEGtimesBef stimEEGtimesAft
cc=1;
end
 %save /opt/data15/gideon/allaudrespsbefaftdataNADAL

%%
% load /opt/data15/gideon/allaudrespsbefaftdataNADAL

%% this looks at response to target sound (pre, dur post), and BBN (pre, post) (AC tets)
% over days. It doesn't show anything striking, tetrodes are very different
% more systematic analysis in next section
 NDLdays=[8:14]

tetXtargetBef=[];
tetXtargetAft=[];
tetXtargetDur=[];
% choose tet here
tetX=1;
for i=NDLdays, 
    tetXtargetBef=[tetXtargetBef allRespAmpsBef{i}(tetX,1)];
 tetXtargetAft=[tetXtargetAft allRespAmpsAft{i}(tetX,1)];
end

figure;
subplot(2,1,1)
plot(NDLdays,tetXtargetBef)
hold on
plot(NDLdays,tetXtargetAft,'r')

subplot(2,1,2)
tetXBBNBef=[];
tetXBBNAft=[];
tetXBBNDur=[];


for i=NDLdays, tetXBBNBef=[tetXBBNBef allRespAmpsBef{i}(tetX,4)];end
for i=NDLdays, tetXBBNAft=[tetXBBNAft allRespAmpsAft{i}(tetX,4)];end
%for i=[1,2,4:12], tetXBBNDur=[tetXBBNDur allRespAmpsDur{i}(tetX)];end

plot(NDLdays,tetXBBNBef)
hold on
plot(NDLdays,tetXBBNAft,'r')
%plot([1,2,4:12],tetXBBNDur,'k')
%% does activity on the track predict enhancement of post-pre? (AC tets)
% currently activity on track predicts the difference post-pre for the
% target sound. It doesn't predict change in pre-post for BBN and pure
% tones (as expected). It does for down chirps.
% looking only at days where the target was up-chirps (1-9)
tetXtargetBef=[];
tetXtargetAft=[];
tetXtargetDur=[];
tetXDist1Bef=[];
tetXDist1Aft=[];
tetXDist2Bef=[];
tetXDist2Aft=[];
tetXBBNBef=[];
tetXBBNAft=[];

for tetX=1:6
for days1=NDLdays, 
    tetXtargetBef=[tetXtargetBef allRespAmpsBef{days1}(tetX,1)];
 tetXtargetAft=[tetXtargetAft allRespAmpsAft{days1}(tetX,1)];
 tetXDist1Bef=[tetXDist1Bef allRespAmpsBef{days1}(tetX,2)];
 tetXDist1Aft=[tetXDist1Aft allRespAmpsAft{days1}(tetX,2)];
  tetXDist2Bef=[tetXDist2Bef allRespAmpsBef{days1}(tetX,3)];
 tetXDist2Aft=[tetXDist2Aft allRespAmpsAft{days1}(tetX,3)];
 tetXBBNBef=[tetXBBNBef allRespAmpsBef{days1}(tetX,4)];
 tetXBBNAft=[tetXBBNAft allRespAmpsAft{days1}(tetX,4)];
 
end
end


%%
% are responses larger after? (AC tets)
figure;
subplot(1,4,1)
plot(tetXtargetBef,tetXtargetAft,'ko')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXtargetBef,tetXtargetAft,0.05,'left')
title(['target, P=' num2str(p)])

subplot(1,4,2)
plot(tetXDist1Bef,tetXDist1Aft,'ko')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXDist1Bef,tetXDist1Aft,0.05,'left')
title(['down chirp, P=' num2str(p)])

subplot(1,4,3)
plot(tetXDist2Bef,tetXDist2Aft,'ko')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXDist2Bef,tetXDist2Aft,0.05,'left')
title(['pure tones, P=' num2str(p)])

subplot(1,4,4)
plot(tetXBBNBef,tetXBBNAft,'ko')
hold on
xx=[0:20];
plot(xx,xx,'r')
xlabel('before')
ylabel('after')
[h p]=ttest(tetXBBNBef,tetXBBNAft,0.05,'left')
title(['BBN, P=' num2str(p)])

%% change in response amplitude over the week
resps1bef=[];
resps2bef=[];
resps3bef=[];
resps4bef=[];

for tetX=1:6
for days1=NDLdays
    resps1bef(tetX,days1-7)= allRespAmpsBef{days1}(tetX,1);
    resps2bef(tetX,days1-7)= allRespAmpsBef{days1}(tetX,2);
    resps3bef(tetX,days1-7)= allRespAmpsBef{days1}(tetX,3);
    resps4bef(tetX,days1-7)= allRespAmpsBef{days1}(tetX,4);

    
end
end

resps1aft=[];
resps2aft=[];
resps3aft=[];
resps4aft=[];

for tetX=1:6
for days1=NDLdays
    resps1aft(tetX,days1-7)= allRespAmpsAft{days1}(tetX,1);
    resps2aft(tetX,days1-7)= allRespAmpsAft{days1}(tetX,2);
    resps3aft(tetX,days1-7)= allRespAmpsAft{days1}(tetX,3);
    resps4aft(tetX,days1-7)= allRespAmpsAft{days1}(tetX,4);

    
end
end
% to target sound
figure;
subplot(4,1,1)
plot(NDLdays,mean(resps1bef),'b')
hold on;
plot(NDLdays,mean(resps1aft),'r')

% to other sounds
subplot(4,1,2)
plot(NDLdays,mean(resps2bef),'b')
hold on;
plot(NDLdays,mean(resps2aft),'r')


subplot(4,1,3);
plot(NDLdays,mean(resps3bef),'b')
hold on;
plot(NDLdays,mean(resps3aft),'r')


subplot(4,1,4)
plot(NDLdays,mean(resps3bef),'b')
hold on;
plot(NDLdays,mean(resps3aft),'r')

%%
% different sounds, bef
figure;hold on

plot(NDLdays,mean(resps1bef),'k')
plot(NDLdays,mean(resps2bef),'b')
plot(NDLdays,mean(resps3bef),'r')
plot(NDLdays,mean(resps4bef),'g')

% different sounds, aft
figure;hold on

plot(NDLdays,mean(resps1aft),'k')
plot(NDLdays,mean(resps2aft),'b')
plot(NDLdays,mean(resps3aft),'r')
plot(NDLdays,mean(resps4aft),'g')

% save /opt/data15/gideon/allaudrespsbefaftdataNADAL

