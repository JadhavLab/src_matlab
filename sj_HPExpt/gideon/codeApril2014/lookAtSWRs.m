for day=12:12
    for epoch=([3 5 7 9]+1)
        try

if day<10,dayStr=['0' num2str(day)];else dayStr=[num2str(day)];end
tets=[8,9,10,13];
tetsStr={'08','09','10','13'};

figure
colors={'k','r','g','b','m'}
for i=1:length(tetsStr)
   load(['/opt/data15/gideon/Rtl/EEG/Rtleeg' dayStr '-' num2str(epoch) '-' tetsStr{i} '.mat'])
   plot(eeg{1,day}{1,epoch}{1,tets(i)}.data(:,1)/std(eeg{1,day}{1,epoch}{1,tets(i)}.data(:,1))+i*10,'color',colors{i});
   hold on
   load(['/opt/data15/gideon/Rtl/Rtlripples' dayStr '.mat'])
    hold on
    rips=ripples{1,day}{1,epoch}{1,tets(i)}.startind';
    line([rips;rips]+i, [zeros(1,length(rips));50*ones(1,length(rips))],'color',colors{i})

end
title(['day= ' num2str(day) ' epoch= ' num2str(epoch)]);
        end
    end
end
