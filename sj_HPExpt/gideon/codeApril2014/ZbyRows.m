function MatZ=ZbyRows(Mat)
MatZ=NaN(size(Mat));
for i=1:size(Mat,1)
    MatZ(i,:)=zscore(Mat(i,:));
end