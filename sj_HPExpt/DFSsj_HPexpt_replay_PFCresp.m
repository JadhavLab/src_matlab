
% Use similar filter and function conditions as DFSsj_HPexpt_replay, to get rasters and histograms 
% aligned to ripple events (and candidate replay events) for PFC ripmod cells
% Use candripples structure, which has times that you need

% Have to use multicellanal



clear; %close all;
runscript = 1;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells

savedir = '/data25/sjadhav/HPExpt/ProcessedData/';

% With Speed criterion - Version 4 onwards
%--------------------------------------
val=1; savefile = [savedir 'HP_replay5_PFCresp']; area = 'PFC'; clr = 'b'; % PFC ripmod cells
% Single day
% ----------
%val=2; savefile = [savedir 'HPa_day1_replay5_PFCresp'], area = 'PFC'; clr = 'b'; % PFC ripmod cells

% No speed criterion - Original
% -------------------------------
%val=3; savefile = [savedir 'HP_replay_PFCresp'], area = 'PFC'; clr = 'b'; % PFC ripmod cells
%val=4; savefile = [savedir 'HPa_day1_replay_PFCresp'], area = 'PFC'; clr = 'b'; % PFC ripmod cells


savefig1=0;


% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    switch val
        case {1,3}
            animals = {'HPa','HPb','HPc','Ndl'}
        case {2,4}
            animals = {'HPa'}
    end
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    switch val
        case 1
            dayfilter = [];
        case 2
            dayfilter = '1'; % Shantanu - I am adding day filter to parse out epoch filter
    end
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    %sleepepochfilter = 'isequal($type, ''sleep'')'; % Only pre and post sleep marked as sleep
    
    % %Cell filter
    % %-----------
    % %PFC
    % %----
    % && strcmp($thetamodtag, ''y'')
    switch val
        case {1,2,3,4}
            PFCcellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag2, ''y'')';           
    end
    
    % Iterator
    % --------
    iterator = 'multicellanal';
    
    % Filter creation
    % ----------------
    switch val
        case {1,3}
            PFCf = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
                PFCcellfilter, 'iterator', iterator);
        case {2,4}
            PFCf = createfilter('animal',animals,'days',dayfilter,'epochs',runepochfilter, 'cells',...
                PFCcellfilter, 'iterator', iterator);
    end
    
    disp('Done Filter Creation');
    
    % Set analysis function
    % ---------------------
    
    switch val
        case {1,2}
            PFCf = setfilterfunction(PFCf,'DFAsj_getreplay_PFCresp',{'spikes','cand5ripples'}); %
        case {3,4}
            PFCf = setfilterfunction(PFCf,'DFAsj_getreplay_PFCresp',{'spikes','candripples'}); %
    end
    
    % Run analysis
    % ------------
    PFCf = runfilter(PFCf);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------

% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
% gatherdata = 1; savegatherdata = 1;
% switch val
%     case 1
%         gatherdatafile = [savedir 'HP_replay5_PFCresp_gather'];
%     case 2
%         gatherdatafile = [savedir 'HPa_day1_replay5_PFCresp_gather'];
% end



keyboard;


% 
% 
% 
% 
% 
% if gatherdata
%     % Parameters if any
%     % -----------------
%     % -------------------------------------------------------------
%     
%     cnt=0; % How many kept? Any condition?
%     %Glm
%     CA1idxs = []; CA1PoplnResp = []; CA1numcells=[]; CA1anim_index=[];
%     %PFC
%     PFCidxs=[]; PFCtrialResps=[]; PFCanim_index=[];
%     
%     
%     for an = 1:length(modf)
%         for i=1:length(modf(an).output{1})
%             % Check for empty output - If Cell defined in epoch and Nspks in ripple response wndow > 0
%             if ~isempty(modf(an).output{1}(i).indices)
%                 cnt=cnt+1;
%                 CA1anim_index{an}{i} = modf(an).output{1}(i).indices;
%                 % Only indexes - Put animal index in front
%                 currCA1idxs=[];
%                 ncurridxs = size(modf(an).output{1}(i).indices,1); % No of idxs
%                 an_tmp = an*ones(ncurridxs,1);
%                 currCA1idxs = [an_tmp, modf(an).output{1}(i).indices];
%                 CA1idxs = [CA1idxs; currCA1idxs];
%                 % CA1 Pop Resp
%                 CA1PoplnResp{cnt} = modf(an).output{1}(i).PoplnResp;
%                 CA1numcells(cnt) = modf(an).output{1}(i).nCA1cells;
%                 
% %                 % WRONG
% %                 % PFC - will be similar indexing
% %                 PFCanim_index{an}{i} = PFCf(an).output{1}(i).index;
% %                 PFCidxs(cnt,:) = [an PFCf(an).output{1}(i).index];
% %                 PFCtrialResps{cnt} = PFCf(an).output{1}(i).trialResps;
%                 
%             end
%         end
%         
%     end
%     
%     % ----------
%     % Combine across epochs - based on PFC idxs
%     % ---------
% 
%     % Have to sort PFCidxs in order of day-epoch, since that is how CA1 Popln Resp is arranged - 
%     % simply in terms of day epoch
%     
%     
%     cntcells = 0;
%     uniqueIndices = unique(PFCidxs(:,[1 2 4 5]),'rows'); % Collapse across epochs
%     % iterating only over the unique indices and finding matches in allindexes
%     
%     for i=1:length(uniqueIndices)
%         cntcells = cntcells+1;
%         curridx = uniqueIndices(i,:);
%         ind=find(ismember(PFCidxs(:,[1 2 4 5]),curridx,'rows'))';
%         
%         currPFCtr=[]; currCA1pr=[]; nc=[];
%         % PFC
%         for r=ind
%             currPFCtr = [currPFCtr; PFCtrialResps{r}];
%             currCA1pr = [currCA1pr; CA1PoplnResp{r}];
%             nc = [nc, CA1numcells(r)];
%         end
%         
%         % Save both in struct, and variable format
%         allPFCidxs(cntcells,:) = curridx;
%         allPFCtrialResps{cntcells} = currPFCtr;
%         allCA1PoplnResps{cntcells} = currCA1pr;
%         allCA1numcells(cntcells) = mean(nc);
%         
%         allPoplnResp(cntcells).PFCidx = curridx;
%         allPoplnResp(cntcells).PFCtrialResps = currPFCtr;
%         allPoplnResp(cntcells).allCA1PoplnResps = currCA1pr;
%         allPoplnResp(cntcells).allCA1numcells = mean(nc);
%     end
%     
%     % Get Corrln between CA1 Poln Resp and PFC trialResp.
%     % Can Also arrange in order of strength, which makes more sense for full histogram, not just trialResps
%     
%     for i=1:cntcells
%         
%         [r,p] = corrcoef(allPFCtrialResps{cntcells}, allCA1PoplnResps{cntcells});
%         allr(cntcells) = r(1,2);
%         allp(cntcells) = p(1,2);
%         
%     end
%     
%     % Save
%     % -----
%     if savegatherdata == 1
%         save(gatherdatafile);
%     end
%     
% else % gatherdata=0
%     
%     load(gatherdatafile);
%     
% end % end gather data
% 
% 
% % ------------
% % PLOTTING, ETC
% % ------------
% 
% % ------------------
% % Population Figures
% % ------------------
% 
% forppr = 0;
% % If yes, everything set to redimscreen_figforppr1
% % If not, everything set to redimscreen_figforppt1
% figdir = '/data25/sjadhav/HPExpt/Figures/ThetaMod/';
% summdir = figdir;
% set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
% 
% if forppr==1
%     set(0,'defaultaxesfontsize',16);
%     tfont = 18; % title font
%     xfont = 16;
%     yfont = 16;
% else
%     set(0,'defaultaxesfontsize',24);
%     tfont = 28;
%     xfont = 20;
%     yfont = 20;
% end
% 
% % Skip NaNs
% rem = find(isnan(r));
% allr(rem) = []; allp(rem) = [];
% sig= find(allp < 0.05);
% 
% 
% 
% figure; hold on;redimscreen_figforppt1;
% plot(allr, 'k.','MarkerSize',24);
% plot(allr(sig), 'r.','MarkerSize',24);
% title(sprintf('CC - PFc tr and CA1 pr'),'FontSize',24,'Fontweight','normal');
% xlabel(['Corr Coeff'],'FontSize',24,'Fontweight','normal');
% ylabel(['PFC cell no'],'FontSize',24,'Fontweight','normal');
% legend('All Cells','Sig Corr');
% %text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');
% 
% 
% 
% 
% keyboard;
% 
% 
% 
