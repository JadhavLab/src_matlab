function out = DFAsj_getripPoplnResp2(indices, excludetimes, ripplemod, cellinfo, varargin)

% Unlike ver1, indices are CA1 cells and PFC cells. So need to parse

tetfilter = '';
dospeed = 0;
excludetimes = [];
acrossregions = 0;
for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        case 'acrossregions'
            acrossregions = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

day = indices(1,1);
epoch = indices(1,2);

cellsi=[]; usecellsi=0; % CA1 cells
cellsp=[]; usecellsp=0; % PFC cells

totalcells = size(indices,1);
for i=1:totalcells
    currarea = cellinfo{indices(i,1)}{indices(i,2)}{indices(i,3)}{indices(i,4)}.area;
    if strcmp(currarea,'PFC'),
        cellsp = [cellsp; day epoch indices(i,3) indices(i,4)];
        usecellsp = usecellsp+1;
    else
        cellsi = [cellsi; day epoch indices(i,3) indices(i,4)];
        usecellsi = usecellsi+1;
    end
end
nCA1cells = size(cellsi,1); nPFCcells = size(cellsp,1);
    
% ------------------
% Parse ripplemoddata - don;t really need to do this, but following format from sj_glm_ripalign1
for i=1:size(cellsi,1)
    i;
    eval(['ripplemodi{',num2str(i),'}= ripplemod{cellsi(',num2str(i),',1)}{cellsi(',num2str(i),',2)}'...
        '{cellsi(',num2str(i),',3)}{cellsi(',num2str(i),',4)}.trialResps;']);
end

for i=1:size(cellsp,1)
    i;
    eval(['ripplemodp{',num2str(i),'}= ripplemod{cellsp(',num2str(i),',1)}{cellsp(',num2str(i),',2)}'...
        '{cellsp(',num2str(i),',3)}{cellsp(',num2str(i),',4)}.trialResps;']);
end  


% Get CA1 Popln resp
% -------------------
Xmat = [];
for i=1:size(cellsi,1)
    eval(['currResps = ripplemodi{',num2str(i),'};']);
    Xmat = [Xmat, currResps]; % Rows are observations, Columns are triaslResps for current CA1 cell
end

PoplnResp = sum(Xmat,2); % Sum across CA1 cells to get popln resp


% For each PFC cell, get trialResp. Save in matrix format for current epoch
% ---------------------------------
alltrialResps = [];
for pp=1:size(cellsp,1)
     eval(['y = ripplemodp{',num2str(pp),'};']); % PFC cell
    alltrialResps = [alltrialResps,y];  % Columns are PFC cells, rows are observations
end

% Repeat CA1 Popln Resp to make it equal to no. of PFC cells
allPoplnResp = repmat(PoplnResp, 1, nPFCcells);
% Get PFC Popln Resp as well, similar to CA1 PoplnResp
PFCPoplnResp = sum(alltrialResps,2);


% % ------ 
% % Output
% % ------
out.CA1indices = cellsi;
out.PoplnResp = PoplnResp;
out.nCA1cells = nCA1cells;
out.allPoplnResp = allPoplnResp;

out.PFCindices = cellsp;
out.nPFCcells = nPFCcells;
out.alltrialResps = alltrialResps;
out.PFCPoplnResp = PFCPoplnResp;
%out.Xmat = Xmat;




