function [ allmodelfits ] = sj_glm_ripalign1(prefix, day, epoch, PFCtet, PFCcell)
%sj_glm_ripalign1 Glm model for PFC rip-align response as a function of rip-aligned CA1 popln responses
%   Try this out before filter framework. Goal is to get coefficients and significance
% Get ripple aligned responses directly from ripplemod structure
% allmodelfits = sj_glm_ripalign1('HPa', 1, 4);

if nargin<4
    PFCtet=[]; PFCcell=[];
end

% Starting stuff
switch prefix
    case 'HPa'
        directoryname = '/data25/sjadhav/HPExpt/HPa_direct';
        animdirect = directoryname;
        dire = '/data25/sjadhav/HPExpt/HPa';
        riptetlist = [1,4,5,6,7,8,9,11,12,14];
        maineegtet = 1; maineegidx=1; % CA1 tet
        peegtet = 16; % PFCtet
    case 'HPb'
        directoryname = '/data25/sjadhav/HPExpt/HPb_direct';
        animdirect = directoryname;
        dire = '/data25/sjadhav/HPExpt/HPb';
        riptetlist = [1,3,4,5,6,16,17,18,20];
        maineegtet = 1; maineegidx=1; % CA1 tet
        peegtet = 9; % PFCtet
end

currdir = pwd;
if (directoryname(end) == '/')
    directoryname = directoryname(1:end-1);
end
if (dire(end) == '/')
    dire = dire(1:end-1);
end
if (day < 10)
    daystring = ['0',num2str(day)];
else
    daystring = num2str(day);
end
animdirect = directoryname;

% Variables
respwin = [0 200]; % You can take trialResps directly from ripplemod structure, or re-generate it from the raster here

% Get data files
% Spike data
%-----------

cellinfofile = sprintf('%s/%scellinfo.mat', directoryname, prefix);
load(cellinfofile);
ripplemodfile = sprintf('%s/%sripplemod%02d.mat', animdirect, prefix, day);
load(ripplemodfile);
% spikefile = sprintf('%s/%sspikes%02d.mat', animdirect, prefix, day);
% load(spikefile);
% tetinfofile = sprintf('%s/%stetinfo.mat', directoryname, prefix);
% load(tetinfofile);
% ripfile = sprintf('%s/%sripples%02d.mat', directoryname, prefix, day);
% load(ripfile);
% posfile = sprintf('%s/%spos%02d.mat', animdirect, prefix, day);
% load(posfile);
% linposfile = sprintf('%s/%slinpos%02d.mat', animdirect, prefix, day);
% load(linposfile);



% Get cells
% ---------
% CA1 cells
%filterString = '( ($meanrate > 0.1) && ~strcmp($tag, ''iCA1Int'') && ~strcmp($tag, ''CA1Int'') && (strcmp($tag2, ''CA1Pyr'') || strcmp($tag2, ''iCA1Pyr'')) ) ';
filterString = '(strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7)';
cellindices = evaluatefilter(cellinfo{day}{epoch}, filterString);
cellsi = [repmat([day epoch], size(cellindices,1),1 ), cellindices]; % day-epoch-tet-cell for CA1 cells
usecellsi = 1:size(cellsi,1); nCA1cells = size(cellsi,1);

% PFC cell: CAn do one cell, or all ripplomodcells in epoch?
if ~isempty(PFCtet),
    cellsp = [day epoch PFCtet PFCcell];
    usecellsp = 1;
else
    %filterString = 'strcmp($area, ''PFC'') && strcmp($ripmodtag, ''y'')';
    filterString = 'strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag2, ''y'') ';
    pcellindices = evaluatefilter(cellinfo{day}{epoch}, filterString);
    cellsp = [repmat([day epoch], size(pcellindices,1),1 ), pcellindices]; % day-epoch-tet-cell for CA1 cells
    usecellsp = 1:size(cellsp,1); nPFCcells = size(cellsp,1);
    
end

% Get ripplemoddata
for i=1:size(cellsi,1)
    i;
    eval(['ripplemodi{',num2str(i),'}= ripplemod{cellsi(',num2str(i),',1)}{cellsi(',num2str(i),',2)}'...
        '{cellsi(',num2str(i),',3)}{cellsi(',num2str(i),',4)}.trialResps;']);
end

for i=1:size(cellsp,1)
    i;
    eval(['ripplemodp{',num2str(i),'}= ripplemod{cellsp(',num2str(i),',1)}{cellsp(',num2str(i),',2)}'...
        '{cellsp(',num2str(i),',3)}{cellsp(',num2str(i),',4)}.trialResps;']);
%     eval(['spiketimep{',num2str(i),'}= spikes{cellsp(',num2str(i),',1)}{cellsp(',num2str(i),',2)}'...
%         '{cellsp(',num2str(i),',3)}{cellsp(',num2str(i),',4)}.data(:,1);']);
end

% Get glmfits for the model using Poisson distr, which uses log link function by default:
% PFC trialresps(y), X = constant and CA1 trialResps, (log(u)) = Xb

% X will be the same for all PFC cells
Xmat = [];
for i=1:size(cellsi,1)
    eval(['currResps = ripplemodi{',num2str(i),'};']);
    Xmat = [Xmat, currResps]; % Rows are observations, Columns are triaslResps for current CA1 cell
end
% Add a column of constant at beginning 
% DONT. glm automatically does it
% nobs = size(Xmat,1); 
% constants = ones(nobs,1); Xmat = [constants, Xmat];

allmodelfits = []; allmodelb = []; allmodelp=[]; allidxs = [];
% For each PFC cell
for i=1:size(cellsp,1)
    cellsp(i,:)
    eval(['y = ripplemodp{',num2str(i),'};']);
    [b, ~, stats] = glmfit(Xmat,y,'poisson');
    allmodelfits(i).stats = stats;
    allmodelb = [allmodelb; b(2:end)]; % Coefficients. Same as stats.beta
    allmodelp = [allmodelp; stats.p(2:end)];  
    
    % Save index as [day epoch CA1tet CA1cell PFCtet PFCcell]
    PFCdet = repmat([cellsp(i,3), cellsp(i,4)], nCA1cells,1 );
    curridxs = [cellsi,PFCdet];
    allidxs = [allidxs; curridxs];   
    
    % For each PFC neuron, what fraction of CA1 cells were significantly predictive - positive or negative?
    currsig = find(stats.p(2:end) < 0.05);
    nsig(i) = length(currsig);
    fracsig(i) = nsig(i)./nCA1cells;
    bsig = b(currsig+1);
    nsigpos(i) = length(find(bsig>0)); fracsigpos(i) = nsigpos(i)./nCA1cells;
    nsigneg(i) = length(find(bsig<0)); fracsigneg(i) = nsigneg(i)./nCA1cells;
    
end

% Get pairwise correlations between CA1 and PFC - to compare to model coeffs
% Skip getting significance from shuffling for now.
corridxs = []; rcorr = []; pcorr = []; nsimul = [];
for pp=1:size(cellsp,1)
     eval(['y = ripplemodp{',num2str(pp),'};']); % PFC cell
     for ii=1:size(cellsi,1)
         eval(['x = ripplemodi{',num2str(ii),'};']); % CA1 cell
         [r, p] = corrcoef(x,y);
         rcorr = [rcorr; r(1,2)];
         pcorr = [pcorr; p(1,2)];
         corridxs = [corridxs; day epoch cellsi(ii,3) cellsi(ii,4) cellsp(pp,3) cellsp(pp,4)];
         
         % Get number of "trials/ripples" with co-occurences as well
         nsimul = [nsimul; length(find((x~=0) & (y~=0)))];
     end
end

% Idxs for allidxs and corrodxs are exactly matched. So you can just take values from glmfits and corrcef directly

% Skip bad fits, and corrlns. where no. of co-occurences are <10
rem = find( ((allmodelb>1) | (allmodelb<-1)) && allmodelp > 0.99); % Corresponding p will be 1
rem2 = find(nsimul<10)
allrem = union(rem, rem2);
allmodelb(allrem)=[]; allmodelp(allrem)=[]; rcorr(allrem)=[]; pcorr(allrem)=[]; allidxs(allrem,:)=[]; corridxs(allrem,:)=[];
sigglm = find(allmodelp < 0.05);
sigcorr = find(pcorr < 0.05);

% Sig GLM vs Sig Corr
glmvec = zeros(size(allmodelb)); glmvec(sigglm)=1;
corrvec = zeros(size(allmodelb)); corrvec(sigcorr)=1;
[rvec,pvec] = corrcoef(glmvec,corrvec)


figure; hold on;redimscreen_figforppt1; 
plot(rcorr, allmodelb, 'k.','MarkerSize',24);
plot(rcorr(sigglm), allmodelb(sigglm), 'r.','MarkerSize',24);
plot(rcorr(sigcorr), allmodelb(sigcorr), 'bo','MarkerSize',20);
title(sprintf('GLM fits vs Corr Coeff'),'FontSize',24,'Fontweight','normal');
xlabel(['Corr Coeff'],'FontSize',24,'Fontweight','normal');
ylabel(['GLM coeffs'],'FontSize',24,'Fontweight','normal');
legend('All Pairs','Sig GLM','Sig Corr');

[rall,pall] = corrcoef(rcorr,allmodelb) 
[rglm,pglm] = corrcoef(rcorr(sigglm),allmodelb(sigglm)) 
[rc,pc] = corrcoef(rcorr(sigcorr),allmodelb(sigcorr))

figure; hold on;redimscreen_figforppt1; 
plot(abs(rcorr), abs(allmodelb), 'k.','MarkerSize',24);
plot(abs(rcorr(sigglm)), abs(allmodelb(sigglm)), 'r.','MarkerSize',24);
plot(abs(rcorr(sigcorr)), abs(allmodelb(sigcorr)), 'bo','MarkerSize',20);
title(sprintf('ABS GLM fits vs Corr Coeff'),'FontSize',24,'Fontweight','normal');
xlabel(['Corr Coeff'],'FontSize',24,'Fontweight','normal');
ylabel(['GLM coeffs'],'FontSize',24,'Fontweight','normal');
legend('All Pairs','Sig GLM','Sig Corr');

keyboard;




         
         
    











