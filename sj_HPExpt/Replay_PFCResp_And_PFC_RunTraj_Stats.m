
% Get output from DFSsj_HPexpt_getreplaydata_withPFCHist.m, and compare
% with compare decoded trajectory to the Corresponding Traj/GlobalPETraj of PFC neuron, and see if that predicts/ correlates with PFC Response

% Similar to Replay_PFCResp_And_PFCProp, which plotted arm_index

clear;

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/Replay2015/';
cd /data25/sjadhav/HPExpt/HP_ProcessedData/;

respidx = [50:70]; % 0:200
bckidx = [1:40]; % -500:-100
binsize = 10; % ms
pret=500; postt=500; %% Times to plot
rwin = [0 200];
nstd = round(binsize*2/binsize); g1 = gaussian(nstd, 3*nstd+1); % nstd=1: gaussian of length 4. nstd = 2: gaussian of length 7, nstd=3: gaussian of length 10.

smwin=6;

figdir = '/data25/sjadhav/HPExpt/Figures/Replay2015/';
figcell=1; savefig1=0;
set(0,'defaultaxesfontsize',20);

val=2;
gatherdata = 1; savegatherdata = 0;
switch val
    case 1
        gatherdatafile = [savedir 'HP_replaydata_PFCrespHist_DecodeBestTraj_Stats_gather_X6']; % for saving current stuff
    case 2
        gatherdatafile = [savedir 'HP_replaydata_PFCrespHist_DecodeBestTraj_Stats4_gather_X6']; % for saving current stuff
end


if gatherdata
    
    replaydir = '/data25/sjadhav/HPExpt/HP_ProcessedData/Replay2015/';
    switch val
        case 1
            load([replaydir,'HP_replaydata_PFCallHist_proc_DecodeTraj_BestTraj_Stats5_X6.mat'],'decodefilter'); % cellthresh = 5
        case 2
            load([replaydir,'HP_replaydata_PFCallHist_proc_DecodeTraj_BestTraj_Stats_X6.mat'],'decodefilter'); % cellthresh = 4?
    end
    
    % Parameters if any
    % ------------------------------------------------------------------------------
    
    cnt=0; % How many PFC cells, separately in epochs
    cntalleps=0;
    %CA1
    CA1replay_arm_index1=[]; CA1replay_arm_index2=[];  curr_PFChist_Valid_epoch=[]; curr_PFCspks_Valid_epoch=[];
    curr_replay_arm_index1_Valid=[]; curr_replay_arm_index2_Valid=[];
    curr_GLobalDecodeTraj = []; CA1GLobalDecodeTraj = []; 
    curr_BestDecodeTraj = []; CA1BestDecodeTraj = []; 
    curr_BestTraj = []; CA1BestTraj = []; % Traj 1 or Traj 2
    curr_pval=[];
    %PFC
    PFCidxs=[]; PFCtype=[]; PFChist_Valid=[]; PFCspks_Valid=[];
    
    % Go over epochs, and extract replay data and PFC resp
    for an = 1:length(decodefilter)
        for i=1:length(decodefilter(an).output{1})
            if ~isempty(decodefilter(an).output{1}(i).GlobalEpochIndex)
                cntalleps=cntalleps+1;
                % Variables for entire epoch
                GlobalEpochIndex(cntalleps,:) = decodefilter(an).output{1}(i).GlobalEpochIndex;
                curr_replay_arm_index1_Valid = decodefilter(an).output{1}(i).replay_arm_index1_Valid;
                curr_replay_arm_index2_Valid = decodefilter(an).output{1}(i).replay_arm_index2_Valid;
                curr_PFChist_Valid_epoch = decodefilter(an).output{1}(i).PFChist_Valid;
                curr_PFCspks_Valid_epoch = decodefilter(an).output{1}(i).PFCspks_Valid;
                curr_GlobalDecodeTraj = decodefilter(an).output{1}(i).GlobalDecodeTrajNorm;
                curr_BestDecodeTraj = decodefilter(an).output{1}(i).BestDecodeTrajNorm;
                curr_BestTraj = decodefilter(an).output{1}(i).BestTraj;
                curr_pval = decodefilter(an).output{1}(i).pvalue_Valid;
                %if ~isempty(curr_pval)
                %    curr_pval = curr_pval(1:length(curr_BestTraj));
                %end
                
                
                curr_GlobalPFCindices = decodefilter(an).output{1}(i).GlobalPFCIndices;
                curr_PFCtype = decodefilter(an).output{1}(i).PFCtype;
                % Separate out by PFC cells - indices for cells in epoch
                currnPFCells = size(curr_GlobalPFCindices ,1);
                for c = 1:currnPFCells
                    cnt = cnt+1;
                    PFCidxs(cnt,:) = curr_GlobalPFCindices(c,:);
                    PFCtype(cnt) = curr_PFCtype(c);
                    if ~isempty(curr_PFChist_Valid_epoch)
                        PFChist_Valid{cnt} = curr_PFChist_Valid_epoch{c}; % For each cell, this is a matrix
                    else % No PFC spikes in epoch
                        PFChist_Valid{cnt} = zeros(length(curr_replay_arm_index1_Valid),101);
                    end
                    if ~isempty(curr_PFChist_Valid_epoch)
                        PFCspks_Valid{cnt} =  curr_PFCspks_Valid_epoch{c}; % For each cell, this is a cell array
                    else
                        PFCspks_Valid{cnt}=[];
                    end
                    % The following is same for entire epoch, but save separately for each cell anyway
                    CA1replay_arm_index1{cnt} =  curr_replay_arm_index1_Valid; % Same for entire epoch
                    CA1replay_arm_index2{cnt} =  curr_replay_arm_index2_Valid;
                    % BestDecodeTraj is also same for entire epoch, but save separately for each PFC cells
                    CA1GlobalDecodeTraj{cnt} = curr_GlobalDecodeTraj;
                    CA1BestDecodeTraj{cnt} = curr_BestDecodeTraj;
                    CA1BestTraj{cnt} = curr_BestTraj;
                    CA1replay_pval{cnt} = curr_pval;
                end
            end
        end % end i
    end %end animal
    
    
    
    
    
    % Copied from DFSsj_HPexpt_ripCA1popresp2
    
    % ----------
    % Combine across epochs - based on PFC idxs
    % ---------
    cntcells = 0;
    uniqueIndices = unique(PFCidxs(:,[1 2 4 5]),'rows'); % Collapse across epochs
    % iterating only over the unique indices and finding matches in allindexes
    
    for i=1:length(uniqueIndices)
        cntcells = cntcells+1;
        curridx = uniqueIndices(i,:);
        ind=find(ismember(PFCidxs(:,[1 2 4 5]),curridx,'rows'))';
        
        curr_replay_armidx1=[]; curr_replay_armidx2=[];
        curr_PFChist = [];curr_PFCspks=[]; curr_PFCspks_ep=[]; curr_PFCtype=[]; curr_GlobalDecodeTraj=[]; curr_BestDecodeTraj=[]; curr_BestTraj=[];
        cntep=0; cntev=0; cntevtot=0;
        curr_pval = [];
        % PFC
        for r=ind
            try
                curr_replay_armidx1 = [curr_replay_armidx1; CA1replay_arm_index1{r}'];
            catch
                keyboard;
            end
            curr_replay_armidx2 = [curr_replay_armidx2; CA1replay_arm_index2{r}'];
            curr_PFChist = [curr_PFChist; PFChist_Valid{r}];
            curr_GlobalDecodeTraj = [curr_GlobalDecodeTraj, CA1GlobalDecodeTraj{r}]; %This is a cell array, so commas
            curr_BestDecodeTraj = [curr_BestDecodeTraj, CA1BestDecodeTraj{r}]; %This is a cell array, so commas
            curr_BestTraj = [curr_BestTraj; CA1BestTraj{r}']; % Traj 1 or 2
            curr_pval = [curr_pval; CA1replay_pval{r}']; 
            cntep=cntep+1; cntevtot = cntevtot+length(PFCspks_Valid{r}); 
            curr_PFCspks_ep{cntep} = PFCspks_Valid{r};
            for ev = 1:length(PFCspks_Valid{r})
                cntev = cntev+1;
                curr_PFCspks{cntev} = PFCspks_Valid{r}{ev}; 
            end
            if isempty(curr_PFCtype)
                curr_PFCtype = PFCtype(r);
            end
        end
        
        
        % Save both in struct, and variable format
        allPFCidxs(cntcells,:) = curridx;
        allPFCtype(cntcells) = curr_PFCtype;
        allPFChist{cntcells} = curr_PFChist;
        allPFCspks{cntcells} = curr_PFCspks;
        allreplay_armidx1{cntcells} = curr_replay_armidx1;
        allreplay_armidx2{cntcells} = curr_replay_armidx2;
        allGlobalDecodeTraj{cntcells} = curr_GlobalDecodeTraj;
        allBestDecodeTraj{cntcells} = curr_BestDecodeTraj;
        allBestTraj{cntcells} = curr_BestTraj;
        allpval{cntcells} = curr_pval;    
        
        all_replayresp_PFC(cntcells).PFCidx = curridx;
        all_replayresp_PFC(cntcells).PFCtype = curr_PFCtype;
        all_replayresp_PFC(cntcells).PFChist = curr_PFChist;
        all_replayresp_PFC(cntcells).PFCspks = curr_PFCspks;
        all_replayresp_PFC(cntcells).allreplay_armidx1 = curr_replay_armidx1;
        all_replayresp_PFC(cntcells).allreplay_armidx2 = curr_replay_armidx2;
        all_replayresp_PFC(cntcells).allGlobalDecodeTraj = curr_GlobalDecodeTraj;
        all_replayresp_PFC(cntcells).allBestDecodeTraj = curr_BestDecodeTraj;
        all_replayresp_PFC(cntcells).allBestTraj = curr_BestTraj;
        all_replayresp_PFC(cntcells).allpval = curr_pval;
    end
    
    
    % For each PFC cells, get r-val between DecodeTraj and GlobalPETraj - will have to load  GlobalPETraj
    % Also get corresponding PFC resp, and correlate with the r-val
    
    %loadfile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_GlobalPEtraj_Runtraj_noFS_X6.mat'
    loadfile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_GlobalPEtraj_Runtraj_noFS_X6.mat'
    load(loadfile);
    
    cntdatacell = 0; % For PFC cells where all the variables exist
    dataPE=[]; dataidx=[]; datatype=[]; % For cells that have reqd variables
    store_rval=[]; store_dtraj=[]; store_ptraj=[]; store_PFCresp=[];
    
    for i=1:cntcells
        
        curr_PFCidx = allPFCidxs(i,:);
        curr_PFCtype = allPFCtype(i);
        curr_armidx1 = allreplay_armidx1{i};
        curr_armidx2 = allreplay_armidx2{i};
        curr_PFChist = allPFChist{i}; PFChistavg = mean(curr_PFChist,1);
        curr_PFCspks = allPFCspks{i};
        curr_GlobalDecodeTraj = allGlobalDecodeTraj{i};
        curr_BestDecodeTraj = allBestDecodeTraj{i};
        curr_BestTraj = allBestTraj{i};
        curr_pval = allpval{i};
        % cell array of vectors. SHould be Same Lth as curr_PFChist
        
        % Check if lengths match
        if length(curr_BestDecodeTraj)==size(curr_PFChist,1),
            % Get match to index in allRunTraj
            match = find(ismember(PFCGlobalIdxs, curr_PFCidx, 'rows'));
            
            if ~isempty(match) % shouldn't happen unless PFC cell is skipped for some reason in decoding
                currPE = PFCGlobalPE(match); % PE dont matter, but keep it
                currPEtraj = PFCGlobalPEtraj{match}';
                currPEtraj = currPEtraj./nanmax(currPEtraj); % Normalized
                currRunTraj = PFCRunTraj{match}';  % Will be two run trajs in 2 columns, have to compare to besttraj for each event
                currRunTraj = currRunTraj./repmat(nanmax(currRunTraj),size(currRunTraj,1),1); % Normalized
                
                rval = []; dtraj_curr=[]; ptraj_curr=[];  %Nev=0;
                if size(currRunTraj,1)>1 && ~isnan(currPE) % not empty or nan
                    for ev = 1:length(curr_BestTraj) % each event
                        if curr_pval(ev)<=0.05 % Only significant events, or leave open using 1
                            besttraj = curr_BestTraj(ev); % Is it traj 1 or Traj 2
                            useRunTraj = currRunTraj(:,besttraj);
                            if length(curr_BestDecodeTraj{ev})>1  % not empty or single nan
                                %Nev = Nev + 1;
                                uselth = min(length(curr_BestDecodeTraj{ev}),length(useRunTraj));
                                
                                % CA1 decode traj
                                dtraj = curr_BestDecodeTraj{ev}(1:uselth); 
                                
                                % Use Best Traj
                                ptraj = useRunTraj(1:uselth);
                                
                                % OR, Use PE Global Traj
                                %ptraj = currPEtraj(1:uselth);
                                
                                rem = [find(isnan(dtraj)), find(isnan(ptraj))];
                                dtraj(rem)=[]; ptraj(rem)=[];
                                rval(ev) = corr(dtraj, ptraj);
                                dtraj_curr{ev} = dtraj; ptraj_curr{ev} = ptraj;
                                
                            else
                                rval(ev) = nan;
                            end
                        else
                            rval(ev) = nan;
                        end % Only significant events
                    end % loop over events
                end 
                
                
                % This if for PE traj
%                 rval = []; %Nev=0;
%                 if length(currPEtraj)>1 && ~isnan(currPE) % not empty or nan
%                     for ev = 1:length(curr_DecodeTraj)
%                         if length(curr_DecodeTraj{ev})>1  % not empty or single nan
%                             %Nev = Nev + 1;
%                             uselth = min(length(curr_DecodeTraj{ev}),length(currPEtraj));
%                             dtraj = curr_DecodeTraj{ev}(1:uselth); ptraj = currPEtraj(1:uselth);
%                             rem = [find(isnan(dtraj)), find(isnan(ptraj))];
%                             dtraj(rem)=[]; ptraj(rem)=[];
%                             rval(ev) = corr(dtraj, ptraj);
%                         else
%                             rval(ev) = nan;
%                         end
%                     end
%                 end
                
                % PFC response
                % ------------
                % Get a vector of PFCresponses and bckground to - to align with rval
                % ---------------------------------------------------------------
                curr_PFCresp = mean((curr_PFChist(:,respidx)),2);
                curr_PFCbck = mean((curr_PFChist(:,bckidx)),2);
                % Raw Subtractive Modln
                curr_PFCmodln =  curr_PFCresp-curr_PFCbck;
                % Norm Subtractive Modln
                curr_PFCmodln_norm =  (curr_PFCresp-curr_PFCbck)./curr_PFCbck;
                % Divisive Modln
                curr_PFCmodln_div =  curr_PFCresp./curr_PFCbck;
                
                
                % GET a CORRLN BETN rval and the PFC Resp: Can also divide between r<0 and r>0
                % ---------------------------------------------------------------------------
                if length(rval) > 1 % not empty or single nan
                    
                    % ----------------------------
                    % TO Use Modln instead of Response
                    % ----------------------------
                    % curr_PFCresp = curr_PFCmodln;
                    
                    rem = [find(isnan(rval)), find(isnan(curr_PFCresp))]; % Get rid of nan rval
                    rval(rem)=[]; curr_PFCresp(rem)=[]; 
                    curr_PFChist(rem,:)=[]; % Also save spks and histogram for plotting
                    curr_PFCspks_use = []; dtraj_currst=[]; ptraj_currst=[];
                    if length(rem)~=0
                        vec = 1:length(curr_PFCspks);
                        useidxs = setdiff(vec,rem);
                        for v = 1:length(useidxs)
                            curr_PFCspks_use{v} = curr_PFCspks{useidxs(v)};
                        end
                        
                        % Also update stored dtraj and ptraj by removing NaNa 
                        vec = 1:length(dtraj_curr);
                        useidxs = setdiff(vec,rem);
                        for v = 1:length(useidxs)
                            dtraj_currst{v} = dtraj_curr{useidxs(v)};
                            ptraj_currst{v} = ptraj_curr{useidxs(v)};
                        end
                        
                    else
                        curr_PFCspks_use = curr_PFCspks;
                    end
                    
                    if length(rval)>10   % Should have atleast x events   5/8/15/20
                        cntdatacell =  cntdatacell+1;
                        try
                            %r_replayPE(cntdatacell) = corr(rval',curr_PFCresp);
                            r_replaytraj(cntdatacell) = corr(rval',curr_PFCresp);
                        catch
                            keyboard;
                        end
                        dataPE(cntdatacell) = currPE;
                        dataidx(cntdatacell,:) = curr_PFCidx;
                        datatype(cntdatacell) = curr_PFCtype;
                        store_rval{cntdatacell} = rval;
                        store_PFCresp{cntdatacell} = curr_PFCresp;
                        store_dtraj{cntdatacell} = dtraj_currst;
                        store_ptraj{cntdatacell} = ptraj_currst;
                        
                        if figcell
                            
                            % To control plotting
                            %if (curr_PFCidx(1)==1 && curr_PFCidx(2)==8 && curr_PFCidx(3)==17 && curr_PFCidx(4)==3)
                            if r_replaytraj(cntdatacell)>=-1 && curr_PFCtype == -1   %&& curr_PFCidx(1)==4
                                
                                
                                % Align histogram by rval
                                [rval_sort, sortidx] = sort(rval,'descend');
                                currPFChist_sort = curr_PFChist(sortidx,:);
                                
                                dtr_plot=[]; ptr_plot=[];
                                for i=1:length(sortidx)
                                    dtr_plot{i} = dtraj_currst(sortidx(i));
                                    ptr_plot{i} = ptraj_currst(sortidx(i));
                                end
                                
                                PFChistavg = mean(currPFChist_sort,1);
                                % Making matrix for imagesc from histogram
                                % First smooth in time: is this done already - No. check back in decode script
                                currPFChist_sort_matrix=[];
                                for c = 1:size(currPFChist_sort,1);
                                    histspks = currPFChist_sort(c,:);
                                    histspks = smoothvect(histspks, g1);
                                    currPFChist_sort_matrix(c,:) = histspks; % This is smoothened in time 
                                end
                                currPFChist_sort_matrix_sm=[];
                                for c = 1:size(currPFChist_sort_matrix,1);
                                    winst = c-smwin/2; winend = c+smwin/2;
                                    if winst<1, winst=1; end
                                    if winend>size(currPFChist_sort,1), winend = size(currPFChist_sort,1); end 
                                    currPFChist_sort_matrix_sm(c,:) = mean(currPFChist_sort_matrix(winst:winend,:));
                                end
                                    
                                
                                % Use Z-scoring
                                rip_spkshist_cell_PFC = currPFChist_sort_matrix; % Start with histogram with time smoothing like usual
                                cellmean_PFC = mean(rip_spkshist_cell_PFC,2); cellstd_PFC = std(rip_spkshist_cell_PFC,[],2);
                                x=find(cellstd_PFC==0); % Remove rows which are all zero / 
                                % OR can set all these rows to zero in Z-score: Make means for these rows 0, and make std any n (say 1). Z-score wil then be 0
                                rip_spkshist_cell_PFC(x,:)=[]; cellmean_PFC(x)=[]; cellstd_PFC(x)=[]; 
                                cellmean_mat = repmat(cellmean_PFC,1,size(rip_spkshist_cell_PFC,2)); cellstd_mat = repmat(cellstd_PFC,1,size(rip_spkshist_cell_PFC,2));
                                rip_spkshist_cellZ_PFC = (rip_spkshist_cell_PFC-cellmean_mat)./cellstd_mat;
                                % Smooth
                                rip_spkshist_cellZ_PFC_sm=[];
                                for c = 1:size(rip_spkshist_cellZ_PFC,1);
                                    winst = c-smwin/2; winend = c+smwin/2;
                                    if winst<1, winst=1; end
                                    if winend>size(rip_spkshist_cellZ_PFC,1), winend = size(rip_spkshist_cellZ_PFC,1); end 
                                    rip_spkshist_cellZ_PFC_sm(c,:) = mean(rip_spkshist_cellZ_PFC(winst:winend,:));
                                end
                                
                                
                                
                                % Spikes for raster
                                % -----------------
                                rip_spks_cellsort_PFC = curr_PFCspks_use;
              
                                %figure; hold on; redimscreen_2versubplots;
                                %subplot(2,1,1); hold on;
                                %imagesc(currPFChist_sort); %Need to Z-score to use imagesc
                                
                                figure; hold on;
                                spkcount = [];
                                for c=1:length(rip_spks_cellsort_PFC)
                                    tmps = rip_spks_cellsort_PFC{sortidx(c)}; % Pick the sorted index
                                    
                                    %plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,[],'LineWidth',3,'Color','b');
                                    plot(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),'k.','MarkerSize',8);
                                    %sj_plotraster(tmps,(length(rip_spks_cellsort_PFC)-c+1)*ones(size(tmps)),0.8,'k');
                                    
                                    % Get count of spikes in response window
                                    if ~isempty(tmps)
                                        subset_tmps = find(tmps>=rwin(1) & tmps<=rwin(2));
                                        spkcount = [spkcount; length(subset_tmps)];
                                    end
                                end
                                set(gca,'XLim',[-400 400]);
                                set(gca,'XTick',[])
                                %set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
                                                                
                                xlabel('Time(ms)','FontSize',24,'Fontweight','normal');
                                ylabel('Replay ordered by rval','FontSize',24,'Fontweight','normal');
                                set(gca,'YLim',[0 length(rip_spks_cellsort_PFC)]);
                                % Plot Line at 0 ms and rwin
                                ypts = 0:1:length(rip_spks_cellsort_PFC);
                                xpts = 0*ones(size(ypts));
                                plot(xpts , ypts, 'k--','Linewidth',2);
                                xpts = 150*ones(size(ypts));
                                plot(xpts , ypts, 'k--','Linewidth',2);
                                switch curr_PFCtype
                                    case 1
                                        str = 'Exc';
                                    case -1
                                        str = 'Inh';
                                    case 0
                                        str = 'Neu';
                                end
                                title(sprintf('Anim %d Day %d Tet %d Cell %d Corr %0.2f Type %s', curr_PFCidx(1), curr_PFCidx(2), curr_PFCidx(3), curr_PFCidx(4), r_replaytraj(cntdatacell), str),...
                                'FontSize',24,'Fontweight','normal');
                                
                                rval_sort = roundn(rval_sort,-2);
                                set(gca,'YTick',[1:length(rip_spks_cellsort_PFC)],'YTickLabel',rval_sort(end:-1:1));
                                %set(gca,'YTick',[1:5:length(rip_spks_cellsort_PFC)],'YTickLabel',[1:5:length(rip_spks_cellsort_PFC)]);
                                
                                figdir = '/data25/sjadhav/HPExpt/Figures/Replay2015/DecodeMatchEg/';
                                figfile = [figdir,'ExampleReplayRaster_1_1_15_2_Inh']
                                if savefig1
                                    %figfile = [figdir,savefigfilename,'/',sprintf('pfcripmod%d', i)];
                                    %print('-dpng', figfile, '-r300');
                                    print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
                                    
                                end
                                
                            figfile = [figdir,'DecodeMatchEg_1_1_17_2_Inh_Low2']
                            aa=26; figure; hold on; plot(dtr_plot{aa}{1},'g'); plot(ptr_plot{aa}{1},'b')

                            
                            %Sparse matrix plot
                            % -------------------
%                             currraster = rip_spks_cellsort_PFC;
%                             numRips=length(currraster);
%                             % creating a raster matrix of 0/1 with 1ms resolution it goes from -500 to +500ms relative to ripples
%                             curRastMat=zeros(numRips,1100);
%                             for i=1:numRips,curRastMat(i,round(currraster{i}+551))=1;end
%                             curRastMat = curRastMat(:,50:1050);
%         
%                             msize=4; %msize=3;
%                             figure; hold on;
%                             %spy(flipud(curRastMat),'ko',msize); % Change axis around
%                             spy(curRastMat,'ko',msize);
%                             xlim([0 1000]); set(gca,'XTick',[0,250,500,750,1000],'XTickLabel',num2str([-500,-250,0,250,500]'));
%                             y=ylim; y2=y(2); maxi=floor(max(y2/100));
%                             set(gca,'YTick',fliplr(y2:-100:0),'YTickLabel',num2str(100*(maxi:-1:0)'));
%                             %title(['idx= ' num2str(curridx) ';  P= ' num2str(p_var2) ' ' allripplemod(ii).type ' FR = ' num2str(roundn(cellfr,-1))]);
%                             %xlabel('Time(ms)','FontSize',xfont,'Fontweight','normal');
%                             %ylabel('SWR number','FontSize',yfont,'Fontweight','normal');
%                             ypts = 0:1:y2; xpts = 500*ones(size(ypts));
%                             plot(xpts , ypts, 'k--','Linewidth',2);
%                             
%                             %figfile = [figdir,'RippleAlignRaster_','Spy_Msize', num2str(msize),'_', num2str(anim),'-',num2str(day),'-',num2str(tet),'-',num2str(cell)]
%                             savefig=0;
%                             if savefig==1,
%                                 print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
%                             end
%                             
                            
                            % Oth matrix plot
                            % ---------------
%                             figure; hold on;
%                             sm=50; %im=1;
%                             %sm=50; im=2;
%                             imagesc(flipud(filter2(ones(sm)/100,curRastMat))); xlim([-1 1001]);                 
%                             set(gca,'XTick',[0,250,500,750,1000],'XTickLabel',num2str([-500,-250,0,250,500]'));
%                             y2=numRips; ypts = 0:1:y2; xpts = 500*ones(size(ypts));
%                             plot(xpts , ypts, 'k--','Linewidth',2);
                            
                            
                            
                            
                                figure; hold on;    
                                PFChistavg = smoothvect(PFChistavg, g1); 
                                xaxis = -pret:binsize:postt;
                                plot(xaxis,PFChistavg,'k-','LineWidth',4);
                                ypts = [0:0.01:max(PFChistavg)];
                                xpts = 0*ones(size(ypts));
                                plot(xpts , ypts, 'k--','Linewidth',1);
                                xpts = 150*ones(size(ypts));
                                plot(xpts , ypts, 'k--','Linewidth',1);
                                set(gca,'XLim',[-400 400]);
                                
                                
%                                 % Matrices
                                figure; hold on; redimscreen_2versubplots;
                                %subplot(2,1,1); hold on;
                                rip_spkshist_cellsort_PFC_matrix = currPFChist_sort_matrix_sm;
                                xaxis = -pret:binsize:postt; yaxis = 1:size(rip_spkshist_cellsort_PFC_matrix,1);
                                imagesc(xaxis,yaxis,flipud(rip_spkshist_cellsort_PFC_matrix));                             
                                set(gca,'XLim',[-pret postt]);
                                set(gca,'YLim',[0 size(rip_spkshist_cellsort_PFC_matrix,1)]);
                                %xlabel('Time(ms)','FontSize',20,'Fontweight','normal');
                                set(gca,'XLim',[-400 400]);
                                set(gca,'XTick',[])
                                %set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
                                xlabel('Time(ms)','FontSize',24,'Fontweight','normal');
                                ylabel('Replay ordered by rval','FontSize',24,'Fontweight','normal');
                                set(gca,'YLim',[0 size(rip_spkshist_cellsort_PFC_matrix,1)]);
                                % Plot Line at 0 ms and rwin
                                ypts = 0:1:size(rip_spkshist_cellsort_PFC_matrix,1);
                                xpts = 0*ones(size(ypts));
                                plot(xpts , ypts, 'k--','Linewidth',2);
                                xpts = 150*ones(size(ypts));
                                plot(xpts , ypts, 'k--','Linewidth',2);
                                title(sprintf('Anim %d Day %d Tet %d Cell %d Corr %0.2f Type %s', curr_PFCidx(1), curr_PFCidx(2), curr_PFCidx(3), curr_PFCidx(4), r_replaytraj(cntdatacell), str),...
                                'FontSize',24,'Fontweight','normal');
%                                 
%                                 subplot(2,1,2); hold on;
%                                 xaxis = -pret:binsize:postt; yaxis = 1:size(rip_spkshist_cellZ_PFC_sm,1);
%                                 imagesc(xaxis,yaxis,flipud(rip_spkshist_cellZ_PFC_sm));                             
%                                 set(gca,'XLim',[-pret postt]);
%                                 set(gca,'YLim',[0 size(rip_spkshist_cellZ_PFC_sm,1)]);
%                                 %xlabel('Time(ms)','FontSize',20,'Fontweight','normal');
%                                 set(gca,'XLim',[-400 400]);
%                                 set(gca,'XTick',[])
%                                 %set(gca,'XTick',[-pret:200:postt],'XTickLabel',num2str([-pret:200:postt]'));
%                                 xlabel('Time(ms)','FontSize',24,'Fontweight','normal');
%                                 ylabel('Replay ordered by rval','FontSize',24,'Fontweight','normal');
%                                 set(gca,'YLim',[0 size(rip_spkshist_cellZ_PFC_sm,1)]);
%                                 % Plot Line at 0 ms and rwin
%                                 ypts = 0:1:size(rip_spkshist_cellZ_PFC_sm,1);
%                                 xpts = 0*ones(size(ypts));
%                                 plot(xpts , ypts, 'k--','Linewidth',2);
%                                 xpts = 150*ones(size(ypts));
%                                 plot(xpts , ypts, 'k--','Linewidth',2);
                            
% 
% %                                 PFChistavg = smoothvect(PFChistavg, g1); lefthistavg = smoothvect(lefthistavg, g1); righthistavg = smoothvect(righthistavg, g1);
% %                                 xaxis = -pret:binsize:postt;
% %                                 %plot(xaxis,mean(rip_spkshist_cellsort_PFC),'k-','Linewidth',4);
% %                                 
% %                                 title(sprintf('PFC Response to Replays'),'FontSize',24,'Fontweight','normal');
% %                                 ylabel('Firing Rate','FontSize',24,'Fontweight','normal');
% %                                 xlabel('Time','FontSize',24,'Fontweight','normal');
% %                                 plot(xaxis,PFChistavg,'k-','LineWidth',4);
% %                                 plot(xaxis,lefthistavg,'b-','LineWidth',2);
% %                                 plot(xaxis,righthistavg,'r-','LineWidth',2);
% %                                 legend('All','LeftArm','RigthArm')
% %                                 ypts = [0:0.01:max(PFChistavg)];
% %                                 xpts = 0*ones(size(ypts));
% %                                 plot(xpts , ypts, 'k--','Linewidth',1);
% %                                 %line([50 50],[0 max(PFChistavg)]);
% %                                 set(gca,'XLim',[-400 400]);
%                                 
                                keyboard;
                            end
                            
                            %figure; hold on;
                            %subplot(2,1,1); hold on; plot(curr_PFCresp,curr_armidx1,'ro'); title('Resp'); xlabel('Resp'); ylabel('ArmIdx');
                            %subplot(2,1,2); hold on; plot(curr_PFCmodln,curr_armidx1,'ro'); title('Modln'); xlabel('Modln'); ylabel('ArmIdx');
                            
                        end
                        
                        
                    end
                end
      
            end % match between decode cell idx and PE cell idx
            
        else
            keyboard;
        end % match length between decode and PFC response
        
        
    end % loop over PFC cells
    
    cntdatacell
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data



% Get PFC classification based on allPFCidxs
excited = find(datatype == 1); exc_idxs = dataidx(excited,:);
inhibited = find(datatype == -1); inh_idxs = dataidx(inhibited,:);
neutral = find(datatype == 0); neu_idxs = dataidx(neutral,:);

Rexc = r_replaytraj(excited); PEexc = dataPE(excited);
Rinh = r_replaytraj(inhibited); PEinh = dataPE(inhibited);
Rneu = r_replaytraj(neutral); PEneu = dataPE(neutral);


rem = [find(isnan(Rexc) | Rexc<-0.4), find(isnan(PEexc))]; Rexc(rem) = []; PEexc(rem) = [];
rem = [find(isnan(Rinh) | Rinh<0-0.2), find(isnan(PEinh))]; Rinh(rem) = []; PEinh(rem) = [];
rem = [find(isnan(Rneu)), find(isnan(PEneu))]; Rneu(rem) = []; PEneu(rem) = [];

% BAR PLOT
% --------
figure; hold on;
plot(1, nanmean(Rexc),'rsq','MarkerSize',24);  errorbar2(1, nanmean(Rexc), nanstderr(Rexc), 0.3, 'r');
plot(2, nanmean(Rinh),'bsq','MarkerSize',24);  errorbar2(2, nanmean(Rinh), nanstderr(Rinh), 0.3, 'b');
plot(3, nanmean(Rneu),'ksq','MarkerSize',24);  errorbar2(3, nanmean(Rneu), nanstderr(Rneu), 0.3, 'k');

plot(ones(1,length(Rexc)), Rexc,'ro');
plot(2*ones(1,length(Rinh)), Rinh,'bo');
plot(3*ones(1,length(Rneu)), Rneu,'ko');


%bar([mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)]); hold on;
%errorbar2([1 2 3], [mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)],  [stderr(pfcEXCPE) stderr(pfcINHPE) stderr(pfcNEUPE)] , 0.3, 'k')
ylim([-0.6 .8]);
title('Corrln betn PFCBestTraj-CA1DecodedTraj and PFCResp','FontSize',20,'FontWeight','normal');
%title('Corrln betn PFCPETraj-CA1DecodedTraj and PFCResp','FontSize',20,'FontWeight','normal');
ylabel('Corrln (Sig Replays-Thrs4-Nevs10)','FontSize',20,'FontWeight','normal')
set(gca,'XTick',[1 2 3]); set(gca,'XTickLabel',{'Exc','Inh','Neu'});

savefig=0;

savefigfilename = 'Decoding-PFCTrajCA1Replay-Vs-PFCResp-cellthrs4-sig-Thrs10alt2';
%savefigfilename = 'Decoding-PFCPETrajCA1Replay-Vs-PFCResp-cellthrs5-sig-Thrs10';
figfile = [figdir,savefigfilename]
if savefig1
    %figfile = [figdir,savefigfilename,'/',sprintf('pfcripmod%d', i)];
    %print('-dpng', figfile, '-r300');
    print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    
end

[pKW table statsKW] = kruskalwallis([Rexc Rinh Rneu], [ones(1, length(Rexc)) ones(1,length(Rinh)).*2 ones(1,length(Rneu)).*3]);
[c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.

i=1;


% % CORRELATION
% % -----------
% [exc_r1,exc_p1] = corr(Rexc',PEexc'); %[exc_r2,exc_p2] = corr(exc_ModlnPrefIdx',exc_ArmIdx');
% [inh_r1,inh_p1] = corr(Rinh',PEinh'); %[inh_r2,inh_p2] = corr(inh_ModlnPrefIdx',inh_ArmIdx');
% [neu_r1,neu_p1] = corr(Rneu',PEneu'); %[neu_r2,neu_p2] = corr(neu_ModlnPrefIdx',neu_ArmIdx');
% 
% % ------------
% figure; hold on;redimscreen_2versubplots;
% subplot(3,1,1); hold on;
% title(sprintf('Excited Neurons: r-decodevstraj vs PE'),'FontSize',24,'Fontweight','normal');
% ylabel('Rexc','FontSize',24,'Fontweight','normal');
% xlabel('PEexc','FontSize',24,'Fontweight','normal');
% plot(PEexc,Rexc,'ro','MarkerSize',20,'LineWidth',2);
% xpts = 0:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:0.4; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% % Regression
% [b00,bint00,r00,rint00,stats00] = regress(Rexc', [ones(size(PEexc')) PEexc']);
% xpts = min(PEexc):0.01:max(PEexc);
% bfit00 = b00(1)+b00(2)*xpts;
% plot(xpts,bfit00,'r-','LineWidth',4);
% rsquare_exc = stats00(1), pregr_exc = stats00(3)
% 
% subplot(3,1,2); hold on;
% title(sprintf('Inhibited Neurons: r-decodevstraj vs PE'),'FontSize',24,'Fontweight','normal');
% ylabel('Rinh','FontSize',24,'Fontweight','normal');
% xlabel('PEinh','FontSize',24,'Fontweight','normal');
% plot(PEinh,Rinh,'bo','MarkerSize',20,'LineWidth',2);
% xpts = 0:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:0.4; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% % Regression
% [b00,bint00,r00,rint00,stats00] = regress(Rinh', [ones(size(PEinh')) PEinh']);
% xpts = min(PEinh):0.01:max(PEinh);
% bfit00 = b00(1)+b00(2)*xpts;
% plot(xpts,bfit00,'b-','LineWidth',4);
% rsquare_inh = stats00(1), pregr_inh = stats00(3)
% 
% subplot(3,1,3); hold on;
% title(sprintf('Neutral Neurons: r-decodevstraj vs PE'),'FontSize',24,'Fontweight','normal');
% ylabel('Rneu','FontSize',24,'Fontweight','normal');
% xlabel('PEneu','FontSize',24,'Fontweight','normal');
% plot(PEneu,Rneu,'go','MarkerSize',20,'LineWidth',2);
% xpts = 0:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:0.4; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% % Regression
% [b00,bint00,r00,rint00,stats00] = regress(Rneu', [ones(size(PEneu')) PEneu']);
% xpts = min(PEneu):0.01:max(PEneu);
% bfit00 = b00(1)+b00(2)*xpts;
% plot(xpts,bfit00,'g-','LineWidth',4);
% rsquare_neu = stats00(1), pregr_neu = stats00(3)
% 
% 
% 
% 
% 
% % ------------
% % figure; hold on;redimscreen_2versubplots;
% % subplot(2,1,1); hold on;
% % title(sprintf('Neutral Neurons: RespPrefIdx vs ArmIdx'),'FontSize',24,'Fontweight','normal');
% % plot(neu_RespPrefIdx,neu_ArmIdx,'go','MarkerSize',20,'LineWidth',2);
% % xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:1; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% % % Regression
% % [b00,bint00,r00,rint00,stats00] = regress(neu_ArmIdx', [ones(size(neu_RespPrefIdx')) neu_RespPrefIdx']);
% % xpts = min(neu_RespPrefIdx):0.01:max(neu_RespPrefIdx);
% % bfit00 = b00(1)+b00(2)*xpts;
% % plot(xpts,bfit00,'g-','LineWidth',4);
% % rsquare = stats00(1)
% %
% % subplot(2,1,2); hold on;
% % title(sprintf('Neutral Neurons: ModlnPrefIdx vs ArmIdx'),'FontSize',24,'Fontweight','normal');
% % plot(neu_ModlnPrefIdx,neu_ArmIdx,'go','MarkerSize',20,'LineWidth',2);
% % xpts = -1:0.1:1; ypts = 0*ones(size(xpts)); plot(xpts,ypts,'k--');ypts = -0.4:0.1:1; xpts = 0*ones(size(ypts)); plot(xpts,ypts,'k--');
% % % Regression
% % [b00,bint00,r00,rint00,stats00] = regress(neu_ArmIdx', [ones(size(neu_ModlnPrefIdx')) neu_ModlnPrefIdx']);
% % xpts = min(neu_ModlnPrefIdx):0.01:max(neu_ModlnPrefIdx);
% % bfit00 = b00(1)+b00(2)*xpts;
% % plot(xpts,bfit00,'g-','LineWidth',4);
% % rsquare = stats00(1)
% 
% 
% 
% 
