function out = DFAsj_getripPoplnResp(indices, excludetimes, ripplemod, varargin)

% All indices are CA1 cells. Get PoplnResp aligned to SWRs

tetfilter = '';
dospeed = 0;
excludetimes = [];

for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

day = indices(1,1);
epoch = indices(1,2);

cellsi=[]; usecellsi=0; % CA1 cells

totalcells = size(indices,1);
for i=1:totalcells
    cellsi = [cellsi; day epoch indices(i,3) indices(i,4)];
end
nCA1cells = size(cellsi,1); 
    
% ------------------
% Parse ripplemoddata - don;t really need to do this. 
for i=1:size(cellsi,1)
    i;
    eval(['ripplemodi{',num2str(i),'}= ripplemod{cellsi(',num2str(i),',1)}{cellsi(',num2str(i),',2)}'...
        '{cellsi(',num2str(i),',3)}{cellsi(',num2str(i),',4)}.trialResps;']);
end

Xmat = [];
for i=1:size(cellsi,1)
    eval(['currResps = ripplemodi{',num2str(i),'};']);
    Xmat = [Xmat, currResps]; % Rows are observations, Columns are triaslResps for current CA1 cell
end

PoplnResp = sum(Xmat,2); % Sum across CA1 cells to get popln resp


% % ------ 
% % Output
% % ------
out.indices = indices;
out.PoplnResp = PoplnResp;
out.nCA1cells = nCA1cells;
%out.Xmat = Xmat;




