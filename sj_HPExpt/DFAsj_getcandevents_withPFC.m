function out = DFAsj_getcandevents_withPFC(indices, excludetimes, spikes, ripples, tetinfo, pos, cellinfo, varargin)
% Like DFAsj_getcandevents, but returns PFC cells activated during ripples as well, and takes in all indices 
% Use tetinfo and tetfilter passed in, or redefine here to get riptets
% Then use ripples to getriptimes. Can Use inter-ripple-interval of 1 sec, and use a low-speed criterion.


tetfilter = '';
excludetimes = [];
maxcell = 0;
minstd = 3;
lowsp_thrs = 4; %cm/sec
highsp_thrs = lowsp_thrs;
dospeed = 0;
cellcountthresh = [];  % Can be used to parse ripples
minrip=1;

% % For ripple trigger
% % ------------------
% binsize = 10; % ms
% pret=550; postt=550; %% Times to plot
% push = 500; % either bwin(2) or postt-trim=500. For jittered trigger in background window
% trim = 50;
% smwin=10; %Smoothing Window - along y-axis for matrix. Carry over from ...getrip4
% rwin = [0 200];
% bwin = [-500 -300];
% push = 500; % either bwin(2) or postt-trim=500. If doing random events. See ... getrip4


for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'minstd'
            minstd = varargin{option+1};
        case 'minrip'
            minrip = varargin{option+1};
        case 'maxcell'
            maxcell = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        case 'lowsp_thrs'
            lowsp_thrs = varargin{option+1};
        case 'cellcountthresh'
            cellcountthresh = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

index = [indices(1,1) indices(1,2)];
day = index(1);
epoch = index(2);

% Separate CA1 and PFC cells
% --------------------------
cellsi=[]; usecellsi=0; % CA1 cells
cellsp=[]; usecellsp=0; % PFC cells

totalcells = size(indices,1);
for i=1:totalcells
    currarea = cellinfo{indices(i,1)}{indices(i,2)}{indices(i,3)}{indices(i,4)}.area;
    if strcmp(currarea,'PFC'),
        cellsp = [cellsp; day epoch indices(i,3) indices(i,4)];
        usecellsp = usecellsp+1;
    else
        cellsi = [cellsi; day epoch indices(i,3) indices(i,4)];
        usecellsi = usecellsi+1;
    end
end
nCA1cells = size(cellsi,1); nPFCcells = size(cellsp,1);


% Get riptimes
% -------------
if isempty(tetfilter)
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', '(isequal($descrip, ''riptet''))','minstd',minstd,'minrip',minrip);
else
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', tetfilter, 'minstd', minstd,'minrip',minrip);
end


% ISI of 1 sec and speed criterion - if needed
% Get triggers as rip starttimes separated by at least 1 sec
% ----------------------------------------------------------
rip_starttime = 1000*riptimes(:,1);  % in ms

% Find ripples separated by atleast a second
% --------------------------------------------
iri = diff(rip_starttime);
keepidx = [1;find(iri>=1000)+1];
rip_starttime = rip_starttime(keepidx);
riptimes = riptimes(keepidx,:); 


% % Implement speed criterion - Keep.
% % ----------------------------------------
if dospeed
    absvel = abs(pos{day}{epoch}.data(:,5)); % Can also use field 9
    postime = pos{day}{epoch}.data(:,1); % in secs
    pidx = lookup(rip_starttime,postime*1000);
    speed_atrip = absvel(pidx);
    lowsp_idx = find(speed_atrip <= lowsp_thrs);
    highsp_idx = find(speed_atrip > highsp_thrs);
    
    rip_starttime = rip_starttime(lowsp_idx);
    riptimes = riptimes(lowsp_idx,:);
end


%Cellcountthresh in CA1 for each event as in getpopulationevents2 or  sj_HPexpt_ripalign_singlecell_getrip4
% -----------------------------------

excludeperiods = excludetimes;
spikecounts = []; pspikecounts = [];
celldata = []; pcelldata = []; 
out.index = []; out.pindex = [];

CA1indices = cellsi; PFCindices = cellsp;

if ~isempty(riptimes)
    %go through each CA1 cell and calculate the binned spike counts
    for cellcount = 1:size(CA1indices,1)

        index = CA1indices(cellcount,:);
        if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)}.data)
            spiketimes = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);
        else
            spiketimes = [];
        end
        %Find valid spikes
        spiketimes = spiketimes(find(~isExcluded(spiketimes, excludeperiods)));
        spikebins = periodAssign(spiketimes, riptimes(:,[1 2]));
        
        if ~isempty(spiketimes)
            validspikes = find(spikebins);
            spiketimes = spiketimes(validspikes);
            spikebins = spikebins(validspikes);
        end
        
        if ~isempty(spiketimes)
            tmpcelldata = [spiketimes spikebins];
            tmpcelldata(:,3) = cellcount;
        else
            tmpcelldata = [0 0 cellcount];
        end
        celldata = [celldata; tmpcelldata];
        spikecount = zeros(1,size(riptimes,1));
        for i = 1:length(spikebins)
            spikecount(spikebins(i)) = spikecount(spikebins(i))+1;
        end

        spikecounts = [spikecounts; spikecount];
        out.index = [out.index; index];
    end
    
    % Gather CA1
    % ----------
    %Sort all spikes by time
    celldata = sortrows(celldata,1);

    cellcounts = sum((spikecounts > 0));
    %Find all events with enough cells
    eventindex = find(cellcounts >= cellcountthresh);
    Ncounts = cellcounts(eventindex);
    
    % Number of spikes per cell
    MeanSpksPerCell_allripples = mean(spikecounts,2); % column vector with one entry for each cells
    NSpksPerCell_allripples = spikecounts(spikecounts>0); % each entry is no of spikes for a cell in a ripple, collapsed across all cells in session 
      
    validcounts = spikecounts(:,eventindex); % No. of spikes per cell, keeping only valid events
    MeanSpksPerCell_candripples = mean(validcounts,2); % column vector with one entry for each cells
    NSpksPerCell_candripples = validcounts(validcounts>0);  % each entry is no of spikes for a cell in a candidate ripple, collapsed across all cells in session 
   
    riptimes_keep = riptimes(eventindex);
   
    
    
    % for PFC cells, return similar parameters
    % ----------------------------------------
    
    if ~isempty(PFCindices)        
        
        for pcellcount = 1:size(PFCindices,1)
            
            index = PFCindices(pcellcount,:);
            if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)}.data)
                spiketimes = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);
            else
                spiketimes = [];
            end
            %Find valid spikes
            spiketimes = spiketimes(find(~isExcluded(spiketimes, excludeperiods)));
            spikebins = periodAssign(spiketimes, riptimes(:,[1 2]));
            
            if ~isempty(spiketimes)
                validspikes = find(spikebins);
                spiketimes = spiketimes(validspikes);
                spikebins = spikebins(validspikes);
            end
            
            if ~isempty(spiketimes)
                tmpcelldata = [spiketimes spikebins];
                tmpcelldata(:,3) = pcellcount;
            else
                tmpcelldata = [0 0 pcellcount];
            end
            pcelldata = [pcelldata; tmpcelldata];
            pspikecount = zeros(1,size(riptimes,1));
            for i = 1:length(spikebins)
                pspikecount(spikebins(i)) = pspikecount(spikebins(i))+1;
            end
            
            pspikecounts = [pspikecounts; pspikecount];
            out.pindex = [out.pindex; index];
        end
        
    
    % Gather PFC
    % ----------
    %Sort all spikes by time
    pcelldata = sortrows(pcelldata,1);
    pcellcounts = sum((pspikecounts > 0),1); % For each event, get no. of cells with at least 1 spike
    try
        Ncounts_PFC = pcellcounts(eventindex); % Get no. of PFC cells responding for each candidate event 
    catch
        keyboard;
    end
    %Find all events with enough cells - for PFC: this is at least 1 cell
    peventindex = find(pcellcounts >= 1); % Cell threshold is 0
    
    
    % Find all CA1 candidate events with at least 1 PFC cell firing    
    PFC_respindex = intersect(eventindex,peventindex);
    Ncounts_PFCwithresp = pcellcounts(PFC_respindex); % No. of PFC cells active in each candidate event, with atleast 1 cell responding
    Ncandevents_PFCresp = length(PFC_respindex);
    
    % Number of spikes per cell
    pMeanSpksPerCell_allripples = mean(pspikecounts,2); % column vector with one entry for each cells
    pNSpksPerCell_allripples = pspikecounts(pspikecounts>0); % each entry is no of spikes for a cell in a ripple, collapsed across all cells in session 
      
    pvalidcounts = pspikecounts(:,PFC_respindex); % No. of spikes per cell, keeping only valid events with PFC response
    pMeanSpksPerCell_candripples = mean(pvalidcounts,2); % column vector with one entry for each cells
    pNSpksPerCell_candripples = pvalidcounts(pvalidcounts>0);  % each entry is no of spikes for a cell in a candidate ripple, collapsed across all cells in session 
    
    else
        Ncounts_PFC = zeros(size(Ncounts));
        Ncounts_PFCwithresp = zeros(size(Ncounts));
        Ncandevents_PFCresp = 0;
        pMeanSpksPerCell_allripples = zeros(size(MeanSpksPerCell_allripples));
        pNSpksPerCell_allripples = zeros(size(NSpksPerCell_allripples));
        pMeanSpksPerCell_candripples = zeros(size(MeanSpksPerCell_candripples));
        pNSpksPerCell_candripples = zeros(size(NSpksPerCell_candripples));
    end % end PFC
    
end %end if ~empty riptimes
     

% Output
% ------

out.Ncandevents = length(eventindex); % No. of candidate events
out.Ncounts = Ncounts; % No. of CA1 cells active in the valid events

out.Ncells = size(CA1indices,1); % No. of CA1 cells in epoch
out.MeanSpksPerCell_allripples = MeanSpksPerCell_allripples;
out.NSpksPerCell_allripples = NSpksPerCell_allripples;
out.MeanSpksPerCell_candripples = MeanSpksPerCell_candripples;
out.NSpksPerCell_candripples = NSpksPerCell_candripples;

% PFC
out.Ncandevents_PFCresp =  Ncandevents_PFCresp; % No. of candidate events with PFC resp
out.Ncounts_PFC = Ncounts_PFC; % No. of PFC cells active in the candidate events
out.Ncounts_PFCwithresp = Ncounts_PFCwithresp; % No. of PFC cells active in the candidate events with at least 1 PFC cell responding

out.Ncells_PFC = size(PFCindices,1); % No. of CA1 cells in epoch
out.pMeanSpksPerCell_allripples = pMeanSpksPerCell_allripples;
out.pNSpksPerCell_allripples = pNSpksPerCell_allripples;
out.pMeanSpksPerCell_candripples = pMeanSpksPerCell_candripples;
out.pNSpksPerCell_candripples = pNSpksPerCell_candripples;









