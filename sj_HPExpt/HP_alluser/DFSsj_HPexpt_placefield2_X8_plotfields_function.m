
function figopt1 = DFSsj_HPexpt_placefield2_X8_plotfields_function(prefix,day,ep,tet,cell)
% DFSsj_HPexpt_placefield2_X8_plotfields_function('HPa',2,2,18,4)

% Based on savefiled_X8 or savefileds_ver4
% Bosrrow plotting from DFSsj_HPexpt_placefiled2. Still not plotting mapfields_sep
% Load the either the  place field gather file for the animal (eg. HPa_allfields_X8_gather
% or HPa_allfields_gather) and plot
% OR
% Better way: just load the mapfields /linfields / mapfiles_sep file and plot directly for which place cell you want
% Make this a function like plotsj_ratempa and plotsj_ratemap_sep and plotsj_ratemap_mulepochs


saveg=0;
showmax = 1;
fontsize = 12;

% Get DATA
% -------


switch prefix
    case 'HPa'
        directoryname = '/data25/sjadhav/HPExpt/HPa_direct/';
    case 'HPb'
        directoryname = '/data25/sjadhav/HPExpt/HPb_direct/';
    case 'HPc'
        directoryname = '/data25/sjadhav/HPExpt/HPc_direct/';
    case 'Ndl'
        directoryname = '/data25/sjadhav/HPExpt/Ndl_direct/';
    case 'Rtl'
        directoryname = '/data25/sjadhav/HPExpt/Rtl_direct/';
    case 'Brg'
        directoryname = '/data25/sjadhav/HPExpt/Brg_direct/';
    case 'RE1'
        directoryname = '/data25/sjadhav/RippleInterruption/RE1_direct';
    case 'RCa'
        directoryname = '/data25/sjadhav/RippleInterruption/RCa_direct';
    case 'RCb'
        directoryname = '/data25/sjadhav/RippleInterruption/RCb_direct';
    case 'RCc'
        directoryname = '/data25/sjadhav/RippleInterruption/RCc_direct';
    case 'RCd'
        directoryname = '/data25/sjadhav/RippleInterruption/RCd_direct';
    case 'REc'
        directoryname = '/data25/sjadhav/RippleInterruption/REc_direct';
    case 'REd'
        directoryname = '/data25/sjadhav/RippleInterruption/REd_direct';
    case 'REe'
        directoryname = '/data25/sjadhav/RippleInterruption/REe_direct';
    case 'REf'
        directoryname = '/data25/sjadhav/RippleInterruption/REf_direct';
end

cd(directoryname);

if (day < 10)
    daystring = ['0',num2str(day)];
else
    daystring = num2str(day);
end
if (tet < 10)
    tetstring = ['0',num2str(tet)];
else
    tetstring = num2str(tet);
end
fname = [prefix,'mapfields',daystring];
load(fname);
rmap = mapfields{day}{ep}{tet}{cell}.smoothedspikerate;
% Get everything so you can also look at xticks and yticks if needed
map = mapfields{day}{ep}{tet}{cell};

fname = [prefix,'linfields',daystring];
load(fname);
trajdata= linfields{day}{ep}{tet}{cell};


% Set up Plotting
% ---------------

figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/RateMaps/DirnIdx/'

forppr = 1;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

summdir = figdir;
%set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',12);
    tfont = 12; % title font
    xfont = 12;
    yfont = 12;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end

clr = {'b','r','g','c','m','y','k','r'};


% Setting up Maps like plotsj_ratemap
cmap = jet(1024);
cmap(1,:) = 1;
colormap(cmap);
% set up the bounds to make it look good:
bounds = [0 0];
peakrate = max(rmap(:));
%bounds(2) = peakrate;
bounds(2) = 0.65*peakrate;

% if (isempty(peakrate))
%     peakrate = max(rmap(:));
%     %bounds(2) = peakrate * 0.65; %0.65;  Dont change peakrate
%     bounds(2) = peakrate;
% else
%     bounds(2) = peakrate;
% end
if (peakrate > 20)
    minbound = -1;
elseif (peakrate > 10)
    minbound = -.5;
    rmap(find(rmap == -1)) = -.5;
elseif (peakrate > 3)
    minbound = -.1;
    rmap(find(rmap == -1)) = -.1;
else
    minbound = -.01;
    rmap(find(rmap == -1)) = -.01;
end

bounds(1) = minbound;
%if (peakrate < .1)
%    bounds(2) = 1;
%end

% Get ticks
x1 = min(map.xticks);
x1m = max(map.xticks);
y1 = min(map.yticks);
y1m = max(map.yticks);


% Plot MAP
% ---------
% set up the colormap to be white at some negative values

%figure; hold on;
%h = imagesc(map1.xticks,map1.yticks,rmap1, bounds);
%h = imagesc(flipud(rmap), bounds); % Produces the same orientation
h = imagesc(flipud(rmap));

ch = colorbar;
set(ch, 'FontSize', fontsize);
if (showmax)
    %set(ch, 'YTick', floor(peakrate * 0.65), 'YTickLabel', num2str(floor(peakrate * 1)));
    set(ch, 'YTick', floor(peakrate), 'YTickLabel', num2str(floor(peakrate * 1)));
    
end
axis off;
%axis equal

figfile = [figdir,prefix,'_ratemap_','d',daystring,'ep',num2str(ep),'t',tetstring,'c',num2str(cell)]
if saveg==1
    %print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end


% Plot Linfields
% --------------
figure; hold on;
set(0,'defaultaxesfontsize',12);
xm = max([max(trajdata{1}(:,5)), max(trajdata{2}(:,5)), max(trajdata{3}(:,5)), max(trajdata{4}(:,5))]);

for i=1:length(trajdata)
    subplot(1,4,i); hold on;
    xdata = 2*(1:length(trajdata{i}(:,5)));
    %plot(xdata,trajdata{i}(:,5),[clr{i} '-'],'Linewidth',2);
    plot(xdata,trajdata{i}(:,5),'k-','Linewidth',2);
    ylim([0 1.1*xm]);
    %axis off;
    
    %legend('OutRealLeft','InRealLeft','OutRealRight','InRealRight');
    %legend('Traj1Out','Traj1In','Traj2Out','Traj2In');
    %xlabel ('Position along linear trajectory (cm)','FontSize',14,'Fontweight','bold');
    %ylabel ('Firing Rate (Hz)','FontSize',14,'Fontweight','bold');
end

figfile = [figdir,prefix,'_lintraj_','d',daystring,'ep',num2str(ep),'t',tetstring,'c',num2str(cell)]
Direction Index 
0.5if saveg==1
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    
end

%keyboard;





