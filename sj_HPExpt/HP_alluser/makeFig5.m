% from DFSsj_HPexpt_glm_theta5
% This creates graphs for CA1 ensembles and PFC combination. 


set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
set(0,'defaultaxesfontname','Arial');
set(0,'defaultAxesFontName','Arial');
set(0,'defaultTextFontName','Arial');
set(0,'defaultaxesfontsize',16);

%set(gca,'fontname','arial')
forppr=1;
tfont = 14; % title font
xfont = 12;
yfont = 12;

clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells
plotGraphs=1;
savedir = '/mnt/data25new/sjadhav/HPExpt/HP_ProcessedData';

val=1; savefile = [savedir 'HP_ripmod_glmfit_theta5']; area = 'PFC'; clr = 'b'; % PFC
savefig1=0;

% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days



load(savefile);



% -------------------------  Filter Format Done -------------------------

% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
switch val
    case 1
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather'];
end




%----- Getting theta model parameters
cnt=0;
%Glm
allglmidxstheta=[];    allglmidxstheta2=[];
allmodelbtheta=[]; allmodelbtheta2=[];allmodelptheta=[]; allmodelfitstheta=[];
allnsigtheta=[]; allnsigpostheta=[]; allnsignegtheta=[];  allfracsigtheta=[]; allfracsigpostheta=[]; allfracsignegtheta=[];
XmatsTheta={};
YmatsTheta={};
XYmatsTheta={};
for an = 1:length(modf)
    for i=1:length(modf(an).output{1})
        
        cnt=cnt+1;
        anim_index{an}{cnt} = modf(an).output{1}(i).indices;
        % Only indexes
        %animindex=[an modf(an).output{1}(i).indices]; % Put animal index in front
        %allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
        %Sub indices for glm and corrcoef
        allglmidxstheta = [allglmidxstheta; modf(an).output{1}(i).glmidxs]; % Need to put "an" in there
        
        indNoAnim=modf(an).output{1}(i).glmidxs2;
        indNoAnim=indNoAnim(find(nansum(indNoAnim')~=0),:);
        indWAnim=[an*ones(size(indNoAnim,1),1) indNoAnim];
        allglmidxstheta2 = [allglmidxstheta2; indWAnim];
        XmatsTheta{cnt}=modf(an).output{1}(i).Xmat;
        YmatsTheta{cnt}=modf(an).output{1}(i).Ymat;
        for d=1:size(indWAnim,1)
            XYmatsTheta{end+1}=[XmatsTheta{cnt} YmatsTheta{cnt}(:,d)];
        end
        
    end
    
end
%removing rows and columns that are all nan
allglmidxstheta2=allglmidxstheta2(find(nansum(allglmidxstheta2')~=0),1:find(nansum(allglmidxstheta2)==0,1));

cnt=0;
%---- Getting ripple model data
allglmidxsrip2=[];
Xmats={};
Ymats={};
XYmats={};

for an = 1:length(modg)
    for i=1:length(modg(an).output{1})
        % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
        %    if ~isempty(modg(an).output{1}(i).nsig)
        cnt=cnt+1;
        anim_index{an}{cnt} = modf(an).output{1}(i).indices;
        
        indNoAnim=modg(an).output{1}(i).glmidxs2;
        indNoAnim=indNoAnim(find(nansum(indNoAnim')~=0),:);
        indWAnim=[an*ones(size(indNoAnim,1),1) indNoAnim];
        % Indices in new form. Each line is one ensemble of cells in the
        % following form:
        % animal day,epoch,hctet,hccell,hctet,hccell...,hctet,hccell,pfctet,pfccell
        allglmidxsrip2 = [allglmidxsrip2; indWAnim]; % Need to put "an" in there
        Xmats{cnt}=modg(an).output{1}(i).Xmat;
        Ymats{cnt}=modg(an).output{1}(i).Ymat;
        for d=1:size(indWAnim,1)
            XYmats{end+1}=[Xmats{cnt} Ymats{cnt}(:,d)];
        end
        
    end
    
end

allglmidxsrip2=allglmidxsrip2(find(nansum(allglmidxsrip2')~=0),1:find(nansum(allglmidxsrip2)==0,1));

% sanity check
numPFCCells=0;for i=1:size(Ymats,2);numPFCCells=numPFCCells+size(Ymats{i},2);end
if numPFCCells~=size(allglmidxsrip2)|numPFCCells~=size(allglmidxstheta2)
    'Numbers dont add up'
    keyboard
end
allinds={};
cnt2=1;
XYmats2={};
XYmats2Theta={};

allPFCCA1sigidxs={};
allglmidxstheta3=[];
%making anim-day-pfctet-pfccell
animdaypfc=[];
for kk=1:size(allglmidxstheta2,1)
    animdaypfc(kk,1:4)=allglmidxstheta2(kk,[1,2,find(isnan(allglmidxstheta2(kk,:)),1)-2,find(isnan(allglmidxstheta2(kk,:)),1)-1]);
end
uniqueIndices=unique(animdaypfc,'rows');
for kk=1:size(uniqueIndices,1)
    %finding all epochs for each pfc cell
    
    ind1=find(ismember(animdaypfc,uniqueIndices(kk,:),'rows'))';
    numepochs=length(ind1);
    
    allcells1=reshape(allglmidxstheta2(ind1(1),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
    allcells1=allcells1(~isnan(allcells1(:,1)),:);
    allca1cells1=allcells1(1:end-1,:);
    X1=XYmats{ind1(1)};
    X1Theta=XYmatsTheta{ind1(1)};
    XYconcat=X1;
    XYconcatTheta=X1Theta;
    
    if numepochs>1
        
        allcells2=reshape(allglmidxstheta2(ind1(2),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
        allcells2=allcells2(~isnan(allcells2(:,1)),:);
        X2=XYmats{ind1(2)};
        X2Theta=XYmatsTheta{ind1(2)};
        
        [C ia ib]=intersect(allcells1,allcells2,'rows');
        XYconcat=[X1(:,ia);X2(:,ib)];
        XYconcatTheta=[X1Theta(:,ia);X2Theta(:,ib)];
        
        allcells1=C;
    end
    if numepochs>2
        
        allcells3=reshape(allglmidxstheta2(ind1(3),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
        allcells3=allcells3(~isnan(allcells3(:,1)),:);
        X3=XYmats{ind1(3)};
        X3Theta=XYmatsTheta{ind1(3)};
        
        [C ia ib]=intersect(allcells1,allcells3,'rows');
        XYconcat=[XYconcat(:,ia);X3(:,ib)];
        XYconcatTheta=[XYconcatTheta(:,ia);X3Theta(:,ib)];
        allcells1=C;
        
        
    end
    if numepochs>3
        
        allcells4=reshape(allglmidxstheta2(ind1(4),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
        allcells4=allcells4(~isnan(allcells4(:,1)),:);
        X4=XYmats{ind1(4)};
        X4Theta=XYmatsTheta{ind1(4)};
        
        [C ia ib]=intersect(allcells1,allcells4,'rows');
        XYconcat=[XYconcat(:,ia);X4(:,ib)];
        XYconcatTheta=[XYconcatTheta(:,ia);X4(:,ib)];
        
        allcells1=C;
    end
    
    XYmats2{end+1}=XYconcat;
    XYmats2Theta{end+1}=XYconcatTheta;
    
    
    animday=allglmidxstheta2(ind1(1),1:2);
    animdayrep=repmat(animday,size(allcells1,1),1);
    allcellswanimday=[animdayrep allcells1];
    allinds{end+1}=allcellswanimday;
    
    
end

scrsz = get(0,'ScreenSize');





allErrRealThetaTheta2=[];
allErrShufThetaTheta2=[];

allPsThetaTheta=[];
allPsKThetaTheta=[];


allErrReal_theta=[];
allErrShuf_theta=[];
allErrRealThetaTheta=[];
allErrShufThetaTheta=[];
allErrReal=[];
allErrShuf=[];
allPs_theta=[];
allPs=[];
allPsK=[];
nsig=[];
fracsig=[];
nsigT=[];
fracsigT=[];
corrPlast=[];
counter=1;
allRealPlast=[];
allShufPlast=[];
figdir = '/data15/gideon/Figs/';
plotGraphs=1;
converged1=[];
converged1theta=[];
fromind=1;
allconcatXYs={};
ii=0;
plotGraphs=1;
% THIS IS NOT THE MOST BEAUTIFUL CODE, BUT IT WORKS. CURRENTLY, IN
% EVERY CELL IN XYMATS2 ALL THE COLUMNS BUT THE LAST ARE FOR CA1 CELLS,
% AND THE LAST IS FOR A SINGLE PFC CELL (THIS IS AFTER COLLAPSING ACROSS
% EPOCHS). THIS CODE MERGES MULTIPLE
% CELLS WITH THE SAME CA1 CELLS, SO THAT WE GET ALL CA1 AND PFC CELLS
% RECORDED SIMULTANEOUSLY IN A SINGLE MATRIX CONCATXY.

examples1=[5 14 37];
%examples1=[5];

% IF YOU WANT TO LOOP, UNCOMMENT HERE
%    while ii<=size(XYmats2,2)
for ii=examples1
    
    runnum=ii;
    ii=ii+1;
    fromind=ii;
    
    
    % the following loop restructures the data in the following way:
    % currently each cell in XYmats2 has some number of CA1 cells and a single
    % PFC cell. This code concatenates the data for cells that have the same
    % CA1 cells
    curca1cells=allinds{fromind}(1:end-1,:);
    nextca1cells=allinds{fromind+1}(1:end-1,:);
    j=1;
    concatXY=XYmats2{fromind};
    
    % this checks that the nest ca1 cells are the same as the current
    while size(curca1cells)==size(nextca1cells)&isequal(curca1cells,nextca1cells)
        if size(concatXY,1)==size(XYmats2{ii+1}(:,end),1)
            
            concatXY=[concatXY XYmats2{ii+1}(:,end)];
            
            j=j+1;
            ii=ii+1;
            nextca1cells=allinds{fromind+j}(1:end-1,:);
        else
            j=j+1;
            ii=ii+1;
            nextca1cells=allinds{fromind+j}(1:end-1,:);
            break
        end
    end
    
    allconcatXYs{end+1}=concatXY;
    fromind=ii+1;
    
    
    nPFCcells=j;
    nCA1cells=size(concatXY,2)-nPFCcells;
    
    [rr pp]=corrcoef([concatXY]);
    regionsLabels=[repmat('CA1',nCA1cells,1);repmat('PFC',nPFCcells,1)];
    % plotting only PFC-CA1 edges (removing CA1-CA1 and PFC-PFC)
    for i=1:length(rr),for j=1:length(rr), if i<=nCA1cells&j<=nCA1cells,rr(i,j)=NaN;end,if i>nCA1cells&j>nCA1cells,rr(i,j)=NaN;end;end;end
    
    
    
    
    % plotting graphs based on correlation values, currently unused
    %figure;
    %plotNiceCorrGraphStraight(rr,pp,regionsLabels,[])
    
    %---- convert to GLM
    
    ball=[];
    pall=[];
    for numPFCcount1=1:nPFCcells
        [b1, ~, s1] = glmfit(concatXY(:,1:nCA1cells),concatXY(:,nCA1cells+numPFCcount1),'poisson');
        ball=[ball;b1(2:end)'];
        pall=[pall;s1.p(2:end)'];
    end
    rrGLM=NaN(size(rr));
    rrGLM((nCA1cells+1):end,1:nCA1cells)=ball;
    ppGLM=NaN(size(pp));
    ppGLM((nCA1cells+1):end,1:nCA1cells)=pall;
    
    
    plotNiceCorrGraphStraight2color2(rrGLM,ppGLM,regionsLabels,[])
    title(['GLM runnum= ' num2str(runnum)])
    % Uncomment for higlighted examples
    
    % if example=5, kk=4
    % if example=37, kk=2
    %for kk=1:nPFCcells
    % for kk=4
    % fig=figure('PaperPosition',[0 0 3 5],'PaperSize',[10 12]);
    % set(gcf,'Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/1.5])
    % subplot(1,100,25:75)
    % % sorting according to PFC firing
    % [qq ww]=sort(concatXY(:,nCA1cells+kk));
    % concatXY=concatXY(ww,:);
    %
    % imagesc(concatXY(:,[1:nCA1cells]));
    %      caxis([0 5])
    % xlabel('CA1 cells')
    % ylabel('# SWR')
    % set(gca,'XAxisLocation','top')
    % subplot(1,100,77:84)
    % imagesc(concatXY(:,nCA1cells+kk));
    %    caxis([0 10])
    % xlabel(['PFC cell' num2str(kk)])
    % set(gca,'XAxisLocation','top')
    % set(gca,'YTicklabel','')
    % set(gca,'XTicklabel',' ')
    % %title(num2str(kk))
    % end
    % %end
    %
    % % close all
    % % HIGHLIGHTING SINGLE RIPPLE PATTERN
    % exampleRip=7:9;
    % fig=figure('PaperPosition',[0 0 3 5],'PaperSize',[10 12]);
    % set(gcf,'Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/6])
    %
    % subplot(3,10,12:18);imagesc(concatXY(exampleRip,[1:nCA1cells]));
    % caxis([0 5])
    % %xlabel('CA1 cells')
    % %ylabel('#ripple')
    % set(gca,'XAxisLocation','top')
    % set(gca,'YTicklabel','')
    %
    % subplot(3,10,19:20)
    % imagesc(concatXY(exampleRip,nCA1cells+kk));
    % caxis([0 10])
    % %xlabel('PFC cell')
    % set(gca,'XAxisLocation','top')
    % set(gca,'YTicklabel','')
    % set(gca,'XTicklabel',' ')
    % %   end
    % % HIGHLIGHTING SINGLE RIPPLE PATTERN
    % exampleRip=[166:168];
    % fig=figure('PaperPosition',[0 0 3 5],'PaperSize',[10 12]);
    % set(gcf,'Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/6])
    %
    % subplot(3,10,12:18);imagesc(concatXY(exampleRip,[1:nCA1cells]));
    % caxis([0 5])
    % %xlabel('CA1 cells')
    % %ylabel('#ripple')
    % set(gca,'XAxisLocation','top')
    % set(gca,'YTicklabel','')
    %
    % subplot(3,10,19:20)
    % imagesc(concatXY(exampleRip,nCA1cells+kk));
    % caxis([0 10])
    % %xlabel('PFC cell')
    % set(gca,'XAxisLocation','top')
    % set(gca,'YTicklabel','')
    % set(gca,'XTicklabel',' ')
    
    keyboard
end


%% from DFSsj_HPexpt_glm_theta5
load('/mnt/data25new/sjadhav/HPExpt/HP_ProcessedDataHP_ripmod_glmfit_theta_gather')
fractionSigPredictableK=mean(allPsK<0.05);
fractionSigPredictableKThetaTheta=mean(allPsKThetaTheta<0.05);
fractionSigPredictableTheta=mean(allPs_theta<0.05);

errDecrease=(allErrReal./allErrShuf);
errDecrease_theta=(allErrReal_theta./allErrShuf_theta);
errDecreaseThetaTheta=(allErrRealThetaTheta2./allErrShufThetaTheta2);

sig0=(allPsK(nsig==0)<0.05);
sig1=(allPsK(nsig==1)<0.05);
sig2=(allPsK(nsig>=2)<0.05);

sig0T=(allPs_theta(nsig==0)<0.05);
sig1T=(allPs_theta(nsig==1)<0.05);
sig2T=(allPs_theta(nsig>=2)<0.05);

[pz0 z0]=ztestprop([sum(sig0),length(sig0)],0.05);
[pz1 z1]=ztestprop([sum(sig1),length(sig1)],0.05);
[pz2 z2]=ztestprop([sum(sig2),length(sig2)],0.05);

[pz0T z0T]=ztestprop([sum(sig0T),length(sig0T)],0.05);
[pz1T z1T]=ztestprop([sum(sig1T),length(sig1T)],0.05);
[pz2T z2T]=ztestprop([sum(sig2T),length(sig2T)],0.05);

numcell=2;
% predicting ripples from ripples
t=[];for i=1:numcell,t=[t mean(allPsK(nsig==(i-1))<0.05)];end
t=[t mean(allPsK(nsig>=(numcell))<0.05)];
tn=[];for i=1:numcell,tn=[tn sum(allPsK(nsig==(i-1))<0.05)];end
tn=[tn sum(allPsK(nsig>=(numcell))<0.05)];
t1=t;

%predicting ripples from theta
t=[];for i=1:numcell,t=[t mean(allPs_theta(nsigT==(i-1))<0.05)];end
t=[t mean(allPs_theta(nsigT>=(numcell))<0.05)];
tn=[];for i=1:numcell,tn=[tn sum(allPs_theta(nsigT==(i-1))<0.05)];end
tn=[tn sum(allPs_theta(nsigT>=(numcell))<0.05)];
t2=t;
figure;
h=bar((0:numcell)-0.12,[t1],'k','barwidth',0.2)
hold on
h2=bar((0:numcell)+0.12,[t2],'b','barwidth',0.2)
set(h2,'facecolor',[1 1 1]*0.7);

xlabel('Number of significant CA1 units');
ylabel('Fraction of predictable PFC cells')
set(gca,'XTick',0:2)
set(gca,'XTickLabel',{'0','1','>=2'})
axis([-0.5 2.5 0 1.5])
set(gca,'box','off')
plot(-0.5:0.05:3.5,0.05,'.-k')

if pz0<0.01
    if pz0<0.001
        if pz0<0.0001
            text(0, 0.95, '***','color','k','fontsize',25)
        else
            text(0, 0.95, '**','color','k','fontsize',25)
        end
    else
        text(0, 0.95, '*','color','k','fontsize',25)
    end
end

if pz1<0.01
    if pz1<0.001
        if pz1<0.0001
            text(0.74, 0.95, '***','color','k','fontsize',25)
        else
            text(0.74, 0.95, '**','color','k','fontsize',25)
        end
    else
        text(0.74, 0.95, '*','color','k','fontsize',25)
    end
end

if pz2<0.01
    if pz2<0.001
        if pz2<0.0001
            text(1.74, 0.95, '***','color','k','fontsize',25)
        else
            text(1.74, 0.95, '**','color','k','fontsize',25)
        end
    else
        text(1.74, 0.95, '*','color','k','fontsize',25)
    end
end

%==========

legend({'trained on ripples','trained on theta'},'location','northwest','fontsize',8)
ylim([0 1])
if pz0T<0.01
    if pz0T<0.001
        if pz0T<0.0001
            text(0, 0.45, '***','color',[1 1 1]*0.6,'fontsize',25)
        else
            text(0, 0.45, '**','color',[1 1 1]*0.6,'fontsize',25)
        end
    else
        text(0, 0.45, '*','color',[1 1 1]*0.6,'fontsize',25)
    end
end

if pz1T<0.01
    if pz1T<0.001
        if pz1T<0.0001
            text(1.04, 0.45, '***','color',[1 1 1]*0.6,'fontsize',25)
        else
            text(1.04, 0.45, '**','color',[1 1 1]*0.6,'fontsize',25)
        end
    else
        text(1.04, 0.45, '*','color',[1 1 1]*0.6,'fontsize',25)
    end
end

if pz2T<0.01
    if pz2T<0.001
        if pz2T<0.0001
            text(2.04, 0.45, '***','color',[1 1 1]*0.6,'fontsize',25)
        else
            text(2.04, 0.45, '**','color',[1 1 1]*0.6,'fontsize',25)
        end
    else
        text(2.04, 0.45, '*','color',[1 1 1]*0.6,'fontsize',25)
    end
end





