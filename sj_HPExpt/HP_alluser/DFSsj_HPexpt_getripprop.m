% Similar to DFSsj_HPexpt_getriprop
% This is to get rip duration: The analysis code can do everrything the positon code does.
% Just get SWR durationhere for now



% Ripple posn: some examples, and get fraction close to reward wells.

% See also paper  notes
% Equivalent is DFSsj_getstimstats_summ / DFAsj_behstats
% Borrow from sj_stim_behstats. Can use it to plot examples outside of filter framework, and also do the whole thing, really
% sj_stim_behstats for each animal, and summary plotted with sj_stimstats_summ1.m

% Started with DFSsj_Hpexpt_getripalignspiking_ver6.m



% ver6: note:
% 1. has all animals
% 2. removed requirement for nrip>1 because it's too harsh

% Ver 4: Sync with everyone

% Ver2, Dec 2013 - Implement Min. NSpike condition for PFC cells. See Line 47 and Line 240

% Ripple modulation of cells, especilally PFC cells. Time filter version of sj_HPexpt_ripalign_singlecell_getrip4.
% Will call DFAsj_getripalign.m
% Also see DFSsj_plotthetamod.m and DFSsj_HPexpt_xcorrmeasures2. Will gather data like these

clear; close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells
%savedir = '/opt/data15/gideon/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';

[y, m, d] = datevec(date);

val=1;
%savefile = [savedir 'HP_rippos_X6'];
savefile = [savedir 'HP_ripprop_X8'];

%val=6;savefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_X6']; area = 'PFC'; clr = 'b';
%val=7;savefile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_X6']; area = 'CA1'; clr = 'r';

savefig1=0;

% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


%If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Nadal','Rosenthal','Borg'};
    %animals = {'HPa','HPb','HPc'};
    %animals = {'Nadal','Rosenthal','Borg'};
    
    %Filter creation
    %-----------------------------------------------------
    
    %runepochfilter = 'isequal($type, ''run'')';
    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    
    
    % Cell filter
    % -----------
    %     switch val
    %
    %         case 6
    %             cellfilter = 'strcmp($area, ''PFC'') && ($numspikes > 100)'; % PFC cells with spiking criterion
    %             %cellfilter = 'strcmp($area, ''PFC'')'; % This includes all, including silent cells
    %         case 7
    %             cellfilter = '(strcmp($area, ''CA1'')|| strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7) ';
    %     end
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % Iterator
    % --------
    %iterator = 'singlecellanal';
    iterator = 'epochbehaveanal';
    
    % Filter creation
    % ----------------
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'iterator', iterator);
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    
    %modf = setfilterfunction(modf,'DFAsj_getrippos',{'ripples', 'tetinfo', 'pos','linpos'},'dospeed',1,'lowsp_thrs',4,'minrip',1); % Default stdev is 3
    modf = setfilterfunction(modf,'DFAsj_HPexpt_getripprop',{'ripples', 'tetinfo', 'pos','linpos'},'dospeed',1,'lowsp_thrs',4,'minrip',1); % Default stdev is 3
    
    %end
    
    % Run analysis
    % ------------
    modf = runfilter(modf);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile);
    end
    
else
    
    %x=1
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------



% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 0; savegatherdata = 1;
[y, m, d] = datevec(date);
switch val
    case 1
        %gatherdatafile = [savedir 'HP_rippos_gather_X6'];
        gatherdatafile = [savedir 'HP_ripprop_gather_X8'];
end


if gatherdata
    
    % Parameters if any
    % -----------------
    
    % -------------------------------------------------------------
    
    cnt=0; % Count how many cells will be kept based on nspikes in output: >0
    allanimindex=[];  all_riprate=[];  allNrip=[];  allpos_atrip=[]; all_ripdurationvec = []; all_ripduration = [];
    %     all_Fracripwell=[];
    %     all_FracripCtrwell=[]; all_FracripSidewell=[]; all_FracripCtrint=[];
    %     allNrip=[]; allNripwell=[]; allNripCtrwell = []; allNripCtrint = [];
    %     allpos=[]; allvel=[]; allwellpos=[]; allintpos=[];
    
    
    % In DFSsj_getbehstats_summ, we already search for days and epoch and separate out by days and epoch
    % while accumulating. Similar thing for sj_stim_behstats
    % Thats nice as you already have variables separated by day. You can just combine across epochs. This will work
    % only when days and epoch are similar across animals, as you loop across days and animals
    
    
    totanim = length(modf);
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
            if (modf(an).output{1}(i).Nrip > 0)
                cnt=cnt+1;
                anim_index{an}(cnt,:) = modf(an).output{1}(i).index;
                % Only indexes
                animindex=[an modf(an).output{1}(i).index]; % Put animal index in front
                allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Index
                
                % Data
                all_riprate(cnt) = modf(an).output{1}(i).riprate;  % Plot this
                allNrip(cnt) =  modf(an).output{1}(i).Nrip;
                allpos_atrip{cnt} = modf(an).output{1}(i).pos_atrip;
                all_rip_durationvec{cnt} = modf(an).output{1}(i).rip_duration;
                % Only get mean of rip_duration
                all_ripduration(cnt) = nanmean(modf(an).output{1}(i).rip_duration);
                
                all_Fracripwell(cnt) = modf(an).output{1}(i).Fracripwell; % Plot this
                all_FracripCtrwell(cnt) = modf(an).output{1}(i).FracripCtrwell;
                all_FracripSidewell(cnt) = modf(an).output{1}(i).FracripSidewell;
                all_FracripCtrint(cnt) = modf(an).output{1}(i).FracripCtrint;
                allNripwell(cnt) =  modf(an).output{1}(i).Nripwell;
                allNripCtrwell(cnt) =  modf(an).output{1}(i).NripCtrwell;
                allNripCtrint(cnt) =  modf(an).output{1}(i).NripCtrint;
                allNripSidewell(cnt) =  modf(an).output{1}(i).NripSidewell;
                allwellpos{cnt} = modf(an).output{1}(i).wellpos;
                allintpos{cnt} = modf(an).output{1}(i).intpos;
                allpos{cnt} = modf(an).output{1}(i).pos;
                allvel{cnt} = modf(an).output{1}(i).vel;
                
                allpripwell{cnt} = modf(an).output{1}(i).pripwell;
                allpripint{cnt} = modf(an).output{1}(i).pripint;
                
                if cnt==1
                    thrsdistint =  modf(an).output{1}(i).thrsdistint;
                    thrsdistwell =  modf(an).output{1}(i).thrsdistwell;
                end
            end
        end
        
    end
    
    
    
    % You can compute and plot without consolidating across epochs. Can also plot across epochs: This is for epochs
    % -------------------------------------------------------------------------------------------------------------
    Global_riprate = mean(all_riprate)
    Global_Fracripwell = mean(all_Fracripwell)
    Global_ripduration = nanmean(all_ripduration), Global_ripduration_err = nansem(all_ripduration)
    
    %Wtrack
    wtr = find(allanimindex(:,1)<=3); % Will be the first 48 epochs
    wtr_riprate = mean(all_riprate(wtr))
    wtr_Fracripwell = mean(all_Fracripwell(wtr))
    wtr_ripduration = mean(all_ripduration(wtr))
    
    %Ytrack
    ytr = find(allanimindex(:,1)>3);
    ytr_riprate = mean(all_riprate(ytr))
    ytr_Fracripwell = mean(all_Fracripwell(ytr))
    ytr_ripduration = mean(all_ripduration(ytr))
    
    [hrate, prate] = kstest2(all_riprate(wtr),all_riprate(ytr)), % p=
    [hfrac, pfrac] = kstest2(all_Fracripwell(wtr),all_Fracripwell(ytr)) % p=0.02
    [hduration, pduration] = kstest2(all_ripduration(wtr),all_ripduration(ytr)),
    
    figure; hold on;
    bar([nanmean(all_ripduration(wtr))  nanmean(all_ripduration(ytr))]); hold on;
    errorbar2([1 2], [nanmean(all_ripduration(wtr))  nanmean(all_ripduration(ytr))], [nansem(all_ripduration(wtr))  nansem(all_ripduration(ytr))], 0.3, 'k')
    title('SWR dur ','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2],'XTickLabel',{'Wtr';'Ytr'});
    ylabel('SWR dur (ms)','FontSize',24,'FontWeight','normal')
    
    
    figure; hold on;
    bar([mean(all_riprate(wtr))  mean(all_riprate(ytr))]); hold on;
    errorbar2([1 2], [mean(all_riprate(wtr))  mean(all_riprate(ytr))], [stderr(all_riprate(wtr))  stderr(all_riprate(ytr))], 0.3, 'k')
    title('SWR rate ','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2],'XTickLabel',{'Wtr';'Ytr'});
    ylabel('SWR rate (Hz)','FontSize',24,'FontWeight','normal')
    
    
    figure; hold on;
    bar([mean(all_Fracripwell(wtr))  mean(all_Fracripwell(ytr))]); hold on;
    errorbar2([1 2], [mean(all_Fracripwell(wtr))  mean(all_Fracripwell(ytr))], [stderr(all_Fracripwell(wtr))  stderr(all_Fracripwell(ytr))], 0.3, 'k')
    title('Frac SWRs at wells ','FontSize',24,'FontWeight','normal');
    set(gca,'XTick',[1 2],'XTickLabel',{'Wtr';'Ytr'});
    ylabel('Frac SWRs at wells','FontSize',24,'FontWeight','normal')
    
    
    
    % Consolidate across epochs. Multiple methods: see also DFSsj_getcellinfo and DFSsj_xcorrmeasures2
    % ----------------------------------------------------------------------------
    
    allripple = struct; allripple_idx=[];
    all_riprated=[]; all_Fracripwelld=[]; all_ripdurationd=[];
    
    % ---------
    dummyindex=allanimindex;  % all anim-day-epoch indices
    cntdays=0;
    for i=1:length(allNrip)
        animday=allanimindex(i,[1 2]);
        ind=[];
        while rowfind(animday,dummyindex(:,[1 2]))~=0          % collect all rows (epochs)
            ind = [ind rowfind(animday,dummyindex(:,[1 2]))];        % finds the first matching row
            dummyindex(rowfind(animday,dummyindex(:,[1 2])),:)=[0 0 0]; % after adding index, remove the corresponding row
            % so you could find the next one
        end
        
        % Gather everything for the current cell across epochs
        currriprate=[]; currFracripwell=[]; currNrip=0; currFracripCtrwell=[]; currFracripSidewell=[]; currFracripCtrint=[];
        currpos = []; currpos_atrip=[]; currwellpos=[]; currintpos=[]; currpripwell=[]; currpripint=[];
        currripduration=[];
        
        for r=ind
            currNrip = currNrip + allNrip(r);
            
            currripduration = [currripduration; all_ripduration(r)];
            currriprate = [currriprate; all_riprate(r)];
            currFracripwell = [currFracripwell; all_Fracripwell(r)];
            currFracripCtrwell = [currFracripCtrwell; all_FracripCtrwell(r)];
            currFracripSidewell = [currFracripSidewell; all_FracripSidewell(r)];
            currFracripCtrint = [currFracripCtrint; all_FracripCtrint(r)];
            
            currpos = [currpos; allpos{r}];
            currpos_atrip = [currpos_atrip; allpos_atrip{r}];
            currpripwell = [currpripwell; allpripwell{r}];
            currpripint = [currpripint; allpripint{r}];
            
            if isempty(currwellpos)
                currwellpos = allwellpos{r};
                currintpos = allintpos{r};
            end
        end
        
        if ~isempty(currriprate)
            cntdays = cntdays+1
            allripple_idx(cntdays,:)=animday;
            allripple(cntdays).index=animday;
            
            allripple(cntdays).ripduration=mean(currripduration);
            allripple(cntdays).Nrip=currNrip;
            allripple(cntdays).riprate=mean(currriprate);
            allripple(cntdays).Fracripwell=mean(currFracripwell);
            allripple(cntdays).FracripCtrwell=mean(currFracripCtrwell);
            allripple(cntdays).FracripSidewell=mean(currFracripSidewell);
            allripple(cntdays).FracripCtrint=mean(currFracripCtrint);
            
            allripple(cntdays).pos=currpos;
            allripple(cntdays).pos_atrip=currpos_atrip;
            allripple(cntdays).wellpos=currwellpos;
            allripple(cntdays).intpos=currintpos;
            allripple(cntdays).pripwell=currpripwell;
            allripple(cntdays).pripint=currpripint;
            
            all_riprated(cntdays) = mean(currriprate);
            all_Fracripwelld(cntdays) = mean(currFracripwell);
            all_ripdurationd(cntdays) = nanmean(currripduration);
            
        end
    end
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
        %return;
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data



% Plot population with day-variables
% ----------------------------------

% Temporarily Change
tmp1 = all_riprate;
tmp2 = all_Fracripwell;
tmp3 = all_ripduration;

all_riprate = all_riprated;
all_Fracripwell = all_Fracripwelld;
all_ripduration = all_ripdurationd;

Global_riprate = mean(all_riprated)  % 0.1149
Global_riprateerr = sem(all_riprated) % 0.0056
Global_Fracripwell = mean(all_Fracripwelld) %0.76
Global_Fracripwellerr = sem(all_Fracripwelld) %0.02
Global_ripduration = nanmean(all_ripduration) %96.7
Global_ripdurationerr = nansem(all_ripduration) %1.2

%Wtrack
wtr = find(allripple_idx(:,1)<=3); % Will be the first 48 epochs
wtr_riprate = mean(all_riprated(wtr)) % 0.12
wtr_Fracripwell = mean(all_Fracripwelld(wtr)), %0.71
wtr_Fracripwellerr = sem(all_Fracripwelld(wtr)) %0.03
wtr_ripduration = nanmean(all_ripdurationd(wtr))
wtr_ripdurationerr = nansem(all_ripdurationd(wtr))

%Ytrack
ytr = find(allripple_idx(:,1)>3);
ytr_riprate = mean(all_riprated(ytr))  % 0.10
ytr_Fracripwell = mean(all_Fracripwelld(ytr)) %0.79
ytr_Fracripwellerr = sem(all_Fracripwelld(ytr)) %0.02
ytr_ripduration = nanmean(all_ripdurationd(ytr))
ytr_ripdurationerr = nansem(all_ripdurationd(ytr))

[hrate, prate] = kstest2(all_riprated(wtr),all_riprated(ytr)), % p = v. high
[hfrac, pfrac] = kstest2(all_Fracripwelld(wtr),all_Fracripwelld(ytr)) % p=0.03
[hfract, pfract] = ttest2(all_Fracripwelld(wtr),all_Fracripwelld(ytr)) % p=0.01
[hdur, pdur] = kstest2(all_ripdurationd(wtr),all_ripdurationd(ytr)), % p = v. high

figure; hold on;
bar([nanmean(all_ripduration(wtr))  nanmean(all_ripduration(ytr))]); hold on;
errorbar2([1 2], [nanmean(all_ripduration(wtr))  nanmean(all_ripduration(ytr))], [nansem(all_ripduration(wtr))  nansem(all_ripduration(ytr))], 0.3, 'k')
title('SWR dur ','FontSize',24,'FontWeight','normal');
set(gca,'XTick',[1 2],'XTickLabel',{'Wtr';'Ytr'});
ylabel('SWR dur (ms)','FontSize',24,'FontWeight','normal')

figure; hold on;
bar([mean(all_riprate(wtr))  mean(all_riprate(ytr))]); hold on;
errorbar2([1 2], [mean(all_riprate(wtr))  mean(all_riprate(ytr))], [stderr(all_riprate(wtr))  stderr(all_riprate(ytr))], 0.3, 'k')
title('SWR rate ','FontSize',24,'FontWeight','normal');
set(gca,'XTick',[1 2],'XTickLabel',{'Wtr';'Ytr'});
ylabel('SWR rate (Hz)','FontSize',24,'FontWeight','normal')


figure; hold on;
bar([mean(all_Fracripwell(wtr))  mean(all_Fracripwell(ytr))]); hold on;
errorbar2([1 2], [mean(all_Fracripwell(wtr))  mean(all_Fracripwell(ytr))], [stderr(all_Fracripwell(wtr))  stderr(all_Fracripwell(ytr))], 0.3, 'k')
title('Frac SWRs at wells ','FontSize',24,'FontWeight','normal');
set(gca,'XTick',[1 2],'XTickLabel',{'Wtr';'Ytr'});
ylabel('Frac SWRs at wells','FontSize',24,'FontWeight','normal')


% Change back
all_riprate = tmp1;
all_Fracripwell = tmp2;








% --------------------------------------------------------------------
% Plotting for individual epochs/ days: Use the function itself
% --------------------------------------------------------------------

forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

figdir = '/data25/sjadhav/HPExpt/Figures/RipBehStats/Indiv/';
summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 24;
    xfont = 24;
    yfont = 24;
end

wellclr = {'r','m','g'};

% Can go over days (cntdays) or over epochs (cnt)

figopt1=1; saveg1=0;
%thrsdistwell = 15; %in cm, for defining well boundary
%thrsdistint = 15; %in cm, for defining intersection boundary

if (figopt1)
    %     for i=1:cntdays
    %         curridx = allripple(i).index;
    %         currpos = allripple(i).pos;
    %         pos_atrip = allripple(i).pos_atrip;
    %         pripwell = allripple(i).pripwell;
    %         pripint = allripple(i).pripint;
    %         wellpos = allripple(i).wellpos;
    %         intpos = allripple(i).intpos;
    %         riprate =  allripple(i).riprate;
    %         Fracripwell =  allripple(i).Fracripwell;
    
    for i=60:cnt
        curridx =  allanimindex(i,:);
        currpos = allpos{i};
        pos_atrip = allpos_atrip{i};
        pripwell =  allpripwell{i};
        pripint = allpripint{i};
        wellpos = allwellpos{i};
        intpos = allintpos{i};
        riprate =  all_riprate(i)
        Fracripwell =  all_Fracripwell(i);
        
        
        anim = curridx(1); day = curridx(2); ep = curridx(3);
        figure; hold on; redimscreen;
        %redimscreen_2x2subplots
        % Plot rippos spreads
        plot(currpos(:,1),currpos(:,2),'color',[0.9 0.9 0.9])
        plot(pos_atrip(:,1),pos_atrip(:,2),'.')
        % plot Well and Intersection positions, and circles around it
        for i=1:3
            plot(wellpos(i,1),wellpos(i,2),[wellclr{i} 's'],'MarkerSize',12,'MarkerFaceColor',[wellclr{i}]);
            plot(pripwell{i}(:,1),pripwell{i}(:,2),[wellclr{i} '.']);
            if i==1
                plot(intpos(i,1),intpos(i,2),[wellclr{i} 's'],'MarkerSize',12,'MarkerFaceColor',[wellclr{i}]);
                plot(pripint{i}(:,1),pripint{i}(:,2),[wellclr{i} '.']);
            end
            
            %plot(pstimarm{i}(:,1),pstimarm{i}(:,2),[wellclr{i} '.']);
        end
        
        %plot(wellpos(:,1),wellpos(:,2),'rs','MarkerSize',12,'MarkerFaceColor','r');
        %plot(intpos(:,1),intpos(:,2),'cs','MarkerSize',12,'MarkerFaceColor','c');
        
        % Plot circle
        for i=1:3
            N=256;
            circle(wellpos(i,:),thrsdistwell,N,'r-');
            if i==1
                circle(intpos(i,:),thrsdistint,N,'c-');
            end
        end
        %axis([-5 150 -5 150]);
        xlabel('X-position (mm)');
        ylabel('Y-position (mm)');
        title(['Posn at Rip: Anim' num2str(anim) ' Day' num2str(day) '; Nrip: ' num2str(length(pos_atrip)) '; Rate: ' num2str(roundn(riprate)) ' Hz; Fracripwell: ' num2str(roundn(Fracripwell))],'FontSize',20,'Fontweight','normal');
        
        keyboard;
        
    end % end cntdays
end % end if figopt



