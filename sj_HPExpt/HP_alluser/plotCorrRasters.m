% based on predurACCA1GLM4

%% --------------- START: Predicting CA1 SWR activity from preceding AC
savedirX = '/opt/data15/gideon/HP_ProcessedData/';
% the following files were created by
% DFSsj_HPexpt_getripalignspiking_ver5.m (I think..)


load([savedirX 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6'])
allripplemodCA1=allripplemod;
allripplemod_idxCA1=allripplemod_idx;

load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6'])
%load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6symmetricwindow']);

allripplemodPFC=allripplemod;
allripplemod_idxPFC=allripplemod_idx;

% combining AC and PFC!!
% allripplemodAC=[allripplemodAC allripplemodPFC];
% allripplemod_idxAC=[allripplemod_idxAC ; allripplemod_idxPFC]

combined_idx=unique([allripplemod_idxPFC(:,1:2)],'rows');
%
% numtimewindows=size(timewindows,1);
% ALLSIGRATIOS=NaN(numtimewindows,numtimewindows);
% ALLERRORRATIOS=NaN(numtimewindows,numtimewindows);

numTrain=1000;
scrsz = get(0,'ScreenSize');
% for ACtimewindow=1:numtimewindows
%     for CA1timewindow=1:numtimewindows
xx=[-500:500];
converged1=[];
n = [];
nsig = [];
fracsig=[];
allErrReal=[];
allErrShuf=[];
allPs=[];
allPsK=[];
allinds=[];
allbetas={};
allsigs={};
allsigbetasvec=[];
allcrosscorrs=[];
allcrosscorrsSWRexc=[];
allcrosscorrsSWRinh=[];
allcrosscovsSWRexc=[];
allcrosscovsSWRinh=[];

plotpairs=0;
%b=fir1(51,0.05);
b=gaussian(20,61);
allpeaks=[];
allpeaksS=[];
allpeaksSWRexc=[];
allpeaksSWRinh=[];
timer=0;
plotres=0
crosscorrpeaktimes=[];
pfcsigdifs=[];
pfcmods=[];
pfclatepeakinds=[];
pfcearlypeakinds=[];
pfclatetroughinds=[];
pfcearlytroughinds=[];
pcorrs=[];
rcorrs=[];
% swr-exc =1
% swr-inh =2
rtimings=[];
ptimings=[];
b=gaussian(30,41);
for v=1:size(combined_idx,1)
    curidx=combined_idx(v,[1:2]);%animal day
    PFCidx=find(ismember(allripplemod_idxPFC(:,1:2),curidx,'rows'));
    CA1idx=find(ismember(allripplemod_idxCA1(:,1:2),curidx,'rows'));
    PFCmat=[];
    for j=1:size(PFCidx,1)
        for k=1:size(CA1idx,1)
            PFCind1=PFCidx(j);
            CA1ind1=CA1idx(k);
            if allripplemodPFC(1,PFCind1).rasterShufP2<0.05 & allripplemodCA1(1,CA1ind1).rasterShufP2<0.05
                fullPFCind=allripplemod_idxPFC(PFCind1,:);
                fullCA1ind=allripplemod_idxCA1(CA1ind1,:);
                
                PFChist=rast2mat(allripplemodPFC(1,PFCind1).raster);
                CA1hist=rast2mat(allripplemodCA1(1,CA1ind1).raster);
                PFCamp=sum(PFChist(:,500:700),2);
                CA1amp=sum(CA1hist(:,500:700),2);
                if length(PFCamp)==length(CA1amp)
                [ripcorR ripcorP]=corrcoef(PFCamp,CA1amp);
                if ripcorP(1,2)<0.05 %& abs(ripcorR(1,2))>0.1
                    CA1fires=find(CA1amp>0);
                    CA1silent=find(CA1amp==0);
                    
                    [s1 s2]=sort(CA1amp);
       
                    figure('Position',[300,300,700 800]);
                    subplot(2,2,1)
                    imagesc(1-ceil(filter2(ones(4)/16,PFChist(s2,:))));
                    colormap(gray)
                    title(num2str(fullPFCind))
                    subplot(2,2,3)
                    plot(filtfilt(b,1,mean(PFChist)),'k','linewidth',2);hold on;
                    plot(filtfilt(b,1,mean(PFChist(CA1fires,:))),'r','linewidth',1);
                    plot(filtfilt(b,1,mean(PFChist(CA1silent,:))),'g','linewidth',1);

                    subplot(2,2,2);
                    imagesc(1-ceil(filter2(ones(4)/16,CA1hist(s2,:))));
                    colormap(gray)
                    title(num2str(fullCA1ind))
                    subplot(2,2,4)
                    plot(filtfilt(b,1,mean(CA1hist)),'k','linewidth',2);hold on;
                    plot(filtfilt(b,1,mean(CA1hist(CA1fires,:))),'r','linewidth',1);
                    plot(filtfilt(b,1,mean(CA1hist(CA1silent,:))),'g','linewidth',1);
                    
                    title(['ripcorR= ' num2str(ripcorR(1,2))])
                    keyboard
                    close all
                end
                end
%                 if strcmp(allripplemodCA1(1,CA1ind1).type,'exc')
%                     % is the pfc cell swr-excited or swr-inhibited
%                     pfcSWRexcinh=strcmp(allripplemodPFC(1,PFCind1).type,'exc');
%                   %  mean(mean(PFChist(:,501:650)))>mean(mean(PFChist(:,250:500)));
%                     %pfcSWRexcinh=mean(mean(PFChist(:,501:700)))>mean(mean(PFChist(:,50:300)));
%                     
%                     %now columns are ripples
%                     CA1hist2=CA1hist';
%                     PFChist2=PFChist';
%                     if size(PFChist2,2)==size(CA1hist2,2)
%                         PFCh=PFChist2(:);
%                         CA1h=CA1hist2(:);
%                         currawcrosscorr=xcorr(PFCh,CA1h,500,'coeff')';
%                         curcrosscorr=filtfilt(b,1,currawcrosscorr);
%                         
%                         % this outputs the same as currawcrosscor except the
%                         % units, which I think are just co-occurrences
%                         spikexcorrOut=spikexcorr(find(PFCh),find(CA1h),1,500);
%                         
%                         % cross-cov calculation from Siapas paper- verify
%                         c1vsc2=spikexcorrOut.c1vsc2;
%                         T=length(PFCh)/1000;
%                         bin=0.001;
%                         spikerate1=sum(PFCh)/T;
%                         spikerate2=sum(CA1h)/T;
%                         xc = (c1vsc2/(bin*T))-(spikerate1*spikerate2);
%                         xcQ= xc*(sqrt((bin*T)./(spikerate1*spikerate2)));
%                         nstd=round(0.01/bin); % 10ms/bin
%                         g1 = gaussian(nstd, 5*nstd+1);
%                         crosscov1 = smoothvect(xcQ, g1);
%                         
%                         % the PFC cell is SWR-excited
%                         if pfcSWRexcinh
%                             %allcrosscorrsSWRexc=[allcrosscorrsSWRexc; curcrosscorr];
%                             allcrosscovsSWRexc=[allcrosscovsSWRexc; crosscov1];
%                             % find peak time
%                             [a maxind]=max(curcrosscorr(251:750));
%                             allpeaksSWRexc=[allpeaksSWRexc maxind+250];
%                         
%                         else % the PFC cell is SWR-inhibited
%                             %allcrosscorrsSWRinh=[allcrosscorrsSWRinh; curcrosscorr];
%                             allcrosscovsSWRinh=[allcrosscovsSWRinh; crosscov1];
%                             % find trough time
%                             [a minind]=min(curcrosscorr(251:750));
%                             allpeaksSWRinh=[allpeaksSWRinh minind+250];
%                         end
%                         
%                         
%                        
%                         timer=timer+1
%                         g2=gaussian(10,51);
%                         g3=gaussian(3,3);
%                         xaxis=-499:500;
%                         % CA1 spiking vs not spiking
%                         [firstSpike swrNum]=find(cumsum(cumsum(CA1hist(:,500:700)'))==1);
%                         CA1wspikes=CA1hist(swrNum,:);
%                         CA1wNospikes=CA1hist(setdiff(1:size(CA1hist,1),swrNum),:);
%                         PFCwCA1spikes=PFChist(swrNum,:);
%                         PFCwNoCA1spikes=PFChist(setdiff(1:size(CA1hist,1),swrNum),:);
%                         pfcampwspikes=sum(PFCwCA1spikes(:,500:700),2);
%                         pfcampnospikes=sum(PFCwNoCA1spikes(:,500:700),2);
%                         %does the PFC cell fire more on SWRs that the CA1
%                         %fired than when the CA1 was silent?
%                         pfcsigdif=ranksum(pfcampwspikes,pfcampnospikes);
%                         pfcsigdifs=[pfcsigdifs pfcsigdif];
%                         % depth of modulation of PFC cell
%                         pfcmod=(mean(pfcampwspikes)-mean(pfcampnospikes))/(mean(pfcampwspikes)+mean(pfcampnospikes));
%                         pfcmods=[pfcmods pfcmod];
%                         
%                         % CA1 spiking temporal order
%                         [sortedFirstSpike sortedFirstSpikeX]=sort(firstSpike);
%                         CA1wspikesSorted=CA1wspikes(sortedFirstSpikeX,:);
%                         PFCwCA1spikesSorted=PFCwCA1spikes(sortedFirstSpikeX,:);
%                         numswrsWspikes=size(CA1wspikes,1);
%                         pfcearly=smoothvect(mean(PFCwCA1spikesSorted(1:round(numswrsWspikes/2),:)),g2);
%                         pfclate=smoothvect(mean(PFCwCA1spikesSorted(round(numswrsWspikes/2)+1:end,:)),g2);
%                        
%                         pfcmatsmooth=gaussian(40,50);
%                         smoothedpfc=(filtfilt(pfcmatsmooth,1,PFCwCA1spikes')');
%                         smoothedpfc=zscore(smoothedpfc')';
%                         
%                         % For PFC SWR-excited cells, looking at timing only if:
%                         % 1. The PFC cell is significantly positively modulated
%                         % 2. There are enough SWRs where CA1 fired
%                         % 3. The PFC cell fires enough spikes during CA1 firing
%                         if pfcSWRexcinh & pfcsigdif<0.05 & pfcmod>0 & size(CA1wspikes,1)>20 & sum(sum(PFCwCA1spikes))>30
%                             [pfclatepeakamp pfclatepeakind]=max(pfclate(500:800));
%                             [pfcearlypeakamp pfcearlypeakind]=max(pfcearly(500:800));
%                             pfclatepeakinds=[pfclatepeakinds pfclatepeakind];
%                             pfcearlypeakinds=[pfcearlypeakinds pfcearlypeakind];
%                             [maxamppfc maxamptimepfpc]=max(smoothedpfc(:,500:800)');
%                             sigpeaks=find(maxamppfc>1);
%                             plotres2=1;
%                             %                             if length(sigpeaks>20)
% %                             [rtiming ptiming]=corrcoef(maxamptimepfpc(sigpeaks),firstSpike(sigpeaks));
% %                             rtimings=[rtimings rtiming(2,1)];
% %                             ptimings=[ptimings ptiming(2,1)];
% %                             end
%                             
%                             % For PFC SWR-inhibited cells, looking at timing only if:
%                             % 1. The PFC cell is significantly negatively modulated
%                             % 2. There are enough SWRs where CA1 fired
%                         elseif ~pfcSWRexcinh & pfcsigdif<0.05 & pfcmod<0 & size(CA1wspikes,1)>20
%                             [pfclatetroughamp pfclatetroughind]=min(pfclate(500:800));
%                             [pfcearlytroughamp pfcearlytroughind]=min(pfcearly(500:800));
%                             pfclatetroughinds=[pfclatetroughinds pfclatetroughind];
%                             pfcearlytroughinds=[pfcearlytroughinds pfcearlytroughind];
%                          plotres2=1;
%                         else
%                             plotres2=0;
%                         end
%                         
%                         
%                         if plotres & plotres2
%                             if 1%pfcsigdif<0.05 & mean(pfcampwspikes)>mean(pfcampnospikes)&size(CA1wNospikes,1)>30&sum(sum(PFCwCA1spikes))>30
%                                 
%                                 % CA1 spiking vs not spiking
%                                 figure('Position',[30 400 scrsz(3)/4 800]);
%                                 g2=gaussian(10,51);
%                                 subplot(3,2,1);
%                                 imagesc(1-ceil(filtfilt(g3,1,CA1wspikes')'));
%                                 title('CA1 w spikes')
%                                 subplot(3,2,3);
%                                 imagesc(1-ceil(filtfilt(g3,1,CA1wNospikes')'));
%                                 colormap(gray)
%                                 title('CA1 w No spikes')
%                                 subplot(3,2,5);
%                                 plot(smoothvect(mean(CA1wspikes),g2),'r');hold on;plot(smoothvect(mean(CA1wNospikes),g2))
%                                 legend('CA1 spikes','CA1 No spikes')
%                                 subplot(3,2,2);
%                                 imagesc(1-ceil(filtfilt(g3,1,PFCwCA1spikes')'));
%                                 title('PFC w CA1 spikes')
%                                 subplot(3,2,4);
%                                 imagesc(1-ceil(filtfilt(g3,1,PFCwNoCA1spikes')'));
%                                 colormap(gray)
%                                 title('PFC w No CA1 spikes')
%                                 subplot(3,2,6)
%                                 plot(smoothvect(mean(PFCwCA1spikes),g2),'r');hold on;plot(smoothvect(mean(PFCwNoCA1spikes),g2))
%                                 title(pfcsigdif)
%                                 
%                                 set(0,'defaultAxesFontName', 'Arial')
%                                 set(0,'defaultUIControlFontName', 'Arial')
%                                 set(0,'defaultTextFontName', 'Arial')
%                                 set(0,'defaultaxesfontsize',40);
%                                 
%                                 figure('Position',[1200 400 scrsz(3)/4 800])
%                                 subplot(2,2,1);
%                                 imagesc(xaxis,1:size(CA1wspikesSorted,1),1-ceil(filtfilt(g3,1,CA1wspikesSorted')'));colormap(gray);
%                                 title('CA1 w spikes sorted')
%                                 xlabel('Time (ms)')
%                                 ylabel('SWR','FontSize',40)
%                                 subplot(2,2,2);
%                                 imagesc(xaxis,1:size(PFCwCA1spikesSorted,1),1-ceil(filtfilt(g3,1,PFCwCA1spikesSorted')'));colormap(gray)
%                                 title('PFC w CA1 spikes sorted')
%                                 xlabel('Time (ms)')
%                                 ylabel('SWR')
%                                 subplot(2,2,3)
%                                 plot(xaxis,1000*smoothvect(mean(CA1wspikesSorted(1:round(numswrsWspikes/2),:)),g2),'linewidth',2)
%                                 hold on
%                                 plot(xaxis,1000*smoothvect(mean(CA1wspikesSorted(round(numswrsWspikes/2)+1:end,:)),g2),'r','linewidth',2)
%                                 legend('early CA1 spikes','late CA1 spikes')
%                                 xlabel('Time (ms)')
%                                 ylabel('Smoothed mean peri-SWR time histogram (spikes/S)')
%                                 subplot(2,2,4)
%                                 plot(xaxis,1000*smoothvect(mean(PFCwCA1spikesSorted(1:round(numswrsWspikes/2),:)),g2),'linewidth',2)
%                                 hold on
%                                 plot(xaxis,1000*smoothvect(mean(PFCwCA1spikesSorted(round(numswrsWspikes/2)+1:end,:)),g2),'r','linewidth',2)
%                                 xlabel('Time (ms)')
%                                 legend('early CA1 spikes','late CA1 spikes')
%                                 if pfcSWRexcinh
%                                 title(['erly pk= ' num2str(pfcearlypeakind) ' late pk= ' num2str(pfclatepeakind) ])
%                                 else
%                                 title(['erly trough= ' num2str(pfcearlytroughind) ' late= ' num2str(pfclatetroughind) ])
% 
%                                 end
%                                 ylabel('Smoothed mean peri-SWR time histogram (spikes/S)')
%                                 
%                                 keyboard
%                                 close all
%                             end
%                         end
                        
                        
                    
                end
            end
        end
    end



