%savedir = '/opt/data15/gideon/HP_ProcessedData/';

savedir = '/data25/sjadhav/HPexpt/HP_ProcessedData/';
xaxis=-500:10:499;
load([savedir 'HP_PFCPFCThetacov_std3_speed4_ntet2_alldata_excexc_gather_X6']);
Sexcexc=SallZcrosscov_sm_runtheta_epcomb;

savedir = '/data25/sjadhav/HPexpt/HP_ProcessedData/';
load([savedir 'HP_PFCPFCThetacov_std3_speed4_ntet2_alldata_excinh_gather_X6']);
Sexcinh=SallZcrosscov_sm_runtheta_epcomb;

savedir = '/data25/sjadhav/HPexpt/HP_ProcessedData/';
load([savedir 'HP_PFCPFCThetacov_std3_speed4_ntet2_alldata_inhinh_gather_X6']);
Sinhinh=SallZcrosscov_sm_runtheta_epcomb;

%%
figure('Position',[1200 400 600 1200])
subplot(3,1,1);
ccampexcexc=mean(Sexcexc(:,40:60),2);
[xx orderexcexc]=sort(ccampexcexc,'descend');
Sexcexc=Sexcexc(orderexcexc,:);
imagesc(xaxis,1:size(Sexcexc,1),Sexcexc)
caxis([-10 10]);colorbar
title('EXC/EXC')
xlabel('Time (ms)')
ylabel('Pairs')
xlim([-450 450])

subplot(3,1,2);
ccampexcinh=mean(Sexcinh(:,40:60),2);
[xx orderexcinh]=sort(ccampexcinh,'descend');
Sexcinh=Sexcinh(orderexcinh,:);
imagesc(xaxis,1:size(Sexcinh,1),Sexcinh)
caxis([-10 10]);colorbar
title('EXC/INH')
xlabel('Time (ms)')
ylabel('Pairs')
xlim([-450 450])

% this is the same cell twice
Sinhinh=Sinhinh([1:34,36:end],:);
ccampinhinh=mean(Sinhinh(:,40:60),2);
[xx orderinhinh]=sort(ccampinhinh,'descend');
Sinhinh=Sinhinh(orderinhinh,:);
subplot(3,1,3);
imagesc(xaxis,1:size(Sinhinh,1),Sinhinh)
caxis([-10 10]);colorbar
title('INH/INH')
xlabel('Time (ms)')
ylabel('Pairs')
xlim([-450 450])
%% all cross-covs
figure('Position',[1200 400 600 1200])
subplot(3,1,1);
plot(xaxis,Sexcexc,'b')
title('EXC/EXC')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
axis([-450 450 -6 10])

subplot(3,1,2);
plot(xaxis,Sexcinh,'k')
title('EXC/INH')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
axis([-450 450 -6 10])

subplot(3,1,3);
plot(xaxis,Sinhinh,'r')
title('INH/INH')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
axis([-450 450 -6 10])
%%

ccmaxinhinh=max((Sinhinh(:,40:60)'));
[xx orderinhinh2]=sort(ccmaxinhinh,'descend');
Sinhinh2=Sinhinh(orderinhinh2,:);

ccmaxexcinh=max((Sexcinh(:,40:60)'));
[xx orderexcinh2]=sort(ccmaxexcinh,'descend');
Sexcinh2=Sexcinh(orderexcinh2,:);

ccmaxexcexc=max((Sexcexc(:,40:60)'));
[xx orderexcexc2]=sort(ccmaxexcexc,'descend');
Sexcexc2=Sexcexc(orderexcexc2,:);


figure('Position',[1200 400 600 1200])
subplot(3,1,1);
plot(xaxis,Sexcexc(1,:),'b','linewidth',2)
title('EXC/EXC')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
axis([-450 450 -6 10])

subplot(3,1,2);
plot(xaxis,Sexcinh(1,:),'k','linewidth',2)
title('EXC/INH')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
axis([-450 450 -6 10])

subplot(3,1,3);
plot(xaxis,Sinhinh(8,:),'r','linewidth',2)
title('INH/INH')
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
axis([-450 450 -6 10])

%%
figure('Position',[1200 400 600 800])
shadedErrorBar(xaxis,mean(Sexcexc),std(Sexcexc)./sqrt(size(Sexcexc,1)),'-b',1);
hold on
shadedErrorBar(xaxis,mean(Sexcinh),std(Sexcinh)./sqrt(size(Sexcinh,1)),'-k',1);
shadedErrorBar(xaxis,mean(Sinhinh),std(Sinhinh)./sqrt(size(Sinhinh,1)),'-r',1);
axis([-450 450 -1 2.2])
box off
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
%%

plot(xaxis,mean(Sexcexc),'b','linewidth',2);
hold on;
plot(xaxis,mean(Sexcinh),'k','linewidth',2);
plot(xaxis,mean(Sinhinh),'r','linewidth',2);
axis([-450 450 -1 2])
xlabel('Time (ms)')
ylabel('Standardized cross-cov')
legend({'EXC/EXC','EXC/INH','INH/INH'})

%%
excexcthetapeakcorr=max(Sexcexc');
excinhthetapeakcorr=max(Sexcinh');
inhinhthetapeakcorr=max(Sinhinh');

figure;
barwitherr([std(excexcthetapeakcorr)/sqrt(length(excexcthetapeakcorr)) std(excinhthetapeakcorr)/sqrt(length(excinhthetapeakcorr)) std(inhinhthetapeakcorr)/sqrt(length(inhinhthetapeakcorr))],[mean(excexcthetapeakcorr) mean(excinhthetapeakcorr) mean(inhinhthetapeakcorr)])
set(gca,'xticklabel',{'EXC/EXC','EXC/INH','INH/INH'})
ylabel('Peak standardized cross-cov')


X=nan(73,3);
X(1:42,1)=excexcthetapeakcorr';
X(:,2)=excinhthetapeakcorr';
X(1:36,3)=inhinhthetapeakcorr';
anova1(X)
[p table stats]=anova1(X)
c=multcompare(stats)
multcompare(stats)