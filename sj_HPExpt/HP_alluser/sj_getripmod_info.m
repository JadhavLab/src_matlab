
% Load the "ripplemod" file and the "cellinfo" file for PFC cells, and add
% a "FStag" field to the allripplemod structure identifying the FS cells

clear;
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
ripplefile = [savedir 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6_CA2tag'];
load(ripplefile); % Load the entire file since you have to eventually save it

anim = allripplemod_idx(:,1);
Wtr_CA1 = length(find(anim <=3)),
Ytr_CA1 = length(find(anim>3)),

% Get FS idxs
cntFS=0; FSripmod=[]; FSidx=[];
for i = 1:size(allripplemod_idx,1)    
    curridx = allripplemod_idx(i,:);
    
    if strcmp(allripplemod(i).FStag,'y');

        cntFS=cntFS+1;
        FSidx=[FSidx;curridx],

        if allripplemod(i).rasterShufP2<0.05
            FSripmod = [FSripmod; 1],
        else
            FSripmod = [FSripmod; 0],
        end
    end


%     curridx = FSidx(i,:); 
%     match = rowfind(curridx, allripplemod_idx) 
%     if match~=0
%         cntFS=cntFS+1;
%         allripplemod(match).FStag = 'y';
%         if allripplemod(match).rasterShufP2<0.05
%             FSripmod = [FSripmod; 1];
%         else
%             FSripmod = [FSripmod; 0];
%         end
%     end
    
    
end




% PFC Ripplemod file
% --------------
ripplefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6']; 
%ripplefile = [savedir 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_3-6-2014']; 
%load(ripplefile, 'allripplemod','allripplemod_idx'); % load allripplemod and allripplemod_idx. 
load(ripplefile); % Load the entire file since you have to eventually save it

anim = allripplemod_idx(:,1);
Wtr_PFC = length(find(anim <=3)),
Ytr_PFC = length(find(anim>3)),

