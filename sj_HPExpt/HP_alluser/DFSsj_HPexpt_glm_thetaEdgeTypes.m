

clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells
plotGraphs=1;
%savedir = 'mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';

savedir = '/opt/data15/gideon/HP_ProcessedData/';

%savedir = '/data15/gideon/ProcessedData/';
%val=1; savefile = [savedir 'HP_ripmod_glmfit_theta6allripmod']; area = 'PFC'; clr = 'b'; % PFC
val=2; savefile = [savedir 'HP_ripmod_glmfit_theta6ripexc']; area = 'PFC'; clr = 'b'; % PFC
%val=3; savefile = [savedir 'HP_ripmod_glmfit_theta6ripinh']; area = 'PFC'; clr = 'b'; % PFC
%val=4; savefile = [savedir 'HP_ripmod_glmfit_theta6ripunmod']; area = 'PFC'; clr = 'b'; % PFC
%val=5; savefile = [savedir 'HP_ripmod_glmfit_theta6All']; area = 'PFC'; clr = 'b'; % PFC


savefig1=0;


% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Nadal','Rosenthal'};
    
    runepochfilter = 'isequal($type, ''run'')';
    
    
    switch val
        case 1
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($FStag, ''n'') && strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y''))';
        case 2
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($FStag, ''n'') && strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc''))';
        case 3
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($FStag, ''n'') && strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh''))';
        case 4
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($FStag, ''n'') && strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''n''))';
        case 5
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($FStag, ''n'') && strcmp($area, ''PFC'') && ($numspikes > 100))';
            
    end
    
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    timefilter_place_new = { {'DFTFsj_getvelpos', '(($absvel >= 5))'},...
        {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',3} };
    
    
    iterator = 'multicellanal';
    
    
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter,  'excludetime', timefilter_place_new, 'iterator', iterator);
    
    modg = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter, 'iterator', iterator);
    
    disp('Done Filter Creation');
    
    
    modg = setfilterfunction(modg,'DFAsj_glm_ripalign_dataForTheta',{'ripplemod','cellinfo'}); %
    modf = setfilterfunction(modf,'DFAsj_glm_thetaGR2',{'spikes','cellinfo'},'thrstime',1); % With includetime condition
    
    
    
    modf = runfilter(modf);
    modg = runfilter(modg);
    disp('Finished running filter script');
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------

% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
switch val
    case 1
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6allripmodEdgeTypes'];
    case 2
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6ripexcEdgeTypes'];
    case 3
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6ripinhEdgeTypes'];
    case 4
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6unripmodEdgeTypes'];
    case 5
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6AllEdgeTypes'];
end



if gatherdata
    %----- Getting theta model parameters
    cnt=0;
    %Glm
    allglmidxstheta=[];    allglmidxstheta2=[];
    allmodelbtheta=[]; allmodelbtheta2=[];allmodelptheta=[]; allmodelfitstheta=[];
    allnsigtheta=[]; allnsigpostheta=[]; allnsignegtheta=[];  allfracsigtheta=[]; allfracsigpostheta=[]; allfracsignegtheta=[];
    XmatsTheta={};
    YmatsTheta={};
    XYmatsTheta={};
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            cnt=cnt+1;
            anim_index{an}{cnt} = modf(an).output{1}(i).indices;
            
            indNoAnim1=modf(an).output{1}(i).glmidxs;
            indWAnim1=[an*ones(size(indNoAnim1,1),1) indNoAnim1];
            allglmidxstheta = [allglmidxstheta; indWAnim1]; % Need to put "an" in there
            
            indNoAnim=modf(an).output{1}(i).glmidxs2;
            try
                indNoAnim=indNoAnim(find(nansum(indNoAnim')~=0),:);
            catch
                keyboard
            end
            indWAnim=[an*ones(size(indNoAnim,1),1) indNoAnim];
            allglmidxstheta2 = [allglmidxstheta2; indWAnim];
            XmatsTheta{cnt}=modf(an).output{1}(i).Xmat;
            YmatsTheta{cnt}=modf(an).output{1}(i).Ymat;
            for d=1:size(indWAnim,1)
                XYmatsTheta{end+1}=[XmatsTheta{cnt} YmatsTheta{cnt}(:,d)];
            end
            
        end
        
    end
    %removing rows and columns that are all nan
    allglmidxstheta2=allglmidxstheta2(find(nansum(allglmidxstheta2')~=0),1:find(nansum(allglmidxstheta2)==0,1));
    
    cnt=0;
    %---- Getting ripple model data
    allglmidxsrip2=[];
    Xmats={};
    Ymats={};
    XYmats={};
    
    for an = 1:length(modg)
        for i=1:length(modg(an).output{1})
            cnt=cnt+1;
            anim_index{an}{cnt} = modf(an).output{1}(i).indices;
            
            indNoAnim=modg(an).output{1}(i).glmidxs2;
            indNoAnim=indNoAnim(find(nansum(indNoAnim')~=0),:);
            indWAnim=[an*ones(size(indNoAnim,1),1) indNoAnim];
            % Indices in new form. Each line is one ensemble of cells in the
            % following form:
            % animal day,epoch,hctet,hccell,hctet,hccell...,hctet,hccell,pfctet,pfccell
            allglmidxsrip2 = [allglmidxsrip2; indWAnim]; % Need to put "an" in there
            Xmats{cnt}=modg(an).output{1}(i).Xmat;
            Ymats{cnt}=modg(an).output{1}(i).Ymat;
            for d=1:size(indWAnim,1)
                XYmats{end+1}=[Xmats{cnt} Ymats{cnt}(:,d)];
            end
            
        end
        
    end
    
    allglmidxsrip2=allglmidxsrip2(find(nansum(allglmidxsrip2')~=0),1:find(nansum(allglmidxsrip2)==0,1));
    
    % sanity check
    numPFCCells=0;for i=1:size(Ymats,2);numPFCCells=numPFCCells+size(Ymats{i},2);end
    if numPFCCells~=size(allglmidxsrip2)|numPFCCells~=size(allglmidxstheta2)
        'Numbers dont add up'
        keyboard
    end
    
    cnt2=1;
    XYmats2={};
    XYmats2Theta={};
    XYinds={};
    
    allPFCCA1sigidxs={};
    allglmidxstheta3=[];
    %making anim-day-pfctet-pfccell
    animdaypfc=[];
    for kk=1:size(allglmidxstheta2,1)
        animdaypfc(kk,1:4)=allglmidxstheta2(kk,[1,2,find(isnan(allglmidxstheta2(kk,:)),1)-2,find(isnan(allglmidxstheta2(kk,:)),1)-1]);
    end
    uniqueIndices=unique(animdaypfc,'rows');
    for kk=1:size(uniqueIndices,1)
        %finding all epochs for each pfc cell
        
        ind1=find(ismember(animdaypfc,uniqueIndices(kk,:),'rows'))';
        numepochs=length(ind1);
        
        allcells1=reshape(allglmidxstheta2(ind1(1),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
        allcells1=allcells1(~isnan(allcells1(:,1)),:);
        allca1cells1=allcells1(1:end-1,:);
        X1=XYmats{ind1(1)};
        X1Theta=XYmatsTheta{ind1(1)};
        XYconcat=X1;
        XYconcatTheta=X1Theta;
        
        if numepochs>1
            
            allcells2=reshape(allglmidxstheta2(ind1(2),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
            allcells2=allcells2(~isnan(allcells2(:,1)),:);
            X2=XYmats{ind1(2)};
            X2Theta=XYmatsTheta{ind1(2)};
            
            [C ia ib]=intersect(allcells1,allcells2,'rows');
            XYconcat=[X1(:,ia);X2(:,ib)];
            XYconcatTheta=[X1Theta(:,ia);X2Theta(:,ib)];
            
            allcells1=C;
        end
        if numepochs>2
            
            allcells3=reshape(allglmidxstheta2(ind1(3),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
            allcells3=allcells3(~isnan(allcells3(:,1)),:);
            X3=XYmats{ind1(3)};
            X3Theta=XYmatsTheta{ind1(3)};
            
            [C ia ib]=intersect(allcells1,allcells3,'rows');
            XYconcat=[XYconcat(:,ia);X3(:,ib)];
            XYconcatTheta=[XYconcatTheta(:,ia);X3Theta(:,ib)];
            allcells1=C;
            
            
        end
        if numepochs>3
            
            allcells4=reshape(allglmidxstheta2(ind1(4),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
            allcells4=allcells4(~isnan(allcells4(:,1)),:);
            X4=XYmats{ind1(4)};
            X4Theta=XYmatsTheta{ind1(4)};
            
            [C ia ib]=intersect(allcells1,allcells4,'rows');
            XYconcat=[XYconcat(:,ia);X4(:,ib)];
            XYconcatTheta=[XYconcatTheta(:,ia);X4(:,ib)];
            
            allcells1=C;
        end
        
        XYmats2{end+1}=XYconcat;
        XYmats2Theta{end+1}=XYconcatTheta;
        
        
        animday=allglmidxstheta2(ind1(1),1:2);
        animdayrep=repmat(animday,size(allcells1,1),1);
        allcellswanimday=[animdayrep allcells1];

        XYinds{end+1}=allcellswanimday;

    end
    
    
    allErrRealThetaTheta2=[];
    allErrShufThetaTheta2=[];
    allPsThetaTheta=[];
    allErrReal_theta=[];
    allErrShuf_theta=[];
    allErrRealThetaTheta=[];
    allErrShufThetaTheta=[];
    allErrReal=[];
    allErrShuf=[];
    allPs_theta=[];
    allPs=[];
    allPsK=[];
    nsig=[];
    fracsig=[];
    nsigT=[];
    fracsigT=[];
    corrPlast=[];
    counter=1;
    allRealPlast=[];
    allShufPlast=[];
    figdir = '/data15/gideon/Figs/';
    plotGraphs=1;
    converged1=[];
    converged1theta=[];
    allnumrips=[];
    allnumcells=[];
    matchinds=[];
    allSigPairsBetaSignsFromCA1=[];
    allSigPairsBetaSignsIntoPFC=[];
allPosProportionFromCA1=[];
allPosProportionIntoPFC=[];
    for ii=1:size(XYmats2,2)
        ii
        currXY=XYmats2{ii};
        currX=currXY(:,1:end-1);
        currY=currXY(:,end);
        
        currXYTheta=XYmats2Theta{ii};
        currXTheta=currXYTheta(:,1:end-1);
        currYTheta=currXYTheta(:,end);
        
        nPFCcells=size(currY,2);
        nCA1cells=size(currX,2);
        
        curInds=XYinds{ii};
        curCA1Inds=curInds(1:end-1,:);
        curPFCInd=curInds(end,:);
        
        %         if plotGraphs
        %             [rr pp]=corrcoef([currX currY]);
        %             regionsLabels=[repmat('CA1',nCA1cells,1);repmat('PFC',nPFCcells,1)];
        %             % plotting only PFC-CA1 edges (removing CA1-CA1 and PFC-PFC)
        %             for i=1:length(rr),for j=1:length(rr), if i<=nCA1cells&j<=nCA1cells,rr(i,j)=NaN;end,if i>nCA1cells&j>nCA1cells,rr(i,j)=NaN;end;end;end
        %             plotNiceCorrGraph(rr,pp,regionsLabels)
        %             animStr=num2str(allglmidxsrip2(counter,1));
        %
        %             dayStr=num2str(allglmidxsrip2(counter,2));
        %             epochStr=num2str(allglmidxsrip2(counter,3));
        %             title(['Rip- corrs, anim=' animStr ' day=' dayStr ' ep=' epochStr])
        %            % saveas(gcf,[figdir 'graph' animStr dayStr epochStr 'B'],'jpg')
        %             keyboard
        %             close all
        %         end
        
        lastwarn('');
        
        [btrall, ~, statsall] = glmfit(currX,currY,'poisson');
        if isempty(lastwarn)
         matchinds=[matchinds;curCA1Inds repmat(curPFCInd,nCA1cells,1) btrall(2:end) statsall.p(2:end) ];
            
            
        end
        % continue only if converged
       
    end
    ensemblesizethresh=2;
    % analyzing multiple edges coming out from same CA1 node
    uniqueCA1cells=unique(matchinds(:,1:4),'rows');
    for tt=1:size(uniqueCA1cells,1)
       curCA1cell=uniqueCA1cells(tt,:);
       curPFCmatchesInds=find(ismember(matchinds(:,1:4),curCA1cell,'rows'));
       betaVals=matchinds(curPFCmatchesInds,9);
       pVals=matchinds(curPFCmatchesInds,10);
       sigBetaValsSign=sign(betaVals(pVals<0.05));
       numedges=length(sigBetaValsSign);
       if numedges>=ensemblesizethresh
           posProportion=mean(sigBetaValsSign>0);
           allPosProportionFromCA1=[allPosProportionFromCA1 posProportion];
       end
       if length(curPFCmatchesInds)>1
        allUniquePairs = nchoosek(curPFCmatchesInds,2);
        numPairs=size(allUniquePairs,1);
        
        pairsBetaVals=reshape(matchinds(allUniquePairs',9),2,numPairs)';
        pairsPVals=reshape(matchinds(allUniquePairs',10),2,numPairs)';
        significantPairs=find(sum(pairsPVals'<0.05)==2);
        
        sigPairsBetaVals=pairsBetaVals(significantPairs,:);
        
        allSigPairsBetaSignsFromCA1=[allSigPairsBetaSignsFromCA1;sign(sigPairsBetaVals)];
       end
        
        
    end
    
    pairresFromCA1=sum(allSigPairsBetaSignsFromCA1');
    
    bothposFromCA1=sum(pairresFromCA1==2);
    bothnegFromCA1=sum(pairresFromCA1==-2);
    posnegFromCA1=sum(pairresFromCA1==0);
    
    figure;
    hist(allPosProportionFromCA1,0:0.1:1);xlim([-0.1 1.1]);set(get(gca,'child'),'FaceColor','k','EdgeColor','k','linewidth',2)
    title('from CA1')
    xlabel('Propotion of positive betas')
    ylabel('Ensemble count')
    
    % analyzing multiple edges going into same PFC node
    uniquePFCcells=unique(matchinds(:,5:8),'rows');
    for tt=1:size(uniquePFCcells,1)
        curPFCcell=uniquePFCcells(tt,:);
       curCA1matchesInds=find(ismember(matchinds(:,5:8),curPFCcell,'rows'));
       betaVals=matchinds(curCA1matchesInds,9);
       pVals=matchinds(curCA1matchesInds,10);
       sigBetaValsSign=sign(betaVals(pVals<0.05));

       if length(sigBetaValsSign)>=ensemblesizethresh
           posProportion=mean(sigBetaValsSign>0);
           allPosProportionIntoPFC=[allPosProportionIntoPFC posProportion];
       end
       
       if length(curCA1matchesInds)>1
        allUniquePairs = nchoosek(curCA1matchesInds,2);
        numPairs=size(allUniquePairs,1);
        
        pairsBetaVals=reshape(matchinds(allUniquePairs',9),2,numPairs)';
        pairsPVals=reshape(matchinds(allUniquePairs',10),2,numPairs)';
        significantPairs=find(sum(pairsPVals'<0.05)==2);
        
        sigPairsBetaVals=pairsBetaVals(significantPairs,:);
        
        allSigPairsBetaSignsIntoPFC=[allSigPairsBetaSignsIntoPFC;sign(sigPairsBetaVals)];
       end
        
        
    end
    
    pairresIntoPFC=sum(allSigPairsBetaSignsIntoPFC');
    
    bothposIntoPFC=sum(pairresIntoPFC==2);
    bothnegIntoPFC=sum(pairresIntoPFC==-2);
    posnegIntoPFC=sum(pairresIntoPFC==0);
    
    figure;
    hist(allPosProportionIntoPFC,0:0.1:1);xlim([-0.1 1.1]);set(get(gca,'child'),'FaceColor','k','EdgeColor','k','linewidth',2)
    title('Into PFC')
    xlabel('Propotion of positive betas')
    ylabel('Ensemble count')

    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
   % load(gatherdatafile);
    
end % end gather data

%%