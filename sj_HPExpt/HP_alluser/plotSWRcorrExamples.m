% based on ripTrigFiringOnsetAnalysis3
scrsz = get(0,'ScreenSize');
savedirX = '/opt/data15/gideon/HP_ProcessedData/';
plotIndividualCells=1;
areas1={'CA1','PFC'}

histOns=[];
histOnsW=[];
allxcorrLags={};
allallripmodhists={};
allallripmodhistsW={};
allallripmodhistssig={};
allallripmodhistsWsig={};
allanimdayvecRip={};
allanimdayvecRipW={};
allallripmodhistssigInd={};
allallripmodhiststype={};

% how to smooth psths
%b=fir1(81,0.06);
b=gaussian(20,61);

for areaInd=1:2
    area1=areas1{areaInd};
    switch area1
        
        case 'CA1'
            
            load([savedirX 'HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6'])
            allripplemod_idx=[];
            for w=1:length(allripplemod),allripplemod_idx=[allripplemod_idx;allripplemod(w).index];end
            
        case 'PFC'
            
            load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6']);
           %  load([savedirX 'HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6symmetricwindow']);
            
            allripplemod_idx=[];
            for w=1:length(allripplemod),allripplemod_idx=[allripplemod_idx;allripplemod(w).index];end
            
    end
    allinds=unique([allripplemod_idx],'rows');
    allripmodhists=[];
    allripmodhistssig=[];
    allripmodhistssigInd=[];
    allripmodonset3=[];
    allripmodhiststype=[];
    for i=1:length(allripplemod)
        %if ~isempty(allripplemod(i).ripmodonset3)
        %   allripmodonset3=[allripmodonset3 allripplemod(i).ripmodonset3];
        %        allripmodhists=[allripmodhists; zscore(mean(allripplemod(i).hist))];
        allripmodhists=[allripmodhists; zscore(filtfilt(b,1,mean(rast2mat(allripplemod(i).raster))))];
        
        if allripplemod(i).rasterShufP2<0.05
            
            %            allripmodhistssig=[allripmodhistssig; zscore(mean(allripplemod(i).hist))];
            allripmodhistssig=[allripmodhistssig; zscore(filtfilt(b,1,mean(rast2mat(allripplemod(i).raster))))];
            allripmodhistssigInd=[allripmodhistssigInd; allripplemod(i).index];
            % 1 for swr-exc, 0 for swr-inh
            allripmodhiststype=[allripmodhiststype strcmp(allripplemod(i).type, 'exc')];
        end
        %else
        %   allripmodonset3=[allripmodonset3 nan];
        %allripmodhists=[allripmodhists; nan(1,size(allripmodhists,2))];
        
        %    end;
    end
    
    
    allallripmodhists{end+1}=allripmodhists;
    allallripmodhistssig{end+1}=allripmodhistssig;
    allallripmodhistssigInd{end+1}=allripmodhistssigInd;
    allallripmodhiststype{end+1}=allripmodhiststype;

    cntcellsRip=length(allripplemod);
    
    %     if areaInd==2
    %     if plotIndividualCells
    %         for kk=1:length(allripplemod)
    %         figure('Position',[300 200 scrsz(3)/3 600])
    %
    %
    %             imagesc(rast2mat(allripplemod(kk).raster));colormap(gray)
    %
    %
    %         keyboard
    %         end
    %     end
    %  end
end
CA1psths=allallripmodhistssig{1};
CA1inds=allallripmodhistssigInd{1};
PFCpsths=allallripmodhistssig{2};
PFCinds=allallripmodhistssigInd{2};
%% RATE OF SWR-MODULATED CELLS

CA1sleepSWRmodrate=size(CA1psths,1)./size(allallripmodhists{1},1)
PFCsleepSWRmodrate=size(PFCpsths,1)./size(allallripmodhists{2},1)



%% ripple-triggered spiking in all regions. Currently sorted by response amplitude, can
% also sort by days using:
% [sortedDays2 sortedDaysS2]=sort(allanimdayvecRip{2}(:,2),'descend');

% % sleep
% figure('Position',[900 100 scrsz(3)/5 scrsz(4)])
% xaxis=-500:10:500;
% subplot(2,1,1);
% [sorted1 sortedS1]=sort(mean(CA1psths(:,50:70),2),'descend');
% imagesc(xaxis,1:size(CA1psths,1),CA1psths(sortedS1,:));
% xlim(zoomWindow)
% title('Ripple-triggered PSTHs, all rip-mod cells, CA1');xlabel('Time (ms)');ylabel('Cells')
% subplot(2,1,2);
% [sorted2 sortedS2]=sort(mean(PFCpsths(:,50:70),2),'descend');
% imagesc(xaxis,1:size(PFCpsths,1),PFCpsths(sortedS2,:));
% xlim(zoomWindow)
% title('Ripple-triggered PSTHs, all rip-mod cells, PFC');xlabel('Time (ms)');ylabel('Cells')
% annotation('textbox', [0.5, 1, 0, 0], 'string', 'Sleep','fontsize',20)
%% unsorted
zoomWindow=[-500 500];
figure('Position',[900 100 scrsz(3)/5 scrsz(4)])
xaxis=-499:500;
subplot(2,1,1);
imagesc(xaxis,1:size(CA1psths,1),CA1psths);
xlim(zoomWindow)
title('Ripple-triggered PSTHs, all rip-mod cells, CA1');xlabel('Time (ms)');ylabel('Cells')
caxis([-5 5])
subplot(2,1,2);
imagesc(xaxis,1:size(PFCpsths,1),PFCpsths);
caxis([-4 4])
xlim(zoomWindow)
title('Ripple-triggered PSTHs, all rip-mod cells, PFC');xlabel('Time (ms)');ylabel('Cells')
annotation('textbox', [0.5, 1, 0, 0], 'string', 'Sleep','fontsize',20)

%%  exc/inh

%excinh=sign(mean(PFCpsths(:,500:650),2)-mean(PFCpsths(:,250:500),2));
excinhPFC=allallripmodhiststype{2};
excPFCpsths=PFCpsths(excinhPFC>0,:);
%removing psths with no clear peak
% noclearpeak=[11 25 36];
% excPFCpsths=excPFCpsths(setdiff(1:size(excPFCpsths,1),noclearpeak),:);

inhPFCpsths=PFCpsths(excinhPFC==0,:);

%excinh1=sign(mean(CA1psths(:,50:65),2)-mean(CA1psths(:,25:50),2));
% FOR DEMETRIS: INDICES OF SIG SWR-EXC/INH
PFCindsExc=PFCinds(excinhPFC>0,:);
PFCindsInh=PFCinds(excinhPFC==0,:);


figure('Position',[900 100 scrsz(3)/5 scrsz(4)]);

subplot(3,1,1)
imagesc(xaxis,1:size(CA1psths,1),CA1psths)
caxis([-4 4]);xlim([-400 400])
title('CA1');xlabel('Time (ms)')



subplot(3,1,2)
imagesc(xaxis,1:size(excPFCpsths,1),excPFCpsths)
title('sig rip-exc');xlabel('Time (ms)')

caxis([-4 4]);xlim([-400 400])
subplot(3,1,3)
imagesc(xaxis,1:size(inhPFCpsths,1),inhPFCpsths)
title('sig rip-inh');xlabel('Time (ms)')

caxis([-4 4]);xlim([-400 400])
%%
% figure
% hold on
% shadedErrorBar(xaxis,mean(CA1psths,1),std(CA1psths)./sqrt(size(CA1psths,1)),'-k',1);
% shadedErrorBar(xaxis,mean(PFCpsths,1),std(PFCpsths)./sqrt(size(PFCpsths,1)),'-r',1);

figure
hold on
shadedErrorBar(xaxis,mean(CA1psths,1),std(CA1psths)./sqrt(size(CA1psths,1)),'-k',1);
%shadedErrorBar(xaxis,mean(PFCpsths,1),std(PFCpsths)./sqrt(size(PFCpsths,1)),'-r',1);
shadedErrorBar(xaxis,mean(excPFCpsths,1),std(excPFCpsths)./sqrt(size(excPFCpsths,1)),'-b',1);
shadedErrorBar(xaxis,mean(inhPFCpsths,1),std(inhPFCpsths)./sqrt(size(inhPFCpsths,1)),'-r',1);
xlim([-400 400])
ylabel('Mean z-scored psth')
xlabel('Time (ms)')
%% another look at the SWR-excited PFC cells
[tmp timingind]=max(excPFCpsths(:,251:750)');



[A B]=sort(timingind);
figure;subplot(2,1,1);imagesc(xaxis,1:length(B),excPFCpsths(B,:));caxis([-3 3])
xlabel('Time (ms)')
ylabel('#Cell')
title('PFC SWR-excited cells')
% another look at the SWR-inhibited PFC cells
[tmp timingind2]=min(inhPFCpsths(:,251:750)');
[A2 B2]=sort(timingind2);
xlim([-400 400])

subplot(2,1,2);imagesc(xaxis,1:length(B2),inhPFCpsths(B2,:));caxis([-3 3])
xlim([-400 400])

xlabel('Time (ms)')
ylabel('#Cell')
title('PFC SWR-inhibited cells')
%% unsorted 2

figure;subplot(2,1,1);imagesc(xaxis,1:size(excPFCpsths,1),excPFCpsths);caxis([-3 3])
xlabel('Time (ms)')
ylabel('#Cell')
title('PFC SWR-excited cells')
subplot(2,1,2);imagesc(xaxis,1:size(inhPFCpsths,1),inhPFCpsths);caxis([-3 3])
xlabel('Time (ms)')
ylabel('#Cell')
title('PFC SWR-inhibited cells')
%% With CA1 (also looking only at SWR-excited)
excinhCA1=allallripmodhiststype{1};

excCA1psths=CA1psths(excinhCA1>0,:);
inhCA1psths=CA1psths(excinhCA1==0,:);


[tmp timingindCA1]=max(excCA1psths(:,251:750)');
[A2 B2]=sort(timingindCA1);
figure;
subplot(2,1,1)
imagesc(xaxis,1:length(B2),excCA1psths(B2,:));caxis([-3 3])
xlabel('Time (ms)')
ylabel('#Cell')
title('CA1 SWR-excited cells')
xlim([-400 400])

subplot(2,1,2)
imagesc(xaxis,1:length(B),excPFCpsths(B,:));caxis([-3 3])
xlabel('Time (ms)')
ylabel('#Cell')
xlim([-400 400])

%% peak timing
bins1=-500:40:500;
[pfc1 t]=hist(timingind-250,bins1);
[pfc2 t]=hist(timingind2-250,bins1);

[CA11 t]=hist(timingindCA1-250,bins1);
figure
plot(bins1,CA11/sum(CA11),'k','linewidth',2)
hold on;
plot(bins1,pfc1/sum(pfc1),'linewidth',2);
plot(bins1,pfc2/sum(pfc2),'r','linewidth',2)
ylabel('fraction of units')
xlabel('Peak time relative to SWR onset')
legend('CA1','PFC swr-exc','PFC swr-inh')
%% timing of start of rise to peak
risestartsPFCexc=[];
risestartsPFCinh=[];
risestartsCA1=[];

for i=1:size(excPFCpsths,1)
    [a maxind]=max(excPFCpsths(i,251:750));
    maxind=maxind+250;
    aa=excPFCpsths(i,1:maxind);
    risestart=find(aa<mean(excPFCpsths(i,:))+std(excPFCpsths(i,:)),1,'last');
    risestartsPFCexc=[risestartsPFCexc risestart];
end
for i=1:size(inhPFCpsths,1)
    [a maxind]=min(inhPFCpsths(i,251:750));
    maxind=maxind+250;
    aa=inhPFCpsths(i,1:maxind);
    risestart=find(aa>mean(inhPFCpsths(i,:))-std(inhPFCpsths(i,:)),1,'last');
    risestartsPFCinh=[risestartsPFCinh risestart];
end
for i=1:size(excCA1psths,1)
    [a maxind]=max(excCA1psths(i,251:750));
    maxind=maxind+250;
    aa=excCA1psths(i,1:maxind);
    risestart=find(aa<mean(excCA1psths(i,:))+std(excCA1psths(i,:)),1,'last');
    risestartsCA1=[risestartsCA1 risestart];
end

%figure;plot(risestarts);hold on;plot(risestartsCA1,'r')
[risePFCexc t]=hist(risestartsPFCexc-500,bins1);
[risePFCinh t]=hist(risestartsPFCinh-500,bins1);
[riseCA1 t]=hist(risestartsCA1-500,bins1);

figure
plot(bins1,riseCA1/sum(riseCA1),'k','linewidth',2)
hold on;
plot(bins1,risePFCexc/sum(risePFCexc),'linewidth',2);
plot(bins1,risePFCinh/sum(risePFCinh),'r','linewidth',2)
ylabel('fraction of units')
xlabel('Rise time relative to SWR onset')
legend('CA1','PFC swr-exc','PFC swr-inh')

%% modulation duration
risedurPFCexc=[];
risedurPFCinh=[];
risedurCA1=[];

for i=1:size(excPFCpsths,1)
    [a maxind]=max(excPFCpsths(i,251:750));
    maxind=maxind+250;
    aa=excPFCpsths(i,1:maxind);
    risestart=find(aa<mean(excPFCpsths(i,:))+std(excPFCpsths(i,:)),1,'last');
    bb=excPFCpsths(i,maxind:end);
    riseend=maxind+find(bb<mean(excPFCpsths(i,:))+std(excPFCpsths(i,:)),1,'first');
    curdur=riseend-risestart;
    risedurPFCexc=[risedurPFCexc curdur];
end

for i=1:size(inhPFCpsths,1)
    [a maxind]=min(inhPFCpsths(i,251:750));
    maxind=maxind+250;
    aa=inhPFCpsths(i,1:maxind);
    risestart=find(aa>mean(inhPFCpsths(i,:))-std(inhPFCpsths(i,:)),1,'last');
    bb=inhPFCpsths(i,maxind:end);
    riseend=maxind+find(bb>mean(inhPFCpsths(i,:))-std(inhPFCpsths(i,:)),1,'first');
    curdur=riseend-risestart;
    risedurPFCinh=[risedurPFCinh curdur];
end

for i=1:size(excCA1psths,1)
    [a maxind]=max(excCA1psths(i,251:750));
    maxind=maxind+250;
    aa=excCA1psths(i,1:maxind);
    risestart=find(aa<mean(excCA1psths(i,:))+std(excCA1psths(i,:)),1,'last');
    bb=excCA1psths(i,maxind:end);
    riseend=maxind+find(bb<mean(excCA1psths(i,:))+std(excCA1psths(i,:)),1,'first');
    curdur=riseend-risestart;
    risedurCA1=[risedurCA1 curdur];
end

bins2=0:10:250;

%figure;plot(risestarts);hold on;plot(risestartsCA1,'r')
[risePFCexc t]=hist(risedurPFCexc,bins2)
[risePFCinh t]=hist(risedurPFCinh,bins2)
[riseCA1 t]=hist(risedurCA1,bins2)

figure
plot(bins2,riseCA1/sum(riseCA1),'k','linewidth',2)
hold on;
plot(bins2,risePFCexc/sum(risePFCexc),'linewidth',2);
plot(bins2,risePFCinh/sum(risePFCinh),'r','linewidth',2)
ylabel('fraction of units')
xlabel('SWR modulation duration (ms)')
legend('CA1','PFC swr-exc','PFC swr-inh')



