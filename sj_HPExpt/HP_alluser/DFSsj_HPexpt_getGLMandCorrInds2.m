
%INCOMPLETE
% GLM model for theta times. Similar to DFSsj_HPexpt_glm_ripalign.m
% Use initial filter settings from "DFSsj_HPexpt_ThetacorrAndRipresp_ver3.m"
% In the function DFAsj_glm_theta.m (based on DFAsj_glm_ripalign), get the time series with theta times
% similar to the beginnin of the function"DFAsj_getthetacrosscov_timecondition.m"

% -----------------
% Glm model for PFC rip-align response as a function of rip-aligned CA1 popln responses
% Goal is to get coefficients and significance
% Get ripple aligned responses directly from ripplemod structure
% Also get pairwise corrlns for now, without shuffle sign for speed. You cal always compare to saved apirwise corrln files later


clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells
plotGraphs=1;
savedir = '/mnt/data25new/sjadhav/HPExpt/HP_ProcessedData';

val=1; savefile = [savedir 'HP_ripmod_glmfit_theta_inds2']; area = 'PFC'; clr = 'b'; % PFC
%val=2; savefile = [savedir 'HP_ripmod_glmfit_theta_inds_unmod2']; area = 'PFC'; clr = 'b'; % PFC


savefig1=0;


% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Nadal'};
    %   animals = {'Nadal'};
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    %dayfilter = ''; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    %sleepepochfilter = 'isequal($type, ''sleep'')'; % Only pre and post sleep marked as sleep
    
    % %Cell filter
    % %-----------
    % %PFC
    % %----
    % && strcmp($thetamodtag, ''y'')
    switch val
        case 1
            % All Ca1 cells and PFC Ripple modulated cells. Function will parse them out.
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag2, ''y'')) ';
    case 2
            % All Ca1 cells and PFC Ripple modulated cells. Function will parse them out.
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag2, ''n'')) ';
 
    
    end
    
    % Time filter - none.
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % Use absvel instead of linearvel
    timefilter_place_new = { {'DFTFsj_getvelpos', '(($absvel >= 5))'},...
        {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',3} };
    
    % Iterator
    % --------
    iterator = 'multicellanal';
    
    % Filter creation
    % ----------------
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter,  'excludetime', timefilter_place_new, 'iterator', iterator);
    
    modg = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter, 'iterator', iterator);
    
    disp('Done Filter Creation');
    
    % Set analysis function
    % ----------------------
    
    % % Need spikes to get time series
    % % If it is across regions, you will need cellinfo to get area where cells are recorded from
    modg = setfilterfunction(modg,'DFAsj_glm_ripalign_dataForTheta',{'ripplemod','cellinfo'}); %
    modf = setfilterfunction(modf,'DFAsj_glm_thetaGR2',{'spikes','cellinfo'},'thrstime',1); % With includetime condition
    
    
    % Run analysis
    % ------------
    modg = runfilter(modg);
    modf = runfilter(modf);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------

% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
switch val
    case 1
        gatherdatafile = [savedir '/allPFCCA1sigidxs_mod2'];
         case 2
        gatherdatafile = [savedir '/allPFCCA1sigidxs_unmod2'];
end



if gatherdata
    %----- Getting theta model parameters
    cnt=0;
    %Glm
    allglmidxstheta=[];    allglmidxstheta2=[];
    allmodelbtheta=[]; allmodelbtheta2=[];allmodelptheta=[]; allmodelfitstheta=[];
    allnsigtheta=[]; allnsigpostheta=[]; allnsignegtheta=[];  allfracsigtheta=[]; allfracsigpostheta=[]; allfracsignegtheta=[];
    
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
     %       if ~isempty(modf(an).output{1}(i).nsig)
                cnt=cnt+1;
                anim_index{an}{cnt} = modf(an).output{1}(i).indices;
                % Only indexes
                %animindex=[an modf(an).output{1}(i).indices]; % Put animal index in front
                %allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
                %Sub indices for glm and corrcoef
                allglmidxstheta = [allglmidxstheta; modf(an).output{1}(i).glmidxs]; % Need to put "an" in there
                
                indNoAnim=modf(an).output{1}(i).glmidxs2;
                indNoAnim=indNoAnim(find(nansum(indNoAnim')~=0),:);
                indWAnim=[an*ones(size(indNoAnim,1),1) indNoAnim];
                allglmidxstheta2 = [allglmidxstheta2; indWAnim];
                
                
                % Glm data
%                 allmodelbtheta = [allmodelbtheta; modf(an).output{1}(i).allmodelb];
%                 allmodelbtheta2 = [allmodelbtheta2; modf(an).output{1}(i).allmodelb2];
%                 allmodelptheta = [allmodelptheta; modf(an).output{1}(i).allmodelp];
%                 allmodelfitstheta{cnt} = modf(an).output{1}(i).allmodelfits;
%                 allnsigtheta = [allnsigtheta; modf(an).output{1}(i).nsig'];
%                 allnsigpostheta = [allnsigpostheta; modf(an).output{1}(i).nsigpos'];
%                 allnsignegtheta = [allnsignegtheta; modf(an).output{1}(i).nsigneg'];
%                 allfracsigtheta = [allfracsigtheta; modf(an).output{1}(i).fracsig'];
%                 allfracsigpostheta = [allfracsigpostheta; modf(an).output{1}(i).fracsigpos'];
%                 allfracsignegtheta = [allfracsignegtheta; modf(an).output{1}(i).fracsigneg'];
                % Corr data
                
           % end
        end
        
    end
    %removing rows and columns that are all nan
    allglmidxstheta2=allglmidxstheta2(find(nansum(allglmidxstheta2')~=0),1:find(nansum(allglmidxstheta2)==0,1));
  %  allmodelbtheta2=allmodelbtheta2(find(nansum(allmodelbtheta2')~=0),1:find(nansum(allmodelbtheta2)==0,1));
    
    cnt=0;
    %---- Getting ripple model data
    allglmidxsrip2=[];
    %     allmodelbtheta=[]; allmodelbtheta2=[];allmodelptheta=[]; allmodelfitstheta=[];
    %     allnsigtheta=[]; allnsigpostheta=[]; allnsignegtheta=[];  allfracsigtheta=[]; allfracsigpostheta=[]; allfracsignegtheta=[];
    Xmats={};
    Ymats={};
    XYmats={};
    for an = 1:length(modg)
        for i=1:length(modg(an).output{1})
            % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
            %    if ~isempty(modg(an).output{1}(i).nsig)
            cnt=cnt+1;
            anim_index{an}{cnt} = modf(an).output{1}(i).indices;
            % Only indexes
            %animindex=[an modf(an).output{1}(i).indices]; % Put animal index in front
            %allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
            %Sub indices for glm and corrcoef
            % allglmidxstheta = [allglmidxstheta; modf(an).output{1}(i).glmidxs]; % Need to put "an" in there
            
            
            indNoAnim=modg(an).output{1}(i).glmidxs2;
            indNoAnim=indNoAnim(find(nansum(indNoAnim')~=0),:);
            indWAnim=[an*ones(size(indNoAnim,1),1) indNoAnim];
            % Indices in new form. Each line is one ensemble of cells in the
            % following form:
            % animal day,epoch,hctet,hccell,hctet,hccell...,hctet,hccell,pfctet,pfccell
            allglmidxsrip2 = [allglmidxsrip2; indWAnim]; % Need to put "an" in there
            Xmats{cnt}=modg(an).output{1}(i).Xmat;
            Ymats{cnt}=modg(an).output{1}(i).Ymat;
            for d=1:size(indWAnim,1)
                XYmats{end+1}=[Xmats{cnt} Ymats{cnt}(:,d)];
            end
            
            
            %    end
        end
        
    end
    
    allglmidxsrip2=allglmidxsrip2(find(nansum(allglmidxsrip2')~=0),1:find(nansum(allglmidxsrip2)==0,1));
    
    % sanity check
    numPFCCells=0;for i=1:size(Ymats,2);numPFCCells=numPFCCells+size(Ymats{i},2);end
    if numPFCCells~=size(allglmidxsrip2)|numPFCCells~=size(allglmidxstheta2)
        'Numbers dont add up'
        keyboard
    end
    cnt2=1;
    XYmats2={};
    allPFCCA1sigidxs={};
    allglmidxstheta3=[];
    %making anim-day-pfctet-pfccell
    animdaypfc=[];for kk=1:size(allglmidxstheta2,1),animdaypfc(kk,1:4)=allglmidxstheta2(kk,[1,2,find(isnan(allglmidxstheta2(kk,:)),1)-2,find(isnan(allglmidxstheta2(kk,:)),1)-1]);end
    uniqueIndices=unique(animdaypfc,'rows');
    for kk=1:size(uniqueIndices,1)
        %finding all epochs for each pfc cell
        
        ind1=find(ismember(animdaypfc,uniqueIndices(kk,:),'rows'))';
        numepochs=length(ind1);
        
        allcells1=reshape(allglmidxstheta2(ind1(1),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
        allcells1=allcells1(~isnan(allcells1(:,1)),:);
        allca1cells1=allcells1(1:end-1,:);
        X1=XYmats{ind1(1)};
        XYconcat=X1;
        if numepochs>1
            
            allcells2=reshape(allglmidxstheta2(ind1(2),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
            allcells2=allcells2(~isnan(allcells2(:,1)),:);
            %allca1cells2=allcells2(1:end-1,:);
            X2=XYmats{ind1(2)};
            
            [C ia ib]=intersect(allcells1,allcells2,'rows')
            XYconcat=[X1(:,ia);X2(:,ib)];
            allcells1=C;
        end
        if numepochs>2
            
            allcells3=reshape(allglmidxstheta2(ind1(3),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
            allcells3=allcells3(~isnan(allcells3(:,1)),:);
            X3=XYmats{ind1(3)};
            [C ia ib]=intersect(allcells1,allcells3,'rows');
            XYconcat=[XYconcat(:,ia);X3(:,ib)];
            allcells1=C;
            
            
        end
        if numepochs>3
            
            allcells4=reshape(allglmidxstheta2(ind1(4),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
            allcells4=allcells4(~isnan(allcells4(:,1)),:);
            X4=XYmats{ind1(4)};
            [C ia ib]=intersect(allcells1,allcells4,'rows');
            XYconcat=[XYconcat(:,ia);X4(:,ib)];
            allcells1=C;
        end
        
        XYmats2{end+1}=XYconcat;
        
        
        animday=allglmidxstheta2(ind1(1),1:2);
        animdayrep=repmat(animday,size(allcells1,1),1);
        allcellswanimday=[animdayrep allcells1];
        
        dd=length(allPFCCA1sigidxs);
        allPFCCA1sigidxs(dd+1).PFCidx = allcellswanimday(end,:)
        allPFCCA1sigidxs(dd+1).CA1idx = allcellswanimday(1:end-1,:)
        
        
        
        
        
        
    end
    
    % now allglmidxstheta3 and XYmats2 has data for only one epoch per day for each pfc
    % cell. In XYmats2,
    
    allfrs=[];
    allErrReal_theta=[];
    allErrShuf_theta=[];
    allErrReal=[];
    allErrShuf=[];
    allPs_theta=[];
    allPs=[];
    allPsK=[];
    nsig=[];
    fracsig=[];
    corrPlast=[];
    counter=1;
    allRealPlast=[];
    allShufPlast=[];
    figdir = '/data15/gideon/Figs/';
    plotGraphs=0;
    notconv=0;
    maxbs=[];
    numRips1=[];
    sig1=[]
    for ii=1:size(allPFCCA1sigidxs,2)
        currXY=XYmats2{ii};
      
        nCA1cells=size(currXY,2)-1;
       numRips1=[numRips1 size(currXY,1)];
        currX=currXY(:,1:end-1);
        y=currXY(:,end);
        %-----------
        lastwarn('');
        [btrall, ~, statsall] = glmfit(currX,y,'poisson');
        maxbs=[maxbs  max(abs(btrall(2:end)))];
        [rr1 pp1]=corrcoef([currX y]);
        corrrvals=rr1(1:end-1,end);
        corrpvals=pp1(1:end-1,end);
       if ~isempty(lastwarn)
          notconv=notconv+1;
       else
%         if ~isnan(msgid)
%             
%            keyboard
%         end
        allfrs=[allfrs mean(y)];
        currsig = find(statsall.p(2:end) < 0.05);
        nsig = [nsig length(currsig)];
        fracsig = [fracsig length(currsig)/nCA1cells];
%         if length(currsig)/nCA1cells>0.6
%             keyboard
%         end

        
        
  
        allPFCCA1sigidxs(ii).pvals = statsall.p(2:end);
        allPFCCA1sigidxs(ii).betas = btrall(2:end);
        allPFCCA1sigidxs(ii).corrrvals = corrrvals;
        allPFCCA1sigidxs(ii).corrpvals = corrpvals;
        sig1=[sig1; (corrpvals<0.05)];

        
       end
    end
    %
    %
    % ----------
    % No combination across epochs
    % ---------
    % Save
    % -----
    if savegatherdata == 1
       save(gatherdatafile,'allPFCCA1sigidxs');
    end
    
else % gatherdata=0
    
    %load(gatherdatafile);
    
end % end gather data


% ------------
% PLOTTING, ETC
% ------------
%
% fractionSigPredictableK=mean(allPsK<0.05)
% fractionSigPredictable=mean(allPs<0.05)
% fractionSigPredictableTheta=mean(allPs_theta<0.05)
    % errDecrease=(allErrReal./allErrShuf);
    % errDecrease_theta=(allErrReal_theta./allErrShuf_theta);
    %
    %
    % t=[];for i=1:3,t=[t mean(allPs(nsig==(i-1))<0.05)];end
    % terr=[];for i=1:3,terr=[terr std(allPs(nsig==(i-1))<0.05)];end
    % tn=[];for i=1:3,tn=[tn sum(allPs(nsig==(i-1))<0.05)];end
    % figure;errorbar(0:2,t,terr./sqrt(tn-1),'k','linewidth',2)
    % xlabel('Number of significant CA1 units');
    % ylabel('Fraction of predictable PFC cells')
    %
    % hold on
    %
    % t=[];for i=1:3,t=[t mean(allPs_theta(allnsigtheta==(i-1))<0.05)];end
    % terr=[];for i=1:3,terr=[terr std(allPs_theta(allnsigtheta==(i-1))<0.05)];end
    % tn=[];for i=1:3,tn=[tn sum(allPs_theta(allnsigtheta==(i-1))<0.05)];end
    % errorbar(0:2,t,terr./sqrt(tn-1),'r','linewidth',2)
    % xlabel('Number of significant CA1 units');
    % ylabel('Fraction of predictable PFC cells')
    %
    %
    % t2=[];for i=1:3,t2=[t2 nanmean(errDecrease(nsig==(i-1)))];end
    % terr2=[];for i=1:3,terr2=[terr2 nanstd(errDecrease(nsig==(i-1)))];end
    % tn2=[];for i=1:3,tn2=[tn2 sum(errDecrease(nsig==(i-1)))];end
    % figure;errorbar(0:2,t2,terr2./sqrt(tn2),'k','linewidth',2)
    % xlabel('Number of significant CA1 units');
    % ylabel('Error relative to shuf')
    % hold on
    %
    % t2=[];for i=1:3,t2=[t2 mean(errDecrease_theta(nsig==(i-1)))];end
    % terr2=[];for i=1:3,terr2=[terr2 std(errDecrease_theta(nsig==(i-1)))];end
    % tn2=[];for i=1:3,tn2=[tn2 sum(errDecrease_theta(nsig==(i-1)))];end
    % errorbar(0:2,t2,terr2./sqrt(tn2),'r','linewidth',2)
    % xlabel('Number of significant CA1 units');
    % ylabel('Error relative to shuf')
    %
    % keyboard
    %
    %
    % % ------------------
    % % Population Figures
    % % ------------------
    %
    % % Idxs for allidxs and corrodxs are exactly matched. So you can just take values from glmfits and corrcef directly
    % %
    % % % Skip bad fits, and corrlns where no. of co-occurences are <10
    % % rem = find( ((allmodelb>1) | (allmodelb<-1)) & allmodelp>0.99); % Corresponding p will usually be 1
    % % %rem = find(allmodelp ==1);
    % % rem2 = find(allnsimul<10);
    % % allrem = union(rem, rem2);
    % % allmodelb(allrem)=[]; allmodelp(allrem)=[]; allrcorr(allrem)=[]; allpcorr(allrem)=[]; allglmidxs(allrem,:)=[]; allcorridxs(allrem,:)=[];
    % % sigglm = find(allmodelp < 0.05);
    % % sigcorr = find(allpcorr < 0.05);
    % %
    % % % Sig GLM vs Sig Corr
    % % glmvec = zeros(size(allmodelb)); glmvec(sigglm)=1;
    % % corrvec = zeros(size(allmodelb)); corrvec(sigcorr)=1;
    % % [rvec,pvec] = corrcoef(glmvec,corrvec)
    % %
    % %
    % % forppr = 0;
    % % % If yes, everything set to redimscreen_figforppr1
    % % % If not, everything set to redimscreen_figforppt1
    % % figdir = '/data25/sjadhav/HPExpt/Figures/ThetaMod/';
    % % summdir = figdir;
    % % set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
    % %
    % % if forppr==1
    % %     set(0,'defaultaxesfontsize',16);
    % %     tfont = 18; % title font
    % %     xfont = 16;
    % %     yfont = 16;
    % % else
    % %     set(0,'defaultaxesfontsize',24);
    % %     tfont = 28;
    % %     xfont = 20;
    % %     yfont = 20;
    % % end
    % %
    % %
    % % figure; hold on;redimscreen_figforppt1;
    % % plot(allrcorr, allmodelb, 'k.','MarkerSize',24);
    % % plot(allrcorr(sigglm), allmodelb(sigglm), 'r.','MarkerSize',24);
    % % plot(allrcorr(sigcorr), allmodelb(sigcorr), 'bo','MarkerSize',20);
    % % title(sprintf('GLM fits vs Corr Coeff'),'FontSize',24,'Fontweight','normal');
    % % xlabel(['Corr Coeff'],'FontSize',24,'Fontweight','normal');
    % % ylabel(['GLM coeffs'],'FontSize',24,'Fontweight','normal');
    % % legend('All Pairs','Sig GLM','Sig Corr');
    % % text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');
    % %
    % %
    % % [rall,pall] = corrcoef(allrcorr,allmodelb)
    % % [rglm,pglm] = corrcoef(allrcorr(sigglm),allmodelb(sigglm))
    % % [rc,pc] = corrcoef(allrcorr(sigcorr),allmodelb(sigcorr))
    % %
    % % % figure; hold on;redimscreen_figforppt1;
    % % % plot(abs(allrcorr), abs(allmodelb), 'k.','MarkerSize',24);
    % % % plot(abs(allrcorr(sigglm)), abs(allmodelb(sigglm)), 'r.','MarkerSize',24);
    % % % plot(abs(allrcorr(sigcorr)), abs(allmodelb(sigcorr)), 'bo','MarkerSize',20);
    % % % title(sprintf('ABS GLM fits vs Corr Coeff'),'FontSize',24,'Fontweight','normal');
    % % % xlabel(['Corr Coeff'],'FontSize',24,'Fontweight','normal');
    % % % ylabel(['GLM coeffs'],'FontSize',24,'Fontweight','normal');
    % % % legend('All Pairs','Sig GLM','Sig Corr');
    % %
    % %
    % %
    % % figure; hold on;%redimscreen_figforppt1;
    % % subplot(3,1,1)
    % % h = bar(1+[0:10], hist(nsig,[0:10])); set(h(1),'facecolor','k'); set(h(1),'edgecolor','k');
    % % title(sprintf('No. of sig CA1 predictors'),'FontSize',24,'Fontweight','normal');
    % % xlabel(['No. of sig CA1 cells in epoch'],'FontSize',24,'Fontweight','normal');
    % % ylabel(['Hist of PFC cells'],'FontSize',24,'Fontweight','normal');
    % % legend('All');
    % % subplot(3,1,2)
    % % h = bar(1+[0:10], hist(nsigpos,[0:10])); set(h(1),'facecolor','r'); set(h(1),'edgecolor','r');
    % % legend('Pos');
    % % subplot(3,1,3)
    % % h = bar(1+[0:10], hist(nsigneg,[0:10])); set(h(1),'facecolor','c'); set(h(1),'edgecolor','c');
    % % legend('Neg');
    % %
    % %
    % % figure; hold on;%redimscreen_figforppt1;
    % % subplot(3,1,1)
    % % h = bar([0:0.1:1], hist(allfracsig,[0:0.1:1])); set(h(1),'facecolor','k'); set(h(1),'edgecolor','k');
    % % title(sprintf('No. of sig CA1 predictors'),'FontSize',24,'Fontweight','normal');
    % % xlabel(['Frac of sig CA1 cells in epoch'],'FontSize',24,'Fontweight','normal');
    % % ylabel(['Hist of PFC cells'],'FontSize',24,'Fontweight','normal');
    % % legend('All');
    % % subplot(3,1,2)
    % % h = bar([0:0.1:1], hist(allfracsigpos,[0:0.1:1])); set(h(1),'facecolor','r'); set(h(1),'edgecolor','r');
    % % legend('Pos');
    % % subplot(3,1,3)
    % % h = bar([0:0.1:1], hist(allfracsigneg,[0:0.1:1])); set(h(1),'facecolor','c'); set(h(1),'edgecolor','c');
    % % legend('Neg');
    % %
    % %
    % %
    % % keyboard;
    % %
    % %
    % %
