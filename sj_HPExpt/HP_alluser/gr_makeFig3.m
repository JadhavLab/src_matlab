% From DFSsj_HPexpt_ThetacorrAndRipresp_ver4
figdir = '/data15/gideon/FigsForPaper/Fig3/';

set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
set(0,'defaultaxesfontname','Arial');
set(0,'defaultAxesFontName','Arial');
set(0,'defaultTextFontName','Arial');
set(0,'defaultaxesfontsize',16);

forppr=1;
tfont = 14; % title font
xfont = 12;
yfont = 12;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells


savedir='/mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';
load([savedir 'HP_ThetacovAndRipmodcorr_std3_speed4_ntet2_alldata_gather_5-2-2014.mat'])



% CONSOLIDATE ACROSS EPOCHS FOR SAME CELL PAIRS
% ----------------------------------------------------------
runpairoutput = struct;
dummyindex=allanimindex;  % all anim-day-epoch-tet1-cell1-tet2-cell2 indices
cntpairs=0;

for ii=1:size(allanimindex)
    animdaytetcell=allanimindex(ii,[1 2 4 5 6 7]);
  
    ind=[];
    while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5 6 7]))~=0          % collect all rows (epochs)
        ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5 6 7]))];        % finds the first matching row
        dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5 6 7])),:)=[0 0 0 0 0 0 0]; % after adding index, remove the corresponding row
        % so you could find the next one if it exists
    end
    
    % Gather everything for the current cell across epochs
    % Theta corr variables
    allZcrosscov_runtheta_epcomb = []; allZcrosscov_sm_runtheta_epcomb = [];
    allNeventscorr_runtheta_epcomb = []; alltheta_peakcorr_epcomb = []; alltheta_peaklag_epcomb = [];
    % Ripcorr variables
    all_nsimul_epcomb=0; all_nsimulrdm_epcomb=0;
    all_trialResps1_epcomb=[]; all_trialResps2_epcomb=[]; all_trialResps1rdm_epcomb=[]; all_trialResps2rdm_epcomb=[];
    r=[]; p=[]; r_rdm=[]; p_rdm=[];
    
    for r=ind
        % Theta Corr variables
        allZcrosscov_sm_runtheta_epcomb = [allZcrosscov_sm_runtheta_epcomb; allZcrosscov_sm_runtheta(r,:)];
        allZcrosscov_runtheta_epcomb = [allZcrosscov_runtheta_epcomb; allZcrosscov_runtheta(r,:)];
        % alltheta_peakcorr_epcomb = [alltheta_peakcorr_epcomb; alltheta_peakcorr(r)];
        % alltheta_peaklag_epcomb = [alltheta_peaklag_epcomb; alltheta_peaklag(r)];
        allNeventscorr_runtheta_epcomb = [allNeventscorr_runtheta_epcomb; allNeventscorr_runtheta(r)];
        % Rip Corr variables
        all_trialResps1_epcomb = [all_trialResps1_epcomb; all_trialResps1{r}];
        all_trialResps2_epcomb = [all_trialResps2_epcomb; all_trialResps2{r}];
        all_trialResps1rdm_epcomb = [all_trialResps1rdm_epcomb; all_trialResps1rdm{r}];
        all_trialResps2rdm_epcomb = [all_trialResps2rdm_epcomb; all_trialResps2rdm{r}];
        all_nsimul_epcomb = all_nsimul_epcomb + all_nsimul(r);
        all_nsimulrdm_epcomb = all_nsimulrdm_epcomb + all_nsimulrdm(r);
    end
    
    % Calculate corrln for combined epoch data
    [r, p] = corrcoef(all_trialResps1_epcomb,all_trialResps2_epcomb);
    % random is actually the back window
    [r_rdm, p_rdm] = corrcoef(all_trialResps1rdm_epcomb,all_trialResps2rdm_epcomb);
    r = r(1,2); p = p(1,2);
    r_rdm = r_rdm(1,2); p_rdm = p_rdm(1,2);
    % this is the true shiffling:
    shufT1=all_trialResps1_epcomb(randperm(length(all_trialResps1_epcomb)));
    shufT2=all_trialResps2_epcomb(randperm(length(all_trialResps2_epcomb)));
    [r_shuf, p_shuf] = corrcoef(shufT1,shufT2);
    r_shuf = r_shuf(1,2); p_shuf = p_shuf(1,2);
    % stability measure:
    %permDataInd=randperm(length(all_trialResps1_epcomb));
    permDataInd=1:length(all_trialResps1_epcomb);
    
    midD=round(length(all_trialResps1_epcomb)/2);
    [rhalf1, phalf1] = corrcoef(all_trialResps1_epcomb(permDataInd(1:midD)),all_trialResps2_epcomb(permDataInd(1:midD)));
    [rhalf2, phalf2] = corrcoef(all_trialResps1_epcomb(permDataInd(midD+1:end)),all_trialResps2_epcomb(permDataInd(midD+1:end)));
    stabilityM=abs(rhalf1(1,2)-rhalf2(1,2));
        
    % stability measure for shuf:
    [rhalf1shuf, phalf1shuf] = corrcoef(shufT1(1:midD),shufT2(1:midD));
    [rhalf2shuf, phalf2shuf] = corrcoef(shufT1(midD+1:end),shufT2(midD+1:end));
    
    stabilityMshuf=abs(rhalf1shuf(1,2)-rhalf2shuf(1,2));
         
    set(0,'defaultaxesfontsize',14);
    
    anim1=animdaytetcell(1);
    day1=animdaytetcell(2);
    tetHC=animdaytetcell(3);
    cellHC=animdaytetcell(4);
    tetPFC=animdaytetcell(5);
    cellPFC=animdaytetcell(6);
    epochs1=allanimindex(ind,3);
    
    % The examples for the figure
    if ~ isempty(ind)&((anim1==1& day1==2 & tetHC==1 & cellHC==1 & tetPFC==17 & cellPFC==2)...
            | (anim1==3& day1==1 & tetHC==2 & cellHC==2 & tetPFC==16 & cellPFC==1))
       
        switch anim1(1)
            case 1
                load(['/mnt/data25new/sjadhav/HPExpt/HPa_direct/HParipplemod0' num2str(day1) '.mat'])
                
            case 2
                load(['/mnt/data25new/sjadhav/HPExpt/HPb_direct/HPbripplemod0' num2str(day1) '.mat'])
            case 3
                load(['/mnt/data25new/sjadhav/HPExpt/HPc_direct/HPcripplemod0' num2str(day1) '.mat'])
            case 4
                if day1<10
                    load(['/data15/gideon/ripplemod/Ndlripplemod0' num2str(day1) '.mat'])
                else
                    load(['/data15/gideon/ripplemod/Ndlripplemod' num2str(day1) '.mat'])
                end
        end
        
        plotRipRasters=1;
        
        smoothRast=0;
        if plotRipRasters
            
            scrsz = get(0,'ScreenSize');
            
            saveg1=0;
            if saveg1==1,
                if smoothRast
                    figfile = [figdir,'RipRasters_',num2str(anim1),'_',num2str(day1),'_',num2str(tetHC),'_',num2str(cellHC),'_',num2str(tetPFC),'_',num2str(cellPFC) '_sm'];
                else
                    figfile = [figdir,'RipRasters_',num2str(anim1),'_',num2str(day1),'_',num2str(tetHC),'_',num2str(cellHC),'_',num2str(tetPFC),'_',num2str(cellPFC)];
                end
                print(fig,'-dpdf', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
            end
            figure('Position',[900 50 scrsz(3)/3 scrsz(4)/1.5])
            
            subplot(2,1,1)
            all_trialResps1_epcombR=all_trialResps1_epcomb;
            all_trialResps1_epcombR(all_trialResps1_epcombR>4)=4;
            plot(all_trialResps1_epcombR'+0.05*randn(1,size(all_trialResps2_epcomb,1)),   all_trialResps2_epcomb'+0.1*randn(1,size(all_trialResps2_epcomb,1)),'ko')
            set(gca,'xticklabel',{'','0','1','2','3','>=4',''})
            xlim([-1 5])
            pf = polyfit(all_trialResps1_epcomb,all_trialResps2_epcomb,1);
            f=polyval(pf,-0.5:4.5);
            hold on
            plot(-0.5:4.5,f,'r','linewidth',3)
            xlabel('Number of spikes in ripple, CA1')
            ylabel('Number of spikes in ripple, PFC')
            [rr pp]=corrcoef(all_trialResps1_epcomb,all_trialResps2_epcomb)
            set(gca,'box','off')
            subplot(2,1,2)
            b=fir1(22,0.3);
            plot(runcorrtime,nanmean(allZcrosscov_sm_runtheta_epcomb,1),'k','linewidth',3)
            title(['corr=' num2str(r) 'thetamax= ' num2str(nanmax(nanmean(allZcrosscov_sm_runtheta_epcomb(:,bins_run),1)))])
            
            hold on;plot([-0.5:0.02:0.5],0,'k--','linewidth',3)
            ylim([-2 4])
            xlim([-0.42 0.42])
            xlabel('Time (s)')
            ylabel('Standardized cross covariance')
            set(gca,'box','off')
            
            
            keyboard
        end
    end
    if ~isempty(allZcrosscov_runtheta_epcomb)
        cntpairs=cntpairs+1;
        runpairoutput_idx(cntpairs,:)=animdaytetcell;
        runpairoutput(cntpairs).index=animdaytetcell; % This is anim-day-tet1-cell1-tet2-cell2. No epoch
        % Theta corr variables
        runpairoutput(cntpairs).allZcrosscov_sm_runtheta_epcomb = nanmean(allZcrosscov_sm_runtheta_epcomb,1);
        runpairoutput(cntpairs).allZcrosscov_runtheta_epcomb = nanmean(allZcrosscov_runtheta_epcomb,1);
        runpairoutput(cntpairs).alltheta_peakcorr_epcomb = nanmean(alltheta_peakcorr_epcomb,1);
        runpairoutput(cntpairs).alltheta_peaklag_epcomb = nanmean(alltheta_peaklag_epcomb,1);
        runpairoutput(cntpairs).allNeventscorr_runtheta_epcomb = nansum([0;allNeventscorr_runtheta_epcomb]);
        % Rip Corr variables
        runpairoutput(cntpairs).all_nsimul_epcomb = all_nsimul_epcomb;
        runpairoutput(cntpairs).all_nsimulrdm_epcomb = all_nsimulrdm_epcomb;
        runpairoutput(cntpairs).all_trialResps1_epcomb = all_trialResps1_epcomb;
        runpairoutput(cntpairs).all_trialResps2_epcomb = all_trialResps2_epcomb;
        runpairoutput(cntpairs).all_trialResps1rdm_epcomb = all_trialResps1rdm_epcomb;
        runpairoutput(cntpairs).all_trialResps2rdm_epcomb = all_trialResps2rdm_epcomb;
        runpairoutput(cntpairs).allr_epcomb = r;
        runpairoutput(cntpairs).allp_epcomb = p;
        runpairoutput(cntpairs).allr_rdm_epcomb = r_rdm;
        runpairoutput(cntpairs).allp_rdm_epcomb = p_rdm;
        
        
        % NOTE: NOT DEFINING S-variables SO I CAN JUST INHERIT FROM THE GATHER FILE
        
        
    end
end
%%

forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

figdir = '/data25/sjadhav/HPExpt/Figures/Jan2014/';
summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
set(0,'defaultaxesfontname','Arial');
set(0,'defaultAxesFontName','Arial');
set(0,'defaultTextFontName','Arial');
set(0,'defaultaxesfontsize',16);

%set(gca,'fontname','arial')
forppr=1;
tfont = 14; % title font
xfont = 12;
yfont = 12;

if strcmp(state,'sleep'),
    statename = 'Sleep';
else
    statename = 'Run';
end

%clr = 'c'


% Change variable names for epoch-combined data so that you can use the same code to plot as in Version 1
% ----------------------------------------------------------------------------------------------------------
% Theta Corr variables
allZcrosscov_sm_runtheta = SallZcrosscov_sm_runtheta_epcomb;
allZcrosscov_runtheta = SallZcrosscov_runtheta_epcomb;

alltheta_peakcorr = Salltheta_peakcorr_epcomb;
alltheta_peaklag = Salltheta_peaklag_epcomb;

% Rip Corr variables
allr = Sallr_epcomb;
allr_rdm = Sallr_rdm_epcomb;
allp = Sallp_epcomb;
allp_rdm = Sallp_rdm_epcomb;
all_nsimul = Sall_nsimul_epcomb;
all_nsimulrdm = Sall_nsimulrdm_epcomb;



if 1
    
    % 1a) Rip corrcoeff vs Total thetacorr for SWR Response
    % -----------------------------------------------------
    % Get rid of NaNs / Take Only Significant
    % Save temp
    alltheta_peakcorr_tmp = alltheta_peakcorr; % Take Peak Corrln
    allr_tmp = allr; allp_tmp = allp;
    
    rnan = find(isnan(allr) | isnan(alltheta_peakcorr));
    %tnan = find(isnan(alltheta_peakcorr));
    nocc = find(all_nsimul<5);
    removeidxs = union(rnan,nocc);
    
    
    allr(removeidxs)=[]; alltheta_peakcorr(removeidxs)=[]; allp(removeidxs)=[];
    sigidx = find(allp<0.05); nosigidx = find(allp>=0.05);
    %allr = allr(sigidx); alltheta_peakcorr = alltheta_peakcorr(sigidx);
    
    
    % For Demetris
    corrindsForSpatial=runpairoutput;
    corrindsForSpatial(removeidxs)=[];
    save corrindsForSpatial corrindsForSpatial
    
    figure; hold on; redimscreen_figforppt1;
    %set(gcf, 'Position',[205 136 723 446]);
    %xaxis = min(allr):0.1:max(allr);
    plot(alltheta_peakcorr, allr,'.','color',[1 1 1]*0.6,'MarkerSize',20);
    plot(alltheta_peakcorr(sigidx), allr(sigidx),'k.','MarkerSize',20);
    % legend('Theta Cov vs SWR CorrCoeff');
    
    title(sprintf('%s %s units', area, statename),'FontSize',tfont,'Fontweight','normal')
    title(sprintf('CA1-PFC SWR modulated pairs'),'FontSize',tfont,'Fontweight','normal')
    ylabel('SWR Response Correlation','FontSize',yfont,'Fontweight','normal');
    xlabel('Peak Theta Covariance','FontSize',xfont,'Fontweight','normal');
    
    % Do statistics on this popln plot
    [r_thetavsrip,p_thetavsrip] = corrcoef(allr, alltheta_peakcorr);
    [rsig,psig] = corrcoef(allr(sigidx), alltheta_peakcorr(sigidx));
    [rnosig,pnosig] = corrcoef(allr(nosigidx), alltheta_peakcorr(nosigidx));
    
    % Regression
    % -----------
    [b00,bint00,r00,rint00,stats00] = regress(allr', [ones(size(alltheta_peakcorr')) alltheta_peakcorr']);
    xpts = min(alltheta_peakcorr):0.01:max(alltheta_peakcorr);
    bfit00 = b00(1)+b00(2)*xpts;
    plot(xpts,bfit00,'k-','LineWidth',4);  % Theta vs Rip

end
%% Examples of rip-correlation, currently not used, can uncomment
% set(0,'defaultaxesfontsize',20);
% tfont = 20;
% xfont = 20;
% yfont = 20;
% figdir = '/data15/gideon/FigsForPaper/Fig3/';
% anim1=1;
% day1=2;
% tetHC1=1%4;
% cellHC1=1%2;
% tetPFC1=17;
% cellPFC1=2;
% open([figdir,'RipRasters_',num2str(anim1),'_',num2str(day1),'_',num2str(tetHC1),'_',num2str(cellHC1),'_',num2str(tetPFC1),'_',num2str(cellPFC1) '.fig']);
% [a exInd1]=ismember([anim1 day1 tetHC1 cellHC1 tetPFC1 cellPFC1],runpairoutput_idx,'rows');
% figure;hold on;
% plot(runcorrtime, smooth(SallZcrosscov_runtheta_epcomb(exInd1,:),10),'k','LineWidth',3);
% ylim([-2 2])
% xlim([-0.4 0.4])
% xlabel('Time (S)')
% ylabel('Standardized cross covariance');
% 
% 
% anim2=4;
% day2=16;
% tetHC2=14;
% cellHC2=1;
% tetPFC2=21;
% cellPFC2=1;
% open([figdir,'RipRasters_',num2str(anim2),'_',num2str(day2),'_',num2str(tetHC2),'_',num2str(cellHC2),'_',num2str(tetPFC2),'_',num2str(cellPFC2) '.fig']);
% [a exInd2]=ismember([anim2 day2 tetHC2 cellHC2 tetPFC2 cellPFC2],runpairoutput_idx,'rows');
% 
% figure;hold on;
% plot(runcorrtime, smooth(SallZcrosscov_runtheta_epcomb(exInd2,:),10),'k','LineWidth',3);
% ylim([-2 2])
% xlim([-0.4 0.4])
% xlabel('Time (S)')
% ylabel('Standardized cross covariance');
% 
% 
% [zzs bbs]=hist(Sallr_shuf_epcomb,-0.5:0.05:0.5)
% [zz bb]=hist(Sallr_epcomb,-0.5:0.05:0.5)
% figure;
% hold on
% plot(bb,zz/sum(zz),'r','linewidth',3)
% plot(bbs,zzs/sum(zzs),'linewidth',3)
% xlabel('Correlation')
% ylabel('Fraction')
% legend('Ripple-correlation','Trial-shuffled')
% 
% 

%% Different stability measures, currently unused, can uncomment

% corrRateRipples = nanmean(Sallp_epcomb < 0.05);
% corrRateRdm = nanmean(Sallp_rdm_epcomb < 0.05);
% % are there more correlated pairs during ripples than before?
% ztestprop2([sum(Sallp_epcomb<0.05) length(Sallp_epcomb)],[sum(Sallp_rdm_epcomb<0.05) length(Sallp_rdm_epcomb)])
% 
% figure;bar([nanmean(Sallp_epcomb<0.05),nanmean(Sallp_rdm_epcomb<0.05)]);ylim([0 0.15])
% set(gca,'XTickLabel',{'Ripples','Pre-ripples'})
% ylabel('% sig rip-cor pairs')
% % are there more correlated pairs during ripples than random?
% 
% % test for proportions
% pqq=ztestprop2([sum(Sallp_epcomb<0.05) length(Sallp_epcomb)],[sum(Sallp_shuf_epcomb<0.05) length(Sallp_shuf_epcomb)]);
% 
% figure;bar([nanmean(Sallp_epcomb<0.05),nanmean(Sallp_shuf_epcomb<0.05)]);ylim([0 0.15])
% 
% set(gca,'XTickLabel',{'Ripples','Trial-shuff'})
% ylabel('% sig rip-cor pairs')
% title(['P=' num2str(pqq)])
% 
% % correlation between correlation during ripples and correlations before
% good2=(~isnan(Sallr_epcomb)&~isnan(Sallr_rdm_epcomb))
% [rr1 pp1]=corrcoef(Sallr_epcomb(good2),Sallr_rdm_epcomb(good2))
% qq=polyfit(Sallr_epcomb(good2)', Sallr_rdm_epcomb(good2)',1);
% xpts = -0.4:0.01:0.6;
% f= polyval(qq,xpts);
% figure;plot(Sallr_epcomb,Sallr_rdm_epcomb,'x')
% hold on
% plot(xpts,f,'-','linewidth',3)
% title(['r= ' num2str(rr1(2,1)) ' p= ' num2str(pp1(2,1))])
% xlabel('Corr during ripples')
% ylabel('Corr pre-ripples')
% % correlation between correlation during ripples and random
% good1=(~isnan(Sallr_epcomb)&~isnan(Sallr_shuf_epcomb))
% [rr2 pp2]=corrcoef(Sallr_epcomb(good1),Sallr_shuf_epcomb(good1))
% qq=polyfit(Sallr_epcomb(good1)', Sallr_shuf_epcomb(good1)',1);
% xpts = -0.4:0.01:0.6;
% f= polyval(qq,xpts);
% figure;plot(Sallr_epcomb,Sallr_shuf_epcomb,'x')
% title(['r= ' num2str(rr2(2,1)) ' p= ' num2str(pp2(2,1))])
% xlabel('Corr during ripples')
% ylabel('Corr, shuffled')
% 
% 
% % stability
% s1=(Sstability(Sallp_epcomb<0.05));
% s2=(SstabilityShuf(Sallp_shuf_epcomb<0.05));
% 
% px=kruskalwallis([s1' [s2 nan(1,length(s1)-length(s2))]']);
% 
% figure;barwitherr([nanstd(s1(~isnan(s1)))/sqrt(length(s1(~isnan(s1)))),nanstd(s2(~isnan(s2)))/sqrt(length(s2(~isnan(s2))))],[nanmean(s1(~isnan(s1))),nanmean(s2(~isnan(s2)))]);ylim([0 0.3])
% 
% set(gca,'XTickLabel',{'Ripples','Trial-shuff'})
% ylabel('Corr jitter')
% title('Stability of correlations')
% 



