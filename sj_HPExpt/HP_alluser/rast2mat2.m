function mat1=rast2mat2(raster,matwidth)

mat1=zeros(length(raster),matwidth);
for c=1:length(raster)
tmps = raster{c};
if ~isempty(tmps)
mat1(c,round(tmps+matwidth/2+1))=1;
end
end 
mat1=mat1(:,51:matwidth-50);