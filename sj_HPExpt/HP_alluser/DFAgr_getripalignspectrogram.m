function out = DFAgr_getripalignspectrogram(index, excludetimes, eeg, ripples, tetinfo, pos, task, varargin)
% out = DFAsj_getripalignspiking(spike_index, excludeperiods, spikes, ripples, tetinfo, options)

% Called from DFSsj_getripalignspiking
% Use tetinfo and tetfilter passed in, or redefine here to get riptets
% Then use ripples to getriptimes. Use inter-ripple-interval of 1 sec, and use a low-speed criterion.
% Then align spikes to ripples


datadir = '/opt/data15/gideon/specgramdata_rip/';        
specdatadir = '/opt/data15/gideon/specgramdata/';

params.freqvector = 0:2:250;
params.window_sec = 1;  % how long before and after nosepoke to analyze
params.bin_sec = 0.05;
params.overlap_sec = 0;

tetfilter = '';
excludetimes = [];
maxcell = 0;
minstd = 3;
lowsp_thrs = 5; %cm/sec
highsp_thrs = lowsp_thrs;
dospeed = 0;
minrip=1;

% For ripple trigger
% ------------------
binsize = 10; % ms
pret=550; postt=550; %% Times to plot
push = 500; % either bwin(2) or postt-trim=500. For jittered trigger in background window
trim = 50;
cellcountthresh = 3;  % Can be used to parse ripples
smwin=10; %Smoothing Window - along y-axis for matrix. Carry over from ...getrip4

rwin = [0 200];
bwin = [-500 -100];
push = 500; % either bwin(2) or postt-trim=500. If doing random events. See ... getrip4


for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'minstd'
            minstd = varargin{option+1};
        case 'minrip'
            minrip = varargin{option+1};
        case 'maxcell'
            maxcell = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        case 'lowsp_thrs'
            lowsp_thrs = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end
if size(index,1)==0
    out.dayepoch = [];
    out.eventtimes = [];
    out.rawspecs = {};
    out.epochmeans = {};
    out.epochstds = {};
    out.timevec = [];
    out.freqs = [];
    out.tetlist = [];
    out.area = {};
    out.subarea = {};
    out.noevents = 0;
    out.eventtags = [];
    out.alleegs = [];

return    
end

day = index(1,1);
epoch = index(1,2);

% Get riptimes
% -------------
%if isempty(tetfilter)
riptimes = sj_getripples_tetinfo2(index, ripples, tetinfo, 'tetfilter', '(isequal($descrip, ''riptet''))','minstd',minstd,'minrip',minrip);
%else
%    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', tetfilter, 'minstd', minstd);
%end
% Can opt to have a cellcountthresh for each event as in getpopulationevents2 or  sj_HPexpt_ripalign_singlecell_getrip4
% Not using as of now

% Get triggers as rip starttimes separated by at least 1 sec
% ----------------------------------------------------------
rip_starttime = 1000*riptimes(:,1);  % in ms

% Find ripples separated by atleast a second
% --------------------------------------------
iri = diff(rip_starttime);
keepidx = [1;find(iri>=1000)+1];
rip_starttime1 = rip_starttime(keepidx);

% ALTERNATIVE LOCKOUT CRITERION THAT LOOKS BOTH BACK AND FRONT OF EACH
% RIPPLE (ABOVE LEAVES RIPPLES THAT HAVE A RIPPLE CLOSE AFTER THEM)

keepidx2=find(iri(1:end-1)>400&iri(2:end)>400)+1;
rip_starttime2 = rip_starttime(keepidx2);

rip_starttime=rip_starttime2;

% Implement speed criterion - Skip for now, or keep. Try both
% ----------------------------------------
if dospeed
    absvel = abs(pos{day}{epoch}.data(:,5)); % Can also use field 9
    postime = pos{day}{epoch}.data(:,1); % in secs
    pidx = lookup(rip_starttime,postime*1000);
    speed_atrip = absvel(pidx);
    lowsp_idx = find(speed_atrip <= lowsp_thrs);
    highsp_idx = find(speed_atrip > highsp_thrs);
    
    rip_starttime = rip_starttime(lowsp_idx);
end

eventtimes=rip_starttime/1000;


tetlist = unique(index(:,3))';

posdata = pos{day}{epoch}.data;
postimes = posdata(:,1);

% install some basic information in output
out.dayepoch = [day epoch];


% grab a dummy times from one of the tetrodes (not matter which)
dummytimes = geteegtimes(eeg{day}{epoch}{index(1,3)})';
Fs = eeg{day}{epoch}{index(1,3)}.samprate;  % ** assuming to be the same for all tetrodes.. (there may be slight differences between DSPs)
% unpack params to make code less cluttered later
window_samp = round(Fs * params.window_sec);
bin_samp = round(Fs * params.bin_sec);
overlap_samp = round(Fs * params.overlap_sec);
freqvector = params.freqvector;



% throwing away events that are too close to start or end
while length(eventtimes)>3 & eventtimes(1)-dummytimes(1)<params.window_sec
    eventtimes=eventtimes(2:end);
end

while length(eventtimes)>3 & dummytimes(end)-eventtimes(end)<params.window_sec
    eventtimes=eventtimes(1:end-1);
end

if ~isempty(eventtimes)
    
    
    % output variables
    S = {};
    areas = {};
    subareas = {};
    epochmeans = {};
    epochstds = {};
    eventtags = nan(length(eventtimes),2);
    
    
    for tet = tetlist
        
        % First, obtain epoch's mean and std spectrogram for each tetrode
        % if this was calculated before (in specdatadir), then get it
        epochmean = [];  epochstd = [];
        %         filename = dir(sprintf('%sSpectrodata_epochs_%s_%s',specdatadir,'SWR_anim',num2str(index(1,1))));
        %         if ~isempty(filename)
        %             load([specdatadir filename.name],'spectrodata')
        %             if day <= length(spectrodata)
        %                 if ~isempty(spectrodata{day}) && epoch <= length(spectrodata{day})
        %                     if ~isempty(spectrodata{day}{epoch}) && tet <= length(spectrodata{day}{epoch})
        %                         if ~isempty(spectrodata{day}{epoch}{tet})
        %                             for pp = 1:length(spectrodata{day}{epoch}{tet})
        %                                 if isequal(spectrodata{day}{epoch}{tet}(pp).params,params)   % check if parameters are the same
        %                                     epochmean = spectrodata{day}{epoch}{tet}(pp).epochmean;
        %                                     epochmeans{tet} = spectrodata{day}{epoch}{tet}(pp).epochmean;
        %                                     epochstd = spectrodata{day}{epoch}{tet}(pp).epochstd;
        %                                     epochstds{tet} = spectrodata{day}{epoch}{tet}(pp).epochstd;
        %                                 end
        %                             end
        %                         end
        %                     end
        %                 end
        %             end
        %         else
        spectrodata = {};
        %         end
        % if not already saved, then calculate and save for future
        if isempty(epochmean) || isempty(epochstd)
             eegtimes = geteegtimes(eeg{day}{epoch}{tet});
            eidx=lookup(eegtimes,postime);
            speed_ateeg=absvel(eidx);
            lowsp_eidx=find(speed_ateeg<0.5);
            
            % NOTE HERE TO MAKE THE BACKGROUND SPECGRM ONLY WHEN ANIMAL VERY
            % LOW SPEED, CHANGE TO spectrogram(eeg{day}{epoch}{tet}.data(lowsp_eidx)
            
            A = struct;
            tic
            [~,freqs,timevec,D] = spectrogram(eeg{day}{epoch}{tet}.data,...
                bin_samp,...
                overlap_samp,...
                freqvector, ...
                Fs);
            toc
            %   figure;imagesc(timevec,freqs,10*log10(abs(D)));axis tight;view(0,90);
      %      A.params = params;
            epochmean = mean(D,2);
       %     A.epochmean = epochmean;
            epochmeans{tet} = epochmean;
            epochstd = std(D,0,2);
        %    A.epochstd = epochstd;
            epochstds{tet} = epochstd;
            % if already an entry for this tetrode (has some other .params) , then concatenate
%             try
%                 if ~isempty(spectrodata{day}{epoch}{tet})
%                     spectrodata{day}{epoch}{tet} = [spectrodata{day}{epoch}{tet}  A];
%                 end
%             catch
%                 spectrodata{day}{epoch}{tet} = A;
%             end
            %  cd(specdatadir)
            %  save(sprintf('%sSpectrodata_epochs_%s_%s',specdatadir,'SWR_anim',num2str(index(1,1))),'spectrodata','-v7.3')
        end
        
        % Second, calculate raw spectrogram for the individual events
        % initialize output  (time, frequency, event #)
        timenumber = round((window_samp*2 - bin_samp) / (bin_samp - overlap_samp)) + 1;
        S{tet} = nan(length(params.freqvector),timenumber,length(eventtimes)) ;
        
        eegtimes = geteegtimes(eeg{day}{epoch}{tet});
        alleegwindows=[];
        for ee = 1:length(eventtimes)
            
            ind = lookup(eventtimes(ee),eegtimes);
            eegwindow = eeg{day}{epoch}{tet}.data((ind - window_samp):(ind + window_samp)) ;
            [~,freqs,timevec,S{tet}(:,:,ee)] = spectrogram(eegwindow,...
                bin_samp,...
                overlap_samp,...
                freqvector, ...
                Fs);
            alleegwindows=[alleegwindows;eegwindow'];
        end
        
        % (transcribe the tetrode's areas)
        areas = [ areas    tetinfo{day}{epoch}{tet}.area] ;
        if isfield(tetinfo{day}{epoch}{tet},'subarea')
            subareas = [subareas    tetinfo{day}{epoch}{tet}.subarea];
        end
        
        %%%%%%%% optional section (for parameter tweaking) %%%%%%%%%%%%%%%%%%%%
        %%% plot z-scored individual spectrogram %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if 0
            figure
%             Z = nan(size(S{tet}(:,:,1)));
%             dummy = nan(size(S{tet}));
%             for eee = 1:length(eventtimes)
%                 %dummy(:,:,eee) = bsxfun(@minus,S{tet}(:,:,eee),epochmean);
%                 %dummy(:,:,eee) = bsxfun(@rdivide,S{tet}(:,:,eee),epochstd);
%                 
%                 dummy(:,:,eee) = mean(S{tet}(:,:,1),eee)-
%                 dummy(:,:,eee) = bsxfun(@rdivide,S{tet}(:,:,eee),epochstd);
%                 
%             end
            Smeantmp=mean(S{tet},3);
            Sstdtmp=std(S{tet},0,3);
            Z=(Smeantmp-repmat(epochmean,1,size(Smeantmp,2)))./repmat(epochstd,1,size(Smeantmp,2));
          %  Z = mean(dummy,3);
            H = imagesc(timevec,freqs,Z);
            colormap hot
            set(gca,'YDir','normal');
            title(['Day ' num2str(day) ' Ep ' num2str(epoch) ' Tet ' num2str(tet) ' Numrips= ' num2str(length(eventtimes))]);
           colormap(hot);colorbar
            dd=dir(['~/Code/Matlab/analysis/stage2analysis/Dropbox/specgrams/']);
            curnumfiles=size(dd,1);
            saveas(gcf,['~/Code/Matlab/analysis/stage2analysis/Dropbox/specgrams/specgram-' num2str(curnumfiles+1) '.jpg'])
            close all;
            ccc=1;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    end
    
    % Third, tag the events with:
    
    % i.  the speed the event occurred at
    if size(posdata,2) == 9
        eventtags(:,1) = posdata(lookup(eventtimes,postimes),9);
    else
        disp('using old velocity column..')
        eventtags(:,1) = posdata(lookup(eventtimes,postimes),5);
    end
    
    % ii. whether it occurred during inferred sleep ("sleepc")
    %   (0: not included in a sleep period, while 1: is included)
    %         if (length(sleep{day}) >= epoch)  &&  ~isempty(sleep{day}{epoch})
    %             sleepincludetimes  =   [  sleep{day}{epoch}.starttime      sleep{day}{epoch}.endtime  ];
    %             eventtags(:,2) = isExcluded(eventtimes,sleepincludetimes);
    %         else
    %             eventtags(:,2) = zeros(size(eventtags,1),1);
    %         end
    
    % returning only the rip-mean specgrams
    try
    SM={};for tt=tetlist,SM{tt}=mean(S{tt},3);end
    catch
        keyboard
    end
    
    %%% install outputs %%%%%%%%%
    out.eventtimes = eventtimes;
    out.rawspecs = SM;
    out.epochmeans = epochmeans;
    out.epochstds = epochstds;
    out.timevec = timevec;
    out.freqs = freqs;
    out.tetlist = tetlist;
    out.area = areas;
    out.subarea = subareas;
    out.noevents = length(eventtimes);
    out.eventtags = eventtags;             % [ < velocity during event>  <sleepc or not> ]
    out.alleegs = alleegwindows;
    
else
    out.dayepoch = [];
    out.eventtimes = {};
    out.rawspecs = {};
    out.epochmeans = {};
    out.epochstds = {};
    out.timevec = [];
    out.freqs = [];
    out.tetlist = [];
    out.area = {};
    out.subarea = {};
    out.noevents = 0;
    out.eventtags = [];
        out.alleegs = [];

    
end


% sleepc stuff
% if strcmp(out.epoch_type,'sleep')
%     out.sleepc_noevents = sum(isExcluded(eventtimes,sleepincludetimes));     %   # of spikes that fall into "sleep-classification" immobility periods
%     if ~isempty(sleepincludetimes)
%         out.sleepc_totalduration = sum(sleepincludetimes(:,2)-sleepincludetimes(:,1));
%     else
%         out.sleepc_totalduration = 0;
%     end
%     out.sleepc_time_immobile = sleep{day}{epoch}.time_immobile;
%     out.sleepc_velocity_thresh = sleep{day}{epoch}.velocity_thresh;
% else
%     out.sleepc_noevents = nan;
%     out.sleepc_totalduration = nan;
%     out.sleepc_time_immobile = nan;
%     out.sleepc_velocity_thresh = nan;
% end


end