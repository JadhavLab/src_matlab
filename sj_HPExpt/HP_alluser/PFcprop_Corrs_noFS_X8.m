% % ------------
% % PLOTTING, ETC
% % PFC Properties - from Demetris' code: DFS_DRsj_spatialvsripcorr.m
% % ------------

% Update everything for FS removed condition

makeCA1neuInds=0;
makePFCneuInds=0;
gatherGlobalTraj = 0; % From global traj, get PFC ones for Exc, Inh, and Neu - just like gatherPE above
% Also get allRunTraj - separated by arm, but in and out combined
gatherPE=1; % organize the PE values into something managebale for plotting

plotPE_armidx=0; % plot PE vs armidx
plotDirnIdx=0; % Plot Dirn Idx spread in poln - can also compare to Arm Idx and PE
%plotPE_replayidx=0; %plot PE vs replayidx
%plotSWRcorr=1; % Plot swrcorr vs spatialcorr

figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/July2015/';
savefig1=0;
% % ----FIG PROP --------------
forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
if forppr==1
    set(0,'defaultaxesfontsize',20);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end
% % ---------------------

if makeCA1neuInds % Get CA1 Inds - All Neurons
    load ('/data25/sjadhav/HPExpt/HP_ProcessedData/HP_ripplemod_CA1_alldata_std3_speed4_ntet2_Nspk50_gather_X6_CA2tag.mat','allripplemod');
    
    CA1inds = []; CA1indsFSandCA2 = [];
    for i=1:length(allripplemod)
        if strcmp(allripplemod(i).FStag,'n') && strcmp(allripplemod(i).CA2tag,'n')
            CA1inds = [CA1inds; allripplemod(i).index];
        else
            CA1indsFSandCA2 = [CA1indsFSandCA2; allripplemod(i).index];
        end
    end
    
    % Originally, we had 585 CA!. With FS and CA2 removed, there are 541. 536 sow up in these idxs here.
    
    savefile =  '/data25/sjadhav/HPExpt/HP_ProcessedData/CA1Inds_noFS_X8.mat'
    save(savefile,'CA1inds','CA1indsFSandCA2');
end % end makePFCneuInds


if makePFCneuInds
    load ('/data25/sjadhav/HPExpt/HP_ProcessedData/HP_ripplemod_PFC_alldata_std3_speed4_ntet2_Nspk50_gather_X6.mat','allripplemod');
    
    % The orig PFCindsExc, PFCindsInh.
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/swrmodinds_Jan27th.mat'; % PFC Exc-Inh Indices. Hmmm
    PFCindsExc_orig = PFCindsExc; PFCindsInh_orig = PFCindsInh; % Keep for comparison
    
    PFCindsNeu = []; PFCindsExc_Redo = []; PFCindsInh_Redo = []; PFCindsFS=[];
    for i=1:length(allripplemod)
        
        if strcmp(allripplemod(i).FStag,'y')
            PFCindsFS = [PFCindsFS; allripplemod(i).index];
        end
        
        if allripplemod(i).rasterShufP2>=0.05 && strcmp(allripplemod(i).FStag,'n')   % Get non-modulated indices, and not FS
            PFCindsNeu = [PFCindsNeu; allripplemod(i).index];
        else %Re-check Exc-Inh indices
            if strcmp(allripplemod(i).type, 'exc')  && strcmp(allripplemod(i).FStag,'n')
                PFCindsExc_Redo = [PFCindsExc_Redo; allripplemod(i).index];
            elseif strcmp(allripplemod(i).type, 'inh')  && strcmp(allripplemod(i).FStag,'n')
                PFCindsInh_Redo = [PFCindsInh_Redo; allripplemod(i).index];
            end
        end
    end
    
    % Originally, we had 60 Exc, 55 Inh, and 209 Neu. With FS removed, have to update this.
    % We now have        57 Exc, 52 Inh, and 203 Neu. (12 FS cells removed):312 RS and 12 FS, 12/324 = 3.7% FS;
    % Mod = 109/312 = 34.9% Exc = 57/312 = 18.3%, Inh = 52/312 = 1.7%
    
    PFCindsExc = PFCindsExc_Redo; % Overwrite the original PFCindsExc and Inh
    PFCindsInh = PFCindsInh_Redo;
    
    savefile =  '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_noFS_X6.mat'
    save(savefile,'PFCindsExc','PFCindsInh','PFCindsNeu','PFCindsExc_Redo','PFCindsInh_Redo','PFCindsFS','PFCindsExc_orig','PFCindsInh_orig');
end % end makePFCneuInds



if gatherGlobalTraj
    %fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/GlobalPEtraj_X6.mat';
    %load(fname,'allGlobalPEtraj','allGlobalIdxs', 'allGlobalPE');
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/GlobalPEtraj_Runtraj_noFS_X6.mat';
    load(fname,'allGlobalPEtraj','allGlobalIdxs', 'allGlobalPE','allRunTraj');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_noFS_X6.mat'; % PFC Exc-Inh-Neu Indices
    
    % a) First just get allPFC ones, instead of separating in Exc, Inh, Neu
    % This will get values for PFC, skipping all CA1 ones
    % You can separate while comparing with replay response directly
    PFCindsAll = [PFCindsExc;PFCindsInh;PFCindsNeu]; % All PFC mod
    PFCGlobalPEtraj=[]; PFCGlobalIdxs=[]; PFCGlobalPE=[]; pfcmatch=[];
    PFCRunTraj=[];
    cnt=0;
    for i = 1:length(PFCindsAll)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsAll(i,:), 'rows'));
        
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCGlobalIdxs = [PFCGlobalIdxs; PFCindsAll(i,:)];
            PFCGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % Also Separate Exc, Inh, Neu, just in case
    % b) PFCexc
    PFCexcGlobalPEtraj=[]; PFCexcGlobalIdxs=[]; PFCexcGlobalPE=[]; pfcmatch=[];
    cnt=0;
    for i = 1:length(PFCindsExc)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsExc(i,:), 'rows'));
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCexcGlobalIdxs = [PFCexcGlobalIdxs; PFCindsExc(i,:)];
            PFCexcGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCexcGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCexcRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % c) PFCinh
    PFCinhGlobalPEtraj=[]; PFCinhGlobalIdxs=[]; PFCinhGlobalPE=[]; pfcmatch=[];
    cnt=0;
    for i = 1:length(PFCindsInh)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsInh(i,:), 'rows'));
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCinhGlobalIdxs = [PFCinhGlobalIdxs; PFCindsInh(i,:)];
            PFCinhGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCinhGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCinhRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % d) PFCneu
    PFCneuGlobalPEtraj=[]; PFCneuGlobalIdxs=[]; PFCneuGlobalPE=[]; pfcmatch=[];
    cnt=0;
    for i = 1:length(PFCindsNeu)
        pfcmatch = find(ismember(allGlobalIdxs, PFCindsNeu(i,:), 'rows'));
        if ~isempty(pfcmatch)
            cnt=cnt+1;
            PFCneuGlobalIdxs = [PFCneuGlobalIdxs; PFCindsNeu(i,:)];
            PFCneuGlobalPEtraj{cnt} = allGlobalPEtraj{pfcmatch};
            PFCneuGlobalPE(cnt) = allGlobalPE(pfcmatch);
            PFCneuRunTraj{cnt} = allRunTraj{pfcmatch};
        end
    end
    
    % Have 310 of 312 total cells, 57/57 exc, 52/52 inh, 201/203 Neu
    
    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_GlobalPEtraj_Runtraj_noFS_X6.mat'
    save(savefile,'PFCGlobalIdxs','PFCGlobalPEtraj','PFCGlobalPE','PFCexcGlobalIdxs','PFCexcGlobalPEtraj','PFCexcGlobalPE', ...
        'PFCinhGlobalIdxs','PFCinhGlobalPEtraj','PFCinhGlobalPE','PFCneuGlobalIdxs','PFCneuGlobalPEtraj','PFCneuGlobalPE', ...
        'PFCRunTraj','PFCexcRunTraj','PFCinhRunTraj','PFCneuRunTraj');
    
end % end gatherGlobalTraj




if gatherPE
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/DR_Proc_SJ_noFS_X8.mat'; % updated, no CA2 cells anymore (951 instead of 1003 elements)
    load(fname,'PEcoefallv2INDS','PEcoefallv2','sparsityALLdata');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_noFS_X6.mat'; % PFC Exc-Inh-Neu Indices
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/CA1Inds_noFS_X8.mat'; % CA1 Indices: all
    
    pfcEXCPE=[]; pfcINHPE=[]; pfcNEUPE=[]; skippedPE=[]; usePECval = 3; %3 is norm PE2, 11 is norm PE1
    pfcEXCPEinds=[]; pfcINHPEinds=[]; pfcNEUPEinds=[];
    pfcEXCSpar=[]; pfcINHSpar=[]; pfcNEUSpar=[];
    
    % PEcoefallv2 has the following:
    % 1 = fldsepcomb{i,2} = INDEX
    % 2 = PE2corr, 3 = PE2corrnorm  (multiple values, for each epoch)
    % 4 = fldsepcombNOTSMOOTH{i,3}; %TAG of either  ca1 = 1, pfc = 2.
    %   PEcoefallv2{i,5} = PE2corrnormmin;
    %   PEcoefallv2{i,6} = {trunctrjsSMOOTHnorm};
    %   PEcoefallv2{i,7} = trunctrjsSMOOTH;
    %   PEcoefallv2{i,8} = shuffPE2corrnorm; PEcoefallv2{i,9} = shuffPE2corrnormmin; PEcoefallv2{i,10} = PE2corrnormMinusShuff; PEcoefallv2{i,11} = PE1corrnorm;
    %   PEcoefallv2{i,12} = PE2corrMinusShuff; PEcoefallv2{i,13} = PE1corr;
    %   PEcoefallv2INDS(i,[1:4]) = fldsepcomb{i,2};
    
    % sparsityALLdata has the following:
    %   sparsityALLdata{i,1} = fldsepcomb{i,2};   INDEX
    %   sparsityALLdata{i,2} = [sparsityabsval sparsityfrac]; % Abs val and fraction value of sparsity/ spatial coverage
    %   sparsityALLdata{i,3} = fldsepcombNOTSMOOTH{i,3}; % Whether CA1(1) or PFC(2)
    
    % Get all the PE values for Exc and Inh PFC
    for i = 1:length(PFCindsExc);
        try%skip cells without PEC
            %              mean(pfcINHPE)   pfcexcmatch = find(ismember(PEcoefall(:,1:4), PFCindsExc(i,:), 'rows'));
            pfcexcmatch = find(ismember(PEcoefallv2INDS(:,1:4), PFCindsExc(i,:), 'rows'));
            %                 pfcEXCPE(i,:) = PEcoefallv2{pfcexcmatch,3}';
            pfcEXCPE(i,:) = nanmean([PEcoefallv2{pfcexcmatch,usePECval}']); % mean across the two PE values
            pfcEXCPEinds(i,:) = PEcoefallv2{pfcexcmatch,1};
            if PEcoefallv2{pfcexcmatch,1}(1,[1:4]) ~= sparsityALLdata{pfcexcmatch,1};
                keyboard %pause if the sparsity data and the PE data don't have matching indices
            end
            %pfcEXCSpar(i,:) = [sparsityALLdata{pfcexcmatch,2} pfcEXCPE(i)];
            if ~isnan(sparsityALLdata{pfcexcmatch,2})
                pfcEXCSpar(i,:) = [sparsityALLdata{pfcexcmatch,2}(2)]; % Only keep fraction of area>thresh
            else
                pfcEXCSpar(i,:) = nan;
            end
        catch
            keyboard;
            skippedPE = [skippedPE; PFCindsExc(i,:)];
        end
    end
    
    [pfcEXCSpar_sort,xx] = sort(pfcEXCSpar);
    pfcEXCinds_sort = pfcEXCPEinds(xx,:);
    
    for i = 1:length(PFCindsInh);
        try %skip cells without PEC
            %                 pfcinhmatch = find(ismember(PEcoefall(:,1:4), PFCindsInh(i,:), 'rows'));
            %                 pfcINHPE(i) = PEcoefall(pfcinhmatch,5);
            pfcinhmatch = find(ismember(PEcoefallv2INDS(:,1:4), PFCindsInh(i,:), 'rows'));
            %                 pfcINHPE(i,:) = PEcoefallv2{pfcinhmatch,3}';
            pfcINHPE(i,:) = nanmean([PEcoefallv2{pfcinhmatch,usePECval}']);
            pfcINHPEinds(i,:) = PEcoefallv2{pfcinhmatch,1};
            if PEcoefallv2{pfcinhmatch,1}(1,[1:4]) ~= sparsityALLdata{pfcinhmatch,1};
                keyboard %pause if the sparsity data and the PE data don't have matching indices
            end
            if ~isnan(sparsityALLdata{pfcinhmatch,2})
                pfcINHSpar(i,:) = [sparsityALLdata{pfcinhmatch,2}(2)];
            else
                pfcINHSpar(i,:) = nan;
            end
        catch
            keyboard;
            skippedPE = [skippedPE; PFCindsInh(i,:)];
        end
    end
    
    [pfcINHSpar_sort,xx] = sort(pfcINHSpar);
    pfcINHinds_sort = pfcINHPEinds(xx,:);
    
    for i = 1:length(PFCindsNeu);
        try %skip cells without PEC
            %                 pfcinhmatch = find(ismember(PEcoefall(:,1:4), PFCindsInh(i,:), 'rows'));
            %                 pfcINHPE(i) = PEcoefall(pfcinhmatch,5);
            pfcneumatch = find(ismember(PEcoefallv2INDS(:,1:4), PFCindsNeu(i,:), 'rows'));
            %                 pfcINHPE(i,:) = PEcoefallv2{pfcinhmatch,3}';
            pfcNEUPE(i,:) = nanmean([PEcoefallv2{pfcneumatch,usePECval}']);
            pfcNEUPEinds(i,:) = PEcoefallv2{pfcneumatch,1};
            if PEcoefallv2{pfcneumatch,1}(1,[1:4]) ~= sparsityALLdata{pfcneumatch,1};
                keyboard %pause if the sparsity data and the PE data don't have matching indices
            end
            if ~isnan(sparsityALLdata{pfcneumatch,2})
                pfcNEUSpar(i,:) = [sparsityALLdata{pfcneumatch,2}(2)];               
            else
                pfcNEUSpar(i,:) = nan;
            end
        catch
            %keyboard;
            skippedPE = [skippedPE; PFCindsNeu(i,:)];
        end
    end
    
    [pfcNEUSpar_sort,xx] = sort(pfcNEUSpar);
    pfcNEUinds_sort = pfcNEUPEinds(xx,:);
    
    
    % Save data for getting sparsity/ covergae examples
    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_SpatialSparsityInds_noFS_X8.mat'
    %save(savefile,'pfcEXCinds_sort','pfcINHinds_sort','pfcNEUinds_sort','pfcEXCSpar_sort','pfcINHSpar_sort','pfcNEUSpar_sort');
    
    
    
    %savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_PE_noFS_X6.mat'
    %save(savefile,'pfcEXCPE','pfcINHPE','pfcNEUPE','pfcEXCPEinds','pfcINHPEinds','pfcNEUPEinds');
    
    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_PEandSparsity_noFS_X8.mat'
    %save(savefile,'pfcEXCPE','pfcINHPE','pfcNEUPE','pfcEXCPEinds','pfcINHPEinds','pfcNEUPEinds',...
    %    'pfcEXCSpar','pfcINHSpar','pfcNEUSpar');
    
    
    % Get CA1 PE as well
    CA1PE=[]; skippedCA1PE=[]; CA1PEinds=[];
    % Get all the PE values for Exc and Inh PFC
    for i = 1:length(CA1inds);
        try%skip cells without PEC
            %              mean(pfcINHPE)   pfcexcmatch = find(ismember(PEcoefall(:,1:4), PFCindsExc(i,:), 'rows'));
            match = find(ismember(PEcoefallv2INDS(:,1:4), CA1inds(i,:), 'rows'));
            %                 pfcEXCPE(i,:) = PEcoefallv2{pfcexcmatch,3}';
            CA1PE(i,:) = nanmean([PEcoefallv2{match,usePECval}']); % mean across the two PE values
            CA1PEinds(i,:) = PEcoefallv2{match,1};
            if PEcoefallv2{match,1}(1,[1:4]) ~= sparsityALLdata{match,1};
                keyboard %pause if the sparsity data and the PE data don't have matching indices
            end
            if ~isnan(sparsityALLdata{match,2})
                CA1Spar(i,:) = [sparsityALLdata{match,2}(2)];              
            else
                CA1Spar(i,:) = nan;
            end
            
        catch
            %keyboard;
            skippedCA1PE = [skippedCA1PE; CA1inds(i,:)];
        end
    end
    
    
    [CA1Spar_sort,xx] = sort(CA1Spar);
    CA1inds_sort = CA1PEinds(xx,:);
    
    
    % Save data for getting sparsity/ covergae examples
    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCandCA1_SpatialSparsityInds_noFS_X8.mat'
    save(savefile,'pfcEXCinds_sort','pfcINHinds_sort','pfcNEUinds_sort','pfcEXCSpar_sort','pfcINHSpar_sort','pfcNEUSpar_sort',...
        'CA1inds_sort','CA1Spar_sort');
    

    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCandCA1_PEandSparsity_noFS_X8.mat'
    %save(savefile,'pfcEXCPE','pfcINHPE','pfcNEUPE','CA1PE','pfcEXCPEinds','pfcINHPEinds','pfcNEUPEinds','CA1PEinds',...
    %    'pfcEXCSpar','pfcINHSpar','pfcNEUSpar','CA1Spar');
    
    
    
    figure; hold on;
    %bar([mean(inhsignegrippairs) mean(inhnonsigrippairs) mean(inhsigposrippairs) mean(excsignegrippairs) mean(excnonsigrippairs) mean(excsigposrippairs)]); hold on;
    bar(1, nanmean(pfcEXCPE),'r');
    bar(2, nanmean(pfcINHPE),'b');
    bar(3, nanmean(pfcNEUPE),'c');
    bar(4, nanmean(CA1PE),'k');
    legend('PFCexc','PFCinh','PFCneu','CA1');
    errorbar2(1, nanmean(pfcEXCPE), nanstderr(pfcEXCPE), 0.3, 'r');
    errorbar2(2, nanmean(pfcINHPE), nanstderr(pfcINHPE), 0.3, 'b');
    errorbar2(3, nanmean(pfcNEUPE), nanstderr(pfcNEUPE), 0.3, 'c');
    errorbar2(4, nanmean(CA1PE), nanstderr(CA1PE), 0.3, 'k');
    
    %bar([mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)]); hold on;
    %errorbar2([1 2 3], [mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)],  [stderr(pfcEXCPE) stderr(pfcINHPE) stderr(pfcNEUPE)] , 0.3, 'k')
    ylim([0 1]);
    title('Path Equivalence for PFC and CA1 neurons ','FontSize',24,'FontWeight','normal');
    ylabel('Path Equivalence','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3 4]); set(gca,'XTickLabel',{'Exc','Inh','Neu','CA1'});
    
    
    figfile = [figdir,'PathEquivalence_PFCandCA1_X8']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    [pKW table statsKW] = kruskalwallis([pfcEXCPE' pfcINHPE' pfcNEUPE' CA1PE'], [ones(1, length(pfcEXCPE)) ones(1,length(pfcINHPE)).*2 ...
        ones(1,length(pfcNEUPE)).*3 ones(1,length(CA1PE)).*4]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    [pKW table statsKW] = kruskalwallis([pfcEXCPE' pfcINHPE' pfcNEUPE'], [ones(1, length(pfcEXCPE)) ones(1,length(pfcINHPE)).*2 ...
        ones(1,length(pfcNEUPE)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    
    figure; hold on;
    bar(1, nanmean(pfcEXCPE),'r');
    bar(2, nanmean(pfcINHPE),'b');
    bar(3, nanmean(pfcNEUPE),'c');
    legend('PFCexc','PFCinh','PFCneu');
    errorbar2(1, nanmean(pfcEXCPE), nanstderr(pfcEXCPE), 0.3, 'r');
    errorbar2(2, nanmean(pfcINHPE), nanstderr(pfcINHPE), 0.3, 'b');
    errorbar2(3, nanmean(pfcNEUPE), nanstderr(pfcNEUPE), 0.3, 'c');
    
    %bar([mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)]); hold on;
    %errorbar2([1 2 3], [mean(pfcEXCPE) mean(pfcINHPE) mean(pfcNEUPE)],  [stderr(pfcEXCPE) stderr(pfcINHPE) stderr(pfcNEUPE)] , 0.3, 'k')
    ylim([0 1]);
    title('Path Equivalence for PFC neurons ','FontSize',24,'FontWeight','normal');
    ylabel('Path Equivalence','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3]); set(gca,'XTickLabel',{'Exc','Inh','Neu'});
    
    
    figfile = [figdir,'PathEquivalence_PFC']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    [pKW table statsKW] = kruskalwallis([pfcEXCPE' pfcINHPE' pfcNEUPE'], [ones(1, length(pfcEXCPE)) ones(1,length(pfcINHPE)).*2 ...
        ones(1,length(pfcNEUPE)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    [pKW table statsKW] = kruskalwallis([pfcEXCPE' pfcINHPE'], [ones(1, length(pfcEXCPE)) ones(1,length(pfcINHPE)).*2]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    
    
    % Plot distributions and do ranksum/ kstest2
    %create histograms and normalize
    edges = [0:.1:1];
    pfcINHPEhist = histc(pfcINHPE,edges); pfcINHPEhist = pfcINHPEhist./nansum(pfcINHPEhist);
    pfcEXCPEhist = histc(pfcEXCPE,edges); pfcEXCPEhist = pfcEXCPEhist./nansum(pfcEXCPEhist);
    pfcNEUPEhist = histc(pfcNEUPE,edges); pfcNEUPEhist = pfcNEUPEhist./nansum(pfcNEUPEhist);
    ca1PEallhist = histc(CA1PE,edges); ca1PEallhist = ca1PEallhist./nansum(ca1PEallhist);
    
    figure; hold on;
    plot(edges,pfcEXCPEhist,'r', edges,pfcINHPEhist, 'b', edges, pfcNEUPEhist,'c', edges, ca1PEallhist,'k','LineWidth',2);
    axis([-.1 1 0 max(max([pfcINHPEhist pfcEXCPEhist pfcNEUPEhist ca1PEallhist]))]);
    xlabel('PE: Norm Overlap'); ylabel('Fraction of cells');
    
    figfile = [figdir,'PathEquivalenceDistr_PFCandCA1']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    [pk1,hk1] = ranksum(pfcEXCPE, pfcINHPE), % p = 4.4 x 10-4
    [pk2,hk2] = ranksum(pfcEXCPE, pfcNEUPE), % p = 1.1X10-5
    [pk3,hk3] = ranksum(pfcINHPE, pfcNEUPE), % p = 0.78
    [pk0,hk0] = ranksum(CA1PE, pfcNEUPE),
    
    
    % Sparsity Plotting
    
    figure; hold on;
    bar(1,nanmean(pfcEXCSpar),'facecolor','r');
    bar(2,nanmean(pfcINHSpar),'facecolor','b'); hold on;
    bar(3,nanmean(pfcNEUSpar),'facecolor','c');
    %bar(3,nanmean(pfcSparallexcl(:,2)),'facecolor','g');
    bar(4,nanmean(CA1Spar),'facecolor','k');
    legend('PFCexc','PFCinh','PFCneu','CA1');
    
    title('% Area >.25peak'); set(gca,'xtick',[], 'xticklabel', [])
    errorbar2(1, nanmean(pfcEXCSpar), nanstderr(pfcEXCSpar), 0.3, 'r');
    errorbar2(2, nanmean(pfcINHSpar), nanstderr(pfcINHSpar), 0.3, 'b');
    errorbar2(3, nanmean(pfcNEUSpar), nanstderr(pfcNEUSpar), 0.3, 'c');
    errorbar2(4, nanmean(CA1Spar), nanstderr(CA1Spar), 0.3, 'k');
    
    ylim([0 1]);
    title('Spatial Coverage (Sparsity) for PFC and CA1 neurons ','FontSize',24,'FontWeight','normal');
    ylabel('% Area >.25peak','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3 4]); set(gca,'XTickLabel',{'Exc','Inh','Neu','CA1'});
    
    
    figfile = [figdir,'Sparsity_PFCandCA1_X8']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    [pKW table statsKW] = kruskalwallis([pfcEXCSpar' pfcINHSpar' pfcNEUSpar' CA1Spar'], [ones(1, length(pfcEXCSpar)) ones(1,length(pfcINHSpar)).*2 ...
        ones(1,length(pfcNEUSpar)).*3 ones(1,length(CA1Spar)).*4]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    [pKW table statsKW] = kruskalwallis([pfcEXCSpar' pfcINHSpar' pfcNEUSpar'], [ones(1, length(pfcEXCSpar)) ones(1,length(pfcINHSpar)).*2 ...
        ones(1,length(pfcNEUSpar)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    [pKW table statsKW] = kruskalwallis([pfcEXCSpar' pfcINHSpar'], [ones(1, length(pfcEXCSpar)) ones(1,length(pfcINHSpar)).*2]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range.
    
    
    % Plot distributions and do ranksum/ kstest2
    %create histograms and normalize
    edges = [0:.2:1];
    pfcINHSparhist = histc(pfcINHSpar,edges); pfcINHSparhist = pfcINHSparhist./nansum(pfcINHSparhist);
    pfcEXCSparhist = histc(pfcEXCSpar,edges); pfcEXCSparhist = pfcEXCSparhist./nansum(pfcEXCSparhist);
    pfcNEUSparhist = histc(pfcNEUSpar,edges); pfcNEUSparhist = pfcNEUSparhist./nansum(pfcNEUSparhist);
    ca1Sparallhist = histc(CA1Spar,edges); ca1Sparallhist = ca1Sparallhist./nansum(ca1Sparallhist);
    
    figure; hold on;
    plot(edges,pfcEXCSparhist,'r', edges,pfcINHSparhist, 'b', edges, pfcNEUSparhist,'c', edges, ca1Sparallhist,'k','LineWidth',2);
    legend('PFCexc','PFCinh','PFCneu','CA1');
    axis([-.1 1 0 max(max([pfcINHSparhist pfcEXCSparhist pfcNEUSparhist ca1Sparallhist]))]);
    xlabel('Spar: % area > Thrs'); ylabel('Fraction of cells');
    
    figfile = [figdir,'SparsityDistr_PFCandCA1_bin2']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    [pk1,hk1] = ranksum(pfcEXCSpar, pfcINHSpar), % p = 0.0044
    [pk2,hk2] = ranksum(pfcEXCSpar, pfcNEUSpar), % p = 9.4 x 10-5
    [pk3,hk3] = ranksum(pfcINHSpar, pfcNEUSpar), % p = 0.51
    [pk0,hk0] = ranksum(CA1Spar, pfcNEUSpar),
    
    
end














% Arm Index - and comparison with PE
% ----------------------------------
if plotPE_armidx
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_PFC_ArmIdx_gather_noFS_X6.mat';
    load(fname,'all_AI_PFCidxs','all_AI_PFCArmIdx');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_noFS_X6.mat'; % PFC Exc-Inh-Neu Indices
    PFCmod = [PFCindsExc;PFCindsInh]; % All PFC mod
    
    % Plot just ArmIdx
    excind = []; inhind = []; neuind = [];
    for i=1:size(all_AI_PFCidxs,1)
        excmatch = find(ismember(all_AI_PFCidxs(i,:), PFCindsExc, 'rows'));
        if ~isempty(excmatch)
            excind = [excind; i];
        else
            inhmatch = find(ismember(all_AI_PFCidxs(i,:), PFCindsInh, 'rows'));
            if ~isempty(inhmatch)
                inhind = [inhind; i];
            else
                neumatch = find(ismember(all_AI_PFCidxs(i,:), PFCindsNeu, 'rows'));
                if ~isempty(neumatch)
                    neuind = [neuind; i];
                end
            end
        end
    end
    
    exc_armidx = all_AI_PFCArmIdx(excind);
    inh_armidx = all_AI_PFCArmIdx(inhind);
    neu_armidx = all_AI_PFCArmIdx(neuind);
    
    excInds = all_AI_PFCidxs(excind,:);
    inhInds = all_AI_PFCidxs(inhind,:);
    neuInds = all_AI_PFCidxs(neuind,:);

    % Get CA1 as well
    % ---------------
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_CA1_ArmIdx_gather_noFS_X8.mat';
    load(fname,'all_AI_CA1idxs','all_AI_CA1ArmIdx');
    %load '/data25/sjadhav/HPExpt/HP_ProcessedData/CA1Inds_noFS_X8.mat'; % Dont need
    CA1_armidx = all_AI_CA1ArmIdx;
  
    
    % Sort and Save Indxs for Plotting examples
    [exc_armidx_sort,xx] = sort(abs(exc_armidx));
    excinds_sort = excInds(xx,:);
    [inh_armidx_sort,xx] = sort(abs(inh_armidx));
    inhinds_sort = inhInds(xx,:);
    [neu_armidx_sort,xx] = sort(abs(neu_armidx));
    neuinds_sort = neuInds(xx,:);
    [CA1_armidx_sort,xx] = sort(abs(CA1_armidx));
    CA1inds_sort = all_AI_CA1idxs(xx,:);
    
    exc_armidx_sort = exc_armidx_sort';
    inh_armidx_sort = inh_armidx_sort';
    neu_armidx_sort = neu_armidx_sort';
    CA1_armidx_sort = CA1_armidx_sort';
    
    % Save data for getting sparsity/ covergae examples
    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCandCA1_ArmInds_noFS_X8.mat'
    save(savefile,'excinds_sort','inhinds_sort','neuinds_sort','CA1inds_sort','exc_armidx_sort','inh_armidx_sort','neu_armidx_sort',...
    'CA1_armidx_sort');
    
    
    
    
    
    figure; hold on;
    bar(1, nanmean(abs(exc_armidx)),'r');
    bar(2, nanmean(abs(inh_armidx)),'b');
    bar(3, nanmean(abs(neu_armidx)),'c');
    bar(4, nanmean(abs(CA1_armidx)),'k');
    legend('PFCexc','PFCinh','PFCneu','CA1');
    errorbar2(1, nanmean(abs(exc_armidx)), nanstderr(abs(exc_armidx)), 0.3, 'r');
    errorbar2(2, nanmean(abs(inh_armidx)), nanstderr(abs(inh_armidx)), 0.3, 'b');
    errorbar2(3, nanmean(abs(neu_armidx)), nanstderr(abs(neu_armidx)), 0.3, 'c');
    errorbar2(4, nanmean(abs(CA1_armidx)), nanstderr(abs(CA1_armidx)), 0.3, 'k');
    
    %ylim([-1 1]);
    title('Arm Idx for PFC and CA1 neurons','FontSize',24,'FontWeight','normal');
    ylabel('Arm Idx','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3 4]); set(gca,'XTickLabel',{'Exc','Inh','Neu','CA1'});

    

    
    [pKW table statsKW] = kruskalwallis([abs(exc_armidx) abs(inh_armidx) abs(neu_armidx)], [ones(1, length(exc_armidx)) ones(1,length(inh_armidx)).*2 ones(1,length(neu_armidx)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range
    c
    title(sprintf('Arm Idx for PFC neurons; pKW(%0.3f)', pKW));
    
    figfile = [figdir,'Abs-ArmIdx_PFCandCA1']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    
     
    % Plot distributions and do ranksum/ kstest2
    %create histograms and normalize
    edges = [0:.1:1];
    pfcINHSparhist = histc(abs(inh_armidx),edges); pfcINHSparhist = pfcINHSparhist./nansum(pfcINHSparhist);
    pfcEXCSparhist = histc(abs(exc_armidx),edges); pfcEXCSparhist = pfcEXCSparhist./nansum(pfcEXCSparhist);
    pfcNEUSparhist = histc(abs(neu_armidx),edges); pfcNEUSparhist = pfcNEUSparhist./nansum(pfcNEUSparhist);
    ca1Sparallhist = histc(abs(CA1_armidx),edges); ca1Sparallhist = ca1Sparallhist./nansum(ca1Sparallhist);
    
    figure; hold on;
    plot(edges,pfcEXCSparhist,'r', edges,pfcINHSparhist, 'b', edges, pfcNEUSparhist,'c', edges, ca1Sparallhist,'k','LineWidth',2);
    %plot(edges,pfcEXCSparhist,'r', edges,pfcINHSparhist, 'b', edges, pfcNEUSparhist,'c','LineWidth',2);
    legend('PFCexc','PFCinh','PFCneu','CA1');
    axis([-.1 1 0 max(max([pfcINHSparhist pfcEXCSparhist pfcNEUSparhist ca1Sparallhist]))]);
    %axis([-.1 1 0 max(max([pfcINHSparhist pfcEXCSparhist pfcNEUSparhist]))]);
    xlabel('Arm Index'); ylabel('Fraction of cells');
    
    figfile = [figdir,'ArmIdxDistr_PFCand CA1']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    [pk1,hk1] = ranksum(abs(exc_armidx), abs(inh_armidx)), % p = 0.21
    [pk2,hk2] = ranksum(abs(exc_armidx), abs(neu_armidx)), % p = 0.01
    [pk3,hk3] = ranksum(abs(neu_armidx), abs(inh_armidx)), % p = 0.58
    
    
    
    
    
    
    % Plot PE vs ArmIdx
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_PE_noFS_X6.mat'
    excmatch=[]; inhmatch=[]; neumatch=[];
    excAI=[]; inhAI = []; neuAI=[]; excPE=[]; inhPE=[]; neuPE=[];
    
    for i=1:size(all_AI_PFCidxs,1)
        excmatch = find(ismember(pfcEXCPEinds, all_AI_PFCidxs(i,:), 'rows'));
        if ~isempty(excmatch)
            excAI = [excAI; all_AI_PFCArmIdx(i)];
            excPE = [excPE; pfcEXCPE(excmatch)];
        else
            inhmatch = find(ismember( pfcINHPEinds, all_AI_PFCidxs(i,:), 'rows'));
            if ~isempty(inhmatch)
                inhAI = [inhAI; all_AI_PFCArmIdx(i)];
                inhPE = [inhPE; pfcINHPE(inhmatch)];
            else
                neumatch = find(ismember(pfcNEUPEinds, all_AI_PFCidxs(i,:), 'rows'));
                if ~isempty(neumatch)
                    neuAI = [neuAI; all_AI_PFCArmIdx(i)];
                    neuPE = [neuPE; pfcNEUPE(neumatch)];
                end
            end
        end
    end
    
    rem = find(isnan(excPE) | isnan(excAI)); excAI(rem)=[]; excPE(rem)=[];
    rem = find(isnan(inhPE) | isnan(inhAI)); inhAI(rem)=[]; inhPE(rem)=[];
    rem = find(isnan(neuPE) | isnan(neuAI)); neuAI(rem)=[]; neuPE(rem)=[];
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(excPE,excAI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(excPE));
        randPE = excPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,excAI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inhPE,inhAI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inhPE));
        randPE = inhPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,inhAI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neuPE,neuAI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neuPE));
        randPE = neuPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,neuAI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    
    figure; hold on;
    subplot(2,2,1);
    scatter(excPE, excAI, 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(excAI, [ones(size(excPE)) excPE]);
    plot(min(excPE):.01:max(excPE), b(1)+b(2)*[min(excPE):.01:max(excPE)],'r')
    axis([0 1 -1 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inhPE, inhAI, 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(inhAI, [ones(size(inhPE)) inhPE]);
    plot(min(inhPE):.01:max(inhPE), b(1)+b(2)*[min(inhPE):.01:max(inhPE)],'b')
    axis([0 1 -1 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neuPE, neuAI, 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(neuAI, [ones(size(neuPE)) neuPE]);
    plot(min(neuPE):.01:max(neuPE), b(1)+b(2)*[min(neuPE):.01:max(neuPE)],'k')
    axis([0 1 -1 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
    
    figfile = [figdir,'ArmIdx_vs_PE_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    % Plot PE vs ABS ArmIdx, which makes more sense
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(excPE,abs(excAI)); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(excPE));
        randPE = excPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,abs(excAI));
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inhPE,abs(inhAI)); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inhPE));
        randPE = inhPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,abs(inhAI));
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neuPE,abs(neuAI)); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neuPE));
        randPE = neuPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,abs(neuAI));
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    
    figure; hold on;
    subplot(2,2,1);
    scatter(excPE, abs(excAI), 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(abs(excAI), [ones(size(excPE)) excPE]);
    plot(min(excPE):.01:max(excPE), b(1)+b(2)*[min(excPE):.01:max(excPE)],'r')
    axis([0 1 0 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inhPE, abs(inhAI), 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(abs(inhAI), [ones(size(inhPE)) inhPE]);
    plot(min(inhPE):.01:max(inhPE), b(1)+b(2)*[min(inhPE):.01:max(inhPE)],'b')
    axis([0 1 0 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neuPE, abs(neuAI), 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(abs(neuAI), [ones(size(neuPE)) neuPE]);
    plot(min(neuPE):.01:max(neuPE), b(1)+b(2)*[min(neuPE):.01:max(neuPE)],'k')
    axis([0 1 0 1]);
    ylabel('ArmIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
    
    figfile = [figdir,'Abs-ArmIdx_vs_PE_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    
end % end plotPE_armidx









if plotDirnIdx
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_PFC_DirnIdx_gather_noFS_X6.mat';
    load(fname,'all_DI_PFCidxs','all_DI_PFCDirnIdx');
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCInds_noFS_X6.mat'; % PFC Exc-Inh-Neu Indices
    PFCmod = [PFCindsExc;PFCindsInh]; % All PFC mod
    
    % Plot just DirnIdx
    excind = []; inhind = []; neuind = [];
    for i=1:size(all_DI_PFCidxs,1)
        excmatch = find(ismember(all_DI_PFCidxs(i,:), PFCindsExc, 'rows'));
        if ~isempty(excmatch)
            excind = [excind; i];
        else
            inhmatch = find(ismember(all_DI_PFCidxs(i,:), PFCindsInh, 'rows'));
            if ~isempty(inhmatch)
                inhind = [inhind; i];
            else
                neumatch = find(ismember(all_DI_PFCidxs(i,:), PFCindsNeu, 'rows'));
                if ~isempty(neumatch)
                    neuind = [neuind; i];
                end
            end
        end
    end
    
     % Get CA1 as well
    % ---------------
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_CA1_DirnIdx_gather_noFS_X8.mat';
    load(fname,'all_DI_CA1idxs','all_DI_CA1DirnIdx');
    %load '/data25/sjadhav/HPExpt/HP_ProcessedData/CA1Inds_noFS_X8.mat'; % Dont need
    CA1_Dirnidx = abs(all_DI_CA1DirnIdx); 
 
    
    % USE ABSOLUTE VALUES
    exc_Dirnidx = abs(all_DI_PFCDirnIdx(excind));
    inh_Dirnidx = abs(all_DI_PFCDirnIdx(inhind));
    neu_Dirnidx = abs(all_DI_PFCDirnIdx(neuind));  
    
    excInds = all_DI_PFCidxs(excind,:);
    inhInds = all_DI_PFCidxs(inhind,:);
    neuInds = all_DI_PFCidxs(neuind,:);
    
    
    % Sort and Save Indxs for Plotting examples
    [exc_Dirnidx_sort,xx] = sort(abs(exc_Dirnidx));
    excinds_sort = excInds(xx,:);
    [inh_Dirnidx_sort,xx] = sort(abs(inh_Dirnidx));
    inhinds_sort = inhInds(xx,:);
    [neu_Dirnidx_sort,xx] = sort(abs(neu_Dirnidx));
    neuinds_sort = neuInds(xx,:);
    [CA1_Dirnidx_sort,xx] = sort(abs(CA1_Dirnidx));
    CA1inds_sort = all_DI_CA1idxs(xx,:);
    
    exc_Dirnidx_sort = exc_Dirnidx_sort';
    inh_Dirnidx_sort = inh_Dirnidx_sort';
    neu_Dirnidx_sort = neu_Dirnidx_sort';
    CA1_Dirnidx_sort = CA1_Dirnidx_sort';
    
    % Save data for getting sparsity/ coverage examples
    savefile = '/data25/sjadhav/HPExpt/HP_ProcessedData/PFCandCA1_DirnInds_noFS_X8.mat'
    save(savefile,'excinds_sort','inhinds_sort','neuinds_sort','CA1inds_sort','exc_Dirnidx_sort','inh_Dirnidx_sort','neu_Dirnidx_sort',...
    'CA1_Dirnidx_sort');
    
    
    
    figure; hold on;
    bar(1, nanmean(exc_Dirnidx),'r');
    bar(2, nanmean(inh_Dirnidx),'b');
    bar(3, nanmean(neu_Dirnidx),'c');
    bar(4, nanmean(CA1_Dirnidx),'k');
    legend('PFCexc','PFCinh','PFCneu','CA1');
    errorbar2(1, nanmean(exc_Dirnidx), nanstderr(exc_Dirnidx), 0.3, 'r');
    errorbar2(2, nanmean(inh_Dirnidx), nanstderr(inh_Dirnidx), 0.3, 'b');
    errorbar2(3, nanmean(neu_Dirnidx), nanstderr(neu_Dirnidx), 0.3, 'c');
    errorbar2(4, nanmean(CA1_Dirnidx), nanstderr(CA1_Dirnidx), 0.3, 'k');
    %ylim([-1 1]);
    title('Dirn Idx for PFC neurons ','FontSize',24,'FontWeight','normal');
    ylabel('Abs (Dirn Idx)','FontSize',24,'FontWeight','normal')
    set(gca,'XTick',[1 2 3 4]); set(gca,'XTickLabel',{'Exc','Inh','Neu','CA1'});
    
    figfile = [figdir,'Abs-DirnIdx_PFCandCA1']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    [pKW table statsKW] = kruskalwallis([exc_Dirnidx inh_Dirnidx neu_Dirnidx], [ones(1, length(exc_Dirnidx)) ones(1,length(inh_Dirnidx)).*2 ones(1,length(neu_Dirnidx)).*3]);
    [c, m, h, gnames] = multcompare(statsKW, 'estimate', 'kruskalwallis', 'ctype', 'hsd', 'display', 'on', 'alpha', 0.05); % change the alpha values to determine significance range
    c
    
    title(sprintf('Dirn Idx for PFC neurons; pKW(%0.3f)', pKW));
    
   
    % Plot distributions and do ranksum/ kstest2
    %create histograms and normalize
    edges = [0:.1:1];
    pfcINHSparhist = histc(abs(inh_Dirnidx),edges); pfcINHSparhist = pfcINHSparhist./nansum(pfcINHSparhist);
    pfcEXCSparhist = histc(abs(exc_Dirnidx),edges); pfcEXCSparhist = pfcEXCSparhist./nansum(pfcEXCSparhist);
    pfcNEUSparhist = histc(abs(neu_Dirnidx),edges); pfcNEUSparhist = pfcNEUSparhist./nansum(pfcNEUSparhist);
    ca1Sparallhist = histc(abs(CA1_Dirnidx),edges); ca1Sparallhist = ca1Sparallhist./nansum(ca1Sparallhist);
    
    figure; hold on;
    plot(edges,pfcEXCSparhist,'r', edges,pfcINHSparhist, 'b', edges, pfcNEUSparhist,'c', edges, ca1Sparallhist,'k','LineWidth',2);
    %plot(edges,pfcEXCSparhist,'r', edges,pfcINHSparhist, 'b', edges, pfcNEUSparhist,'c','LineWidth',2);
    legend('PFCexc','PFCinh','PFCneu','CA1');
    %axis([-.1 1 0 max(max([pfcINHSparhist pfcEXCSparhist pfcNEUSparhist ca1Sparallhist]))]);
    axis([-.1 1 0 max(max([pfcINHSparhist pfcEXCSparhist pfcNEUSparhist]))]);
    xlabel('Dirn Index'); ylabel('Fraction of cells');
    
    figfile = [figdir,'DirnIdxDistr_PFCandCA1']
    if savefig1==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    [pk1,hk1] = ranksum(abs(exc_Dirnidx), abs(inh_Dirnidx)), % p = 0.54
    [pk2,hk2] = ranksum(abs(exc_Dirnidx), abs(neu_Dirnidx)), % p = 0.005
    [pk3,hk3] = ranksum(abs(neu_Dirnidx), abs(inh_Dirnidx)), % p = 0.058
    
    
    
    
    
    
    
    
    
    % Plot PE vs DirnIdx
    load '/data25/sjadhav/HPExpt/HP_ProcessedData/PFC_PE_noFS_X6.mat'
    excmatch=[]; inhmatch=[]; neumatch=[];
    excDI=[]; inhDI = []; neuDI=[]; excPE=[]; inhPE=[]; neuPE=[];
    
    for i=1:size(all_DI_PFCidxs,1)
        excmatch = find(ismember(pfcEXCPEinds, all_DI_PFCidxs(i,:), 'rows'));
        if ~isempty(excmatch)
            excDI = [excDI; all_DI_PFCDirnIdx(i)];
            excPE = [excPE; pfcEXCPE(excmatch)];
        else
            inhmatch = find(ismember( pfcINHPEinds, all_DI_PFCidxs(i,:), 'rows'));
            if ~isempty(inhmatch)
                inhDI = [inhDI; all_DI_PFCDirnIdx(i)];
                inhPE = [inhPE; pfcINHPE(inhmatch)];
            else
                neumatch = find(ismember(pfcNEUPEinds, all_DI_PFCidxs(i,:), 'rows'));
                if ~isempty(neumatch)
                    neuDI = [neuDI; all_DI_PFCDirnIdx(i)];
                    neuPE = [neuPE; pfcNEUPE(neumatch)];
                end
            end
        end
    end
    
    excDI = abs(excDI); inhDI = abs(inhDI); neuDI = abs(neuDI);
    
    rem = find(isnan(excPE) | isnan(excDI)); excDI(rem)=[]; excPE(rem)=[];
    rem = find(isnan(inhPE) | isnan(inhDI)); inhDI(rem)=[]; inhPE(rem)=[];
    rem = find(isnan(neuPE) | isnan(neuDI)); neuDI(rem)=[]; neuPE(rem)=[];
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(excPE,excDI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(excPE));
        randPE = excPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,excDI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inhPE,inhDI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inhPE));
        randPE = inhPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,inhDI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neuPE,neuDI); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neuPE));
        randPE = neuPE(rorder);
        [rtmp, ptmp] = corrcoef(randPE,neuDI);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    
    figure; hold on;
    subplot(2,2,1);
    scatter(excPE, excDI, 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(excDI, [ones(size(excPE)) excPE]);
    plot(min(excPE):.01:max(excPE), b(1)+b(2)*[min(excPE):.01:max(excPE)],'r')
    axis([0 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inhPE, inhDI, 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(inhDI, [ones(size(inhPE)) inhPE]);
    plot(min(inhPE):.01:max(inhPE), b(1)+b(2)*[min(inhPE):.01:max(inhPE)],'b')
    axis([0 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neuPE, neuDI, 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(neuDI, [ones(size(neuPE)) neuPE]);
    plot(min(neuPE):.01:max(neuPE), b(1)+b(2)*[min(neuPE):.01:max(neuPE)],'k')
    axis([0 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('PEoverlap'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
    
    figfile = [figdir,'Abs-DirnIdx_vs_PE_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
    % Plot Dirn Idx vs Arm Idx
    fname = '/data25/sjadhav/HPExpt/HP_ProcessedData/HP_PFC_ArmIdx_gather_noFS_X6.mat';
    load(fname,'all_AI_PFCidxs','all_AI_PFCArmIdx');
    
    % NOTE: AI and DI idxs are the same!
    exc_armidx = abs(all_AI_PFCArmIdx(excind));
    inh_armidx = abs(all_AI_PFCArmIdx(inhind));
    neu_armidx = abs(all_AI_PFCArmIdx(neuind));
    
    rem = find(isnan(exc_armidx) | isnan(exc_Dirnidx)); exc_armidx(rem)=[]; exc_Dirnidx(rem)=[];
    rem = find(isnan(inh_armidx) | isnan(inh_Dirnidx)); inh_armidx(rem)=[]; inh_Dirnidx(rem)=[];
    rem = find(isnan(neu_armidx) | isnan(neu_Dirnidx)); neu_armidx(rem)=[]; neu_Dirnidx(rem)=[];
    
    
    % Stats using shuffle-Exc
    [r2,p2] = corrcoef(exc_armidx,exc_Dirnidx); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(exc_armidx));
        rand_armidx = exc_armidx(rorder);
        [rtmp, ptmp] = corrcoef(rand_armidx,exc_Dirnidx);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pexc = length(find(abs(rshuf)>abs(r2)))./1000;
    %length(find(pshuf<0.05)); prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
    
    % Stats using shuffle-Inh
    [r2,p2] = corrcoef(inh_armidx,inh_Dirnidx); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(inh_armidx));
        rand_armidx = inh_armidx(rorder);
        [rtmp, ptmp] = corrcoef(rand_armidx,inh_Dirnidx);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pinh = length(find(abs(rshuf)>abs(r2)))./1000;
    
    % Stats using shuffle-Neu
    [r2,p2] = corrcoef(neu_armidx,neu_Dirnidx); r2=r2(1,2);
    rshuf=[]; pshuf=[];
    for i = 1:1000
        rorder = randperm(length(neu_armidx));
        rand_armidx = neu_armidx(rorder);
        [rtmp, ptmp] = corrcoef(rand_armidx,neu_Dirnidx);
        rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
    end
    pneu = length(find(abs(rshuf)>abs(r2)))./1000;
    
    exc_armidx = exc_armidx';
    inh_armidx = inh_armidx';
    neu_armidx = neu_armidx';
    exc_Dirnidx = exc_Dirnidx';
    inh_Dirnidx = inh_Dirnidx';
    neu_Dirnidx = neu_Dirnidx';
    
    figure; hold on;
    subplot(2,2,1);
    scatter(exc_armidx, exc_Dirnidx, 'r.'); hold on;
    [b,bint,r,rint,stats] = regress(exc_Dirnidx, [ones(size(exc_armidx)) exc_armidx]);
    plot(min(exc_armidx):.01:max(exc_armidx), b(1)+b(2)*[min(exc_armidx):.01:max(exc_armidx)],'r')
    axis([-0.05 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('ArmIdx'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pexc));
    
    subplot(2,2,2);
    scatter(inh_armidx, inh_Dirnidx, 'b.'); hold on;
    [b,bint,r,rint,stats] = regress(inh_Dirnidx, [ones(size(inh_armidx)) inh_armidx]);
    plot(min(inh_armidx):.01:max(inh_armidx), b(1)+b(2)*[min(inh_armidx):.01:max(inh_armidx)],'b')
    axis([-0.05 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('ArmIdx'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pinh));
    
    subplot(2,2,3);
    scatter(neu_armidx, neu_Dirnidx, 'k.'); hold on;
    [b,bint,r,rint,stats] = regress(neu_Dirnidx, [ones(size(neu_armidx)) neu_armidx]);
    plot(min(neu_armidx):.01:max(neu_armidx), b(1)+b(2)*[min(neu_armidx):.01:max(neu_armidx)],'k')
    axis([-0.05 1 -0.05 1]);
    ylabel('DirnIdx'); xlabel('ArmIdx'); title(sprintf('r^2(%0.3f) p(%0.3f)', stats(1), pneu));
    
    figfile = [figdir,'Abs-DirnIdx_vs_Abs-ArmIdx_PFC']
    if savefig1==1,
        print('-depsc2', figfile); print('-djpeg', figfile);  print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig');
    end
    
    
end % end plotPE_Dirnidx


