

clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells
plotGraphs=1;
%savedir = 'mnt/data25new/sjadhav/HPExpt/HP_ProcessedData/';

%savedir = '/opt/data15/gideon/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';

%savedir = '/data15/gideon/ProcessedData/';
val=1; savefile = [savedir 'HP_ripmod_glmfit_theta6allripmod']; area = 'PFC'; clr = 'b'; % PFC
%val=2; savefile = [savedir 'HP_ripmod_glmfit_theta6ripexc']; area = 'PFC'; clr = 'b'; % PFC
%val=3; savefile = [savedir 'HP_ripmod_glmfit_theta6ripinh']; area = 'PFC'; clr = 'b'; % PFC
%val=4; savefile = [savedir 'HP_ripmod_glmfit_theta6ripunmod']; area = 'PFC'; clr = 'b'; % PFC
%val=5; savefile = [savedir 'HP_ripmod_glmfit_theta6All']; area = 'PFC'; clr = 'b'; % PFC


savefig1=0;


% Plot options
plotanimidx =  []; % To pick animals for plotting
plotdays = []; % If you only load data when runscript=0 and savedata=0, then this field will supplant days


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Nadal','Rosenthal'};
    
    runepochfilter = 'isequal($type, ''run'')';
    
    
    switch val
        case 1
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($FStag, ''n'') && strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y''))';
        case 2
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($FStag, ''n'') && strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc''))';
        case 3
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($FStag, ''n'') && strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh''))';
        case 4
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($FStag, ''n'') && strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''n''))';
        case 5
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($FStag, ''n'') && strcmp($area, ''PFC'') && ($numspikes > 100))';
            
    end
    
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    timefilter_place_new = { {'DFTFsj_getvelpos', '(($absvel >= 5))'},...
        {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',3} };
    
    
    iterator = 'multicellanal';
    
    
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter,  'excludetime', timefilter_place_new, 'iterator', iterator);
    
    modg = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter, 'iterator', iterator);
    
    disp('Done Filter Creation');
    
    
    modg = setfilterfunction(modg,'DFAsj_glm_ripalign_dataForTheta',{'ripplemod','cellinfo'}); %
    modf = setfilterfunction(modf,'DFAsj_glm_thetaGR2',{'spikes','cellinfo'},'thrstime',1); % With includetime condition
    
    
    
    modf = runfilter(modf);
    modg = runfilter(modg);
    disp('Finished running filter script');
    
    if savedata == 1
        clear figopt1 runscript plotdays plotanimidx savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end


% -------------------------  Filter Format Done -------------------------

% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
switch val
    case 1
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6allripmod'];
    case 2
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6ripexc'];
    case 3
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6ripinh'];
    case 4
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6unripmod'];
    case 5
        gatherdatafile = [savedir 'HP_ripmod_glmfit_theta_gather6All'];
end



if gatherdata
    %----- Getting theta model parameters
    cnt=0;
    %Glm
    allglmidxstheta=[];    allglmidxstheta2=[];
    allmodelbtheta=[]; allmodelbtheta2=[];allmodelptheta=[]; allmodelfitstheta=[];
    allnsigtheta=[]; allnsigpostheta=[]; allnsignegtheta=[];  allfracsigtheta=[]; allfracsigpostheta=[]; allfracsignegtheta=[];
    XmatsTheta={};
    YmatsTheta={};
    XYmatsTheta={};
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            cnt=cnt+1;
            anim_index{an}{cnt} = modf(an).output{1}(i).indices;
            
            indNoAnim1=modf(an).output{1}(i).glmidxs;
            indWAnim1=[an*ones(size(indNoAnim1,1),1) indNoAnim1];
            allglmidxstheta = [allglmidxstheta; indWAnim1]; % Need to put "an" in there
            
            indNoAnim=modf(an).output{1}(i).glmidxs2;
            try
                indNoAnim=indNoAnim(find(nansum(indNoAnim')~=0),:);
            catch
                keyboard
            end
            indWAnim=[an*ones(size(indNoAnim,1),1) indNoAnim];
            allglmidxstheta2 = [allglmidxstheta2; indWAnim];
            XmatsTheta{cnt}=modf(an).output{1}(i).Xmat;
            YmatsTheta{cnt}=modf(an).output{1}(i).Ymat;
            for d=1:size(indWAnim,1)
                XYmatsTheta{end+1}=[XmatsTheta{cnt} YmatsTheta{cnt}(:,d)];
            end
            
        end
        
    end
    %removing rows and columns that are all nan
    allglmidxstheta2=allglmidxstheta2(find(nansum(allglmidxstheta2')~=0),1:find(nansum(allglmidxstheta2)==0,1));
    
    cnt=0;
    %---- Getting ripple model data
    allglmidxsrip2=[];
    Xmats={};
    Ymats={};
    XYmats={};
    
    for an = 1:length(modg)
        for i=1:length(modg(an).output{1})
            cnt=cnt+1;
            anim_index{an}{cnt} = modf(an).output{1}(i).indices;
            
            indNoAnim=modg(an).output{1}(i).glmidxs2;
            indNoAnim=indNoAnim(find(nansum(indNoAnim')~=0),:);
            indWAnim=[an*ones(size(indNoAnim,1),1) indNoAnim];
            % Indices in new form. Each line is one ensemble of cells in the
            % following form:
            % animal day,epoch,hctet,hccell,hctet,hccell...,hctet,hccell,pfctet,pfccell
            allglmidxsrip2 = [allglmidxsrip2; indWAnim]; % Need to put "an" in there
            Xmats{cnt}=modg(an).output{1}(i).Xmat;
            Ymats{cnt}=modg(an).output{1}(i).Ymat;
            for d=1:size(indWAnim,1)
                XYmats{end+1}=[Xmats{cnt} Ymats{cnt}(:,d)];
            end
            
        end
        
    end
    
    allglmidxsrip2=allglmidxsrip2(find(nansum(allglmidxsrip2')~=0),1:find(nansum(allglmidxsrip2)==0,1));
    
    % sanity check
    numPFCCells=0;for i=1:size(Ymats,2);numPFCCells=numPFCCells+size(Ymats{i},2);end
    if numPFCCells~=size(allglmidxsrip2)|numPFCCells~=size(allglmidxstheta2)
        'Numbers dont add up'
        keyboard
    end
    
    cnt2=1;
    XYmats2={};
    XYmats2Theta={};
    XYinds={};
    
    allPFCCA1sigidxs={};
    allglmidxstheta3=[];
    %making anim-day-pfctet-pfccell
    animdaypfc=[];
    for kk=1:size(allglmidxstheta2,1)
        animdaypfc(kk,1:4)=allglmidxstheta2(kk,[1,2,find(isnan(allglmidxstheta2(kk,:)),1)-2,find(isnan(allglmidxstheta2(kk,:)),1)-1]);
    end
    uniqueIndices=unique(animdaypfc,'rows');
    for kk=1:size(uniqueIndices,1)
        %finding all epochs for each pfc cell
        
        ind1=find(ismember(animdaypfc,uniqueIndices(kk,:),'rows'))';
        numepochs=length(ind1);
        
        allcells1=reshape(allglmidxstheta2(ind1(1),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
        allcells1=allcells1(~isnan(allcells1(:,1)),:);
        allca1cells1=allcells1(1:end-1,:);
        X1=XYmats{ind1(1)};
        X1Theta=XYmatsTheta{ind1(1)};
        XYconcat=X1;
        XYconcatTheta=X1Theta;
        
        if numepochs>1
            
            allcells2=reshape(allglmidxstheta2(ind1(2),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
            allcells2=allcells2(~isnan(allcells2(:,1)),:);
            X2=XYmats{ind1(2)};
            X2Theta=XYmatsTheta{ind1(2)};
            
            [C ia ib]=intersect(allcells1,allcells2,'rows');
            XYconcat=[X1(:,ia);X2(:,ib)];
            XYconcatTheta=[X1Theta(:,ia);X2Theta(:,ib)];
            
            allcells1=C;
        end
        if numepochs>2
            
            allcells3=reshape(allglmidxstheta2(ind1(3),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
            allcells3=allcells3(~isnan(allcells3(:,1)),:);
            X3=XYmats{ind1(3)};
            X3Theta=XYmatsTheta{ind1(3)};
            
            [C ia ib]=intersect(allcells1,allcells3,'rows');
            XYconcat=[XYconcat(:,ia);X3(:,ib)];
            XYconcatTheta=[XYconcatTheta(:,ia);X3Theta(:,ib)];
            allcells1=C;
            
            
        end
        if numepochs>3
            
            allcells4=reshape(allglmidxstheta2(ind1(4),4:end-1)',2,size(allglmidxstheta2(:,4:end-1),2)/2)';
            allcells4=allcells4(~isnan(allcells4(:,1)),:);
            X4=XYmats{ind1(4)};
            X4Theta=XYmatsTheta{ind1(4)};
            
            [C ia ib]=intersect(allcells1,allcells4,'rows');
            XYconcat=[XYconcat(:,ia);X4(:,ib)];
            XYconcatTheta=[XYconcatTheta(:,ia);X4(:,ib)];
            
            allcells1=C;
        end
        
        XYmats2{end+1}=XYconcat;
        XYmats2Theta{end+1}=XYconcatTheta;
        
        
        animday=allglmidxstheta2(ind1(1),1:2);
        animdayrep=repmat(animday,size(allcells1,1),1);
        allcellswanimday=[animdayrep allcells1];

        XYinds{end+1}=allcellswanimday;

    end
    
    
    allErrRealThetaTheta2=[];
    allErrShufThetaTheta2=[];
    allPsThetaTheta=[];
    allErrReal_theta=[];
    allErrShuf_theta=[];
    allErrRealThetaTheta=[];
    allErrShufThetaTheta=[];
    allErrReal=[];
    allErrShuf=[];
    allPs_theta=[];
    allPs=[];
    allPsK=[];
    nsig=[];
    fracsig=[];
    nsigT=[];
    fracsigT=[];
    corrPlast=[];
    counter=1;
    allRealPlast=[];
    allShufPlast=[];
    figdir = '/data15/gideon/Figs/';
    plotGraphs=1;
    converged1=[];
    converged1theta=[];
    allnumrips=[];
    allnumcells=[];
    for ii=1:size(XYmats2,2)
        ii
        currXY=XYmats2{ii};
        currX=currXY(:,1:end-1);
        currY=currXY(:,end);
        
        currXYTheta=XYmats2Theta{ii};
        currXTheta=currXYTheta(:,1:end-1);
        currYTheta=currXYTheta(:,end);
        
        nPFCcells=size(currY,2);
        nCA1cells=size(currX,2);
        %         if plotGraphs
        %             [rr pp]=corrcoef([currX currY]);
        %             regionsLabels=[repmat('CA1',nCA1cells,1);repmat('PFC',nPFCcells,1)];
        %             % plotting only PFC-CA1 edges (removing CA1-CA1 and PFC-PFC)
        %             for i=1:length(rr),for j=1:length(rr), if i<=nCA1cells&j<=nCA1cells,rr(i,j)=NaN;end,if i>nCA1cells&j>nCA1cells,rr(i,j)=NaN;end;end;end
        %             plotNiceCorrGraph(rr,pp,regionsLabels)
        %             animStr=num2str(allglmidxsrip2(counter,1));
        %
        %             dayStr=num2str(allglmidxsrip2(counter,2));
        %             epochStr=num2str(allglmidxsrip2(counter,3));
        %             title(['Rip- corrs, anim=' animStr ' day=' dayStr ' ep=' epochStr])
        %            % saveas(gcf,[figdir 'graph' animStr dayStr epochStr 'B'],'jpg')
        %             keyboard
        %             close all
        %         end
        
        lastwarn('');
        
        [btrall, ~, statsall] = glmfit(currX,currY,'poisson');
        if isempty(lastwarn)
            converged1=[converged1 1];
        else
            converged1=[converged1 0];
            
        end
        % continue only if converged
        if isempty(lastwarn)
            currsig = find(statsall.p(2:end) < 0.05);
            nsig = [nsig length(currsig)];
            fracsig = [fracsig length(currsig)/nCA1cells];
            
            
            % predicting PFC ripple firing from training on ripples
            
            numRips=size(currX,1);
            allnumrips=[allnumrips numRips];
            numCells=size(currX,2);
            allnumcells=[allnumcells numCells];
            
            allErrReal1=[];
            allErrShuf1=[];
            numTrain=1000;
            
            for iii=1:numTrain
                ripidxs=randperm(numRips);
                dataPercentForTrain=0.9;
                Ntrain=ripidxs(1:round(numRips*dataPercentForTrain));
                Ntest=ripidxs(round(numRips*dataPercentForTrain)+1:numRips);
                lastwarn('');
                [btr, ~, statstr] = glmfit(currX(Ntrain,:),currY(Ntrain),'poisson');
                % continue only if converged
                if isempty(lastwarn)
                    yfit = glmval(btr, currX(Ntest,:),'log',statstr,0.95);
                    Ntestshufd=Ntest(randperm(length(Ntest)));
                    errReal=nanmean(abs(yfit-currY(Ntest)));
                    errShuf=nanmean(abs(yfit-currY(Ntestshufd)));
                    
                    allErrReal1=[allErrReal1 errReal];
                    allErrShuf1=[allErrShuf1 errShuf];
                end
            end

            [r1,kp1]=ttest2(allErrReal1,allErrShuf1,0.05,'left');

            allErrReal=[allErrReal nanmean(allErrReal1)];
            allErrShuf=[allErrShuf nanmean(allErrShuf1)];
            
            allPsK=[allPsK kp1];
            
            [btrallT, ~, statsallT] = glmfit(currXTheta,currYTheta,'poisson');
            currsigT = find(statsallT.p(2:end) < 0.05);
            nsigT = [nsigT length(currsigT)];
            fracsigT = [fracsigT length(currsigT)/nCA1cells];
            
            % predicting PFC ripple firing from training on theta
            yfit_theta = glmval(btrallT, currX,'log');
            errReal_theta=nanmean(abs(yfit_theta-currY));
            allErrShufTmp=[];
            for pp=1:numTrain
                errShuf=nanmean(abs(yfit_theta-currY(randperm(length(currY)))));
                allErrShufTmp=[allErrShufTmp errShuf];
            end
            currP=nanmean(errReal_theta>allErrShufTmp);
            allPs_theta=[allPs_theta currP];
            allErrReal_theta=[allErrReal_theta errReal_theta];
            allErrShuf_theta=[allErrShuf_theta nanmean(allErrShufTmp)];
            counter=counter+1;
        end
    end
    
    
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data

%%
% ------------
% PLOTTING, ETC
% ------------
fractionSigPredictableK=mean(allPsK<0.05)
fractionSigPredictableTheta=mean(allPs_theta<0.05)

% These alternatives yield almost identical result:
% 1.
%errDecrease=(1-allErrReal./allErrShuf);
%errDecrease_theta=(1-allErrReal_theta./allErrShuf_theta);
% 2. 
errDecrease=(allErrShuf./allErrReal-1);
errDecrease_theta=(allErrShuf_theta./allErrReal_theta-1);

% sig rate SWRs
sigratebycells=[];

sigrate1to5=mean(allPsK(allnumcells<=5)<0.05); numsig1to5=sum(allPsK(allnumcells<=5)<0.05);numcells1to5=length(allPsK(allnumcells<=5)<0.05);
sigrate6to10=mean(allPsK(allnumcells>5&allnumcells<=10)<0.05);numsig6to10=sum(allPsK(allnumcells>5&allnumcells<=10)<0.05);numcells6to10=length(allPsK(allnumcells>5&allnumcells<=10)<0.05);
sigrate11to30=mean(allPsK(allnumcells>10&allnumcells<=30)<0.05);numsig11to30=sum(allPsK(allnumcells>10&allnumcells<=30)<0.05);numcells11to30=length(allPsK(allnumcells>10&allnumcells<=30)<0.05);

sigratebycells=[sigrate1to5 sigrate6to10 sigrate11to30];


% sig rate theta
sigratebycellstheta=[];

sigrate1to5T=mean(allPs_theta(allnumcells<=5)<0.05); numsig1to5T=sum(allPs_theta(allnumcells<=5)<0.05);numcells1to5T=length(allPs_theta(allnumcells<=5)<0.05);
sigrate6to10T=mean(allPs_theta(allnumcells>5&allnumcells<=10)<0.05);numsig6to10T=sum(allPs_theta(allnumcells>5&allnumcells<=10)<0.05);numcells6to10T=length(allPs_theta(allnumcells>5&allnumcells<=10)<0.05);
sigrate11to30T=mean(allPs_theta(allnumcells>10&allnumcells<=30)<0.05);numsig11to30T=sum(allPs_theta(allnumcells>10&allnumcells<=30)<0.05);numcells11to30T=length(allPs_theta(allnumcells>10&allnumcells<=30)<0.05);

sigratebycellstheta=[sigrate1to5T sigrate6to10T sigrate11to30T];


% both
figure;bar([sigratebycells;sigratebycellstheta]')
text(0.7,1.1,[num2str(numsig1to5) '/' num2str(numcells1to5)])
text(1.7,1.1,[num2str(numsig6to10) '/' num2str(numcells6to10)])
text(2.7,1.1,[num2str(numsig11to30) '/' num2str(numcells11to30)])

text(1.1,1.1,[num2str(numsig1to5T) '/' num2str(numcells1to5T)])
text(2.1,1.1,[num2str(numsig6to10T) '/' num2str(numcells6to10T)])
text(3.1,1.1,[num2str(numsig11to30T) '/' num2str(numcells11to30T)])
ylim([0 1.2])
set(gca,'XTickLabel',{'1-5','6-10','11-30'})

%--------- error rate
% err rate SWRs
sigratebycells=[];

errrate1to5=mean(errDecrease(allnumcells<=5)); errnumcells1to5=sum(allnumcells<=5);
errrate6to10=mean(errDecrease(allnumcells>5&allnumcells<=10));errnumcells6to10=sum(allnumcells>5&allnumcells<=10);
errrate11to30=mean(errDecrease(allnumcells>10&allnumcells<=30));errnumcells11to30=sum(allnumcells>10&allnumcells<=30);
errratebycells=[errrate1to5 errrate6to10 errrate11to30];

% SEM:
errrate1to5SEM=std(errDecrease(allnumcells<=5))/sqrt(sum(allnumcells<=5));
errrate6to10SEM=std(errDecrease(allnumcells>5&allnumcells<=10))/sqrt(sum(allnumcells>5&allnumcells<=10));
errrate11to30SEM=std(errDecrease(allnumcells>10&allnumcells<=30))/sqrt(sum(allnumcells>10&allnumcells<=30));
errratebycellsSEM=[errrate1to5SEM errrate6to10SEM errrate11to30SEM];



% err rate theta
errratebycellstheta=[];

errrate1to5=mean(errDecrease_theta(allnumcells<=5));
errrate6to10=mean(errDecrease_theta(allnumcells>5&allnumcells<=10));
errrate11to30=mean(errDecrease_theta(allnumcells>10&allnumcells<=30));
errratebycellstheta=[errrate1to5 errrate6to10 errrate11to30];

% SEM:
errrate1to5SEM=std(errDecrease_theta(allnumcells<=5))/sqrt(sum(allnumcells<=5));
errrate6to10SEM=std(errDecrease_theta(allnumcells>5&allnumcells<=10))/sqrt(sum(allnumcells>5&allnumcells<=10));
errrate11to30SEM=std(errDecrease_theta(allnumcells>10&allnumcells<=30))/sqrt(sum(allnumcells>10&allnumcells<=30));
errratebycellsthetaSEM=[errrate1to5SEM errrate6to10SEM errrate11to30SEM];

% both
figure;
bar(1:2:6,errratebycells,0.25,'linewidth',2);
hold on;
errorbar(1:2:6,errratebycells,errratebycellsSEM,'.k','linewidth',2)
bar(1.6:2:6,errratebycellstheta,0.25,'y','linewidth',2);
errorbar(1.6:2:6,errratebycellstheta,errratebycellsthetaSEM,'.k','linewidth',2)


text(0.7,.085,[num2str(errnumcells1to5)])
text(1.7,.085,[num2str(errnumcells6to10)])
text(2.7,.085,[num2str(errnumcells11to30)])

text(1.1,.085,[num2str(errnumcells1to5)])
text(2.1,.085,[num2str(errnumcells6to10)])
text(3.1,.085,[num2str(errnumcells11to30)])
ylim([0 0.09])
set(gca,'XTickLabel',{'1-5','6-10','11-30'})


%% Statistics between groups
%savedir = '/opt/data15/gideon/HP_ProcessedData/';
savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';

gatherdatafileAllripmod = [savedir 'HP_ripmod_glmfit_theta_gather6allripmod'];
gatherdatafileExc = [savedir 'HP_ripmod_glmfit_theta_gather6ripexc'];
gatherdatafileInh = [savedir 'HP_ripmod_glmfit_theta_gather6ripinh'];
gatherdatafileUnmod = [savedir 'HP_ripmod_glmfit_theta_gather6unripmod'];


load(gatherdatafileAllripmod);
errDecreaseAllripmod=(allErrShuf./allErrReal-1);
errDecrease_thetaAllripmod=(allErrShuf_theta./allErrReal_theta-1);
numcellgroupAllripmod=zeros(1,length(allnumcells));
numcellgroupAllripmod(allnumcells<=5)=1;
numcellgroupAllripmod(allnumcells>5&allnumcells<=10)=2;
numcellgroupAllripmod(allnumcells>10&allnumcells<=30)=3;



load(gatherdatafileExc);
errDecreaseExc=(allErrShuf./allErrReal-1);
errDecrease_thetaExc=(allErrShuf_theta./allErrReal_theta-1);
numcellgroupExc=zeros(1,length(allnumcells));
numcellgroupExc(allnumcells<=5)=1;
numcellgroupExc(allnumcells>5&allnumcells<=10)=2;
numcellgroupExc(allnumcells>10&allnumcells<=30)=3;

load(gatherdatafileInh);
errDecreaseInh=(allErrShuf./allErrReal-1);
errDecrease_thetaInh=(allErrShuf_theta./allErrReal_theta-1);
numcellgroupInh=zeros(1,length(allnumcells));
numcellgroupInh(allnumcells<=5)=1;
numcellgroupInh(allnumcells>5&allnumcells<=10)=2;
numcellgroupInh(allnumcells>10&allnumcells<=30)=3;

load(gatherdatafileUnmod);
errDecreaseUnmod=(allErrShuf./allErrReal-1);
errDecrease_thetaUnmod=(allErrShuf_theta./allErrReal_theta-1);
numcellgroupUnmod=zeros(1,length(allnumcells));
numcellgroupUnmod(allnumcells<=5)=1;
numcellgroupUnmod(allnumcells>5&allnumcells<=10)=2;
numcellgroupUnmod(allnumcells>10&allnumcells<=30)=3;

%% testing effect of cell number
alldata=[errDecreaseAllripmod errDecreaseUnmod];
%modunmod=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecreaseUnmod))];
numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
p=anovan(alldata,{numcellgroup})

%% comparing SWR-modulated and SWR-unmodulated
alldata=[errDecreaseAllripmod errDecreaseUnmod];
modunmod=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecreaseUnmod))];
numcellgroup=[numcellgroupAllripmod  numcellgroupUnmod];
p=anovan(alldata,{modunmod,numcellgroup})

%% comparing SWR-exc and SWR-inh
alldata=[errDecreaseExc errDecreaseInh];
excinh=[ones(1,length(errDecreaseExc)) 2*ones(1,length(errDecreaseInh))];
numcellgroup=[numcellgroupExc numcellgroupInh ];
p=anovan(alldata,{excinh,numcellgroup})

%% comparing theta trained/SWR trained for swr-modulated cells
alldata=[errDecreaseAllripmod errDecrease_thetaAllripmod errDecreaseUnmod errDecrease_thetaUnmod];
modunmod=[ones(1,length(errDecreaseAllripmod)+length(errDecrease_thetaAllripmod)) 2*ones(1,length(errDecreaseUnmod)+length(errDecrease_thetaUnmod))];
riptheta=[ones(1,length(errDecreaseAllripmod)) 2*ones(1,length(errDecrease_thetaAllripmod)) ones(1,length(errDecreaseUnmod)) 2*ones(1,length(errDecrease_thetaUnmod)) ];
numcellgroup=[numcellgroupAllripmod numcellgroupAllripmod numcellgroupUnmod numcellgroupUnmod];
p=anovan(alldata,{modunmod,riptheta,numcellgroup})

%% comparing theta trained/SWR trained for swr-excited and swr-inhibited cells
alldata=[errDecreaseExc errDecrease_thetaExc errDecreaseInh errDecrease_thetaInh];
excinh=[ones(1,length(errDecreaseExc)+length(errDecrease_thetaExc)) 2*ones(1,length(errDecreaseInh)+length(errDecrease_thetaInh))];
riptheta=[ones(1,length(errDecreaseExc)) 2*ones(1,length(errDecrease_thetaExc)) ones(1,length(errDecreaseInh)) 2*ones(1,length(errDecrease_thetaInh)) ];
numcellgroup=[numcellgroupExc numcellgroupExc numcellgroupInh numcellgroupInh];
p=anovan(alldata,{excinh,riptheta,numcellgroup})

[p tbl stats] = anovan(alldata,{excinh,riptheta,numcellgroup},'model','interaction','varnames',{'excinh','riptheta','numcellgroup'})
results = multcompare(stats,'Dimension',[1])

%%
alldata=[errDecreaseExc errDecrease_thetaExc errDecreaseInh errDecrease_thetaInh errDecreaseUnmod errDecrease_thetaUnmod];
excinhunmod=[ones(1,length(errDecreaseExc)+length(errDecrease_thetaExc)) 2*ones(1,length(errDecreaseInh)+length(errDecrease_thetaInh)) 3*ones(1,length(errDecreaseUnmod)+length(errDecrease_thetaUnmod))];
riptheta=[ones(1,length(errDecreaseExc)) 2*ones(1,length(errDecrease_thetaExc)) ones(1,length(errDecreaseInh)) 2*ones(1,length(errDecrease_thetaInh)) ones(1,length(errDecreaseUnmod)) 2*ones(1,length(errDecrease_thetaUnmod))];
numcellgroup=[numcellgroupExc numcellgroupExc numcellgroupInh numcellgroupInh numcellgroupUnmod numcellgroupUnmod];
p=anovan(alldata,{excinhunmod,riptheta,numcellgroup})
[p tbl stats] = anovan(alldata,{excinhunmod,riptheta,numcellgroup},'model','interaction','varnames',{'excinhunmod','riptheta','numcellgroup'})
results = multcompare(stats,'Dimension',[1])


        