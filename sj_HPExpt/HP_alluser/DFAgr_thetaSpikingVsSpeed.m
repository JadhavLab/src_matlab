function out = DFAgr_thetaSpikingVsSpeed(indices, excludetimes, spikes, cellinfo,pos, varargin)

% For speed vs firng rate


tetfilter = '';
% gideon
%excludetimes = [];
thrstime = 1; % Minimum length of includetime segments in sec
binsize = 0.2; % 200ms bins?

%acrossregions = 0;

% lowsp_thrs = 5; %cm/sec
% highsp_thrs = lowsp_thrs;
% dospeed = 0;


for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'thrstime'
            thrstime = varargin{option+1};  
        case 'binsize'
            binsize = varargin{option+1};
        case 'acrossregions'
            acrossregions = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

day = index(1); epoch = index(2);



% % THIS IS NOT VALID FOR SINGLECELL ANAL First, get cell indices for CA1 and PFC
% % ----------------------------------------
% day = indices(1,1);
% epoch = indices(1,2);
% cellsi=[]; usecellsi=0; % CA1 cells
% cellsp=[]; usecellsp=0; % PFC cells
% totalcells = size(indices,1);
% for i=1:totalcells
%     currarea = cellinfo{indices(i,1)}{indices(i,2)}{indices(i,3)}{indices(i,4)}.area;
%     if strcmp(currarea,'PFC'),
%         cellsp = [cellsp; day epoch indices(i,3) indices(i,4)];
%         usecellsp = usecellsp+1;
% %     else
% %         cellsi = [cellsi; day epoch indices(i,3) indices(i,4)];
% %         usecellsi = usecellsi+1;
%     end
% end
% % nCA1cells = size(cellsi,1); 
% nPFCcells = size(cellsp,1);
    

% Get epochtimes, and implement the timecondition
% ------------------------------------------------
%ind = cellsp(1,:); % day.ep.tet.cell for 1st CA1 cell 


ind = index;
ttemp = spikes{ind(1)}{ind(2)}{ind(3)}{ind(4)}.data(:,1);

totaleptime = diff(spikes{ind(1)}{ind(2)}{ind(3)}{ind(4)}.timerange)./10000; % in secs
excltime = sum(diff(excludetimes'));
% Use Include periods
% --------------------------
% Get IncludeTimes from flanking edges in excludetimes and epoch start-end
epstend = spikes{ind(1)}{ind(2)}{ind(3)}{ind(4)}.timerange./10000;
incl=[];
incl(:,1) = excludetimes(1:end-1,2);
incl(:,2) = excludetimes(2:end,1);
incl = [epstend(1),incl(1,1) ;incl];
incl = [incl; incl(end,2),epstend(2)];

% Length of include periods
incl_lths = diff(incl')';
% Discard anything < thrstime
discard = find(incl_lths<thrstime);
incl(discard,:)=[];



for i=1:size(cellsp,1)
    i;
    eval(['spikesp{',num2str(i),'}= spikes{cellsp(',num2str(i),',1)}{cellsp(',num2str(i),',2)}'...
        '{cellsp(',num2str(i),',3)}{cellsp(',num2str(i),',4)}.data(:,1);']);
 % gideon added, verify with shantanu
    eval(['spikespinc{',num2str(i),'}= spikesp{',num2str(i),'}(find(isIncluded(spikesp{',num2str(i),'},incl)));'])

end

% Going over all incl intervals, and from each interval extracting bin
% times of 200ms going from the interval start, ending >=200ms from its end
% thetaBins will hold the start times of all 200ms bins
thetaBins=[];
thetaBinSize=0.2;
for ii=1:length(incl),
    curIntervalSize=incl(ii,2)-incl(ii,1);
    numCurBins=floor(curIntervalSize/thetaBinSize);
    curBins=incl(ii,1):0.2:(incl(ii,1)+0.2*(numCurBins-1));
    thetaBins=[thetaBins curBins];
end




spikeBinsPFC=[];


% associate speeds to theta bins
sptimes=pos{ind(1)}{ind(2)}.data(:,1);
sp=pos{ind(1)}{ind(2)}.data(:,5);
meanSpeedBins=[];
speedsToBins=lookup(sptimes,thetaBins,-1);
for jj=1:length(thetaBins)
meanSpeedBins=[meanSpeedBins nanmean(sp(speedsToBins==jj))];
end
meanSpeedBins=meanSpeedBins';
for k=1:nPFCcells
    
spikesToBins=lookup(spikespinc{k},thetaBins,-1);
tabulateSpikes=tabulate(spikesToBins);
spikesInBins=NaN(1,length(thetaBins));
if ~isempty(tabulateSpikes)
spikesInBins(1:length(tabulateSpikes))=tabulateSpikes(:,2);
end
spikeBinsPFC=[spikeBinsPFC spikesInBins'];
end

meanSpeedBinsNoNAN=meanSpeedBins(~isnan(spikeBinsPFC)&~isnan(meanSpeedBins));
spikeBinsPFCNoNAN=spikeBinsPFC(~isnan(spikeBinsPFC)&~isnan(meanSpeedBins));

[r, p]=corrcoef(meanSpeedBinsNoNAN,spikeBinsPFCNoNAN);

% % ------ 
% % Output
% % ------
out.indices = indices;

out.speeds=meanSpeedBinsNoNAN;
out.spiking=spikeBinsPFCNoNAN;
out.FR_speed_corr_r=r(1,2);
out.FR_speed_corr_p=p(1,2);
if isnan(r(1,2))
    keyboard
end


