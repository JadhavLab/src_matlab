
% In ver6, divide PFC cells in to exc and inh and plot properties: fir rate
% vs width

% Based on DFSsj_getcellinfo2, which was in DFScripts. Put this in
% sj_HPexpt/ now

% In version 2, get rid of csi and propbursts, since Ndl doesn't seem to have these.
% Also, you want to make an addition, where you go back to the params and matclust file, and load other parameters
% such as amplitude, ahp size, etc
% Just using the cellinfo structure to get and plot cell properties


clear;
runscript = 0;
savedata = 0; % save data option - only works if runscript is also on
figopt1 = 1; % Figure Options

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
% Re-do CA1 for X8
%val=1; savefile = [savedir 'HP_cellinfo_CA1_X8']; %
val=2; savefile = [savedir 'HP_cellinfo_PFC_X6']; %
%val=3; savefile = [savedir 'HP_cellinfo_iCA1_X6']; %

figdir = '/data25/sjadhav/HPExpt/Figures/PFCprop/';
savefig=0;

% If runscript, run Datafilter and save data
if runscript == 1
    
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Ndl','Rtl','Brg'};
    if val==3
        animals = {'HPa','HPb'};
    end
    
    %Filter creation
    %--------------------------------------------------------
    
    % Epoch filter
    % -------------
    %dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    %epochfilter = 'isequal($type, ''run'')'; % All run environments
    epochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''ytr'')'; % Start with just W-tracks run
    
    % Cell filter
    % -----------
    
    % Any cell defined in at least one run in environment
    
    switch val
        case 2
            cells = '(strcmp($area, ''PFC'')) &&  ($numspikes > 100)';
        case 1
            cells = '( strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) &&  ($numspikes > 100)';
        case 3
            cells = '(strcmp($area, ''iCA1'')) &&  ($numspikes > 100)';
    end
    
    
    % Time filter
    % -----------
    % None
    
    
    % Iterator
    % --------
    iterator = 'singlecellanal';
    
    % Filter creation
    % ----------------
    cellsf = createfilter('animal',animals,'epochs',epochfilter,'cells',cells,'iterator', iterator);
    
    % Set analysis function
    % ----------------------
    cellsf = setfilterfunction(cellsf, 'DFAsj_HPexpt_getcellinfo6', {'cellinfo'}); % You can also use "cellprop" which uses spikes as the variable and calls getcellprop
    
    
    % Run analysis
    % ------------
    cellsf = runfilter(cellsf);  %
    
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end  % end runscript

if ~exist('savedata')
    return
end


% --------------------------
% Post-filter run analysis
% --------------------------

% ---------------------------------------------------------
% Gather data and append animal index to it as well
% Unlike codes where you want to keep data for epochs separate for plotting (eg. place field code), here, combine everything for population analysis.
% ---------------------------------------------------------

alldata = []; alltags = []; allanimindex = []; % includes animal index
cnt = 0; % For cnting across animals for tags
for an = 1:length(animals)
    for i=1:length(cellsf(an).output{1}),
        anim_index{an}(i,:)=cellsf(an).output{1}(i).index;
        % Only indexes
        animindex=[an cellsf(an).output{1}(i).index]; % Put animal index in front
        allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
        
        % Indexes + Data [anim day epoch tet cell rate numspikes spikewidth
        append = [an cellsf(an).output{1}(i).index cellsf(an).output{1}(i).meanrate ...
            cellsf(an).output{1}(i).numspikes cellsf(an).output{1}(i).spikewidth cellsf(an).output{1}(i).ripmod cellsf(an).output{1}(i).type];
        alldata = [alldata; append];
        
        % Make a tag field with the same length
        %cnt=cnt+1;
        %allrawtags(cnt).tag = cellsf(an).output{1}(i).tag;
        
    end
end

% -----------------------------------------------------------------
% Consolidate single cells' across epochs in a day .celloutput field
% Can do for each animal and then store separately by using the index from output field - Do in loop for each animal,
% or can use the above appended animal+index field to parse data
% -----------------------------------------------------------------

% Save the consolidated data back in a structure - TO put back in filter
% structure, has to be animal-wise. Sell alternate method below
celloutput = struct;
dummyindex=allanimindex;  % all anim-day-epoch indices


cntcells=0;
for i=1:size(alldata,1)
    animdaytetcell=alldata(i,[1 2 4 5]);
    ind=[];
    while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
        ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
        dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
        % so you could find the next one
    end
    
    % Gather everything for the current cell across epochs
    currrate=[]; currcsi=[]; currpropbursts=[]; currnumspikes=[]; currspikewidth=[]; currtag = [];
    for r=ind
        currrate=[currrate ; alldata(r,6)];
        currnumspikes=[currnumspikes;alldata(r,7)];
        currspikewidth=[currspikewidth;alldata(r,8)];
    end
    
    currripmod = alldata(r,9);
    currtype = alldata(r,10); % get only once
    
    if ~isempty(currrate)
        %currtag = allrawtags(ind(1)).tag; % Tag is same for all epochs
        cntcells = cntcells + 1;
        celloutput(cntcells).index=animdaytetcell;
        celloutput(cntcells).type = currtype;
        celloutput(cntcells).ripmod = currripmod;
        celloutput(cntcells).meanrate=currrate; % Remember, this is seperated across epochs. Can take mean
        celloutput(cntcells).numspikes=currnumspikes;
        celloutput(cntcells).spikewidth=currspikewidth;
    end
end

% Alternate method - Keep Animal data separate. For each animal, look within dayeptetcell
% ----------------------------------------------------------------------------------------
% for an = 1:length(animals)
%     dummyindex = anim_index{an};     % collect all day-epoch-tet-cell indices
%     for i=1:length(cellsf(an).output{1})
%         daytetcell=cellsf.output{1}(i).index([1 3 4]);
%         ind=[];
%         while rowfind(daytetcell,dummyindex(:,[1 3 4]))~=0          % collect all rows (epochs)
%             ind = [ind rowfind(daytetcell,dummyindex(:,[1 3 4]))];
%             dummyindex(rowfind(daytetcell,dummyindex(:,[1 3 4])),:)=[0 0 0 0];
%         end
%         % Gather everything for the current cell across epochs
%         currrate=[]; currcsi=[]; currpropbursts=[]; currnumspikes=[]; currspikewidth=[]; currtag = [];
%         for r=ind
%             currrate=[currrate ; cellsf.output{1}(r).meanrate];
%             currcsi=[currcsi; cellsf.output{1}(r).csi];
%             currpropbursts=[currpropbursts; cellsf.output{1}(r).propbursts];
%             currnumspikes=[currnumspikes;cellsf.output{1}(r).numspikes];
%             currspikewidth=[currspikewidth;cellsf.output{1}(r).spikewidth];
%         end
%         if ~isempty(currrate)
%             currtag = cellsf.output{1}(ind(1)).tag; % Tag is same for all epochs
%             cellsf(an).celloutput.index=daytetcell;
%             cellsf(an).celloutput.meanrate=currrate;
%             cellsf(an).celloutput.csi=csi;
%             cellsf(an).celloutput.propbursts=propbursts;
%             cellsf(an).celloutput.numspikes=numspikes;
%             cellsf(an).celloutput.spikewidth=spikewidth;
%             cellsf(an).celloutput.tag=ncurrtag;
%         end
%     end
% end


% Values for cells - take means across epochs
allmeanrate = []; allcsi = []; allpropbursts = []; allnumspikes = []; allspikewidth = [];
allFS=[];
alltype = []; allripmod = [];
for i=1:length(celloutput)
    allcellidx(i,:) = celloutput(i).index;
    allmeanrate = [allmeanrate; mean(celloutput(i).meanrate)];
    allspikewidth = [allspikewidth, mean(celloutput(i).spikewidth)];
    alltype = [alltype; celloutput(i).type];
    allrpmod = [allripmod; celloutput(i).ripmod];   
    
    % For PFC
    if val==2
        if (mean(celloutput(i).meanrate)>17) | (mean(celloutput(i).spikewidth)<6)
            allFS = [allFS; 1];
        else
            allFS = [allFS; 0];
        end
    else % CA1
        if (mean(celloutput(i).meanrate)>7)
            allFS = [allFS; 1];
        else
            allFS = [allFS; 0];
        end
    end
    
end




% ---------
% Plotting
% ---------

length(allmeanrate)
length(find(allmeanrate<1))
length(find(allmeanrate<0.1))


exc = find(alltype==1);
inh = find(alltype==-1);
neu = find(alltype==0);

%1) All Cells - Fir rates vs spikewidths
figure; hold on;
redimscreen_figforppt1;

%scatter(allspikewidth, allmeanrate, [],'r');
plot(allspikewidth, allmeanrate,'ko','MarkerSize',8,'LineWidth',1.5);
%plot(allspikewidth(Intidx), allmeanrate(Intidx),'ro','MarkerSize',8,'LineWidth',1.5);

% Exc, Inh, Neu
plot(allspikewidth(exc), allmeanrate(exc),'rx','MarkerSize',8,'LineWidth',1.5);
plot(allspikewidth(inh), allmeanrate(inh),'bo','MarkerSize',8,'LineWidth',1.5);
plot(allspikewidth(neu), allmeanrate(neu),'go','MarkerSize',8,'LineWidth',1.5);

xaxis = min(allspikewidth):0.1:max(allspikewidth);
plot(xaxis,0.1*ones(size(xaxis)),'k-','Linewidth',1);
plot(xaxis,10*ones(size(xaxis)),'k-','Linewidth',1);

set(gca,'YLim',[0 1.05*max(allmeanrate)]);
set(gca,'XLim',[0.95*min(xaxis) 1.05*max(xaxis)]);

xlabel('Spike Widths','FontSize',24,'Fontweight','normal');
ylabel('Firing Rates (Hz)','FontSize',24,'Fontweight','normal');
title('Cell Properties','FontSize',24,'Fontweight','normal')


% Now shed FS cells like in DFSsj_getcellinfo6'
exc = find(alltype==1 & allFS==0);
inh = find(alltype==-1 & allFS==0);
neu = find(alltype==0 & allFS==0);



if val == 2
    % Mean Fir Rates
    excmeanfr = mean(allmeanrate(exc)); excerrfr = sem(allmeanrate(exc));
    inhmeanfr = mean(allmeanrate(inh)); inherrfr = sem(allmeanrate(inh));
    neumeanfr = mean(allmeanrate(neu)); neuerrfr = sem(allmeanrate(neu));
    %bar([excmeanfr;inhmeanfr;neumeanfr]);
    [r_ei, p_ei] = ttest2(allmeanrate(exc),allmeanrate(inh)), % ks:0.34  ttest p=0.11 FS p=0.9778
    [r_in, p_in] = ttest2(allmeanrate(inh),allmeanrate(neu)), % ks:0.01  tt p=0.01 FS p = 0.0014
    [r_en, p_en] = ttest2(allmeanrate(exc),allmeanrate(neu)), % ks: 10-4 tt p=2.16e-6   FS p = 0.000038
    
    [rk_ei, pk_ei] = kstest2(allmeanrate(exc),allmeanrate(inh)), %
    [rk_in, pk_in] = kstest2(allmeanrate(inh),allmeanrate(neu)), % 
    [rk_en, pk_en] = kstest2(allmeanrate(exc),allmeanrate(neu)),
    
    [pei] = ranksum(allmeanrate(exc),allmeanrate(inh)), %
    [pin] = ranksum(allmeanrate(inh),allmeanrate(neu)), % 
    [pen] = ranksum(allmeanrate(exc),allmeanrate(neu)),
    
    figure; hold on;
%     errorbar(1,excmeanfr,excerrfr,'ro-','MarkerSize',16,'LineWidth',3)
%     errorbar(2,inhmeanfr,inherrfr,'bo-','MarkerSize',16,'LineWidth',3)
%     errorbar(3,neumeanfr,neuerrfr,'go-','MarkerSize',16,'LineWidth',3)
    bar(1, excmeanfr,'r');  
    bar(2, inhmeanfr,'b');  
    bar(3, neumeanfr,'g');  
    legend('PFC excited','PFC inhibited','PFC unmodulated'); 
    errorbar2(1, excmeanfr, excerrfr, 0.3, 'r');
    errorbar2(2, inhmeanfr, inherrfr, 0.3, 'b');
    errorbar2(3, neumeanfr, neuerrfr, 0.3, 'g');
    set(gca, 'YLim', [0 5.5]);
    title('Mean Firing Rates','FontSize',16,'FontWeight','Normal');
    set(gca,'XTickLabel','');
    figfile = [figdir,'ExcInhNeu_MeanFr_Bar']
    if savefig==1,
        print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
    end
    
    
    
    % Fir rates with quartiles
    excmedianfr = median(allmeanrate(exc)); inhmedianfr = median(allmeanrate(inh)); neumedianfr = median(allmeanrate(neu));
    figure; hold on;
    errorbar(1,excmedianfr,excmedianfr-prctile(allmeanrate(exc),25),prctile(allmeanrate(exc),75)-excmedianfr,'ro-','MarkerSize',16,'LineWidth',3)
    errorbar(2,inhmedianfr,inhmedianfr-prctile(allmeanrate(inh),25),prctile(allmeanrate(inh),75)-inhmedianfr,'bo-','MarkerSize',16,'LineWidth',3)
    errorbar(3,neumedianfr,neumedianfr-prctile(allmeanrate(neu),25),prctile(allmeanrate(neu),75)-neumedianfr,'go-','MarkerSize',16,'LineWidth',3)
    set(gca, 'YLim', [0 8.5]);
    title('Median Firing Rates with quartiles','FontSize',24,'FontWeight','Normal');
    legend;
    set(gca,'XTickLabel','');
    
    
    % SpikeWidths
    excmeansw = mean(allspikewidth(exc)); excerrsw = sem(allspikewidth(exc));
    inhmeansw = mean(allspikewidth(inh)); inherrsw = sem(allspikewidth(inh));
    neumeansw = mean(allspikewidth(neu)); neuerrsw = sem(allspikewidth(neu));
    %bar([excmeansw;inhmeansw;neumeansw]);
    [r_ei, p_ei] = ttest2(allspikewidth(exc),allspikewidth(inh)) % p = 0.431
    [r_in, p_in] = ttest2(allspikewidth(inh),allspikewidth(neu)) % p = 0.241
    [r_en, p_en] = ttest2(allspikewidth(exc),allspikewidth(neu)) % p = 0.851
    
    
    
    figure; hold on;
    errorbar(1,excmeansw,excerrsw,'ro-','MarkerSize',16,'LineWidth',3)
    errorbar(2,inhmeansw,inherrsw,'bo-','MarkerSize',16,'LineWidth',3)
    errorbar(3,neumeansw,neuerrsw,'go-','MarkerSize',16,'LineWidth',3)
    set(gca, 'YLim', [10 14]);
    title('SpikeWidths','FontSize',24,'FontWeight','Normal');
    legend;
    set(gca,'XTickLabel','');    
    
     % Spikewidths with quartiles
    excmediansw = median(allspikewidth(exc)); inhmediansw = median(allspikewidth(inh)); neumediansw = median(allspikewidth(neu));
    figure; hold on;
    errorbar(1,excmediansw,excmediansw-prctile(allspikewidth(exc),25),prctile(allspikewidth(exc),75)-excmediansw,'ro-','MarkerSize',16,'LineWidth',3)
    errorbar(2,inhmediansw,inhmediansw-prctile(allspikewidth(inh),25),prctile(allspikewidth(inh),75)-inhmediansw,'bo-','MarkerSize',16,'LineWidth',3)
    errorbar(3,neumediansw,neumediansw-prctile(allspikewidth(neu),25),prctile(allspikewidth(neu),75)-neumediansw,'go-','MarkerSize',16,'LineWidth',3)
    set(gca, 'YLim', [10 18]);
    title('Median SpikeWidths with Quartiles','FontSize',24,'FontWeight','Normal');
    legend;
    set(gca,'XTickLabel','');
    
    
end



figure; hold on;
redimscreen_figforppt1;
clr = ['r','b','c','m','g','y'];

for an=1:6
    curridxs = find(allcellidx(:,1)==an);
    plot(allspikewidth(curridxs), allmeanrate(curridxs),[clr(an) 'o'],'MarkerSize',8,'LineWidth',1.5);
    i=1;
end
    
    



%length(find(allcellidx(:,1)==1 & alltype==1 ))
%length(find(allcellidx(:,1)==2 & alltype==1 )),  length(find(allcellidx(:,1)==2 & alltype==-1 ))

% *WITH FS*
% PFC, 354 cells/ 115 Rip-Modulated (32%)/ 60 Exc (16.95%)/ 55 Inh(15.5%)/ 239 Neu (67.51%)
% CA1 659 cells 
% iCA1 66 cells (51+15)

% Anim NcellsPFC Exc Inh Neu      CA1
% HPa  42         13  12 17       185 (51 iCA1)
% HPb  80         13  9  58       159 (15 iCA1)
% HPc  56         6   6  44       93
% ---  178        32  27 119   -- 434
% 
% Ndl  77         18  18 41       48
% Rtl  47         3   8  36       172
% Brg  52         7   2  43       2
% ---  176        28  28 120   -- 222   
% ----------------------------------
%      354        60  55        656


%12 PFC are FS
% exc=60-3 = 57; inh = 55-3 = 52; Neu = 239-6 = 233;
% Total = 354-12 = 342;







