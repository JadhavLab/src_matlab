function out = sj_getpopulationevents2(indices, excludeperiods, spikes, linpos, pos, ripples, tetinfo, varargin)
% Exactly the same as sj_getpopulationevents2_forsave?
% SJ - Combine getpopulationevents2 and mcarr/getpopulationevents. Minor differences
%Calculates the binned spike counts of all neurons in the index list.
% Get riptimes separated by atleast 1 sec

%
%spikes - the 'spikes' cell array for the day you are analyzing
%linpos - the output of LINEARDAYPROCESS for the day you are analyzing.
%indices - [day epoch tetrode cell]
%timebin- the length of each temporal bin (default 0.01 sec)
%excludeperiods - [start end] times for each exlcude period
%
%In the output, the spikecounts field is n by t, where n is the number of
%cells and t is the number of timebins.

tetfilter = '';
excludeperiods = [];
maxcell = 0;
minstd = 3;
lowsp_thrs = 4; %cm/sec
highsp_thrs = lowsp_thrs;
dospeed = 0;
cellcountthresh = [];
minrip=1;

for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludeperiods'
            excludeperiods = varargin{option+1};
        case 'minstd'
            minstd = varargin{option+1};
        case 'minrip'
            minrip = varargin{option+1};
        case 'maxcell'
            maxcell = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        case 'lowsp_thrs'
            lowsp_thrs = varargin{option+1};
        case 'cellcountthresh'
            cellcountthresh = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
    
end

index = [indices(1,1) indices(1,2)];
day = index(1);
epoch = index(2);

posdata = pos{index(1)}{index(2)}.data;
statematrix = linpos{index(1)}{index(2)}.statematrix;

% Get riptimes
% -------------
if isempty(tetfilter)
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', '(isequal($descrip, ''riptet''))','minstd',minstd,'minrip',minrip);
else
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', tetfilter, 'minstd', minstd,'minrip',minrip);
end

% Get triggers as rip starttimes separated by at least 1 sec
% ----------------------------------------------------------
rip_starttime = 1000*riptimes(:,1);  % in ms

% Find ripples separated by atleast a second
% --------------------------------------------
iri = diff(rip_starttime);
keepidx = [1;find(iri>=1000)+1];
rip_starttime = rip_starttime(keepidx);
riptimes = riptimes(keepidx,:);

% Implement speed criterion - Keep.
% ----------------------------------------
if dospeed
    absvel = abs(pos{day}{epoch}.data(:,5)); % Can also use field 9
    postime = pos{day}{epoch}.data(:,1); % in secs
    pidx = lookup(rip_starttime,postime*1000);
    speed_atrip = absvel(pidx);
    lowsp_idx = find(speed_atrip <= lowsp_thrs);
    highsp_idx = find(speed_atrip > highsp_thrs);
    
    rip_starttime = rip_starttime(lowsp_idx);
    riptimes = riptimes(lowsp_idx,:);
end


% riptimes = getripples(index, ripples, cellinfo, 'cellfilter', '(isequal($area, ''CA1'')|isequal($area, ''CA3''))','excludeperiods', excludeperiods,'minstd',3);
% %riptimes(:,1) = riptimes(:,2)-(window/2);
% %riptimes(:,3) = riptimes(:,2)+(window/2);


out.index = [];
out.eventtraj = [];
out.eventdist = [];
out.eventtime = [];
out.preeventcount = [];
out.eventimmobiletime = [];
out.eventdata = [];
out.peak = 0;
%out.speed = [];
out.noneventtime = []; % Non-event times
out.riptimes = riptimes; % All riptimes

spikecounts = [];
celldata = [];

if ~isempty(riptimes)
    %go through each cell and calculate the binned spike counts
    for cellcount = 1:size(indices,1)
        
        index = indices(cellcount,:);
        if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)}.data)
            spiketimes = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);
        else
            spiketimes = [];
        end
        %Find valid spikes
        spiketimes = spiketimes(find(~isExcluded(spiketimes, excludeperiods)));
        spikebins = periodAssign(spiketimes, riptimes(:,[1 2]));
        
        %spikebins = lookup(spiketimes,riptimes(:,2));
        if ~isempty(spiketimes)
            validspikes = find(spikebins);
            spiketimes = spiketimes(validspikes);
            spikebins = spikebins(validspikes);
            
            %             spikedeviation = abs(spiketimes - riptimes(spikebins,2));
            %             validspikes = find(spikedeviation <= (window/2));
            %             spiketimes = spiketimes(validspikes);
            %             spikebins = spikebins(validspikes);
        end
        
        if ~isempty(spiketimes)
            tmpcelldata = [spiketimes spikebins];
            tmpcelldata(:,3) = cellcount;
        else
            tmpcelldata = [0 0 cellcount];
        end
        %tmpcelldata = [spiketimes spikebins];
        %tmpcelldata(:,3) = cellcount;
        
        celldata = [celldata; tmpcelldata];
        spikecount = zeros(1,size(riptimes,1));
        for i = 1:length(spikebins)
            spikecount(spikebins(i)) = spikecount(spikebins(i))+1;
        end
        
        spikecounts = [spikecounts; spikecount];
        out.index = [out.index; index];
    end
    celldata = sortrows(celldata,1); %sort all spikes by time
    
    %newtimebinsInd = find(~isExcluded(timebins, excludeperiods));
    %newtimes = timebins(newtimebinsInd);
    
    cellcounts = sum((spikecounts > 0));
    %cellcounts = cellcounts(newtimebinsInd);
    try
        eventindex = find(cellcounts >= cellcountthresh);
    catch
        index, keyboard;
    end
    
     % Sequence of CA1 cels in ripple events 
    event_cellSeq = [];
    for ev = 1:length(eventindex)
        cellsi = celldata(find(celldata(:,2)==eventindex(ev)),3); 
        [cellsi,ia] = unique(cellsi,'first');
        [~,sortorder] = sort(ia);
        event_cellSeq{ev} = cellsi(sortorder);
    end
    
    % Add non-significant events
    noneventindex = find(cellcounts < cellcountthresh);
    
    % Get other data for event
    % ------------------------
    timeindex = lookup(riptimes(:,2),posdata(:,1));  
    tmpvel = posdata(:,5);
    %tmpvel = tmpvel<(max(tmpvel)*.05);
    tmpvel = tmpvel<(2);
    
    
    resetpoints = find(diff(tmpvel) < 0)+1;
    immobiletime = cumsum(tmpvel);
    for i = 1:length(resetpoints)
        immobiletime(resetpoints(i):end) = immobiletime(resetpoints(i):end)-immobiletime(resetpoints(i)-1);
    end
    immobiletime = immobiletime/30;
    immobile = immobiletime(timeindex);
    
    %For each event define: event time, ammount of time spent unmoving,
    %trajectory, linear distance, speed, and the times and identity of
    %cells that fired during that event
    %timeindex = lookup(riptimes(:,2),statematrix.time);
    %     if iscell(statematrix.traj)
    %         traj = statematrix.traj{6}(timeindex);
    %     else
    %         traj = statematrix.traj(timeindex);
    %     end
    traj = statematrix.traj(timeindex);
    dist = statematrix.lindist(timeindex);
    %vel = posdata(timeindex,8);
    
    for event = 1:length(eventindex)
        out.eventtime(event,1:2) = riptimes(eventindex(event),[1 2]);
        out.eventimmobiletime(event,1) = immobile(eventindex(event));
        out.eventtraj(event) = traj(eventindex(event));
        out.eventdist(event) = dist(eventindex(event));
        tmpind = find(celldata(:,2) == eventindex(event));
        out.eventdata(event).spiketimes = celldata(tmpind,1);
        out.eventdata(event).cellindex = celldata(tmpind,3);
        out.eventdata(event).cellcount = cellcounts(eventindex(event));
        
        % Sequence of CA1 cells in ripple events
        cellord = celldata(find(celldata(:,2)==eventindex(event)),3); 
        [cellord,ia] = unique(cellord,'first');
        [~,sortorder] = sort(ia);
        cellord = cellord(sortorder);
        out.eventdata(event).cellSeq = indices(cellord,:);
        %out.speed(event) = vel(eventindex(event));       
    end
    
    for event = 1:length(noneventindex)
        out.noneventtime(event,1:2) = riptimes(noneventindex(event),[1 2]);
    end
    
else
    warning('No ripples found')
end



function out = periodAssign(times, periods)
% out = periodAssign(times, periods)
% TIMES is a vector of times
% PERIODS is an N by 2 list of start and end times
% Returns the index of the period that each time falls into.  If a time
% does not fall into one of the periods, a zero is returned.
% This function assumes that the periods are not overlapping.
%

if ~isempty(periods)
    oneborder = [(periods(:,1)-.0000001);periods(:,2)+.0000001];
    oneborder(:,2) = 0;
    insideborder = [(periods(:,1)+.0000001) (1:length(periods))'; (periods(:,2)-.0000001) (1:length(periods))'];
    sortedMatrix = [[-inf 0]; sortrows([oneborder;insideborder],1); [inf 0]];
else
    sortedMatrix = [[-inf 0]; [inf 0]];
end
out = sortedMatrix(lookup(times,sortedMatrix(:,1)),2);



