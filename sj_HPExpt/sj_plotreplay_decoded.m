% Plot example of replay, CA1 and PFC; CA1  raw eeg, and filtered  ripples


% % In version 1, was using older data - Cannot locate right version
% % ------------------------------
% % Load data
% savedir = '/data25/sjadhav/HPExpt/ProcessedData/Replay2014/';
% %savefile = [savedir 'HPa_day1_replay5decode_gather'];
% %savefile = [savedir 'HPa_day1_replay4decode_gather'];
% %savefile = [savedir 'HPa_day1_replaydecode_gather'];
%
% % mcarr
% -----
% load(savefile);
% %Load decodefilter
% % load '/data21/mcarr/RipplePaper/decodefilter.mat'
% % load '/data21/mcarr/RipplePaper/trainingfilter.mat'
% % load '/data21/mcarr/Bon/boncellinfo.mat'
%event_index =54;
%Great Replay event: animal = 1, day = 1, epoch = 1, event_index = 54
%Bad Replay event: animal = 1, day = 1, epoch = 1, event_index = 73;
%Medoicre, but significant: animal 1, day 1, epoch 1, event_index = 8;
%Determine which cells fire
% -------

% New Data
% -------
savedir = '/data25/sjadhav/HPExpt/ProcessedData/Replay2015/'
%savefile = [savedir 'HP_replaydata_proc1_withsig'];
savefile = [savedir 'HP_replaydata_PFCall_proc'];
load(savefile);

doPFC=1;
savefig=0;

eventindex = 42;
event_index = eventindex;
%Find largest replay event - HPa, day1 epoch 1
an = 1; dd = 1; epoch=1; animal = an;
r=decodefilter(an).output{dd}(epoch).rvalue;
slope=decodefilter(an).output{dd}(epoch).slope;
arm_index1 = decodefilter(an).output{dd}(epoch).replay_arm_index1;
arm_index2 = decodefilter(an).output{dd}(epoch).replay_arm_index2;
n=decodefilter(an).output{dd}(epoch).ncells;
pdata=decodefilter(an).output{dd}(epoch).probdata;
xd=decodefilter(an).output{dd}(epoch).xdata;
ddata=decodefilter(an).output{dd}(epoch).decodedata;

long = find(n>=10);  % Long events

% For HP_replaydata_proc1_withsig
% --------------------------------
p=decodefilter(an).output{dd}(epoch).pvalue;
sig = find(p<0.01); % Highly sig events. idx = 40,42,73
n(sig); % 10,9,9
arm_index1(sig); %0.05, -0.08, 0.11;


xdata = xd{eventindex};
probdata = pdata{eventindex};

%Define color for replay plot
color = zeros(19,3);
color(1:12,1) = [0.2 0.4 0.6 0.8 1 1 1 1 1 1 1 0.2];
color(6:15,2) = [0.2 0.4 0.6 0.8 1 1 1 1 0.8 0.2];
color(11:19,3) = [0.2 0.8 1 1 1 1 0.8 0.6 0.4];
color = color(end:-1:1,:);
color(20:25,3) = 0;

%Plot replay plot - CA1
traj1 = mean(mean(probdata{1}))>=mean(mean(probdata{2}));
if traj1; traj = 1, else traj = 2, end
traj1=1; traj2=2;

figure; hold on;  redimscreen_2versubplots
subplot(2,1,1); hold on;
for i = size(probdata{traj1},2):-1:1
    plot(xdata{traj1}+i,i*0.005+probdata{traj1}(:,i),'color',color(i,:),'LineWidth',2)
end
title(sprintf('Ev: %d; armidx1: %0.2f; armidx2: %0.2f; rval: %0.2f; slope: %0.1f; pval: %0.3f;',eventindex, arm_index1(eventindex), arm_index2(eventindex), r(eventindex), slope(eventindex), p(eventindex) ));
ylabel('CA1: Traj1 - Left Arm');

subplot(2,1,2); hold on;
for i = size(probdata{traj2},2):-1:1
    plot(xdata{traj2}+i,i*0.005+probdata{traj2}(:,i),'color',color(i,:),'LineWidth',2)
end
ylabel('CA1: Traj2 - Right Arm')
%['Ev',eventindex decodefilter(animal).output{d}(e).pvalue(event_index) decodefilter(animal).output{d}(e).rvalue(event_index)])

[y, m, d] = datevec(date);
savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/2015/%d_%d_%d_replay_example_ep1_%d', m, d, y, eventindex);
figfile = savestring
% % Save figure
if savefig==1
    print('-dpdf', savestring); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end

%-----
% PFC
% ----
if doPFC==1
    % PFC
    PFCp=decodefilter(an).output{dd}(epoch).pvalue;
    PFCr=decodefilter(an).output{dd}(epoch).PFCrvalue;
    PFCslope=decodefilter(an).output{dd}(epoch).PFCslope;
    PFCarm_index1 = decodefilter(an).output{dd}(epoch).PFCreplay_arm_index1;
    PFCarm_index2 = decodefilter(an).output{dd}(epoch).PFCreplay_arm_index2;
    PFCn=decodefilter(an).output{dd}(epoch).PFCncells;
    PFCpdata=decodefilter(an).output{dd}(epoch).PFCprobdata;
    PFCxd=decodefilter(an).output{dd}(epoch).PFCxdata;
    PFCddata=decodefilter(an).output{dd}(epoch).PFCdecodedata;
    if ~isempty(PFCxd)
        xdata = PFCxd{eventindex};
        probdata = PFCpdata{eventindex};
        
        figure; hold on;  redimscreen_2versubplots
        subplot(2,1,1); hold on;
        for i = size(probdata{traj1},2):-1:1
            plot(xdata{traj1}+i,i*0.005+probdata{traj1}(:,i),'color',color(i,:),'LineWidth',2)
        end
        title(sprintf('Ev: %d; armidx1: %0.2f; armidx2: %0.2f; rval: %0.2f; slope: %0.1f; pval: %0.3f;',eventindex, PFCarm_index1(eventindex), PFCarm_index2(eventindex), PFCr(eventindex), PFCslope(eventindex), PFCp(eventindex) ));
        ylabel('PFC: Traj1 - Left Arm');
        
        subplot(2,1,2); hold on;
        for i = size(probdata{traj2},2):-1:1
            plot(xdata{traj2}+i,i*0.005+probdata{traj2}(:,i),'color',color(i,:),'LineWidth',2)
        end
        ylabel('PFC: Traj2 - Right Arm')
        
    else
        disp('PFC empty')
    end
end

[y, m, d] = datevec(date);
savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/2015/%d_%d_%d_replay_example_ep1_%d_PFCnew', m, d, y, eventindex);
figfile = savestring
% % Save figure
if savefig==1
    print('-dpdf', savestring); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end














%% Plot rasters, color coded for CA1-CA3, left and right hemispheres
d=dd; e=epoch; trainingindex=[d e];
day = decodefilter(animal).output{d}(e).index(1,1);
epoch = decodefilter(animal).output{d}(e).index(1,2);
epoch=e;


% Re-do first part of evaluate code
% -----------------------
% -------
matches = rowfind(trainingfilter(animal).output{d}(e).index(:,[1 3 4]),decodefilter(animal).output{d}(e).index(:,[1 3 4])); %find the matching cell indices
startevent = decodefilter(animal).output{d}(e).eventtime(event_index,1);
endevent = decodefilter(animal).output{d}(e).eventtime(event_index,2);

binsize = 0.015;
timebins = startevent:binsize:endevent;

trainingdata = []; spikedata = []; indexlist = []; activecount = 0;
activerates = []; activespiketimes = [];
for trainingcell = 1:length(matches)
    if (matches(trainingcell) > 0) %we have a match
        trainingdata = [trainingdata; trainingfilter(animal).output{d}(e).rates(trainingcell,:)];
        tmpspiketimes = decodefilter(animal).output{d}(e).eventdata(event_index).spiketimes(find(decodefilter(animal).output{d}(e).eventdata(event_index).cellindex == matches(trainingcell)));
        %save all the info for the active cells
        if ~isempty(tmpspiketimes)
            activecount = activecount+1;
            activespiketimes{activecount} = tmpspiketimes;
            activerates = [activerates; trainingfilter(animal).output{d}(e).rates(trainingcell,:)];
            indexlist = [indexlist; trainingfilter(animal).output{d}(e).index(trainingcell,:)];
        end
    end
end
trainingdata = trainingdata*binsize; %transform rates to expected number of spikes
activerates = activerates*binsize;

distvector = trainingfilter(animal).output{d}(e).dist;
totalsamples = 10000;
spikedata = [];
cellsactive = [];
exponentmatrix = exp(-activerates);
%histogram the spikes for each active cell
for i = 1:length(activespiketimes)
    spikebins = lookup(activespiketimes{i},timebins);
    spikecount = zeros(1,length(timebins));
    for j = 1:length(spikebins)
        spikecount(spikebins(j)) = spikecount(spikebins(j))+1;
    end
    spikedata = [spikedata; spikecount];
    cellsactive = [cellsactive; (spikecount > 0)];
end

%the decoded data contains spatial probabilities, and is x by t
decodedata = zeros(size(activerates,2),size(spikedata,2));
naninds = find(isnan(activerates(1,:)));
for t = 1:size(spikedata,2) %time
    Tspikecount = repmat(spikedata(:,t),1,size(activerates,2));
    %calculate P(spikecount|x) for this timebin across all cells and all x
    spatialprob = prod(((activerates.^Tspikecount)./factorial(Tspikecount)).*exp(-activerates),1)';
    spatialprob(naninds) = 0;
    spatialprob = spatialprob/sum(spatialprob);  %normalize across space
    decodedata(:,t) = spatialprob;
end
% -----------


cell_identity = zeros(size(cellsactive));

%Try sorting
ind = zeros(size(cellsactive,1),1);
for i = 1:size(cellsactive,1)
    ind(i) = find(cellsactive(i,:),1);
end
[y ind] = sort(ind,1,'ascend'); clear y
cellsactive = cellsactive(ind,:);
indexlist = indexlist(ind,:);

for c = 1:size(indexlist,1);
    cell_identity(c,find(cellsactive(c,:))) = -3;
    %     if rowfind(indexlist(c,[3 4]),ca1_left)
    %         cell_identity(c,find(cellsactive(c,:))) = -1;
    %     elseif rowfind(indexlist(c,[3 4]),ca3_left)
    %         cell_identity(c,find(cellsactive(c,:))) = -3;
    %     elseif rowfind(indexlist(c,[3 4]),ca1_right)
    %         cell_identity(c,find(cellsactive(c,:))) = 1;
    %     elseif rowfind(indexlist(c,[3 4]),ca3_right)
    %         cell_identity(c,find(cellsactive(c,:))) = 3;
    %     end
end

figure; hold on;
for i = 1:size(cellsactive,1)
    if any(cell_identity(i,:)==-1)
        plotraster(activespiketimes{ind(i)},i,0.8,2,'color','b','LineWidth',2)
    end
    if any(cell_identity(i,:)==1)
        plotraster(activespiketimes{ind(i)},i,0.8,2,'color','c','LineWidth',2)
    end
    if any(cell_identity(i,:)==-3)
        plotraster(activespiketimes{ind(i)},i,0.8,2,'color','r','LineWidth',2)
    end
    if any(cell_identity(i,:)==3)
        plotraster(activespiketimes{ind(i)},i,0.8,2,'color','m','LineWidth',2)
    end
end
nHp=i;
set(gca,'xlim',[timebins(1) timebins(end)],'xtick',timebins(1):0.1:timebins(end),...
    'xticklabel',[timebins(1):0.1:timebins(end)]-startevent,'ytick',1.4:1:size(cellsactive,1)+0.4,...
    'yticklabel',1:size(cellsactive,1))
box off
xlabel('Time since ripple detection (sec)')
ylabel('Cell count')


if doPFC==1
    % Do For PFC also
    % ----------------
    matches = rowfind(PFCtrainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).index(:,[1 3 4]),decodefilter(animal).output{d}(e).pindex(:,[1 3 4])), %find the matching cell indices for PFC
    cellcount = decodefilter(animal).output{dd}(epoch).eventdata(eventindex).pcellcount;
    %traj_cellSeq = trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).cellSequence; % cell seq for the 4 trajectories
    %cellpeakpos = trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).cellpeakpos;
    %cellSeq = decodefilter(animal).output{dd}(epoch).eventdata(eventindex).cellSeq; % cell sequence in the SWR
    
    %timebins = startevent:binsize:endevent;
    %peventcellsactive = [];
    %pactivecount = 0;
    %activepeakpos = [];
    
    ptrainingdata = []; pspikedata = []; pindexlist = []; pactivecount = 0;
    pactiverates = []; pactivespiketimes = [];  pdecodedata = [];
    
    for trainingcell = 1:length(matches)
        if (matches(trainingcell) > 0) %we have a match
            pindexlist = [pindexlist; PFCtrainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).index(trainingcell,:)];
            ptrainingdata = [ptrainingdata; PFCtrainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).rates(trainingcell,:)];
            %currpeakpos = cellpeakpos(trainingcell,:);
            
            if ~isempty(decodefilter(animal).output{dd}(epoch).eventdata(eventindex).pspiketimes) % PFC can be empty - no spike
                tmpspiketimes = decodefilter(animal).output{dd}(epoch).eventdata(eventindex).pspiketimes(find(decodefilter(animal).output{dd}(epoch).eventdata(eventindex).pcellindex == matches(trainingcell)));
                %save all the info for the active cells
                if ~isempty(tmpspiketimes)
                    pactivecount = pactivecount+1;
                    pactivespiketimes{pactivecount} = tmpspiketimes;
                    pactiverates = [pactiverates; PFCtrainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).rates(trainingcell,:)];
                    %activepeakpos = [activepeakpos; currpeakpos];
                end
            end
            
        end
    end
    ptrainingdata = ptrainingdata*binsize; %transform rates to expected number of spikes
    pactiverates = pactiverates*binsize;
    
    % ---------------------------------------------------------
    % Decoding Part of PFC
    % ----------------------------------------------------------
    expSpikeCounts = pactiverates;
    exponentmatrix = exp(-expSpikeCounts);
    pcellsactive = [];
    for i = 1:length(pactivespiketimes)
        spikebins = lookup(pactivespiketimes{i},timebins);
        spikecount = zeros(1,length(timebins));
        for j = 1:length(spikebins)
            spikecount(spikebins(j)) = spikecount(spikebins(j))+1;
        end
        pspikedata = [pspikedata; spikecount];
        pcellsactive = [pcellsactive; (spikecount > 0)];
    end
    
    %the decoded data contains spatial probabilities, and is x by t
    pdecodedata = zeros(size(expSpikeCounts,2),size(pspikedata,2));
    naninds = find(isnan(expSpikeCounts(1,:)));
    for t = 1:size(pspikedata,2) %time
        Tspikecount = repmat(pspikedata(:,t),1,size(expSpikeCounts,2));
        %calculate P(spikecount|x) for this timebin across all cells and all x
        spatialprob = prod(((expSpikeCounts.^Tspikecount)./factorial(Tspikecount)).*exp(-expSpikeCounts),1)';
        spatialprob(naninds) = 0;
        spatialprob = spatialprob/sum(spatialprob);  %normalize across space
        %spatialprob(find(spatialprob < max(spatialprob/2))) = 0;
        pdecodedata(:,t) = spatialprob;
    end
    
    for i = 1:size(pcellsactive,1)
        plotraster(pactivespiketimes{i},i+nHp,0.8,2,'color','b','LineWidth',2)
    end
    
    
    
end





% Save figure
[y, m, d] = datevec(date);
savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/2015/%d_%d_%d_replay_raster_ep1_%d', m, d, y, eventindex);
figfile = savestring;
if savefig==1
    print('-dpdf', savestring); print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end

%% Plot EEGs for this event

if e==1,
    epo=4;
else
    epo=6;
end

% CA1
load '/data25/sjadhav/HPExpt/HPa_direct/EEG/HPaeeg01-4-01.mat'
etimes = geteegtimes(eeg{1}{epo}{1});
eind = lookup([startevent endevent],etimes);
ca1_left = eeg{1}{epo}{1}.data(eind(1) - 1500:eind(2)+1500);
eegtime = etimes(eind(1)-1500:eind(2)+1500);
starttime = etimes(eind(1));

load '/data25/sjadhav/HPExpt/HPa_direct/EEG/HParipple01-4-01.mat'
ca1_left_ripple = ripple{1}{epo}{1}.data(eind(1) - 1500:eind(2)+1500,1);


% --mcarr
% %CA1 left
% load '/data21/mcarr/Bon/EEGnonreference/boneeg03-2-13.mat'
% etimes = geteegtimes(eeg{3}{2}{13});
% eind = lookup([startevent endevent],etimes);
% ca1_left = eeg{3}{2}{13}.data(eind(1) - 1500:eind(2)+1500);
% eegtime = etimes(eind(1)-1500:eind(2)+1500);
% starttime = etimes(eind(1));
%
% load '/data21/mcarr/Bon/EEGnonreference/bonripple03-2-13.mat'
% ca1_left_ripple = ripple{3}{2}{13}.data(eind(1) - 1500:eind(2)+1500,1);
%
% load '/data21/mcarr/Bon/EEGnonreference/bonlowgamma03-2-13.mat'
% ca1_left_gamma = lowgamma{3}{2}{13}.data(eind(1) - 1500:eind(2)+1500,1);
%
% %CA1 right
% load '/data21/mcarr/Bon/EEGnonreference/boneeg03-2-03.mat'
% etimes = geteegtimes(eeg{3}{2}{3});
% eind = lookup(eegtime,etimes);
% ca1_right = eeg{3}{2}{3}.data(eind);
%
% load '/data21/mcarr/Bon/EEGnonreference/bonripple03-2-03.mat'
% ca1_right_ripple = ripple{3}{2}{3}.data(eind);
%
% load '/data21/mcarr/Bon/EEGnonreference/bonlowgamma03-2-03.mat'
% ca1_right_gamma = lowgamma{3}{2}{3}.data(eind);
%
% %CA3 left
% load '/data21/mcarr/Bon/EEGnonreference/boneeg03-2-18.mat'
% etimes = geteegtimes(eeg{3}{2}{18});
% eind = lookup(eegtime,etimes);
% ca3_left = eeg{3}{2}{18}.data(eind);
%
% load '/data21/mcarr/Bon/EEGnonreference/bonripple03-2-18.mat'
% ca3_left_ripple = ripple{3}{2}{18}.data(eind);
%
% load '/data21/mcarr/Bon/EEGnonreference/bonlowgamma03-2-18.mat'
% ca3_left_gamma = lowgamma{3}{2}{18}.data(eind);
%
% %CA3 right
% load '/data21/mcarr/Bon/EEGnonreference/boneeg03-2-01.mat'
% etimes = geteegtimes(eeg{3}{2}{1});
% eind = lookup(eegtime,etimes);
% ca3_right = eeg{3}{2}{1}.data(eind);
%
% load '/data21/mcarr/Bon/EEGnonreference/bonripple03-2-01.mat'
% ca3_right_ripple = ripple{3}{2}{1}.data(eind);
%
% load '/data21/mcarr/Bon/EEGnonreference/bonlowgamma03-2-01.mat'
% ca3_right_gamma = lowgamma{3}{2}{1}.data(eind);
% --------mcarr


eegtime = eegtime - starttime;
clear eeg etimes eind ripple lowgamma

% %Plot the  eeg traces
% ---------------
figure; hold on; redimscreen_2versubplots;
subplot(3,1,1); hold on;
plot(eegtime,ca1_left,'r')
set(gca,'xlim',[-0.4 0.7],'ylim',[-800 800])
legend([{'LFP(1-400Hz)'}]);
xlabel('Time since ripple detection (sec)')
% figure
% plot(eegtime,ca1_left,'b',eegtime,1000+ca1_right,'c',eegtime,2000+ca3_left,'r',eegtime,3000+ca3_right,'m')
% set(gca,'xlim',[-0.4 0.7],'ylim',[-800 5000])
% xlabel('Time since ripple detection (sec)')
% legend([{'CA1 left'},{'CA1 right'},{'CA3 left'},{'CA3 right'}])

%Plot the  ripple traces at large time scale and zoomed in
% ---------------
subplot(3,1,2); hold on;
plot(eegtime,ca1_left_ripple,'r')
set(gca,'xlim',[-0.4 0.7],'ylim',[-150 150])
xlabel('Time since ripple detection (sec)')
legend([{'Ripple band'}])
box off
% figure
% plot(eegtime,ca1_left_ripple,'b',eegtime,200+ca1_right_ripple,'c',...
%     eegtime,400+ca3_left_ripple,'r',eegtime,600+ca3_right_ripple,'m')
% set(gca,'xlim',[-0.4 0.7],'ylim',[-150 750])
% xlabel('Time since ripple detection (sec)')
% legend([{'CA1 left'},{'CA1 right'},{'CA3 left'},{'CA3 right'}])
% box off


%Plot extended gamma, ripple, and eeg for figure 2e
% ---------------
subplot(3,1,3); hold on;
plot(eegtime,ca1_left_ripple,'r',...
    eegtime,-600+ca1_left,'k')
set(gca,'xlim',[-0.5 1],'ylim',[-1200 500])
xlabel('Time since ripple detection (sec)')
legend([{'CA1 ripple'},{'CA1 eeg'}])
box off
% figure
% plot(eegtime,ca1_left_ripple,'k',...
%     eegtime,-1600+ca3_left,'b',eegtime,-1600+ca3_left_gamma,'c',...
%     eegtime,-600+ca1_right,'r',eegtime,-600+ca1_right_gamma,'m')
% set(gca,'xlim',[-0.5 1],'ylim',[-2500 500])
% xlabel('Time since ripple detection (sec)')
% legend([{'CA1 ripple'},{'CA3 eeg'},{'CA3 gamma'},{'CA1 eeg'},{'CA1 gamma'}])
% box off

% Save figure
[y, m, d] = datevec(date);
savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/2015/%d_%d_%d_2e_ep1_%d', m, d, y, eventindex);
figfile = savestring;
if savefig==1
    print('-dpdf', savestring)
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end
