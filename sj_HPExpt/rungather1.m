function [ gatherdatafile ] = rungather1(win)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

savedir = '/data25/sjadhav/HPExpt/ProcessedData/';
val=1; savefile = [savedir 'HP_ripplemod_PFC_alldata']; area = 'PFC'; clr = 'b';
load(savefile);

gatherdatafile = [savedir,'HP_ripplemod_PFC_alldata_gather_winneg',num2str(abs(win(1))),'to',num2str(win(2))] % PFC cells to Hipp ripples
savegatherdata = 1;

% Parameters if any
% -----------------

% -------------------------------------------------------------

cnt=0; % Count how many cells will be kept based on nspikes in output: >0
allanimindex=[]; alldataraster=[]; alldatahist = []; all_Nspk=[];

for an = 1:length(modf)
    for i=1:length(modf(an).output{1})
        % Check for empty output - If Cell defined in rpoch and Nspks in ripple response wndow > 0
        if (modf(an).output{1}(i).Nspikes > 0)
            cnt=cnt+1;
            anim_index{an}(cnt,:) = modf(an).output{1}(i).index;
            % Only indexes
            animindex=[an modf(an).output{1}(i).index]; % Put animal index in front
            allanimindex = [allanimindex; animindex]; % Collect all Anim Day Epoch Tet Cell Index
            % Data
            alldataraster{cnt} = modf(an).output{1}(i).rip_spks_cell; % Only get raster and histogram response
            alldatahist{cnt} = modf(an).output{1}(i).rip_spkshist_cell;
            all_Nspk(cnt) = modf(an).output{1}(i).Nspikes;
            alldataraster_rdm{cnt} = modf(an).output{1}(i).rdm_spks_cell; % Only get raster and histogram response
            alldatahist_rdm{cnt} = modf(an).output{1}(i).rdm_spkshist_cell;
            % trialResps: Summed Nspks/trial in respective window
            alldatatrialResps{cnt} = modf(an).output{1}(i).trialResps;
            alldatatrialResps_bck{cnt} = modf(an).output{1}(i).trialResps_bck;
            alldatatrialResps_rdm{cnt} = modf(an).output{1}(i).trialResps_rdm;
            % Nspikes summed across trials in response and bckgnd window
            all_Nspk_resp(cnt) = sum(modf(an).output{1}(i).trialResps);
            all_Nspk_bck(cnt) = sum(modf(an).output{1}(i).trialResps_bck);
            % Properties
            allcellfr(cnt) = modf(an).output{1}(i).cellfr;
            
            %end
            if cnt==1
                pret =  modf(an).output{1}(i).pret;
                postt = modf(an).output{1}(i).postt;
                binsize = modf(an).output{1}(i).binsize;
                rwin = modf(an).output{1}(i).rwin;
                bckwin = modf(an).output{1}(i).bckwin;
                bins_resp  = modf(an).output{1}(i).bins_resp;
                bins_bck = modf(an).output{1}(i).bins_bck;
                timeaxis = modf(an).output{1}(i).timeaxis;
            end
        end
    end
    
end

% Consolidate single cells across epochs. Multiple methods: see also DFSsj_getcellinfo and DFSsj_xcorrmeasures2
% ----------------------------------------------------------------------------

allripplemod = struct;

% Method 1
% ---------------------------------------------
%     cntcells=0;
%     animdaytetcell = unique(allanimindex(:,[1 2 4 5]),'rows'); % Collapse across epochs
%     for ind = 1:size(animdaytetcell,1)
%         currhist=[]; currraster = [];
%         for a = 1:size(allanimindex,1)
%             if animdaytetcell(ind,:)==allanimindex(a,[1 2 4 5]) % Epoch match
%                 currhist = [currhist; alldatahist{a}];
%                 currraster = [currraster; alldataraster{a}];
%             end
%         end
%         cntcells = cntcells + 1;
%         allripplemod_idx(cntcells,:)=animdaytetcell(ind,:);
%         allripplemod(cntcells).index=animdaytetcell(ind,:);
%         allripplemod(cntcells).hist=currhist;
%         allripplemod(cntcells).raster=currraster;
%         allripplemod(cntcells).Nspk=Nspk;
%     end


% Method 2
% ---------
dummyindex=allanimindex;  % all anim-day-epoch-tet-cell indices
cntcells=0;
for i=1:length(alldatahist)
    animdaytetcell=allanimindex(i,[1 2 4 5]);
    ind=[];
    while rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))~=0          % collect all rows (epochs)
        ind = [ind rowfind(animdaytetcell,dummyindex(:,[1 2 4 5]))];        % finds the first matching row
        dummyindex(rowfind(animdaytetcell,dummyindex(:,[1 2 4 5])),:)=[0 0 0 0 0]; % after adding index, remove the corresponding row
        % so you could find the next one
    end
    
    % Gather everything for the current cell across epochs
    currhist=[]; currraster=[]; currNspk=0; currNspk_resp=0; currNspk_bck=0; curr_cellfr=[];
    currhist_rdm=[]; currraster_rdm=[];
    currtrialResps=[]; currtrialResps_rdm=[]; currtrialResps_bck=[];
    for r=ind
        currNspk = currNspk + all_Nspk(r);
        currNspk_resp = currNspk_resp + all_Nspk_resp(r);
        currNspk_bck = currNspk_bck + all_Nspk_bck(r);
        currhist = [currhist; alldatahist{r}];
        currraster = [currraster, alldataraster{r}];
        currhist_rdm = [currhist_rdm; alldatahist_rdm{r}];
        currraster_rdm = [currraster_rdm, alldataraster_rdm{r}];
        currtrialResps = [currtrialResps, alldatatrialResps{r}];
        currtrialResps_rdm = [currtrialResps_rdm, alldatatrialResps_rdm{r}];
        currtrialResps_bck = [currtrialResps_bck, alldatatrialResps_bck{r}];
        curr_cellfr = [curr_cellfr; allcellfr(r)];
    end
    
    % Condition for Nspk. Version 1 had a min of 50 for entire window. Increase it to 100,
    % and can also add a condition for spikes in (resp+bck) window. Need to try a few values
    if (currNspk >= 100)
        %if ((currNspk_resp+currNspk_bck) >= 40)
        %if (currNspk >= 100) && ((currNspk_resp+currNspk_bck) >= 40)
        cntcells = cntcells + 1;
        % For Ndl-GIdeon;s animal, shift days
        if animdaytetcell(1)==4
            animdaytetcell(2)=animdaytetcell(2)-7; % Day starts from no. 8
        end
        allripplemod_idx(cntcells,:)=animdaytetcell;
        allripplemod(cntcells).index=animdaytetcell;
        allripplemod(cntcells).hist=currhist*(1000/binsize); % Convert to firing rate in Hz
        allripplemod(cntcells).raster=currraster;
        allripplemod(cntcells).Nspk=currNspk;
        allripplemod(cntcells).hist_rdm=currhist_rdm*(1000/binsize); % Convert to firing rate in Hz
        allripplemod(cntcells).raster_rdm=currraster_rdm;
        % Trial Resps
        allripplemod(cntcells).trialResps = currtrialResps';
        allripplemod(cntcells).trialResps_rdm = currtrialResps_rdm';
        allripplemod(cntcells).trialResps_bck = currtrialResps_bck';
        % Properties
        allripplemod(cntcells).cellfr = nanmean(curr_cellfr);
    end
end


% GIDEON
%varRange=[400:700];
varRange=[500-win(1):500+win(2)];
bckRange=[1:(win(2)-win(1))];
for ii=1:cntcells
    ii
    curRast=allripplemod(ii).raster;
    numRips=length(curRast);
    curRastMat=zeros(numRips,1100);
    for i=1:numRips,curRastMat(i,round(curRast{i}+551))=1;end
    %figure;
    meanResp=smooth(mean(curRastMat(:,50:end-50)),50);
    respVar=var(meanResp(varRange)); bckVar=var(meanResp(bckRange));
    var_respbck = respVar/bckVar; % Var in win divided by var in bck
    respVarShufs=[]; bckVarShufs=[];
    respbckratioShufs=[];
    numRuns=1000;
    for runs=1:numRuns
        curRastMatShuf=zeros(numRips,1100);
        
        for i=1:numRips
            shiftVal=round(rand(1)*1000);
            curRastMatShuf(i,1+mod(round(curRast{i}+551+shiftVal),1100))=1;
        end
        meanRespShuf=smooth(mean(curRastMatShuf(:,50:end-50)),50);
        %             plot(smooth(mean(curRastMatShuf(:,50:end-50)),50),'r')
        %             hold on;
        respVarShufs=[respVarShufs var(meanRespShuf(varRange))];
        bckVarShufs=[bckVarShufs var(meanRespShuf(bckRange))];
        respbckratioShufs=[respbckratioShufs var(meanRespShuf(varRange))./var(meanRespShuf(bckRange))];
    end
    rasterShufP=1-sum(respVar>respVarShufs)/numRuns;
    rasterShufP_respbck=1-sum(var_respbck>respbckratioShufs)/numRuns;
    %          plot(smooth(mean(curRastMat(:,50:end-50)),50),'k','linewidth',2)
    allripplemod(ii).rasterShufP=rasterShufP;
    varRespAmp=respVar/mean(respVarShufs);
    allripplemod(ii).varRespAmp=varRespAmp;
    
    
    % Ratio of var in Resp / ratio of var in bck: how much did it change in resp relative to bck
    varBckAmp=bckVar/mean(bckVarShufs);
    var_changerespbck = varRespAmp/varBckAmp;
    %Save
    allripplemod(ii).var_respbck=var_respbck; allripplemod(ii).rasterShufP_respbck=rasterShufP_respbck;
    allripplemod(ii).var_changerespbck=var_changerespbck;
end








% Calculations/ Stats. Stats between response and b
% Similar to ...getrip4
% -----------------------------------------------------------
for i=1:cntcells
    
    curr_cellfr = allripplemod(i).cellfr;
    currhist = allripplemod(i).hist; %currraster = allripplemod(i).raster;
    currhist_rdm = allripplemod(i).hist_rdm; %currraster_rdm = allripplemod(i).raster_rdm;
    
    % Get the bckresp, rresp and rdmresp again - using firing rate matrices
    rresp = currhist(:,bins_resp);
    bckresp = currhist(:,bins_bck);
    rresp_rdm = currhist_rdm(:,bins_resp);
    
    
    % Bck
    avgbckresp_trial = mean(bckresp,2); avgrespbck_trial = avgbckresp_trial; % Mean in bck for each ripple
    %trialRespsh_bck = sum(bckresp,2);  % Nspikes/ trial in resp window
    avgbckhist = mean(bckresp); % Avg bck histogram
    mean_bckresp = mean(mean(bckresp)); % Single value
    distr_bckresp = bckresp(:); %All values taken by bins in background
    
    % Response
    avgresp_trial = mean(rresp,2); % Mean for each ripple
    %trialRespsh = sum(rresp,2); % Nspikes/ trial in resp window
    avgresphist = mean(rresp,1); % Avg resp histogram
    mean_rresp = mean(mean(rresp)); % Single value
    
    % RandomResponse
    avgresprdm_trial = mean(rresp_rdm,2); % Mean for each random ripple
    %trialRespsh_rdm = sum(rresp_rdm,2); % Nspikes/ trial in resp window
    avgrdmhist = mean(rresp_rdm,1); % Avg resp histogram
    mean_rdmresp = mean(mean(rresp_rdm)); % Single value
    
    sig_shuf = 0; sig_ttest = 0;
    
    % 0) Simple t-test
    % -----------------
    [sig_ttest, p] = ttest2(avgbckresp_trial, avgresp_trial);
    
    
    % % 1) Significance test - USE SHUFFLE BETWEEN MEAN RESP AND MEAN BCK FOR EACH TRIAL
    % % ---------------------------------------------------------------------------
    % Get the actual mean difference between bck and resp
    Dm = mean(avgresp_trial) - mean(avgbckresp_trial); % DONT want absolute value. want to shuffle.
    
    % Shuffle mean bck and mean resp 1000 times
    comb = [avgresp_trial; avgbckresp_trial];
    ntr = size(comb,1);
    nshuffles = 1000;
    for shufidx = 1:nshuffles
        order = randperm(ntr);
        shuffle = comb(order,:);
        
        shufresp = shuffle(1:ntr/2,:); shufavgresp = mean(shufresp);
        shufbck = shuffle((ntr/2)+1:ntr,:); shufavgbckresp = mean(shufbck);
        %if shufidx==1, figure; hold on; end
        %plot(shufavgresp,'b'); plot(shufavgrdmresp,'g')
        Dshuf(shufidx) = shufavgresp - shufavgbckresp;
    end
    % Can Plot the distribution of shuffled values
    histD = histc(Dshuf,min(Dshuf):0.1:max(Dshuf));
    %figure; hold on; plot([min(Dshuf):0.1:max(Dshuf)],histD)
    
    % Get significance by comparing Dm to Dshuf. One-tailed test
    % --------------------------------------------------------------
    if Dm>=0
        pshuf = length(find(Dshuf>Dm))/nshuffles;
        if Dm > prctile(Dshuf,95)
            sig_shuf = 1;
        end
        type = 'exc'; peakresp = max(avgresphist); % Peak in response histogram
        peakresp_rdm = max(avgrdmhist);
    else
        pshuf = length(find(Dshuf<Dm))/nshuffles;
        if Dm < prctile(Dshuf,5)
            sig_shuf = 1;
        end
        type = 'inh'; peakresp = min(avgresphist); % Trough in response histogram
        peakresp_rdm = min(avgrdmhist);
    end
    % Get the p-value of shuffle. The modulation index for shuffle will be the prctile value
    % ----------------------------
    modln_shuf = 100 - (pshuf*100); %eg p=0.05 => prctile is 95
    % Get %tage change over baseline and peak/trough: Save with sign- +ve or -ve modln. Not abs
    % ----------------------------------------------
    modln_raw = Dm;
    modln = 100*(Dm)/mean(avgbckresp_trial); % Mean %tage change above/below baseline
    peakchange = peakresp - mean(avgbckhist); % mean(avgbckhist) is same as mean(avgbckresp_trial)
    modln_peak = 100*(peakchange)/mean(avgbckresp_trial); % Peak/trough %tage change above/below baseline
    modln_div = 100*peakresp/mean(avgbckresp_trial);
    
    
    % Get same values for random times as well
    Dm_rdm = mean(avgresprdm_trial) - mean(avgbckresp_trial);
    modln_rdm = 100*(Dm_rdm)/mean(avgbckresp_trial); % Mean %tage change above/below baseline
    peakchange_rdm = peakresp_rdm - mean(avgbckhist); % mean(avgbckhist) is same as mean(avgbckresp_trial)
    modln_peak_rdm = 100*(peakchange_rdm)/mean(avgbckresp_trial); % Peak/trough %tage change above/below baseline
    
    
    % Save
    % -----
    allripplemod(i).Dm = Dm;
    allripplemod(i).pshuf = pshuf; allripplemod(i).p = pshuf;
    allripplemod(i).sig_shuf = sig_shuf; allripplemod(i).h = sig_shuf; % Sig or not
    allripplemod(i).sig_ttest = sig_ttest; % Sig or not
    allripplemod(i).modln_shuf = modln_shuf ; % %tile value of shuffle
    allripplemod(i).modln_peak = modln_peak; % %tage peak change over baseline
    allripplemod(i).modln_div = modln_div;
    allripplemod(i).modln = modln; % %tage mean change over baseline
    allripplemod(i).modln_raw = modln_raw; % Raw change in firing rate. The statistic
    allripplemod(i).type = type; % exc or inh
    allripplemod(i).anim = allripplemod(i).index(1); allanim(i) = allripplemod(i).index(1);
    allripplemod(i).days = allripplemod(i).index(2); alldays(i) = allripplemod(i).index(2);
    
    % Mean resp from histogram in respective window
    allripplemod(i).avghisttrialResps = avgresp_trial;
    allripplemod(i).avghisttrialResps_rdm = avgresprdm_trial;
    allripplemod(i).avghisttrialResps_bck = avgrespbck_trial;
    
    allsig_shuf(i) = sig_shuf;
    allsig_ttest(i) = sig_ttest;
    
    % Properties
    allripplemod(i).cellfr = curr_cellfr;
    allripplemod(i).Nrip = length(avgresp_trial);
    
    % Rdm resp modulation
    allripplemod(i).modln_peak_rdm = modln_peak_rdm; % %tage peak change over baseline
    allripplemod(i).modln_rdm = modln_rdm; % %tage mean change over baseline
    
end



% Save
% -----
if savegatherdata == 1
    save(gatherdatafile);
end





end

