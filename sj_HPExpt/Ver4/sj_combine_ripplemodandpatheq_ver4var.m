
% Ver4 : Starting 10Feb2014 - Sync codes with everyone

% Version 2 - USing filter framework ripplemod

% USe the saved data files - HP_thetamod and HP_ripplemod to plot
% correlations between theta modulation and ripple modulation'

clear;

savefig1=0;
savedir = '/data25/sjadhav/HPExpt/ProcessedData/';


patheqfile = [savedir 'HPall_PFC_patheq1_gather'];
area = 'PFC'; state = ''; %state = 'sleep'; %or state = '';
%PFC or CA1 - specify area above
ripplefile = [savedir 'HP_ripplemod',state,'_',area,'_alldata_std3_speed4_ntet2_Nspk50_gather_2-12-2014']; % ripple mod in awake or sleep

load(ripplefile, 'allripplemod','allripplemod_idx'); % load allripplemod and allripplemod_idx.
load(patheqfile,'allpatheq','patheq_output_idx');

if strcmp(state,'sleep'),
    statename = 'Sleep';
else
    statename = 'Run';
end

% Match idxs as in xcorrmesaures2

cntcells=0; cnt_mismatch=0;

% for i=1:length(allripplemod)
%
%     rippleidx = allripplemod_idx(i,:);
%     match = rowfind(rippleidx, allthetamod_idx);

for i=1:length(allpatheq)
    i;
    curridx = patheq_output_idx(i,:);
    
    % DONT SHIFT ANY DAYS
    % Temp - till Gideon's animal Nadal is fixed in DFSsj_plotthetamod
    %     if curridx(1)==4
    %         curridx(2)=curridx(2)-7; %Adjust day
    %     end
    
    match = rowfind(curridx, allripplemod_idx);
    
    if match~=0,
        cntcells = cntcells+1;
        allmod(cntcells).idx = curridx;
        matchidx = curridx
        % Patheq
        allmod(cntcells).patheq = allpatheq(i);
        matchpatheq(cntcells) = allpatheq(i);
        
        % Ripple
        allmod(cntcells).sig_shuf = allripplemod(match).sig_shuf; % Use this to determine significance
        %allmod(cntcells).pshuf = allripplemod(match).pshuf;
        %allmod(cntcells).D = allripplemod(match).D; % Distance metric
        allmod(cntcells).ripmodln_peak = allripplemod(match).modln_peak; % % peak change above baseline
        allmod(cntcells).ripmodln = allripplemod(match).modln; % Mean change over baseline
        allmod(cntcells).ripmodln_shuf = allripplemod(match).modln_shuf; % %value of significance
        allmod(cntcells).sig_ttest = allripplemod(match).sig_ttest;
        
        % New
        %allmod(cntcells).modln_raw = allripplemod(match).modln_raw;
        allmod(cntcells).ripmodln_div = allripplemod(match).modln_div; % % peak change above baseline
        allmod(cntcells).pvar = allripplemod(match).rasterShufP;
        allmod(cntcells).mvar = allripplemod(match).varRespAmp;
        allmod(cntcells).mvar2 = allripplemod(match).varRespAmp2;
        allmod(cntcells).pvar2 = allripplemod(match).rasterShufP2;
        
        %allmod(cntcells).mvar3 = allripplemod(match).var_changerespbck;
        %matchripmod(cntcells) = allripplemod(match).varRespAmp2;
        
        % Prop
        match
        allmod(cntcells).cellfr = allripplemod(match).cellfr;
        
        % Rdm resp modln
        allmod(cntcells).rdmmodln_peak = allripplemod(match).modln_peak_rdm; % % peak change above baseline
        allmod(cntcells).rdmmodln = allripplemod(match).modln_rdm; % Mean change over baseline
        
    end
    
end

% ------------------
% Population Figures
% ------------------
forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

figdir = '/data25/sjadhav/HPExpt/Figures/Jan2014/';
summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',40);
    tfont = 40;
    xfont = 40;
    yfont = 40;
end

% Get data
for i=1:length(allmod)
    
    % Ripple
    allripmodln_peak(i) = allmod(i).ripmodln_peak;
    allripmodln_div(i) = allmod(i).ripmodln_div;
    allripmodln(i) = allmod(i).ripmodln;
    allripmodln_shuf(i) = allmod(i).ripmodln_shuf; %[]
    %allD(i) = allmod(i).D;
    %allpshuf(i) = allmod(i).pshuf;
    allsigshuf(i) = allmod(i).sig_shuf;
    allsigttest(i) = allmod(i).sig_ttest;
    
    % New
    allpvar(i) = allmod(i).pvar;
    allripmodln_var(i) = allmod(i).mvar;
    allripmodln_var2(i) = allmod(i).mvar2;
    %allripmodln_var3(i) = allmod(i).mvar3;
    allpvar2(i) = allmod(i).pvar2;
    %allmodln_raw(i) = allmod(i).modln_raw;
    
    % Prop
    allcellfr(i) = allmod(i).cellfr;
    
    % Rdm resp
    allrdmmodln_peak(i) = allmod(i).rdmmodln_peak;
    allrdmmodln(i) = allmod(i).rdmmodln;
    
    
    
end



% WhichSWR modln to use
% ------------------------
%allripmodln = allripmodln_peak; % Peak change over baseline
%allripmodln = allripmodln; % Mean change over baseline
%allripmodln = abs(allripmodln); % Mean change over baseline

%allripmodln = abs(allripmodln_peak);
allripmodln = abs(allripmodln_var2);

%sigrip = find(allsigshuf==1); sigboth = find(allprayl<0.05 & allsigshuf==1); allrdmmodln = abs(allrdmmodln_peak); %



% %Remove outlier in allripmodln_var
% if ~strcmp(state,'sleep')
%     rem = find(allripmodln>12);
%     allripmodln(rem)=[]; allmodln(rem)=[]; allkappas(rem)=[]; allprayl(rem)=[]; allpvar(rem)=[]; allpvar2(rem)=[]; allsigshuf(rem)=[];
%     allcellfr(rem) = [];
% end


% sigrip = find(allpvar2<0.05); sigtheta = find(allprayl<0.05); sigboth = find(allprayl<0.05 & allpvar2<0.05==1);
% allsig = union(sigrip, sigtheta);
%
%
% % Vector of 0s and 1s - sig ripple vs sig theta
% ripvec = zeros(size(allripmodln)); ripvec(sigrip)=1;
% thetavec = zeros(size(allripmodln)); thetavec(sigtheta)=1;
% [rvec,pvec] = corrcoef(ripvec,thetavec);



%x = find(allmodln > 0.65 & allmodln < 0.7 & allripmodln > 80);
%allidxs(x,:);





% -------------------------------% -------------------------------% -------------------------------
% -------------------------------% -------------------------------% -------------------------------



% -------------------------------
%PathEq vs. Ripple Modulation
%-------------------------------------

findnan = find(isnan(matchpatheq));
matchpatheq(findnan)=[]; allmodln = matchpatheq;
allripmodln(findnan)=[]; matchripmod = allripmodln;

figure; hold on;
% forppr=1;
% if forppr==1, redimscreen_figforppr1; else redimscreen_figforppt1; end

plot(matchpatheq, matchripmod, 'k.','MarkerSize',24); % Plot all if you want to

%plot(allmodln(sigtheta), allripmodln(sigtheta), 'k.','MarkerSize',24);
%plot(allmodln(sigrip), allripmodln(sigrip), 'k.','MarkerSize',24);
% plot(allmodln(sigtheta), allripmodln(sigtheta), 'cs','MarkerSize',22,'LineWidth',2);
% plot(allmodln(sigrip), allripmodln(sigrip), 'ro','MarkerSize',20,'LineWidth',2);
title(sprintf('%s: PathEq vs %s Ripple Modulation', area, statename),'FontSize',tfont,'Fontweight','normal');
xlabel(['Path Eq'],'FontSize',xfont,'Fontweight','normal');
ylabel(sprintf('%s Ripple Modln',statename),'FontSize',yfont,'Fontweight','normal');
%legend('All Units','Sig Theta Phlock',sprintf('Sig %s Ripple',statename));

[r2,p2] = corrcoef(matchpatheq, matchripmod)
% [r2rt,p2rt] = corrcoef(allmodln(allsig),allripmodln(allsig))
% [r2t,p2t] = corrcoef(allmodln(sigtheta),allripmodln(sigtheta))
% [r2r,p2r] = corrcoef(allmodln(sigrip),allripmodln(sigrip))

%xaxis = 0.5:0.1:1;
%plot(xaxis,zeros(size(xaxis)),'k--','LineWidth',2);

str = ''; rstr = ''; tstr = ''; rtstr = '';
if p2(1,2)<0.05, str = '*'; end
if p2(1,2)<0.01, str = '**'; end
if p2(1,2)<0.001, str = '***'; end
% if p2r(1,2)<0.05, rstr = '*'; end
% if p2r(1,2)<0.01, rstr = '**'; end
% if p2r(1,2)<0.001, rstr = '***'; end
% if p2t(1,2)<0.05, tstr = '*'; end
% if p2t(1,2)<0.01, tstr = '**'; end
% if p2t(1,2)<0.001, tstr = '***'; end
% if p2rt(1,2)<0.05, rtstr = '*'; end
% if p2rt(1,2)<0.01, rtstr = '**'; end
% if p2rt(1,2)<0.001, rtstr = '***'; end


% if strcmp(area,'CA1')
%     if ~strcmp(state,'sleep')
%         set(gca,'XLim',[0.55 1])
%         text(0.56,1000,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
%         text(0.56,900,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
%         text(0.56,800,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
%         text(0.56,700,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
%     else
%         set(gca,'XLim',[0.55 1])
%         text(0.8,1450,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
%         text(0.8,1300,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
%         text(0.8,1200,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
%         text(0.8,1100,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
%     end
% else
%     if ~strcmp(state,'sleep');
%         set(gca,'XLim',[0.5 1]);
%         %set(gca,'YLim',[-120 200]); % Modln peak
%         %set(gca,'YLim',[-80 100]); % Modln
%         text(0.55,40,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
%         text(0.82,35,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
%         text(0.82,30,sprintf('Signf in Rip: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
%         text(0.82,25,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
%         text(0.82,20,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
%         text(0.82,5,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
%         text(0.72,5,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
%         text(0.92,5,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');
%
%
%     else
%         set(gca,'XLim',[0.5 1.]);
%         %set(gca,'YLim',[-120 500]); % Modln peak
%         % set(gca,'YLim',[-80 250]); % Modln
%         text(0.6,40,sprintf('Total Valid Cells: %d', length(allsigshuf)),'FontSize',30,'Fontweight','normal');
%         text(0.75,35,sprintf('Signf in both: %d',length(sigboth)),'FontSize',30,'Fontweight','normal');
%         text(0.75,30,sprintf('Signf in Sleep SWR: %d',length(sigrip)),'FontSize',30,'Fontweight','normal');
%         text(0.75,25,sprintf('Signf in Theta: %d',length(sigtheta)),'FontSize',30,'Fontweight','normal');
%         text(0.75,20,sprintf('R = %0.2f%s',r2(1,2), str),'FontSize',30,'Fontweight','normal');
%         text(0.75,5,sprintf('Rr = %0.2f%s',r2r(1,2), rstr),'FontSize',30,'Fontweight','normal');
%         text(0.65,5,sprintf('Rt = %0.2f%s',r2t(1,2), tstr),'FontSize',30,'Fontweight','normal');
%         text(0.85,5,sprintf('Rrt = %0.2f%s',r2rt(1,2), rtstr),'FontSize',30,'Fontweight','normal');
%
%
%     end
% end

figfile = [figdir,area,sprintf('_PathEqVs%sRipModln',statename)]
if savefig1==1,
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end




% Do a shuffle test - both a normal shuffle test, and a regression shuffle
% ------------------------------------------------------------------------

rshuf=[]; pshuf=[];
for i = 1:1000
    rorder = randperm(length(allmodln));
    randmodln = allmodln(rorder);
    [rtmp, ptmp] = corrcoef(randmodln,allripmodln);
    rshuf(i) = rtmp(1,2); pshuf(i) = ptmp(1,2);
end

length(find(pshuf<0.05));
prctile95 = prctile(rshuf,95); prctile99 = prctile(rshuf,99);
if r2(1,2)>prctile95,
    Sig95 = 1,
else
    Sig95 = 0,
end
if r2(1,2)>prctile99,
    Sig99 = 1,
else
    Sig99 = 0,
end



% Regression
% -----------
[b00,bint00,r00,rint00,stats00] = regress(allripmodln', [ones(size(allmodln')) allmodln']);
xpts = min(allmodln):0.01:max(allmodln);
bfit00 = b00(1)+b00(2)*xpts;
plot(xpts,bfit00,'k-','LineWidth',4);  % Theta vs Rip
rsquare = stats00(1);

% % Regression for only SWR modulated cells
% % --------------------------------------------
% [b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigrip)', [ones(size(allmodln(sigrip)')) allmodln(sigrip)']);
% xpts = min(allmodln):0.01:max(allmodln);
% bfit00 = b00(1)+b00(2)*xpts;
% plot(xpts,bfit00,'r-','LineWidth',4);  % Theta vs Rip - Only SWR significant
% rsquare = stats00(1);
%
% % Regression for only theta modulated cells
% % --------------------------------------------
% [b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigtheta)', [ones(size(allmodln(sigtheta)')) allmodln(sigtheta)']);
% xpts = min(allmodln):0.01:max(allmodln);
% bfit00 = b00(1)+b00(2)*xpts;
% plot(xpts,bfit00,'c-','LineWidth',4);  % Theta vs Rip - Only Theta significant
% rsquare = stats00(1);
%
% % Regression for both modulated cells
% % --------------------------------------------
% [b00,bint00,r00,rint00,stats00] = regress(allripmodln(sigboth)', [ones(size(allmodln(sigboth)')) allmodln(sigboth)']);
% xpts = min(allmodln):0.01:max(allmodln);
% bfit00 = b00(1)+b00(2)*xpts;
% plot(xpts,bfit00,'b-','LineWidth',4);  % Theta vs Rip - Both SWR and Theta significant
% rsquare = stats00(1);



% Do regression after shifting data to make intercept 0?
% ------------------------------------------------------
allripmodln_0 = allripmodln-mean(allripmodln);
allmodln_0 = allmodln-mean(allmodln);
[b0,bint0,r0,rint0,stats0] = regress(allripmodln_0',[ones(size(allmodln_0')) allmodln_0']);
bfit0 = b0(1)+b0(2)*xpts;

rshuffle = []; pshuffle = []; rsquare_shuffle = []; psquare_shuffle = []; b_shuffle = [];

% % Shuffling
% % ---------
for n=1:1000
    rorder = randperm(length(allmodln));
    randmodln = allmodln(rorder);
    % Get corrcoeff of shuffle
    [rsh,psh] = corrcoef(randmodln, allripmodln);
    r_shuffle(n) = rsh(1,2); p_shuffle(n) = psh(1,2);
    % Get regression of shuffle after making intercept 0 / Or Not
    %shuffle_0 = shuffle - mean(shuffle);
    %[bsh,bintsh,rsh,rintsh,statssh] = regress(shuffle_0', [ones(size(allmodln_0')) allmodln_0']);
    [bsh,bintsh,rsh,rintsh,statssh] = regress(allripmodln', [ones(size(randmodln')) randmodln']);
    rsquare_shuffle(n) = statssh(1); preg_shuffle(n) = statssh(3);
    b_shuffle(n,:) = bsh;
end

% Significance from shuffle
prctile(rsquare_shuffle,99); prctile(r_shuffle,99); %figure; hist(r_shuffle,50); hist(rsquare_shuffle,50);
pshuf2 = length(find(r2(1,2)<r_shuffle))/n

% Get regression corresponding to 99 percentile
idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,99));
idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
plot(xpts,bfitsh,'k--','LineWidth',2);  % Theta vs Rip - 99% shuffle line
%
% % and 95 %tile
% idxs=find(rsquare_shuffle>=prctile(rsquare_shuffle,95));
% idx=idxs(find(rsquare_shuffle(idxs)==min(rsquare_shuffle(idxs))));
% bfitsh = b_shuffle(idx,1)+b_shuffle(idx,2)*xpts;
% plot(xpts,bfitsh,'g--','LineWidth',2);  % Theta vs Rip - 95% shuffle line






















