function [out, out_decodedata]= sj_calcepochreplaystats(index, trainingindex, trainingfilter, decodefilter,varargin)

animal = index(1);
dd = index(2);
epoch = index(3);
trajmapping = [1 1 2 2];
binsize = .015; %default temporal bin
out = [];
out_decodedata = [];
for option = 1:2:length(varargin)-1
    if isstr(varargin{option})
        switch(varargin{option})
            case 'binsize'
                binsize = varargin{option+1};
            otherwise
                error(['Option ',varargin{option},' unknown.']);
        end
    else
        error('Options must be strings, followed by the variable');
    end
end

for eventindex = 1:length(decodefilter(animal).output{dd}(epoch).eventdata)
    disp(eventindex)
    trainingdata = [];
    spikedata = [];
    decodedata = [];
    indexlist = [];
    activespiketimes = [];
    activerates = [];
    
    %pick out all the matching cells from the training data and the
    %decoding data
    %traindata contains linear rates, and is n by x, where n is the
    %number of cells and x is the number of spatial bins
    %spikedata contains spikecounts, and is n by t, where t is the
    %number of temporal bins in the data to be decoded.
    matches = rowfind(trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).index(:,[1 3 4]),decodefilter(animal).output{dd}(epoch).index(:,[1 3 4])); %find the matching cell indices
    startevent = decodefilter(animal).output{dd}(epoch).eventtime(eventindex,1);
    endevent = decodefilter(animal).output{dd}(epoch).eventtime(eventindex,2);
    %if ((endevent-startevent) < 2) % length of reply < 2 sec - Dont need this
    timebins = startevent:binsize:endevent;
    eventcellsactive = [];
    activecount = 0;
    for trainingcell = 1:length(matches)
        if (matches(trainingcell) > 0) %we have a match
            indexlist = [indexlist; trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).index(trainingcell,:)];
            trainingdata = [trainingdata; trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).rates(trainingcell,:)];
            
            tmpspiketimes = decodefilter(animal).output{dd}(epoch).eventdata(eventindex).spiketimes(find(decodefilter(animal).output{dd}(epoch).eventdata(eventindex).cellindex == matches(trainingcell)));
            %save all the info for the active cells
            if ~isempty(tmpspiketimes)
                activecount = activecount+1;
                activespiketimes{activecount} = tmpspiketimes;
                activerates = [activerates; trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).rates(trainingcell,:)];
            end
            
        end
    end
    trainingdata = trainingdata*binsize; %transform rates to expected number of spikes
    activerates = activerates*binsize;
    
    if (length(activespiketimes) >= 4)
        %out = [out; calcReplayStats(activespiketimes,activerates,timebins,trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).dist)];
        %out = [out; [calcReplayStats(activespiketimes,activerates,timebins,trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).dist) decodefilter(animal).output{dd}(epoch).eventimmobiletime(eventindex) length(activespiketimes) ]];  %decodefilter(animal).output{1}(epochnum).std(eventindex)
        %out = [out; [decodefilter(animal).output{dd}(epoch).eventimmobiletime(eventindex) length(activespiketimes) ]];  %decodefilter(animal).output{dd}(epoch).std(eventindex)
        %out = [out;index(1)];
        
        %out = [out; [calcReplayStats(activespiketimes,activerates,timebins,trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).dist) length(activespiketimes) eventindex]];
        tmpout= sj_calcReplayStats(activespiketimes,activerates,timebins,trainingfilter(animal).output{trainingindex(1)}(trainingindex(2)).dist);
        tmpoutstats = tmpout.stats;
        out = [out; [tmpoutstats length(activespiketimes) length(timebins) eventindex]];
        out_decodedata{eventindex} = tmpout.decodedata;
    end
    %end
end