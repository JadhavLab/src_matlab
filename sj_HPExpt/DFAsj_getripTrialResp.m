function out = DFAsj_getreplay_PFCresp(indices, excludetimes, spikes, ripples, tetinfo, varargin)

% GET PFC resp for ripples/ cand replay events gpotten simlar to "sj_getpopulationevents2"
% FOr response, do similar to ripalign code

tetfilter = '';
dospeed = 0;
excludetimes = [];

tetfilter = '';
excludetimes = [];
maxcell = 0;
minstd = 3;
lowsp_thrs = 5; %cm/sec
highsp_thrs = lowsp_thrs;
dospeed = 0;
cellcountthresh = 5;

for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'minstd'
            minstd = varargin{option+1};
        case 'maxcell'
            maxcell = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        case 'lowsp_thrs'
            lowsp_thrs = varargin{option+1};
        case 'cellcountthresh'
            cellcountthresh = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
    
end


index = [indices(1,1) indices(1,2)];
day = index(1);
epoch = index(2);

posdata = pos{index(1)}{index(2)}.data;
statematrix = linpos{index(1)}{index(2)}.statematrix;

% Get riptimes
% -------------
if isempty(tetfilter)
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', '(isequal($descrip, ''riptet''))','minstd',minstd);
else
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', tetfilter, 'minstd', minstd);
end

out.index = [];
out.eventtime = [];

spikecounts = [];
celldata = [];

if ~isempty(riptimes)
    %go through each cell and calculate the binned spike counts
    for cellcount = 1:size(indices,1)
        
        index = indices(cellcount,:);
        if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)}.data)
            spiketimes = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);
        else
            spiketimes = [];
        end
        %Find valid spikes
        spiketimes = spiketimes(find(~isExcluded(spiketimes, excludeperiods)));
        spikebins = periodAssign(spiketimes, riptimes(:,[1 2]));
        
        %spikebins = lookup(spiketimes,riptimes(:,2));
        if ~isempty(spiketimes)
            validspikes = find(spikebins);
            spiketimes = spiketimes(validspikes);
            spikebins = spikebins(validspikes);
            
            %             spikedeviation = abs(spiketimes - riptimes(spikebins,2));
            %             validspikes = find(spikedeviation <= (window/2));
            %             spiketimes = spiketimes(validspikes);
            %             spikebins = spikebins(validspikes);
        end
        
        if ~isempty(spiketimes)
            tmpcelldata = [spiketimes spikebins];
            tmpcelldata(:,3) = cellcount;
        else
            tmpcelldata = [0 0 cellcount];
        end
        %tmpcelldata = [spiketimes spikebins];
        %tmpcelldata(:,3) = cellcount;
        
        celldata = [celldata; tmpcelldata];
        spikecount = zeros(1,size(riptimes,1));
        for i = 1:length(spikebins)
            spikecount(spikebins(i)) = spikecount(spikebins(i))+1;
        end
        
        spikecounts = [spikecounts; spikecount];
        out.index = [out.index; index];
    end
    celldata = sortrows(celldata,1); %sort all spikes by time
    
    %newtimebinsInd = find(~isExcluded(timebins, excludeperiods));
    %newtimes = timebins(newtimebinsInd);
    
    cellcounts = sum((spikecounts > 0));
    %cellcounts = cellcounts(newtimebinsInd);
    try
        eventindex = find(cellcounts >= cellcountthresh);
    catch
        index, keyboard;
    end

     for event = 1:length(eventindex)
        out.eventtime(event,1:2) = riptimes(eventindex(event),[1 2]);
     end
    
end

    
    
    


trialResps = ripplemod{index(1)}{index(2)}{index(3)}{index(4)}.trialResps;
out.index = index;
out.trialResps = trialResps;



