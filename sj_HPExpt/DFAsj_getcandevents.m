function out = DFAsj_getcandevents(indices, excludetimes, spikes, ripples, tetinfo, pos, varargin)
% Use tetinfo and tetfilter passed in, or redefine here to get riptets
% Then use ripples to getriptimes. Can Use inter-ripple-interval of 1 sec, and use a low-speed criterion.


tetfilter = '';
excludetimes = [];
maxcell = 0;
minstd = 3;
lowsp_thrs = 4; %cm/sec
highsp_thrs = lowsp_thrs;
dospeed = 0;
cellcountthresh = [];  % Can be used to parse ripples
minrip=1;

% % For ripple trigger
% % ------------------
% binsize = 10; % ms
% pret=550; postt=550; %% Times to plot
% push = 500; % either bwin(2) or postt-trim=500. For jittered trigger in background window
% trim = 50;
% smwin=10; %Smoothing Window - along y-axis for matrix. Carry over from ...getrip4
% rwin = [0 200];
% bwin = [-500 -300];
% push = 500; % either bwin(2) or postt-trim=500. If doing random events. See ... getrip4


for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludetimes'
            excludetimes = varargin{option+1};
        case 'minstd'
            minstd = varargin{option+1};
        case 'minrip'
            minrip = varargin{option+1};
        case 'maxcell'
            maxcell = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        case 'lowsp_thrs'
            lowsp_thrs = varargin{option+1};
        case 'cellcountthresh'
            cellcountthresh = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
end

index = [indices(1,1) indices(1,2)];
day = index(1);
epoch = index(2);

% Get riptimes
% -------------
if isempty(tetfilter)
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', '(isequal($descrip, ''riptet''))','minstd',minstd,'minrip',minrip);
else
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', tetfilter, 'minstd', minstd,'minrip',minrip);
end


% ISI of 1 sec and speed criterion - if needed
% Get triggers as rip starttimes separated by at least 1 sec
% ----------------------------------------------------------
rip_starttime = 1000*riptimes(:,1);  % in ms

% Find ripples separated by atleast a second
% --------------------------------------------
iri = diff(rip_starttime);
keepidx = [1;find(iri>=1000)+1];
rip_starttime = rip_starttime(keepidx);
riptimes = riptimes(keepidx,:); 


% % Implement speed criterion - Keep.
% % ----------------------------------------
if dospeed
    absvel = abs(pos{day}{epoch}.data(:,5)); % Can also use field 9
    postime = pos{day}{epoch}.data(:,1); % in secs
    pidx = lookup(rip_starttime,postime*1000);
    speed_atrip = absvel(pidx);
    lowsp_idx = find(speed_atrip <= lowsp_thrs);
    highsp_idx = find(speed_atrip > highsp_thrs);
    
    rip_starttime = rip_starttime(lowsp_idx);
    riptimes = riptimes(lowsp_idx,:);
end


%Cellcountthresh for each event as in getpopulationevents2 or  sj_HPexpt_ripalign_singlecell_getrip4
% -----------------------------------

excludeperiods = excludetimes;
spikecounts = [];
celldata = [];
out.index = [];

if ~isempty(riptimes)
    %go through each cell and calculate the binned spike counts
    for cellcount = 1:size(indices,1)

        index = indices(cellcount,:);
        if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)}.data)
            spiketimes = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);
        else
            spiketimes = [];
        end
        %Find valid spikes
        spiketimes = spiketimes(find(~isExcluded(spiketimes, excludeperiods)));
        spikebins = periodAssign(spiketimes, riptimes(:,[1 2]));
        
        if ~isempty(spiketimes)
            validspikes = find(spikebins);
            spiketimes = spiketimes(validspikes);
            spikebins = spikebins(validspikes);
        end
        
        if ~isempty(spiketimes)
            tmpcelldata = [spiketimes spikebins];
            tmpcelldata(:,3) = cellcount;
        else
            tmpcelldata = [0 0 cellcount];
        end
        celldata = [celldata; tmpcelldata];
        spikecount = zeros(1,size(riptimes,1));
        for i = 1:length(spikebins)
            spikecount(spikebins(i)) = spikecount(spikebins(i))+1;
        end

        spikecounts = [spikecounts; spikecount];
        out.index = [out.index; index];
    end
    %Sort all spikes by time
    celldata = sortrows(celldata,1);

    cellcounts = sum((spikecounts > 0));
    %Find all events with enough cells
    eventindex = find(cellcounts >= cellcountthresh);
    Ncounts = cellcounts(eventindex);
    
    % Number of spikes per cell
    MeanSpksPerCell_allripples = mean(spikecounts,2); % column vector with one entry for each cells
    NSpksPerCell_allripples = spikecounts(spikecounts>0); % each entry is no of spikes for a cell in a ripple, collapsed across all cells in session 
      
    validcounts = spikecounts(:,eventindex); % No. of spikes per cell, keeping only valid events
    MeanSpksPerCell_candripples = mean(validcounts,2); % column vector with one entry for each cells
    NSpksPerCell_candripples = validcounts(validcounts>0);  % each entry is no of spikes for a cell in a candidate ripple, collapsed across all cells in session 
   
    riptimes_keep = riptimes(eventindex);
end
     

% Output
% ------

out.Ncandevents = length(eventindex); % No. of candidate events
out.Ncounts = Ncounts; % No. of cells active in the valid events

out.Ncells = size(indices,1);
out.MeanSpksPerCell_allripples = MeanSpksPerCell_allripples;
out.NSpksPerCell_allripples = NSpksPerCell_allripples;
out.MeanSpksPerCell_candripples = MeanSpksPerCell_candripples;
out.NSpksPerCell_candripples = NSpksPerCell_candripples;








