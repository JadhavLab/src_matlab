
% Instead of decoding PFC replay as in DFSsj_HPexpt_getreplaydata_withPFC,
% get PFC neuron responses aligned to different kinds of CA1 replay

% Older comments
% Add PFC responses to DFSsj_HPexpt_getreplaydata
% Based on DFSsj_HPexpt_replay. See documentation on replay decoding
% Replay decoding, simlar to mcarr/runrippledecoding or mkarlsso/populationdecoding_forloren

clear; %close all;
runscript = 0;
savedata = 0; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/Replay2015/';


% With Speed criterion - Version 4 onwards
%--------------------------------------
val=1; cellcountthresh=2; savefile = [savedir 'HP_replaydata_thrs2_PFCallHIST_X6'];
%val=2; cellcountthresh=2; savefile = [savedir 'HP_replaydata_thrs2_PFCexc'];
%val=3; cellcountthresh=2; savefile = [savedir 'HP_replaydata_thrs2_PFCinh'];
%val=4; cellcountthresh=2; savefile = [savedir 'HP_replaydata_thrs2_PFCneu'];

savefig1=0;


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Ndl','Rtl'};
    %animals = {'Rtl'};
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    % dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    epochfilter = runepochfilter;
    
    %runepochfilter = 'isequal($type, ''run'')'; EASIER TO DO THIS
    %sleepepochfilter = 'isequal($type, ''sleep'')'; % Only pre and post sleep marked as sleep
    
    % %Cell filter
    % %-----------
    % %PFC
    % %----
    % && strcmp($thetamodtag, ''y'')
    
    % All CA1 cells. Function will parse them out.
    cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7)) ';
    
    % PFC cells. Function will parse them out.
    switch val
        case 1
            % Run all PFC cells, including exc, inh and no resp together - Parse out later.
            PFCcellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100)) '
            allcellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100))'
        case 2
            % OR, Separate while calling itself
            PFCcellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc''))'
            allcellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''exc'') ) '
            
        case 3
            % OR, Separate while calling itself
            PFCcellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh''))'
            allcellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($ripmodtype3, ''inh'') ) '
            
        case 4
            % OR, Separate while calling itself
            PFCcellfilter = '(strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''n'')) '
            allcellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''n'') ) '
            
    end
    
    
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % Iterator
    % --------
    iterator = 'multicellanal';
    
    
    %% RUN TRAINING FILTER
    % -------------------
    timefilter_place = { {'DFTFsj_getvelpos', '(($absvel >= 5))'},...
        {'DFTFsj_getriptimes','($nripples == 0)','tetfilter',riptetfilter,'minthresh',3} };
    
    %create training data by calulating the linearized rates of all cells
    trainingfilter = createfilter('animal',animals,'epochs',epochfilter,'cells',cellfilter,'excludetime', timefilter_place,'iterator',iterator);
    
    disp('Done CA1 Training Filter Creation');
    
    %Options set: bin size = 2 cm, peakthresh = 3Hz / std above the mean
    trainingfilter = setfilterfunction(trainingfilter, 'sj_calcpopulationlinfields', {'spikes','linpos'},2,3);
    % Could also just pass linfields instead of calculating again, but need statematrix info from linpos as well
    trainingfilter = runfilter(trainingfilter);
    
    disp('Finished running CA1 training filter script');
    
    
    %% RUN PFC TRAINING FILTER
    % -------------------
    % This not necessary anymore, but keep it intact, so that all decoding data also passes along
    % What is Needed is to use "linfields" to get a ArmPrefIndex for each PFC cell
    % Add later, or can be done separately from this code
    
    %create training data by calulating the linearized rates of all cells
    PFCtrainingfilter = createfilter('animal',animals,'epochs',epochfilter,'cells',PFCcellfilter,'excludetime', timefilter_place,'iterator',iterator);
    
    disp('Done PFC Training Filter Creation');
    
    %Options set: bin size = 2 cm, peakthresh = 3Hz / std above the mean
    PFCtrainingfilter = setfilterfunction(PFCtrainingfilter, 'sj_calcpopulationlinfields', {'spikes','linpos'},2,3);
    % Could also just pass linfields instead of calculating again, but need statematrix info from linpos as well
    PFCtrainingfilter = runfilter(PFCtrainingfilter);
    
    disp('Finished running PFC training filter script');
    
    
    %% RUN DECODING FILTER FOR RIPPLES DURING RUNS
    % -------------------------
    
    % Timefilter for ripple in function. Can use a speed filter as well. Both CA1 and PFC cells together
    % THIS HAS BEEN CHANGED, to also return PFC responses to Events in Histogram Form
    % Will Be Carried Forward in evaluate_replaydata_epoch for classifying PFC responses
    
    % Filter creation
    % ----------------
    decodefilter = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        allcellfilter, 'iterator', iterator);
    
    disp('Done Decode Filter Creation');
    
    % Set analysis function
    % ---------------------
    
    %switch val
    %    case {1,2}
    decodefilter = setfilterfunction(decodefilter,'sj_getpopulationevents2_withPFCHist',{'spikes', 'linpos', 'pos', 'ripples','tetinfo','cellinfo'},'cellcountthresh',cellcountthresh,'dospeed',1,'lowsp_thrs',4,'minrip',1,'minstd',3);
    %end
    
    % Default stdev for ripples is 3. You will use tetinfo to get ripples
    
    % Run analysis
    % ------------
    decodefilter = runfilter(decodefilter);
    disp('Finished running decode filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript savedata
        save(savefile, '-v7.3');
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end



% -------------------------  Filter Format Done -------------------------

%% DETERMINE WHICH EVENTS ARE REPLAY EVENTS


% SJ - training filter epochs and decodefilter epochs are the same for me


% %Run only for some epochs with this option. eg. HPa day 1 both epochs. an=1; e=1:2
% %-------------------------------------
% an=1; usee=1:2; d=1;
% for ee=1:length(usee)
%     e=usee(ee)
%     decode_dayep = decodefilter(an).epochs{d}(e,:)
%     training_e = e; % SJ - training filter epochs and decodefilter epochs are the same for me
%     [decodefilter]= sj_evaluate_replaydata_epoch([an d e], [d training_e], trainingfilter, decodefilter);
% end
%
% savefile = [savedir 'HPa_day1_replaydata_proc'];
% clear figopt1 runscript savedata
% save(savefile, '-v7.3');
% keyboard;


do_decodedtraj = 1;
%Run for entire data
%-------------------

if do_decodedtraj == 0
    for an = 1:length(decodefilter)
        for d = 1:length(decodefilter(an).output)
            for e = 1:length(decodefilter(an).output{d})
                disp(sprintf('Anim %d Day %d Epoch %d', an, d, e));
                decode_dayep = decodefilter(an).epochs{d}(e,:)
                % SJ - training filter epochs and decodefilter epochs are the same for me
                training_e = e;
                if ~isempty(decodefilter(an).output{d}(e).index)
                    [decodefilter]= sj_evaluate_replaydata_epoch_withPFCHist([an d e], [d training_e], trainingfilter, PFCtrainingfilter, decodefilter);
                end
            end
        end
    end
    
    
    savedir = '/data25/sjadhav/HPExpt/ProcessedData/Replay2015/';
    switch val
        case 1
            savefile = [savedir 'HP_replaydata_PFCallHist_proc_X6']
            
        case 2
            savefile = [savedir 'HP_replaydata_PFCexcHist_proc']
            
        case 3
            savefile = [savedir 'HP_replaydata_PFCinhHist_proc']
            
        case 4
            savefile = [savedir 'HP_replaydata_PFCneuHist_proc']
            
    end
    
else % do_decodedtraj = 1, just write a new function
    
    for an = 1:length(decodefilter)
        for d = 1:length(decodefilter(an).output)
            for e = 1:length(decodefilter(an).output{d})
                disp(sprintf('Anim %d Day %d Epoch %d', an, d, e));
                decode_dayep = decodefilter(an).epochs{d}(e,:)
                % SJ - training filter epochs and decodefilter epochs are the same for me
                training_e = e;
                if ~isempty(decodefilter(an).output{d}(e).index)
                    %[decodefilter]= sj_evaluate_replaydata_epoch_withPFCHist_decodetraj([an d e], [d training_e], trainingfilter, PFCtrainingfilter, decodefilter);
                    %[decodefilter]= sj_evaluate_replaydata_epoch_withPFCHist_decodetraj_besttraj([an d e], [d training_e], trainingfilter, PFCtrainingfilter, decodefilter);
                    [decodefilter]= sj_evaluate_replaydata_epoch_withPFCHist_decodetraj_besttraj_st([an d e], [d training_e], trainingfilter, PFCtrainingfilter, decodefilter);
                end
            end
        end
    end
    
    savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/Replay2015/';
    %savefile = [savedir 'HP_replaydata_PFCallHist_proc_DecodeTraj_X6'];
    %savefile = [savedir 'HP_replaydata_PFCallHist_proc_DecodeTraj_BestTraj_X6'];
    savefile = [savedir 'HP_replaydata_PFCallHist_proc_DecodeTraj_BestTraj_Stats5_X6'];
end

%save(savefile);
%save(savefile,'decodefilter');

clear figopt1 runscript savedata
save(savefile, '-v7.3');

keyboard;


% Plotting Summary - Old one for decoding responses
% --------------------------------------------------
cntsess=0; corrval=[]; corrsig = [];
for an = 1:length(decodefilter)
    for d = 1:length(decodefilter(an).output)
        for e = 1:length(decodefilter(an).output{d})
            disp(sprintf('Anim %d Day %d Epoch %d', an, d, e));
            decode_dayep = decodefilter(an).epochs{d}(e,:)
            
            cntsess = cntsess+1;
            CA1idx{cntsess} = decodefilter(an).output{d}(e).replay_arm_index1;
            PFCidx{cntsess} = decodefilter(an).output{d}(e).PFCreplay_arm_index1;
            
            useCA1 = find(~isnan(CA1idx{cntsess}));
            usePFC = find(~isnan(PFCidx{cntsess}));
            useCA1PFC = intersect(useCA1,usePFC);
            length(useCA1PFC)
            
            if length(useCA1PFC)>=10
                [val, sig] = corr(CA1idx{cntsess}(useCA1PFC), PFCidx{cntsess}(useCA1PFC));
                corrval = [corrval; val];
                corrsig = [corrsig; sig];
            end
            
        end
    end
end














% % ----------------------------------
% % Whether to gather data or to load previously gathered data
% % --------------------------------------------------------------------
% gatherdata = 1; savegatherdata = 1;
% switch val
%     case 1
%         gatherdatafile = [savedir 'HP_replaydecode_gather'];
%     case 2
%         gatherdatafile = [savedir 'HP_candevents4_gather'];
% end
%
%
%
%
%
%
% if gatherdata
%     % Parameters if any
%     % -----------------
%     % -------------------------------------------------------------
%
%     cnt=0; % counting epochs
%     anim_index=[]; allNcandevents=[]; allNcounts=[]; allNcountsvec = [];
%
%
%
%     for an = 1:length(modf)
%         for i=1:length(modf(an).output{1})
%
%             cnt=cnt+1;
%
%             % Variables for entire epoch
%             anim_index{an}{i} = modf(an).output{1}(i).index;
%             allNcandevents(cnt) = modf(an).output{1}(i).Ncandevents; % How many cand events in epoch
%             allNcounts{cnt} = modf(an).output{1}(i).Ncounts; % No of active cells in each epoch
%             allNcountsvec = [allNcountsvec, modf(an).output{1}(i).Ncounts];
%         end
%     end
%
%
%     % Save
%     % -----
%     if savegatherdata == 1
%         save(gatherdatafile);
%     end
%
% else % gatherdata=0
%
%     load(gatherdatafile);
%
% end % end gather data
%
%
% % ------------
% % PLOTTING, ETC
% % ------------
%
% % ------------------
% % Population Figures
% % ------------------
%
% forppr = 0;
% % If yes, everything set to redimscreen_figforppr1
% % If not, everything set to redimscreen_figforppt1
% figdir = '/data25/sjadhav/HPExpt/Figures/ThetaMod/';
% summdir = figdir;
% set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);
%
% if forppr==1
%     set(0,'defaultaxesfontsize',16);
%     tfont = 18; % title font
%     xfont = 16;
%     yfont = 16;
% else
%     set(0,'defaultaxesfontsize',24);
%     tfont = 28;
%     xfont = 20;
%     yfont = 20;
% end
%
%
% figure; hold on;redimscreen_figforppt1;
% bar(allNcandevents);
% title(sprintf(['No. of cand events for thrs ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
% xlabel(['Epoch No'],'FontSize',24,'Fontweight','normal');
% ylabel(['N cand events'],'FontSize',24,'Fontweight','normal');
% line([16 16], [0 150], 'Color','r'); line([32 32], [0 150], 'Color','r'); line([48 48], [0 150], 'Color','r');
% line([11 11], [0 150], 'Color','c'); line([27 27], [0 150], 'Color','c'); line([43 43], [0 150], 'Color','c');
% %text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');
%
% figure; hold on;redimscreen_figforppt1;
% vec = 0:1:20;
% distr = histc(allNcountsvec,vec);
% bar(vec, distr, 'r');
% title(sprintf(['Distr No. of cells in events ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
% xlabel(['No. of cells'],'FontSize',24,'Fontweight','normal');
% ylabel(['Hist'],'FontSize',24,'Fontweight','normal');
%
%
% keyboard;
%
%
%
