function out = sj_getpopulationevents2_withPFCHist(indices, excludeperiods, spikes, linpos, pos, ripples, tetinfo, cellinfo, varargin)
% Borrow from DFAsj_getcandevents_withPFC, to also return PFC responses during SWRs.
% Called form DFSsj+HPexpt_replay_withPFC, with both CA1 and PFC as input
% For PFC, also return ripmodtag from cellinfo: ripple excited = 1, ripple inh = -1, ripp neutral = 0;

% Exactly the same as sj_getpopulationevents2_forsave?
% SJ - Combine getpopulationevents2 and mcarr/getpopulationevents. Minor differences
%Calculates the binned spike counts of all neurons in the index list.
% Get riptimes separated by atleast 1 sec

%
%spikes - the 'spikes' cell array for the day you are analyzing
%linpos - the output of LINEARDAYPROCESS for the day you are analyzing.
%indices - [day epoch tetrode cell]
%timebin- the length of each temporal bin (default 0.01 sec)
%excludeperiods - [start end] times for each exlcude period
%
%In the output, the spikecounts field is n by t, where n is the number of
%cells and t is the number of timebins.

tetfilter = '';
excludeperiods = [];
maxcell = 0;
minstd = 3;
lowsp_thrs = 4; %cm/sec
highsp_thrs = lowsp_thrs;
dospeed = 0;
cellcountthresh = [];
minrip=1;

% Parameters for PFC histogram responses
rwin = [0 200];
%bwin = [-500 -300];
pret=500; postt=500;
binsize = 10;
smwin=10; %Smoothing Window


for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'tetfilter'
            tetfilter = varargin{option+1};
        case 'excludeperiods'
            excludeperiods = varargin{option+1};
        case 'minstd'
            minstd = varargin{option+1};
        case 'minrip'
            minrip = varargin{option+1};
        case 'maxcell'
            maxcell = varargin{option+1};
        case 'dospeed'
            dospeed = varargin{option+1};
        case 'lowsp_thrs'
            lowsp_thrs = varargin{option+1};
        case 'cellcountthresh'
            cellcountthresh = varargin{option+1};
        otherwise
            error(['Option ''', varargin{option}, ''' not defined']);
    end
    
end

index = [indices(1,1) indices(1,2)];
day = index(1);
epoch = index(2);

posdata = pos{index(1)}{index(2)}.data;
statematrix = linpos{index(1)}{index(2)}.statematrix;


% Separate CA1 and PFC cells
% --------------------------
cellsi=[]; usecellsi=0; % CA1 cells
cellsp=[]; usecellsp=0; % PFC cells
PFC_ripmod = []; %ripple excited = 1, ripple inh = -1, ripp neutral = 0;


totalcells = size(indices,1);
for i=1:totalcells
    currarea = cellinfo{indices(i,1)}{indices(i,2)}{indices(i,3)}{indices(i,4)}.area;
    if strcmp(currarea,'PFC'),
        cellsp = [cellsp; day epoch indices(i,3) indices(i,4)];
        usecellsp = usecellsp+1;
        
        currriptag = cellinfo{indices(i,1)}{indices(i,2)}{indices(i,3)}{indices(i,4)}.ripmodtag3;
        currripmodtypetag = cellinfo{indices(i,1)}{indices(i,2)}{indices(i,3)}{indices(i,4)}.ripmodtype3;
        if strcmp(currriptag,'y'),  % if Ripple-modulated
            switch currripmodtypetag
                case 'exc'
                    tag = 1;
                case 'inh'
                    tag = -1;
                otherwise
                    tag = 0;
            end
        else
            tag = 0;
        end
        PFC_ripmod = [PFC_ripmod;tag];
    else
        % CA1
        cellsi = [cellsi; day epoch indices(i,3) indices(i,4)];
        usecellsi = usecellsi+1;
    end
end
nCA1cells = size(cellsi,1); nPFCcells = size(cellsp,1);


% Get riptimes
% -------------
if isempty(tetfilter)
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', '(isequal($descrip, ''riptet''))','minstd',minstd,'minrip',minrip);
else
    riptimes = sj_getripples_tetinfo(index, ripples, tetinfo, 'tetfilter', tetfilter, 'minstd', minstd,'minrip',minrip);
end

% Get triggers as rip starttimes separated by at least 1 sec
% ----------------------------------------------------------
rip_starttime = 1000*riptimes(:,1);  % in ms

% Find ripples separated by atleast a second
% --------------------------------------------
iri = diff(rip_starttime);
keepidx = [1;find(iri>=1000)+1];
rip_starttime = rip_starttime(keepidx);
riptimes = riptimes(keepidx,:);

% Implement speed criterion - Keep.
% ----------------------------------------
if dospeed
    absvel = abs(pos{day}{epoch}.data(:,5)); % Can also use field 9
    postime = pos{day}{epoch}.data(:,1); % in secs
    pidx = lookup(rip_starttime,postime*1000);
    speed_atrip = absvel(pidx);
    lowsp_idx = find(speed_atrip <= lowsp_thrs);
    highsp_idx = find(speed_atrip > highsp_thrs);
    
    rip_starttime = rip_starttime(lowsp_idx);
    riptimes = riptimes(lowsp_idx,:);
end


% riptimes = getripples(index, ripples, cellinfo, 'cellfilter', '(isequal($area, ''CA1'')|isequal($area, ''CA3''))','excludeperiods', excludeperiods,'minstd',3);
% %riptimes(:,1) = riptimes(:,2)-(window/2);
% %riptimes(:,3) = riptimes(:,2)+(window/2);


out.index = []; out.pindex = [];
out.eventtraj = [];
out.eventdist = [];
out.eventtime = [];
out.preeventcount = [];
out.eventimmobiletime = [];
out.eventdata = [];
out.peak = 0;
%out.speed = [];
out.noneventtime = []; % Non-event times
out.riptimes = riptimes; % All riptimes

spikecounts = [];  pspikecounts = [];
celldata = []; pcelldata = [];

CA1indices = cellsi; PFCindices = cellsp;
out.nCA1cells = nCA1cells;
out.nPFCcells = nPFCcells;
out.PFCindices = PFCindices;
out.PFCtype = PFC_ripmod;

if ~isempty(riptimes)
    %go through each CA1 cell and calculate the binned spike counts
    for cellcount = 1:size(CA1indices,1)
        index = CA1indices(cellcount,:);
        if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)}.data)
            spiketimes = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);
        else
            spiketimes = [];
        end
        %Find valid spikes
        spiketimes = spiketimes(find(~isExcluded(spiketimes, excludeperiods)));
        spikebins = periodAssign(spiketimes, riptimes(:,[1 2]));
        
        %spikebins = lookup(spiketimes,riptimes(:,2));
        if ~isempty(spiketimes)
            validspikes = find(spikebins);
            spiketimes = spiketimes(validspikes);
            spikebins = spikebins(validspikes);
            
            %             spikedeviation = abs(spiketimes - riptimes(spikebins,2));
            %             validspikes = find(spikedeviation <= (window/2));
            %             spiketimes = spiketimes(validspikes);
            %             spikebins = spikebins(validspikes);
        end
        
        if ~isempty(spiketimes)
            tmpcelldata = [spiketimes spikebins];
            tmpcelldata(:,3) = cellcount;
        else
            tmpcelldata = [0 0 cellcount];
        end
        %tmpcelldata = [spiketimes spikebins];
        %tmpcelldata(:,3) = cellcount;
        
        celldata = [celldata; tmpcelldata];
        spikecount = zeros(1,size(riptimes,1));
        for i = 1:length(spikebins)
            spikecount(spikebins(i)) = spikecount(spikebins(i))+1;
        end
        
        spikecounts = [spikecounts; spikecount];
        out.index = [out.index; index];
    end
    
    % Gather CA1
    % ----------
    celldata = sortrows(celldata,1); %sort all spikes by time
    
    %newtimebinsInd = find(~isExcluded(timebins, excludeperiods));
    %newtimes = timebins(newtimebinsInd);
    
    cellcounts = sum((spikecounts > 0));
    %cellcounts = cellcounts(newtimebinsInd);
    try
        eventindex = find(cellcounts >= cellcountthresh);
    catch
        index, keyboard;
    end
    
    %      % Sequence of CA1 cels in ripple events - Implemented below
    %     event_cellSeq = [];
    %     for ev = 1:length(eventindex)
    %         cellsi = celldata(find(celldata(:,2)==eventindex(ev)),3);
    %         [cellsi,ia] = unique(cellsi,'first');
    %         [~,sortorder] = sort(ia);
    %         event_cellSeq{ev} = cellsi(sortorder);
    %     end
    
    % Add non-significant events
    noneventindex = find(cellcounts < cellcountthresh);
    
    % Get other data for event
    % ------------------------
    timeindex = lookup(riptimes(:,2),posdata(:,1));
    tmpvel = posdata(:,5);
    %tmpvel = tmpvel<(max(tmpvel)*.05);
    tmpvel = tmpvel<(2);
    
    resetpoints = find(diff(tmpvel) < 0)+1;
    immobiletime = cumsum(tmpvel);
    for i = 1:length(resetpoints)
        immobiletime(resetpoints(i):end) = immobiletime(resetpoints(i):end)-immobiletime(resetpoints(i)-1);
    end
    immobiletime = immobiletime/30;
    immobile = immobiletime(timeindex);
    
    %For each event define: event time, ammount of time spent unmoving,
    %trajectory, linear distance, speed, and the times and identity of
    %cells that fired during that event
    %timeindex = lookup(riptimes(:,2),statematrix.time);
    %     if iscell(statematrix.traj)
    %         traj = statematrix.traj{6}(timeindex);
    %     else
    %         traj = statematrix.traj(timeindex);
    %     end
    traj = statematrix.traj(timeindex);
    dist = statematrix.lindist(timeindex);
    %vel = posdata(timeindex,8);
    
    for event = 1:length(eventindex)
        out.eventtime(event,1:2) = riptimes(eventindex(event),[1 2]);
        out.eventimmobiletime(event,1) = immobile(eventindex(event));
        out.eventtraj(event) = traj(eventindex(event));
        out.eventdist(event) = dist(eventindex(event));
        tmpind = find(celldata(:,2) == eventindex(event));
        out.eventdata(event).spiketimes = celldata(tmpind,1);
        out.eventdata(event).cellindex = celldata(tmpind,3);
        out.eventdata(event).cellcount = cellcounts(eventindex(event));
        
        % Sequence of CA1 cells in ripple events
        cellord = celldata(find(celldata(:,2)==eventindex(event)),3);
        [cellord,ia] = unique(cellord,'first');
        [~,sortorder] = sort(ia);
        cellord = cellord(sortorder);
        out.eventdata(event).cellSeq = CA1indices(cellord,:);
        %out.speed(event) = vel(eventindex(event));
    end
    
    for event = 1:length(noneventindex)
        out.noneventtime(event,1:2) = riptimes(noneventindex(event),[1 2]);
    end
    
    
    
    % FOR PFC cells, align spikes to eventtimes
    % Borrow from DFAsj_getreplay_PFCresp.m and DFAsj_getripPoplnResp2.m
    % ------------------------------------------------------------------
    
    
    if ~isempty(PFCindices)
        for pcellcount = 1:size(PFCindices,1)
            index = PFCindices(pcellcount,:);
            PFCtype = PFC_ripmod(pcellcount); % excited or inhibited or neutral (+1/-1/0)
            if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)}.data)
                spiketimes = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);
            else
                spiketimes = [];
            end
            %Find valid spikes
            spikeu = spiketimes(find(~isExcluded(spiketimes, excludeperiods)))*1000; % in ms
            
            trialResps=[]; rip_spks_cell=[]; rip_spkshist_cell=[];
            trialResps_nonevent=[]; rip_spks_cell_nonevent=[]; rip_spkshist_cell_nonevent=[];
            rip_spks_all=[]; rip_spkshist_all=[]; trialResps_all=[]; cntall=0;
            
            cntrip=0; nspk=0;
            % Events
            % ------
            for ii = 1:length(eventindex)
                cntrip=cntrip+1;
                currrip = out.eventtime(ii,1)*1000; %in ms
                currspks =  spikeu(find( (spikeu>=(currrip-pret)) & (spikeu<=(currrip+postt)) ));
                nspk = nspk + length(currspks);
                currspks = currspks-(currrip);
                histspks = histc(currspks,[-pret:binsize:postt]);
                rip_spks_cell{cntrip}=currspks;
                rip_spkshist_cell(cntrip,:)=histspks;
                % Get no of spikes in response window, and back window
                trialResps(cntrip) =  length(find(currspks>=rwin(1) & currspks<=rwin(2)));
                
                cntall = cntall+1;
                rip_spks_all{cntall}=currspks;
                rip_spkshist_all(cntall,:)=histspks;
                trialRespsall(cntall) = length(find(currspks>=rwin(1) & currspks<=rwin(2)));
            end
            
            % Non-events
            % ----------
            cntrip=0; nspk=0;
            for ii = 1:length(noneventindex)
                cntrip=cntrip+1;
                currrip = out.noneventtime(ii,1)*1000; %in ms
                currspks =  spikeu(find( (spikeu>=(currrip-pret)) & (spikeu<=(currrip+postt)) ));
                nspk = nspk + length(currspks);
                currspks = currspks-(currrip);
                histspks = histc(currspks,[-pret:binsize:postt]);
                rip_spks_cell_nonevent{cntrip}=currspks;
                rip_spkshist_cell_nonevent(cntrip,:)=histspks;
                % Get no of spikes in response window, and back window
                trialResp_nonevent(cntrip) =  length(find(currspks>=rwin(1) & currspks<=rwin(2)));
                
                cntall = cntall+1;
                rip_spks_all{cntall}=currspks;
                rip_spkshist_all(cntall,:)=histspks;
                trialResps_all(cntall) = length(find(currspks>=rwin(1) & currspks<=rwin(2)));
            end
            
            % Save for current PFC cell in output
            out.eventPFCresp.rip_spks_cell{pcellcount} = rip_spks_cell; % Each entry is a cell array
            out.eventPFCresp.rip_spkshist_cell{pcellcount} = rip_spkshist_cell; % Each entry is a matrix
            out.eventPFCresp.trialResps{pcellcount} = trialResps; % Each entry is a vector
            out.eventPFCresp.rip_spks_cell_nonevent{pcellcount} = rip_spks_cell; % Each entry is a cell array
            out.eventPFCresp.rip_spkshist_cell_nonevent{pcellcount} = rip_spkshist_cell; % Each entry is a matrix
            out.eventPFCresp.trialResps_nonevent{pcellcount} = trialResps; % Each entry is a vector
            out.eventPFCresp.rip_spks_all{pcellcount} = rip_spks_all; % Each entry is a cell array
            out.eventPFCresp.rip_spkshist_all{pcellcount} = rip_spkshist_all; % Each entry is a matrix
            out.eventPFCresp.trialResps_all{pcellcount} = trialResps_all; % Each entry is a vector
        end
        
    else

        out.eventPFCresp.rip_spks_cell = [];
        out.eventPFCresp.rip_spkshist_cell = [];
        out.eventPFCresp.trialResps = [];
        out.eventPFCresp.rip_spks_cell_nonevent = [];
        out.eventPFCresp.rip_spkshist_cell_nonevent = [];
        out.eventPFCresp.trialResps_nonevent = [];
        out.eventPFCresp.rip_spks_all = [];
        out.eventPFCresp.rip_spkshist_all = [];
        out.eventPFCresp.trialResps_all = [];
        
    end
   
    
    % for PFC cells, return similar parameters as CA1 for each event. 
    % This is for decoding, if you decide to go that route
    % -----------------------------------------------------
    
    if ~isempty(PFCindices)
        
        for pcellcount = 1:size(PFCindices,1)
            
            index = PFCindices(pcellcount,:);
            PFCtype = PFC_ripmod(pcellcount); % excited or inhibited or neutral
            
            if ~isempty(spikes{index(1)}{index(2)}{index(3)}{index(4)}.data)
                spiketimes = spikes{index(1)}{index(2)}{index(3)}{index(4)}.data(:,1);
            else
                spiketimes = [];
            end
            %Find valid spikes
            spiketimes = spiketimes(find(~isExcluded(spiketimes, excludeperiods)));
            spikebins = periodAssign(spiketimes, riptimes(:,[1 2]));
            
            if ~isempty(spiketimes)
                validspikes = find(spikebins);
                spiketimes = spiketimes(validspikes);
                spikebins = spikebins(validspikes);
            end
            
            if ~isempty(spiketimes)
                tmpcelldata = [spiketimes spikebins];
                tmpcelldata(:,3) = pcellcount;
                tmpcelldata(:,4) = PFCtype;
            else
                tmpcelldata = [0 0 pcellcount 0];
            end
            pcelldata = [pcelldata; tmpcelldata];
            pspikecount = zeros(1,size(riptimes,1));
            for i = 1:length(spikebins)
                pspikecount(spikebins(i)) = pspikecount(spikebins(i))+1;
            end
            
            pspikecounts = [pspikecounts; pspikecount];
            out.pindex = [out.pindex; index];
        end
    end
    
    % Gather PFC
    % ----------
    %Sort all spikes by time
    if ~isempty(pcelldata)
        pcelldata = sortrows(pcelldata,1);
    end
    pcellcounts = sum((pspikecounts > 0),1); % For each event, get no. of cells with at least 1 spike
    
    
    % eventindex is based on number of cells in event. PFC just follows
    for event = 1:length(eventindex)
        if ~isempty(pcelldata)
            tmpind = find(pcelldata(:,2) == eventindex(event));
            out.eventdata(event).pspiketimes = pcelldata(tmpind,1);
            out.eventdata(event).pcellindex = pcelldata(tmpind,3);
            out.eventdata(event).pcellcount = pcellcounts(eventindex(event));
            % This will assign for each spike - exc=1, inh=-1, neutr=0
            % Ideally should be for each
            out.eventdata(event).PFCtype = pcelldata(tmpind,4);
            
            % Sequence of PFC cells in ripple events
            cellord = pcelldata(find(pcelldata(:,2)==eventindex(event)),3);
            [cellord,ia] = unique(cellord,'first');
            [~,sortorder] = sort(ia);
            cellord = cellord(sortorder);
            out.eventdata(event).pcellSeq = PFCindices(cellord,:);
        else
            out.eventdata(event).pspiketimes=[];
            out.eventdata(event).pcellindex=[];
            out.eventdata(event).pcellcount=[];
            out.eventdata(event).PFCtype=[];
            out.eventdata(event).pcellSeq=[];
        end
    end    
    
    
    
    
else
    warning('No ripples found')
end



function out = periodAssign(times, periods)
% out = periodAssign(times, periods)
% TIMES is a vector of times
% PERIODS is an N by 2 list of start and end times
% Returns the index of the period that each time falls into.  If a time
% does not fall into one of the periods, a zero is returned.
% This function assumes that the periods are not overlapping.
%

if ~isempty(periods)
    oneborder = [(periods(:,1)-.0000001);periods(:,2)+.0000001];
    oneborder(:,2) = 0;
    insideborder = [(periods(:,1)+.0000001) (1:length(periods))'; (periods(:,2)-.0000001) (1:length(periods))'];
    sortedMatrix = [[-inf 0]; sortrows([oneborder;insideborder],1); [inf 0]];
else
    sortedMatrix = [[-inf 0]; [inf 0]];
end
out = sortedMatrix(lookup(times,sortedMatrix(:,1)),2);



