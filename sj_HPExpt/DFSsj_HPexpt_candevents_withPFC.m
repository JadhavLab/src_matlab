
% Similar to DFSsj_HPexpt_getcandevents, but with PFC statistics as well
% As a precursoe to replay decoding, see how many candidate events you get

clear; %close all;
runscript = 1;
savedata = 0; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells

savedir = '/data25/sjadhav/HPExpt/ProcessedData/';
%val=1; cellcountthresh=5; savefile = [savedir 'HP_candevents5_isi_std3_speed4_ntet2_withPFC']; % _isi = 1sec ripple isi condition
%val=2; cellcountthresh=4; savefile = [savedir 'HP_candevents4_isi_std3_speed4_ntet2_withPFC']; 
%val=3; cellcountthresh=4; savefile = [savedir 'HP_candevents4_isi_std3_speed4_ntet1_withPFC']; 
val=4; cellcountthresh=2; savefile = [savedir 'HP_candevents2_isi_std3_speed4_ntet1_withPFC']; 

savefig=0;


% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    animals = {'HPa','HPb','HPc','Ndl'};
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    % dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    %sleepepochfilter = 'isequal($type, ''sleep'')'; % Only pre and post sleep marked as sleep
    
    % %Cell filter
    % %-----------
    % %PFC
    % %----
    % && strcmp($thetamodtag, ''y'')
    
            % All Ca1 cells and PFC Ripple modulated cells. Function will parse them out.
            %cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7)) ';                   
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && ($meanrate < 7))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag2, ''y'')) ';                   

    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % Iterator
    % --------
    iterator = 'multicellanal';
    
    % Filter creation
    % ----------------
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter, 'iterator', iterator);
 
    disp('Done Filter Creation');
    
    % Set analysis function
    % ---------------------
    switch val
        case {1,2}
            modf = setfilterfunction(modf,'DFAsj_getcandevents_withPFC',{'spikes','ripples','tetinfo','pos','cellinfo'},'cellcountthresh',cellcountthresh,'dospeed',1,'lowsp_thrs',4,'minrip',2,'minstd',3);
        case {3,4}
            modf = setfilterfunction(modf,'DFAsj_getcandevents_withPFC',{'spikes','ripples','tetinfo','pos','cellinfo'},'cellcountthresh',cellcountthresh,'dospeed',1,'lowsp_thrs',4,'minrip',1,'minstd',3);       
    end
    
    % Default stdev for ripples is 3. You will use tetinfo to get ripples
    
    % Run analysis
    % ------------
    modf = runfilter(modf);
    disp('Finished running filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript savedata
        save(savefile);
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end



% -------------------------  Filter Format Done -------------------------

% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 1; savegatherdata = 1;
switch val
    case 1
        gatherdatafile = [savedir 'HP_candevents5_isi_std3_speed4_ntet2_withPFC_gather'];
    case 2
        gatherdatafile = [savedir 'HP_candevents4_isi_std3_speed4_ntet2_withPFC_gather'];
    case 3
        gatherdatafile = [savedir 'HP_candevents4_isi_std3_speed4_ntet1_withPFC_gather'];
    case 4
        gatherdatafile = [savedir 'HP_candevents2_isi_std3_speed4_ntet1_withPFC_gather'];
end

if gatherdata
    % Parameters if any
    % -----------------
    % -------------------------------------------------------------

    cnt=0; % counting epochs
    anim_index=[]; allNcandevents=[]; allNcounts=[]; allNcountsvec = []; allNcells = [];
    pallNcandevents=[]; pallNcounts=[]; pallNcountsvec = []; pallNcells = [];
    
    
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1}) 

            cnt=cnt+1;
            
            % Variables for entire epoch
            anim_index{an}{i} = modf(an).output{1}(i).index;
            allNcandevents(cnt) = modf(an).output{1}(i).Ncandevents; % How many cand events in epoch
            allNcounts{cnt} = modf(an).output{1}(i).Ncounts; % No of active CA1 cells in each candidate event
            allNcountsvec = [allNcountsvec, modf(an).output{1}(i).Ncounts]; % Vectorize above 
            allNcells(cnt) = modf(an).output{1}(i).Ncells; % Total no. of CA1 cells in epoch
            
            
            pallNcounts{cnt} = modf(an).output{1}(i).Ncounts_PFC; % No of PFC active cells in each candidate event
            pallNcounts_withresp{cnt} = modf(an).output{1}(i).Ncounts_PFCwithresp; % No of PFC active cells in each candidate event, with at least 1 PFC cell responding
            pallNcandevents(cnt) = modf(an).output{1}(i).Ncandevents_PFCresp; % How many cand events in epoch with PFC resp
            
            pallNcountsvec = [pallNcountsvec, modf(an).output{1}(i).Ncounts_PFC]; % Vectorize above 
            pallNcells(cnt) = modf(an).output{1}(i).Ncells_PFC; % Total no. of PFC cells in epoch
            
            % [an day ep] - for Nspks/cell/ripple for CA1 cells
            [dayep] = modf(an).output{1}(i).index(1,:);
            Idxs(cnt,:) = [an,dayep];
            MeanSpksPerCell_candripples{cnt} = modf(an).output{1}(i).MeanSpksPerCell_candripples;
            NSpksPerCell_candripples{cnt} = modf(an).output{1}(i).NSpksPerCell_candripples;
            MeanSpksPerCell_allripples{cnt} = modf(an).output{1}(i).MeanSpksPerCell_allripples;
            NSpksPerCell_allripples{cnt} = modf(an).output{1}(i).NSpksPerCell_allripples;
            
            pMeanSpksPerCell_candripples{cnt} = modf(an).output{1}(i).MeanSpksPerCell_candripples;
            pNSpksPerCell_candripples{cnt} = modf(an).output{1}(i).NSpksPerCell_candripples;
            pMeanSpksPerCell_allripples{cnt} = modf(an).output{1}(i).MeanSpksPerCell_allripples;
            pNSpksPerCell_allripples{cnt} = modf(an).output{1}(i).NSpksPerCell_allripples;
            
        end     
    end

    % --------------------------------------------------
    % Combine across epochs - to get nos. across days
    % -------------------------------------------------
    cntdays = 0;
    uniqueIndices = unique(Idxs(:,[1 2]),'rows'); % Collapse across epochs
    % iterating only over the unique indices and finding matches in allindexes
    
    for i=1:length(uniqueIndices)
        curridx = uniqueIndices(i,:);
        ind=find(ismember(Idxs(:,[1 2]),curridx,'rows'))';
        
        currMc=[]; currNc=[];  currMa=[]; currNa=[]; currNev=[];
        pcurrMc=[]; pcurrNc=[];  pcurrMa=[]; pcurrNa=[]; pcurrNev=[];
        for r=ind
            r;
            currMc = [currMc; MeanSpksPerCell_candripples{r}]; % column vector of mean fr of each CA1 cell in ripple
            currNc = [currNc; NSpksPerCell_candripples{r}]; % each entry is no of spikes for a CA1 cell in a candidate ripple
            currMa = [currMa; MeanSpksPerCell_allripples{r}]; % column vector of mean fr of each CA1 cell in ripple
            currNa = [currNa; NSpksPerCell_allripples{r}]; % each entry is no of spikes for a CA1 cell in a candidate ripple
            currNev = [currNev; allNcandevents(r)];
            
            pcurrMc = [pcurrMc; MeanSpksPerCell_candripples{r}]; % column vector of mean fr of each PFC cell in ripple
            pcurrNc = [pcurrNc; NSpksPerCell_candripples{r}]; % each entry is no of spikes for a PFC cell in a candidate ripple
            pcurrMa = [pcurrMa; MeanSpksPerCell_allripples{r}]; % column vector of mean fr of each PFC cell in ripple
            pcurrNa = [pcurrNa; NSpksPerCell_allripples{r}]; % each entry is no of spikes for a PFC cell in a candidate ripple
            pcurrNev = [pcurrNev; pallNcandevents(r)];
            
        end
        
        
            cntdays = cntdays+1;
 
            % Save both in struct, and variable format
            alldayidxs(cntdays,:) = curridx;
            allCA1Resp_days(cntdays).MeanSpksPerCell_candripples = currMc;
            allCA1Resp_days(cntdays).NSpksPerCell_candripples = currNc;
            allCA1Resp_days(cntdays).MeanSpksPerCell_allripples = currMa;
            allCA1Resp_days(cntdays).NSpksPerCell_allripples = currNa;
            allNcandevents_days(cntdays) = sum(currNev);
            
            allPFCResp_days(cntdays).MeanSpksPerCell_candripples = pcurrMc;
            allPFCResp_days(cntdays).NSpksPerCell_candripples = pcurrNc;
            allPFCResp_days(cntdays).MeanSpksPerCell_allripples = pcurrMa;
            allPFCResp_days(cntdays).NSpksPerCell_allripples = pcurrNa;
            pallNcandevents_days(cntdays) = sum(pcurrNev);
            
    end
    
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data


% ------------
% PLOTTING, ETC
% ------------

% ------------------
% Population Figures
% ------------------

forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1
figdir = '/data25/sjadhav/HPExpt/Figures/Replay/';
summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end


% Plotting histogram of no. of CA1 cells in Candidate events
% ----------------------------------------------------------
figure; hold on;redimscreen_figforppt1;
vec = 0:1:20;
distr = histc(allNcountsvec,vec);
bar(vec, distr, 'r');
title(sprintf(['Distribution - No. of CA1 cells in candidate events with cellthresh ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
xlabel(['Number of cells'],'FontSize',24,'Fontweight','normal');
ylabel(['Number of events'],'FontSize',24,'Fontweight','normal');
set(gca,'XLim',[3 15]);
text(8,400,sprintf('Total Events = %d',length(allNcountsvec)),'FontSize',24,'Fontweight','normal');

disp(['Total Number of candidate events: ', num2str(length(allNcountsvec))]);

% Save figure
[y, m, d] = datevec(date);
%savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/%d_%d_%d_2e_ep2_%d', m, d, y, event_index);
savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/2015/Hist_NoOfCA1CellsInCandEvents_thresh4');
figfile = savestring;
if savefig==1
    print('-dpdf', savestring)
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end


% Plotting No. of candidate events across all epochs
% --------------------------------------------------
figure; hold on;redimscreen_figforppt1;
bar(allNcandevents);
title(sprintf(['No. of cand events for thrs ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
xlabel(['Epoch No'],'FontSize',24,'Fontweight','normal');
ylabel(['N cand events'],'FontSize',24,'Fontweight','normal');
line([16 16], [0 150], 'Color','r'); line([32 32], [0 150], 'Color','r'); line([48 48], [0 150], 'Color','r');
line([11 11], [0 150], 'Color','c'); line([27 27], [0 150], 'Color','c'); line([43 43], [0 150], 'Color','c');
%text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');

% Plotting No. of candidate events across all days
% --------------------------------------------------
figure; hold on;redimscreen_figforppt1;
bar(allNcandevents_days);
title(sprintf(['No. of candidate events with cellthresh ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
xlabel(['Day Number'],'FontSize',24,'Fontweight','normal');
ylabel(['Number of events'],'FontSize',24,'Fontweight','normal');
line([8.5 8.5], [0 150], 'Color','r'); line([16.5 16.5], [0 150], 'Color','r'); line([24.5 24.5], [0 150], 'Color','r'); % Mark Animal Transition
line([5 5], [0 150], 'Color','c'); line([13 13], [0 150], 'Color','c'); line([21 21], [0 150], 'Color','c'); % Mark Novel env
%text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');
text(3.5,140,sprintf('HPa'),'FontSize',24,'Fontweight','normal');
text(11,140,sprintf('HPb'),'FontSize',24,'Fontweight','normal');
text(19,140,sprintf('HPc'),'FontSize',24,'Fontweight','normal');
text(29,140,sprintf('Ndl'),'FontSize',24,'Fontweight','normal');

savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/2015/CA1_CandEventsvsDays_Thresh4');
figfile = savestring;
if savefig==1
    print('-dpdf', savestring)
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end



% Plot for PFC
% -----------
% Plotting histogram of no. of PFC cells in Candidate events
% ----------------------------------------------------------
figure; hold on;redimscreen_figforppt1;
vec = 0:1:20;
distr = histc(pallNcountsvec,vec);
bar(vec, distr, 'r');
title(sprintf(['No. of PFC cells in candidate events with cellthresh ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
xlabel(['Number of cells'],'FontSize',24,'Fontweight','normal');
ylabel(['Number of events'],'FontSize',24,'Fontweight','normal');
set(gca,'XLim',[3 15]);
set(gca,'XLim',[-0.9 7]);
text(2,350,sprintf('Total Events with PFC Response = %d',length(find(pallNcountsvec>0))),'FontSize',20,'Fontweight','normal');

disp(['Total Number of candidate events with PFC resp: ', num2str(length(find(pallNcountsvec>0)))]);

% Save figure
[y, m, d] = datevec(date);
%savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/%d_%d_%d_2e_ep2_%d', m, d, y, event_index);
savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/2015/Hist_NoOfPFCCellsInCandEvents_thresh4');
figfile = savestring;
if savefig==1
    print('-dpdf', savestring)
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end


% Plotting No. of candidate events with PFC resp across all epochs
% --------------------------------------------------
figure; hold on;redimscreen_figforppt1;
bar(pallNcandevents);
title(sprintf(['No. of cand events with PFC resp for thrs ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
xlabel(['Epoch No'],'FontSize',24,'Fontweight','normal');
ylabel(['N cand events with PFC resp'],'FontSize',24,'Fontweight','normal');
line([16 16], [0 150], 'Color','r'); line([32 32], [0 150], 'Color','r'); line([48 48], [0 150], 'Color','r');
line([11 11], [0 150], 'Color','c'); line([27 27], [0 150], 'Color','c'); line([43 43], [0 150], 'Color','c');
%text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');

% Plotting No. of candidate events with PFC resp across all days
% --------------------------------------------------
figure; hold on;redimscreen_figforppt1;
bar(pallNcandevents_days);
title(sprintf(['No. of events with PFC resp for cellthresh ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
xlabel(['Day Number'],'FontSize',24,'Fontweight','normal');
ylabel(['Number of events'],'FontSize',24,'Fontweight','normal');
line([8.5 8.5], [0 150], 'Color','r'); line([16.5 16.5], [0 150], 'Color','r'); line([24.5 24.5], [0 150], 'Color','r'); % Mark Animal Transition
line([5 5], [0 150], 'Color','c'); line([13 13], [0 150], 'Color','c'); line([21 21], [0 150], 'Color','c'); % Mark Novel env%text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');

text(3.5,140,sprintf('HPa'),'FontSize',24,'Fontweight','normal');
text(11,140,sprintf('HPb'),'FontSize',24,'Fontweight','normal');
text(19,140,sprintf('HPc'),'FontSize',24,'Fontweight','normal');
text(29,140,sprintf('Ndl'),'FontSize',24,'Fontweight','normal');

savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/2015/PFCResp_CandEventsvsDays_Thresh4');
figfile = savestring;
if savefig==1
    print('-dpdf', savestring)
    print('-depsc2', figfile); print('-djpeg', figfile); saveas(gcf,figfile,'fig');
end





% % CA1 Spike statistics during ripples
% % ------------------------------------
% 
% % Combine days for global statistics
% % -----------
% all_MeanSpksPerCell_candripples=[]; all_NSpksPerCell_candripples=[]; all_MeanSpksPerCell_allripples=[]; all_NSpksPerCell_allripples=[];
% for d = 1:length(allCA1Resp_days)
%     all_MeanSpksPerCell_candripples = [all_MeanSpksPerCell_candripples; allCA1Resp_days(d).MeanSpksPerCell_candripples];
%     all_NSpksPerCell_candripples = [all_NSpksPerCell_candripples; allCA1Resp_days(d).NSpksPerCell_candripples];
%     all_MeanSpksPerCell_allripples = [all_MeanSpksPerCell_allripples; allCA1Resp_days(d).MeanSpksPerCell_allripples];
%     all_NSpksPerCell_allripples = [all_NSpksPerCell_allripples; allCA1Resp_days(d).NSpksPerCell_allripples];
% end
%     
% Nall = length(all_NSpksPerCell_allripples);
% Ncan = length(all_NSpksPerCell_candripples);
% 
% figure; hold on; redimscreen_2versubplots;
% subplot(2,1,1); hold on;
% vec = 0:1:10;
% distr = histc(all_NSpksPerCell_candripples,vec); 
% distr_frac = distr./Ncan; 
% bar(vec, distr_frac, 'r');
% title(sprintf(['Distribution - NSpks/CA1 Cell/SWR in candidate replay events with cellthresh ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
% xlabel(['NSpks/CA1 cell/SWR'],'FontSize',24,'Fontweight','normal');
% ylabel(['Hist'],'FontSize',24,'Fontweight','normal');
% 
% subplot(2,1,2); hold on;
% vec = 0:1:10;
% distr = histc(all_NSpksPerCell_allripples,vec);
% distr_frac = distr./Nall; 
% bar(vec, distr_frac, 'r');
% title(sprintf(['Distribution - NSpks/CA1 Cell/SWR in ALL ripples']),'FontSize',24,'Fontweight','normal');
% xlabel(['NSpks/CA1 cell/SWR'],'FontSize',24,'Fontweight','normal');
% ylabel(['Hist'],'FontSize',24,'Fontweight','normal');
% 
% 
% % Plot vs days
% % -----------
% 
% % Shift days for Nadal, or use only 3 W-track animals maybe?
% Ndl = find(alldayidxs(:,1)==4);
% alldayidxs(Ndl,2) = alldayidxs(Ndl,2)-7; 
% 
% allWtridxs = alldayidxs(1:24,:);
% days = unique(allWtridxs(:,2));
% for d = 1:length(days)
%     curridxs = find(allWtridxs(:,2)==d);
%     tmpNc=[]; tmpNa=[];
%     for i=1:length(curridxs)
%         tmpNc = [tmpNc;allCA1Resp_days(curridxs(i)).NSpksPerCell_candripples];
%         tmpNa = [tmpNa;allCA1Resp_days(curridxs(i)).NSpksPerCell_allripples];
%     end
%     day_Nc{d}=tmpNc;
%     day_Na{d}=tmpNa;
%     
%     mean_dayNc(d) = mean(tmpNc); err_dayNc(d) = sem(tmpNc);
%     mean_dayNa(d) = mean(tmpNa); err_dayNa(d) = sem(tmpNa);
%     
% end
% 
% % Plot Nspks/CA1 cell/SWR vs days
% figure; hold on; redimscreen_2versubplots;
% subplot(2,1,1); hold on;
% plot(days,mean_dayNc,'ro-','LineWidth',2,'MarkerSize',8)
% errorbar(days, mean_dayNc,err_dayNc,'ro-')
% title(sprintf(['Distribution - NSpks/ CA1 Cell/ SWR in CAND ripples']),'FontSize',24,'Fontweight','normal');
% ylabel(['NSpks/ CA1 cell/ SWR'],'FontSize',24,'Fontweight','normal');
% xlabel(['Days'],'FontSize',24,'Fontweight','normal');
% set(gca,'YLim',[0 2.5]);
% 
% subplot(2,1,2); hold on;
% plot(days,mean_dayNa,'ro-','LineWidth',2,'MarkerSize',8)
% errorbar(days, mean_dayNa,err_dayNa,'ro-')
% title(sprintf(['Distribution - NSpks/CA1 Cell/SWR in ALL ripples vs. Days']),'FontSize',24,'Fontweight','normal');
% ylabel(['NSpks/ CA1 cell/ SWR'],'FontSize',24,'Fontweight','normal');
% xlabel(['Days'],'FontSize',24,'Fontweight','normal');
% set(gca,'YLim',[0 2.5]);






    









