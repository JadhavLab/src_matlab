
% Get no of CA1 cells and PFC cells during SWRs
% --------------------------------------------

% Based on DFSsj-HPexpt_getreplaydata_withPFChist -> sj_getpopulationevents2_withPFChist
% OR  DFSsj_HPexpt_candevents_withPFC -> DFAsj_getcandevents
% OR DFSsj_HPexpt_ripCA1popresp2




clear; %close all;
runscript = 0;
savedata = 1; % save data option - only works if runscript is also on
figopt1 = 0; % Figure Options - Individual cells

savedir = '/data25/sjadhav/HPExpt/HP_ProcessedData/';
figdir = '/data25/sjadhav/HPExpt/RipProp/';
savefig1=0;

cellcountthresh=0;

%--------------------------------------
%val=0; str='4anim';  savefile = [savedir 'HP_getSWRncells_X8']; % skip Rtl and Brg. errors

%val=1; str='PFCripmod_withCA1';  savefile = [savedir 'HP_getSWRncells_X8_CA1PFCripmod'];
val=2; str='PFCexc_withCA1';  savefile = [savedir 'HP_getSWRncells_X8_CA1PFCexc'];
%val=3; str='PFCinh_withCA1';  savefile = [savedir 'HP_getSWRncells_X8_CA1PFCinh'];




% If runscript, run Datafilter and save data
if runscript == 1
    
    %Animal selection
    %-----------------------------------------------------
    switch val
        case 0
            animals = {'HPa','HPb','HPc','Ndl'}
        case {1,2,3,4}
            animals = {'HPa','HPb','HPc','Ndl','Rtl','Brg'};
    end
    %animals = {'Rtl'};
    
    %Filter creation
    %-----------------------------------------------------
    
    % Epoch filter
    % -------------
    % dayfilter = '1:8'; % Shantanu - I am adding day filter to parse out epoch filter
    % Either Only do 1st w-track. 2 or 1 epochs per day
    % Or do Wtr1 and Wtr2, 2 epochs per day
    runepochfilter = 'isequal($environment, ''wtr1'') || isequal($environment, ''wtr2'') || isequal($environment, ''ytr'')';
    epochfilter = runepochfilter;
    
    %runepochfilter = 'isequal($type, ''run'')'; EASIER TO DO THIS
    %sleepepochfilter = 'isequal($type, ''sleep'')'; % Only pre and post sleep marked as sleep
    
    
    switch val
        case 0
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'')) ';
            
        case 1
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'')) ';
            
        case 2
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'') && strcmp($ripmodtype3, ''exc'')) ';
            
        case 3
            cellfilter = '((strcmp($area, ''CA1'') || strcmp($area, ''iCA1'')) && ($numspikes > 100) && strcmp($FStag, ''n''))  ||  (strcmp($area, ''PFC'') && ($numspikes > 100) && strcmp($ripmodtag3, ''y'') && strcmp($FStag, ''n'') && strcmp($ripmodtype3, ''inh'')) ';
            
    end
    
    
    
    % Time filter
    % -----------
    riptetfilter = '(isequal($descrip, ''riptet''))';
    
    % Iterator
    % --------
    iterator = 'multicellanal';
    
    
    % Filter creation
    % ----------------
    modf = createfilter('animal',animals,'epochs',runepochfilter, 'cells',...
        cellfilter, 'iterator', iterator);
    
    disp('Done Decode Filter Creation');
    
    % Set analysis function
    % ---------------------
    
    %switch val
    %    case {1,2}
    modf = setfilterfunction(modf,'DFAsj_getSWRncells',{'spikes','ripples','tetinfo','pos','cellinfo'},'cellcountthresh',0,'dospeed',1,'lowsp_thrs',4,'minrip',1);
    %end
    
    % Default stdev for ripples is 3. You will use tetinfo to get ripples
    
    % Run analysis
    % ------------
    modf = runfilter(modf);
    disp('Finished running decode filter script');
    %--------------------- Finished Filter Function Run -------------------
    
    if savedata == 1
        clear figopt1 runscript savedata
        save(savefile, '-v7.3');
    end
    
else
    
    load(savefile);
    
end % end runscript

if ~exist('savedata')
    return
end



% -------------------------  Filter Format Done -------------------------

% ----------------------------------
% Whether to gather data or to load previously gathered data
% --------------------------------------------------------------------
gatherdata = 0; savegatherdata = 1;
switch val
    case 0
        gatherdatafile = [savedir 'HP_getSWRncells_X8_gather'];
    case 1
        gatherdatafile = [savedir 'HP_getSWRncells_X8_CA1PFCripmod_gather'];
    case 2
        gatherdatafile = [savedir 'HP_getSWRncells_X8_CA1PFCexc_gather'];
    case 3
        gatherdatafile = [savedir 'HP_getSWRncells_X8_CA1PFCinh_gather'];
end

if gatherdata
    % Parameters if any
    % -----------------
    % -------------------------------------------------------------
    
    cnt=0; % counting epochs
    anim_index=[]; allNcandevents=[]; allNcounts=[]; allNcountsvec = []; allNcells = [];
    pallNcandevents=[]; pallNcounts=[]; pallNcountsvec = []; pallNcells = [];
    
    
    for an = 1:length(modf)
        for i=1:length(modf(an).output{1})
            
            if ~isempty(modf(an).output{1}(i).index);
                
                cnt=cnt+1;
                
                % Variables for entire epoch
                anim_index{an}{i} = modf(an).output{1}(i).index;
                try
                    allNcandevents(cnt) = modf(an).output{1}(i).Ncandevents; % How many cand events in epoch
                catch
                    keyboard;
                end
                
                allNcounts{cnt} = modf(an).output{1}(i).Ncounts; % No of active CA1 cells in each candidate event
                
                try
                    allNcountsvec = [allNcountsvec, modf(an).output{1}(i).Ncounts]; % Vectorize above
                catch
                    allNcountsvec = [allNcountsvec, modf(an).output{1}(i).Ncounts']; % Vectorize above
                end 
                allNcells(cnt) = modf(an).output{1}(i).Ncells; % Total no. of CA1 cells in epoch
                
                
                pallNcounts{cnt} = modf(an).output{1}(i).Ncounts_PFC; % No of PFC active cells in each candidate event
                %            pallNcounts_withresp{cnt} = modf(an).output{1}(i).Ncounts_PFCwithresp; % No of PFC active cells in each candidate event, with at least 1 PFC cell responding
                %            pallNcandevents(cnt) = modf(an).output{1}(i).Ncandevents_PFCresp; % How many cand events in epoch with PFC resp
                
                try
                    pallNcountsvec = [pallNcountsvec, modf(an).output{1}(i).Ncounts_PFC]; % Vectorize above
                catch
                    pallNcountsvec = [pallNcountsvec, modf(an).output{1}(i).Ncounts_PFC']; % Vectorize above
                end
                pallNcells(cnt) = modf(an).output{1}(i).Ncells_PFC; % Total no. of PFC cells in epoch
                
                % [an day ep] - for Nspks/cell/ripple for CA1 cells
                [dayep] = modf(an).output{1}(i).index(1,:);
                Idxs(cnt,:) = [an,dayep];
                %             MeanSpksPerCell_candripples{cnt} = modf(an).output{1}(i).MeanSpksPerCell_candripples;
                %             NSpksPerCell_candripples{cnt} = modf(an).output{1}(i).NSpksPerCell_candripples;
                %             MeanSpksPerCell_allripples{cnt} = modf(an).output{1}(i).MeanSpksPerCell_allripples;
                %             NSpksPerCell_allripples{cnt} = modf(an).output{1}(i).NSpksPerCell_allripples;
                %
                %             pMeanSpksPerCell_candripples{cnt} = modf(an).output{1}(i).MeanSpksPerCell_candripples;
                %             pNSpksPerCell_candripples{cnt} = modf(an).output{1}(i).NSpksPerCell_candripples;
                %             pMeanSpksPerCell_allripples{cnt} = modf(an).output{1}(i).MeanSpksPerCell_allripples;
                %             pNSpksPerCell_allripples{cnt} = modf(an).output{1}(i).NSpksPerCell_allripples;
                
            end
            
        end
    end
    
    % --------------------------------------------------
    % Combine across epochs - to get nos. across days
    % -------------------------------------------------
    cntdays = 0;
    uniqueIndices = unique(Idxs(:,[1 2]),'rows'); % Collapse across epochs
    % iterating only over the unique indices and finding matches in allindexes
    
    for i=1:length(uniqueIndices)
        curridx = uniqueIndices(i,:);
        ind=find(ismember(Idxs(:,[1 2]),curridx,'rows'))';
        
        currMc=[]; currNc=[];  currMa=[]; currNa=[]; currNev=[];
        pcurrMc=[]; pcurrNc=[];  pcurrMa=[]; pcurrNa=[]; pcurrNev=[];
        for r=ind
            r;
            %             currMc = [currMc; MeanSpksPerCell_candripples{r}]; % column vector of mean fr of each CA1 cell in ripple
            %             currNc = [currNc; NSpksPerCell_candripples{r}]; % each entry is no of spikes for a CA1 cell in a candidate ripple
            %             currMa = [currMa; MeanSpksPerCell_allripples{r}]; % column vector of mean fr of each CA1 cell in ripple
            %             currNa = [currNa; NSpksPerCell_allripples{r}]; % each entry is no of spikes for a CA1 cell in a candidate ripple
            currNev = [currNev; allNcandevents(r)];
            
            %             pcurrMc = [pcurrMc; MeanSpksPerCell_candripples{r}]; % column vector of mean fr of each PFC cell in ripple
            %             pcurrNc = [pcurrNc; NSpksPerCell_candripples{r}]; % each entry is no of spikes for a PFC cell in a candidate ripple
            %             pcurrMa = [pcurrMa; MeanSpksPerCell_allripples{r}]; % column vector of mean fr of each PFC cell in ripple
            %             pcurrNa = [pcurrNa; NSpksPerCell_allripples{r}]; % each entry is no of spikes for a PFC cell in a candidate ripple
            %            pcurrNev = [pcurrNev; pallNcandevents(r)];
            
        end
        
        
        cntdays = cntdays+1;
        
        % Save both in struct, and variable format
        alldayidxs(cntdays,:) = curridx;
        %             allCA1Resp_days(cntdays).MeanSpksPerCell_candripples = currMc;
        %             allCA1Resp_days(cntdays).NSpksPerCell_candripples = currNc;
        %             allCA1Resp_days(cntdays).MeanSpksPerCell_allripples = currMa;
        %             allCA1Resp_days(cntdays).NSpksPerCell_allripples = currNa;
        allNcandevents_days(cntdays) = sum(currNev);
        
        %             allPFCResp_days(cntdays).MeanSpksPerCell_candripples = pcurrMc;
        %             allPFCResp_days(cntdays).NSpksPerCell_candripples = pcurrNc;
        %             allPFCResp_days(cntdays).MeanSpksPerCell_allripples = pcurrMa;
        %             allPFCResp_days(cntdays).NSpksPerCell_allripples = pcurrNa;
        %             pallNcandevents_days(cntdays) = sum(pcurrNev);
        
    end
    
    
    % Save
    % -----
    if savegatherdata == 1
        save(gatherdatafile);
    end
    
else % gatherdata=0
    
    load(gatherdatafile);
    
end % end gather data


% ------------
% PLOTTING, ETC
% ------------

% ------------------
% Population Figures
% ------------------

forppr = 0;
% If yes, everything set to redimscreen_figforppr1
% If not, everything set to redimscreen_figforppt1

figdir = '/data25/sjadhav/HPExpt/Figures/RipProp/';

summdir = figdir;
set(0,'defaultaxesfontweight','normal'); set(0,'defaultaxeslinewidth',2);

if forppr==1
    set(0,'defaultaxesfontsize',16);
    tfont = 18; % title font
    xfont = 16;
    yfont = 16;
else
    set(0,'defaultaxesfontsize',24);
    tfont = 28;
    xfont = 20;
    yfont = 20;
end


% Plotting histogram of no. of CA1 cells in Candidate events
% ----------------------------------------------------------

mean(allNcountsvec), sem(allNcountsvec)
nonzero = length(find(allNcountsvec~=0))
frc_act_CA1 = nonzero./length(allNcountsvec)

figure; hold on;redimscreen_figforppt1;
vec = 0:1:20;
distr = histc(allNcountsvec,vec);
distr(1), distr(2), sum(distr(3:end))

bar(vec, distr, 'r');
title(sprintf(['Distribution - No. of CA1 cells in candidate SWRs with cellthresh ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
xlabel(['Number of cells'],'FontSize',24,'Fontweight','normal');
ylabel(['Number of SWRs'],'FontSize',24,'Fontweight','normal');
set(gca,'XLim',[-1 11]);
text(5,2000,sprintf('Total Events = %d',length(allNcountsvec)),'FontSize',24,'Fontweight','normal');

disp(['Total Number of candidate SWRs: ', num2str(length(allNcountsvec))]);

% Save figure
[y, m, d] = datevec(date);
%savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/%d_%d_%d_2e_ep2_%d', m, d, y, event_index);
%savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/2015/Hist_NoOfCA1CellsInCandEvents_thresh4');
%figfile = savestring;


figfile = [figdir,'SWR_nCA1cells_all']
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end



% Plotting histogram of no. of PFC cells in Candidate events
% ----------------------------------------------------------

mean(pallNcountsvec), sem(pallNcountsvec)
nonzero = length(find(pallNcountsvec~=0))
frc_act_PFC = nonzero./length(pallNcountsvec)

figure; hold on;redimscreen_figforppt1;
vec = 0:1:20;
distr = histc(pallNcountsvec,vec);
distr(1), distr(2), sum(distr(3:end))

bar(vec, distr, 'r');
title(sprintf(['Distribution - No. of PFC cells in candidate SWRs with cellthresh ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
xlabel(['Number of cells'],'FontSize',24,'Fontweight','normal');
ylabel(['Number of SWRs'],'FontSize',24,'Fontweight','normal');
set(gca,'XLim',[-1 4]);
text(3,5000,sprintf('Total Events = %d',length(allNcountsvec)),'FontSize',24,'Fontweight','normal');

disp(['Total Number of candidate SWRs: ', num2str(length(allNcountsvec))]);

% Save figure
[y, m, d] = datevec(date);
%savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/%d_%d_%d_2e_ep2_%d', m, d, y, event_index);
%savestring = sprintf('/data25/sjadhav/HPExpt/Figures/Replay/2015/Hist_NoOfCA1CellsInCandEvents_thresh4');
%figfile = savestring;

figfile = [figdir,'SWR_nPFCcells_',str]
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end




% Plotting No. of candidate events across all epochs: This is just no of SWRs
% ----------------------------------------------------------------------------------
figure; hold on;redimscreen_figforppt1;
bar(allNcandevents);
title(sprintf(['No. of SWRs for thrs ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
xlabel(['Epoch No'],'FontSize',24,'Fontweight','normal');
ylabel(['N SWRs'],'FontSize',24,'Fontweight','normal');
line([16 16], [0 150], 'Color','r'); line([32 32], [0 150], 'Color','r'); line([48 48], [0 150], 'Color','r');
line([11 11], [0 150], 'Color','c'); line([27 27], [0 150], 'Color','c'); line([43 43], [0 150], 'Color','c');
%text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');

% Plotting No. of candidate events across all days: This is just no of SWRs
% ---------------------------------------------------------------------------------

mean(allNcandevents_days), sem(allNcandevents_days)

figure; hold on;redimscreen_figforppt1;
bar(allNcandevents_days);
title(sprintf(['No. of SWRs with cellthresh ',num2str(cellcountthresh)]),'FontSize',24,'Fontweight','normal');
xlabel(['Day Number'],'FontSize',24,'Fontweight','normal');
ylabel(['N SWRs'],'FontSize',24,'Fontweight','normal');
line([8.5 8.5], [0 150], 'Color','r'); line([16.5 16.5], [0 150], 'Color','r'); line([24.5 24.5], [0 150], 'Color','r'); % Mark Animal Transition
line([5 5], [0 150], 'Color','c'); line([13 13], [0 150], 'Color','c'); line([21 21], [0 150], 'Color','c'); % Mark Novel env
%text(-0.22,0.4,['Npairs:' num2str(length(allmodelb))],'FontSize',24,'Fontweight','normal');
text(3.5,690,sprintf('HPa'),'FontSize',24,'Fontweight','normal');
text(11,690,sprintf('HPb'),'FontSize',24,'Fontweight','normal');
text(19,690,sprintf('HPc'),'FontSize',24,'Fontweight','normal');
text(29,690,sprintf('Ndl'),'FontSize',24,'Fontweight','normal');

figfile = [figdir,'NSWRs_',str]
if savefig1==1,
    print('-dpdf', figfile); print('-dpng', figfile, '-r300'); saveas(gcf,figfile,'fig'); print('-depsc2', figfile); print('-djpeg', figfile);
end

